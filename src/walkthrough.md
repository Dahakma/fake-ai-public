# Walkthrough  (Spoilers!)

- [Main Stats](#main-stats)
- [First Nerd](#first-nerd)
- [Second Nerd](#second-nerd)
- [Third Nerd](#third-nerd)
- [Nerds](#nerds)
- [Saša](#sasa)
- [Eva](#eva)
- [Chairman](#chairman)

***

## Main Stats

### Dominant X Submissive 

- Kate could be submissive or dominant
- acting confidently and dominantly unlocks paths with more dominant and confident Kate and vice-versa
- `mile.sub` (negative dominant) (global variable accessible via the browser's console)

### Prude X Slut 

- Kate could be a slut or a prude
- it is left ambiguous how slutty Kate was before the start of the game and readjusted retroactively 
- higher sluttiness unlocks more sexually-shameless choices, events and tasks; also sluttier clothes in shops
- prude Kate is more embarrassed and uncomfortable, there are some low-slut events or tasks which would not bother slutty Kate
- `mile.slut` (negative prude)

### Popularity

- how much is Kate popular and respected among her peers
- Kate loses popularity by being publicly humiliated and doing things that might make her seem weak, insecure or asocial 
- at the moment badly implemented and heavily wip 
- `mile.stat` (higher better)

### Gamer

- Kate might find out she actually enjoys the game and nerdy stuff 
- Kate hating the game is less attached, e.g. sees the characters in the game only as NPCs 
- Kate liking the game is more immersed and interested in the lore
- `mile.immersion` (negative hates)

### School

- Kate could have good or bad grades (1 is the best grade; 5 is the worst)
- `mile.grades` (lower is better)
- random events at school (e.g. biology examination)
- optional events (e.g. study chemistry with AAA)
- consequence of tasks (e.g. vibrating panties)

### Lesbian

- Kate can discover she is interested in women
- even though making the full lesbian route would be probably impossible due to the structure of the game
- omnisexual AI; encounters in the virtual game; Saša
- `mile.lesbian`

***


## First Nerd

- AAA has a serious crush on Kate. Kate could refute him (making him angry) or start dating him (mostly not specified whether she is into him or fakes the relationship)

### Hate 

- Kate and AAA actively hate each other
- Kate refused the apology or refused to date him or betrayed him
- `mile.a_burned === true` burned relationship
- `mile.a_hate` how much

### Girlfriend 

- Kate is dating AAA
- `mila.a_dating === true` and possibly `mila.a_girlfriend === true` if things get more serious
- `mile.a_love` how much

### Dominant X Submissive Girlfriend

- Kate is either more dominant or submissive in the relationship
- `mile.a_dom` (negative is Kate is submissive)

### Cuckolding 

- Kate makes AAA jealous by flirting and sleeping with other men
- generally a subroute of *Dominant Girlfriend* but some events are accessible in *Hate* route
- `mile.a_cuck`

***


## Second Nerd

- BBB enjoys humiliating and degrading Kate. She can submit or turn the tables. 

### Slave 
- Kate agrees to be BBB's slave
- `mile.b_slave === true`

### Victim X Masochist Slave
- Kate could despise or kinda perversly enjoy the slavery
- `mile.b_vic` - victim points (Kate suffers)
- `mile.b_mas` - masochist points (Kate enjoys it)

### Domina 
- Kate can turn the situation and dominate BBB
- `mile.b_domina === true`
- `mile.b_cruel` - positive cruel domina; negative nice one

***


## Third Nerd

- all three arcs could be combined; there is no one decision locking the other paths; however, some scenes might be locked/skipped

### Friend

- Kate enjoys hanging out with CCC and his interests
- `mile.c_friend`

### Bimbofication

- Kate is pushed into modifying and bimbofying her body 
- `mile.c_bimbo`

### Feminization

- Kate is feminizing CCC
- at the moment wip, not yet implemented 
- `mile.c_tg`

***


## Nerds

### Nice nerds TODO
- nice nerds do not want to hurt Kate
- AAA *Dominant Girlfriend*
- AAA *Girlfriend*
- BBB *Cruel domina*
- CCC *Friend*
- CCC *Feminization?*

### Mostly nice nerds TODO 
- AAA *Submissive Girlfriend*
- BBB *Slave Masochist*
- BBB *Nice Domina*

### Mean nerds TODO
- do not particularly care about being mean to Kate or actively want to destroy her
- AAA *Hate*
- BBB *Slave Victim* (despite what he claims)
- BBB default
- CCC *Bimbo*
- CCC default

### Game Advice
- Kate needs advice from the nerds to help her with the game
- 1st advice - before Feyd Mora - sell your clothes (`mile.advice_1`)
	- AAA - kiss (`1`)
	- BBB - degradingly beg (`2`)
	- CCC - wear something pink (`3`)
- 2nd advice - before Ship - hire the first mate, sail to Karged (`mile.advice_1`)
	- AAA - glasses
	- BBB - choker
	- CCC - pink hair
- 3rd and 4th advice - co-op game - (`mile.advice_3` and `mile.advice_4`)
	- AAA - just talk him into it
	- BBB - blowjob
	- CCC - panties

***

## Saša 

- Saša is timid Kate's friend

### Confident Saša
- boosting her confidence makes her more self-assured and dominant
- `mile.sas_sassy`

### Vengeful Saša
- exploiting her makes her more side with Eva rather than Kate
- `mile.sas_exploit`

### Lesbian  
- Saša has a secret lesbian crush on Kate
- triggered by Saša's nudes (task) or by Tricked by girls (optional) (TODO not implemented)

### Lover x Friend with Benefits 

- they could keep their relationship casual or get more serious 
- `mile.sas_love` - lovers 

### Dominant x Submissive

- Saša could become the more dominant person if her confidence or Kate's submissiveness are high 
- `mile.sas_dom` 

***


## Eva 

- Eva enjoy mischievously messing with people and would not mind to become the most popular girl instead of Kate

***


## Chairman

- Kate wants to be reelected to the position of the class chairwoman and the class representative on the student council
- her chances are lowered by losing popularity and displeasing the form teacher; increased by convincing people to support her
- TODO, wip, at the moment badly implemented 

