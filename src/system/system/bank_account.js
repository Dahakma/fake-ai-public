import {time} from "System/time";
import {money} from "Data/items/prices";
import {shops} from "Data/items/shops";
import {popup} from "Libraries/popup/index.js";

/*
👩
👨
💰
💶
🏬
💎
🎽
👙
👗
👢
👠
👒
👖
💍
🍸
🍷
👕
💉
🏥
⚕️
🌺
✴️
*/

export const accounts = {
	error: {
		id: 0,
		icon: `?`,
		name: `Error`,
	},
	mother: {
		id: 1,
		icon: `👩`,
		name: `Ivana @katsur`,
	},
	cash: {
		id: 2,
		icon: `💰`,
		name: `Cash deposit`, 
	},
	qaa: {
		id: 11,
		icon: `👨`,
		name: `@qaa`, 
	},
	qbb: {
		id: 12,
		icon: `👨`,
		name: `@qbb`, 
	},
	qcc: {
		id: 13,
		icon: `👨`,
		name: `@qcc`, 
	},
	clinic: {
		id: 21,
		icon: `🌺`,
		name: `Cosmetic Clinic`, 
	},
	dildo: {
		id: 22,
		icon: `🍆`,
		name: `Dildopolis`, 
	},
	requiem: { //bar - task kissing girls
		id: 23,
		icon: `🍷`,
		name: `Requiem`, 
	},
	corp: { //corporation where father works - watering plants
		id: 24,
		icon: `🏢`,
		name: `Megacorp`, //TODO
	},
	starz: { //karaoke bar where eva works
		id: 25,
		icon: `🎤`,
		name: `Starz`, //TODO
	},
	shop: { 
		id: 26,
		icon: `🛒`,
		name: `Shop`, 
	},
	drug: { 
		id: 27,
		icon: `🧴`,
		name: `Drugstore`, 
	},
	tattoo: { 
		id: 28,
		icon: `💘`,
		name: `Tattoo Studio`, 
	},
	agency: { 
		id: 29,
		icon: `✴️`,
		name: `Agency`,  //TODO
	},
	hair: { 
		id: 30,
		icon: `✂️ `,
		name: `Hair Salon`, 
	},

};

Object.entries(shops).forEach( ([key,{id, icon, name}]) => accounts[key] = {id, icon, name} );

/*
console.warn("x");
console.warn(accounts);
*/

export const standing_orders = function(){
	if(time.day === "sun"){
		if(mile.pocket_money > 0){
			payment(money.pocket_money_increased, "mother");
		}else if(mile.pocket_money < 0){
			payment(money.pocket_money_decreased, "mother");
		}else{
			payment(money.pocket_money, "mother");
		}
		
		if(mile.clinic_boobjob > 0){
			payment(-money.boobs, "clinic");
		}
	}
}

export const payment = function(amount, key){
	if(typeof amount === "string"){
		if(amount[0] === "-"){ //TODO - THIS IS STUPID
			amount = -money[amount.slice(1)];		
		}else{
			amount = money[amount];	
		}
	}
	
	if( isNaN(amount) || !accounts[key] ){
		console.warn(`WARNING: invalid money change (key: ${key}; money: ${amount}) (canceled)`);
		return;
	}
	amount = Math.round(amount * 100)/100;
	ext.stats.payments.push( pack(amount, accounts[key].id, head.day) );
	ext.irl_money += amount;
	
	if(SETTING.notifications_payment) popup.note(`${accounts[key].icon}<span style = "font-weight: bold; color: ${amount > 0 ? "green" : "red"}"> ${amount} € </span>`);
}

export const pack = function(money, id, date){
	date = (`${date}`).padStart(2,0);
	id = (`${id}`).padStart(2,0);
	return `${money}${id}${date}`;
}

//used by account_balance.svelte
export const unpack = function(code){
	const date = +code.slice(-2);
	const id = +code.slice(-4, -2);
	const money = +code.slice(0, -4);
	const key = Object.keys(accounts).find( key => accounts[key].id === id );

	return {
		date,
		money: Math.round(money * 100)/100,
		icon: accounts[key].icon,
		name: accounts[key].name,
		account: accounts[key],
		key,
	}
}


//when date & key are the same, the payments are grouped together
//TODO - when trigger? on save? on new payment? when bank screen opened? 
//TODO - ATM triggers on bank screen opened (account_balance.svelte)
export const compress = function(){
	const items = ext.stats.payments.map( a => unpack(a) );
	
	for(let i = 0; i < items.length - 1; i++){
		if(items[i].date === items[i+1].date && items[i].key === items[i+1].key){
			items[i+1].money += items[i].money
			items.splice(i, 1);
			i--;
		}
	}
	
	ext.stats.payments.length = 0;
	items.forEach(  ({money, account, date}) => ext.stats.payments.push( pack(money, account.id, date) )  );
}



	
	
	



		
		