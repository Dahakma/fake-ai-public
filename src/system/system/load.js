//SAVING & LOADING FROM THE LOCAL STORAGE OF THE BROWSER 
import {toHSLA} from "Libraries/color/index";
import {constants} from "Data/constants";
import {version, NPC, NAME, gamekey} from "./variables";
import {default_npcs} from "Data/npcs/npcs"
import {author} from 'System/variables';
const root = document.documentElement;


const default_stuff = {
	npcs: default_npcs,
	css: constants.css,
	setting: constants.setting,
};


//check if the local storage is available 
export const storage_disabled = (()=> {
	const test = 1;
	try {
		localStorage.setItem("test", test);
		localStorage.removeItem("test");
		return false;
	} catch(e) {
		/* eslint-disable no-console */
			//TODO - error message??
			console.warn(`WARNING: the "localStorage" is disabled! No progress will be saved in the memory of the browser and will be lost forever after reload! `);
		/* eslint-enable no-console */
		return true;
	}
})();


//initialise & save/load settings and stuff from localStorage; runs when the game is loaded 
export function check(){
	
	if(storage_disabled){
		apply_everything(default_stuff);
		/* eslint-disable no-console */
			console.log(`>Default variables and settings initiated. `); //LOPRO
		/* eslint-enable no-console */
		return;
	}
	
	//NEW - INITIALISE
	if (localStorage.getItem(`${gamekey}_initiated`) === null){ //new game

		localStorage.setItem(`${gamekey}_initiated`, 1);
		localStorage.setItem(`${gamekey}_version`,version);
		
		save(default_stuff);
		apply_everything();
		
		/* eslint-disable no-console */
			console.log(`>Default variables and settings initiated. `); //LOPRO
		/* eslint-enable no-console */
		
	//WRONG VERSION
	}else if(localStorage.getItem(`${gamekey}_version`) != version){
		const original = localStorage.getItem(`${gamekey}_version`);
		
		//stuff changed between versions here comes here:
		

		apply_everything(load());
		localStorage.setItem(`${gamekey}_version`,version);	
		
		/* eslint-disable no-console */
			console.log(`>Default variables and settings updated from ${original} to ${version}. `); //LOPRO
		/* eslint-enable no-console */
		
	//THE SAME VERSION
	}else{ 
		apply_everything(load());
		/* eslint-disable no-console */
			console.log(`>Default variables and settings loaded. `); //LOPRO
		/* eslint-enable no-console */
		
	}
	
}
	

	
export function apply_everything(values = {}){
	apply_setting(values.setting);
	apply_css(values.css);
	//apply_avatar(settings.avatar);
	apply_npcs(values.npcs);
}



//SETTINGS
export function apply_setting(setting){
	setting = load_redefine(setting, "setting");

	//UNEFINED VARIABLES (like when new one was added between versions)
	Object.keys(default_stuff.setting).forEach( a => {
		if(setting[a] === undefined) setting[a] = default_stuff.setting[a];
	});
	
	Object.keys(setting).forEach( a => SETTING[a] = setting[a] );
	
	if(SETTING.debug){
		SETTING.hardbug = true;
		SETTING.softbug = true;
	}else if(SETTING.hardbug){
		SETTING.softbug = true;
	}
	
	if(author !== "DAHAKMA"){
		SETTING.hardbug = false;
		//SETTING.softbug = false;
		SETTING.debug = false;
	}
	
	//setting handling css is wacky but whatever
	root.style.setProperty("--avatar", SETTING.avatar_width);
	
	debug = SETTING.debug;
	hardbug = SETTING.hardbug;
	softbug = SETTING.softbug;
}


//CSS - change the css of the page

export function apply_css(css){
	css = load_redefine(css, "css");
	const tweak = 8;
	
	function set(variable, color){
		root.style.setProperty(variable, color);
	}	

	function grad(color){
		const co = toHSLA(color);
		return `linear-gradient(to right bottom, hsla(${co.h}, ${co.s}%, ${co.l + tweak}%), hsla(${co.h}, ${co.s}%, ${co.l - tweak}%) 100%)`;
	}	
	
	
	set("--bad", css.bad);
	set("--good", css.good);
	
	set("--text", css.text);
	set("--bgr", css.bgr);
	set("--grad", grad(css.bgr) );	
	
	//sides menu
	if(SETTING.bgr2){
		set("--bgr2", css.bgr2);
		set("--grad2", grad(css.bgr2) );	
	}else{
		set("--bgr2", `transparent`);
		set("--grad2", `undefined`); //TODO - should not work with css but makes it not working ans skips it anyway
	}
	
	//page background
	if(SETTING.bgr3){
		set("--textBackgroundColor", css.bgr); //used in the right div
		set("--textBackgroundGrad", grad(css.bgr) );
		
		set("--pageBackgroundColor", css.bgr3); //used in the maint div
		set("--pageBackgroundGrad", grad(css.bgr3) );
		
		set("--bgr3", css.bgr3);
		set("--grad3", grad(css.bgr3) );	
		
	}else{
		set("--textBackgroundColor", `transparent`); //used in the right div
		set("--textBackgroundGrad", `undefined`);
		
		set("--pageBackgroundColor", css.bgr); //used in the maint div
		set("--pageBackgroundGrad", grad(css.bgr) );
		
		
		set("--bgr3", `transparent`);
		set("--grad3", `undefined`); //TODO - should not work with css but makes it not working ans skips it anyway
	}
	
	set("--highlight",css.highlight);
	

}



//NPCS - takes npc; saves to local storage, creates NPC and NAME
export function apply_npcs(npcs){

	//load all npc properties
	let input = npcs ? JSON.parse( JSON.stringify(npcs) ) : undefined;
	const original = JSON.parse( JSON.stringify(default_npcs) );
	for(const key in NPC) delete NPC[key];

	if(input) input = filter_npc_properties(input); //because as "input" is usually inputed whole "NPC" but only user-editable 

	input = load_redefine(input, "npcs"); //if input defined saves it; if not loads default

	Object.keys(original).forEach( a => {
		NPC[a] = {};
		Object.keys(original[a]).forEach( b => {
			NPC[a][b] = input[a]?.[b] ?? original[a]?.[b];
		});
	});
	
	Object.entries(NPC).forEach( ([key,val]) => {
		NAME[key] = val.name ? val.name : val.surname;
		if(val.prefix) NAME[key] = `${val.prefix} ${NAME[key]}`;
		if(val.altname) NAME[`${key}alt`] = val.altname;
		if(val.gamename) NAME[`${key}game`] = val.gamename;
		if(!val.prefix && val.surname) NAME[`${key}sur`] = NPC[key].surname;
	});


}

function filter_npc_properties(input){ //saves only names & colors; other properties are not changed 
	const out = {};
	Object.keys(input).forEach( a => {
		out[a] = {};
		if(input[a].name) out[a].name = input[a].name;
		if(input[a].altname) out[a].altname = input[a].altname; //damn you Katarína
		if(input[a].surname) out[a].surname = input[a].surname; 
		if(input[a].color) out[a].color = input[a].color; 
	});
	return out;
}



//if new_input uses and saves new_input; if undefined, loads and uses values from localStorage
function load_redefine(new_input, what){
	if(storage_disabled){
		if(new_input){
			return new_input;
		}else{
			return default_stuff[what];
		}
	}

	if(new_input){
		const temp = load();
		temp[what] = new_input;
		save(temp);
		return new_input;
	}else{
		return load()[what];
	}
}



//accessing localStorage
function load(){
	return JSON.parse(localStorage.getItem(`${gamekey}_settings`));
}

function save(settings){
	localStorage.setItem(`${gamekey}_settings`, JSON.stringify(settings));	
}