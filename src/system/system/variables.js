export const version = "0.9.53";
export const author = "DAHAKMA";
export const gamekey = "FAI92"

window.reset = function(){
	localStorage.removeItem(`${gamekey}_initiated`);
	location.reload();
}

//HEAD - controls the the current state of the game, scene, chapter, etc. 
window.head = {};

export const default_head = {
	day: 0,	
	schedule: 0, //current part of day
	scene: 101,
	chapter: "intro",
	reality: 1, //1 - normal world; 2 - metaverse; 3 - game
	act: 1, //
	act_chapter: `feyd`,
	act_scene: 101,
	
	//autosaved: false,
	
	present: {},
	placetime: { //describtion of place and time (for saving)
		place: "",
		time: "",
	},
	
	//TODO - SHOULD BE SHEDULE HERE????
	today: {}, //what is happening today 
	tomorrow: {},  //what is going to happen tommorow
	yesterday: {}, //what happened yesterday
	workday: {},  //the next workday (things that won't happen durning weekend, i.e. events at school)
	before_workday: {}, //before the next workday (i.e. Sunday)
	weekend: {},
};

//COUNTER - temporary variable, but have to be saved
window.counter = {};

//PC - dad object, player character 
window.PC = {
	basedim: {},
	Mods: {},
	inventory: [],
	gameInventory: [],
}; 

//DEBUG -game is in debugging mode
window.debug = false;
window.softbug = false;
window.hardbug = false;

//ATTRIBUTES - all other stuff related to player which I don't want to save in PC (nor miles)
window.ext = {};

export const default_PC = {
	apex: {
		ears: "elf", //elf, cat
		horns: "straight", //straight, curvy
		pupils: undefined, //cat
		hoofs: undefined, //hoofs
		tail: 0, //demo cat
		wings: 0, //demo cat
		game_lips: undefined,
		irl_lips: undefined,
		game_nails: undefined,
		irl_nails: undefined,
		game_mascara_top: undefined,
		game_mascara_bot: undefined,
		irl_mascara_top: undefined,
		irl_mascara_bot: undefined,
		
		
		//kate default (shouldn't be here)
		earrings: true, 
		bellybutton: true,
		
		//head
		eyebrow: false,
		tongue: false, 
		
		//nose
		noseSide: false, 
		noseTop: false, 
		noseBot: false, 
		//noseChain
			
		//lips
		lipsTop: false, 
		lipsBot: false, 
		lipsSide: false, 
		
		//nipples
		nipples: false, 
		nipplesBot: false, 
		//nippleChain
		
		//genital
		labia: false,
		labiaBot: false,
		pubic: false,
		hch: false,
		vch: false,
		
			
			
	},
	
	base: {
		char: 0, //charisma
		str: 0, //strength
		int: 0, //intelligence
		dex: 0, //dexterity 
		
		hp: 0,
		eng: 0,
	},
	
	att: { //base + modification
	},
	
	dyno: { //hp; eng (PC.att.hp is max value; PC.dyno.hp is dynamic actual value;
	},
	
	effects: [],
};


export const default_ext = {
	class: 2, //1 harlot; 2 hellion; 3 fallen;
	background: 0, //0 //1 party girl; 2 athlete; 3 president;
//BEWARE!!!
	race: 1, //0 human; 1 elf; 2 orc;
	origin: 1, //game character background/history: 1 strong; 2 dexterous; 3 intelligent; 4 charismatic
	
	irl_money: 0, //set in Data/items/prices (rewards.irl_initial)
	game_money: 0, //set in Data/items/prices (rewards.game_initial)
	
	last_optional: undefined, //type of last picked optional
	
	location: 0, //for ship exploration shenanigans
	//act: 1,
	
	/*
	char: 0, //charisma
	str: 0, //strength
	int: 0, //intelligence
	dex: 0, //dexterity 
	
	hp: 30,
	eng: 30,
	*/
		
	level: 1, //virtual game character level 
	
	//follower: {},
	
	
	
	/*
	stuff: {
		lips_irl: undefined,
		lips_game: undefined,
	}
	*/
	designs: [
		"tightTee",
	],
	
	outfits: [
		/*
			auto_irl, auto_game, auto_meta, auto_inventory
		*/
		/*
		{
			name: ""
			setting: 0,
			random: true,
			items: [
				{
					id
					seed
				}
			],
		}
		*/
	],
	makeups: [],

	dressup: {
		//default
		get_dressed: "random", //"last_worn", "random", "outfit", "conservative", "custom", "slutty"	
		//custom setting
		follow_rules: false,
		bra: true, //wear bra
		panties: true, //wear panties
		sexy_underwear: false, //sexy underwear
		dresses: true, //dresses
		skirts: true, //skirts
		pants: true, //pants
		socks: true,
		high_socks: true,
		stockings: true,
		pantyhose: true,
		piercings: true,
		choker: false,
		collar: false,
		belt: false,
		slut: undefined,
		glasses: false,
		gloves: false,
	},
	
	timers:  [],
	
	potions: {},
	
	stats: {
		potions: {}, //drink potions in virtual game
		deaths: 0, //killed in virtual game
		plugged: 0, //days with inserted buttplug
		commando: 0, //days without wearing underwear
		no_bra: 0, //without wearing bra
		tasks: [],
		payments: [],
		
		self: 0, //masturbation
		cum: [], //keys of NPC who cummed into Kat irl
		sex_m: [0,0,0,0], //empty, irl, meta, game
		sex_f: [0,0,0,0], 
		oral_s: [0,0,0,0], //received oral sex
		oral_m:  [0,0,0,0], //give blowjob
		oral_f: [0,0,0,0], //give cunnilingus
		anal_s:  [0,0,0,0], //received anal sex
		anal_m:  [0,0,0,0], //pegged guy
		anal_f: [0,0,0,0], //pegged girl
	},
	
	rules: {
		show_legs: false,  //allowed only skirts and shorts 
		short_skirt: false, //can wear only sort skirts
		sexy_top: false,
		no_bra: false,
		sexy_panties: false,
		sexy_legwear: false,
		
		collar: false,
		choker: false,
		no_panties: false, //TODO
	},
	
	quests: {
		/*
		roses: {
			title: `Collect 4 roses `,
			objective: "collect"
			counter:
			desired:
			check(){
			
			}
		}
		
		
		*/
		
	},
	
	follower: undefined,
	dungeon: undefined,
	
	//npc: {}, //seeds for npc???? - allows reroll
	
	irl: {}, //Mods irl
	meta: {}, //Mods meta
	game: {}, //Mods game
	user: {}, //Mods manually edited by the player
	
	original: {}, //Mods original character TODO - LEGACY, REPLACED BY USER; delete 
	
	
	snapshots: [],
	
	//checkpoint: {},  //saved game state in the case character dies in dungeon (clothes, follower)
	bars: undefined, //status bars
	
	

	
	slut: 0, //general slut level; dynamically calculated based on mile.slut
	/*
		0 - prude
		1 - tease
		2 - slut
		3 - superslut 
		4 - hyperslut
	*/
	
	sub: 0,
	/*
		0 despotic
		1 dominant
		2 confident
		3 humble
		4 meek
		5 pathetic
	*/
	
	stat: 0,
	/*
		0 pariah
		1 outcast
		2 falling
		3 cool
		4 popular
		5 princess
		6 queen
	*/
	
	counter: {}, //temp storage for window.counter that has to survive midnight
};

//MILESTONES - progress of the main character through the game and players decisions
window.mile = {}

export const stored_mile = {}; //window.mile is saved before the scene happens (used for saves, loadings runs the scene again, which would otherwise changed the variables twice


//DEFAULT SETTINGS
export const NPC = {};
export const NAME = {};


window.SETTING = {};


/*
export const default_stuff = {
	
	
	npcs: {},
};



//from "default_NPC" takes properties which are user-editable and puts tem to "default_stuff.npcs"
Object.keys(default_NPC).forEach( a => {
	default_stuff.npcs[a] = {
		name: default_NPC[a].name,
		color: default_NPC[a].color,
	}
	if( default_NPC[a].surname ) default_stuff.npcs[a].surname = default_NPC[a].surname;
	if( default_NPC[a].altname ) default_stuff.npcs[a].altname = default_NPC[a].altname;
});
*/

//console.warn(default_stuff.npcs);



	
	


