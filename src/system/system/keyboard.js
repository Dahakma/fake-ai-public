export const KEYBOARD = {
	
//to esc could be assigned something
	back(input, keycode, force){
		if(this.back_force && force !== true) return;
		if(Array.isArray(keycode)){
			this.back_keycode = keycode[0];
			this.back_keycode_alt = keycode[1];
		}else{
			this.back_keycode = keycode;
			this.back_keycode_alt = undefined;
		}
		this.back_fce = input;
		this.back_force = force;
	
	},
	
	back_force: undefined,
	back_key: undefined,
	back_key_alt: undefined,
	back_fce: ()=>{},
	
	
	//TODO TEMPFIX
	force_clear_back(){
		this.back_force = undefined;
		this.back_key = undefined;
		this.back_key_alt = undefined;
		this.back_fce = ()=>{};
	},
	
//DYNAMICALLY ATTACHED
	//"attached" are objects of {function, key or code}, the function is called when the key is pressed
	attached:  [],
	attach(fce, keycode){ //keycode means both KeyboardEvent.key and KeyboardEvent.code, checks both
		//checks if already assigned, overrides it
		const existing = this.attached.findIndex( a => a.keycode === keycode);
		if(existing !== -1) this.attached.splice(existing, 1); 
		this.attached.push({fce, keycode});
	},
	
	
//PAUSE
	//keyboard could be temporarily disabled 
	paused: false,
	pause(){		
		KEYBOARD.paused = true;
	},
	unpause(){
		KEYBOARD.paused = false;
	},
	
	
	
//NUMBERED EVENTS
	//"events" are functions assigned to numbers (in order as they are added) and called when the key with the number is pressed
	events: [],
	add(fce){
		if(SETTING.keyboard) this.events.push(fce);
	}, 
	reset(){
		this.events.length = 0;
	},
	get index(){
		return SETTING.keyboard ? `${this.events.length}. ` : ``;
	},
	g(fce){
		this.add(fce);
		return this.index;
	},
	
	
//INITIATE
	initiate(){
		
		const banned = ["AltLeft", "AltRight", "ControlLeft", "ControlRight"];
		
		document.addEventListener('keyup', function keyboard(event) {
			//ctr and alt disables the game //TODO
			if( banned.includes(event.code) ){ 
				KEYBOARD.unpause();
			}
		});
			
		document.addEventListener('keydown', function keyboard(event) {
			//keyboard disabled
			if(!SETTING.keyboard) return;
			
			//temporaly disabled
			if(KEYBOARD.paused) return;
		
			//ctr, alt - disable keyboard
			if( banned.includes(event.code) ){  //TODO
				return KEYBOARD.pause();
			}
			

		//ESCAPE - back to menu/game/somewhere
			if(event.code == "Escape" || event.code === KEYBOARD.back_keycode || event.key === KEYBOARD.back_keycode  || event.code === KEYBOARD.back_keycode_alt || event.key === KEYBOARD.back_keycode_alt  ){ 
				if(KEYBOARD.back_fce) return KEYBOARD.back_fce();
			}				
			
			
		//DYNAMICALLY ATTACHED to specific letters 
			const atta = KEYBOARD.attached.find( a => {
				return (a.keycode === event.key) || (a.keycode === event.code);
			});
			if(atta) atta.fce();
			
			
		//NUMBERED EVENTS 
			for(let i = 0; i < 10; i++) { 
				if(event.code === `Numpad${i+1}` || event.code === `Digit${i+1}`){
					return trigger_event(i);
				}	
			}
			
			function trigger_event(index){
				if(KEYBOARD.events[index] !== undefined){ 
					KEYBOARD.events[index]();
				}
			}

		});
		
		console.log(`>Keyboard initiated. `); //LOPRO
	},
};

KEYBOARD.initiate(); //IMPORTANT!


