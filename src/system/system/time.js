export const time = {
	get day(){
		return ["fri", "sat", "sun", "mon", "tue", "wed", "thu"][head.day % 7];
	},
	get day_name(){
		/*
			0 - Friday 
			1 - Saturday
		*/
		return ["Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday"][head.day % 7];
	},
	get week(){
		if(head.day <= 2) return 0;
		if(head.day <= 9) return 1;
		if(head.day <= 16) return 2;
		if(head.day <= 23) return 3;
		if(head.day <= 30) return 4;
		if(head.day <= 37) return 5;
		if(head.day <= 44) return 6;
		return -1;
	},
	get is_weekend(){
		const day = this.day;
		return day === "sun" || day === "sat";
	},
	get is_weekday(){
		return !this.is_weekend;
	},
	get is_workday(){
		return !this.is_weekend;
	},
	get weekend_follows(){
		const day = this.day;
		return day === "fri" || day === "sat";
	},
	
	
	get late_week(){
		switch(this.day){
			case "thu": return 1; //fri
			case "wed": return 0.8; //thu
			case "tue": return 0.4; //wen
			default: return 0;
		}
	},
	
	get mid_week(){
		switch(this.day){
			case "thu": return 1; //fri
			case "wed": return 1; //thu
			case "tue": return 0.8; //wed
			case "mon": return 0.4; //tue
			default: return 0;
		}
	},
}