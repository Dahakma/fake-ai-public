import source_success from 'Assets/audio/success.wav';
import source_death from 'Assets/audio/death.wav';
import source_click from 'Assets/audio/button.wav';
import source_denied from 'Assets/audio/denied.mp3';

function sound(src) {
	this.sound = document.createElement("audio");
	this.sound.src = src;
	this.sound.setAttribute("preload", "auto");
	this.sound.setAttribute("controls", "none");
	this.sound.style.display = "none";
	document.body.appendChild(this.sound);
	this.play = function(){
		this.sound.play();
	}
	this.stop = function(){
		this.sound.pause();
	}
}

export const success = new sound(source_success);
export const bell = new sound(source_death);
export const click = new sound(source_click);
export const denied = new sound(source_denied);


/*

const click = new sound(src_click); //by JarredGibb
const denied = new sound(src_denied); //by original_sound
const success = new sound(src_success);//https://freesound.org/people/thisusernameis/sounds/426889/
*/