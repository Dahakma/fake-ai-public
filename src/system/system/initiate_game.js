/**
	initiates the default variables when the new game is started or the game is loaded
*/

import {rg} from "Libraries/random";
import {crew, resetAvatarsCache, createPC, updateDraw} from "Avatar/index";
import {POTIONS} from "Avatar/game/potions";
import {main} from "Loop/engine";
import {default_mile} from "Data/milestones";
import {NPC, default_ext, default_head, stored_mile} from "./variables";
import {clone} from "Libraries/dh";
import {apply_npcs} from "System/load";
import {DISPLAY} from "Gui/gui";
import {ATTRIBUTES} from "Virtual/attributes";
import {INVENTORY, createMultiple} from "Avatar/index";
import {money} from "Data/items/prices";
import {NAME} from "System/variables"; //TODO
//import {merchants} from "Data/items/merchants/index";
import {lists} from "Data/items/lists/index"
export let GAMESEED;

//stars the new game (when the new game is started)
export function start_game(){
	initiate_game();
	
	//draws avatar 
	updateDraw(PC);
	
	//the first scene of the game
	main(100, "intro");
}

//runs during third scene when the avatar dimensions & character background are finalized
export function initiate_secondary_stuff(){
	//ext.meta = {}; //TODO - will removing this break anything?
	//ext.game = {};
	
	ext.irl_money = money.irl_initial;
	ext.game_money = money.game_initial;
	
	
	//TODO - THIS IS STUPID TEMPORAL SOLUTION; I'M TO TIRED TO BOTHER WITH THIS AT TOMENT
	const items = [];
	const list = [...lists.initial, ...lists[["","partygirl","athlete","president"][ext.background]]];
	list.forEach( what  => {
			if( Array.isArray(what) && !isNaN(what[0]) ){
				for(let i = 0; i < what[0]; i++){
					items.push(what[1]); 
				}
			}else{
				items.push(what);
			}
	});
	
	//console.warn(items);
	//console.warn( createMultiple(items) );
	PC.inventory = createMultiple(items); //TODO - FIND WHY RECREATE DOESNT WORK

/*	
	INVENTORY.recreate( //default clothes in wardrobe
		createMultiple(items),
);
*/	
	
	//console.warn(PC.inventory);

	
}

//initiate game (when the new game is started or when is a save loaded)
export function initiate_game(seed){
	//seed
	if(seed === undefined) seed = rg.date;
	GAMESEED = seed;
	
	//PC
	initiate_pc(seed);
	
	//inventory - TODO
	PC.inventory = [];
	PC.gameInventory = [];
	
	//default variables: 
	mile = clone(default_mile);
	Object.keys(mile).forEach( key => stored_mile[key] = mile[key] ); //redundant but makes me feel better to clear this variable immediatelly and not just when running main() 
	ext = clone(default_ext);
	head = clone(default_head);
	counter = {};
	
	//npcs
	initiate_npcs(seed);
	
	//other
	POTIONS.initiate(seed); //colors of potions are random
	
	DISPLAY.initial(); 
}

//initiate PC avatar 
function initiate_pc(seed){
	PC = createPC(seed); //creates PC's DAD avatar 
	ATTRIBUTES.initiate(PC); //attributes //TODO - very clumsy!!!
}

//initiate only seeds for NPC characters, lazy creation only when needed
function initiate_npcs(seed){
	rg.i(seed);
		
	resetAvatarsCache(); //remove loaded cached avatars
	apply_npcs(); //drops all stored npc properties belonging to the particular gameplay (like changed dimensions); loads from the localStorage general properties (like name, etc)
	
	Object.keys(NPC).forEach( a => {
		NPC[a].seed = rg.seed; //seed for avatar generation 
	});	
	NPC.ayy.seed = PC.seed; //AI uses the same seed as the player //TODO - redundant
	NAME.ayyalt = NAME.katalt; //TODO CHECK IF WORKS
	
}