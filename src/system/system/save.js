import {clone, changed_properties, capitalise} from "Libraries/dh";
import {rg} from "Libraries/random";
import {autoload as lib_autoload} from "Libraries/save";
import {autosave as lib_autosave} from "Libraries/save";
import {menu as lib_menu} from "Libraries/save";

import {fellowship} from "Virtual/follower";
import {default_mile} from "Data/milestones";
import {NPC, version, gamekey, default_ext, default_PC, stored_mile} from "System/variables";
import {main, set, present} from "Loop/engine";
import {unpackGarment, wearEverything, cloneOriginalCharacter , updateApex, update, updateDraw, updateMods, onlyChangedMods, wornOutfit, saveWornOutfit, wearOutfit, unpackEverything, packEverything} from "Avatar/index";
import {initiate_game, GAMESEED} from "System/initiate_game";
//import {DISPLAY} from "System/gui";
import {effigy} from "Avatar/index";
import {save as save_follower} from "Virtual/follower";
//import {reenter} from "System/dungeon/engine";
import {reenter, clear} from "Dungeon/engine";
//import {BARS} from "Virtual/index"; 
import {preprocess} from "Loop/preprocess_text";
import {popup} from "Gui/gui";

import {storage_disabled} from './load';


export let savefile_loading_in_progress = false;

export function autosave(name){
	if(storage_disabled) return; //localStorage disabled 
	if(savefile_loading_in_progress) return;
	
	const prepare = save();
	if(name) prepare.name = name;
	if(SETTING.notifications_save) popup.note("💾 saved");
	lib_autosave(gamekey, prepare);
}

export function save_menu(container, back){
	lib_menu(
		gamekey,
		container, 
		save, //prepare_save
		load, //finish_load
		{
			name: `Fake AI saves`,
			note: `Save file of Fake AI ${version} by Dahakma`, //export_note,
		},{
			container: "saves_menu", //style_container
			click: "saves_link", //style_link
			
			text: "saves_text", //style_text
		},
		back,
	);
}
	

export function autoload(alternative){
	try{
		const savegame = lib_autoload(gamekey);
		//console.log(savegame);
		if(!savegame) return alternative();
		load(savegame);
		
	//error; localStorege not available or the game is broken
	}catch(err){
		/* eslint-disable no-console */
			console.log(err);
		/* eslint-enable no-console */
		
		if(alternative) alternative();
	}
}


function load(savegame){
	savefile_loading_in_progress = true;
	
	//LOG
	/* eslint-disable no-console */
		console.log(">Load:");
		console.log(savegame);
	/* eslint-enable no-console */
	
	
	//initiate everything
	initiate_game(savegame.seed);
	
	//update MILE
	Object.keys(savegame.mile).forEach( a => {
		const saved = savegame.mile[a];
		if(saved === null || saved === undefined) return; //TODO - coppied from updated ext
		stored_mile[a] = saved;
		mile[a] = saved;
	});
	
	
//TODO CLODE LEGACY FIX (has to be before the update EXT)
if(savegame.ext.game_money === undefined){
	savegame.ext.game_money = savegame.ext.money || 100; //TODO - RANDOM VALUES
	savegame.ext.irl_money = 300; //TODO!! RANDOM VALUSE
}

	
	//updated EXT 
	Object.keys(savegame.ext).forEach( a => {
		const saved = savegame.ext[a];
		if(saved === null || saved === undefined) return; //TODO - null breaks stuff; I'm not sure why should be undefined saved but it is just feeling
		if(typeof saved === "object"){
			Object.keys(saved).forEach( b => {
				//THIS CANNOT FUKCING WORK
				if(!ext[a]) ext[a] = [];
				ext[a][b] = saved[b];
			});
		}else{
			ext[a] = saved;
		}
	});
	
	//EXT.user - applies manutaly-edited avatar dimensions
	for(const prop of Object.keys(ext.user) ) {
		PC.Mods[prop] += ext.user[prop];
	} 
	
	//inventory 
	PC.inventory = unpackEverything(savegame.inventory);
	PC.gameInventory = unpackEverything(savegame.gameInventory);
	
	//head & counter
	head = clone(savegame.head);
	counter = clone(savegame.counter);
	
	
//TODO SNAPSHOTS TEPMFIX
if(!Array.isArray(ext.snapshots)) ext.snapshots = [];

//TODO LEGACY TEMPFIX	
if(head.tommorow){
	head.tomorrow = clone(head.tommorow);
	if(head.tomorrow.morningOld) head.tomorrow.morningReaction = head.tomorrow.morningOld;
	if(head.today.morningOld) head.today.morningReaction = head.today.morningOld;
	delete head.tommorow;
}
if(!head.workday) head.workday = {};
if(mile.a_hate > 0) mile.a_burned = true; //DELETE
if(mile.a_hate < 0 && (mile.a_another_apology || mile.a_meeting_relationship) ) mile.a_dating = true; //DELETE

//TODO LEGACY TEMPFIX
if( Array.isArray(ext.potions) ) ext.potions = {};
if( Array.isArray(ext.stats.potions) ) ext.stats.potions = {};

//LEGACY TEMPFIX 
	if(!ext.timer) ext.timer = []; 
	ext.timers = ext.timers.map( a => {
		if( Array.isArray(a) ){ //in version with array timers it were only rules
			return {
				countdown: a[0],
				//obj: a[1],
				rule: a[2],
			}
		}else{
			return a;
		}
	});


	
	const set_resets_head_present = clone(head.present); //set.whatever will clear present charaters
	
	
	if(head.reality === 3){
		//DISPLAY.game();
		set.game();
	}else if(head.reality === 2){
		set.meta();
	}else{
		set.irl();
	}
	
	PC.perks = [...savegame.perks],
	PC.skills = [...savegame.skills], 
	PC.effects = clone(savegame.effects ?? []), //TODO!!
	
	updateProperties(PC.Mods, savegame.mods); //TODO - ORDER 
	updateProperties(PC.apex, savegame.apex);
	updateProperties(PC.base, savegame.base);
	
	npcs_load_changed(savegame.npc);
	
	wearOutfit("save", PC, undefined, true);
	updateDraw(PC);
	
	
	//TEMPFIX - HAS TO BE AFTER ALL IS ADDED BECAUSE ATTRIBUTES!!!
	if(head.reality === 3){
		//DISPLAY.game();
		set.game();
	}else if(head.reality === 2){
		set.meta();
	}else{
		set.irl();
	}
	
	if(set_resets_head_present) head.present = set_resets_head_present; //set. will overrrides the head 
	

	if(ext.follower){
		fellowship(ext.follower); //TODO TEMPFIX MIGHT BREAK THE FOLLOWER 
	}	



	if(ext.dungeon){
		reenter();
	}else{
		main(head.scene, head.chapter);
	}

	savefile_loading_in_progress = false;
}







export function updateProperties(output, input){
	Object.keys(input).forEach( a => {
		output[a] = input[a];
	})
}

/*
function apply_ch(original, changes = {}){
	if(original === undefined || typeof original !== "object") return;
	Object.keys(changes).forEach( a => {
		original[a] = changes[a]
	})
}
*/

export function load_snapshot(key){
	if(storage_disabled) return; //localStorage disabled 
	const snapshots = JSON.parse(  localStorage.getItem(`${gamekey}_snapshots`)  );
	//ext.snapshot[index]
	const snap = snapshots[key];
	if(!snap){
		/* eslint-disable no-alert, no-console */
		console.warn(snapshots);
		return alert(`ERROR: snapshot "${key}" not found!`);
		/* eslint-enable no-alert, no-console */
	}
	
	const temp = cloneOriginalCharacter(PC); //TODO - NOT PC!!
	temp.Mods = {
		...temp.Mods,
		...snap.mods,
	}
	temp.apex = {
		...temp.apex,
		...snap.apex,
	}
	/*
	const items = snap.cloth.map( a => unpackGarment(a) );
	wearEverything(items, temp);
	*/
	wearEverything( unpackEverything(snap.cloth) , temp);

	const reality = head.reality; //reality affect which apex is attached
		
		if(snap.meta) head.reality = 2;
		if(snap.game) head.reality = 3;
		update(temp);
		
	head.reality = reality;

	return temp;
} 

export function snapshot(id, desc, subject = PC){
	if(storage_disabled) return; //localStorage disabled 
	remove_orphaned_snapshots();
	
//TODO CHECK IF LOCAL STORAGE AVAILABLE
	const snapshots = JSON.parse(  localStorage.getItem(`${gamekey}_snapshots`) ?? "{}"  );

	const mods = onlyChangedMods(subject);
	const apex = changed_properties(default_PC.apex, subject.apex); //using "default_PC.apex" should mostly work even with NPC's
	const cloth = wornOutfit(subject);
	const varname = new Date().getTime();
	
	
//console.warn(snapshots)

	const object = {
		id,
		mods,
		apex,
		cloth,
	};
	
	if(desc) object.desc = desc;
	if(head.reality === 3) object.game = true;
	if(head.reality === 2) object.meta = true;	
	if(subject.key !== "kat") object.key = subject.key;
//TODO - IS SUBJECT KEY KAT??	
	
	snapshots[varname] = object;
	
	localStorage.setItem(`${gamekey}_snapshots`, JSON.stringify(snapshots) );
	ext.snapshots.push(varname);
	
	
}

//TODO TODO TODO - VERY IMPORTANT SHIT
export function remove_orphaned_snapshots(){
	if(storage_disabled) return; //localStorage disabled 
	const snapshots = JSON.parse(  localStorage.getItem(`${gamekey}_snapshots`) ) ?? {};
	const list = Object.keys(snapshots); //id's of all snapshots
	const savegames = JSON.parse( localStorage.getItem(`${gamekey}_save_list`) ) ?? []; //list of all savegames
	
	let used = []; //snapshots used by savegames
	for(let i = savegames.length - 1; i > 0; i--){
		const save = JSON.parse(localStorage.getItem(`${gamekey}_save_${i}`)); 
		if(save !== null) used.push(...save.ext.snapshots);
	}
	used.push(...ext.snapshots); //also those not yet saved
	used = [...(new Set(used))]; //unique
		
		
	used.forEach( id => { //when is the snapshot among snapshots used by saves the snapshot is removed from the list of orphaned snapshots
		const index = list.indexOf(`${id}`);
		if(index !== -1) list.splice(index, 1);
	})
	
	list.forEach( key => delete snapshots[key] );
	
	localStorage.setItem(`${gamekey}_snapshots`, JSON.stringify(snapshots) );
}






function save(){
	
	saveWornOutfit("save", "save", PC, 0);
	
	const savegame = {
		seed: GAMESEED,
		
		mile: {},
		ext: {},
		
		perks: [...PC.perks],
		skills: [...PC.skills],
		effects: clone(PC.effects),
		
		counter: clone(counter),
		head: clone(head),
		
		inventory: packEverything(PC.inventory),
		gameInventory: packEverything(PC.gameInventory),
		
		mods: onlyChangedMods(PC),
		apex: changed_properties(default_PC.apex, PC.apex),
		base: changed_properties(default_PC.base, PC.base),
		
		npc: npcs_save_changed(),
	};
	
	//MILE
	Object.keys(stored_mile).forEach( a => {
		if(stored_mile[a] !== default_mile[a]) savegame.mile[a] = stored_mile[a];
	});
	
	//EXT
	Object.keys(ext).forEach( a => {

		if(typeof ext[a] === "object"){
			savegame.ext[a] = Array.isArray(default_ext[a]) ? [] : {};
			Object.keys(ext[a]).forEach( b => {
//TODO THIS FUCKING CANNOT WORK!!!
				if(ext[a][b]!== default_ext[a]?.[b]) savegame.ext[a][b] = ext[a][b];
			});
		}else{
			if(ext[a]!== default_ext[a]) savegame.ext[a] = ext[a];
		}
	});
	
	if(savegame.ext.follower){ //TODO - TEMPFIX, MIGHT BREAK FOLLOWER
		savegame.ext.follower = ext.follower.key;
		//return; 
	}

	//LOG
	/* eslint-disable no-console */
		console.log(">Save:");
		console.log(savegame);
	/* eslint-enable no-console */
	

	//NAME OF SAVEFILE
	let name = `Day ${head.day}`;
	if(head.reality === 3 && ext.dungeon){
		name += `; ${ext.dungeon.name}`;
	}else{
		if(head.placetime.place) name += `; ${capitalise(head.placetime.place)}`;
		if(head.placetime.time) name += `; ${capitalise(head.placetime.time)}`;
	}
	name = preprocess(name);



	return {
		name,
		savegame,
	};
}








const npcs_saved_properties = [
	"user", //user edited avatar dimensions
	"irl", //avatar dimensions irl (different from defalut) 
	"meta", //... in the metaverse
	"game", //... in the game
	"apex", //stuff attached to avatar different from default (makeup, horns)
	"perks", //vr game followers perks
	"perks", //vr game followers skills
];
	
 
function npcs_save_changed(){
	save_follower();
	
	const keys = Object.keys(NPC);
	const save = {};
		
	
	rg.i(GAMESEED); 
	keys.forEach( key => {
		save[key] = {};
		
		const seed = rg.seed; //saves seeds only when they are different from default 
		if(seed !== NPC[key].seed) save[key].seed = NPC[key].seed;
		
		npcs_saved_properties.forEach( prop => {
			if(NPC[key][prop]) save[key][prop] = NPC[key][prop];
		});
		
	})
	
	//don't bother save empty objects 
	keys.forEach( a => {
		if(!Object.keys(save[a]).length) delete save[a];
	});
	
	return save;
}



function npcs_load_changed(save){
	Object.keys(save).forEach( key => {
		if(save[key].seed) NPC[key].seed = save[key].seed;
		
		npcs_saved_properties.forEach( prop => {
			if(save[key][prop]) NPC[key][prop] = save[key][prop];
		});
	})
}

 