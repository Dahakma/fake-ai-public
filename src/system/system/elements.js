//import {div_subtext} from "System/gui_skeleton";
let div_subtext; //TODO !!!!!!!!!!!!!!!!!!

import {KEYBOARD} from "./keyboard";
import {var_name, remove_children} from "Libraries/dh";
import {toHEX} from "Libraries/color/index";

export let div_container;
export let div_last;

export function write(input){
	div_container.innerHTML = input;
}

export function clear(){
	remove_children(div_container);
}

export function link_esc(input, fce, keyboard){
	escape(fce);
	link(input, fce, keyboard);
}


export function escape(fce){
	KEYBOARD.back(fce);
}
export function container(style = "", parent = div_subtext){
	KEYBOARD.reset();
	remove_children(parent);
	//EL.container(parent, style);
	
	div_container = document.createElement("DIV");
	div_container.id = "div_container";
	div_container.className = style;
	parent.append(div_container);	
	
}


export function button(name, fce, top = false){
	const temp = document.createElement("DIV");
	temp.innerHTML = `${KEYBOARD.index}${name}`;
	temp.className = "link menu";
	//temp.className = "link";
	div_container.append(temp);
	temp.addEventListener("click",function(){
		fce();
	}, false);
	
	if(top){
		const temp = document.createElement("DIV");
		temp.innerHTML = `${KEYBOARD.index}${name}`;
		temp.className = "link menu";
		div_container.prepend(temp);
		temp.addEventListener("click",function(){
			fce();
		}, false);		
	}
	
	KEYBOARD.add(fce);
	
}



//creates clicable link
export function link(name, fce, keyboard){
	const box = document.createElement("DIV");
	box.classList.add("grid_breaker");
	div_container.appendChild(box);
	
	const temp = document.createElement("DIV");
	if(keyboard) name = `${KEYBOARD.index}${name}`;
	temp.innerHTML = name;
	temp.classList.add("link");
	box.appendChild(temp);
	temp.addEventListener("click", fce, false);
	if(keyboard) KEYBOARD.add(fce);
	div_last = temp;
}


//create title/headline
export function title(name){
	const temp = document.createElement("DIV");
	temp.innerHTML = name;
	temp.classList.add("title");
	div_container.appendChild(temp);
	div_last = temp;
}


export function subtitle(name){
	const temp = document.createElement("DIV");
	temp.innerHTML = name;
	temp.classList.add("subtitle");
	div_container.appendChild(temp);
	div_last = temp;
	return temp;
}


export function color(name, value, fce, label = true){
	//field
	const field = document.createElement("INPUT");
	field.setAttribute("type", "color"); 
	field.id = var_name(name);
	
	field.value = toHEX(value);
	field.addEventListener("input", function(){
		fce(this.value, this);
	}, false);
	div_container.appendChild(field);
	
	//label
	if(label){
		const desc = document.createElement("label");
		desc.htmlFor = field.id;
		desc.innerHTML = `${name}`;
		desc.classList.add("slider_desc");
		div_container.appendChild(desc);
	}
}


 


//creates option with slider
export function slider(name, value, min, max, fce, step){
	const divy = document.createElement("DIV");
	divy.innerHTML = `${name}: `;
	divy.classList.add("slider_desc");
	div_container.appendChild(divy);
	
	const temp = document.createElement("INPUT");
	temp.setAttribute("type", "range"); 
	temp.classList.add("slidy");
	temp.style.width = "200px";
	temp.max = max;
	temp.min = min;
	temp.step = step || 1;
	temp.value = value;
	
	temp.addEventListener("input", function(){
		fce(+this.value, this);
	}, false);
	
	div_container.appendChild(temp);
}



//creates a checkbox option
export function checkbox(name, value, fce){
	const field = document.createElement("INPUT");
	field.setAttribute("type", "checkbox"); 
	field.classList.add("checkbox");
	field.id = var_name(name);
	field.checked = value;
	field.style.margin = "auto";
	
	field.addEventListener("input",function(){
		fce(this.checked, this);
	}, false);
	
	div_container.appendChild(field);
	
	const label = document.createElement("label");
	label.htmlFor = field.id;
	label.innerHTML = `${name}`;
	label.classList.add("slider_desc");
	div_container.appendChild(label);
}


export function divy(text, style = "slider_desc", id){
	/*
	const temp = EL.div(text, style);
	if(id) temp.id = id;
	return temp;
	*/
	
	const temp = document.createElement("DIV");
	temp.innerHTML = text;
	if(style){
		temp.classList.add(style); //TODO
	}else{
		temp.classList.add("slider_desc"); //TODO
	}
	if(id) temp.id = id;
	div_container.appendChild(temp);
	
	div_last = temp;
	return temp;
	
}



export function sound(src) {
	
	const temp = document.createElement("audio");
	temp.src = src;
	temp.setAttribute("preload", "auto");
	temp.setAttribute("controls", "none");
	temp.style.display = "none";
	//document.body.appendChild(temp.sound);
	div_container.appendChild(temp);
	/*
	temp.play = function(){
		console.log(temp);
		console.log(this);
		this.play();
	}
	temp.stop = function(){
		this.pause();
	}
	*/
	return temp;
}




/*
export function input(){
	const field = document.createElement("INPUT");
	field.setAttribute("type", "checkbox"); 
	field.classList.add("checkbox");
	field.id = var_name(name)
	field.checked = value;
	field.style.margin = "auto";
	
	field.addEventListener("input",function(){
		fce(this.checked, this);
	}, false);
	
	container.appendChild(field);
	
	const label = document.createElement("label");
	label.htmlFor = field.id;
	label.innerHTML = `${name}`;
	label.classList.add("slider_desc");
	container.appendChild(label);
}
*/

