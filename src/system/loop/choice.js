/*
	create choices in the middle of the text
*/
//messy but works, don't touch again!!

import {preprocess} from "Loop/preprocess_text";
import {animate} from "Avatar/index";
import {DISPLAY} from "Gui/gui";
import {ra} from "Libraries/random";

//CHOICES && EFFECTS
const raw_effects_list = []; //iputed stuff before effects are create
const effects_list = []; //all created effect (used for random choice)

let required_min = undefined; //minimal number of choices neccessary to continue
let required_max = undefined; //maximal number of choices 

export function choices_required(min = undefined, max = undefined){
	required_min = min;
	required_max = max;
}


//add choice (in story)
export function choice(id, label){
	if(label === undefined) label = String(id);
	return `<div class = "choice" id= "choice_${id}">${preprocess(label)}</div>`;
}

//randomly select one of each choices
function select_random(){
	if(required_min !== undefined) return select_required();
	effects_list.forEach( a => {
		const clickEvent = new Event('click');
		const available =  a.filter(a => !a.active);
		if(!available.length) return; //i.e. there's only one and already clicked
		ra.array(available).obj.dispatchEvent(clickEvent);	
	})
}

//randomly select required number of choices
function select_required(){
	//basicall the same as  "select_random()" but not all choices, only required number 
	
	effects_list.forEach( a => {
		a.forEach( b => {
			if(b.active){
				const clickEvent = new Event('click');
				b.obj.dispatchEvent(clickEvent);
			}
		})
	});
	
	const effs = ra.shuffle( effects_list.map( (a,i) => i ) )
	const required = required_max ? ra.integer(required_min, required_max) : required_min; //minimal number of required OR random number between min and max required
	
	for(let i = 0; i < required && i < effs.length; i++){ //TODO - better would be cut the array
		//const a = effects_list[effs[i]];
		const clickEvent = new Event('click');
		//const available =  a.filter(a => !a.active);
		//if(!available.length) return; //i.e. there's only one and already clicked
		ra.array( effects_list[effs[i]] ).obj.dispatchEvent(clickEvent);
	}
}



//create links for the random selection of choices
export function link_effects(a){
	
	//there are no effects 
	if(!raw_effects_list.length){
		return {link: undefined, nolink: undefined};	
	}
	
	//there is a choice (one of effect consist of multiple choices OR you do not have to select all effects)
	const randomisable = raw_effects_list.some( a => a.filter( b => b.id !== "initial").length > 1 ) || required_min !== undefined; 
	
	
	let link = undefined; //link - present when are other links displayed
	if(randomisable){
		link = {
			text: `Random. `,
			fce: select_random,
		};
		
		/*
		link = {
			text: `Clear. `,
			fce: ()=>{
				//TODO - WACKY
				//active_choices are unclicked (it is not remembered anywhere whether they are clicked, so otherwise when you rerender the whole page again, the changed values would stay changed but the choices would be unclicked)
				Array.from(document.getElementsByClassName("choice_active")).forEach( div => {
					const clickEvent = new Event('click');
					div.dispatchEvent(clickEvent);	
				})
			},
		};
		*/
	}
	
	//nolink - present when are other links hidden
	const nolink = {
		text: randomisable ? `Random. `  : `Click on everything. `,
		fce: select_random,
	};
	
	return {link, nolink};
}



//add effects (in story)
export function effects(array){
	raw_effects_list.push(array);
}

//clear existing effects (in loop/engine.js)
export function clear_effects(){
	effects_list.length = 0;
	raw_effects_list.length = 0;
	required_min = undefined;
	required_max = undefined;
}

//create effects and connects them with choices (in loop/engine.js)
export function create_effects(){
	effects_list.length = 0;
	raw_effects_list.forEach( a => actual_effects(a) );
}

function actual_effects(effects){
	const array = [...effects];

	//sets initial state which happens when nothing is picked (id "initial")
	const initial = (()=>{ 
		if(array[0].id === "initial"){
			const fce = array[0].fce;
			array.shift();
			return fce;
		}else{
			return ()=>{};
		}
	})();
	
	//adds all choices (if their object exists, (changes innerHTML of object if required) )
	const choices = array.map( (a,i) => {
		a.id ??= i;
		a.fce ??= ()=>{};
		a.subject ??= PC;

		if(a.absolute){
			if(!a.relative) a.relative = {};
			Object.keys(a.absolute).forEach( key => {
				a.relative[key] = a.absolute[key] - ( (a.subject.basedim?.[key] ?? 0) + (a.subject.Mods?.[key] ?? 0) );
			});
		}

		return {
			label: a.label,
			id: a.id,
			obj: document.getElementById(`choice_${a.id}`),
			active: false,
			dead: false,
			fce: a.fce,  //??
			relative: a.relative,
			subject: a.subject,
		};
	}).filter( a => a.obj );
	
	if( !choices.length ) return; //no valid choice => the whole thing is scrapped;
	
	choices.filter( b => b.label ).forEach( a =>  a.obj.innerHTML = preprocess(a.label) ); //change text of choices
	choices.done = false; //one of the choices is picked 
	
	//body transformations 
		function reverse_primitives(object){
			const output = {};
			Object.keys(object).forEach( a => {
				if(typeof object[a] === "boolean"){
					output[a] = !object[a];
				}else if(typeof object[a] === "number"){
					output[a] = -object[a];
				}
			})
			return output;
		}
		
		
		function check_upgraded(a){ //upgades are usually done onmouseenter and onmouseleave; this check whether they were triggered and triggers them if werent (using choice_random or mouseless devices)
			if(!a.upgraded && a.upgrade) a.upgrade();
		}
		
		function check_downgraded(b){ //TODO TEMPFIX
			if(b.upgraded &&  b.downgrade) b.downgrade();
		}
		
		
	choices.forEach( a => {
		const {relative, obj, subject} = {...a};
		if(!relative || !Object.entries(relative).length) return;
		let time = 400;
		if(relative.time){
			time = relative.time;
			delete relative.time;
		}
		
		a.upgrade = function(){
			if(choices.done) return;
			a.upgraded = true;
			animate(subject, {Mods: relative}, time);
		}
		
		a.downgrade = function(){
			if(!a.upgraded) return;
			if(choices.done) return;
			a.upgraded = false;
			animate(subject, {Mods: reverse_primitives(relative)}, time);
		}
		
		obj.addEventListener("mouseenter", a.upgrade); 
		obj.addEventListener("mouseleave", a.downgrade); 		
	});
	
	
	//what happens on activation (click)
	choices.forEach( a => a.obj.addEventListener("click", trigger) );
	
	
	function activate(a){
		initial(); //TODO - I BELIEVE IT SHOULD BE HERE BUT IS NOT WELL-THOUGHT NOR PROPERLY TESTED TODO ASAP 

		choices.forEach( b => {
			if(b === a) return;
			b.dead = true;
			b.obj.classList.add("choice_dead");
		});
		
		check_upgraded(a);
		
		choices.done = true;
		a.active = true;
		a.obj.classList.add("choice_active");
				
		a.fce();
	}
	
	function deactivate(a){
		choices.forEach( b => {
			b.dead = false;
			b.obj.classList.remove("choice_dead");
		})
					
		choices.done = false;
		a.active = false;
		a.obj.classList.remove("choice_active");
				
		check_downgraded(a);
		
		initial();
		//TODO BODY??
	}
	
	function trigger(){
		for(const a of choices){
			if(a.obj === this){
				if(a.exhausted && !a.dead){
					//disabled because over required_max
				}else if(a.dead){
					const b = choices.find( b => b.active );
					deactivate(b);
					activate(a);
				}else if(a.active){ 
					deactivate(a);
				}else{
					activate(a)
				}
				check_links();
				break;
			}
		}
	}
	
	choices.required_exhausted = function(exhausted){ //disable choices because over required_max
		this.forEach( a => {
			if(exhausted){
				if(a.active) return;
				a.exhausted = true;
				a.obj.classList.add("choice_dead");
			}else{
				a.exhausted = false;
				if(!a.dead) a.obj.classList.remove("choice_dead");
			}
		})
	};
	
	effects_list.push(choices);	
	check_links();
}



function check_links(){
	let are_done = 0;
	let not_done = 0;
	for(const a of effects_list){
		if(!a.done){
			not_done++;
		}else{
			are_done++;
		}
	}
	
	if(required_min === undefined){
		//basic situation, all has to be checked 
		if(not_done > 0){
			return DISPLAY.display_links(false);
		}else{
			return DISPLAY.display_links(true);
		}
	}else{
		if(required_max !== undefined){
			if(are_done === required_max){ //reached the maximal required number 
				effects_list.forEach( a => a.required_exhausted(true) );
				return DISPLAY.display_links(true);
			}else{ //the maximual number NOT reached 
				effects_list.forEach( a => a.required_exhausted(false) );
			}
		}
		
		//failsafe - not enough choices for minimal required number (follow all has to be checked rules) 
		if( (are_done+not_done) < required_min){
			if(not_done > 0) return DISPLAY.display_links(false);
		//minimal required number has to be checked 
		}else if(are_done < required_min){
			return DISPLAY.display_links(false);
		}
		
		return DISPLAY.display_links(true);
	}
}