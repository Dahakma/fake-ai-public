import {NAME} from "System/variables";
import {mention_npc} from "Loop/engine";
import {wdesc, bdesc} from "Avatar/wardrobe/appearance";
import names from "Libraries/names";
import {ra} from "Libraries/random";

import {aly, emy, reac, and, s, capitalise, niceness, last, chat} from "Loop/text";

import {preprocess_text} from "Libraries/preprocess_text/index";


export function preprocess(input){
	return preprocess_text(input, {
		fix: {
			typos: true,
		},
		markdown: {
			block: false,
		},
		replace: {
			pattern: "@(\\w+)",
			fce: smail,
		},
	})
}


function smail(key){
	
//FOLLOWER
	if(ext.follower){
		
		if(key === "fol"){
			mention_npc.push(ext.follower.key);
			return `<span class = "name_${ext.follower.key}">${NAME[ext.follower.key]}</span>`;
		}else if(key === "FOL"){
			mention_npc.push(ext.follower.key);
			return `<span class = "name_${ext.follower.key}">${NAME[ext.follower.key].toUpperCase()}</span>`;
		}
		
		switch(key){
			case "we":
				return  "we";
			case "We":
				return  "We";
			case "he":
				return  ext.follower.sex ? "she" : "he";
			case "He":
				return  ext.follower.sex ? "She" : "He";
			case "his":
				return  ext.follower.sex ? "her" : "his";
			case "His":
				return  ext.follower.sex ? "Her" : "His";
			case "him":
				return  ext.follower.sex ? "her" : "him";
			case "Him":
				return  ext.follower.sex ? "Her" : "Him";
			default:
				//nothing
		}
		
	}else{	
		switch(key){
			case "we":
			case "We":
				return  "I";
			default:
				//nothing
		}
	}
		
	
	if(key === "fol" && ext.follower){
		mention_npc.push(ext.follower.key);
		return `<span class = "name_${ext.follower.key}">${NAME[ext.follower.key]}</span>`;
	}
	
	
//HEAD.PRESENT OVERRIDE
	if(head?.present?.[key]?.name){
		mention_npc.push(key);
		return `<span class = "name_${key}">${head.present[key]?.name}</span>`;
	}
	
//LAST NERD WHO SPOKE 
	if(key === "last"){
		key = last;
	}
	
//NAMES
	if(NAME[key]){ //@kat = kat = Kate
		mention_npc.push(key);
		return `<span class = "name_${key}">${NAME[key]}</span>`;
	}
	
	if(NAME[key.slice(0,3)] && key.slice(3) === "s"){ //@kats = kats = Kates
		mention_npc.push(key.slice(0,3));
		return `<span class = "name_${key}">${NAME[key.slice(0,3)]}s</span>`;
	}

	if(NAME[key.toLowerCase()]){ //@KAT = KAT = KATE
		mention_npc.push(key.toLowerCase());
		return `<span class = "name_${key}">${NAME[key.toLowerCase()].toUpperCase()}</span>`;
	}
	
	
//WDESC
	if(wdesc[key]) return wdesc[key];
	if(bdesc[key]) return bdesc[key];

//RANDOM NAME
	if(key === `randomName`) return ra.g < 0.5 ? ra.array( names[`female_${SETTING.local}`] ) : ra.array( names[`male_${SETTING.local}`] );
	if(key === `randomFemaleName`) return ra.array( names[`female_${SETTING.local}`] );
	if(key === `randomMaleName`) return ra.array( names[`male_${SETTING.local}`] );
	if(key === `randomSurname`) return ra.array( names[`surname_${SETTING.local}`] );
	
	
		
//SPECIAL
	if(key === "mis") return mile.mistress;
	
	//TODO
	if(key === "afternoon") return "good afternoon";
	if(key === "Afternoon") return "Good afternoon";
	


}

