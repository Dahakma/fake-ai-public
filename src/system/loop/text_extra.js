import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";
import {emo, rollUpSkirt, wdesc, quickSaveOutfit, quickLoadOutfit, crew, updateDraw, violates, draw, effigy, sluttiness, makeup, wearRandomClothes, remove, wear, create, wears, showLess, showAll} from "Avatar/index";

import {money} from "Data/items/prices";
import {ra} from "Libraries/random";
import {active_timeslot, free_today, name_timeslot} from "Taskmaster/scheduler";

export function rumor_pussy(situation){
	if(wears.hairy){
		mile.rumor_beaver = mile.rumor_beaver > situation ? mile.rumor_beaver : situation;
	}else{
		mile.rumor_shaved = mile.rumor_shaved > situation ? mile.rumor_shaved : situation;
	}
	mile.rumor_commando++;
	if(mile.plugged) mile.rumor_plug++;
	if(mile.piercedGenitals) mile.rumor_clit++;
}


export function placetime(place, time){
	if(time === undefined){
		time = head.reality === 3 ? "" : capitalise( name_timeslot() );
	}
	if(!place) place = "";
	if(!time) time = "";
	head.placetime = {place, time};
	bold(`${capitalise(time)}${place && time ? "; " : ""}${capitalise(place)}.`);
}
	
export function task_reaction(place){
	if(active_timeslot() === "eveningReaction"){
		set.meta();
		placetime(place ?? "metaverse");
	}else if(active_timeslot() === "morningReaction"){
		set.irl();
		placetime(place ?? "locker room");
	}
	emo("anoy");
}
	
/*

import {placetime} from "Loop/text_extra";
task_reaction();
task_intro();
placetime("locker room", "before classes");
placetime("canteen");
placetime("my room", "late evening");
placetime("canteen");
*/

//HOW WAS THE GAME FINE?
export function task_intro(){
	let available = [];

	if(active_timeslot() === "eveningNew"){
		if( free_today("eveningReaction") ){
			set.meta();
			placetime("metaverse");
			available = [
				`The nerds sent me a message they wanted to talk with me so I connected to the Metaverse. `,
				`In the evening I met with the nerds in the Metaverse. `,
				`The nerds ordered me to met them in the Metaverse. `,
				
			]
		}else{
			//initiated before
			available = [
				`When we finished the debriefing, the nerds wanted to talk about a new task. `,
				`The nerds moved to the next point of our meeting - my next task. `,
				`Then the excited nerds presented me the new task. `,
				
			]
		}
	}else if(active_timeslot() === "morningNew"){
		if( free_today("morningReaction") ){
			set.irl();
			placetime("locker room");
		}else{
			//initiated before
		}
		
	}
	
	emo("anoy");
	if(!available.length) return;
	
	txt( ra.array(available) );
}



export function raise_skirt_panties(){ //TODO - ROLLUP 
	//showLess(-1, 1, 3);
	rollUpSkirt(2);
	let action  = `${wears.skirt ? "raised my " : "lowered my "} ${wdesc.lower}`;
	if(wears.pantyhose){
		action += `${wears.skirt ? "and pulled down my " : " and "} ${wdesc.socks}`;
	}
	return action;
}





import {NPC} from "System/variables";

/**
 * sex is added to ext.stats.
 * @param {string|Array} [act = "hand"] - sexual act or array of acts; "hand", "sex" ("sexs" for explicitly safe sex, (implicitly with woman or in vr)), "oral" (== "bj"), "anal", "self", "mm",  - mutual masturbation, "69" - mutual oral
 * @param {string} [receiver = "kat"] - key of NPC having sex with Kate (when Kate receive oral sex or is fucked anally; receiver is "kat")
 * @param {string}  [giver = "kat"] ; key partner; atm not used for anything, futureproofing
 */
export function log_sex(act = "hand", receiver = "kat",  giver = "kat"){
	//TODO - sucking strap-on counts as oral
	if(act === "hand" && receiver !== "kat") return; //TODO - handjobs not recorded 
	if(act === "69"){
			 log_sex("oral",  receiver,  giver);
			 log_sex("oral",  giver,  receiver);
			return;
	}
	if(act === "mm"){
			 log_sex("hand",  receiver,  giver);
			 log_sex("hand",  giver,  receiver);
			return;
	}
	if(act === "bj") act = "oral";
	
	const acts = Array.isArray(act) ? act : [act];
	const gender = (()=>{
		if(receiver.startsWith("npc")){
			const index = +("npc_1".split("_")[1]);
			return index >= 4 ? "f" : "m";
		}else{
			return NPC[receiver]?.sex === 1 ? "f" : "m";
		}
	})();
	
	for(act of acts){
		if(act === "self"){
			//same as log_sex("hand","kat")
			ext.stats.self++;
		}else if(act === "sex" || act === "sexs"){
			ext.stats[`sex_${gender}`][head.reality]++;
			if(act === "sex" && head.reality === 1 && gender === "m"){
				const identifier = (()=>{
					if(receiver.startsWith("npc")){
						const index = +("npc_1".split("_")[1]);
						return head?.present?.[index]?.name;
					}else if(NPC[receiver]){
						return receiver;
					}else{
						return undefined;
					}
				})();
		
				ext.stats.cum.push(identifier)
				ext.stats.cum = [...(new Set(ext.stats.cum))]; //unique items
			}
		}else if(receiver === "kat"){
			 if(act === "hand"){
					ext.stats.self++; 
			 }else if(act === "oral"){
				ext.stats[`oral_s`][head.reality]++;
			}else if(act === "anal"){
				ext.stats[`anal_s`][head.reality]++;
			}
		}else if(act === "oral"){
			ext.stats[`oral_${gender}`][head.reality]++;
		}else if(act === "anal"){
			ext.stats[`anal_${gender}`][head.reality]++;
		}
	}
}




/**
 * @param {String} input 
 * @return {String}  if hardbug mode, returns  "`<i>(##input)</i>"
 */
export function spoiler(input){
	return hardbug ? `<i>(##${input})</i>` : ``;
}


//posítive masochist; negative victim
export function mas_vic(){
	return mile.b_mas - mile.b_vic;
}
	
export function masochist(yes = true, no = ""){
	if(yes === true){
		return mile.b_mas > mile.b_vic;
	}else{
		return mile.b_mas > mile.b_vic ? yes : no;
	}
}

export function dyke(input = true){
	if(mile.slut > 4 && mile.lesbian > 1) return true; //TODO
	return input === true ? false : "";
}




export function bills(amount){
	if(typeof amount === "string") amount = money[amount];
	//TODO - COLOR OF EUROBILLS!!!!!
	return `the bills`;
}


export function buttplug(input = mile.buttplug){
	switch(input){
		default: return "buttplug";
		case 1: return "small plastic buttplug";
		case 2: return "steel buttplug";
		//case 3: return "thick black dildo";
	}
}

export function strapon(input = mile.strapon){
	switch(input){
		default: return "strapon";
		case 1: return "strap-on dildo";
		case 2: return "big strap-on dildo";
		case 3: return "massive strap-on dildo";
	}
}



export function dildo(input = mile.dildo){
	switch(input){
		default: return "dildo";
		case 1: return "ergonomic blue dildo";
		case 2: return "sleek glass dildo";
		case 3: return "thick black dildo";
		case 4: return "huge purple dildo";
		case 5: return "obscene fantasy dildo";
		case 6: return "monstrous dragon dildo";
		case 7: return "absolutely enormous horsecock-shaped dildo";
		case 10:
		case 0: return "trusty pink dildo"
	}
}


export function dildo_material(input = mile.dildo){
	switch(input){
		default: return "";
		case 1: return "blue silicone";
		case 2: return "borosilicate glass";
		case 3: return "black, wrinkled";
		case 4: return "purple silicone";
		case 5: return "bump-covered orange and green"; //obscene fantasy dildo
		case 6: return  "bump-covered orange and green"; //monstrous dragon dildo";
		case 7: return "heavy pinto";
		case 10:
		case 0: return "pink silicone"
	}
}




function sex_toys(){ //TODO - SHOULD BE HERE?!?
	let toys = 2;
	let dildos_count = 1;
	let tex = `There was my old pink dildo, broken vibrator I got for my 18th birthday`

	//dildos
		const dildos = [];
		for(let i = 1; i <= 7; i++){
			if(mile[`dildo_${i}`] && mile.dildo_task !== i ) dildos.push( dildo(i) );
		}

		if(mile.dildo_task){
			if(dildos.length){
				tex += `, the ${dildo(mile.dildo_task)} the nerds made me to buy and ${and(dildos)} I bought on my own. `
			}else{
				tex += ` and the ${dildo(mile.dildo_task)} the nerds made me to buy. `
			}
		}else if(dildos.length){
			tex += ` and my new ${and(dildos)}. `
		}

		dildos_count += dildos.length;
		toys += dildos.length;
		if(mile.dildo_task){
			dildos_count++;
			toys++;
		}
			
	
	//buttplug
		tex += `Next to them `;

		if(mile.buttplug === 1){
			tex += `was my buttplug`;
			toys += 1;
		}else if(mile.buttplug === 2){
			tex += `were my two buttplugs, smaller and bigger`;
			toys += 2;
		}else if(mile.buttplug === 3){
			tex += `were my three buttplugs`;
			toys += 3;
		}
		if(mile.plugged){
			tex += `(I cleaned the ${buttplug(mile.plugged)} and hid it into the drawer before @qaa arrived because leaving it in open felt awkward.) `
		}else{
			tex += `. `;
		}

	//strapons 
		const strapons = [];
		for(let i = 1; i <= 3; i++){
			if(mile[`strapon_${i}`]  ) strapons.push( strapon(i) );
		}
		toys += strapons.length;
	 
		tex += `And finally there ${strapons.length > 1 ? "were" : "was"} my ${and(strapons)}. `

		return {tex, toys, strapons, dildos, dildos_count};
}