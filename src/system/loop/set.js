import {DISPLAY} from "Gui/gui";
import {emo, INVENTORY, update, updateOriginalMods, createMultiple, showLess, showAll, effigy, removeEverything, wearOutfit, saveWornOutfit, updateDraw, updateMods, onlyChangedMods} from "Avatar/index";
import {dismiss, fellowship} from "Virtual/follower";

import {clone} from "Libraries/dh";




export const set = {
	shared(){
		updateOriginalMods(PC, {Mods: ext[this.key]});  //drops all the Mods (body dimensions) changes; restores the original one (seeded generation) + user edits and then update them with saved ones
		
		showAll(); //includes - or maybe does not?????
		effigy.showAll(); //TODO!!!!!
				
		update(PC); //emo calls draw(); //updateDraw(PC);
		emo("relax", PC); //default facial expressions
		
		DISPLAY.check();
	},
	
	save(){
		ext[this.key] = onlyChangedMods(PC);
	},
	
	clr(){
		head.present = undefined;
	},
	
	fantasyClothes(fantasy){
		if(fantasy){
			saveWornOutfit(`auto_real`, `clothes worn irl (auto)`, PC, 0);
			wearOutfit(`auto_fantasy`, PC, [], true);
		}else{
			saveWornOutfit(`auto_fantasy`, `clothes worn in vr game (auto)`, PC, 0);
			wearOutfit(`auto_real`, PC, [], true);
		}
	},
	
	
	//real life
	irl(){
		this.clr();
		if(head.reality === 3){ //back to reality - changes clothes to normal & dismisses follower
			this.fantasyClothes(false);
			head.act_follower = ext.follower?.key;
			dismiss();
		}
		this.save();
		head.reality = 1;
		this.shared();
	},
	
	//metaverse
	meta(){
		this.clr();
		if(head.reality === 3){
			this.fantasyClothes(false);
			head.act_follower = ext.follower?.key;
			dismiss();
		}
		this.save();
		head.reality = 2;
		this.shared();
	},
	
	//virtual game fantasy word 
	game(){
		const into_game = head.reality === 1 || head.reality === 2;
		this.clr();
		if(into_game) this.fantasyClothes(true);
		this.save();
		head.reality = 3;
		if(into_game){
			PC.effects = [];
			fellowship(head.act_follower); //BEWARE - HAS TO BE AFTER THE head.reality change
		}
		this.shared();
		
// !!!!!!!!!!!!!!!!!!!!!!!!!!
		//TODO!!! SHOULD OR SHOULD NOT BE HERE? i
		PC.dyno.hp = PC.att.hp;
		PC.dyno.eng = PC.att.eng;
// !!!!!!!!!!!!!!!!!!!!!!!

	},
	
	
	//TODO - SHOULD BE HERE?
	reentry(chapter, scene = 101){
		head.act_chapter = chapter;
		head.act_scene = scene;
	},

	//TODO - DEFINITELY SHOULD NOT BE HERE
	get key(){ //accessed here as this.key BEWARE
		if(head.reality === 3){
			return `game`;
		}else if(head.reality === 2){
			return `meta`;
		}else{
			return `irl`;
		}
	},

};