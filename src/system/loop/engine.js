import {story} from "Story/index";
import {room} from "Dungeon/rooms/index"; 
import {machina} from "Dungeon/engine";
import {DISPLAY, ULTRA} from "Gui/gui";
import {preprocess} from "Loop/preprocess_text";
import {author, NPC, NAME, stored_mile} from "System/variables";
import {create_effects, clear_effects, link_effects} from "Loop/choice.js";
import {next_event} from "Taskmaster/scheduler"; 
import {shuffle} from "Libraries/dh";
import {showAll, effigy, draw, updateDraw} from "Avatar/index";
import {createTransformation, transformAndShow} from "Libraries/da";

//import {last} from "Loop/text"; //TODO - REALLY STUPID TO HANDLE THIS WAS TOTO ASAP

export {choice, effects, choices_required} from "Loop/choice.js"; //MOVE TO INDEX
export {set} from "Loop/set.js"; //MOVE TO INDEX


import {constants} from "Data/constants";

export function update_variables(){
	//window.mile is stored in stored_mile - player can manually save the game after the scene happened (and changed variables) but we need to save unchanged variables (stored_mile) because when the game is reloaded, the whole scene happens again (including the change of the variables, which would happen twice)
	Object.keys(stored_mile).forEach( key => delete stored_mile[key] );
	Object.keys(mile).forEach( key => stored_mile[key] = mile[key] );
	
	["slut","sub","stat"].forEach( key => {
		//calculates overall level
		for(let i = 0; ; i++){
			ext[key] = i;
			if(constants[key].limits[i] === undefined) break;
			if(mile[key] <= constants[key].limits[i]) break;
		}
	});
	
}






//CORE
export function main(scene = head.scene, chapter = head.chapter, mod){
	/* eslint-disable no-console */
	if(debug) console.log(`<${scene},${chapter}>`); //SIC
	/* eslint-enable no-console */

	//new scene
	if(scene === null) return;
	update_head(scene, chapter);
	
	//before
	clr_before();
	update_variables();
	
	//STORY
	if(ext.dungeon){ //TODO BAD!!!
		/*
		if(room[chapter] === undefined || typeof room[chapter] !== "function"){
			story.error(chapter);
		}else{
			room[chapter](scene, mod);
		}
		*/ //there is fallback room, I think
		room[chapter](scene, mod);
		machina(); //TODO??
	}else{
		if(story[chapter] === undefined || typeof story[chapter] !== "function"){
			story.error(chapter);
		}else{
			story[chapter](scene);
		}
	}
	
	//random choices/effects
	const nolinks = (() => {
		const temp = link_effects();
		if(temp.link) cache.links.push(temp.link);
		return temp.nolink ? [temp.nolink] : [];
	})();
	
	//GUI
	DISPLAY.check(); //TODO
	DISPLAY.text_content({
		paras: cache.paras,
		links: cache.links,
		nolinks,
		done: () => {
			efigize();
			create_effects();
			cache.done.forEach( fce => fce() );
		},
	});
	
	//after
	done();
}



//CORE -RELATED
function update_head(scene, chapter){
	head.scene = scene;
	head.chapter = chapter;
	//could be used for BACK function to go to previous scene but too much extra work (saving the state etc.)
}


//TODO - SOLVE THE STUPID REDUNDANCY


export function clr_before(){
	//ULTRA.off();
	
	//DISPLAY.text_clr();
	cache.paras.length = 0;
	cache.links.length = 0;
	cache.done.length = 0;
	
	clear_effects();
	mention_npc.length = 0;
	
	
	//showAll(); //TODO????
	//last = "";
	draw(PC); //TODO???
	
	run_initial_on_destroy = true;
}	

export let run_initial_on_destroy = false;

export function clr_after(){
	run_initial_on_destroy = false;
	ULTRA.off();
}	



export function done(){

}	

const cache = {
	paras: [],
	links: [],
	done: [],
};




//TEXT & LINKS
export function text(text, style){
	if(text === undefined) return;
	text = preprocess(text); //remove needless whitespaces, add names att
	cache.paras.push({text, style});
}

export function speech(text, key){
	if(text === undefined) return;
	mention_npc.push(key);
	text = preprocess(text); //remove needless whitespaces, add names att
	const color = NPC[key].color ?? NPC[ NPC[key].parent ]?.color;
	if(SETTING.show_names) text = `<span class = "name_${key}">${head?.present?.[key]?.name || NAME[key]}</span>: "${text}"`;
	cache.paras.push({text, color, style: "speech"});
}

export function wait(fce){
	cache.done.push(fce);
}

export function con_link(con, reason, text, scene, before = () => {}, after = () => {}, condition = true, chapter, alt){
	if(con){
		link(text, scene, before, after, true, chapter, alt)
	}else if(hardbug){
		link(`##${text} (${reason})`, scene, before, after, true, chapter, alt)
	}else if(softbug){ //TODO
		link(`${text} (${reason})`, scene, before, after, false, chapter, alt)
	}
}


export function link(text, scene, before = () => {}, after = () => {}, condition = true, chapter, alt){
	text = preprocess(text);
	if(!condition){
		cache.links.push({
			text, 
			style: "dead",
			alt,
		});
	}else{
		cache.links.push({
			text, 
			fce(){
				//ULTRA.off(); //TODO ????
				clr_after();
				if(typeof before !== `function`){
					/* eslint-disable no-alert, no-console */
						if(debug) alert(`ERROR! "${before}" is NOT function! (Part of link "${text}"; "${scene}")`);
						console.error(`ERROR: "${before}" is NOT function! (Part of link "${text}"; "${scene}")`); 
					/* eslint-enable no-alert, no-console */
				}else{
					before();
				}
				if(scene !== null) main(scene, chapter);
				after();
			},
			alt,
		});
	}
}

export function next(text = "Continue. ", before = () => {}, condition = true){
	text = preprocess(text);
	if(!condition){
		cache.links.push({
			text, 
			style: "dead",
		});
	}else{
		cache.links.push({
			text, 
			fce(){
				//ULTRA.off(); //TODO ????
				clr_after();
				if(typeof before !== `function`){
					/* eslint-disable no-alert, no-console */
						if(debug) alert(`ERROR! "${before}" is NOT function! (Part of next "${text}"`);
						console.error(`ERROR: "${before}" is NOT function! (Part of link "${text}"`); 
					/* eslint-enable no-alert, no-console */
				}else{
					before();
				}
				next_event();
			},
		});
	}
}

export function chapter(text, chapter, scene = 101, before){
	link(text, scene, before, undefined, undefined, chapter);
}

//TODO - is ultra_
export function alt_link(text, scene, alt, before = () => {}, after = () => {}, condition = true, chapter){
	link(text, scene, before, after, condition, chapter, alt);
}

export function links_mix(...items){
	items = shuffle(items);
	items.forEach( a => link(a[0], a[1], a[2], a[3], a[4], a[5], a[6]) );
}

//TODO - wacky and WILL cause troubles
export function insert(chapter, scene = 101, before, original_chapter, original_scene){
	head.insert_chapter = original_chapter;
	head.insert_scene = original_scene;
	if(before) before();
	head.chapter = chapter;
	head.scene = scene;
	story[chapter](scene);
}

export function insert_return(text, before){
	//TODO head.insert_scene etc. should be probably set to undefined
	link(text, head.insert_scene, before, undefined, undefined, head.insert_chapter);
}







//MENTIONED
//displays avatars of npc's on mouse overs
export const mention_npc = [];
function mentioned_npcs(){ //remembers names of mentioned npcs or npc who spoke (to not go through the list of all npcs)
	return [...new Set(mention_npc)].filter( a => a );
}
function efigize(){
	mentioned_npcs().forEach( key => {
		Array.from(document.getElementsByClassName(`name_${key}`)).forEach( element => {
			element.addEventListener("mouseenter",  event => effigy[key]?.draw() );
			element.addEventListener("mouseleave",  event => draw(PC) );
			element.style.cursor = "zoom-in"; //TODO stupid
		})
	})
}




//PRESENT
export const present = function(...array){
	head.present = {};
	array.forEach( a => {
		if(!isNaN(a[0])){
			const [index, name, clothes, body] = a;
			head.present[`npc_${index}`] = {name, clothes, body};
		}else{
			const [key, clothes] = a;
			head.present[key] = {clothes};
		}
	});
};


/*
let current = {};
let previous = {};

function rollback_link(){
	if(SETTING.rollback){
		link(`# Back`, head.previous.scene, rollback, updateDraw, true, head.previous.chapter);
	}
}


function rollback(){
	head.reality = previous.reality;
	head.schedule = previous.schedule;
	
	mile = clone(previous.mile);
	ext = clone(previous.ext);
	PC.Mods = clone(previous.mods);
	counter = clone(previous.counter);
	
	//main(hp.scene, hp.chapter);
}



*/
