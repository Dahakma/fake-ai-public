import {time} from "System/time";

function game_load(){
	return {
		name: `Play the Game (${note()})`,
		event: head.act_chapter,
		scene: head.act_scene,
		type: `game`,
	}
}

function game_skip(what){
	const alternative = (()=>{
		if(mile.game_progress < 20){
			return `skip_feyd`;
		}else if(mile.game_progress < 22){
			return `skip_goth_1`;
		}else if(mile.game_progress < 24){
			return `skip_ship`;
		}else if(mile.game_progress < 28){
			return `skip_goth_2`;
		}
	})();
	
	what.name ??= `Play the Game  (${note()})`;
	return {
		name: what.name,
		event: alternative,
		type: `game`,
	}
}


function note(){
	if(mile.game_progress < 10){
		return  `Feyd Mora`;
	}else if(mile.game_progress === 20){
		return  `Voyage to Gothred`;
	}else if(mile.game_progress < 30){
		return  `Gothred`;
	}else{
		return  ``;
	}
}


/*
	ACT 1 
	Feyd Mora
	ACT 2
	Voyage to Gothred
	Gothred
	
	voyage; exit
	goblins || ghost ship; arazor meeting; exit
	kargerd; arazor meeting; exit
	oskyl, escape; exit
*/



/*
	mile.game_progress
	0 - intro
	10 - Feyd Mora visited
	20 - Voyage to Gothred
	21 arrived
	22 - Meeting with Arazor, searching for the crew
	23 ship
	24 - Karged explored, next step Oskyl
	28 - Gothred finished
*/

export const game_progress = function(output){
	let added = false;
	let block = "";
	function later_in_week(week){
		return (time.day === "thu" || time.day === "fri" || time.is_weekend) && (time.week >= week);
	}
	function end_of_week(week){
		return (time.day === "sun") && (time.week >= week);
	}
	
	
	
	function rewrite( what = game_load() ){ //replace other optional choices to force you to play the game
		if(SETTING.no_virtual_game) what = game_skip(what);
		if(hardbug){
			output.forEach( a => a.name = `##${a.name}` );
			output.unshift(what);
		}else{
			output.length = []
			output.push(what);
		}
		added = true;
	}
	
	function add( what = game_load() ){ //adds game among other optional choices
		if(SETTING.no_virtual_game) what = game_skip(what);
		output.push(what);
		added = true;
	}
	
	
	//initiation
	if(mile.game_progress === 0){
		//waits for the "advice_1" event
		//triggers the game after you seek for the advice; game cannot be accessed directly
		//also initiates reentry point
		
		//HAVE  to initiate the game at least later in the week 1
		if( later_in_week(1) ){
			rewrite({
				name: `Play the game`,
				note: `seeking advice`,
				condit(){
					if(!mile.advice_1) return 1;
				},
				event: `advice_1`,		
			});
		}else{
			if(softbug) block = `seek for Advice`;
		}
		
	//Feyd Mora until voyage to Gothred
	}else if(mile.game_progress < 20){
		//first weekend HAVE to start act 2
		if( end_of_week(1) ){
			rewrite();
		}else{
			add();
		}
	
	//end of Voyage and Gothred until meeting Arazor
	}else if(mile.game_progress < 22){
		
		//console.log("vog")
		//console.log( later_in_week(2)  )
		
		if( later_in_week(2)  ){
			//console.log("rew");
			rewrite();
		}else{
			//console.log("add");
			add();
		}
	
	//traveling Dewyrain sea, exploring Karged and returning to Arazor
	}else if(mile.game_progress < 24){
		if(!mile.advice_2 || time.week < 2){
			//waiting for advice_2
			//also not during the first week
			if(softbug){
				if(!mile.advice_2 && time.week < 2){
					block = `ask for Advice 2; too ahead in Game`;
				}else if(!mile.advice_2){
					block = `ask for Advice 2`;
				}else{
					block = `too ahead in Game`;
				}
			}
			
		}else if( end_of_week(2) ){
			rewrite();
		}else{
			add();
		}
	
	//finishing Gothred storyline, escape
	}else if(mile.game_progress < 28){
		add();
		
	}else{
		//end of the current content
	}	
	
	
	
	//if game not available - make it available in hardbug mode
	if(!added && hardbug){
		//console.log("habr")
		const what = game_load();
		what.name = `##${what.name} (${block}) (${head.act_chapter};${head.act_scene})`;
		add(what)
		
	}

	return output;	
}
