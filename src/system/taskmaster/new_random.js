/**
	decides which (or if any) random event from `Data/events/randoms` will be triggered during workday and schedules it
*/

import {DISPLAY} from "Gui/gui";
import {randoms} from "Data/events/randoms";
import {weekend_randoms} from "Data/events/weekend_randoms";
import {ra} from "Libraries/random";
import {time} from "System/time";
import {next_event, add_multiple} from "./scheduler";


export const new_random = function(){
	const list = time.is_weekend ? weekend_randoms : randoms;
	const week = time.week;
	
	const available = list.map( a => {
		if(
			time.is_weekend 
			&& ( a.weekends === undefined || a.weekends.includes(week) )   
		){
			return a.condit();
		}else if(
			!time.is_weekend 
			&& ( a.weeks === undefined || a.weeks.includes(week) )   
		){
			return a.condit();
		}else{
			return null;
		}
	});
	/*
		`undefined` - not passing conditions for this task
		`null` - the task is not assigned to this week and might be game-breaking
	*/


	if(SETTING.taskmaster_random){ //will be selected manually 
		DISPLAY.temp({
			available,
			list,
			back: assign_random,
		});
		DISPLAY.subtext("schedule");
		//DISPLAY.subtext("schedule_random");
	}else{
		const chosen = ra.weighted(available);
		assign_random(chosen);
	}
};


export const assign_random = function(chosen){
	const list = time.is_weekend ? weekend_randoms : randoms;
	if(list?.[chosen]?.events === undefined) return next_event();
	add_multiple(list[chosen].events); //schedules all events in the chosen random
	next_event();
};