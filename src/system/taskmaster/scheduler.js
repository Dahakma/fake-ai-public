//import {assign_events} from "../story/assign_events";
import {main, set} from "Loop/engine";
import {DISPLAY} from "Gui/gui";
import {showAll} from "Avatar/index";
import {resetClothesCache, wears} from "Avatar/game/gameCharacters";
import {new_task} from "./new_task";
import {new_random} from "./new_random";
//import {new_hardcoded} from "./new_hardcoded";
import {new_rules_check} from "./new_rules_check";
import {standing_orders} from  "System/bank_account";

import {time} from "System/time";
/*
	0 Friday
	1 Saturday
	2 Sunday
	3 Monday
	7 Friday
	8 Saturday
	9 Sunday
*/


export const order = [
	//"NEW",
	
	"awakening",
		"earlyMorning", //TODO!!
	"morning", 
	//"morning",
		
	"morningReaction",	//typical reaction to task 
	"TASK",
	
	"morningNew",	//the new task is usually set in morning
	"RULES",
	"RANDOM",
	
	
	//"beforeClasses",
	
	"classes",
	"classes2",
	"noon",
	"classes3",
	"classes4",
	
	"afterSchool",
	
	"afternoon0",
	"afternoon",	//or the new task could be set in the afternoon
	"afternoon2",
	"afternoon3",
	
	"evening", //TODO!!!
	"evening2", //TODO!!!

	"eveningReaction",	//or even the evening //EXCLUSIVELY!
	
	"DAYBREAK",
	
	"eveningNew", 
	
	"lateEvening", //todo
	
];

export const revert_schedule = function(when){
	const index = order.indexOf(when);
	if(index === -1) return;
	head.schedule = index - 1; //when called next_event() head.schedule is ++
}


export const active_timeslot = function(){ //TODO BEWARE
	return order[head.schedule];
}


export const name_timeslot = function(){ //TODO BEWARE
	const list = {
		//morning: "before school",
		awakening: "early morning",
		morning: "morning",
		afterSchool: "after school",
		
		//beforeClasses: "before classes"
		classes: "first break",
		classes2: "second break",
		classes3: "fourth break",	
		
		morningReaction: "before classes",
		morningNew: "before classes",
		noon: "noon",
		afternoon0: "early afternoon",
		afternoon: "afternoon",
		afternoon2: "late afternoon",
		afternoon3: "very late afternoon",
		
		eveningNew: "evening",
		eveningReaction: "evening",
		
		lateEvening: "late evening",
	};
	
	if(time.is_weekend){
		list.morning = `${time.day_name} morning`;
		list.noon = `${time.day_name} noon`;
		list.afternoon = `${time.day_name} afternoon`;
		list.evening = `${time.day_name} evening`;
	}
	
	return list[ order[head.schedule] ] ?? order[head.schedule];
}


export const schedule_initiate = function(){
	head.day = 2;
	standing_orders(); //to get pocket money
	head.schedule = 0;
	
	head.today = {};
	head.yesterday = {};
	head.tomorrow = {};
}



//TODO DEBUG
export const add_tomorrow = function(when, what){
	add(when, what, "tomorrow");
}

export const add_today = function(when, what){
	add(when, what, "today");
}

export const add_workday = function(when, what){
	add(when, what, "workday");
}

export const add_weekend = function(when, what){
	add(when, what, "weekend");
}

export const add_before_workday = function(when, what){
	add(when, what, "before_workday");
}

export const add_multiple = function(input, set = "today"){
	if(!input || typeof input !== "object") return;
	if(input.today || input.tomorrow || input.yesterday || input.before_workday || input.workday || input.weekend){
		["today","tomorrow","yesterday","before_workday","workday","weekend"].forEach( key =>
			add_multiple(input[key], key),
		);
	}else{
		Object.keys(input).forEach( key =>
			add(key, input[key], set),
		)
	}
}


export const remove_today = function(what){
	remove(what, "today");
}

export const remove_tomorrow = function(what){
	remove(what, "tomorrow");
}

export const remove = function(what, set){
	for(const prop in head[set]){
		if(head[set] === what){
			delete head[set];
			return
		}
	}
}
	
export const add = function(when, what, set){
	if( !order.includes(when) ){
		/* eslint-disable no-alert, no-console */
		if(debug){
			alert(`"${when}" slot for "${what}" does not exist`);
		}else{
			console.error(`ERROR: "${when}" slot for "${what}" does not exist`);
		}
		/* eslint-enable no-alert, no-console */
	}
	if(when === "classes" && head[set].classes) when = "classes2";
	if(when === "classes2" && head[set].classes2) when = "classes3";
	if(when === "classes3" && head[set].classes3){
		//random event is assigned before task (so normaly it would be pushed to happen late); but it is better when the task happens later
		head[set].classes4 = head[set].classes3;
		head[set].classes3 = what;
		return;
	}
	if(when === "noon" && head[set].noon) when = "classes3";
	
	if(when === "afterSchool" && head[set].afterSchool) when = "afternoon0";
	if(when === "afternoon0" && head[set].afternoon0) when = "afternoon2"; //TODO!!!!
	if(when === "afternoon" && head[set].afternoon) when = "afternoon2";
	if(when === "afternoon2" && head[set].afternoon2) when = "afternoon3";
	
	//scheduling conflict
	if( head[set][when] ){
		/* eslint-disable no-alert, no-console */
			if(debug){
				alert(`WARNING: Scheduling conflict. Timeslot: "${when}"; Current event: "${head[set][when]}"; Replaced by: "${what}";`);
			}
			console.warn(`WARNING: Scheduling conflict. Timeslot: "${when}"; Current event: "${head[set][when]}"; Replaced by: "${what}";`);
		/* eslint-enable no-alert, no-console */
	}
	
	
	head[set][when] = what;
}



export const add_rule = function(rule){
	//if(!ext.timer) ext.timer = []; //TODO!!
	//ext.timers.push([1, `rule`, what]);
	ext.timers.push({
		countdown: 1,
		rule,
	});
}

export const add_mile = function(countdown = 1,  mile,  value){
	//if(!ext.timer) ext.timer = []; //TODO!!
	//ext.timers.push([time, `mile`, what]);
	ext.timers.push({
		countdown,
		mile,
		value,
	});
}

export const add_timer = function(countdown = 1, fce){
	//if(!ext.timer) ext.timer = []; //TODO!!
	//ext.timers.push([time, `mile`, what]);
	ext.timers.push({
		countdown,
		fce,
	});
}



export const free_today = function(when){
	return head.today[when] === undefined ||  head.today[when] === null;
}

export const no_morning_meeting = function(when){
	return head.today.morningReaction === undefined && head.today.morningNew === undefined;
}


export const free_schedule = function(when){
	return head.tomorrow[when] === undefined || head.tomorrow[when] === null; //TODO - dont remember if it should be null or undefined!!
}

export const get_schedule = function(when){
	return head.tomorrow[when];
}

export const next_event = function(){

	for(let failsafe = 100; failsafe > 0; failsafe--){
		head.schedule++;
		
		//NEXT DAY
		if(head.schedule >= order.length) return next_day();  
		
		const key = order[head.schedule];


		if(head.today[key] === null){ //forced skip
			return next_event();
		}

		if(head.today[key]){
			return main(101, head.today[key]);
		}
		

//CURRENT ENDING 
/*
if(key === "DAYBREAK" && head.day === 23){
	return main(101, "current_end");	
}
*/
		
		//MORNING 
		if(key === "awakening"){
			//return DISPLAY.morning();
			return main(101, "awakening");
		}
		
		//WEEKEND
		if(time.is_weekend){  
			if(["morning", "noon", "afternoon", "evening"].includes(key)){
				//console.error("xxx")
				return main(101, "weekend");
			}
		}
			
		//NEW TASK
		if(key === "TASK" /*&& !time.weekend_follows*/){ 
			return new_task(); 
		}
		
	
		//NEW RANDOM
		if(key === "RANDOM" /*&& time.is_weekday*/){
			if(!mile.skip_random){
				return new_random();	
			}else{
				mile.skip_random = false;
			}
		}
		
		//RULES
		if(key === "RULES" && time.is_weekday){
			return new_rules_check();	
		}
		

		if(key === "afternoon" && time.is_weekday){
			return main(101, "afternoon");
		}
		
	}
}








import {nightie} from "Avatar/game/nightie"; //TODO? 



function resolve_timers(){
	ext.timers.forEach( timer => timer.countdown-- );
	ext.timers.filter(timer => timer.countdown === 0).forEach( timer => {
		if(timer.rule){
			ext.rules[timer.rule] = true;
		}else if(timer.mile){
			if(debug) console.log(`>Timer resoved: "mile.${timer.mile} = ${timer.value} "`);
			mile[ timer.mile ] = timer.value;
		}else if(typeof timer.fce === "function"){
			timer.fce();
		}
	})
	ext.timers = ext.timers.filter(timer => timer.countdown); //remove resovled ones
	
}


export const next_day = function(){
	
	showAll(); //TODO - doesn't have to be set with next
	head.schedule = -1;
	head.day++;
	standing_orders(); //bank account payments; after head.day increase
	
	ext.last_optional = undefined;
	
	counter = {};
	
	//TIMERS
	resolve_timers();
/*
	ext.timers.forEach( a => a[0]-- );
	ext.timers.filter(a => a[0] === 0).forEach( a => {
		if(a[1] === `rule`){
			ext.rules[a[2]] = true;
		}else if(a[1] === `mile`){
			if(debug) console.log(`>Timer resoved: "mile.${a[2]} = ${a[3]} "`);
			mile[a[2]] = a[3];
		}
	})
	ext.timers = ext.timers.filter(a => a[0])
*/
	
	//AVATARS 
	resetClothesCache();
	
	order.forEach( a => {
		if(head.yesterday[a] !== undefined) delete head.yesterday[a]; //checking undefined - intentionally empty are null

		//evenst from Today to Yesterday
		if(head.today[a] !== undefined){
			head.yesterday[a] = head.today[a];
			delete head.today[a]; // = undefined;
		}

		//event from Tommorow to Today
		if(head.tomorrow[a] !== undefined){
			head.today[a] = head.tomorrow[a];
			delete head.tomorrow[a]; // = undefined;
		}
	})	
		
		
	order.forEach( a => {
		//workday happens on the closest workday 
		if(head.workday[a] !== undefined && time.is_weekday){
			add_today(a, head.workday[a]); 
			delete head.workday[a]; // = undefined;	
		}else{
			//weekend happens on the closest weekend
			if(head.weekend[a] !== undefined){
				add_today(a, head.weekend[a]);
				delete head.weekend[a];
			}
		}
		
		//events happening before the closest workday //TODO - needlesly complcated but used like once 
		if(head.before_workday[a] !== undefined && time.day !== "sat" && time.day !== "fri"){
			add_today(a, head.before_workday[a]);
			delete head.before_workday[a]; // = undefined;
		}
		
	})
	
	
	mile.freepass = false; //rule-breaking is ignored (because it was already addressed)
	if(mile.plugged) ext.stats.plugged++; //days with inserted buttplug
	//if(!wears.panties) ext.stats.commando++; //days without wearing underwear
	if(!PC.panties) ext.stats.commando++; //days without wearing underwear
	if(!PC.bra) ext.stats.no_bra++; //days without wearing bra
	
	set.irl();
	PC.Mods.hairLength += 1; //TODO - probably should be one
	nightie(); //TODO?
		
	next_event();
	
	//autosave(/*name*/); //TODO - can work ?????

}












