/**
	decides which optional events from `Data/events/optionals` will be available during afternoons or weekends and returns array of them
*/

import {optionals} from "Data/events/optionals";
import {game_progress} from "./game_progress";
import {capitalise} from "Libraries/dh";
import {time} from "System/time";


export const new_optionals = function(){
	const week = time.week;
	let available = [];
	let output = [];
	
	function add(a, prefix = ""){
		let name = `${prefix}${capitalise(a.name)}`;
		if(softbug && a.note) name += ` (${prefix ? "" : "#"}${a.note})`;
		return {
			name,
			event: a.event,
			type: a.type,
		};
	}
	
	
	//check which are available based on week and other conditions
	available = optionals.map( a => {
			if(
				time.is_weekend 
				&& ( a.weekends === undefined || a.weekends.includes(week) )   
			){
				return a.condit();
			}else if(
				!time.is_weekend 
				&& ( a.weeks === undefined || a.weeks.includes(week) )   
			){
				return a.condit();
			}else{
				return null;
			}
	});	


/*		
	if(time.is_weekend){
		available = optionals.map( a => {
			if( a.weekends === undefined || a.weekends.includes(week)  ){
				return a.condit();
			}else{
				return null;
			}
		});		
	}else{
		available = optionals.map( a => {
			if( a.weeks === undefined || a.weeks.includes(week)  ){
				return a.condit();
			}else{
				return null;
			}
		});	
	}
*/
	
	
//TODO - OPTIONALS OF THE SAME TYPE

	//STANDARD OPTIONALS - add optionals which are available; except for those which would repeat the event of same type twice in row
	output = optionals.map( (a,i) => { 
		if( available[i] /*&& (!a.type || ext.last_optional !== a.type)*/ ) return add(a);
		return;
	}).filter( a => a);
	


/*
	//SOFTBUG - adds available but not added above because of the type
	if(softbug){
		optionals.forEach( (a,i) => { 
			if( available[i] ) output.push( add(a,`#`) );
		});
	}
*/


	//VIRTUAL GAME - adds option to play the game (could be disabled or rewrite all the other options - based on game progress)
	game_progress(output); 
	
	
	//HARDBUG - adds those unavailable because conditions are not met
	if(hardbug){
		optionals.forEach( (a,i) => { 
			if(!available[i] && available[i] !== null) output.push( add(a,`##`) );
		});
	}
	
	//DEBUG - adds those unavailable this week
	if(debug){
		optionals.forEach( (a,i) => { 
			if(available[i] === null) output.push( add(a,`###`) );
		});
	}
	
	
	return output;
	
};