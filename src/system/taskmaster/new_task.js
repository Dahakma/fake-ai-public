/**
	decides which task from `Data/events/task` will have to be fulfilled tomorrow and schedules it
*/

import {DISPLAY} from "Gui/gui";
import {ra} from "Libraries/random";
import {tasks} from "Data/events/tasks";
import {time} from "System/time";
import {next_event, add_multiple, get_schedule} from "./scheduler";


export const new_task = function(){
	let week = time.week;
	if(time.day === "sat") return next_event(); //no task is assigned on Saturday
	if(time.day === "fri") return assign_weekend_task(); //Friday - special weekend task
	if(time.day === "sun") week++; //Monday task is assigned on Sunday which belongs to the previous week
	
	const available = tasks.map( a => {
		if( a.weeks === undefined || a.weeks.includes(week)  ){ 
			if(time.day === "sun" && a.events.yesterday?.morningNew){ //Monday task cannot be given on Sunday morning (evening or is not given a day in advance)
				return undefined;
			}else if( ext.stats.tasks[head.day] === a.name ){ //the task is already scheduled for today //TODO  - checking the name is clumy
				return null;
			}
			return a.condit();
		}else{
			return null;
		}
	});
	/*
		`undefined` - not passing conditions for this task
		`null` - the task is not assigned to this week and might be game-breaking
	*/
	
	
	if(SETTING.taskmaster){ //displays window where the task could be picked manually 
		DISPLAY.temp({
			available,
			list: tasks,
			back: assign_task,
		});
		DISPLAY.subtext("schedule");
		/*
		DISPLAY.temp(available);
		DISPLAY.subtext("schedule_task");
		*/
	}else{
		const chosen = ra.weighted(available);
		assign_task(chosen);
	}
};


export const assign_task = function(chosen){
	if(tasks?.[chosen]?.events === undefined) return next_event(); //something is broken
	ext.stats.tasks[head.day+1] = tasks[chosen].name; //recording game stats
	
	const events = tasks[chosen].events;
	const temp = {};

	//task are assigned a day before i.e. "today" part is scheduled for tomorrow and "yesterday" part happens today. 
	if(events.today || events.yesterday){
		temp.today = events.yesterday;
		temp.tomorrow = events.today;
	}else{
		temp.tomorrow = events;
	}
	
	//if "anyNew", new task is assigned during "morningNew" or "eveningNew" based on when the nerds have an opportunity to talk with Kate
	if(temp.today?.anyNew){ 
		if(get_schedule("eveningReaction") || time.day === "sun" || !get_schedule("morningReaction") ){
			temp.today.eveningNew = temp.today.anyNew;
		}else{
			temp.today.morningNew = temp.today.anyNew;
		}
		delete temp.today.anyNew;
	}

	add_multiple(temp);
	next_event();
};


function assign_weekend_task(){ //weekend task; assinged & taking place during fridays
	const index = tasks.findIndex( a => a.weekend_task === time.week );
	if(index === -1) return;
	add_multiple( tasks[index].events );
	next_event();
}