import {rg, ra} from "Libraries/random";
import {filters} from "../game/rules";
import {garmentIs} from "Avatar/wardrobe/types";
import {unpackGarmentIdSeed} from "Avatar/wardrobe/main";
import {allPiercings} from "Avatar/wardrobe/all";
import {garment} from "Data/items/blueprints/index";


let randomOutfitLastId;
export function randomOutfit(inventory, rules = false){
	let outfits = [];
	
	//only outfit saved irl (or meta) are available irl (filters out system & fantasy outfits)
	if(head.reality === 1 || head.reality === 2){
		outfits = ext.outfits.filter( a => a.reality === 1 || a.reality === 2);
	}else if(head.reality === 3){
		outfits = ext.outfits.filter( a => a.reality === 3);
	}else{
		return;
	}

	outfits = outfits.filter( a => a.random );

	if(outfits.length > 1) outfits = outfits.filter( a => a.id !== randomOutfitLastId);
	const id = ra.array(outfits).id;
	randomOutfitLastId = id;
	return id;
}
	
	
	
export const randomGarment = function(inventory, subject = PC, slot, type, rules){
	let array = inventory.filter( a => a?.slot === slot); //clones && removes undefined
	if(rules) array = rule_complying(array);
	return rg.array(array);
};

export function randomClothes(inventory, subject = PC, type, rules, slut = ext.slut ?? 1){
	let array = inventory.filter(a => a); //clones && removes undefined
	if(rules) array = rule_complying(array);	

	if(fashion[type]){
		return fashion[type](subject, array, slut);
	}else{
		return fashion.normal(subject, array, slut);
	}
	
}



//filters out garments not complying with rules
function rule_complying(array){
	Object.keys(ext.rules).filter(
		a => ext.rules[a], //only active rules (=== true)
	).forEach( b => {	//BEWARE ext.rules!!
		/* eslint-disable no-alert, no-console */
			if(b === "enforced") return;
			if(debug && !filters[b]) return console.warn(`WARINING: Filter for clothes "${b}" NOT found!`);
		/* eslint-enable no-alert, no-console */
		
		array = filters[b](array);
	});
	return array;
}












const fashion = {
	
//CUSTOM
	custom(subject, inventory, slut){
		const bra = {
			slot: "bra", 
			condit: +ext.dressup.bra,
		};
		
		const panties = {
			slot: "panties", 
			condit: +ext.dressup.panties,
		};
		
		if(ext.dressup.sexy_underwear){
			//TODO - sluttyPanties???
			panties.filters = [
				garmentIs.sexyPanties, 
				garmentIs.nicePanties, 
			];
			bra.filters = [
				garmentIs.niceBra, 
			];
		}
		
		
		const upper = {
			slot: (()=>{
				if(ext.dressup.dresses && !ext.dressup.skirts && !ext.dressup.pants) return "upperLower"; //dresses are only allowed option
				if(ext.dressup.dresses) return ["upper", "upperLower"]; //dresses or tops
				return "upper"; //only tops
			})(),
		};
		
		const lower = {
			slot: "lower", 
			strict: true, // ext.dressup.dresses ? true : false, //TODO - HOW STRICT???
			filters: a => {
				if( ext.dressup.skirts && garmentIs.skirt(a) ) return true;
				if( ext.dressup.pants && garmentIs.pants(a) ) return true;
			},
		};
		
		const socks = {
			slot: "socks", 
			strict: true,
			filters: a => {
				if( ext.dressup.socks && garmentIs.socks(a) ) return true;
				if( ext.dressup.high_socks && garmentIs.highSocks(a) ) return true;
				if( ext.dressup.stockings && garmentIs.stockings(a) ) return true;
				if( ext.dressup.pantyhose && garmentIs.pantyhose(a) ) return true;
			},
		};
		
		const collar = {
			slot: "collar", 
			condit(){
				if( (ext.rules.choker || ext.rules.collar) && ext.dressup.follow_rules) return 1;  //custom setting follow nerd's rules have preference over custom setting wear choker
				return +(ext.dressup.choker ||  ext.dressup.collar) //collar X choker
			},
			filters: a => {
				
				//setting following rules: 
				if(ext.dressup.follow_rules){ 
					if(ext.rules.collar) return garmentIs.collar(a); //always and only collar when rule collar
					if(ext.rules.choker){
						if( ext.dressup.collar && garmentIs.collar(a) ) return true; //collars too if enabled 
						if( ext.dressup.collar &&  !ext.dressup.choker && garmentIs.choker(a)  ) return false; //allows to use collars instead of chokers 
						return garmentIs.choker(a); //otherwise always chokers 
					}
				}
				
				//voluntarily enabled in settings			
				if( ext.dressup.collar && garmentIs.collar(a) ) return true; //collars true when enabled
				if( ext.dressup.choker && garmentIs.choker(a) ) return true; //chokers true when enabled 
				
			},
		};
		
		const belt = {
			slot: "belt", 
			condit: +ext.dressup.belt,
		};
		
		const glasses = {
			slot: "glasses", 
			condit: +ext.dressup.glasses,
		};
		
		const gloves = {
			slot: "gloves", 
			condit: +ext.dressup.gloves,
		};
		
		const piercings = [];
		
		if(ext.dressup.piercings){
			allPiercings.forEach( a => {
				if(subject.apex[a]){
					piercings.push(a);
				}
			});
		}
		
		
		
		return rag(subject, inventory, [
			bra,
			panties,
			socks,
			lower,
			collar,
			"shoes",
			"necklace",
			glasses,
			belt,
			gloves,
			...piercings,
			upper,
		]);
		
	},	
	
	
//CONSERVATIVE 	
	conservative(subject, inventory){ //DODO - CHECK IF EVER USED
		const belt = {
			slot: "belt", 
			condit(){
				return 0.66;
			},
		};
		
		const glasses = {
			slot: "glasses", 
			condit(){
				return mile.glasses ? 0.66 : 0;
			},
		};
		
		const gloves = {
			slot: "gloves", 
			condit(){
				return 0.5;
			},
		};
		
		const piercings = [];
		allPiercings.forEach( a => {
			if(subject.apex[a]){
				piercings.push(a);
			}
		});
		
		return rag(subject, inventory, [
			"bra",
			"panties",
			"socks",
			"lower",
			"shoes",
			"necklace",
			belt,
			glasses,
			gloves,
			...piercings,
			{slot: ["upper", "upperLower"]},
		]);
	},
	
	
//ADVANCED
	advanced(subject, inventory, slut){
		
		const bra = {
			slot: "bra", 
			condit(){
				switch(slut){
					default:
					case 0: return 1; //no
					case 1: return 1; //embarrassing
					case 2: return 0.75;
					case 3: return 0.3; //naughty
					case 4: return 0;
					case 5: return 0;
				}
			},
		};
		const panties = {
			slot: "panties", 
			condit(){
				switch(slut){
					default:
					case 0: return 1; //no
					case 1: return 1; //no
					case 2: return 1; //embarrasing
					case 3: return 1;
					case 4: return 0.75;      //TODO - before 5 was naughty here SLUT5
					case 5: return 0.44; //naughty
				}
			},
		};
		
		const collar = {
			slot: "collar", 
			condit(){
				if(ext.rules.choker || ext.rules.collar) return 1; //TODO - THERE'S NO STANDARISED WAY TO ENFORCE THINGS TO WEAR!!!
				switch(slut){ //TODO SHOULD BE SUB????
					default:
					case 0: return 0;
					case 1: return 0.25;
					case 2: return 0.65;
					case 3: return 0.9;
					case 4: return 1;
					case 5: return 1;
				}
			},
			//TODO - probably there should be filter 
		};
		
		const belt = {
			slot: "belt", 
			condit(){
				return 0.66;
			},
		};
		
		const glasses = {
			slot: "glasses", 
			condit(){
				return mile.glasses ? 0.33 : 0;
			},
		};
		
		const gloves = {
			slot: "gloves", 
			condit(){
				return 0.5;
			},
		};
		
		const piercings = [];
		allPiercings.forEach( a => {
			if(subject.apex[a]){
				piercings.push(a);
			}
		});
		
		
		
		return rag(subject, inventory, [
			bra,
			panties,
			collar,
			"socks",
			"lower",
			"shoes",
			"necklace",
			belt,
			glasses,
			gloves,
			...piercings,
			{slot: ["upper", "upperLower"]},
		]);
	},
	
	
	
	
	
//TODO - SHOULD BE ADVANCED WITHOUT DRESS!!!!
	noDress(subject, inventory, slut){
		const bra = {
			slot: "bra", 
			condit(){
				switch(slut){
					default:
					case 0: return 1; //no
					case 1: return 1; //embarrassing
					case 2: return 0.75;
					case 3: return 0.3; //naughty
					case 4: return 0;
					case 5: return 0;
				}
			},
		};
		const panties = {
			slot: "panties", 
			condit(){
				switch(slut){
					default:
					case 0: return 1; //no
					case 1: return 1; //no
					case 2: return 1; //embarrasing
					case 3: return 1;
					case 4: return 0.75;      //TODO - before 5 was naughty here SLUT5
					case 5: return 0.44; //naughty
				}
			},
		};
		const piercings = [];
		allPiercings.forEach( a => {
			if(subject.apex[a]){
				piercings.push(a);
			}
		});
		
		
		
		return rag(subject, inventory, [
			bra,
			panties,
			"socks",
			"lower",
			"shoes",
			"necklace",
			...piercings,
			"upper",
		]);
	},
	
	
	
	
	normal(subject, inventory){
		return rag(subject, inventory, [
			"bra",
			"panties",
			"socks",
			"lower",
			"shoes",
			"earrings",
			"necklace",
			{slot: ["upper", "upperLower"]},
		]);
	},

	unsexyUnderwear(subject, inventory){
		return rag(subject, inventory, [{
			slot: "bra",
			filters: a => !garmentIs.niceBra(a), 
		},{
			slot: "panties",
			filters: [
				a => !garmentIs.sexyPanties(a), 
			],
		}]);

	},

	sexyUnderwear(subject, inventory){
		return rag(subject, inventory, [{
			slot: "bra",
			filters: garmentIs.niceBra, 
		},{
			slot: "panties",
			filters:  garmentIs.nicePanties, 
		}]);
	},
	
	underwear(subject, inventory){
		return rag(subject, inventory, ["bra","panties"]);
	},
};




/*
- slot {string or array of strings}(optional) - slot of the desired item
- condit {integer or function returning integer}(optional) - probability of this item not being skipped (defaultly don't skip)
- filters {function or array of functions}(optional) - array will be filtered with those functions;
	- input - the item; output - true if should be included
	- if nothing passes through first filtering function, second is tried etc.
- strict = false{boolean}(optional) - if true, returns nothing if nothing pass filters; if false returns at least something
*/
const rag = function(subject, inventory, demands){
	const wear = [];
 

	demands.forEach( a => {
		if(typeof a === "string") a = {slot: a};
		
		let out = [...inventory];
		const {slot, strict} = a;
		let {condit, filters} = a;
		
		
		//is skipped?
		if(condit !== undefined){
			if( typeof condit === "function" ) condit = condit(subject);
			if(rg.g >= condit) return;
		}
	

		//pre-filter based on slot
		if(slot === undefined){
			//nothing
		}else{
			out = out.filter( a => {
				if(Array.isArray(slot)){
					return slot.includes(a.slot);
				}else{
					return slot === a.slot;
				}
			}); 
		}


		if(!out.length) return;
		
		
		//filtering
		if(filters !== undefined){
			if( !Array.isArray(filters) ) filters = [filters];
			while(filters.length){
				const sifter = filters.shift();
				const temp = out.filter(sifter);
				if(temp.length) return wear.push( rg.array(temp) );
			}
		}
		
		//nothing remained after filtering. Add nothing or pre-filtered items?
		if(!strict){
			wear.push( rg.array(out) );
		}
		
	});
		
	return wear;
};









/*
export const filter = {
	no_bra(input){
		return input.filter( a =>  a.slot !== "bra" )
	},
	sexy_panties(input){
		return input.filter( a =>
			a.blueprint !== "briefs" && a.blueprint !== "boyshorts"
		)
	},
	no_panties(input){
		return input.filter( a.slot !== "panties" )
	},
	no_socks(input){
		return input.filter( a =>
			!["socks","neutralSocks"].incluces(a.blueprint)
		)
	},
	sexy_stockings(input){
		return input.filter( a =>
			!["socks","neutralSocks","lightTights","darkTights","pantyhose","polyPantyhose"].includes(a.blueprint)
		)
	},
	short_skirt(input){
		return input.filter( a =>
			!["longSkirt"].includes(a.blueprint)
		)
	},
	show_legs(input){
		return input.filter( a =>
			a.dad !== "wJeans" && a.dad !== "wSuperLeggins"
		)
	},
	sexy_pants(input){
		return input.filter( a =>
			!["jeans","pants"].includes(a.blueprint)
		)
	},
	sexy_top(input){
		return input.filter( a =>
			(["wTubeTop","wHalterTop"].includes(a.dad) || ["sluttyTee","sluttyDualTee","sheerTee","fishnetDualTee"].includes(a.blueprint) || false)
		)
	},
	max_items(input, max){
		return shuffle(input).splice(0, max);
	},
}

*/
 
 


export const randomSexyClothes = function(inventory, subject){
	const wear = [];
	
	
	
	["panties","socks","lower","shoes"].forEach( a => {
		const temp = inventory.filter( b => b.slot === a);
		wear.push( rg.array(temp) );
	});
	
	if(rg.g < 0.60){
		const temp = inventory.filter( a => a.slot === "bra");
		wear.push( rg.array(temp) );
	}
	
	{
		const temp = inventory.filter( a => a.slot === "upper" || a.slot === "upperLower");
		wear.push( rg.array(temp) );
	}
	
	//console.warn(wear);
	return wear;
	
	
	
	
};














export const randomUnderwear = function(inventory){
	const wear = [];
	
	["panties","bra"].forEach( a => {
		const temp = inventory.filter( b => b.slot === a);
		wear.push( rg.array(temp) );
	});
	
	return wear;
};




export const blunt = function(inventory){
	const wear = [];
	
	["bra","panties","socks","lower","shoes","earrings","necklace"].forEach( a => {
		const temp = inventory.filter( b => b.slot === a);
		wear.push( rg.array(temp) );
	});
	
	{
		const temp = inventory.filter( a => a.slot === "upper" || a.slot === "upperLower");
		wear.push( rg.array(temp) );
	}
	
	return wear;
};



export const noDress = function(inventory){
	const wear = [];
	
	["bra","panties","socks","lower","shoes","earrings","necklace"].forEach( a => {
		const temp = inventory.filter( b => b.slot === a);
		wear.push( rg.array(temp) );
	});
		
	{
		const temp = inventory.filter( a => a.slot === "upper");
		wear.push( rg.array(temp) );
	}
	
	return wear;
};




export const bozo = function(inventory){
	
	
	const wear = [];
	
	if(rg.g < 0.85){
		const temp = inventory.filter( a => a.slot === "lower");
		wear.push( rg.array(temp) );
	}
	
	if(rg.g < 0.85){
		const temp = inventory.filter( a => a.slot === "upper" || a.slot === "upperLower");
		wear.push( rg.array(temp) );
	}

	if(rg.g < 0.5){
		const temp = inventory.filter( a => a.slot === "bra");
		wear.push( rg.array(temp) );
	}

	if(rg.g < 0.6){
		const temp = inventory.filter( a => a.slot === "panties");
		wear.push( rg.array(temp) );
	}
	
	if(rg.g < 0.25){
		const temp = inventory.filter( a => a.slot === "upperOuter");
		wear.push( rg.array(temp) );
	}
	
	if(rg.g < 0.75){
		const temp = inventory.filter( a => a.slot === "socks");
		wear.push( rg.array(temp) );
	}
	
	if(rg.g < 0.8){
		const temp = inventory.filter( a => a.slot === "shoes");
		wear.push( rg.array(temp) );
	}
	
	if(rg.g < 0.2){
		const temp = inventory.filter( a => a.slot === "collar");
		wear.push( rg.array(temp) );
	}
	
	return wear;
};