import {
	british,
	
	transparent_1,
	transparent_2,
	
	groinLegCoverage,
	groinWaistCoverage,
	glimpseLegCoverage,
	glimpseWaistCoverage,
	
	stockingsLegCoverage,
	highSocksLegCoverage, 
	
	microSkirtLegCoverage,
	miniSkirtLegCoverage, 
	shortSkirtLegCoverage,
	mediumSkirtLegCoverage,
	
	shortSleevesArmCoverage, 
	miniSleevesArmCoverage,
	
	mediumBreasts,
	bigBreasts,
	hugeBreasts, 
	enormousBreasts,
	
	absoluteCleavage,
	deepCleavage,
	revealingCleavage,
	niceCleavage,

	
} from "./constants";

import {tattoos_slutty} from "Assets/tattoos.js";
import {garment} from "Data/items/blueprints/index";
import {garmentDesc} from "./garmentDesc.js";
import {bodyDesc} from "./bodyDesc.js";

import {
	wears,
	garmentIs,
	subjectWears,
} from "./types";


import {
	itemQuality,
} from "./all";


import {
	ra,
} from "Libraries/random";

import {and} from "Loop/text";
/*
function combine(..args){
	args.filter( a => a).reduce( (s,a) => `${}
	
};
*/
/*
function sp(input){
	return (input && typeof input === "string") ? `${sp} ` : ``;
};
*/
function trim(input){
	if(!input || typeof input !== "string") return ``;
	let output = input.trim(); // "  abc   " => "abc";
	output = output.replace(/[,]{2,}|[;]{2,}|\s{2,}/gi, a => a[0] ); // "abc,,   qwe;;;" => "abc, qwe;";
//TODO ASAP Lookbehind issues
//	output = output.replace(/(?<=\w)[,;](?=\w)/gi, `$& `); // "abc,qwe" => "abc, qwe"
//	output = output.replace(/(?<=\w\.)[a-z]/g, a => ` ${a.toUpperCase()}` ); // "abc.qwe " => "abc. Qwe";
	return output;
}
//console.warn( trim(' as,,df  basdfx,y;s;;feefq.wfefe   ') );



//are nipples on this garment visible? 
export const nipplesVisible = function(slot, subject = PC){
	
	
	if(!subject[slot]) return undefined;

	if(subject.bra && (subject.bra.invisible === undefined || subject.bra.invisible < 1) ) return false; //covered by bra

	if( !subjectWears.tits(subject) ) return false; //character has no tits worthy of mentioning

	if(slot === "upper"){
		if(subject.upperOuter && !subject.upperOuter.cupless && subject.upperOuter.invisible < 1) return false; //covered by outerwear		
	}else if(slot === "upperOuter"){
		if(subject.upper && (subject.upper.invisible === undefined || subject.upper.invisible < 1) ) return false; //covered by shirt
		if(garmentDesc.upperOuter(subject) === "sweater"){ //not used with jackets and corsets
			if(subject.upperOuter.thickness > 1.3) return false;
		}
	}
	
	return true;
};




	

export const wdesc = {};
Object.keys(garmentDesc).forEach( a => {
	Object.defineProperty(wdesc, a, {
		get(){ return garmentDesc[a](PC) },
	});
});

export const bdesc = {};
Object.keys(bodyDesc).forEach( a => {
	Object.defineProperty(bdesc, a, {
		get(){ return bodyDesc[a](PC) },
	});
}); 

export const  checkQuality = function(subject = PC){ //TODO -REWORK!!!
	const slots = ["upper", "lower", "shoes", "necklace"].map(  slot => {
		let quality = itemQuality[ subject?.[slot]?.quality ??  0 ] - 2; 
		if(quality < -2) quality = -2;
		if(quality > 2) quality = 2;
		return quality;
	});
	
	//console.error(slots);
	const quality = Math.round( slots.reduce( (a,b) => a + b ) / slots.length  );
	//console.log(quality);
	
	const qualityDesc = [
		"poor",
		"cheap",
		"fine",
		"premium",
		"luxurious",
	][quality + 2];
	
	return {
		value: quality,
		quality,
		qualityDesc,
	};
	
	/* 
	 export const itemQuality = [ //TODO - should be here?????
	2, //0 ordina
	0, //1 second
	1, //2 cheap
	2, //3 fine
	3, //4 brandede
	4, //5 premium
	5, //6 designer
	3, //7 user created
];
*/
 }
	
	
	
	
	
	
	
export const sluttiness = function(subject = PC){
	
//console.warn("----SLUTCALC----");


	function add(string, number){
		value += number;
		desc += `<li> + ${number} - ${string}`;
	}
	
	const wears = {}; //works as ordinary `wears` but with any subject
	Object.keys(subjectWears).forEach( a => {
		Object.defineProperty(wears, a, {
			get(){ return subjectWears[a](subject) },
		});
	});

	



	let value = 0; //slutiness value
	let desc = "<ul>"; //description of clothes 
	let valueDesc = ""; //value in word
	
	const details = {
		bra: false, //bra visible
		braOnly: false, //breasts are covred only by bra
		topless: false, //no clothes covering upper part of body
		bottomless: false,  //no clothes covering lower part of body
		nude: false,
		zettai: false, //narrow space between skirt and stockings
		
		cleavage: false, //revealing or deep cleavage
		titfall: false, //big breasts almost falling out of cleavage
		nipslip: false, //clothes able to accidentally reveal nipple
		nipples: false, //nipples visible
		breasts: false, //breasts exposed
hintedBreasts: false, //breasts almost visible (transparent clothes)
		
		midriff: false, //belly exposed
		
		tightLower: false, //tight pants x skirt
	lowLower: false, //low cut pants?
	
		glimpse: false,  //glimpse of panties or crotch
		panties: false, //panties visible
		pantiesAbove: false, //panties visible above skirt 
		pantiesBelow: false, //panties visible below skirt
		whaletail: false, //panties above + thong

pussy: false, //genitals visible
hintedPussy: false, //genitals almost visible (transparent clothes)
noPanties: false, //visibly wearing no panties
			
		barefoot: false, //barefoot, no socks
		socksTop: false, //space between skirt and socks
		
		
		bareArms: false,
		bareShoulders: false, 
		miniSleeves: false,
		shortSleeves: false, 
		transparentSleeves: false,
		
		bareUpperChest: false,
		semiBareUpperChest: false,
		shoulderStraps: false,
		
		bareLowerChest: false,
		semiBareLowerChest: false,
		
		bareMidriff: false,
		semiBareMidriff: false,
		
		barePelvis: false,
		semiBarePelvis: false,
		
		transparentUpper: false,
		transparentLower: false,
		
		hintedSluttyTattoo: false,
		sluttyTattoo: false,
		
		hintedSluttyTattooList: [],
		sluttyTattooList: [],
		
	
	};
	
	
	const vest = garmentDesc.upperOuter(subject);
	const shirt = garmentDesc.upper(subject);
	const skirt = garmentDesc.lower(subject);
	const knickers = garmentDesc.panties(subject);
	const stockings = garmentDesc.socks(subject);
	
	const upper = subject.upper;
	const upperOuter = subject.upperOuter;
	const socks = subject.socks;
	
	function fish(item){
		return garmentIs.fishnet(item);
	}
	
	function ether(item){
		return fish(item) || item.alpha < transparent_2;
	}
	
	function hiEther(item){
		const dummy = {
			fishness: item?.hi_fishness,
			alpha: item?.hi_alpha,
		};
		return ether(dummy);
	}
	


	if(upper){
		details.transparentUpper = ether(upper);
	}
	
	
	//if(!subject.bra && !subject.upper && !subject.upperLower) details.topless = true;
	//if(!subject.panties && !subject.lower && !subject.outerLower) details.bottomless = true;
	
	
//COLLAR (max 3)
	if(subject.collar){
		if(wears.collar){
			add("collar",2)
		}else if(wears.choker){
			add("choker",1);
		}
		//TODO EXX - maybe hidden by turtleneck
	}
	
	


//CLEAVAGE (max 4)
	if(vest !== "sweater" && upper){
		//strapless dressed - TODO - quickfix
		if( ["wMiniDress", "wMiniDress3"].includes(upper.dad) /*&& upper.alpha >= transparent_1*/ ){
			details.titfall = true; 
			if(upper.dad === "wMiniDress"){
				upper.showStrap ? add(`dress with feeble straps`, 2) : add(`strapless dress`, 3);
				
				details.bareUpperChest = upper.showStrap;
				details.bareUpperChest = !upper.showStrap;
			}else if(upper.dad === "wMiniDress3"){
				if(upper.chestCoverage < 0.35){
					details.nipslip = true;
					details.bareUpperChest = true;
					add(`revealing strapless dress`, 3);
				}else{
					details.bareUpperChest = true;
					add(`strapless dress`, 2);
				}
			}
		
		//tube top
		}else if(shirt === "tube top" /*&& upper.alpha >= transparent_1*/ ){  //TODO - should transparency override?
			details.bareUpperChest = true;
			details.titfall = true; 
			if(upper.chestCoverage < 0.35){
				details.nipslip = true;
				add("revealing tube top", 3);
			}else{
				add("tube top", 2);
			}
		
		//triangle top
		}else if(upper.dad === "TriangleTop"){
			if(upper.topTriangle && upper.topTriangleCoverage < 0.15){
				details.semiBareUpperChest = true;
			}else{
				details.bareUpperChest = true;
			}
			
		//midCorset
		}else if(shirt === "corset"){
			add("corset", 3);
			details.bareUpperChest = true; //TODO??
		//dual shirt with cleavage
		
		
		//top with cleavage
		}else if(upper.cleavageCoverage && /*upper.alpha >= transparent_1*/ !ether(upper) ){
			let cleavage = upper.cleavageCoverage;
			if(upper.cleavageCoverage2 && hiEther(upper)) cleavage = upper.cleavageCoverage2; //wascky but works for dualShirts 
			if(upper.cleavageInnerCoverage && hiEther(upper)) cleavage = upper.cleavageInnerCoverage; //wascky but works for new SuperTees
			
			if(cleavage > absoluteCleavage && vest !== "corset"){
				add("absolute cleavage", 4);
				details.titfall = true; //TODO
				details.cleavage = true;
			}else if(cleavage > deepCleavage && vest !== "corset"){
				add("deep cleavage", 3);
				details.titfall = true; //TODO
				details.cleavage = true;
			}else if(cleavage > revealingCleavage){
				add("revealing cleavage", 2);
				details.titfall = true; //TODO
				details.cleavage = true;
			}else if(cleavage > niceCleavage){
				add("nice cleavage", 1);
			}
		
			//upper chest exposed
			if(cleavage > 0.2){  //0.18
				details.bareUpperChest = true;
			}else if(cleavage > 0.13){
				details.semiBareUpperChest = true;
			}
			
			//lower chest exposed 
			 if(cleavage > deepCleavage && vest !== "corset"){
				 details.semiBareLowerChest = true;
			 } 
				
			//shoulder straps
			if(upper.innerNeckCoverage !== undefined && upper.outerNeckCoverage !== undefined){
				if( (upper.outerNeckCoverage - upper.innerNeckCoverage) <= 0.1 ) details.shoulderStraps = true;
			}
			
		//top without cleavage
		}else{
		
		}
		
		if(details.transparentUpper){
			details.semiBareUpperChest = true;
			details.semiBareLowerChest = true;
		}
	}
	
	
	

//NIPPLES
	//upperOuter & upper
	if(upperOuter && upper){
		if(vest === "sweater" || vest === "jacket"){ //EXT - dont concider transparent
			if(upper.sideLoose <= 0){
				add(`form-fitting ${vest}`, 1); //TODO - maybe 2
			}
		}else if(vest === "corset"){ //outCorset
			add(`form-fitting ${vest}`, 1); //TODO - maybe 2
		}
		
	//only upperOuter
	}else if(upperOuter){
		if(vest === "corset"){ //outCorset
			if(upperOuter.cupless){
				if(subject.bra){
					add(`bra fully exposed`, 4); //???? TODO WACKY
					
				}else{
					add(`breasts exposed`, 6); //??? TODO
					//details.topless = true;  // TODO - NOT TOPLESS
					details.breasts = true; 
				}
			}else{
				add(vest, 3); //TODO
			}
			
		}else if(vest === "jacket"){
			if(upperOuter.sideLoose <= 0)	add(`form-fitting ${vest}`, 1);
			if(subject.bra){
				add(`just ${vest}  and bra`, 4); 
			}else{
				add(`just ${vest} with nothing under`, 5); 
			}
			
		}else if(vest === "sweater"){
			if(upperOuter.sideLoose <= 0)	add(`form-fitting ${vest}`, 1);
			
			if( fish(upperOuter) ){
				details.hintedBreasts = true;
				add(`fishnet ${vest} with nothing underneath`, 8); //TODO
				if(wears.piercedNipples) add(`pierced nipples`, 1);
			}else if(upperOuter.alpha <  transparent_2){
				details.hintedBreasts = true;
				add(`see-through ${vest} with nothing underneath`, 7);
				if(wears.piercedNipples) add(`pierced nipples`, 1);
			}else if(upperOuter.alpha <  transparent_1){
				//details.hintedBreasts = true; //TODO - yes or not? 
				add(`translucent ${vest} with nothing underneath`, 4);
			}else if( nipplesVisible("upperOuter") ){
				details.nipples = true;
				add(`obviously no bra`, 2);
				if(wears.piercedNipples) add(`pierced nipples`, 1);
			}
		}
		
	//upper	
	}else if(upper){
		//with bra
		if(subject.bra){
			if( fish(upper) ){
				add(`fishnet ${shirt}`, 5); //TODO
			}else if(upper.alpha <  transparent_2){
				add(`see-through ${shirt}`, 4); //TODO  
			}else if(upper.alpha <  transparent_1){
				add(`translucent ${shirt}`, 2);
			}
		//without bra
		}else{
			if( fish(upper) ){
				details.hintedBreasts = true;
				add(`fishnet ${shirt} with nothing underneath`, 8); //TODO
				if(wears.piercedNipples) add(`pierced nipples`, 1);
			}else if(upper.alpha <  transparent_2){
				details.hintedBreasts = true;
				add(`see-through ${shirt} with nothing underneath`, 7);
				if(wears.piercedNipples) add(`pierced nipples`, 1);
			}else if(upper.alpha <  transparent_1){
				//details.hintedBreasts = true; //TODO - yes or not? 
				add(`translucent ${shirt} with nothing underneath`, 4);
			}else if( nipplesVisible("upper") ){
				details.nipples = true;
				add(`obviously no bra`, 2);
				if(wears.piercedNipples) add(`pierced nipples`, 1);
			}else{
				add(`no bra`, 1);
			}
		}
	
	//bra	
	}else if(subject.bra){
		add(`only in bra`, 6);
		details.bra = true; //TODO
		details.braOnly = true; //TODO
		
		details.bareUpperChest = true;
		if(subject.bra.botStrapCoverage && subject.bra.botStrapCoverage > 1.75){
			details.semiBareLowerChest = true;
		}else{
			details.bareLowerChest = true;
		}
		
	//nothing
	}else{
		details.topless = true;
		details.breasts = true;
		details.bareUpperChest = true;
		details.bareLowerChest = true;
	}
		
		
		
		

//SLEEVES (MAX 2)
	if(details.topless || details.braOnly){
		//counted in topless
		details.bareArms = true; 
		details.bareShoulders = true;
	}else{		
		
		let bareArms = true;
		let bareShoulders = true;
		let shortSleeves = true;
		let miniSleeves = true;
		let transparentSleeves = true;
		
	
		//upperOuter
		if(upperOuter && upperOuter.armCoverage){
			bareArms = false;
			bareShoulders = false; //TODO - upperOuter with detached sleeves
			if(upperOuter.armCoverage >= shortSleevesArmCoverage){
				miniSleeves = false;
				shortSleeves = false;
				transparentSleeves = false; 
			}else if(upperOuter.armCoverage >= miniSleevesArmCoverage){
				miniSleeves = false;
				transparentSleeves = false; 
			} 
		}else{
			//no upperOuter or is corset
		}
		
		//upper
		if(upper){
			bareArms = false;
			
			if(upper.armCoverage){
							
				if(upper.shoulderDetach){
					//detached sleeves
				}else if(upper.shoulderCoverage < 0.15){ //TODO - STUPID RANDOM CONSTANT MAGIC NUMBER
					//old detached sleeves
					//new shoulder window is huge
				}else{
					bareShoulders = false;
				}
			
				if(
					(
						!ether(upper) 
					) //no fishnet/transparent tee
					&&
					(
						upper.armInnerCoverage === undefined || upper.armInnerCoverage > upper.armCoverage || !hiEther(upper) 
					) //no fishnet sleeves
				){
					transparentSleeves = false;
					//TODO TEST
				}
				
				if(upper.armCoverage >= shortSleevesArmCoverage){
					shortSleeves = false;
					miniSleeves = false;
				}else if(upper.armCoverage >= miniSleevesArmCoverage){
					shortSleeves = true;
					miniSleeves = false;
				}else{
					shortSleeves = true;
					miniSleeves = true;
				}
				
				
				
			}else{ //no arm coverage!  - top or corset
				transparentSleeves = false;
				shortSleeves = false;
				miniSleeves = false;
			}
		}
		
		//writes findings
		if(bareArms){
			add("bare arms ", 2);
	/*
		}else if(bareShoulders && miniSleeves){
			bareArms = true; //sleeves so short they are basically bare
			add("bare arms ", 2);
	*/
		}else if(transparentSleeves){
			add("transparent sleeves ", 1);
		}else if(bareShoulders){
			add("bare shoulders ", 1);
		}else if(shortSleeves){
			add("short sleeves ", 1);
		}
		
		details.bareArms = bareArms;
		details.bareShoulders = bareShoulders;
		details.shortSleeves = shortSleeves;
		details.miniSleeves = miniSleeves;
		details.transparentSleeves = transparentSleeves;
		
		
	}
	
	
	
	
	

	
	
//LOWER 
	let legCoverage = -1;  //TODO - doesnt handle Outer & Inner things; shouldnt for pants; should for socks
	let lower;
	if(subject.lower?.legCoverage){
		lower = subject.lower;
		legCoverage = subject.lower.legCoverage;
	}else if(wears.dress && upper.legCoverage){
		lower = subject.upper;
		legCoverage = upper.legCoverage;
	}
	
	if(lower){
			details.transparentLower  = ether(lower);
	}
	

//PANTS 
	if(wears.pants){
		if(legCoverage < groinLegCoverage){ //14
			add(`hotpants`, 4);
		}else if(legCoverage < 0.30){ 
			add(`shorts`, 3);
		}else if(legCoverage < shortSkirtLegCoverage){
			add(`long shorts`, 2);
		}else if(legCoverage < mediumSkirtLegCoverage){
			add(`capri`, 1);
		}else{
			//normal pants
		}
	
		
		if(subject.lower.outerLoose < 0.01 && subject.lower.innerLoose < 0.01){
			add(`very tight ${skirt}`, 2);
		}else if(subject.lower.outerLoose < 0.36 && subject.lower.innerLoose < 0.5){
			add(`tight ${skirt}`, 1);
		}
				
	//SKIRT OR DRESS (MAX 6)
	}else if(wears.skirt){
	
	
		if(legCoverage < 0.08){ //TODO glimpseLegCoverage??
			//basically no skirt
		//TODO - BOOSTED BY EXPOSED PANTIES??
		}else if(legCoverage < groinLegCoverage){ //14
			add(`${skirt} not hiding anything`,5);
	
		}else if(legCoverage < microSkirtLegCoverage){
			add(`micro${skirt}`,4);
		}else if(legCoverage < miniSkirtLegCoverage){
			add(`mini${skirt}`,3);
		}else if(legCoverage < shortSkirtLegCoverage){
			add(`short ${skirt}`,2);
		}else if(legCoverage < mediumSkirtLegCoverage){
			add(`midi ${skirt}`,1);
		}else{
			//long skirt
		}
		
		
		const lose = subject.lower ? subject.lower.outerLoose : upper.legLoose;


		if(legCoverage < groinLegCoverage){
			//nothing
		}else if(legCoverage < 0.30 && lose > 1.3){
			add(`one windblow from pantyshot`, 1);
			details.windblow = true;
		}else{
			if(lose < 0.01){
				add(`very tight ${skirt}`, 2);
			}else if(lose < 0.36){
				add(`tight ${skirt}`, 1);
			}
		}
		
		
		
	}
	
	
	// pelvis exposed
	if(lower){
		if(legCoverage <= 0.14){ //todo magic adjust
			details.barePelvis = true;
		}else if(legCoverage <= 0.20){ //todo magic 
			details.semiBarePelvis = true;
		}
	}
	
	
	
/*
	//LONG TOP	
		}else if(upper && subject.){
			//add("only long shirt",4);

	//NOTHING
		}else{
			//panties_bottom = true;
			//add("no trousers",5);
		};
*/	
	
	/*
	let waistCoverage = -10;
	let waistCoverageWhat = "";
	if(upper && upper.waistCoverage){
		waistCoverage = upper.waistCoverage;
		waistCoverageWhat = shirt;
	};
	if(upperOuter){
		if(vest === "sweater" || vest === "corset" || vest === "apron"){ //only ones with waistCoverage
			if(upperOuter.waistCoverage > waistCoverage){
				waistCoverage = upperOuter.waistCoverage;
				waistCoverageWhat = vest;
			}
		}
	};
	*/
	


//calculates what covers the GENITAL AREA, overly complicated 
	
	function genitalCoverage(waistLimit, legLimit){
		let what = ""; //garment on topmost level
		let level = 3;
		/*
			0 covered
			1 tran_1
			2 tran_2
			3 exposed
		*/
		
		function checkLayers(item, name){
			if( fish(item) ){
				if(level === 3){ //TODO
					level = 2;
					what = name;
				}else if(level === 2){
					level = 1;
					what += `, ${name}`;
				}
				
			}else if(item.alpha < transparent_2){
				if(level === 3){
					level = 2;
					what = name;
				}else if(level === 2){
					level = 1; //two see-throug == translucent 
					what += `, ${name}`;
				}
			}else if(item.alpha < transparent_1){
				if(level > 1){
					level = 1;
					what = name;
				}else if(level === 1){
					level = 0; //two see-through == covered
					what += `, ${name}`;
				}
			}else{
				level = 0;
				what = name;
			} 
		}
				
				
		//pantyhose
		if(wears.pantyhose && !wears.sexyPantyhose){
			checkLayers(socks, stockings);
		}
		
		//lowerInner
		if(subject.lowerInner){
			if(subject.lowerInner.innerLoose < 1 || subject.lowerInner.legCoverage > legLimit){ //pants or skirt covering the area
				checkLayers(subject.lowerInner, name);
			}
		}
		
		//lower
		if(subject.lower){
			if(subject.lower.innerLoose < 1 || subject.lower.legCoverage > legLimit){ //pants or skirt covering the area
				checkLayers(subject.lower, skirt);
			}
		}
		
		
		//BEWARE - waist coverage goes up; leg coverage goes down
		if(upper){
			if(
					(upper.legCoverage && upper.legCoverage !== 1 /*TODO!*/ && upper.legCoverage > legLimit) 
				||	(upper.waistCoverage && upper.waistCoverage < waistLimit)
			){
				checkLayers(upper, shirt);
			}
		}
		
		if(upperOuter){
			if(vest === "sweater" || vest === "corset"){
				if(
						(upperOuter.legCoverage && upperOuter.legCoverage !== 1 /*TODO!*/ && upperOuter.legCoverage > legLimit)
					||	(upperOuter.waistCoverage && upperOuter.waistCoverage < waistLimit)
				){
					checkLayers(upperOuter, vest);
				}
			}
		}
		
		return {
			what,
			level,
		};
	}
	
	
	/*
		0 covered
		1 tran_1
		2 tran_2
		3 exposed
	*/
	const glimpse = genitalCoverage(groinWaistCoverage, groinLegCoverage);
	const exposed = genitalCoverage(glimpseWaistCoverage, glimpseLegCoverage);	




//console.log(exposed);
//console.log(glimpse);

	
	
	/*
		fully exposed
		any see-through
		gimpse exposed
		any translucent
		barely covered
	*/
	
	//TODO
//	const sexyPanties = ["thong","g-string"].includes(panties) ? true : false;
//	const crotchlessPanties = false; //TODO
	details.glimpse = true;

	//fully exposed
	if(exposed.level === 3){
		if(!subject.panties){
			details.pussy = true;
			details.noPanties = true;
			if(!subject.lower){
				details.bottomless = true; //13  + 3  10 + 4 14
			}else{
				add(`genitals fully exposed`, 10);
			}
		}else if(wears.crotchlessPanties){
			add(`${knickers} exposed`, 9);
		}else if(wears.sexyPanties){
			add(`${knickers} exposed`, 8);
		}else{
			add(`${knickers} exposed`, 7);
		}
	//see through
	}else if(exposed.level === 2 || glimpse.level === 2){
		const what = exposed.level === 2 ? exposed.what : glimpse.what; //TODO - or opposite order 
		if(!subject.panties){
			details.noPanties = true;
			details.hintedPussy = true;
			add(`see-through ${what} with nothing under`, 8);
		}else if(wears.crotchlessPanties){
			add(`see-through ${what} and ${knickers}`, 7);
		}else if(wears.sexyPanties){
			add(`see-through ${what} and ${knickers}`, 6);
		}else{
			add(`see-through ${what}`, 5);
		}
	//glimpse
	}else if(glimpse.level === 3){
		if(!subject.panties){
			details.pussy = true; //TODO - yes or not? 
			details.noPanties = true; 
			add(`glimpse of genitals`, 6);
		}else if(wears.crotchlessPanties){
			add(`glimpse of ${knickers}`, 5);
		}else if(wears.sexyPanties){
			add(`glimpse of ${knickers}`, 4);
		}else{
			add(`glimpse of ${knickers}`, 3);
		}
		
	//translucent
	}else if(exposed.level === 1 || glimpse.level === 1){
		const what = exposed.level === 1 ? exposed.what : glimpse.what; //TODO - or opposite order 
		if(!subject.panties){
			details.noPanties = true;
			//details.hintedPussy = true; //TODO?
			add(`translucent ${what} with nothing under`, 5);
		}else if(wears.crotchlessPanties){
			add(`translucent ${what} and ${knickers}`, 4);
		}else if(wears.sexyPanties){
			add(`translucent ${what} and ${knickers}`, 3);
		}else{
			add(`translucent ${what}`, 2);
		}
	//barely covered
	}else{
		//don't mention 
		details.glimpse = false;
	}
	
	
	//TODO TOP OF PANTYHOSE 
	//no pants //TODO
	if(!details.bottomless && !subject.lower && !wears.dress){
		if(wears.pantyhose){ 
			add(`only ${stockings}`, 4);  //todo - 3 or 4? should be more without bonus for panties
		}else{
			add(`no pants`, 4); //todo - 3?
		}
	}
	
	
	let waistCoverage = -10;
	if(upper && upper.waistCoverage){
		waistCoverage = upper.waistCoverage;
	}else if(upperOuter){
		if(vest === "sweater" || vest === "corset" || vest === "apron"){
			waistCoverage = upperOuter.waistCoverage > waistCoverage ? upperOuter.waistCoverage : waistCoverage;
		}
	}
	
	
	
	//MIDRIFF
	{
		//TODO -tempfix 
		if(upper?.dad === "BotOpenTop"){
			add(`boob window`, 2); //TODO ??
		}
				
				
		// waistCoverage is 1 high above waist; -1 mid-thighs
		const upperWaistCoverage = (()=>{
			let min = Infinity;
			function check(item){
				if(item?.waistCoverage < min && !ether(item) ) min = item.waistCoverage;
			}
			
			if(vest === "jaket"){ //doestn't count
			}else if(vest === "apron"){ //covers everyting
				min = -2;
			}else{
				check(subject.upperOuter);
			}
			check(upper);
			check(subject.upperInner);
			return min;
		})();
		
		const lowerWaistCoverage = (()=>{
			let max = -Infinity;
			function check(item){
				if(item?.waistCoverage > max && !ether(item) ) max = item.waistCoverage;
			}
			
			check(subject.lowerOuter);
			check(subject.lower);
			check(subject.lowerInner);
			if(wears.pantyhose) check(socks);
			check(subject.panties);
			
			return max;
		})();
			
/*			
console.warn(upperWaistCoverage)
console.warn(lowerWaistCoverage)
*/
		if(upperWaistCoverage !== Infinity && lowerWaistCoverage !== -Infinity){ //i.e. when both are valid numbers 
			if(upperWaistCoverage < lowerWaistCoverage){
				//there is no space between shirt and pants
			}else{
				let difference;
				//values under zero are more prominent (hip - tight covers double area of waist - hip)
				//so the difference value bellow zero is doubled 
				const modifier = 2;
				
				if(upperWaistCoverage > 0 && lowerWaistCoverage >= 0){ 
					difference = upperWaistCoverage - lowerWaistCoverage;
				}else if(upperWaistCoverage > 0){
					difference = upperWaistCoverage + Math.abs(lowerWaistCoverage * modifier);
				}else{
					difference = Math.abs(lowerWaistCoverage - upperWaistCoverage) * modifier;
				}
//console.warn(difference)		
				if(difference > 0.9){ //TODO 0.8
					add("bare midriff", 3);
					details.midriff = true;
					details.bareMidriff = true;
				}else if(difference > 0.45){ //TODO 0.5
					add("exposed midriff", 2);
					details.midriff = true;
					details.semiBareMidriff = true;
				}else if(difference > 0.1){
					add("glimpse of midriff",1);
					details.midriff = true;
				}

				
				//lower chest coverage		//TODO - test & adjust
				if(upper.dad === "TriangleTop" && upper.botTriangle){
					if(upper.botTriangleCoverage > 0.5){
						//nothing
					}else{
						details.semiBareLowerChest = true;
					}
				}else if(upperWaistCoverage > 1.3){ //magic number
					details.bareLowerChest = true;
				}else if(upperWaistCoverage > 1){
					details.semiBareLowerChest = true;
				}else if(upper.dad === "BotOpenTop"){
					details.semiBareLowerChest = true;
				}
				
				//low cut pants
				if(lowerWaistCoverage < 0){ //TODO adjust, 0.1 would work too? 
					details.lowLower = true;
				}		
				
			}
		}
	
	
		//TOP OF PANTIES ABOVE PANTS WAISTLINE
		if(subject.panties?.waistCoverage !== undefined){
			const lowerWaistCoverage = (()=>{ //calulated again; this time without panties 
				let max = -Infinity;
				function check(item){
					if(item?.waistCoverage > max && !ether(item) ) max = item.waistCoverage;
				}
				
				check(subject.lowerOuter);
				check(subject.lower);
				check(subject.lowerInner);
				if(wears.pantyhose) check(socks);
				//check(subject.panties);	
				return max;
			})();

			const difference_mod = 0.1; //to panties be really visible
			if(lowerWaistCoverage !== -Infinity){ //panties are covered by something else
//console.log(subject.panties.waistCoverage);
//console.log(subject.panties.waistCoverage - difference_mod);
//console.log(lowerWaistCoverage);
				if(subject.panties.waistCoverage - difference_mod > lowerWaistCoverage ){ //they are higher than something else

					if(upperWaistCoverage === Infinity || upperWaistCoverage > subject.panties.waistCoverage){ //and there are no upper clothes or not cover the panties
						details.pantiesAbove = true;
						add(`top of ${knickers} visible`, 1);
					}
				}
			}
		}
	}
	
	
	
	//SHOES & SOCK
	if(!subject.shoes && !socks) details.barefoot = true;
	
	//SOCKS
//console.log(socks);
	if(socks){

/*
console.log("socks");
console.log(stockings);
console.log(legCoverage);
*/
		const check_space_above = function(){
			//space between skirt and socks
			details.socksTop = true; //TODO ASAP		
			if(
					socks.legCoverage < 0.5 //socks above knees
					&& wears.miniSkirt //wears miniskirt 
					&& legCoverage > groinLegCoverage //panties not exposed
					//TODO - no fishnet? 
					&& (socks.legCoverage >= legCoverage + 0.03) //big enough
					&& (socks.legCoverage <= legCoverage + 0.10) //but not too big
			){
					add("zettai ryouiki",1);
					details.zettai = true;
			}
		}

		//legCoverage: 1 ankle; 0 almost waist
		if(legCoverage > highSocksLegCoverage){
			//covered by trousers, no details could be guessed
		}else{
			//high socks
			if(wears.highSocks){
				if(subject.shoes?.legCoverage < socks.legCoverage){
					//covered by boots
				}else{
					add("high socks",1);
					check_space_above();
				}
			//stockings
			}else if(wears.stockings){
				//if skirt is too long it is not obvious whether she's wearing pantyhose or stockings
				const visibilityLimit = socks.legCoverage - (socks.bandWidth / 200) - 0.02; //0.02 random bonus
				
				if(subject.shoes?.legCoverage < socks.legCoverage){
					//covered by boots
				}else if(visibilityLimit > legCoverage){
					if( fish(socks)  ){
						add("fishnet stockings", 3);
					}else{
						add("stockings", 2);
					}
					check_space_above();
				}else{
					if( fish(socks) ){
						add("fishnet legwear", 2);
					}else{
						add("legwear", 1);
					}
				}
			
			//pantyhose
			}else if(wears.pantyhose){
				if(wears.sexyPantyhose){
					//let maxLegCoverage = 0.33 - ( 2 * (1 - socks.openHeight) );
			/*
			console.warn("slex");
			console.warn(legCoverage);
			console.warn(socks.openHeight);
			console.warn(maxLegCoverage);
			*/
					//if(legCoverage < maxLegCoverage){
					if(legCoverage < 0.3 ){ //TODO - JUST A WILD QUESS
						if( fish(socks) ){
							add("fishnet sexy pantyhose", 4);
						}else{
							add("sexy pantyhose", 3); //TODO 3????
						}
					}else{
						if( fish(socks) ){
							add("fishnet legwear", 2);
						}else{
							add("legwear", 1);
						}
					}
				}else{
					if( fish(socks) ){
						add("fishnet pantyhose", 2);
					}else{
						add("pantyhose", 1);
					}
				}
			}
			
		}
		
		
		
		
	
	
	}
	
	//SHOES
	if(subject.shoes){
	//TODO ASAP - HEELED BOOTS 
		//heels
		if(subject.shoes.shoeHeight){
			if( subject.shoes.shoeHeight > 3){
				add("very high heels", 2);
			}else if( subject.shoes.shoeHeight > 0.5){ //TODO - set constant limits for what are high heels, etc.
				add("high heels", 1);
			}
		}
		//boots - also compensates for socks 
		if(subject.shoes.legCoverage < 0.64){ //TODO 
			add("high boots", 2);
		}else if(subject.shoes.legCoverage < 0.85){
			add("boots", 1);
		}
	}
	
	
	
	//NUDITY
	if(details.topless && details.bottomless){
		add("nude", 20);
		details.nude = true;
	}else{
		if(details.topless){
			add("topless", 8);
		}else if(details.bottomless){
			//4 legs
			add("bottomless", 14);
		}
		if(details.barefoot){
			add("barefoot", 1);
		}
	}
	
	
	
	//TATTOOS
	{
	//	console.warn("TATTOO:");
		
		const slutat = function(slot){
			const code = PC?.apex?.[`tat_${slot}`];
			if(code === undefined) return false; 
			return tattoos_slutty.includes( code.split(";")[0] );
		}
		
		let visible = 0;
		let hinted = 0;
		const visible_list = [];
		const hinted_list = [];
		
		//UPPER CHEST
		if( slutat("chest")  ){
			if(details.bareUpperChest){
				visible++;
				visible_list.push("upper chest")
			}else if(details.semiBareUpperChest){
				hinted++;
				hinted_list.push("upper chest")
			}
		}
		
		//LOWER CHEST
		if( slutat("underBreasts")  ){
			if(details.bareLowerChest){
				visible++;
				visible_list.push("lower chest")
			}else if(details.semiBareLowerChest){
				hinted++;
				hinted_list.push("lower chest")
			}
		}
		
		//LOWER BELLY
		if( slutat("lowerBelly")  ){
			if(details.bottomless){
				visible++;
				visible_list.push("lower belly")
			}else if(details.lowLower){
				hinted++;
				hinted_list.push("lower belly")
			}
		}
		
		//THIGH
		if( slutat("thigh") || slutat("thighLeft")  ){
			if(details.barePelvis || details.bottomless){
				visible++;
				visible_list.push("thigh")
			}else if(details.semiBarePelvis){
				hinted++;
				hinted_list.push("thigh")
			}
		}
		
		//SIDE
		if( slutat("sideLeft") ||  slutat("side") ||  slutat("hipSmall")  ||  slutat("hipSmallLeft")  ){
			if(details.bareMidriff || details.bottomless){
				visible++;
				visible_list.push("side")
			}else if(details.semiBareMidriff){
				hinted++;
				hinted_list.push("side")
			}
		}
		
		//SHOULDER TATTOO 
		if( slutat("upperArm") || slutat("upperArmLeft")  ){
			if(details.bareArms || details.bareShoulders){
				visible++;
				visible_list.push("shoulder")
			}else if(details.transparentSleeves || details.miniSleeves){
				hinted++;
				hinted_list.push("shoulder")
			}
		}
		
		/*
		console.warn(visible);
		console.warn(hinted);
		console.warn(visible_list);
		console.warn(hinted_list);
		*/
		
		const hinted_text = `(${hinted_list.join(", ")})`;
		const visible_text = `(${visible_list.join(", ")})`;
		details.sluttyTattooList = visible_list;
		details.hintedSluttyTattooList = hinted_list;
		
		if(visible > 1){
			details.sluttyTattoo = true;
			add(`slutty tattoos ${visible_text}`,  4);
		}else if(visible > 0){
			details.sluttyTattoo = true;
			add(`slutty tattoo ${visible_text}`,  3);
		}
		
		if(hinted > 1){
			details.hintedSluttyTattoo = true;
			add(`hints of slutty tattoos ${hinted_text}`,  visible ? 0 : 2);
		}else if(hinted > 0){
			details.hintedSluttyTattoo = true;
			add(`hint of slutty tattoo ${hinted_text}`,  visible ? 0 : 1);
		}
	}
	
	
	
	
	//BODY
	if(wears.hugeTits){
		add("huge breasts", 2);
	}else if(wears.bigTits){
		add("big breasts", 1);
	}else if(wears.mediumTits){
		//not worthy mentioning
	}else{
		details.titfall = false; //counts only with big tits
	}
	//TODO - only without bra?
	
	if(details.cleavage) details.nipslip = true;
	if(subject.bra) details.nipslip = false; //count only without bra
	
	
//QUALITY
	//TODO TODO!!!!
	const {quality, qualityDesc} = checkQuality(subject);
	details.quality = quality;
	details.qualityDesc = qualityDesc;
	desc += `<hr>quality: ${qualityDesc}`;
	


//DESCRIPTION 
	desc += `<hr><strong>= ${value} = `;
	
	if(quality < 0){
		desc += `cheap `;
	}else if(quality > 0){
		desc += `classy `;
	}
	
	//if(value <= 4){ TODO - 3 feels better? but 4 is system
	if(value <= 3){
		desc += "prude";
		valueDesc = "prude";
		details.slut = 0;
	}else if(value <= 9){
		desc += "beauty";
		details.slut = 1;
		valueDesc = "beauty";
	}else if(value <= 15){
		desc += "tease";
		details.slut = 2;
		valueDesc = "tease";
	}else if(value <= 22){
		desc += "slut";
		details.slut = 3;
		valueDesc = "slut";
	}else if(value <= 30){ //TODO SLUT5
		desc += "whore";
		details.slut = 4;
		valueDesc = "whore";
	}else{
		desc += "cumdump";
		details.slut = 5;
		valueDesc = "cumdump";
	}

//TODO CUMDUP!!FUCKTOY

	/*
	if(value === 0){
		desc += "virgin";
		details.slut = 0;
	}else if(value <= 5){
		desc += "bashful";
		details.slut = 1;
	}else if(value <= 10){
		desc += "sexy";
		details.slut = 2;
	}else if(value <= 15){
		desc += "shameless";
		details.slut = 3;
	}else if(value <= 20){
		desc += "slutty";
		details.slut = 4;
	}else{
		desc += "whore";
		details.slut = 5;
	}
	*/
	desc += "</strong>";
	
	desc += "</ul>";
	
	
	




	if(details.glimpse){
		details.panties = !!subject.panties;
		details.pussy = !subject.panties;
	}
	
	if(details.pantiesAbove){
		details.panties = true;
	}else if(details.panties){
		details.pantiesBelow = true
	}

	if(details.pussy) details.hintedPussy = true;
	
	
	if(details.pantiesAbove && wears.sexyPanties) details.whaletail = true;
	
	

	
	const  output = {
		value,
		valueDesc,
		desc,
		details,
		quality,
		qualityDesc,
	};
	
	if(debug) console.log(details);
	
	return output;
		
};


/*
TODO - complex code for dual shirts - they look fucking cool
unbuttoned
+ pont to tight
*/