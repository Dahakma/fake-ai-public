/**
	garmentIs. and wears. 
	function deciding the exact type of pieces of clothes (e.g miniskirt)
	(used to decide whether the character breaks clothing rules)
*/

import {
	british,
	
	transparent_1,
	transparent_2,
	
	groinLegCoverage,

	/*
	groinWaistCoverage,
	glimpseLegCoverage,
	glimpseWaistCoverage,
	*/

	stockingsLegCoverage,
	highSocksLegCoverage, 
	
	microSkirtLegCoverage,
	miniSkirtLegCoverage, 
	shortSkirtLegCoverage,
	mediumSkirtLegCoverage,
	
	mediumBreasts,
	bigBreasts,
	hugeBreasts, 
	
	//shortSleevesArmCoverage, 
} from "./constants";

import {garment} from "Data/items/blueprints/index";
import {allSlots, allPiercings} from "./all";


export const garmentIs = {
//ACCESSORY
	collar(item){
		return item?.dad === "wCollar" || item?.dad === "Collar";  //TODO
	},

	choker(item){
		return item?.slot === "collar" && !this.collar(item);
	},
	
//UPPER
	bodysuit(item){ //TODO
		return ["wCatsuit"].includes(item?.dad) || false;
	},
	
	dress(item){
		return item?.slot === "upperLower"; //TODO - works atm but won't work with leotard etc.
	},
	
	minidress(item){
//TODO - shouldn't be miniskirt + dress?
		return ["minidress"].includes(item?.subtype);
	},
	
	sexyTop(item){
//TODO - what do do with dresses????
		return [
			"TubeTop", //TODO - inlcudes longTubeTop
			"HalterTop", //TODO - includes longHalterTop
			"BotOpenTop",
			"TriangleTop",
			"ShrugTop", //??
			"wTubeTop", //legacy
			"wHalterTop", //legacy
		].includes(item?.dad) || [
			"deepTee",
			"sheerTee",		
			"sluttyTee",
			"niceSluttyTee",
			"fishnetTee", //??
			"sluttyDualTee",//legacy
			"fishnetDualTee", //legacy
			
			"designTubeTop",
			"designSexyTubeTop",
			"designHalterTop",
			"designSexyHalterTop",
			//"designTee",
			"designSexyTee",

		].includes(item?.blueprint) || item?.subtype === "sexyTee" || this.minidress(item) || false;
	},
	
	
	
	
	tee(item){
	//TODO
		return ["Tee","wDualTee","wTee"].includes(item?.dad);
	},
	
	
//LOWER
	skirt(item){
		return item?.innerLoose >= 1 || this.dress(item);
	},
	
	nanoSkirt(item){
		return item?.legCoverage < groinLegCoverage;
	},
	
	microSkirt(item){
		return item?.legCoverage < microSkirtLegCoverage;
	},
	
	miniSkirt(item){
		return item?.legCoverage < miniSkirtLegCoverage;
	},
	
	shortSkirt(item){
		return item?.legCoverage < shortSkirtLegCoverage;
	},
	
	mediumSkirt(item){
		return item?.legCoverage < mediumSkirtLegCoverage;
	},
	
	longSkirt(item){
		return this.skirt(item) && !this.mediumSkirt(item);
	},
	
	pants(item){
		return item?.innerLoose < 1;
	},

	shorts(item){
		return this.pants(item) && item?.legCoverage < shortSkirtLegCoverage;
	},
	

	
//LEGWEAR
	pantyhose(item){
		if(item?.dad === "wPantyhose") return true; //normal pantyhose
		if(item?.dad === "wSuperSocks" && !item.lockGroin && item.legCoverage < groinLegCoverage); //TODO - untested
	},
	
	stockings(item){		
		return (item?.legCoverage < stockingsLegCoverage || item?.dad === "wStockingsGarter" || item?.dad === "StockingsGarter" || item?.subtype === "stockings"); //TODO - untested
	},
	
	fishnetLegwear(item){
//TODO
		return garment?.[item?.blueprint]?.adjective === "fishnet"; //TODO
	},
	
	highSocks(item){
		return item?.legCoverage >= stockingsLegCoverage && item?.legCoverage < highSocksLegCoverage;
	},
	
	sexyPantyhose(item){
		return this.pantyhose(item) && item?.openHeight && item?.openWidth;
	},
	
	socks(item){
		return item && !this.highSocks(item) && !this.stockings(item) && !this.pantyhose(item);
	},
	
	sexyLegwear(item){
		//TODO - all pantyhose considered sex 
		return 	this.pantyhose(item) || this.stockings(item) || this.highSocks(item);
	},
	
	sluttyLegwear(item){
		if(this.sexyPantyhose(item) ) return true;
		return this.fishnetLegwear(item);
	},
	
//PANTIES
	nicePanties(item){
		return ["hipsters","thong","gString"].includes(item?.subtype);
	},

	sexyPanties(item){
		return ["thong","gString","crotchless"].includes(item?.subtype);
	},
	
	crotchlessPanties(item){
		return item?.openCrotch; //TODO - check if works and covers everything
		//return ["wCrotchlessPanties"].includes(item?.dad);
	},
	
//BRA
	niceBra(item){
		return (item?.dad === "CageBra" && (item?.cage || item?.ruffling) ) || (item?.dad === "PushUpBra" && item?.ruffling);
	},
	
//TODO wacky stop-gap
	fishnet(item){
		if(item?.fishness > 0.8) return true;
		//TODO - add check for texture 
		if( item?.name && item.name.includes("fishnet") ) return true; //could be not a real garment but a simple dummy without name
	},
	
	
	trasparent(item){
		return item?.alpha < transparent_2;
	},
	
	
	translucent(item){
		return item?.alpha < transparent_1;
	},
	
	//TODO 
	highHeels(item){
		return item?.shoeHeight >= 1.2; //BEWARE - works now but only with current shoes
	},
	
	heels(item){
		return item?.shoeHeight >= 0.2; //BEWARE - works now but only with current shoes
	},
	
	boots(item){
		return item?.legCoverage < 0.9;
	},
	
};




export const subjectWears = {
//ACCESSORY
	collar(subject){
		return garmentIs.collar(subject.collar);
	},
	
	choker(subject){
		return garmentIs.choker(subject.collar);
	},
	
//UPPER
	bodysuit(subject){
		return garmentIs.bodysuit(subject.upper);
	},
	
	dress(subject){
		return garmentIs.dress(subject.upper);
	},
	
	minidress(subject){
		return garmentIs.minidress(subject.upper);
	},
	
  //UPPER
	sexyTop(subject){
		//TODO
		return garmentIs.sexyTop(subject.upper);
	},
	
	tee(subject){
		//TODO
		return garmentIs.tee(subject.upper);
	},	
	
//LOWER
	skirt(subject){
		return garmentIs.skirt(subject.lower) || garmentIs.dress(subject.upper);
	},
	
	nanoSkirt(subject){
		return garmentIs.nanoSkirt(subject.lower) || ( garmentIs.dress(subject.upper) && garmentIs.nanoSkirt(subject.upper) );
	},
	
	microSkirt(subject){
		return garmentIs.microSkirt(subject.lower) || ( garmentIs.dress(subject.upper) && garmentIs.microSkirt(subject.upper) );
	},
	
	miniSkirt(subject){
		return garmentIs.miniSkirt(subject.lower) || ( garmentIs.dress(subject.upper) && garmentIs.miniSkirt(subject.upper) );
	},
	
	shortSkirt(subject){
		return garmentIs.shortSkirt(subject.lower) || ( garmentIs.dress(subject.upper) && garmentIs.shortSkirt(subject.upper) );
	},
	
	mediumSkirt(subject){
		return garmentIs.mediumSkirt(subject.lower) || ( garmentIs.dress(subject.upper) && garmentIs.mediumSkirt(subject.upper) );
	},
	
	longSkirt(subject){
		return garmentIs.longSkirt(subject.lower) || ( garmentIs.dress(subject.upper) && garmentIs.longSkirt(subject.upper) );
	},
	
	pants(subject){
		return garmentIs.pants(subject.lower);
	},
	
	shorts(subject){
		return garmentIs.shorts(subject.lower);
	},
	
//LEGWEAR
	pantyhose(subject){
		return garmentIs.pantyhose(subject.socks);
	},
	
	stockings(subject){
		return garmentIs.stockings(subject.socks);
	},
	
	fishnetLegwear(subject){
		return garmentIs.fishnetLegwear(subject.socks);
	},
	
	highSocks(subject){
		return garmentIs.highSocks(subject.socks);
	},
	
	sexyPantyhose(subject){
		return garmentIs.sexyPantyhose(subject.socks);
	},
	
	sexyLegwear(subject){
		//TODO - all pantyhose considered sexy 
		return garmentIs.sexyLegwear(subject.socks);
	},
	
	sluttyLegwear(subject){
		return garmentIs.sluttyLegwear(subject.socks);
	},
	
	socks(subject){
		return garmentIs.socks(subject.socks);
	},
	
//PANTIES
	panties(subject){
		return !!subject?.panties;
	},
	
	sexyPanties(subject){
		return garmentIs.sexyPanties(subject.panties);
	},

	crotchlessPanties(subject){
		return garmentIs.crotchlessPanties(subject.panties);
	},
	
	
//BRA
	niceBra(subject){
		return garmentIs.niceBra(subject.bra);
	},
	
	bra(subject){
		return !!subject.bra;
	},
	
//BODY
	tits(subject){
		return subject.getDim("breastSize") > 0;
	},
	
	smallTits(subject){
		return subject.getDim("breastSize") <= mediumBreasts;
	},
	
	
	bigTits(subject){
		return subject.getDim("breastSize") > bigBreasts;
	},

	hugeTits(subject){
		return subject.getDim("breastSize") > hugeBreasts;
	},
	



	trimmedPussy(subject){ //TODO
		return (
			subject.getDim("pubicLength") > 0
			&&
			subject.getDim("pubicWidthTop") < -3
		);
	},

	bushyPussy(subject){ //TODO
		return (
			subject.getDim("pubicLength") > 0
			&&
			subject.getDim("pubicWidthTop") > -2
		);
	},

	hairyPussy(subject){ //TODO
		return subject.getDim("pubicLength")  > 0;
	},
	
	shavedPussy(subject){
		return subject.getDim("pubicLength") <= 0;
	},
	


	piercedNipples(subject){
		return subject.nipples || subject.nipplesBot;
	},
	
	piercedNose(subject){
		return Boolean(subject.noseSide || subject.noseTop || subject.noseBot);
	},
	
	piercedLips(subject){
		return Boolean(subject.lipsTop || subject.lipsBot || subject.lipsSide);
	},
	
	piercedBrow(subject){
		return Boolean(subject.eyebrow);
	},
	
	piercedFace(subject){
		return Boolean(this.piercedNose(subject) || this.piercedLips(subject) || this.piercedBrow(subject) );
	},
	
	piercedGenitals(subject){
		return  Boolean(subject.vch || subject.labia || subject.hch || subject.pubic);
	},
	
	
	
//TODO
	heavilyPiercedGenitals(subject){
		let count = 0;
		if(subject.vch) count++;
		if(subject.hch) count++;
		if(subject.labia) count++;
		if(subject.pubic) count++;
		return count > 2;
	},
	
	
	visiblePiercedNipples(subject){
		return subject.upper?.showPiercings || subject.upper?.showPiercingsTop || subject.upper?.showPiercingsBot;
	},
	
	visibleNipples(subject){
		return subject.upper?.showNipples === true;
	},		
			
	heavilyPiercedFace(subject){
		let count = 0;
		if(subject.noseSide) count++;
		if(subject.noseTop) count++;
		if(subject.noseBot) count++;
		if(subject.lipsTop) count++;
		if(subject.lipsBot) count++;
		if(subject.lipsSide) count++;
		if(subject.eyebrow) count++;
		return count > 2;
	},
	
	piercedNavel(subject){
		return Boolean(subject.bellybutton);
	},
	
	
	countPiercings(subject){
		return allPiercings.reduce( (acc,key) => acc + ( subject[key] ? 1 : 0 ), 0 );
	},
	
	
	highHeels(subject){
		return garmentIs.highHeels(subject.shoes);
	},
	
	heels(subject){
		return garmentIs.heels(subject.shoes);
	},
	
	boots(subject){
		return garmentIs.boots(subject.shoes);
	},
	
	
	glasses(subject){
		return Boolean(subject.glasses);
	},
/*
	facePierced(subject){
		//TODO!!!
	},
*/

	heavyMakeup(subject){
		//TODO
		if(mile.makeup) return true; //TODO - atm checks any makeup 
	},

	makeup(subject){
		//TODO
		if(mile.makeup) return true; //TODO - atm checks any makeup 
	},
	
};



export const wears = {};
Object.keys(subjectWears).forEach( a => {
	Object.defineProperty(wears, a, {
		get(){ return subjectWears[a](PC); },
	});
});