import * as DA from "Libraries/da";

import {draw} from "../canvas/draw";

import {big_base10_to_base64, big_base64_to_base10} from "Libraries/dh";
import {rg, ra} from "Libraries/random";
import {create} from "./create"
import {update, updateDraw} from "./update"
import {nipplesVisible} from "./appearance";
import {garment} from "Data/items/blueprints/index";
import {ids} from "Data/items/ids";
import {allSlots} from "./all";
import {unpackExtra} from "./createExtra";

//RECREATE
export function recreate(item, subject = PC){
	const extra = unpackExtra(item.extra, item.blueprint);
	return create(item.blueprint, item.seed, extra, subject);
}

//CREATE MULTIPLE
export function createMultiple(array, seed){
	rg.i(seed ?? ra.g);
	const output = [];
	for(const a of array){
		if(typeof a === "object"){ //in format [numer_of_items; "blueprint", extra]
			if(!a[1]) continue; //empty or undefined 
			for(let k = 0; k < a[0]; k++){
				output.push( create(a[1], rg.g, a[2]) );
			}	
		}else{ //in format "blueprint"
			if(!a) continue;
			output.push( create(a, rg.g) );
		}
	}
	return output.filter( a => a ); //removes undefined (failed to be created)
}


//WEAR
function wearActual(garment, subject = PC, inventory){
	if(!garment || !garment.slot) return;
	
	
	
	
	//multislot clothes remove the other 
	let slot = garment.slot; 
			
		//dress removes skirt
		if(garment.slot === "upperLower"){ 
			slot = "upper";
			if(subject.lower?.locked) return;
			removeActual("lower", subject, inventory);
		}else if(garment.slot === "upperLowerInner"){ 
			slot = "upperInner";
			if(subject.upperInner?.locked) return;
			removeActual("lowerInner", subject, inventory);
		}else if(garment.slot === "upperLowerOuter"){ 
			slot = "upperOuter";
			if(subject.upperOuter?.locked) return;
			removeActual("lowerOuter", subject, inventory);
		}
		
		//skirt removes dress
		if(garment.slot === "lower" && subject?.upper?.slot === "upperLower"){ 
			if(subject.upper?.locked) return;
			removeActual("upper", subject, inventory);
		}else if(garment.slot === "lowerInner" && subject?.upperInner?.slot === "upperLowerInner"){ 
			if(subject.upperInner?.locked) return;
			removeActual("upperInner", subject, inventory);
		}else if(garment.slot === "lowerOuter" && subject?.upperOuter?.slot === "upperLowerOuter"){ 
			if(subject.lowerOuter?.locked) return;
			removeActual("upperOuter", subject, inventory);
		}

	//removes item already occuping the slot
	if(subject[slot]?.locked) return; //locked items cannot be removed!
	removeActual(slot, subject, inventory)
	
	subject[slot] = garment;
	
	
	//removes it from inventory
	if( inventory !== undefined && Array.isArray(inventory) ){
		const index = inventory.findIndex( a => a?.seed === garment.seed && a?.blueprint === garment.blueprint);
		if(index !== -1) inventory.splice(index, 1);
	}
	
}
	
export function quickWear(garment, subject = PC, inventory){
	wearActual(garment, subject, inventory);
}
	
export function wear(garment, subject = PC, inventory){
	wearActual(garment, subject, inventory);
	updateDraw(subject);
}

export function wearEverything(array, subject = PC, inventory){
	for(const garment of array){
		wearActual(garment, subject, inventory);
	}
	update(subject); //BEWARE - does not draw
}



export function createWear(blueprint, seed, extra, subject = PC){
	const garment = create(blueprint, seed, extra);
	wear(garment, subject);
}



//REMOVE 
export function removeActual(garment, subject = PC, inventory){
	let slot;
	
	//it could be inputed garment or slot
	if(garment?.slot){
		slot = garment.slot; 
	}else{
		slot = garment;
	}
	
	if(subject[slot]?.locked) return; //locked items cannot be removed
	
	//dress always needs something extra
	if(slot === "upperLower"){
		slot = "upper";
	}else if(slot === "upperLowerInner"){
		slot = "upperInner";
	}else if(slot === "upperLowerOuter"){
		slot = "upperOuter";
	}
	
	//places removed item to inventory
	if(subject[slot] && inventory !== undefined && Array.isArray(inventory) ){
		inventory.push(subject[slot]);
	}
	
	subject[slot] = void 0;
}
	
	
export function remove(garment, subject = PC, inventory){
	removeActual(garment, subject, inventory);
	updateDraw(subject);
}

export function removeEverything(subject = PC, inventory){
	allSlots.forEach( a => {
		removeActual(a, subject, inventory);
	})
	
	update(subject); //TODO - reduntant, usually get dressed afterwards 
}



//PACK
/*
	items are transformed into number
	14444666666
	- 6 digits are the seed (item.seed) - procedural generation is based on it
	- 4 digits are ID (item.id) - each blueprint (type of clothes) has unique id (src/data/items/ids.js)
	- between 0 and n digits 
		- in case of consumables (ID < 100) it is their amount
		- in case of garments it could be item.extra - user-edited properties or color of the piece - packed in number
	the number is then futher compressed into number in base 64
*/

function packGarment(item){
	//either count (consumable) or extra properties
	const more = (()=>{
		if(item.id < 100) return `${item.count ?? 0}`;
		if(item.extra) return item.extra;
		return ``;
	})();
	const id = (`${item.id}`).padStart(4,0);
	const seed = (`${item.seed}`).padStart(6,0);
		
	return big_base10_to_base64(`${more}${id}${seed}`);
}

export function unpackGarmentIdSeed(code){
	code = big_base64_to_base10(code);

	const seed = +code.slice(-6);
	code = code.slice(0, -6);
	const id = +code.slice(-4);
	code = code.slice(0, -4);
	const more = code; //string!
	
	return {id, seed, more};
}

export function unpackGarment(code){
	const {id, seed, more} = unpackGarmentIdSeed(code);

	const blueprint = (()=>{
		return Object.keys(ids).find( a => ids[a] === id );
	})();

	const count = id < 100 ? +more : 1;
	const extra = id < 100 ? {} : unpackExtra(more, blueprint);
	
	const garment = create(blueprint, seed, extra); //when inserted non-existing blueprint, returns undefined
	if(garment) garment.count = count;
	return garment;
}

export function packEverything(array){
	const output = [];
	const potions = [];
	
	for(const a of array){
		output.push( packGarment(a) );
	}
	
	return output;
}

export function unpackEverything(array){
	const output = [];
	for(const a of array){
		output.push(  unpackGarment(a)  );
	}
	return output.filter(a => a); //remove broken undefined pieces
}




//OUTFITS
export function checkOutfit(id){
	const index = ext.outfits.findIndex( a => a.id === id );
	return index !== -1;
}	

export function outfitIndex(id){
	const index = ext.outfits.findIndex( a => a.id === id );
	return index;
}

/*
	id - string with id or array with packed items
*/

export function unpackOutfit(id){
	const index = outfitIndex(id);
	if(index === -1) return [];
	const items = ext.outfits[index].items;
	
	return items.map( item => unpackGarment(item) );
}	



export function wearOutfit(id, subject = PC, inventory = [], conjure = false, rerender = true){ //TODO - accesses ext
	if( !checkOutfit(id) ) return;
	removeEverything(subject, inventory); //TODO?
	const items = unpackOutfit(id);
	
	if(conjure){
		wearEverything(items, subject);
		if(rerender) draw(subject);
	}else{
		items.forEach( item => {
			if( inventory.find( b => item.seed === b?.seed && item.id === b?.id )  === undefined) return;
			wearActual(item, subject, inventory);
		})
		if(rerender){
			updateDraw(subject);
		}else{
			update(subject);
		}
	}
	
	
	
	
	/*
	let items = [];
	if(typeof id === "string"){
		const index = outfitIndex(id);
		if(index === -1) return;
		items = ext.outfits[index].items;
	}else if( Array.isArray(id) ){
		items = id;
	}
	
	
	
	if(conjure){
		for(const a of items){
			const garb = unpackGarment(a);
			wearActual(garb, subject); //TODO - inventory should not be necessary, has to be checked
		}
	}else{
		for(const a of items){
			const {seed, id} = unpackGarmentIdSeed(a);
			const garb = inventory.find( b => seed === b?.seed && id === ids[b?.blueprint] );
			if(garb !== undefined){
				wearActual(garb, subject, inventory);
			}
		}	
	}
	*/
	
}



export function wornOutfit(subject = PC){ //TODO - accesses ext
	return allSlots.filter( a => subject[a] ).map( b => packGarment(subject[b]) );
}


export function saveWornOutfit(id, name, subject = PC, reality = 0, randomly_offered = true){ //TODO - accesses ext
	const already_exists =  outfitIndex(id);
	if(already_exists !== -1) ext.outfits.splice(already_exists, 1);

	ext.outfits.push({
		id,
		name,
		reality,
		items: wornOutfit(subject),
		random: randomly_offered,
	});
}









export function makeupIndex(id){
	const index = ext.makeups.findIndex( a => a.id === id );
	return index;
}

export function saveMakeup(id, name, subject = PC, reality = 0, randomly_offered = true){ //TODO - accesses ext
	const already_exists =  makeupIndex(id);
	if(already_exists !== -1) ext.makeups.splice(already_exists, 1);

	const items = [
		subject.apex.irl_lips,
		subject.apex.irl_nails,
		subject.Mods.eyelashLength,
		subject.apex.irl_mascara_top,
		subject.apex.irl_mascara_bot,
	]
	
	//TODO - could be way simpler, but follows ext.outfits logic 
	ext.makeups.push({
		id,
		name,
		items,
		reality,
		random: randomly_offered,
	});
}

export function loadMakeup(id, subject = PC, rerender = true){ 
	const index = makeupIndex(id);
	if(index === -1) return;
	
	([
		subject.apex.irl_lips,
		subject.apex.irl_nails,
		subject.Mods.eyelashLength,
		subject.apex.irl_mascara_top,
		subject.apex.irl_mascara_bot,
	] = ext.makeups[index].items)
		
		
	if(rerender) updateDraw(subject);
}
	
export function deleteMakeup(id){ 
	const index = makeupIndex(id);
	if(index === -1) return;
	ext.makeups.splice(index, 1);
}










