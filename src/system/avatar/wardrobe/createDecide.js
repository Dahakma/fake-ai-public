import * as DA from "Libraries/da";
import {scaled, loadedTextures} from "Avatar/canvas/initiate";
import {rg} from "Libraries/random";
import {stringifyHSLA, randomColor, nameColor, complimentary, analogue, monochromatic} from "Libraries/color/index";
import {slotCategories} from "./all";
import {prices, modifiers, primary_attributes_prices, secondary_attributes_prices} from "Data/items/prices";





function getTexture(tex){
	if(tex === undefined) return undefined;
	
	let pattern = "";
	let patternSize = 100;
	
	//string
	if(typeof tex === "string"){
		pattern = tex;
		patternSize = rg.range(100, 200);
	//array 
	}else if( Array.isArray(tex) ){
		//array of arrays 
		if( Array.isArray(tex[0]) ){
			tex = rg.array(tex);
		}
	
		pattern = tex[0];
		patternSize = tex[2] ? rg.range(tex[1], tex[2]) : tex[1];
	}

	if(!loadedTextures.includes(pattern)) return "lime";
	patternSize = patternSize * scaled();
	return DA.getPattern(pattern, patternSize);
}
	
	
export function decideTexture(temp, blue){
	const texture = (()=>{
		if(blue.texture){
			return blue.texture;
		}else if(temp.material){
			switch(temp.material){ //TODO
				case "bronze": return "copper";
				case "steel": return "iron";
				case "placeholder": return ["placeholder", 50, 200];
				default: return;
			}
		}else{
			return undefined;
		}
	})();
	
	const highlightTexture = (()=>{
		if(blue.highlightTexture){
			return blue.highlightTexture;
		}else if(temp.highlightMaterial){
			switch(temp.highlightMaterial){ //TODO
				case "bronze": return "copper";
				case "steel": return "iron";
				case "placeholder": return ["placeholder", 50, 200];
				default: return;
			}
		}else{
			return undefined;
		}
	})();
	
	const fishnetTexture = (()=>{
		if(blue.fishnetTexture){
			return blue.fishnetTexture;
		}else{
			return undefined;
		}
	})();
	
	return {
		texture: getTexture(texture),
		fishnetTexture: getTexture(fishnetTexture),
		highlightTexture: getTexture(highlightTexture),
	}
}



export function decideCategory(what){
	for(const key of Object.keys(slotCategories) ){
		if( slotCategories[key].includes(what) ) return key;
	}
	return "OTHER";
}

function toHi(hsla){
	return {
		hi_hue: hsla.hue,
		hi_satur: hsla.satur,
		hi_light: hsla.light,
		hi_alpha: hsla.alpha,
	}
}

function fromHi(hsla){
	return {
		hue: hsla.hi_hue,
		satur: hsla.hi_satur,
		light: hsla.hi_light,
		alpha: hsla.hi_alpha,
	}
}

export function decideColor(temp, blue, dimensions, extra){
	const hslaKeys = ["hue","satur","light","alpha"];
	let fillKey = ""; //input for random color generator
	let filler = {}; //hsla object made by the genrator 
	let fill = ""; //base fill color
	let stroke = ""; //stroke color of outline 
	let highlight = "";
	let name = ""; //displayed name of the base color
	
//FILL - BASE COLOR
	//7. "temp.fill" - default from DAD {color}
		//practically never used
		
	//6. "garment[bluepring].fill" {string input for color generator}
		//MOST COMMON!
		fillKey = rg.one(blue.fill)
	//5. based on material {string input for color generator}
		//COMMON
		if(temp.material) fillKey = temp.material;
	//4. "extra.color" (it was already put in "temp.fill" but "temp.fill" has low priority) {string input for color generator}
		if(extra?.color) fillKey = extra.color;
		
//TEMPFIX FOR TEXTURES
if( temp.texture ) fillKey = "nearlyBlack";

	//COLOR GENERATOR
	filler = colorGenerator(fillKey);
		
	//3. "garment[blueprint].dim().hue" {hsla object}
		hslaKeys.forEach( a => {
			if(dimensions[a]){
				filler[a] = dimensions[a];
				fillKey = "";
			}
		});

	//2. "extra.hue" {hsla object}
		hslaKeys.forEach( a => {
			if(extra[a]){
				filler[a] = extra[a];
				fillKey = "";
			}
		});
		
	fill = stringifyHSLA(filler);
	
	//1. blueprint.dim().fill {color}
	if( dimensions.fill ){
		fill = dimensions.fill;
		fillKey = "";
		filler = {};
	}
	
	
//STROKE - OUTLINE
	//6. "temp.stroke" - default from DAD {color}
	//1. "garment[blueprint].dim().stroke" {color}
		if( dimensions.stroke !== undefined ){
			stroke = dimensions.stroke;
		//2. based on texture "temp.texture"
		}else if( temp.texture ){
			stroke = "black"; 
//TODO - FOR SOME REASON DOESN WORK
//TODO - adjust for different textures but its almost always black


//TODO 3 NONE?
		}else if(blue.stroke === ""){
			stroke = ""; //none
		}else{
			let strokeKey;
		//4. "garment[blueprint].stroke" {string input for color generator} 
			if(blue.stroke){
				strokeKey = rg.one(blue.stroke);
		//5. based on fill {hsla object} 
			//MOST USED COMMON
			}else if(filler?.hue !== undefined){
				strokeKey = "stroke";
			}
			
			stroke = stringifyHSLA(  colorGenerator(strokeKey, filler)  );
		}
		
		
	//HIGHLIGHT 
		let highlightKey;
		
		//4.  result of magic powers
		if( temp.att ){ //TODO - CHECK FOR NOT EMPTY
			highlightKey = Object.keys(temp.att)[0];
		
		//5. "garment[blueprint].highlight" {string input for the color generator}
		}else if(blue.highlight !== undefined ){
			highlightKey = rg.one(blue.highlight);
		
		//6. based on filler {hsla object}
		}else if(filler?.hue !== undefined){
			highlightKey = "highlight";
		}
		
		if(highlightKey){
			const tempco = colorGenerator(highlightKey, filler);
			//highlight = stringifyHSLA(tempco);
			filler = {
				...filler,
				...(toHi(tempco)),
			}
		//7. based on stroke {color}
		}else{
			//highlight = temp.stroke;
			hslaKeys.forEach( a => filler[`hi_${a}`] = undefined );
		}
		

		hslaKeys.forEach( key => {
			//3. extra {hsla object}
			if(extra[`hi_${key}`]) filler[`hi_${key}`] = extra[`hi_${key}`];
			//2. "blueprint.dim().hi_hue" {hsla object} 
			if(dimensions[`hi_${key}`]) filler[`hi_${key}`] = dimensions[`hi_${key}`];
		});
		
		highlight = stringifyHSLA(fromHi(filler));
		
		
		// 1. "blueprint.dim().highlight" {color}
		if(dimensions.highlight !== undefined){
			hslaKeys.forEach( a => filler[`hi_${a}`] = undefined );
			highlight = dimensions.highlight; 
		}



//NAME OF THE COLOR 
		if(temp.material){
			name = nameGenerator(undefined, temp.material);
		}else if(temp.texture){
			name = nameGenerator(undefined, temp.texture.patternName); //TODO TEXTURE TEMPFIX
		}else{
			name = nameGenerator(filler, fillKey);
		}

	return {
//BEWARE - filler is not used
		filler, //hue, satur, light, alpha, hi_hue, hi_satur, hi_light, hi_alpha as object
		fill, //color string (usually stringified filler)
		stroke, //color string 
		highlight, //color string
		name,
	}
	
}














function nameGenerator(tempco, name = ""){
	switch(name){
		case "irlSilver": return "silver";
		case "irlGold": return "gold";
		case "irl14Gold": return "14k gold";
		case "irl18Gold": return "18k gold";
		case "irl24Gold": return "24k gold";
		case "irlRoseGold": return "rose gold";
		
		case "NXgold": return "golden";
		case "NXsilver": return "silver";
		case "NXbronze": return "bronze";
		case "NXsteel": return "steel";
		
		
		case "mithril": return "mithril";
		case "electrum": return "electrum";
		case "gold": return "gold";
		case "silver": return "silver";
		case "bronze": return "bronze";
		case "copper": return "copper";
		case "steel": return "steel";
		case "iron": return "iron";
		
		case "glass": return "glass";
		
		case "hexa": return "hexapuma leather";
		case "wyve": return "wyvern leather";
		case "unic": return "unicorn leather";
		case "levi": return "leviathan leather";
		
		case "linen": return "linen";
		case "hemp": return "hemp";
		case "nettle": return "nettle";
		
		
		
		
		case "leather": return "leather";
		case "darkLeather": return "leather";
		
		
		
		case "carmine": return "carmine";
		case "purple": return "purple";
		case "indigo": return "indigo";
		case "pure": return "pure";
		case "royalBlue": return "royal blue"; 
		case "burgundy": return "burgundy"; 
		
		//case "dark": return "dark";
		
		
		//TODO - TEXTURES - SHOULD BE HANDLED HERE? 
		case "leopard": return "leopard";
		case "snake": return "snakeskin";
		case "zebra": return "zebra";
		case "camo": return "camo";
		
		default: //skip
		
	}
	
	if(name && name.startsWith("fishnet")) return "fishnet"; //TESXTURES TOO
	
	if(tempco === undefined) return "";
	
	//TODO - BLACK TEMPFIX
	if(tempco.satur <=15 && tempco.light <= 15) return "black";
	if(tempco.light < 5) return "black";
	
	//TODO - COLOR MODULE CHECKING INPUT
	return nameColor({
		h: tempco.hue,
		s: tempco.satur,
		l: tempco.light,
		a: tempco.alpha,
	}); 
	
}


const colorGenerator = function(color, base){
	
	
	if(color === "complimentary" || color === "contrast"){
		return complimentary(base, undefined, rg.g);
	}else if(color === "analogous" || color === "analogue"){ //TODO
		return analogue(base, undefined, rg.g);
	}else if(color === "monochromatic"){
		
		
//console.error("MONO");
//console.error(base);
		return monochromatic(base, undefined, rg.g);
	}else if(color === "stroke"){ //"stroke" based on "base" (== "fill")
		return {
			hue: base.hue,
			satur: base.satur + 6,
			light: base.light - 12,
			alpha: base.alpha,	
		};
	}else if(color === "highlight"){ //"highlight" based on "base" (== "fill")
		return {
			hue: base.hue,
			satur: base.satur + 10,
			light: base.light - 10,
			alpha: base.alpha,	
		};
	}else if(color === "fillDark" || color === "darker"){
		return {
			hue: base.hue,
			satur: base.satur,
			light: base.light - 10,
			alpha: base.alpha,	
		};
	}
	
	
	
	
	if(color === "metal") color = rg.array(["silver","gold","bronze","steel"]); //TODO

	//i.e not in the COLOR module
	const notYetApproved = {
		hp: [[0,5],[90,100],[40,60]],
		eng: [[100,105],[90,100],[40,60]],
		
		str: [[20,30],[90,100],[40,60]],
		dex: [[170,180],[90,100],[40,60]],
		int: [[230,240],[90,100],[40,60]],
		char: [[320,330],[90,100],[40,60]],
		
		lockpick: [[195,205],[20,30],[40,60]],
		evasion: [[150,160],[90,100],[40,60]],
		reflex: [[230,240],[90,100],[40,60]],
	
		
		lipRed: [[3,3],[80,80],[40,40]],
		lipCarmine: [[350,350],[100,100],[29,29]],
		lipPurple: [[321,321],[88,88],[40,40]],
		lipAmaranth: [[342,342],[63,63],[41,41]],
		lipIndigo: [[275,275],[100,100],[51,51]],
		lipCobalt: [[215,215],[100,100],[34,34]],
		lipBlack: [[0,0],[20,20],[20,20]],
		
		navySuit: [[220,230],[0,70],[8,16]],	
		leatherJacket: [[15,30],[60,90],[14,24]],	
		
		
		/*APPROVED IN TESTING */
	bright: [[0,360],[85,100],[45,55]],	//!!!!!!!!!!!!!!!!
	manlyBright: [[0,250],[85,100],[45,55]],	
		//manlyLight: [[0,250],[50,90],[55,75]],
		//light: [[0,360],[50,90],[55,75]], //!!!!!!!!!!!!!!!!!!!!!!!!!!
	manlyLight: [[0,250],[60,80],[60,80]],
	light: [[0,360],[60,80],[60,80]], 
		
	full: [[0,360],[70,90],[40,55]],
	manlyFull: [[0,250],[70,90],[40,55]],
		
		//dark: [[0,360],[55,85],[20,45]],
		//manlyDark: [[0,250],[55,85],[20,45]],
	dark: [[0,360],[55,85],[20,45]],
	manlyDark: [[0,250],[55,85],[20,45]],
		
		leather: [[25,35],[60,95],[10,25]],
		darkLeather: [[25,35],[30,90],[8,15]],
		//leather: [[34,40],[54,90],[25,40]],	
		//darkLeather: [[25,35],[30,90],[8,15]],
		//darkLeather: [[25,30],[60,100],[5,10]],
		//darkLeather: [[34,40],[10,40],[10,14]],	
		
		jeans: [[190,230],[40,90],[10,52]],
		nylon: [[0,360],[1,6],[1,6],[0.6,0.9]],
		//barbie: [[295,335],[75,100],[40,60]],
		/**/
		
		
		darkPink: [[300,330],[60,100],[30,60]],
		lightPink: [[300,330],[60,90],[60,80]],
		intensePink: [[300,330],[85,100],[38,58]],
		whitePink: [[300,330],[40,100],[75,95]],
		brightGreen: [ [95,105],[95,100],[35,50]],	//roughly around lime
		heavyRed: [	[355,360],[95,100],[25,45]],	//bright & dark red
		heavyPurple: [	[280,300],[95,100],[30,50]],	//bright & dark red
		
		
	nearlyWhite: [[0,360],[5,15],[90,95]],
	nearlyBlack: [[0,360],[5,15],[10,15]],	//black but not quite
	
	tintedWhite: [[0,360],[30,70],[90,95]],
	tintedBlack: [[0,360],[30,70],[10,15]],
	tintedLight: [[0,360],[30,70],[80,95]],
	tintedDark: [[0,360],[30,70],[10,25]],
	
	grey: [[0,0],[0,0],[5,95]],
	gray: [[0,0],[0,0],[5,95]],
	lightGrey: [[0,0],[0,0],[65,95]],
	lightGray: [[0,0],[0,0],[65,95]],
	darkGrey: [[0,0],[0,0],[5,35]],
	darkGray: [[0,0],[0,0],[5,35]],
	midGrey: [[0,0],[0,0],[30,70]],
	midGray: [[0,0],[0,0],[30,70]],
	

		//darkBlack: [	[0,360],[3,13],[6,15]],	//very dark grey //TODO [5,15],[8,16]
		//darkGrey: [	[0,360],[0,10],[15,30]],	//dark grey //TODO  [5,15],[25,35]
		
		aaa: [[0, 1],[0,100],[50,50]],
		
		dirtyWhite: [[0,360],[8,18],[80,90]],
		
		
		
		
		lighterWhite: [[0,360],[0, 10],[96,98]],
		darkerWhite: [[0,360],[0, 10],[91,95]],
		
		
		//pinkish color
		barbie: [[295,335],[75,100],[40,60]],
		cute: [[325,345],[50,100],[70,90]],
		deepRed: [[3,7],[70,95],[30,53]], //TEMP
		
		//METAL
		
		
		
		/*
		gold: [[48,52],[80,100],[48,52]],
		silver: [[45,55],[0,10],[70,80]],
		bronze: [[38,48],[75,100],[48,52]],
		steel: [[200,210],[20,40],[45,65]],
		*/
		NXgold: [[48,52],[80,100],[48,52]],
		NXsilver: [[45,55],[0,10],[70,80]],
		NXbronze: [[19,21], [48,52],[56,62]], //[[38,48],[75,100],[48,52]],
		NXsteel: [[200,210],[20,40],[45,65]],
		
		//now just outline for patterns
 //TODO
irl14Gold:[[48,52],[80,100],[40,50]],
irl18Gold:[[48,52],[80,100],[40,50]],
irl24Gold:[[48,52],[80,100],[40,50]],
irlGold:[[48,52],[80,100],[40,50]],
		gold: [[48,52],[80,100],[40,50]],
irlSilver:[[45,55],[0,10],[50,60]],
		silver: [[45,55],[0,10],[50,60]],
		bronze: [[19,21], [48,52],[56,62]], //[[38,48],[75,100],[40,50]],
		copper: [[19,21], [48,52],[56,62]], //TODO - SAME AS BRONZE
		steel: [[200,210],[0,10],[30,40]],
		iron: [[200,210],[0,10],[30,40]], //TODO SAME AS STEEL
		
		wyve: [[120,140],[60,80],[22,28]],	
		hexa: [[358,360],[60,80],[22,28]],	
		unic: [[55,58],[90,95],[90,93]],	
		levi: [[210,220],[70,80],[23,28]],	
		
		electrum: [[45,50],[70, 80],[50, 60]], //TODO 
		pewter: [[190,210],[3,6],[44,64]],
irlRoseGold:[[346,356],[30,40],[55,70]],
		roseGold: [[346,356],[30,40],[55,70]],
		
		mail: [[200,210],[20,40],[40,50]], 
		damask: [[20,24],[9,13],[70,80]],//hsla(21, 12%, 77%, 1)
		demonic: [[0,1],[95,100],[34,39]],
		
		
		mithril: [[240,260],[20,25],[78,86]], //TODO
		
		//glass: [[90,110],[40,60],[20,30],[0.6,0.6]], //glass
		
	
		
		
		pure: [[0,360],[0,1],[95,98]],
		carmine: [[350,355],[99,100],[29,30]],
		indigo: [[254,256],[99,100],[26,27]],
		purple: [[299,301],[99,100],[24,26]],
		
		
		royalBlue: [[220,230],[68,78],[54,60]], //225,73,57
		burgundy: [[345,349],[68,78],[25,32]],  //hsla(347, 100%, 28%, 1)
		
		//cloth: [[42,44],[9,11],[78,80]], //TODO 
		
		//dark: [[0,360],[0,8],[11,16]],
		
		
		
		silk: [[26,34],[8,20],[84,92]], //TODO 
		hemp: [[40,45],[50,60],[70,80]], //TODO 
nettle: [[40,45],[50,60],[70,80]], //TODO 
		
		
		linen: [[36,39],[50,60],[80,85]], //TODO 
		wool: [[39,42],[50,60],[80,85]], //TODO 
		cloth: [[42,44],[9,11],[78,80]], //TODO 
		
		
		khaki: [[65,75],[40,50],[30,40]],
		
		//glass: [[90,110],[90,100],[30,50],[0.7,0.7]], //glass
		solidGreen: [[95,105],[90,100],[30,35]], //glass
		greenGlass: [[90,110],[40,60],[20,30],[0.6,0.6]], //glass
		glass: [[90,110],[40,60],[20,30],[0.6,0.6]], //glass
	};
	

	color = notYetApproved[color] ? notYetApproved[color] : color;
	
//console.log(color)
	color = randomColor(color, rg.g);
//console.log(color)

	return {
		hue: color.h,
		satur: color.s,
		light: color.l,
		alpha: color.a,
	}
}







export function decideArtifactName(temp){
	if(!temp.att) return "";
	const entries = Object.entries(temp.att);
	if(entries.length === 1){
		switch(entries[0][0]){
			case "presence": return entries[0][1] < 0 ? " of inconspicuousness" : " of attraction";
			case "reflex": return entries[0][1] < 0 ? " of initiative" : " of biding";
			
			case "dex": return " of speed";
			case "char": return " of beauty";
			case "int": return " of wisdom";
			case "str": return " of power";
			
			case "hp": return " of fortitude";
			case "eng": return " of vitality";
			case "evasion": return " of escape";
			//case "reflex": return " of initiative";
			case "lockpick": return " of thievery";
			
			case "fire": return " of pyromancy";
			case "ice": return " of cryomancy";
			case "wind": return " of aeromancy";
			
			default: return " of something";
		}
	}else{
		return "";
	}
}


export function decideArtifactPrice(temp){
	if(!temp.att) return 0;
	
	return Object.keys(temp.att).reduce( (sum,key) => {
		if(!temp.att[key] || isNaN(temp.att[key]) ) return sum; //"0" would fuck up everything
		
		const price = (()=>{
			if(primary_attributes_prices[key]){
				const price = primary_attributes_prices[key]
				const multiplier = 2 ** (temp.att[key] - 1);
				return price * multiplier;
			}
			if(secondary_attributes_prices[key]){
				const price = secondary_attributes_prices[key]
				const multiplier = 2 ** (  Math.abs(temp.att[key] / 5) - 1  ); //TODO - FOR NEGATIVE PRESENCE!
				return price * multiplier;
			}
			return 0
		})();
			
		//TODO - CURSED ITEMS
		/*
			eg:
			1 => x1
			2 => x2
			3 => x4
			4 => x16
		*/
		return sum + price;	
	}, 0);
	//return modifiers[keys[0]] ?? 0;	//TODO - WORKS NOW
}
