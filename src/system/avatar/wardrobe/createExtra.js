import {designs} from "Data/items/designer";
import {garment} from "Data/items/blueprints/index";


export const packExtra = function(extra, item){
	function two(number){
		number = Math.round(number);
		if(number >= 100) return `99`;
		if(number <= 0) return `00`;
		return `${number}`.padStart(2,"0");
	}

	function three(number){
		number = Math.round(number);
		if(number >= 1000) return `999`;
		if(number <= 0) return `000`;
		return `${number}`.padStart(3,"0");
	}
	
	
	const length = Object.entries(extra).length;

	//nothing
	if(!length){
		return "";
	//just quality (quality almost always)
	}else if(length === 1 && extra.quality){
		return `${extra.quality}`;
	//quality & color (either extra.color (string input for color generator) or directly extra.hue etc.)
	}else if( (length <= 2 && extra.color) || (length <= 4 && extra.hue)  ){
		return `${three(item.hue)}${two(item.satur)}${two(item.light)}${extra.quality}`; //total final hsl; not extra (because extra.color is string)
	//everyting
	}else if(designs[item.blueprint]){
		const keys = designs[item.blueprint].map( a => a );
		const dimensions = garment[item.blueprint].dim();
		let code = ``;
		
		keys.forEach( key => {
			if(typeof dimensions[key] === "boolean"){
				code = `${code}${+(extra[key] ?? item[key])}`;
			}else{
				const [min, max] = dimensions[key];
				const value = (+(extra[key] ?? item[key]) - min) / (max - min);
				code = `${code}${three(value * 100)}`;
			}	
		})
		
		/* eslint-disable no-alert, no-console */
		if(isNaN(code)){
			console.warn(`Garment "${garment.name}" - broken saving or user-edited properties - "${code}"`)
			code = ``;
		}
		/* eslint-enable no-alert, no-console */
		return `${code}${three(item.hue)}${two(item.satur)}${two(item.light)}${extra.quality}`;
	}else{
		return ``;
	}
}


export const unpackExtra = function(code, blueprint){
	function one(){
		const temp = code.slice(-1);
		code = code.slice(0, -1);
		return +temp;
	}
	
	function two(){
		const temp = code.slice(-2);
		code = code.slice(0, -2);
		return +temp;
	}

	function three(){
		const temp = code.slice(-3);
		code = code.slice(0, -3);
		return +temp;
	}
	
	const extra = {};
	
	if(code.length === 1){
		extra.quality = +code;
	}else{
		extra.quality = one();
		extra.light = two()
		extra.satur = two();
		extra.hue = three();

		if(code.length){
			const keys = designs[blueprint].map( a => a );
			const dimensions = garment[blueprint].dim();
			while(code.length){
				const key = keys.pop();
				if(typeof dimensions[key] === "boolean"){
					extra[key] = !!one();
				}else{
					const [min, max] = dimensions[key];
					extra[key] = min + ((max - min) * (three()/100))
				}	
			}
		}
	}
	return extra;
}


