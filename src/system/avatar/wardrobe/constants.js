export const british = false; //British English is prefered (knickers X panties) //not implemented properly

export const transparent_1 = 0.94; //.85 //the garment with lower "alpha" is considered to be TRANSLUNCENT
export const transparent_2 = 0.80; //0.65 the garment with lower "alpha" is considered to be SEE-THROUGH

export const groinLegCoverage = 0.16; //the garment with HIGHER "legCoverage" fully covers the groin area
export const groinWaistCoverage = -0.65; //the garment with LOWER "waistCoverage" fully covers the groin area

export const glimpseLegCoverage = 0.135; //the garment with HIGHER "legCoverage" partially covers the groin area, giving only a brief glimpse
export const glimpseWaistCoverage = -0.525; //the garment with LOWER "waistCoverage" partially covers the groin area, giving only a brief glimpse

export const stockingsLegCoverage = 0.40; //the socks with LOWER "legCoverage" are considered to be STOCKINGS
export const highSocksLegCoverage = 0.69; //the socks with LOWER "legCoverage" are considered to be HIGH SOCKS

export const miniSleevesArmCoverage = 0.15; //shirts or dresses with LOWER "armCoverage" are considered to have mini sleeves
export const shortSleevesArmCoverage = 0.45; //shirts or dresses with LOWER "armCoverage" are considered to have short sleeves

export const microSkirtLegCoverage = 0.25; //the skirt with LOWER "legCoverage" is MICROSKIRT
export const miniSkirtLegCoverage = 0.35; //the skirt with LOWER "legCoverage" is MINISKIRT
export const shortSkirtLegCoverage = 0.5; //the skirt with LOWER "legCoverage" is SHORT SKIRT
export const mediumSkirtLegCoverage = 0.75; //the skirt with LOWER "legCoverage" is MEDIUM SKIRT


export const absoluteCleavage = 0.69; //the garment with HIGHER "cleavageCoverage" has ABSOLUTE CLEAVAGE
export const deepCleavage = 0.42; //the garment with HIGHER "cleavageCoverage" has DEEP CLEAVAGE
export const revealingCleavage = 0.3; //the garment with HIGHER "cleavageCoverage" has REVEALING CLEAVAGE
export const niceCleavage = 0.18; //the garment with HIGHER "cleavageCoverage" had NICE CLEAVAGE


//TODO
export const noBreasts = 3; // "breastSize" BELOW or SAM is FLAT CHEST
export const mediumBreasts = 15; // "breastSize" ABOVE is MEDIUM BREASTS
export const bigBreasts = 30; // "breastSize" ABOVE is BIG BREASTS
export const hugeBreasts = 45; // "breastSize" ABOVE is HUGE BREASTS
export const enormousBreasts = 60; // "breastSize" ABOVE is ENORMOUS BREASTS

