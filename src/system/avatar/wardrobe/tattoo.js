import {scaled} from "../canvas/initiate";
import {tattoos} from "Assets/tattoos";
import {Tattoos, Tattoo} from "Libraries/da";

const layer_tattoo_arms = 7; //TODO - BEWARE - WATCH OUT FOR CHANGES OF LAYERS IN DAD
const layer_tattoo_breasts = 9;
const layer_tatto_face = 11;
const tattoo_slots = [];


const base_dimensions = {
	
	face: {
		loc: "head",
		layer: layer_tatto_face,
		width: 80,
		relativeLocation: {
			drawpoint: "eyes.center",
			dx: -1,
			dy: 6,
		},
	},
	

	upperArm: {
		layer: layer_tattoo_arms,
		loc: "arm",
					
		width: 40,
		rotation: 2,
		relativeLocation: {
			drawpoint: "shoulder",
			dx: -1,
			dy: 2,
		},
	},

	sleeve: {
		layer: layer_tattoo_arms,
		loc: "arm",
		width: 75,
		side: "left",
		rotation: 7,
		relativeLocation: {
			drawpoint: "shoulder",
			dx: -5,
			dy: -1,
		},
	},

	forearm: {
		layer: layer_tattoo_arms,
		loc: "arm",
		width: 75,
		side: "left",
		rotation: 8,
		relativeLocation: {
			drawpoint: "elbow.out",
			dx: -1,
			dy: -8,
		},
	},


	bothSleeves: { //TODO - conflict with sleeve
		layer: layer_tattoo_arms,
		loc: "arm",
		width: 75,
		side: "left",
		rotation: 7,
		relativeLocation: {
			drawpoint: "shoulder",
			dx: -5,
			dy: -1,
		},
		complementary: "true",
	},

	bothForearms: {	//TODO - experimental, conflicting with forearm
		layer: layer_tattoo_arms,
		loc: "arm",
		width: 75,
		side: "left",
		rotation: 8,
		relativeLocation: {
			drawpoint: "elbow.out",
			dx: -1,
			dy: -8,
		},
		complementary: "true",
	},
//CHEST
	chest: {
		complementary: "chestRight",
		loc: "torso",
		width: 90,
		relativeLocation: {
			drawpoint: "chin.bot",
			dx: 0,
			dy: -18,
		},
	},
	
	chestSmall: {
		loc: "torso",
		width: 30,
		relativeLocation: {
			drawpoint: "chin.bot",
			dx: 6,
			dy: -17,
		},
	},

//BREASTS
	breasts: {
		mirrorable: true,
		layer: layer_tattoo_breasts ,
		loc: "chest",
		width: 50,
		relativeLocation: {
			drawpoint: "chest.nipples",
			dx: 0,
			dy: 0,
		},
		complementary: "breastsRight",
	},
	
	underBreasts: {
		complementary: "underBreastsRight",
		loc: "torso",
		width: 115,
		side: "left",
		//rotation: 0,
		relativeLocation: {
			drawpoint: "chest.center",
			dx: 0,
			dy: -15,
		},
	},
	
	underBreastsSide: {
		mirrorable: true,
		complementary: "underBreastsSideRight",
		loc: "torso",
		width: 60,
		relativeLocation: {
			drawpoint: "waist",
			dx: -2,
			dy: 8,
		},
	},

//BELLY	
	belly: {
		loc: "torso",
		width: 50,
		side: "right",
		ignoreClip: true,
		relativeLocation: {
			drawpoint: "bellybutton.top",
			dx: 0,
			dy: -1,
		},
	},
	
	lowerBelly: {
		loc: "leg",
		width: 110,
		side: "right",
		ignoreClip: true,
		relativeLocation: {
			drawpoint: "pelvis",
			dx: 0,
			dy: -1,
		},
	},

	side: {
		mirrorable: true,
		loc: "leg",
		width: 90,
		relativeLocation: {
			drawpoint: "hip",
			dx: -11,
			dy: 3,
		},
	},
			
	sideSmall: {
		loc: "torso",
		width: 35,
		relativeLocation: {
			drawpoint: "waist",
			dx: -3,
			dy: 0,
		},
	},
	
	
	hipSmall: {
		loc: "torso",
		width: 40,
		relativeLocation: {
			drawpoint: "waist",
			dx: -1,
			dy: -15,
		},
	},
	
//LEG
	thigh: {
		loc: "leg",
		width: 80,
		relativeLocation: {
			drawpoint: "groin.top",
			dx: 9,
			dy: 1,
		},
	},
	
//CALF
	calf: {
		loc: "leg",
		width: 80,
		relativeLocation: {
			drawpoint: "calf.out",
			dx: -6,
			dy: -1,
		},
	},
	
	
	ankle: {
		loc: "leg",
		width: 40,
		relativeLocation: {
			drawpoint: "ankle.out",
			dx: 3,
			dy: 10,
		},
	},
}

function create(source, xxx, yyy, size, rotation, slot, sinistra){
	let temp;
	
	//DEFAULT
	const dim = {
		dx: 0, //manual adjustment, added to relativeLocation.dx
		dy: 0,
		size: 1,  
		rotation: 0,
			
		layer: 2,
		loc: "",
		width: 50,
		
		ignoreClip: 0, //true,// false,
		
		...base_dimensions[slot],
		relativeLocation: {...base_dimensions[slot].relativeLocation}, //omg, the line above passes it as reference, not values
		
		side: sinistra ? "right" : "left", //BEWARE - DAD USES SIDES WRONGLY == dextral side is "left"
	};
	
	//ADJUSTED 
	if(xxx) dim.relativeLocation.dx += +xxx;
	if(yyy) dim.relativeLocation.dy += +yyy;
	if(size) dim.size *= +size;
	if(rotation) dim.rotation += +rotation;
	
	//MAID
	dim.src = tattoos[source];
	dim.name = `tat_${source}_${slot}`;
	dim.tattoo = true;
	dim.width = Math.floor( dim.size * dim.width * scaled() );	
	if(dim.side == "right" && dim.rotation) dim.rotation = dim.rotation * -1;

	//CREATION
	try{
		temp = Tattoos.create(Tattoo, dim);
	}catch(err){
		/* eslint-disable no-alert, no-console */
			alert(`WARNING! DAD was not able to create demanded tattoo ("${dim.name}")!`)
			console.warn(err); 
		/* eslint-enable no-alert, no-console */
		return;
	}
	
	
	//TO DO - THIS IS AWFUL:
	temp.belowParts = ["clothingParts leg","clothingParts torso","clothingParts chest"];
	if(xxx || yyy || size || rotation){
		temp.blueprint = `${source};${xxx ?? 0};${yyy ?? 0};${size ?? 1};${rotation ?? 0}`;
	}else{
		temp.blueprint = source;
	}
	//dim.default_width = dim.size * dim.width /* * scaled()*/;   //place-based * image source adjustment * canvas

	return temp;
}


export const tattoo = function(blueprint, slot, subject = PC){
	if(!blueprint) return;
	const [source, xxx, yyy, size, rotation] = blueprint.split(";");

	const sinistra = slot.search("Left") !== -1;
	slot = slot.replace("Left", "").replace("Right", "");

	if(!base_dimensions[slot]) return;
	
	const temp = create(source, xxx, yyy, size, rotation, slot, sinistra);
	subject.addTattoo(temp);
	
	//COMPLIMENTARY
	if(temp.complementary){
		const temp = create(source, xxx, yyy, size, rotation, slot, !sinistra);
		subject.addTattoo(temp);
	}
}
	
	
	
	
	


		
		