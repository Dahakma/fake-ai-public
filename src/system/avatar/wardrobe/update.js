//import * as DA from "Libraries/da";
import {PORTRAIT, draw} from "../canvas/draw";
//import {recreate} from "../wardrobe/create";
import {nipplesVisible} from "./appearance";
import {recreate} from "./main";
import {allMakeup, allClothes} from "./all";
import {garmentIs, subjectWears} from "./types";
import {glimpseWaistCoverage, glimpseLegCoverage, groinLegCoverage, groinWaistCoverage} from "./constants";
import {attachBodyparts} from 'Avatar/game/bodyparts';
/*
	Top
	-1 - everything
	0 - topless
	1 - bra
	2 - undershirt
	3 - shirt
	4 - vest/sweater/armor
	5 - coat

	Bot:
	-1 - everything
	0 - nude
	1 - panties & socks
	2 - pantyhose
	3 - lower
	4 - armor
	
	Legs
	- 1 - undefined (follows bot)
	0 - none
	1 - socks/stockings
	2 - pantyhose
	3 - shoes
	//3 - armor
	
	Jewellery
	0 - none
	1 - piercings
	2 - jewellery
*/

const showSlots = ["top","bot","leg","jew"];

export function showLess(top, bot, leg, jew, subject = PC){
	const inputSlots = {top, bot, leg, jew};
	let tampered = false; //checks if necessary (there was no change)
	showSlots.forEach( a => {
		if(inputSlots[a] !== undefined && inputSlots[a] !== subject.display[a]){
			subject.display[a] = inputSlots[a];
			tampered = true;
		}
	});
	if(tampered) updateDraw(subject);
}

export function showAll(subject = PC){
	if( showSlots.every( a => subject.display[a] === -1 && subject.tampered === false) ) return; //checks if necessary (there was no change)
	showSlots.forEach( a => subject.display[a] = -1 );
	updateDraw(subject); //TODO??? - shout it draw???
}


export function rollUpTop(subject = PC){
	//TODO
}


//TODO ASAP  - roll up skirt and remove panties 
//TODO  probably should not be in /wardrobe but /game
export function rollUpSkirt(degree = 3, lower = -1, subject = PC, who = ""){ //TODO - WORKS ONLY IN ONE LAYER
	/*
		-1 unroll
		0 - to groin
		1 - glimpse
		2 - fully
	*/
	if(degree === -1) return updateDraw(subject);
	
	
	if(subject?.lower?.dad === 'PleatedSkirt' && degree === 3) degree = 2; // TODO - TEMPFIX - ERROR WITH DEG 3 AND PEATED

	if( subjectWears.pants(subject) ){
		//pants are unable to be rolled
		showLess(undefined, 1, -1); 	
	}else{
		
		if(lower !== -1){
			//subject.display.bot = 0;
			//subject.display.leg = 5;
			showLess(undefined, lower, undefined); 	//TODO - TEST UNDEFINED 
		}
	
		
		if( subjectWears.skirt(subject) ){
			subject.upper.tampered = true;
			
			//dress
			if( subjectWears.dress(subject) ){
				switch(degree){
					case 0: //to groin
						subject.upper.legCoverage = groinLegCoverage;
						break;
					case 1: //glimpse
						subject.upper.legCoverage = glimpseLegCoverage;
						break;
					default:
					case 2: //fully
						subject.upper.legCoverage = 0.06;
						break;
					case 3: //max
						subject.upper.legCoverage = 0.025;
						break;
				}
			//skirt
			}else{
				subject.lower.tampered = true;
				switch(degree){
					case 0: //to groin
						subject.lower.legCoverage = groinLegCoverage;
						break;
					case 1: //glimpse
						subject.lower.legCoverage = glimpseLegCoverage;
						break;
					default:
					case 2: //fully
						subject.lower.legCoverage = 0.06;
						break;
					case 3: //max
						subject.lower.legCoverage = 0.025;
						break;
				}		
			}
			draw(subject); 
		}
	}
	
	
	let tex = "";
	const panties = subjectWears.panties(subject) && lower === 0;
	const pantyhose = subjectWears.pantyhose(subject) && lower <= 1;
	if(!who) who = subject.key === "kat" ? "I" : `@${subject.key}`;
	const his = (()=>{
		if(subject.key === "kat") return "my";
		return "her"; //TODO ADD HIS
	})();
	if( subjectWears.skirt(subject) ){
		tex = `${who} lifted up ${his} @lower`;
		if(panties && pantyhose){
			tex += ` and pulled down ${his} @panties and @socks`;
		}else if(panties){
			tex += ` and pulled down ${his} @panties`;
		}else if(pantyhose){
			tex += ` and pulled down ${his} @socks`;
		}
	}else{
		tex = `${who} pulled down ${his}`;
		if(panties && pantyhose){
			tex += ` @lower, @socks and @panties`;
		}else if(panties){
			tex += ` @lower and @panties`;
		}else if(pantyhose){
			tex += ` @lower and @socks`;
		}else{
			tex += ` @lower`;
		}
	}
	return tex;
	
}



export function update(subject = PC){
	const display = {
		top: -1,
		bot: -1, 
		leg: -1, 
		jew: -1,
		...subject.display, //overrides the default values above
	};
	
	subject.removeAllClothing(); //strips everything, tabula rasa
	
	attachBodyparts(subject); //attached non-default extra body parts (e.g. horns)
	
	allMakeup.filter( what => subject[what] ).forEach(  what => subject.wearClothing( subject[what] )  ); //equip everything in slots designated as makeup
	
	
	function equip(what){
		if(!subject[what]) return; //garment exists
		checkTransparency(subject, what); //clothes could be set as transparent
		if(subject[what].tampered) subject[what] = recreate(subject[what]); //checks if the dimensions of the garment were altered and if so recreates it
		subject.wearClothing( subject[what] ); //actually puts clothes one
	}
	
	const on = {	
		top(layer, what){
			if(display.top < 0 || display.top >= layer){
				equip(what);
			}
		},
		bot(layer, what){
			if(display.bot < 0 || display.bot >= layer){
				equip(what);
			}
		},
		jew(layer, what){
			if(!subject[what]) return;
			if(display.jew < 0 || display.jew >= layer){
				//checkTransparency(subject, what);
				subject.wearClothing( subject[what] );
			}
		},
		def(what){
			equip(what);
		},
	};
	
	
	
	//JEWELERY 
	{
		//piercings 
		[
			"earrings",
			
			"bellybutton",
			"lips",
			"lipsSide",
			"lipsTop",
			"lipsBot",
		
			"nose",
			"noseSide",
			"noseTop",
			"noseBot",
		
			"eyebrow",
			
		].forEach( key => on.jew(1, key) );
		
		//jewellery 
		[
			"bodychain",
			"wristRight",
			"wristLeft",
			"necklace",
			"collar",
	
			"noseChain", //??
		].forEach( key => on.jew(2, key) );
		
		//on.jew(1, "tongue"); //lol, invisible //TODO 
		
		
		//nipple piercings - hidden under clothes? 
		if(!PORTRAIT.transparent && !subject.isFigurine){
			if( (display.top !== 0 && display.top !== -2) && subject.bra){
				//hidden under bra
			}else{
				on.jew(1, "nipples");
				on.jew(1, "nipplesBot");
			
				//TODO - TESTING HAS TO BE DONE 			
				if( (display.top >= 2  || display.top < 0) && subject.upper){
					//hidden under dress, exceptions: 
					if(subject.upper.dad == "wHalterTop") on.jew(2, "nippleChain");
					//if(PC.upper.dad == "wTubeTop") on.jew(1, "nippleChain"); - would work but its hidden anyway 
					if(subject.upper.cleavageCoverage && subject.upper.cleavageCoverage > 0.4) on.jew(2, "nippleChain");
				}else{
					on.jew(1, "nippleChain");
				}
			}
			
		}else{
			on.jew(1, "nipples");
			on.jew(1, "nipplesBot");
			on.jew(2, "nippleChain");
		}
		
		
		//genital piercings - hidden under clothes? 
		if(!PORTRAIT.transparent &&  !subject.isFigurine && display.bot !== 0 && subject.panties){
			//hiddend under panties, doesn't have to be PORTRAITed 
		}else{
			[
				"labia",
				"labiaBot",
				"pubic",
				"clit",
				"hch",
				"vch",
			].forEach( key => on.jew(1, key) );
		}
		on.jew(2, "hoodChain"); //TODO??????
		
		
		on.jew(1,"glasses"); //TODO THIS IS TEMPORAL
		on.jew(3,"hat"); //TODO THIS IS TEMPORAL
	}	


	//TOP
		/*
			-1 - everything
			0 - topless
			1 - bra
			2 - undershirt
			3 - shirt
			4 - vest/sweater/armor
			5 - coat
		*/
		if(display.top !== -2) on.top(1, "bra");
		on.top(2,"upperInner");
		on.top(3,"upper");
		on.top(4,"vest");
		on.top(4,"upperOuter");
		
		on.top(5,"coat");	
			
			
		//OTHER/ARMOR
		on.top(3,"gloves");		
		on.top(5,"neck");
		on.top(5,"vambraces");
		on.top(5,"pauldrons");
		
		
	//BOT
		/*
			Bot:
			0 - nude
			1 - panties & socks
			2 - pantyhose
			3 - lower
			4 - armor
		*/
		if(display.bot !== -2) on.bot(1,"panties");
		on.bot(2,"lowerInner");
		on.bot(3,"lower");
		
		//on.bot(4,"belt");
		on.bot(4,"lowerOuter");

		
//TODO - BELT - TEMPFIX 
if(subject.belt){
	if(subject?.upper?.slot === "upperLower"){
		on.top(3,"belt");
	}else{
		on.bot(4,"belt");
	}
}
	
	//LEGS
		/*
			- 1 - undefined (follows bot)
			0 - none
			1 - socks
			2 - pantyhose
			3 - shoes
		*/

		if(display.leg === 0){
			//no socks
		}else if(display.leg === 1){
			//socks
			if( !subjectWears.pantyhose(subject) ) on.def("socks");
		}else if(display.leg === 2){
			//empty - TODO
			if( !subjectWears.pantyhose(subject) ) on.def("socks"); 
		}else if(display.leg === 3){
			//shoes
			if( !subjectWears.pantyhose(subject) ) on.def("socks");
			on.def("shoes");
			
		}else if(display.leg === 4){
			//armor
			on.def("greaves"); //TODO
			on.def("shoes");
			if( !subjectWears.pantyhose(subject) )  on.def("socks");
		
		}else{
			if( display.bot !== -2 ){
				if( !subjectWears.pantyhose(subject) ){
					on.bot(1, "socks");
				}else{
					on.bot(2, "socks");
				}
			}
			on.bot(3, "shoes");
			on.bot(5, "greaves"); 
		}
		
		
	//skirt rolled up
		//TODO - OTHER LAYERS
		//TODO - LONG UPPPER?
		if(
			subject?.upper?.slot === "upperLower" 
			&& (display.top < 0 || display.top >= 3) //top still displayed
			&& (display.bot >= 0 && display.bot < 3) //bot hidden
			&& subject.upper.legCoverage !== undefined
		){
			subject.upper.legCoverage = 0.06;
			subject.upper.tampered = true;
		}
	
	 
	//nipple visibility
		//TODO UPPERINNER
		if(subject.upper){
			hideNipples("upper");
			if( nipplesVisible("upper", subject) ) showNipples("upper");
		}else if(subject.upperOuter){
			hideNipples("upperOuter");
			if(!subject.upperOuter.hp){  //TODO - TEMPFIX ARMOR
				if( nipplesVisible("upperOuter", subject) ) showNipples("upperOuter");
			}
		}
		
		function hideNipples(where){
			subject[where].showNipples = false;
			subject[where].showPiercings = false;
			subject[where].showPiercingsTop = false;
			subject[where].showPiercingsBot = false;
		}
		
		function showNipples(where){
			subject[where].showNipples = true;
			if(subject.nipples){
				if(subject.nipples.dad === "MultipleNipplePiercings"){
					subject.upper.showPiercings = true;
					if(subject.nipples.number == 4){
						subject[where].showPiercingsTop = true;
						subject[where].showPiercingsBot = true;
					}
				}
				if(subject.nipples.dad === "SimpleNipplePiercings"){
					if(subject.nipples.bar){
						subject[where].showPiercings = true;
					}
					if(subject.nipples.ring && subject.nipples.cbr){
						subject[where].showPiercingsBot = true;
					}
				}
			}
			
			/*
			if(subject.nipples || subject.nipplesBot){
				//ie NippleBars & MultipleNiple
				if(subject.nipples && subject.nipples.dad === "MultipleNipplePiercings"){
										
					subject.upper.showPiercings = true;
					if(subject.nipples.number == 4){
						subject[where].showPiercingsTop = true;
						subject[where].showPiercingsBot = true;
					}
				//ie NippleRings & NippleHeavy (out)
				}
				if(subject.nipplesBot && subject.nipplesBot.dad === "SimpleNipplePiercings"){
					if(subject.nipplesBot.bar){
						subject[where].showPiercings = true;
					}
					if(subject.nipplesBot.ring && subject.nipplesBot.cbr){
						subject[where].showPiercingsBot = true;
					}
				}
			}
			
			*/
		}
		
//TODO - BELT ACTUALLY EQUIPED
	//belt aligned to pants
		if(subject.belt && subject.lower && subject.lower.subtype !== "loincloth"){ //TODO TEMP EXPERIMENT
			if(subject.belt.waistCoverage && subject.lower.waistCoverage) subject.belt.waistCoverage = subject.lower.waistCoverage;
			if(subject.belt.beltCurve && subject.lower.beltCurve) subject.belt.beltCurve = subject.lower.beltCurve;
			if(subject.lower.beltWidth)subject.lower.beltWidth = subject.belt.beltWidth-0.05; //?????
		
//TODO  lowerouter x lowerOuter????
		}else if(subject.belt && subject.lowerOuter){
			if(subject.belt.waistCoverage && subject.lowerOuter.waistCoverage) subject.belt.waistCoverage = subject.lowerOuter.waistCoverage;
			if(subject.belt.beltCurve && subject.lowerOuter.beltCurve) subject.belt.beltCurve = subject.lowerOuter.beltCurve;
			if(subject.lowerOuter.beltWidth) subject.lowerOuter.beltWidth = subject.belt.beltWidth-0.05; //?????
	
		
		}else if(subject.belt && subject.lowerArmor){
			if(subject.belt.waistCoverage && subject.lowerArmor.waistCoverage) subject.belt.waistCoverage = subject.lowerArmor.waistCoverage;
			if(subject.belt.beltCurve && subject.lowerArmor.beltCurve) subject.belt.beltCurve = subject.lowerArmor.beltCurve;
			if(subject.lowerArmor.beltWidth) subject.lowerArmor.beltWidth = subject.belt.beltWidth-0.05; //?????
		}
		
	//pants tucked into greaves
		if(subject.greaves && subject.lower && subject.lower && subject.lower.innerLoose<1){
			subject.lower.tampered = true;
			subject.lower.innerLoose = 0;
			subject.lower.outerLoose = 0;
		}
		
	//sleeves tucked into vambraces
		if(subject.vambraces && subject.upper){
			subject.upper.tampered = true;
			subject.upper.armLoose = 0;
		}
		
	//vest (respective corset) makes top tight
		if( (subject.upperArmor || subject.vest) && subject.upper){
			if(subject.upper.sideLoose){
				subject.upper.tampered = true;
				subject.upper.sideLoose = 0;
			}
		}
		
	//lacing has clipping issues with upper layers
		if(subject.upper && subject.upperOuter){
			if(subject.upper.lacing){
				subject.upper.tampered = true;
				subject.upper.lacing = false;
			}
		}
		
	//shirt sleeves smaller than sweaters
		if(subject.upper && subject.upperOuter){
			if(subject.upper.armCoverage < subject.upperOuter.armCoverage){
				subject.upper.tampered = true;
				subject.upper.armLoose = 0; //in theory can cause troubles, but in practise it should not; everything shortening upperOuter sleeves would probably also tightened upper sleeves
			}else if(subject.upper.armLoose > subject.upperOuter.armLoose){
				subject.upper.tampered = true;
				subject.upper.armLoose = subject.upperOuter.armLoose;
				//subject.upperOuter.armLoose = subject.upper.armLoose; //todo - shouldn I change the upper loose? 
			}
			if(subject.upper.sideLoose > subject.upperOuter.sideLoose){
				subject.upper.tampered = true;
				subject.upper.sideLoose = subject.upperOuter.sideLoose;
				//subject.upperOuter.sideLoose = subject.upper.sideLoose;
			}
		}
	
	//upper clipped to fit under upperOuter
		if(subject.upper && subject.upperOuter && subject.upper.clipTightly !== undefined){
			subject.upper.tampered = true;
			subject.upper.clipTightly = true;
		}
		
		
		
	//pantyhose under lower 
//TODO EXT!!!
		if(ext.dressup.tuck_socks){
			if(subject?.socks?.waistCoverage > subject?.lower?.waistCoverage){
				subject.socks.waistCoverage = subject.lower.waistCoverage;
				subject.socks.tampered = true;
			}
		}
	//panties under lower
//TODO EXT!!!
		if(ext.dressup.tuck_panties){
			if(subject?.panties?.waistCoverage > subject?.lower?.waistCoverage){
				subject.panties.waistCoverage = subject.lower.waistCoverage;
				subject.panties.tampered = true;
			}
		}
		
	//TODO!!!!
	//BOOTS UNDER SKIRT
		if(subject.shoes && subject.lower){
			subject.shoes.clothingLayer = 2;
			if(subject.shoes.legCoverage < subject.lower.legCoverage){
				if(subject.lower.innerLoose > 0.05 || subject.lower.outerLoose > 0.05){
						subject.shoes.clothingLayer = 1;
				}	
			}
		}
		


//LOINCLOTH TEMPFIX	
/*
if(subject.lower.subtype === "loincloth"){
	
}
if(subject.lowerOuter.subtype === "loincloth"){
	
}
*/

		
		
	//pants > skirt
		if(subject.lower){
			if(subject.lower.innerLoose >= 1){
				subject.lower.parts[0].layer = 5;
				subject.lower.parts[1].layer = 5;
				subject.lower.noCover = true;
			}else if(subject.lower.innerLoose < 1){
				subject.lower.parts[0].layer = 2;
				subject.lower.parts[1].layer = 2;
				subject.lower.noCover = false;
			}
		}
		
		//bodysuit - not relevant but will keep
		/*
			if(subject.done.bodysuit){
				if(subject.upper)subject.upper.bodysuit = true;
				if(subject.lower)subject.lower.bodysuit = true;
				//if(subject.upper)subject.upper.bodysuit = true;
			}else{
				if(subject.upper)subject.upper.bodysuit = false;
				if(subject.lower)subject.lower.bodysuit = false;
			};
		*/ 
				
			
	//draw(subject);  
}





function checkTransparency(subject, what){
	if( subject[what].forcedTransparency != PORTRAIT.transparent ){
		switchTransparency(subject, what);
	}
	/*else if( subject[what].forcedTransparency && PORTRAIT.highlight === what ){
		switchTransparency(subject, what);
	}
	*/
}

export function switchHighlight(subject, what, on){ //TODO - TRANSPARENCY HAS TO BE SOLVED
	return;
/*
	if(on){
		PORTRAIT.highlighted = true;
		allClothes.forEach( a => {
			//console.log(a);
			//console.log(subject[a] );
			if( !subject[a] ) return;
			//if( subject[a].alpha )
			subject[a].alpha -= 0.9;
			//if( subject[a].hi_alpha ) 
			subject[a].hi_alpha -= 0.9;
		});
		//if( subject[what].alpha ) 
		subject[what].alpha += 2;
		//if( subject[what].hi_alpha ) 
		subject[what].hi_alpha += 2;
	}else{
		PORTRAIT.highlighted = false;
		allClothes.forEach( a => {
			if( !subject[a] ) return;
			//if( subject[a].alpha ) 
			subject[a].alpha += 0.9;
			//if( subject[a].hi_alpha )
			subject[a].hi_alpha += 0.9;
		});
		//if( subject[what].alpha ) 
		subject[what].alpha -= 2;
		//if( subject[what].hi_alpha ) 
		subject[what].hi_alpha -= 2;
	}
	update(subject);
*/
}


function switchTransparency(subject = PC, what){
	let alpha = 1;
	//console.warn(what);
	//console.warn(PORTRAIT.highlight);
	const transparent = PORTRAIT.transparent;
	//if(what === PORTRAIT.highlight) transparent = false;
	
	switch(subject[what].slot){
		case "coat":
			alpha = 0.95;
			break;
			
		case "upperArmor":
		case "lowerArmor":
		case "lowerOuter":
		case "upperOuter":
		case "upperLowerOuter":
			alpha = 0.9;
			break;
			
		case "vest":	
		case "shoes":
			alpha = 0.85;
			break;
			
		case "upperLower":
		case "upper":
		case "lower":
			alpha = 0.8;
			break;
			
		case "upperInner":
		case "lowerInner":
		case "socks":
			alpha = 0.7;
			break;
			
		case "bra":
		case "panties":
			alpha = 0.6;
			break;
			
		case "belt":
		case "hat":
		case "gloves":
			alpha = 0.3;
			break;
		
		default: return;
	}
	
	if(transparent) alpha *= -1;

	if(subject[what].hi_alpha) subject[what].hi_alpha += alpha;
	
	subject[what].alpha += alpha;
	subject[what].fill = `hsla(${subject[what].hue},${subject[what].satur}%,${subject[what].light}%,${subject[what].alpha})`;
	subject[what].forcedTransparency = transparent; // !subject[what].forcedTransparency; 
	/*	
		if)alpha(
		let temp = COLOR.parseHSLA(subject[what].fill);

*/
	//	temp.a = subject[what].alpha;
		//subject[what].fill = COLOR.stringifyHSLA(temp);
		
		
		//TODO HIGHLIGHT
		
		/*
		const top = 0.8;
		const mid = 0.7;
		const bot = 0; //0.6;
		
		const direction = PORTRAIT.transparent ? -1 : 1;
		
		aleph("upper",top);
		aleph("lower",top);
		aleph("socks",mid);
		aleph("bra",bot);
		aleph("panties",bot);
		
		function aleph(what,value){
			if(subject[what]){
				subject[what].alpha += direction * value;
				if( subject[what].alpha>1 )subject[what].alpha = 1;
console.log( subject[what].alpha );  
				//if(subject[what].highlight)subject[what].highlight = value;
			}
		}
		*/
		
}
	
	

export function updateDraw(subject){
	update(subject);
	draw(subject);
}