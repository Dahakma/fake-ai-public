import {subjectWears} from "./types";
import {and} from "Loop/text";
import {british} from "./constants";


export const garmentDesc = {
//SPECIFIC PIECES 
	bra(subject){
		if(subject.bra) return "bra";
		return "";
	},

	BRA(subject){
		if(subject.bra) return subject.bra.name;
		return "";
	},

	upper(subject){ //TODO 
		if(!subject.upper) return "";
		if(subject.upper.slot === "upperLower"){
			//TODO - MINIDRESS 
			return "dress";
		}else if(subject.upper.blueprint == "Tunic" ){
				return "tunic";	
		}else if(subject.upper.subtype === "tee" || subject.upper.dad === "wTee" || subject.upper.dad === "wDualTee" ){
			return "tee";
		}else if(subject.upper.dad === "wTubeTop" ||  subject.upper.dad === "TubeTop"){
			return "tube top";
		}else{
			return "top";
		}
	},

	UPPER(subject){
		if(!subject.upper) return "";
		return subject.upper.name;
	},

	upperOuter(subject){
		if(!subject.upperOuter) return "";
		if(subject.upperOuter.dad == "wSuperSuit" ){
			return "jacket";
		}else if(subject.upperOuter.dad == "wTurtleneck"){
			return "sweater";
		}else if(subject.upperOuter.dad == "wCorset" || subject.upperOuter.dad == "wHalfCorset"){
			return "corset";
		}
	},

	UPPEROUTER(subject){
		if(!subject.upperOuter) return "";
		return subject.upperOuter.name;
	},

	panties(subject){
		if(!subject.panties) return "";
		if( subject.panties.dad == "wCrotchlessPanties" ) return "crotchless panties";
		if( subject.panties.blueprint == "gString" ) return "g-string";
		if( ["thong","hiThong","lowThong","bimboThong"].includes(subject.panties.blueprint ) ) return "thong"; //BEWARE!!!!
		return british ? "knickers" : "panties";
	}, 
	
	PANTIES(subject){
		if(!subject.panties) return "";
	//TODO - add adjective - sexy? 
		return subject.panties.name;
	}, 
	
	lower(subject){
		if(!subject.lower && subject?.upper?.slot == "upperLower") return  "dress";
		if(!subject.lower) return "";
		if( subjectWears.shorts(subject) ) return "shorts";
		if( ["wSuperLeggins","wLacedLeggins"].includes(subject.lower.dad) ) return  "leggings"; 
		if( ["wJeans"].includes(subject.lower.dad) ) return  "jeans"; 
		if( subject.lower.innerLoose < 1) return british ? "trousers" : "pants";
		return "skirt";
	},
	
	LOWER(subject){
		if(!subject.lower && subject?.upper?.slot == "upperLower") return this.UPPER(subject);
		if(!subject.lower) return "";
		return subject.lower.name;
	},


	socks(subject){
		if( !subject.socks) return "";
		if(subjectWears.pantyhose(subject)) return "pantyhose";
		if(subjectWears.stockings(subject)){
			return "stockings";
		}else if(subjectWears.highSocks(subject)){
			return "high socks";
		}
		return "socks";
	},
	
	SOCKS(subject){
		if( !subject.socks) return "";
		return subject.socks.name;
	},

	sock(subject){
		if( !subject.socks) return "";
		if(subjectWears.pantyhose(subject)) return "pantyhose";
		if(subjectWears.stockings(subject)){
			return "stocking";
		}else if(subjectWears.highSocks(subject)){
			return "high sock";
		}
		return "sock";
	},

	SOCK(subject){
		if( !subject.socks) return "";
		if(subjectWears.pantyhose(subject)) return subject.socks.name;
		return subject.socks.name.slice(0, -1); //removes the "s"
	},


	shoes(subject){
		if(!subject.shoes) return "";
		if( subjectWears.boots(subject) ) return "boots";
		if( subjectWears.heels(subject) ) return "heels";
		return "shoes";
	},
	
	SHOES(subject){
		if(!subject.shoes) return "";
		return subject.shoes.name;
	},

	shoe(subject){
		if(!subject.shoes) return "";
		if( subjectWears.boots(subject) ) return "boot";
		if( subjectWears.heels(subject) ) return "heel";
		return "shoe";
	},

	SHOE(subject){
		if(!subject.shoes) return "";
		return subject.shoes.name.slice(0, -1); //removes the "s"
	},

	collar(subject){
		if(!subject.collar) return "";
		if( subjectWears.collar(subject) ) return "collar";
		if( subjectWears.choker(subject) ) return "choker";
	},
	
	COLLAR(subject){
		if(!subject.collar) return "";
		return subject.collar.name; 
	},



	//outermost above waist 
	top(subject){ 
		if(subject.upperOuter) return this.upperOuter(subject);
		if(subject.upper) return this.upper(subject);
		if(subject.bra) return this.bra(subject);
	},
	
	//outermost below waist 
	bot(subject){
		if(!subject.lower && subject?.upper?.slot == "upperLower") return  "dress";
		if(!subject.lower) return "";
		if( ["wSuperLeggins","wLacedLeggins"].includes(subject.lower.dad) ) return  "leggings";
		if( ["wJeans"].includes(subject.lower.dad) ) return  "jeans"; 
		if( subject.lower.innerLoose < 1) return british ? "trousers" : "pants";
		return "skirt";
	},
	

//COMBOS 
	//upper and bra 
	upperBra(subject){ 
		const a = [];
		if( subject.upper ) a.push(this.upper(subject)); 
		if( subject.bra ) a.push(this.bra(subject));
		return and(a);
	},

	//panties and pantyhose (not skirt) 
	pantiesPantyhose(subject){ 
		const a = [];
		if( subjectWears.pantyhose(subject) ) a.push(this.socks(subject))
		if( subjectWears.panties(subject) ) a.push(this.panties(subject))
		return and(a);
	},
	
	//lower, pantyhose and panties (everything) 
	lowerPanties(subject){
		const a = [];
		if( subject.lower ) a.push(this.lower(subject)); //TODO - IGNORES DRESS - ? YES OR NO?
		if( subjectWears.pantyhose(subject) ) a.push(this.socks(subject));
		if( subjectWears.panties(subject) ) a.push(this.panties(subject));
		return and(a);
	},
	
	//lower and pantyhose (not panties)
	lowerPantyhose(subject){
		const a = [];
		if( subject.lower ) a.push(this.lower(subject)); //TODO - IGNORES DRESS - ? YES OR NO?
		if( subjectWears.pantyhose(subject) ) a.push(this.socks(subject));
		return and(a);
	},

	//either "tee and skirt" or "dress"
	upperLower(subject){ 
		const a = [];
		if( subject.upper ) a.push( this.upper(subject) );
		if( subject.lower && subject?.upper?.slot !== "upperLower" ) a.push( this.lower(subject) );
		return and(a);
	},
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	

};