import {prices, modifiers} from "Data/items/prices";
import {ATTRIBUTES} from "Virtual/attributes";
import {weapon_damage, weapon_damage_modifiers, weapon_types} from "Data/items/armory";
import {garment} from "Data/items/blueprints/index";
import {ids} from "Data/items/ids";
import {clone} from "Libraries/dh";
import {decideArtifactName, decideArtifactPrice} from "./createDecide";

export function createWeapon(blueprint, seed){
	const source = garment[blueprint];
	/*let [base, adjective] =  blueprint.split("_").reverse();
	if(source.adjective) adjective = source.adjective;
	if(source.base) base = source.base;
*/	
	const subtype = source.subtype ?? blueprint;
	const adjective = source.adjective; 
	let name = source.name ?? subtype ?? blueprint;
	if(adjective) name = `${adjective} ${name}`;
	
	const weapon = {
		name,
		slot: "weapon",
		blueprint,
		seed,
		id: ids[blueprint],
		att: source.att ? clone(source.att) : {},
	};

 

	
	weapon.name = `${weapon.name} ${decideArtifactName(weapon)}`.trim();
	
	weapon.att.weapon_damage = Math.round(  weapon_damage[subtype] * ( weapon_damage_modifiers[adjective] ?? 1 )  );
	weapon.damage = weapon.att.weapon_damage + (weapon.att.weapon_ice ?? 0) + (weapon.att.weapon_fire ?? 0);
	

	weapon.price = prices[subtype] * ( modifiers[adjective] ?? 1 ) * modifiers.weapons * modifiers.master
	weapon.price += decideArtifactPrice(weapon);
	weapon.price = Math.round( weapon.price );
	weapon.type = Object.keys(weapon_types).find( key => weapon_types[key].includes(subtype) );
	
	weapon.category = "WEAPONS"; //TODO, necessary???
	
	return weapon;
	/*
	return {
		name:  a.name,
		attack: a.attack,
		original_attack: a.attack,
		price: a.price ?? 0,
		type: a.type,
		slot: "weapon",
		class: a.class,
		category: "WEAPONS",
		blueprint,
		seed,
				
		mag: a.mag,
		hp: a.hp,
	};
	*/
	
	
}