import * as DA from "Libraries/da";
import {rg} from "Libraries/random";
import {clone} from "Libraries/dh";

import {garment} from "Data/items/blueprints/index";
import {ids} from "Data/items/ids";
import {prices, modifiers} from "Data/items/prices";


import {POTIONS} from "Avatar/game/potions";
import {elixirs} from "Data/items/elixirs/index";
import {scaled} from "Avatar/canvas/initiate";


import {allClothes} from "./all";
import {decideTexture, decideColor, decideCategory, decideArtifactName, decideArtifactPrice} from "./createDecide";
import {createWeapon} from "./createWeapon";
import {armor_defence, armor_weight} from "Data/items/armory";
import {packExtra} from "./createExtra";

export function create(blueprint, seed, extra = {}, subject = PC){
	//if(debug) console.log({blueprint, seed, extra, key: subject?.key, subject});
		
	let temp; //created outputted piece of clothes
	
	
	//>>>> ELIXIR
/*
	if( blueprint?.[0] == "x" || blueprint == "elixir" || blueprint == "randomElixir" || blueprint == "HP" ||  blueprint == "ENG"){
		return createElixir(blueprint, seed);
	}
*/	
	
	//CHECK IF BLUEPRINT EXISTS
	/* eslint-disable no-alert, no-console */
	if(!blueprint){
		console.warn(`WARNING: Blueprint of generated item is undefined! `);
		return;
	}else if(garment[blueprint] === undefined){
		if(debug) alert(`WARNING: Blueprint of demanded type of clothes - "${blueprint}" - not found!`);
		console.warn(`WARNING: Blueprint of demanded type of clothes - "${blueprint}" - not found!`); 
		return;
	}
	/* eslint-enable no-alert, no-console */
	
	
	
	//SEED - TODO!!!
	if(seed && isNaN(seed)) console.warn("Seed is NaN!", blueprint, seed);
	if(seed === undefined || isNaN(seed) ) seed = rg.date; 
	if(seed < 1){
		seed = seed * 1_000_000; //seed should be date, but it also could be inputed Math.random, this is the best way to handle this
	}else if(seed > 999_999){
		seed = seed % 1_000_000;
	}
	seed = Math.floor(seed);
	rg.i(seed); //important!!
	

		
	//EXCEPTIONS
	//
	if( garment[blueprint].slot === "potion" || garment[blueprint].slot === "charm" ){
		return createPotion(blueprint, seed);
	
	}
	//>>> WEAPON
	if( garment[blueprint].slot === "weapon" ){
		return createWeapon(blueprint, seed);
	}
	
	//DIMENSIONS FROM BLUEPRINT
	const dimensions = garment[blueprint].dim(rg.g) ?? {}; //dimensions of DAD object different from defalut (from "garment[blueprint].dim")
	for(const key in dimensions){
		if( Array.isArray(dimensions[key]) && dimensions[key].length === 2 ){
			dimensions[key] = rg.range( dimensions[key][0], dimensions[key][1] );
		}
	}
	
	//DAD CREATION
	try{
		temp = DA.Clothes.create(DA[garment[blueprint].dad], dimensions);
	}catch(err){
		/* eslint-disable no-alert, no-console */
			if(debug) alert(`Error! DAD was not able to create demanded type of clothes - "${blueprint}"`);
			console.warn(`WARNING: DAD was not able to create demanded type of clothes - "${blueprint}"`); //SIC
			console.log(err); //SIC
		/* eslint-enable no-alert, no-console */
		return;
	}

	//EXTRA APPLY
	if(typeof extra === "number"){
		temp.quality = extra;
	}else if(typeof extra === "object"){
		for(const key in extra){
			temp[key] = extra[key];
		}
	}
	temp.quality ??= 0;
	
	//IMPORTANT STUFF used for re-creation
	temp.seed = seed;
	temp.blueprint = blueprint;
//temp.extra = extra;
	if(!ids[blueprint]){
	/* eslint-disable no-alert, no-console */
			if(debug) alert(`Error! ID of blueprint "${blueprint}" not found!`);
			console.warn(`WARNING: ID of blueprint "${blueprint}" not found! `); //SIC
	/* eslint-enable no-alert, no-console */
	}
	temp.id = ids[blueprint];
	
	//STUFF FROM garment[blueprint];
	temp.type = garment[blueprint].type;
	temp.subtype = garment[blueprint].subtype;
	temp.slot = garment[blueprint].slot;
	temp.dad = garment[blueprint].dad;
	temp.category = decideCategory(temp.slot);
	temp.material = rg.one(garment[blueprint].material); //BEWARE! material has to share name with fill color (generateColor()), name (generateName()) and price mod (price[])
	
	
	//MORE STATS 
	if(garment[blueprint].piercing) temp.piercing = true;
	temp.forcedTransparency = false; //item is displayed as transparent
	temp.locked = false; //item cannot be removed
	temp.tampered = false; //dimensions changed by other clothes 

	
	//TEXTURE
	const tempex = decideTexture(temp, garment[blueprint]);
	temp.texture = tempex.texture;
	temp.fishnetTexture = tempex.fishnetTexture;
	temp.highlightTexture = tempex.highlightTexture;
	
	//MAGIC POWERS
	if(garment[blueprint].att){
/*
console.log(blueprint);
console.log( garment[blueprint].att );
console.log( rg.one(garment[blueprint].att) );
*/
		temp.att = garment[blueprint].att ? clone(rg.one(garment[blueprint].att)) : undefined; //TODO - WORKS ONLY FOR ARRAYS!
	}



	//COLOR
	const tempco = decideColor(temp, garment[blueprint], dimensions, extra);
	//TODO - would work without filler, just temp.hue etc. have to be set to undefined
	temp.hue = tempco.filler.hue;
	temp.satur = tempco.filler.satur;
	temp.light = tempco.filler.light;
	temp.alpha = tempco.filler.alpha;
	temp.hi_hue = tempco.filler.hi_hue;
	temp.hi_satur = tempco.filler.hi_satur;
	temp.hi_light = tempco.filler.hi_light;
	temp.hi_alpha = tempco.filler.hi_alpha;
	temp.fill = tempco.fill;
	temp.stroke = tempco.stroke;
	temp.highlight = tempco.highlight;


//TODO - FUCKING TEXTURES 
if(temp.texture){ 
	temp.fill = temp.texture;
	//if( temp.texture?.[0] && temp.texture[0].startsWith("fishnet") ){
	if(temp.texture?.patternName && temp.texture.patternName.startsWith("fishnet") ){
		temp.thickness = 0.2;
		temp.alpha = 0.5; //alpha is not used for displaying (overriden by texture) but it is used for calculations whether the garment is transparent; TODO TEMPFIX CLUMSY
	}
}
if(temp.highlightTexture){ 
	temp.highlight = temp.highlightTexture;
	if( temp.highlightTexture?.patternName && temp.highlightTexture.patternName.startsWith("fishnet") ){
		temp.thickness = 0.4; //TODO - DOES IT EVEN FUCKING WORK? 
		temp.hi_alpha = 0.5; //alpha is not used for displaying (overriden by texture) but it is used for calculations whether the garment is transparent; TODO TEMPFIX CLUMSY
	}	
}
	
	//PRICE
	//base
	temp.price = (extra?.price ?? prices[blueprint] ?? prices.base) * modifiers.master;
	//material
	if(temp.material) temp.price = temp.price * ( modifiers[temp.material] ?? 1 );
	//quality
	if(temp.quality) temp.price = temp.price * modifiers.quality[temp.quality];
	//artifact
	temp.price += decideArtifactPrice(temp); //MAGIC
	//blur price	
	temp.price = (temp.price * 0.80) + (temp.price * 0.4 * rg.g);


//TODO ASAP THIS WONT WORK WHILE LOADING, will only work for merchant which is in theory the only thing we need but... dunno
	if(head.reality === 3){
		temp.price = Math.round(temp.price); 
	}else{
		//if(temp.price  temp.price = Math.round(temp.price); 
		temp.price = Math.round(temp.price * 10) / 10;
		if(temp.price % 10 === 0){ //20 -> 19.90
			temp.price = temp.price - 0.10;
		}
		//if(temp.price % 1 === 0) temp.price -= 0.1; // 2 -> 1.90
	}
 
//console.log(temp.price);

//TODO ASAP	
//
	
	
	
	
	
	//NAME
	if(!garment[blueprint].name){
		temp.name = blueprint; //mostly for testing only 
	}else if(garment[blueprint].adjective !== undefined){
		temp.name = `${garment[blueprint].adjective} ${garment[blueprint].name}`;
	}else{
		temp.name = `${tempco.name} ${garment[blueprint].name}`;
	}
	if(temp.att) temp.name += decideArtifactName(temp);
	//power.name(temp); //MAGIC
	
	
	//ARMOR
	if(temp.type){
		temp.att ??= {};
		temp.att.armor = Math.round( (armor_defence[temp.type] ?? 1) * (armor_defence[temp.material] ?? 1) );
		temp.att.burden = Math.round( (armor_weight[temp.type] ?? 1) * (armor_weight[temp.material] ?? 1) );
		temp.price = Math.round( temp.price * modifiers.armors );
	}
	
	
	//EXTRA SAVE
	temp.extra = packExtra(extra, temp);


	temp.name = temp.name.trim(); //capitalise(
	
	return temp;
}


/*
const materials(){
	gold: 
	
}
*/


/*
const power = {
	name(temp){
		if(!temp.att) return;
		temp.name = `${temp.name} ${asdf(temp)}`;
		
		function asdf(){ //TODO
			const entries = Object.entries(temp.att);
			if(entries.length === 1){
				switch(entries[0][0]){
					case "dex": return "of speed";
					case "char": return "of beauty";
					case "int": return "of wisdom";
					case "str": return "of power";
					case "hp": return "of toughness";
					case "eng": return "of vitality";
					default: return "of something";
				}
			}else{
				//empty
			}
		}
	},
	price(temp){
		if(!temp.att) return;
		temp.price += modifiers.att;
	},
	highlight(temp){
		if(!temp.att) return;
		const entries = Object.entries(temp.att);
		if(entries.length === 1){
			temp.highlight = entries[0][0];
		}else{
			//empty
		}
		
	},
}	
	*/
	



/*
const potion_prices = []; //TODO STOPGAP
Object.keys(prices).filter(a => a.startsWith("x") ).forEach( a => {
	potion_prices[ POTION.id(a) ] = prices[a];
})
*/

function createPotion(blueprint, seed){
	const source = garment[blueprint];
	const id = ids[blueprint];
	
	//console.error(blueprint)
	//console.warn(id)
	
	if(source.slot === "charm"){
		return {
			name: source.name,
			price: Math.round( prices[blueprint] * modifiers.master * modifiers.charms ),  //TODO
			slot: "charms",
			category: "POTIONS", //TODO
			blueprint,
			id,
			seed: id,
			skill: source.skill,
		};
	}
	
	
	return {
		get name(){
			return POTIONS.name(blueprint);
		},
		id: ids[blueprint],
		price: Math.round( prices[blueprint] * modifiers.master * modifiers.elixirs ),
		slot: "potion",
		category: "POTIONS",
		blueprint,
		seed: ids[blueprint],
		skill: source.skill,
	};
		
		
		
	
	//temp.category = "WEAPONS"; //TODO, necessary???
	
	/*
	if(blueprint === "elixir"){
		
		//if(seed >= 100) seed /= 1000000;
	}else{
		seed = POTION.id(blueprint);
		blueprint = "elixir";		
	}
	

	let price = potion_prices[seed] ?? 11;
	price = Math.round(price * modifiers.master * modifiers.elixirs);
	
	return {
		get name(){
			//console.log(this.seed);
			return POTION.name(this.seed);
		},
		id: seed, //TODO!!
		price,
		slot: "potion",
		category: "POTIONS",
		blueprint: "elixir",
		seed, //: seed * 1000000,
	};
	*/
}


