export const slotCategories = {
	POTIONS: [
		"potion",
		"charms", //TODO!!
	],
	WEAPONS: [
		"weapon",
	],
	UPPER: [
		"vest",
		"upper",
		"upperArmor",
		"upperLower",
		"neck",
		"upperOuter",
		"upperInner",
		"coat",
	],
	LOWER: [
		"lower",
		"lowerArmor",
		//
		"lowerOuter",
		"lowerInner",
	],
	UNDERWEAR: [
		"bra",
		"panties", 
	],
	ARMS: [
		"pauldrons",
		"gloves",
		"vambraces",
	],
	LEGWEAR: [
		"greaves", 
		"socks", 
	],
	SHOES: [
		"shoes",
	],
	ACCESSORY: [
		"hat",
		"belt",  
		"glasses",
	],
	JEWELERY: [
		"collar",
			//break
		"necklace",
		"bodychain",
		"wristLeft",
		"wristRight",
		"earrings", 
	],
	PIERCING: [
		"nose",
		"noseSide",
		"noseTop",
		"noseBot",
		"noseChain",
			//break
		"lips",
		"lipsSide",
		"lipsTop",
		"lipsBot",
		"eyebrow",
			//break
		"nipples",
		"nipplesBot",
		"nippleChain",
			//break
		"pubic",
		"hch",
		"vch",
		"labiaBot",
		"labia",
		"clit",
		"hoodChain",
			//break
		"tongue",
		"bellybutton", 
	],
};


export const allCategories = Object.keys(slotCategories);
export const allSlots = allCategories.reduce( (sum, key) => [...sum, ...slotCategories[key]], []);
//export const allCategories = ["UPPER","LOWER","UNDERWEAR","ARMS","LEGWEAR","SHOES","ACCESSORY","JEWELRY","PIERCINGS","WEAPONS","POTIONS"];
//export const allSlots = ["glasses", "hat","weapon","collar","necklace","belt","vest","upper","upperInner","upperOuter","upperArmor","lower","lowerOuter","lowerInner","lowerArmor","bra","panties","socks","shoes","gloves","gauntlets","neck","vambraces","greaves","pauldrons","earrings","bodychain","nose","noseSide","noseBot","noseTop","noseChain","lips","lipsSide","lipsBot","lipsTop","eyebrow","nipples","pubic","tongue","bellybutton","nipplesBot","nippleChain","hch","vch","labiaBot","labia","clit","hoodChain","wristRight","wristLeft"];
export const allPiercings =  ["earrings","nose","noseTop","noseBot","noseSide","noseChain","lips","lipsSide","lipsBot","lipsTop","eyebrow","nipples","pubic","tongue","bellybutton","nipplesBot","nippleChain","hch","vch","labiaBot","labia","clit","hoodChain"];
export const allArmor = ["upperArmor","lowerArmor","vambraces","pauldrons","greaves"];
export const allMakeup = ["lipstick","nails","mascara"];
export const allClothes = [ 
"hat",

"gloves",
"gauntlets",

"pauldrons",
"greaves",
"vambraces",
"neck",

"upperArmor",
"lowerArmor",

"upperOuter",
"lowerOuter",

"collar",
"vest",

"shoes",

"belt",
"socks",

"upper",
"lower",

"upperInner",
"lowerInner",

"bra",
"panties",
]; //IN UNDRESSING ORDER TODO



export const itemQuality = [ //TODO - should be here?????
	2, //0 ordina
	0, //1 second
	1, //2 cheap
	2, //3 fine
	3, //4 brandede
	4, //5 premium
	5, //6 designer
	3, //7 user created
];

export const itemQualityString = [ //TODO - should be here?????
	`ordinary`, //0
	`second-hand`, //1
	`cheap`, //2
	`ordinary`, //3
	`branded`, //4
	`premium`, //5
	`designer`, //6
	`created`, //7
];

export const itemQualityStar = [ 
	`★★`, //0 ordina
	``, //1 second
	`★`, //2 cheap
	`★★`, //3 fine
	`★★★`, //4 brandede
	`★★★★`, //5 premium
	`★★★★★`, //6 designer
	`★★★`, //7 user created
];



export const itemQualityModifier = [ 
	0, //0 ordina
	-1, //1 second
	-0.5, //2 cheap
	0, //3 fine
	0.5, //4 brandede
	1, //5 premium
	1.5, //6 designer
	0.5, //7 user created
];


