import * as da from "Libraries/da";

import {
	draw,
	animate,
} from "Avatar/canvas/draw";


export const emo = function(emotion = "neutral", subject = PC){
	if(SETTING.no_emotions) return;
	/*
	console.warn(da);
	console.warn(da.Expression);
	*/
	subject.applyExpression(da.Expression.create(da.Expression.neutral, 1));
	if(!subject.applyExpression) return; //??? wtf this means? 
	
	/*
		- *neutral* - resting bitch face
		- *relax*, *relaxed* - slight smile, good mood, default???
		- *anoy*, *annoyed* - slight frown, bad mood, 
		- *happy* - very good mood
		- *fine* - everything is fine, fake smile, currently *happy* //TODO
		- *angry* - pissed by something
		- *imp*, *impish* - mischievous, planning something evil or fun
		- *suspect*, *suspicious* - suspicious
		- *focus* - focused on task, determined //todo
		- *shy* - surprised by something embarrassing //TODO
		- *unco* - uncomfortable, reluctantly being forced something that makes you aroused, currently uses the same thing as *shy* //TODO
		- *help* - helpless, rezigned, hapless, currently uses *sad* //TODO
		- *shock*, "shocked" - surprised
		- *sad* - slightly sad
		- *oops* - realizing something will be bad, something between annoyed and surprised, currently uses *shy* //TODO
		- *cry* - very sad
		- *fake* - pretending to be extremely sad
		- *horny* - aroused
		- *bliss*, *ahegao* - blissful, orgasming
		- *sarc*, *sarcasctic* - something between annoyed and mischievous, currently *neutral** //TODO
		- *pain* - currently *sad* //TODO
		
		necessary?
		*sleep*
		- *cur*, *curious* - currently *neutral* //TODO
		impatient 
		maybe?
		- *pain*
		- *fine/*
		seductitive? 
		verythint is fine! fake smile 
		
	*/
	subject.emotion = emotion;
	
	switch(emotion){
		default:
				if(debug) console.warn(`WARNING - facial expression "${emotion}" NOT found`);
			break;
			
		case "cur":
		case "curious":
		case "sarc":
		case "sarcastic":
		case "neutral":
			//subject.applyExpression(da.Expression.create(da.Expression.neutral, 1));
			break;
			
		case "relaxed":
		case "relax":
			subject.applyExpression(da.Expression.create(da.Expression.happy, 0.45));
			break;
			
		case "anoy":
		case "annoyed":	
			subject.applyExpression(da.Expression.create(da.Expression.angry, 0.4));
			break;	
			
		case "fine":
		case "happy":
			subject.applyExpression(da.Expression.create(da.Expression.happy, 1.1));
			break;	
 
		case "angry":
			subject.applyExpression(da.Expression.create(da.Expression.angry, 0.95));
			break;	
		
		case "impish":
		case "imp":	
			subject.applyExpression(da.Expression.create(da.Expression.mischievous, 0.7));
			break;
		
		case "smug":	
			subject.applyExpression(da.Expression.create(da.Expression.mischievous, 1.1));
			break;
			
		case "suspicious":
		case "suspect":
			subject.applyExpression(da.Expression.create(da.Expression.suspicious, 0.9));
			break;
			
		case "focus":
			subject.applyExpression(da.Expression.create(da.Expression.suspicious, 0.4));
			break;
		
		case "oops":
		case "unco": //TODO
		case "shy":
			subject.applyExpression(da.Expression.create(da.Expression.surprised, 0.55));
			break;
		
		case "shocked":		
		case "shock":
				subject.applyExpression(da.Expression.create(da.Expression.surprised, 0.95));
				break;	
		
		case "pain":
		case "help":
		case "sad":
			subject.applyExpression(da.Expression.create(da.Expression.sad, 0.6));
			break;
			
		case "cry":
			subject.applyExpression(da.Expression.create(da.Expression.sad, 1.2));
			break;
			
		case "fake":
			subject.applyExpression(da.Expression.create(da.Expression.sad, 1.8));
			break;
			
		case "horny":	
			subject.applyExpression(da.Expression.create(da.Expression.aroused, 0.9));
			break;
			
		case "bliss":
		case "ahegao":		
			subject.applyExpression(da.Expression.create(da.Expression.bliss, 1));
			break;
		
		
		
		/*		
			case "horny1":	
			subject.applyExpression(da.Expression.create(da.Expression.aroused, 1));
			break;
			
			case "horny12":	
			subject.applyExpression(da.Expression.create(da.Expression.aroused, 1.2));
			break;
		
			case "shock11":
			subject.applyExpression(da.Expression.create(da.Expression.surprised, 1.1));
			break;
		
		
		
		case "imp1":	
			subject.applyExpression(da.Expression.create(da.Expression.mischievous, 1));
			break;
			
		
			
		case "angry8":
			subject.applyExpression(da.Expression.create(da.Expression.angry, 0.75));
			break;	
		

		

		case "cry1":
			subject.applyExpression(da.Expression.create(da.Expression.sad, 1));
			break;
			
		case "cry8":
			subject.applyExpression(da.Expression.create(da.Expression.sad, 0.8));
			break;
		*/	
			
			/*
		case "relax0":
			subject.applyExpression(da.Expression.create(da.Expression.happy, 0.25));
			break;
			
		
			
			
		case "happy8":
			subject.applyExpression(da.Expression.create(da.Expression.happy, 0.8));
			break;
			
		case "happy15":
			subject.applyExpression(da.Expression.create(da.Expression.happy, 1.5));
			break;
			
		
			
		
			
		
			*/
	}
		
		
	draw(subject);
	
}