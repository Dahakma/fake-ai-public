import {ra} from "Libraries/random";
import {and} from "Libraries/dh";
import {loot as LOOT} from "Data/items/loot";
import {INVENTORY, effigy, wear, updateDraw, create} from "Avatar/index";
//import {POTIONS} from "Avatar/game/potions";
import {garment} from "Data/items/blueprints/index";

import {txt} from "Loop/text";
import {wait} from "Loop/engine";
import {We} from "Virtual/text";
 
const booty = [];
/* 
	object with looted items 
	{
		blueprint - type of item armor, weapon, clothes, elixir, legendary, money 
		item - DAD object
		active - was not manipulated; could be clicked on to be instantly used/equiped
		desc - what is written on the screen
		effect - what happen when activated directly
		dibz - what happens when activated by follower 
	}
	
*/


export const loot_multiple = function(...array){
	booty.length = 0;
	array.forEach( a => actual(a) );
	display();
	
};


export const loot = function(...input){
	booty.length = 0;
	actual(input);
	display();
};


function display(){
	const desc = booty.map( a => a.desc );
	txt(`<span class = "span_loot_list"> ${We()} ${ra.array(["gained","looted"])} ${and(desc)} .</span>`);
	
	//TODO!!!! THIS IS VERY BAD!!!!!!! FIX ASAP!!!!!!
	//setTimeout( ()=> booty.forEach( (a, i) => document.getElementById(`div_loot_${i}`).addEventListener("click", a.effect) ), 200 );
	wait( ()=> booty.forEach( (a, i) => {
		if(booty[i].active){
			document.getElementById(`div_loot_${i}`).addEventListener("click", a.effect) 
		}else{
			document.getElementById(`div_loot_${i}`).className  = "neutral";
		}
	}) );
}


export const dibz = function(){

	const follower = ext.follower;
	if(!follower?.key) return;

	const subject = effigy[follower.key]; //TODO


	let weight = booty.map( a => {
		//TODO UPPER LOWER
		if(a.slot === "potion"){
			//console.log(POTION.id("eng"), a.item.seed)
			if(a.item.blueprint === "xHP"){
				return need_potion_attributes("hp") + 0.5; //boost because the character doesn't want to die
			}else if(a.item.blueprint === "xEng"){
				return need_potion_attributes("eng") - 0.5;
			}
			
			const need_potion_attributes = function(what){
				let desire = 0;
				let hesitation = 0;
				if(follower.dyno[what] <= 1){
					desire = 5;
				}else if(follower.dyno[what] < follower.base[what] / 4){
					desire = 4;
				}else if(follower.dyno[what] < follower.base[what] / 2){
					desire = 3;
				}else if(follower.dyno[what] < (follower.base[what] / 4) * 3){
					desire = 2;
				}else if(follower.dyno[what] >= follower.base[what]){
					desire = -5; 
				}else{
					desire = 0;
				}
				
				if(PC.dyno[what] <= 1){
					hesitation = 5;
				}else if(PC.dyno[what] < PC.base[what] / 4){
					hesitation = 4;
				}else if(PC.dyno[what] < PC.base[what] / 2){
					hesitation = 3;
				}else if(PC.dyno[what] < (PC.base[what] / 4) * 3){
					hesitation = 2;
				}else if(PC.dyno[what] >= PC.base[what]){
					hesitation = -1;
				}else{
					hesitation = 0;
				}
				
				return (desire - hesitation) + [-5, -1, -0.5, 1, 2, 3][follower.att.greed];
			}
			
			return [-5, -1, -0.5, 1, 2, 3][follower.att.chem]; 
		
		}else if(a.slot){
			//DESIRE - how much npc wants the item (better than items she already owns)
			let desire = 0;
			if(subject.slot[a.slot]){
				const item = subject.slot[a.slot];
							
				//TODO CONSIDER HP TOO
				if(a.price > item.price * 3){
					desire += 4;
				}else if(a.price > item.price * 2){
					desire += 3;
				}else if(a.price > item.price * 1.4){
					desire += 2;
				}else if(a.price > item.price){
					desire += 1;
				}
			}else{
				//TODO - useless items
				if(a.slot === "bra" && subject.getDim("breastSize") < 1){
					desire = 0;
				}else{
					desire = 3;
				}
			}
			
			//HESITATION  - how much npc assumes the player wants the item (and doesn't want to steal it for herself)
			let hesitation = 0;
			if(PC.slot[a.slot]){
				const item = PC.slot[a.slot];
							
				//TODO CONSIDER HP TOO
				if(a.price > item.price * 3){
					hesitation += 4;
				}else if(a.price > item.price * 2){
					hesitation += 3;
				}else if(a.price > item.price * 1.4){
					hesitation += 2;
				}else if(a.price > item.price){
					hesitation += 1;
				}
			}else{
				if(a.slot === "bra" && PC.getDim("breastSize") < 1){
					hesitation = 0;
				}else{
					hesitation = 2;
				}
			}
			
			return (desire - hesitation) + [-5, -1, -0.5, 1, 2, 3][follower.att.greed]; //adjustement based on character's greed
		
		
		}else{
			return 1; //TODO MONEY
		}
		
		//return true;
	})
	

	if(booty.length === 1){
		weight = weight.map( a => a - 1 );
	}else if(booty.length > 2){
		weight = weight.map( a => a + 1 );
	}

	if( !weight.some(a => a > 0) ) return;
	
	
	
	const what = booty[ra.weighted(weight)];
		
	if(what.item.slot === `potion`){
		txt(`@fol decided to drink <span class = "bold"> ${what.item.name}</span>. `);
	}else{
		txt(`@fol decided to keep <span class = "bold"> ${what.item.name}</span>. `);		
	}
	
	wait( ()=> what.dibz(subject) );

};





function actual(input){
	let loops = 1;
	 
	if(typeof input[0] == "number"){
		loops = input[0];
		input.shift(); //input.splice(0,1);
	}

	for(let i = 0; i < loops; i++)  add(input);
}


function add(input){
	const what = ra.array(input);
/*
console.warn(what);	
console.warn(ext.dungeon.loot);
console.warn(ext.dungeon);
console.warn(LOOT);

console.warn(LOOT[ext.dungeon?.loot]);

console.warn(LOOT[ext.dungeon?.loot]?.[what]);
*/
	const available = (()=>{
		if(LOOT[ext.dungeon?.loot]?.[what]){
			return LOOT[ext.dungeon.loot][what];
		}else if(LOOT[`act_${head.act}`]){

			return LOOT[`act_${head.act}`][what];
		}
	})();
		
	if(what === "money"){
		const coins = ra.integer(available[0], available[1]);
		ext.game_money += coins;
		booty.push({
			blueprint: `money`,
			item: {name: `${coins} coins`},
			desc: `<span class = "bold" id = "div_loot_${booty.length}"> ${coins} coins</span>`,
			dibz(subject){
				ext.game_money -= coins;
			},
		})
	}else{
				
		const blueprint = garment[what] ? what : ra.array(available);
		const item = create(blueprint);
		INVENTORY.add(item);
			 
		const index = booty.length;
		booty.push({
			blueprint: item.blueprint,
			item,
			desc: `<span class = "click" id = "div_loot_${index}"> ${item.name}</span>`,
			active: true,
			effect(e){
				if(booty[index].active){
					this.className  = "neutral";
					if(item.slot === "potion"){
						INVENTORY.drink(item, PC);
					}else{
						wear(item, PC, INVENTORY.get);
						updateDraw(PC);
					}
				}
				booty[index].active = false;
			},
			dibz(subject){
				document.getElementById(`div_loot_${index}`).className  = "neutral";
				if(item.slot === "potion"){			
					INVENTORY.drink(item, subject);
				}else{
					wear(item, subject, INVENTORY.get);
					updateDraw(subject);
				}
				booty[index].active = false;
			},
		})
	}
}



