import {createCharacter} from "../canvas/create";
import {draw} from "../canvas/draw";
import {NPC} from "System/variables";
import {rg} from "Libraries/random";
import {pick_one, changed_properties} from "Libraries/dh";
import {removeEverything, createMultiple, wearEverything, wearOutfit} from "../wardrobe/main";
import {updateMods, onlyChangedMods } from "Avatar/index";
import {update, updateDraw} from "../wardrobe/update";
import {
	showLess /*as updateShowLess*/,
	showAll /*as updateShowAll*/,
} from "../wardrobe/update";
import {default_PC, NAME} from "System/variables";
import {followers} from "Data/npcs/followers"; //TODO !!

import {avatars} from "Data/npcs/avatars";
import {uniforms} from "Data/items/uniforms";

import {set} from "Loop/set";



//only currently used 
const avatarsCache = {};
export function resetAvatarsCache(){
	for(const key in avatarsCache) delete avatarsCache[key];
}
export function resetClothesCache(){
	Object.keys(avatarsCache).forEach( a => {
		if(avatarsCache[a]?.clothes){
			avatarsCache[a].clothes = undefined; //TODO - delete property 
		}
	});
}




export function createPC(seed, bias, randomness){
	bias ??= avatars.kat.bias;
	randomness ??= avatars.kat.randomness;
	
	const pc = createCharacter(seed, bias, randomness);
	pc.key = "kat";

	//DIMENSIONS
	//absolute
	const absolute = avatars.kat?.absolute;
	if(absolute){
		Object.keys(absolute).forEach( a => {
			const value = Array.isArray(absolute[a]) ? rg.range(absolute[a][0], absolute[a][1]) : absolute[a]; //could be ether "value" or "[min_value, max_value]"
			const base = pc.basedim[a] ?? 0;
			pc.Mods[a] = value - base 
		});
	}
	//relative
	const relative = avatars.kat.relative;
	if(relative) Object.keys(relative).forEach( a => pc.Mods[a] += relative[a] );
	
	//more
	if(avatars.kat.adjust) avatars.kat.adjust.call(pc);

/*
	if(head.reality === 2){
		pc.Mods.irisSaturation += 20;
		pc.Mods.irisLightness += 10;
	}
*/
	pc.name = NAME.kat; //TODO!!!
	return pc;
}


export function createNPC(key, body){
	
	const {blueprint, seed} = (()=>{
		if(NPC[key].generic){
			return{
				blueprint: avatars[body],
				//seed is based on name; the same concrete npc should always have the same avatar; but different than other npc's reusing the same generic npc slot (i.e. NPC.npc_2)
				seed: (NPC[key]?.seed ?? rg.date) + ( NAME[key].charCodeAt(0) || 0 ) + ( NAME[key].charCodeAt(1) || 0 )  + ( NAME[key].charCodeAt(2) || 0 ),
			}
		}else{
			return{
				blueprint: avatars[key],
				seed: NPC[key]?.seed ?? rg.date, //TODO - it should be always defined!
			}
		}
	})();
	
	
	//get randomnes & bias
	const bias = blueprint?.bias ?? (NPC[key].sex === 1 ? 0.8 : -0.25);
	const randomness = blueprint?.randomness ?? 0.4;
	
	//create DAD object 
	const npc = createCharacter(seed, bias, randomness);
	
	
	
	//DIMENSIONS
	function apply_avatar_blueprint({absolute, relative, adjust}){
		//absolute
		if(absolute){
			Object.keys(absolute).forEach( a => {
				const value = Array.isArray(absolute[a]) ? rg.range(absolute[a][0], absolute[a][1]) : absolute[a]; //could be ether "value" or "[min_value, max_value]"
				const base = npc.basedim[a] ?? 0;
				npc.Mods[a] = value - base 
			});
		}
		
		//relative
		if(relative) Object.keys(relative).forEach( a => npc.Mods[a] += relative[a] );
		
		//more
		if(adjust) adjust.call(npc);
	}
	
	if(blueprint) apply_avatar_blueprint(blueprint);
	if(head.reality === 3 && blueprint?.game) apply_avatar_blueprint(blueprint.game);
	if(head.reality === 2 && blueprint?.meta) apply_avatar_blueprint(blueprint.meta);
	
	if(blueprint?.apex) npc.apex = {...npc.apex, ...blueprint.apex};
	
/*
	if(head.reality === 2){
		npc.Mods.irisSaturation += 20;
		npc.Mods.irisLightness += 10;
	}
*/

	
	npc.key = key; //remember the key
	npc.name = NAME[key]; //fucking iffy, used to distinguish generic npc_1 etc, only their names are different 

	return npc;
}




function returnCachedAvatar(key, body){
	if(!avatarsCache[key]) avatarsCache[key] = {};
	let subject;
	const reality = set.key;
	
	subject = avatarsCache[key]?.[reality];
	
	//if avatar is not cached or name is different (i.e. generic npc is reused) 
	if(!subject || NAME[key] !== subject.name){
		
		if(key === "ayy"){
			//create
			subject = createPC(PC.seed);
			subject.key = "ayy"; 
			subject.name = NAME.ayy;
			updateMods(subject, {Mods: ext.user});
			updateMods(subject, {Mods: ext.meta});
			Object.keys(PC.apex).forEach( a => subject.apex[a] = PC.apex[a])
		
		}else{
			//create
			subject = createNPC(key, body);
				
			//load saved npc properties 
			if(NPC[key].user){
				updateMods(subject, {Mods: NPC[key].user});
			}
			
			if(NPC[key][reality]){
				updateMods(subject, {Mods: NPC[key][reality]});
			}
			if(NPC[key].apex){
				Object.keys(NPC[key].apex).forEach( a => subject.apex[a] = NPC[key].apex[a])
			}		
		}

		update(subject); //TODO ??? necessary???? //attachBodyparts(subject);
		
		//save to cache
		avatarsCache[key].clothes = undefined;
		avatarsCache[key][reality] = subject;
	}
	
	return subject;
}






function saveAvatarMods(subject){
	const key = subject.key;
	
	if(key === "ayy") return; //AI just used PC's avatar, no point saving anything
	
	if(head.reality === 3){
		NPC[key].game = onlyChangedMods(subject); //changed_properties(dummy.Mods, subject.Mods);

		//is follower TODO CHECK IF WORKS
		if(ext.follower.key === subject.key){
			if(followers[key]?.perks && ext.follower.perks){
				NPC[key].perks = ext.follower.perks.filter( a => !followers[key].perks.includes(a) );
			}
			if(followers[key]?.skills && ext.follower.skills){
				NPC[key].skills = ext.follower.skills.filter( a => !followers[key].skills.includes(a) );
			}
		}
	}else if(head.reality === 2){
		NPC[key].meta = onlyChangedMods(subject);  //changed_properties(dummy.Mods, subject.Mods);
	}else{	
		NPC[key].irl = onlyChangedMods(subject); //changed_properties(dummy.Mods, subject.Mods);
	}
	
	
	let original_apex = default_PC.apex; //default for all NPCs
	if(avatars[key]?.apex) original_apex = {...original_apex, ...avatars[key].apex}; //NPC blueprint
	NPC[key].apex = changed_properties(original_apex, subject.apex);  //saves only those different from original ones (original ones does not have to be saved)
	
	
}






export function returnOriginalAvatar(key){ //so it could be edited without changed irl or meta or game properties
	if(key === "kat"){
		const subject = createPC(PC.seed, PC.bias, PC.randomness);
		for(const prop of Object.keys(ext.user) ) {
			subject.Mods[prop] += ext.user[prop];
		} 
		return subject;
	}else{
		const subject = createNPC(key);
		if(NPC[key].user){
			for(const prop of Object.keys(NPC[key].user) ) {
				subject.Mods[prop] += NPC[key].user[prop];
			}
		}
		return subject;
	}
}

export function saveOriginalAvatarMods(subject){
	//BEWARE! combined only with returnOriginalAvatar()!!
	if(subject.key === "kat"){
		ext.user = calculate(subject, createPC(PC.seed, PC.bias, PC.randomness) );
		NPC.ayy.user = ext.user; //AI looks the same
	}else{
		NPC[subject.key].user = calculate(subject, createNPC(subject.key) ); //changed_properties(dummy.Mods, subject.Mods);	
	}
	
	
	function round(b){
		return Math.round(b * 1000) / 1000;
	}
	function calculate(subject, original){
		const out = {};
		Object.keys(subject.Mods).forEach( prop => {
			const sub = round(subject.Mods[prop]);
			const org = round(original.Mods[prop]);
			if( sub !== org ) out[prop] = round( sub - org );
		});
		return out;
	}
	
}



/*
function avatar(key, clothes){ //TODO DISPLAY
	if(key === "kat") return; //TODO
	
//	alert(key);
//console.log(avatarsCache);
//console.warn(key, clothes)
	if(!key) return;
	
	const subject = returnCachedAvatar(key);

//console.log(subject)


	if(!clothes && !avatarsCache[key].clothes){ //undefined
		if(uniform[key]){
			get_dressed(key);
		}else if(avatarsCache[key].dad.sex === 1){
			get_dressed("female_generic");
		}else if(avatarsCache[key].dad.sex === 0){
			get_dressed("male_generic");
		}else{
			get_dressed("male_generic"); //TODO
		}
	}else if(clothes && clothes !== avatarsCache[key].clothes){ //redefined
		get_dressed(clothes);
	}else if(clothes === avatarsCache[key].clothes){ //the same stuff
		draw(subject);
	}else{
		draw(subject);
	}
	
		
	
	
	//PC = subject;
//console.log(subject)
	//return subject;
};



*/

//window.aaa = avatarsCache;

function get_dressed(subject, clothes){
	removeEverything(subject);
	let a = [];
	if(Array.isArray(clothes)){
		a = clothes
	}else if(uniforms[clothes]){
		const items = typeof uniforms[clothes] === "function" ? uniforms[clothes]() : uniforms[clothes];
		a = items.map( b => Array.isArray(b) ? pick_one(b) : b );	
	}
	a = createMultiple(a);
	wearEverything(a, subject);
	avatarsCache[subject.key].clothes = clothes;
}

export const effigy = {
	reset(key){
		if(avatarsCache[key]){
			if(avatarsCache[key].game) avatarsCache[key].game = undefined;
			if(avatarsCache[key].irl) avatarsCache[key].irl = undefined;
			if(avatarsCache[key].meta) avatarsCache[key].meta = undefined;
		}
		this.grab(key);
	},

	save(key){
		const subject = this.grab(key);
		saveAvatarMods(subject);
	},
	
	
	original(key){ //TEMPFIX
		return returnOriginalAvatar(key);
	},
	
	saveOriginal(subject){ //TEMPFIX
		saveOriginalAvatarMods(subject);
	},
	
	
	grab(key, clothes, body){
		if(key === "kat") return; //TODO
		if(!key) return;
		
		if(clothes === undefined) clothes = head.present?.[key]?.clothes;
		if(body === undefined) body = head.present?.[key]?.body;
					
		const subject = returnCachedAvatar(key, body);

		if(clothes === "nude"){
			removeEverything(subject);
			avatarsCache[key].clothes = "nude";
		}else if(!clothes/*clothes === undefined*/ && !avatarsCache[key].clothes){ //undefined
			//load followers clothes
			if(ext.follower?.key === key && ext.outfits.find( a=> a.id === `npc_${key}`) ){
				wearOutfit(`npc_${key}`, subject, undefined, true);
				avatarsCache[subject.key].clothes = "follower";
			//load uniform with the same key
			}else if(uniforms[key]){
				get_dressed(subject, key);
			//load generic clothes
			}else if(subject.sex === 1){
				get_dressed(subject, "female_generic");
			}else if(subject.sex === 0){
				get_dressed(subject, "male_generic");
			}else{
				get_dressed(subject, "male_generic"); //TODO
			}
		}else if(clothes && clothes !== avatarsCache[key].clothes){ //redefined
			get_dressed(subject, clothes);
		}else if(clothes === avatarsCache[key].clothes){ //the same stuff
		}else if(clothes === undefined){
			//the most common thing after the initiation; nothing happens
		}else{
			//if(debug) alert(`effigy.grab - ELSE - should not happen, why? `);
		}
		
		
		
		//TODO - probably should be a constructor
		subject.showLess = function(top, bot, leg, jew){
			showLess(top, bot, leg, jew, subject);
		};
		
		subject.showAll = function(top, bot, leg, jew){
			showAll(subject);
		};
		
		subject.draw = function(top, bot, leg, jew){
			draw(subject);
		};
		
		return subject;
	},
	
	
	redress(key, clothes){
		clothes ??= avatarsCache[key].clothes;
		avatarsCache[key].clothes = undefined;
		this.grab(key, clothes);
	},
	
	/*
	showLess(key, top, bot, leg, jew){
		updateShowLess(top, bot, leg, jew, this[key]);
	},
	*/
	
	showAll(key){
		Object.keys(avatarsCache).forEach(  a => showAll(effigy[a])  );
		/*
		if(key){
			updateShowAll(this[key]);
		}else{
			Object.keys(avatarsCache).forEach(  a => updateShowAll(effigy[a])  );
		}
		*/
	},
	
}


Object.keys(NPC).forEach( key => {
	//TODO - probably should be a constructor
	
	Object.defineProperty(effigy, key, {
		get(){ return this.grab(key) },
	});

	if(NPC[key].altname){
		Object.defineProperty(effigy, `${key}alt`, { //katalt
			get(){ return this.grab(key) },
		});
	}
	
	if(!NPC[key].prefix && NPC[key].surname){
		Object.defineProperty(effigy, `${key}sur`, { //katsur
			get(){ return this.grab(key) },
		});
	}
	
	if(NPC[key].gamename){
		Object.defineProperty(effigy, `${key}game`, { //katgame
			get(){ return this.grab(key) },
		});
	}
})





