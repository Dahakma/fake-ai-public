import * as DA from "Libraries/da";
import {attachPart} from "../canvas/body";
import {tattoo} from "../wardrobe/tattoo";

//TODO - speedup by checking whether its necessary 
export function attachBodyparts(subject = PC){
	const ax = subject.apex;
	
	
	//TATTOOS
	while(subject.tattoos.length > 0){
		subject.removeTattoo(subject.tattoos[0]);
	}
	Object.keys(ax).forEach( key => {
		if(head.reality === 3){
			if( key.startsWith(`game_tat_`) ){
				tattoo(ax[key], key.replace(`game_tat_`,``), subject);
			}
		}else{
			if( key.startsWith(`tat_`) ){
				tattoo(ax[key], key.replace(`tat_`,``), subject);
			}
		}
	});
	
	/*
		Object.keys(figurine.apex).forEach( key => {
			if( key.startsWith(prefix) ) delete figurine.apex[key];
		} ); //clears all previous tatttoos
	*/
	
	
	function add(what, mod){
		attachPart(what, subject, mod);
	}
//console.log(ax.irl_lips)	
	if(head.reality === 3){
		if(ax.ears === "cat"){
			add("CatEars");
		}else if(ax.ears === "elf"){
			add("Ears");
		}
			
		if(ax.horns === "curvy"){
			add("CurvedHorns", {
				fill: "hsla(360, 100%, 7%, 1)",
			});
		}else if(ax.horns === "straight"){
			add("StraightHorns", {
				fill: "hsla(360, 100%, 27%, 1)",
				stroke: "hsla(360, 100%, 27%, 1)",
			});
		}
		
		if(ax.pupils === "cat"){
			add("PupilCat");
		}
	
		if(ax.hoofs){
			add("Hoofs");
		}

		if(ax.tail === "demo"){
			add("DemonicTail");
		}else if(ax.tail === "cat"){
			add("CatTail");
		}
		
		if(ax.wings){
			add("DemonicWings");
		}
		

		//MAKEUP - TODO MESSY
		if(ax.game_lips){
			subject.lipstick = DA.Clothes.create(DA.wLipstick, {
				fill: ax.game_lips,
			});
		}else{
			subject.lipstick = undefined;
		}
		
		if(ax.game_nails){
			subject.nails = DA.Clothes.create(DA.Nails, {
				fill: ax.game_nails,
			});
		}else{
			subject.nails = undefined;
		}
		
		if(ax.game_mascara_top){
			subject.mascara = DA.Clothes.create(DA.Mascara, {
				botFill: ax.game_mascara_bot,
				topFill: ax.game_mascara_top,
			});
		}else{
			subject.mascara = undefined;
		}
		
		
	}else{
		if(ax.ears !== "elf"){
			add("ElfEars");
		}
		
		if(ax.pupils === "cat"){
			add("Pupil");
		}
	
		if(ax.hoofs){
			add("Feet");
		}
		
		//MAKEUP - TODO MESSY 
		if(ax.irl_lips){
			subject.lipstick = DA.Clothes.create(DA.wLipstick, {
				fill: ax.irl_lips,
			});
		}else{
			subject.lipstick = undefined;
		}
		
		if(ax.irl_nails){
			subject.nails = DA.Clothes.create(DA.Nails, {
				fill: ax.irl_nails,
			});
		}else{
			subject.nails = undefined;
		}
		
		if(ax.irl_mascara_top){
			subject.mascara = DA.Clothes.create(DA.Mascara, {
				botFill: ax.irl_mascara_bot,
				topFill: ax.irl_mascara_top,
			});
		}else{
			subject.mascara = undefined;
		}
		
	}
	
}