
import {DISPLAY} from "Gui/gui"; //TODO - REFERS TO THE OLD sys

//import {div_subtext} from "System/gui_skeleton";

import {ra} from "Libraries/random";

//import {inventory} from "../wardrobe/inventory";
import {INVENTORY} from "./inventory";
import {randomClothes, randomGarment, randomOutfit} from "../wardrobe/random";
import {createMultiple, wear} from "../wardrobe/main";
import {allClothes} from "../wardrobe/all";
import {wearOutfit, saveWornOutfit, loadMakeup} from "../wardrobe/main";

import {wearEverything, removeEverything, removeActual} from "../wardrobe/main";
import {updateDraw, update} from "../wardrobe/update";
import {draw} from "../canvas/draw";
import {filters} from "../game/rules";
import {sluttiness} from "../wardrobe/appearance";

import {uniforms} from "Data/items/uniforms";

import {savefile_loading_in_progress} from "System/save";


export function randomMakeup(subject = PC, rerender){
	const ids = ext.makeups.filter( a => a.reality === head.reality && a.random ).map( a => a.id );
	loadMakeup(ra.array(ids), subject, rerender);
}



export const countSpecificGarments = function(slot, filter){
	let array = INVENTORY.get.filter( a => a?.slot === slot); //clones && removes undefined
	if(filter) array = array.filter( a => filter(a) );
	return array.length;
}


//QUCK SAVE & LOAD
/*
export function quickLoadOutfit(conjure = false, strip = false, subject = PC){
	if(strip) removeEverything(subject, undefined);
	wearOutfit(`temp_${subject.key}`, subject, INVENTORY.get, conjure)
}
*/
export function quickLoadOutfit(inventory = false, subject = PC){
	if(savefile_loading_in_progress) return; //result of quickLoadOutfit was saved; load it is enought, does not have to run for the second time
	
	if(inventory){	
		wearOutfit(`temp_${subject.key}`, subject, INVENTORY.get);
	}else{
		removeEverything(subject, undefined);
		wearOutfit(`temp_${subject.key}`, subject, undefined, true);
	}
	//wearOutfit(`temp_${subject.key}`, subject, INVENTORY.get, conjure);
}

export function quickTempOutfit(blueprints, subject = PC){
	if(savefile_loading_in_progress) return; 
	
	if(typeof blueprints === "string") blueprints = uniforms[blueprints];
	if(!blueprints && !Array.isArray(blueprints) ) return; //error, better cancel right here than end up with half-naked avatar

	quickSaveOutfit(subject);
	removeEverything(subject, undefined);
	
	const garbs = createMultiple(blueprints);
	wearEverything(garbs, subject, undefined);
	draw(subject); 

}


export function quickSaveOutfit(subject = PC){
	if(savefile_loading_in_progress) return; 
	saveWornOutfit(`temp_${subject.key}`, `temp saved outfit (${subject.key}) (auto)`, subject, /*head.reality*/0);	
}


//WEAR RANDOM

//TODO - IMPORTANT IN NEW FEATURE, CHECK TEST THINK ABOUT, TEMPFIX, probably move to randomGarment
export function wearRandomGarment(slot, type = "", rules = false, force = false){
	if(typeof type === "boolean"){
		force = rules;
		rules = type;
		type = "";
	}
	
	let piece = randomGarment(INVENTORY.get, PC, slot, type, rules);
	if(!piece && force) piece = randomGarment(INVENTORY.get, PC, slot, type, false);
	
	wear(piece, PC, INVENTORY.get);
	draw(PC);
}



export function wearRandomClothes(type = "", rules = false){
	if(typeof type === "boolean"){
		rules = type;
		type = "";
	}
	
	removeEverything(PC, INVENTORY.get);
	const clothes = randomClothes(INVENTORY.get, PC, type, rules);
	wearEverything(clothes, PC, INVENTORY.get);
	draw(PC);
}


export function wearRandomBrutalClothes(slut, type = "", rules = false, force = false){
	if(typeof type === "boolean"){
		rules = type;
		type = "";
	}
	
	removeEverything(PC, INVENTORY.get);
	const clothes = randomBrutalClothes(slut, 10, INVENTORY.get, PC, type, rules)
	wearEverything(clothes, PC, INVENTORY.get);
	draw(PC);
}


export function randomBrutalClothes(slut = ext.slut, attempts = 5, inventory, subject, type, rules){
	if(typeof type === "boolean"){
		rules = type;
		type = "";
	}

	
	attempts = 4; //TODO - TEMPFIX
	
	const items = [];
	
	
	for(let i = 0; i < attempts; i++){
		let clothes;
		let value;
		let diverg;
		const sub_attempts = 4; // attempts; //Math.ceil(attempts / 2);
		let again = true;

		for(let k = 0; k < sub_attempts && again; k++){
			again = false;
			removeEverything(PC);
			
			clothes = randomClothes(inventory, PC, type ? type : "advanced", rules, slut); //TODO BOZO??!!??
			wearEverything(clothes, PC);
		
			value = sluttiness(PC).details.slut;
			diverg = Math.abs(slut - value);
			

			if(diverg > 1) again = true;

			
			
			//panties under lower
			if(!again && PC?.lower?.legCoverage < 0.13 && PC?.panties?.waistCoverageLower < -0.5){
				again = true;
				console.log("panties!!");
			}
			
		}
		
		
		//belt with open zipper
		if(PC.belt && PC?.lower.open > 0){
			clothes = clothes.filter( item => item.slot !== "belt" );	
			console.log(clothes);	
		}
		
		removeEverything(PC);
		
		items.push({
			clothes,
			value,
			diverg,
		})
		
		
		if(diverg === 0) break;
		
	}
	
	items.sort( (a, b) => a.diverg - b.diverg );
	return items[0].clothes;
}

export function wearRandomOutfit(rules = false){
	const id = randomOutfit(INVENTORY.get, rules);
	wearOutfit(id, PC, INVENTORY.get );
}
	





/*
export function brutalExact(slut = 5, attempts = 10, inventory, subject){
	const items = [];
	
	for(let i = 0; i < attempts; i++){
		const clothes = randomClothes(inventory, PC, "bozo", false);
		wearEverything(clothes, PC);
		
		
		const value = sluttiness(PC).value;
		const diverg = Math.abs(slut - value);
		items.push({
			clothes,
			value,
			diverg,
		})
		removeEverything(PC);
	}
	
	items.sort( (a,b) => a.diverg - b.diverg );
	return items[0].clothes;
}
*/			
				

export function toUnderwear(subject = PC){
	allClothes.filter( a => !["bra", "panties", "upperInner", "lowerInner", "shoes"].includes(a) ).forEach( a => {
		removeActual(a, subject, INVENTORY.get);
	})
	updateDraw(subject);
}

export function toSexyTime(subject = PC){
	allClothes.filter( a => !["bra", "socks"].includes(a) ).forEach( a => {
		removeActual(a, subject, INVENTORY.get);
	})
	updateDraw(subject);
}

export function toNudity(subject = PC){
	allClothes.filter( a => ![""].includes(a) ).forEach( a => {
		removeActual(a, subject, INVENTORY.get);
	})
	updateDraw(subject);
}


export function removeUnsexyClothesFromInventory(){
	Object.keys(ext.rules).filter(
		a => ext.rules[a], //only active rules (=== true)
	).forEach( b => {	//BEWARE ext.rules!!
		if(!filters[b]) return console.warn(`Filter for clothes "${b}" NOT found!`);
		PC.inventory = filters[b](PC.inventory); //BEWARE - won't work with inventory.get because assigment
	})	
}





 
function checkItemColor(a, c){
	return (a.hue > c[0][0] && a.hue < c[0][1]) && (a.satur > c[1][0] && a.satur < c[1][1]) && (a.light > c[2][0] && a.light < c[2][1]);
}
		
export function wearsPink(){
	const output = [];
	
	//TODO - temporarily, SHOULD BE AUTOMATED TO GRAB DATA FROM EXGAR & COLOR
	const barbie =[[295, 335], [75, 100], [40, 60]];
	const cute =  [[325, 345], [50, 100], [70, 90]];
	const barcuta =  [[295, 345], [50, 100], [40, 90]];
	
	allClothes.filter( a => PC[a] ).forEach( a => {
		if( checkItemColor(PC[a], barcuta) ){
			output.push(a);
		}
	})
	
	return output;
}
		
		
export function crew(...array){
	removeEverything(PC, INVENTORY.get );
	array = array.map( a => Array.isArray(a) ? ra.array(a) : a );
	const garbs = createMultiple(array);
	wearEverything(garbs, PC, INVENTORY.get );
	draw(PC); 
}


export function npcrew(array, subject = PC, use_inventory = false){
	if(use_inventory){
		removeEverything(subject, INVENTORY.get );
	}else{
		removeEverything(subject);
	}
	array = array.map( a => Array.isArray(a) ? ra.array(a) : a );
	const garbs = createMultiple(array);
	if(use_inventory){
		wearEverything(garbs, subject, INVENTORY.get );
	}else{
		wearEverything(garbs, subject);
	}
	draw(subject); 
}


