/**
	rules of what clothes PC can wear
*/

import {wears, garmentIs} from "../wardrobe/types";
import {
	stockingsLegCoverage,
	shortSkirtLegCoverage,
} from "../wardrobe/constants";

/**
	violates.show_legs - checks whether the clothes PC is currenly wearing violate the rules 
	filters.show_legs(array) -  removes from the array banned items
*/

const rules = {
	collar: {
		name: `wear collar`,
		check(subject = PC){
			return ext.rules.collar && !wears.collar; 
		},
		filter(input){
			return input.filter( a => {
				if(a.slot !== "collar") return true;
				return garmentIs.collar(a) ;  //chokers aren't collars (even thought they are slot collars)
			});
		},
	},
	
	choker: {
		name: `wear choker`,
		check(subject = PC){
			return ext.rules.choker && !(wears.collar || wears.choker); //!subject.collar //TODO add wears.choker && COLLAR
		},
		filter(input){
			return input;
		},
	},
	
	show_legs: {
		name: `show legs`,
		check(subject = PC){
			return ext.rules.show_legs && (subject.lower || wears.dress) && !(wears.shortSkirt || wears.shorts);
		},
		filter(input){
			return input.filter( a => {
				if(a.slot !== "lower" && a.slot !== "upperLower") return true;
				return garmentIs.shortSkirt(a) || garmentIs.shorts(a);
			});
		},
	},
	
	short_skirt: {
		name: `wear short skirts`,
		check(subject = PC){
			return ext.rules.short_skirt && (subject.lower || wears.dress) && !wears.shortSkirt;
		},
		filter(input){
			return input.filter( a => {
				if(a.slot !== "lower" && a.slot !== "upperLower") return true;
				return garmentIs.shortSkirt(a);
			});
		},
	},
	
	sexy_top: {
//TODO - atm includes minidresses too
		name: `wear sexy tops`,
		check(subject = PC){
			return ext.rules.sexy_top && !wears.sexyTop;
		},
		filter(input){
			return input.filter( a => {
				if(a.slot !== "upper" && a.slot !== "upperLower") return true;
				return garmentIs.sexyTop(a);
			});
		},
	},
	
	no_bra: {
		name: `no bras`,
		check(subject = PC){
			return  ext.rules.no_bra && subject.bra;
		},
		filter(input){
			return input.filter( a =>  a.slot !== "bra" )
		},
	},
	
	no_panties: {
		name: `no panties`,
		check(subject = PC){
			return  ext.rules.no_panties && subject.panties;
		},
		filter(input){
			return input.filter( a =>  a.slot !== "panties" );
		},
	},
	
	sexy_panties: {
		name: `wear sexy panties`,
		check(subject = PC){
			return  ext.rules.sexy_panties && subject.panties && !wears.sexyPanties
		},
		filter(input){
			return input.filter( a => {
				if(a.slot !== "panties") return true;
				return garmentIs.sexyPanties(a);
			});
		},
	},	
	
	sexy_legwear: {
		name: `wear sexy legwear`,
		check(subject = PC){
			return  ext.rules.sexy_legwear && /*subject.socks &&*/ !wears.sexyLegwear; 
		},
		filter(input){
			return input.filter( a => {
				if(a.slot !== "socks") return true;
				return garmentIs.sexyLegwear(a);
			});
		},
	},
	/*
	no_socks
	sexy_stockings
	*/
}


export const filters = {};
export const violates = {};
Object.keys(rules).forEach( a => {
	Object.defineProperty(violates, a, {
		get(){ return rules[a].check() },
	});
	filters[a] = rules[a].filter;
});

violates.report = function(subject = PC){
	let report = ``;
	
	Object.keys(rules).forEach( key => {
		if(rules[key].check(subject)) report += `<li>${rules[key].name}`;
	});
		
	return report ? `<ul>${report}</ul>` : "";
};


Object.defineProperty(violates, "severity", {
	get(){ 
		let severity = 0;
	
		if(this.short_skirt && !wears.skirt){ //short skirt but wearing pants 
			severity += 1;
		}else if(this.short_skirt || this.show_legs && wears.mediumSkirt){ //wearing medum skirt
			severity += 0.5;
		}else if(this.short_skirt || this.show_legs){
			severity += 1;
		}
		
		if(this.sexy_top){
			severity += 0.5;
		}
		if(this.sexy_legwear){
			severity += 0.5;
		}
		if(this.no_bra){
			severity += 0.5;
		}
		if(this.choker){
			severity += 0.5;
		}
		
		return severity;
	},
});
	
	
