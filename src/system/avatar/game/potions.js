
import {rg, ra} from "Libraries/random";
import {percent, capitalise, clone, gradual} from "Libraries/dh";
import {createTransformation, transformAndShow} from "Libraries/da";
import {popup, DISPLAY} from "Gui/gui"; 
import {createCharacter, draw, animate, update, updateDraw, cloneCharacter, cloneOriginalCharacter, updateMods} from "Avatar/index";
import {EFFECTS} from "Virtual/effects";

import {constants} from "Data/constants";
import {elixirs} from "Data/items/elixirs/index";
const list = Object.keys(elixirs);
let write = ``;
/*
transformation_speed
learn_maximal
learn_minimal
*/

export const POTIONS = {
	initiate(seed){
		rg.i(seed);
		const tinctures = rg.shuffle(["red","orange","maroon","yellow","amber","green","lime","cyan","azure","blue","teal","indigo","magenta","pink","viole","white","black","gray"]);
		list.forEach( (key, index) => elixirs[key].color = tinctures[index] );
		/* eslint-disable no-console */
			console.log(`>Potions: `);
			console.table(elixirs, ["name", "color", "note"])
		/* eslint-enable no-console */
	},
	
	name(key, force){
		const actual = elixirs[key].name;
		return (ext.potions[key] || force) ? elixirs[key].name : `${elixirs[key].color} elixir ${softbug ? `(${elixirs[key].name})` : ``}`;
	},
	
	
	
	
	color(key){
		return elixirs[key].color;
	},
	
	//redundant?
	knows(key){
		return ext.potions[key];
	},
	
	
	drink(item, subject = PC, inventory, back = DISPLAY.text){

		const key = typeof item === "object" ? item.blueprint : item;
		const player = subject.key === "kat"; //player or npc
		write = ``;


		//STAT - saves to stats of drink potions
		if(player){
			ext.stats.potions[key] ??= 0; //TODO
			ext.stats.potions[key]++; 
		}
		
		
		//LEARN - chance to learn the potions
		if(!ext.potions[key]){
			const base = gradual([constants.elixirs.learn_minimal, constants.elixirs.learn_maximal], ext.stats.potions[key], [0, 1]);
			const random = Math.random();
			if(base > random){
				write += `I learned the ${elixirs[key].color} potion was ${capitalise(elixirs[key].name)}! <br>`;
				ext.potions[key] = true;
			}
		}
		
		//ORIGINAL - stupid thing used to humanium which returns you to the original state, needlesly complicated
		const og = cloneOriginalCharacter(subject);
		//if(player) updateMods(og, ext.original);
		subject.original = og.Mods;
		
		
		//AVAILABLE TRANSFORMATIONS
		const dim = (()=>{
			const output = {};
			const keys = [...(new Set([...Object.keys(subject.basedim), ...Object.keys(subject.Mods)]))];
			keys.forEach( key => output[key] = (subject.basedim[key] ?? 0) + (subject.Mods[key] ?? 0) );
			return output;
		})();

		const available = elixirs[key].trans.map( a => a.condit(dim, subject) );
	
		
		//EFFECTS
		if(elixirs[key].effect){
			write += `<p>${player ? "I" : subject.name} gained temporal <i>${EFFECTS.add(elixirs[key].effect, subject).name}</i> effect. </p>`;			
		}
 
 
 
		//MANUAL SELECTION OF EFFECTS
		//only one 
		if(elixirs[key].trans.length === 1){
			selected(0);
		//manual - cheatmode or the potion is known
		}else if(ext.potions[key] || SETTING.alchemist){
			const array = [{
				text: "Random",
				fce: random,
			}]
			elixirs[key].trans.forEach( (a, i)=>{
				if(available[i] <= 0) return;
				array.push({
					text: capitalise(a.name),
					fce: ()=> selected(i),
				})
			});
			
			if(hardbug){
				elixirs[key].trans.forEach( (a, i)=>{
					array.push({
						text: `##(${percent(available[i])}) ${a.name}`,
						fce: ()=> selected(i),
					})
				});
			}	
			
			popup.options("Select effect: ", array);

		//unknown potion - random 
		}else{
			random();
		}
		
		
		function random(){
			const index = ra.weighted(available);
			selected(index);
		}
		
		function selected(index){
			//index === undefined - no availabele transformation
			if(index) write += `<p>${player ? elixirs[key].trans[index].desc : elixirs[key].trans[index].npc(subject.name, subject.sex)}</p>`;			
			POTIONS.execute(key, index, subject, dim);
			if(write) popup.alert(write);
			write = "";
		}
		
	},
	
	execute(key, index, subject = PC, dim){
		dim = dim ?? (()=>{
			const output = {};
			const keys = [...(new Set([...Object.keys(subject.basedim), ...Object.keys(subject.Mods)]))];
			keys.forEach( key => output[key] = (subject.basedim[key] ?? 0) + (subject.Mods[key] ?? 0) );
			return output;
		})();
		
		const trans = elixirs[key].trans[index]?.execute ? elixirs[key].trans[index].execute(dim, subject) : {};
		
		const Mods = {};
		if(trans.relative) Object.keys(trans.relative).forEach( key => Mods[key] = trans.relative[key] );
		if(trans.absolute) Object.keys(trans.absolute).forEach( key => Mods[key] = trans.absolute[key] - dim[key] );
		
		const transformation = {Mods};
		Object.keys(trans).forEach( key => {
			if( ["relative", "absolute", "apex"].includes(key) ) return;
			transformation[key] = trans[key];
		});
		
		const apex_changed = (()=>{
			if(!trans.apex) return false;
			for(const key of Object.keys(trans.apex) ){
				if(trans.apex[key] !== subject.apex[key]) return true;
			}
			return false;
		})();
			
	
		if(apex_changed){
						
			if(trans.relative || trans.absolute){
				const zero = {};
				Object.keys(transformation).forEach( key => {
					zero[key] = {};
					if(key === "Mods"){
						Object.keys(transformation[key]).forEach( a => zero[key][a] = 0 - dim[a] );
					}else{
						Object.keys(transformation[key]).forEach( a => zero[key][a] = 0 - subject[key][a] );
					}
				})
				
				//Object.keys(Mods).forEach( key => zero[key] = 0 - dim[key] );
				animate(subject, zero, constants.elixirs.transformation_speed / 2, ()=>{
					Object.keys(trans.apex).forEach( key => subject.apex[key] = trans.apex[key] );
					updateDraw(subject);
					animate(subject, transformation, constants.elixirs.transformation_speed);
				});
			
			}else{
				Object.keys(trans.apex).forEach( key => subject.apex[key] = trans.apex[key] );
				updateDraw(subject);
			}
		}else{
			if(trans.relative || trans.absolute){
				animate(subject, transformation, constants.elixirs.transformation_speed);
			}
		}
		
		if(elixirs[key].trans[index]?.fce) elixirs[key].trans[index].fce(subject);
		
		
	},
}
	
	
	
	
	




 
//export let tinctures = [];


//export const circa = function(a, b, -2){




/*	
export const gradual = function(limit, value, [min, mid, max], include){
	if(limit.length === 2){
		limit[2] = limit[1];
		limit[1] = (limit[2] + limit[0]) * 0.5;
	};
	if(max === undefined){
		max = mid;
		mid = (min + max) * 0.5;
	};
	
	if(value < limit[1]){
		value -= limit[0];
		if(value <= 0){
			if(include === 1 || include === 0) return 0;
			return min;
		}
		const percent = value / (limit[1] - limit[0]);
		return min + (percent * (mid - min));
		
	}else{
		value -= limit[1];
		if(value > limit[2]){
			if(include === -1 || include === 0) return 0;
			return max;
		}
		const percent = value / (limit[2] - limit[1]);
		return mid + (percent * (max - mid));
	}
}
*/
/*


let dummy;
let reality;
export const brewery = function(back){
	reality = head.reality;
	head.reality = 3;
	
 
		
	dummy = createCharacter(undefined, 0.7, 0.5); //cloneCharacter(PC);
	dummy.original = clone(dummy.Mods);
	
	//dummy.body = clone(default_ext.body);
	updateDraw(dummy);
	
	
	function reset(){
		dummy = cloneOriginalCharacter(dummy);
		//dummy.body = clone(default_ext.body);
		updateDraw(dummy);
	}
		
		
	function menu(){
		DISPLAY.subtext();
		container("one_column");
		
		list.forEach( (a, i) => {
			if(a.support) return;
			link(`${capitalise(a.name)}.`, ()=> effects(i), true) 
		});
		link(`Reset. `, reset, true);
		link(`Exit. `, ()=>{
			head.reality = reality;
			//DISPLAY.text(); //TODO
			//draw();
			back();
		}, true);
	}
	
	
	function effects(index){
		container("one_column");
		const available = list[index].effects.map( a => a.condit( a => dim(a, dummy, dummy.body), dummy, dummy.body ) );
		list[index].effects.forEach( (a, i)=>{
				link(`(${percent(available[i])}) ${capitalise(a.name)}. `, ()=>{
					container("one_column");
				//	write(a.desc);
					POTION.execute(index, i, dummy, dummy.body);
					link(`${capitalise(list[index].name)}. `, ()=> effects(index), true);
					link(`Index. `, menu, true);
				}, true)
		}, true);
		link(`Index. `, menu, true);
	}
		
		
	menu();
}
	
	
let write = "";
	
	
	
	
	
	
export const POTION = {
	
	id(name){
		name = name[0] === "x" ? name.substring(1) : name;
		name = name.toLowerCase();
		return Object.values(elixirs).find( a => a.name.startsWith(name) || a.alias?.startsWith(name) ).id;
	},
	
	name(id){
		return ext.potions[id] ? `${list[id].name}` : `${list[id].color} elixir ${softbug ? "("+list[id].name+")" : ""}`;
	},

 
	
	color(id){
		return list[id].color;
	},
	
	real_name(id){
		return list[id].name;
	},	




	//drink(id, subject = PC, inventory, back = DISPLAY.text){
	drink(item, subject = PC, inventory, back = DISPLAY.text){
		const id = typeof item === "object" ? item.id : id;
		write = "";
		 
		
		//gui
//DISPLAY.subtext();
//		container("one_column");
		
		//PC.original = ext.original; //TODO!!!!!!
		//TEMPFIX TODO TODO 
		
		
		const og = cloneOriginalCharacter(subject);
		updateMods(og, ext.original);
		subject.original = og.Mods;
		
		//available effects
		const available = list[id].effects.map( a => a.condit( a => dim(a, subject), subject ) );
		
		//stats
		if(subject.key === "kat"){
			ext.stats.potions[id] ??= 0; //TODO
			ext.stats.potions[id]++; 
		}
		
		//gradual([minimal_potions, maximal_potions], ext.stats.potions[id], [0, 1])
		
		
//console.trace(); 
		if(!ext.potions[id]){
			const base = gradual([minimal_potions, maximal_potions], ext.stats.potions[id], [0, 1]);
			const random = Math.random();
//console.warn(ext.stats.potions[id], base, random);
			if(base > random){
			//if(true){
				write += `I learned the ${list[id].color} potion was ${capitalise(list[id].name)}! <br>`;
				//popup.alert(`I learned the ${list[id].color} potion was ${capitalise(list[id].name)}!`)
				ext.potions[id] = true;
			}
		}

//alert();		
		//only one 
		if(list[id].effects.length === 1){
			selected(0);
		//manual - cheatmode or the potion is known
		}else if(ext.potions[id] || SETTING.alchemist){
		//}else if(true){
			const array = [{
				text: "Random",
				fce: random,
			}]
			list[id].effects.forEach( (a, i)=>{
				if(available[i] <= 0) return;
				array.push({
					text: capitalise(a.name),
					fce: ()=> selected(i),
				})
			});
			if(hardbug){
				list[id].effects.forEach( (a, i)=>{
					array.push({
						text: `##(${percent(available[i])}) ${a.name}`,
						fce: ()=> selected(i),
					})
				});
			};	
			console.log(array)
			popup.options("Select effect: ", array);
	 
		//unknown potion - random 
		}else{
			random();
		}
		
		
		function selected(i){
			//console.error(i);
			//container();
			console.log(write);
			write += subject.key === "kat" ? list[id].effects[i].desc : list[id].effects[i].npc(subject.name, subject.sex);
			POTION.execute(id, i, subject);

			popup.alert(write);
			write = "";
			//link_esc(`Done. `, back, true);
		}
		
		function random(){
			//console.log(available);
			const i = ra.weighted(available);
			//TODO - returned undefined??
			//console.log(i);
			selected(i);
		}
		
	},
	execute(id, i, subject = PC){
		const eff = list[id].effects[i];
		//console.warn(subject);
		//console.log( eff.mods( a => dim(a, subject, bodyparts)  ) );
		
		if(list[id].perk){//TODO SHOULD NOT BE CALLED BERK
			write += `${subject.key === "kat" ? "I" : subject.name} gained temporal <i>${EFFECTS.add(list[id].perk, subject).name}</i> effect. `;			
		}
 
		if(eff.perk){
			const what = perks[eff.perk];
			const who = subject.key === "kat" ? "You" : subject.name;
			write(`<strong>${who} gained perk ${capitalise(what.name)}! <br> ${subject.key === "kat" ? what.desc : what.npc(subject.name, subject.sex)}</strong>`);
			//if(what.att) Object.keys(what.att).forEach( a => write(`<strong>${capitalise(att_list[a])} ${what.att[a]}</strong>`) );
			subject.perks.push(eff.perk);
			//att_recalculate(subject.key === "kat" ? subject : ext.follower);
		}
		 
		
		if(eff.attach){
			const {mods, how, zero} = eff.attach( a => dim(a, subject), subject  );
			
			if(zero && mods){
				const zeroids = {};
				Object.keys(mods).forEach( a => {
					if(subject.Mods[a] && !isNaN(subject.Mods[a]) && subject.Mods[a] !== 0) zeroids[a] = 0 - subject.Mods[a];
				});
				
				//console.log(zeroids);
				if(Object.keys(zeroids).length){					
					return animate(subject, {Mods: zeroids}, time/2, actual);
				}
			}
			actual();
			
			function actual(){
				how();
				update(subject);
				
				if(mods){
					animate(subject, {Mods: mods}, time);
				}else{
					draw(subject);
				}
			}
		};
	
//animate(subject, value, time = 5000, back = ()=>{}){	
		
		if(eff.fadeOut){
			const {what, how} = eff.fadeOut( a => dim(a, subject), subject);
			const temp = {}
			temp[what] = {invisible: 1};
			animate(subject, temp, time, ()=> {
				how();
				update(subject);
			});
		};
		
		if(eff.fadeIn){
			const {what, how} = eff.fadeIn( a => dim(a, subject), subject);
			if(subject[what]){
				const temp = {}
				temp[what] = {invisible: 1};
				animate(subject, temp, time/2, actual);
			}else{
				actual();
			};

			function actual(){
				how();
				subject[what].invisible = 1;
				update(subject); //????
					
				const temp = {}
				temp[what] = {invisible: -1};
				animate(subject, temp, time);	
			}
		}
		
		
		if(eff.mods){
			animate(subject, {Mods: eff.mods( a => dim(a, subject), subject)}, time);
			//const transformation = createTransformation(subject, ()=> draw(subject), {Mods: eff.mods( a => dim(a, subject, bodyparts), subject, bodyparts  ) });
			//transformAndShow(transformation, time);
		}
		
		if(eff.trigger){
			eff.trigger(subject);
		}		
		//console.error(index);
		//console.error(i);
	},
	
	
		
	initiate(seed){
		rg.i(seed);
		const tinctures = rg.shuffle(["red","orange","maroon","yellow","amber","green","lime","cyan","azure","blue","teal","indigo","magenta","pink","viole","white","black","gray"]);
		list.filter( a => a.key).forEach( a => a.color = tinctures.pop() );
		//ext.potions = elixirs.map( a => false );
	}

}
*/









