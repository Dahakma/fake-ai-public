//import {merchants} from "Data/items/merchants/index";
import {createMultiple} from "../wardrobe/main";
import {create} from "../wardrobe/create";
import {POTIONS} from "Avatar/game/potions";

//import {create} from "../wardrobe/create"
//import {inventory as inventoryMenu} from "../wardrobe/inventory";


//TODO - LOCATION WILL BE CHANGED
//import {menu as inventoryMenu} from "Gui/avatar/inventory";
//import {bfce} from "Gui/Avatar/inventory";

import {DISPLAY, popup, ULTRA} from "Gui/gui";
import {payment} from "System/bank_account.js";


import {rg, ra} from "Libraries/random";

//TODO - THIS IS FUCKED UP
import {menu as inventoryMenu} from "Gui/avatar/inventory.js";
//import {shop as shopMenu} from "Gui/avatar/inventory.js";

import {shops} from "Data/items/shops"
import {lists} from "Data/items/lists/index"


export const INVENTORY = {
	
	
	shop(key, back = ()=>{}, options = {}){
		const store = shops[key];
		rg.i;
		//creates one-dimensional list of shop items
		let actualItemList = [];
		const itemList = (()=>{
			let sets = [];
			if( Array.isArray(store.items) ){ //multiple lists of items could be associated with one shop
				sets = store.items.map( a => {
					const out = lists[a];
					if(out === undefined){
						console.warn(`WARNING: item list "${a}" not found (shop "${key}")`);
						return [];
					}
					return out;
				});
			}else{
				sets = [lists[store.items]];
			}
			return sets.reduce( (s, b) => { //reduces all lists into one
				if(typeof b === "function") b = b();
				return [...s, ...b];
			}, [])
		})();
			
		itemList.forEach( item => { //seems kinda redundant because exactly the same code is in createMultiple() but it is necessary because in the next step items are shuffled and their number is reduced
			const quality = Array.isArray(store.quality) ? rg.array(store.quality) : store.quality;
			if(typeof item === "object"){ //in format [numer_of_items; "blueprint", extra]
				const [number, blueprint, extra] = item;
				if(!blueprint) return; //empty or undefined 
				for(let k = 0; k < number; k++){
					if(extra){
						if(typeof extra === "object"){
							extra.quality ??= quality;
						}
						actualItemList.push({
							blueprint,
							seed: rg.g,
							extra,
						});
					}else{
						actualItemList.push({
							blueprint,
							seed: rg.g,
							extra: quality,
						});
					}
				}
			}else{ //in format "blueprint"
				if(!item) return;
				actualItemList.push({
					blueprint: item, 
					seed: rg.g,
					extra: quality,
				});
			}
		});
			
		//reduces number of items
		if(store.max){
			actualItemList = rg.shuffle(actualItemList);
			actualItemList.length = store.max;
		}

		//create items
		let items = actualItemList.map( ({blueprint, seed, extra}) => create(blueprint, seed, extra) );
		items.filter( item => item ); //remove those failed to be created 

		//combines the exactly same items (consumables) (there is no fucking way to create potions in bulk)
		items = this.compress(items);
		
		//what happens when done
		const done_back = function(money){
			if(money) payment(money, key);
			back();
		}
		

		inventoryMenu(PC, done_back, items, {...store.options, ...options});

	},
	
	
	
	
/*	
	shop(merchant, back, multiple){
		shopMenu(merchant, back, multiple);
	},
*/


	menu(subject, back, merchant){
		
//console.error(subject);
		inventoryMenu(subject, back, merchant);
	},
	
/*	
	merchant(shop = "shop", multiple = 1){
		let merch = [];
		
		//"merchants[key]" could be array or function returning array
		function retrive(a){
			if(typeof merchants[a] === "function"){
				return merchants[a](); 
			}else{
				return merchants[a];
			}
		}
		
		//"shop" could be string with key or one of "merchants" or array containing multiple keys
		if(Array.isArray(shop)){
			shop.forEach(  a => merch = merch.concat( retrive(a) ) );
		}else{
			merch = retrive(shop);
		}
		
		//"multiple" could be used to read the merchant multiple times (to boost the amount of items)
		for(let i = 0; i++; i <= multiple){
			merch = merch.concat(merch)
		}
		merch = createMultiple(merch);
		merch = this.compress(merch);
		
		return merch;
	},
*/	
	
	
/*
	TODO - NOES NOT WORK FOR SOME FUCKNG REASON 
	trade(merch, back, multiple){
		merch = this.merchant(merch, multiple);
		merch = this.compress(merch);
		inventoryMenu(PC, back, merch);
	},
	
	open: (subject = PC, back) => {
		console.error(this);
		inventoryMenu();
		bfce();
		//DISPLAY.inventory();
		//inventoryMenu(subject, back);
	},
*/
	
	get get(){
		//reality ??= head.reality;
		//return reality === 3 ? ext.gameInventory : ext.inventory
		return head.reality === 3 ? PC.gameInventory : PC.inventory;
	},

	has(blueprint){
		const inventory = this.get;
		const index = inventory.findIndex( a => a.blueprint === blueprint );
		return index !== -1;
	},
		
	count(blueprint, count){
		const inventory = this.get;
		const index = inventory.findIndex( a => a.blueprint === blueprint );
		if(index === -1) return 0;
		const item = inventory[index];
		if(!item.count) return 1; //it is in inventory so it is assumed there is at least one
		return item.count;
	},

	add(item, count){
		if(item === undefined) return;
		
		const inventory = this.get;
		const index = inventory.findIndex( a => a.id === item.id && a.seed === item.seed);
		
		//exactly same thing already in inventory 
		if(index !== -1 && item.id < 100){ // id < 100 == only consumables, not clothes and weapons
			inventory[index].count ??= 1; //probably redundant, in case "count" is not defined
			//count is explicitely defined 
			if(count){ 
				inventory[index].count += count;
			//count is based on item.count
			}else if(item.count){
				inventory[index].count += item.count;
			//assuption only 1 item is added
			}else{
				inventory[index].count++;
			}
		//not in inventory, "count" explicitely defined (beware - ignored item.count??)
		}else if(count){
			inventory.push(item);
			inventory[inventory.length - 1].count = count;
		//not in inventory, simply added (work for both those with item.count and without)
		}else{
			inventory.push(item);
		}
	},
	
	
	
	compress(array){
		const inventory = [];
		array.forEach( item => add(item) );
		
		//TODO
		function add(item, count){
			const index = inventory.findIndex( a => a.id === item.id && a.seed === item.seed);

			//exactly same thing already in inventory
			if(index !== -1){
				inventory[index].count ??= 1; //probably redundant, in case "count" is not defined
				//count is explicitely defined 
				if(count){ 
					inventory[index].count += count;
				//count is based on item.count
				}else if(item.count){
					inventory[index].count += item.count;
				//assuption only 1 item is added
				}else{
					inventory[index].count++;
				}
			//not in inventory, "count" explicitely defined
			}else if(count){
				inventory.push(item);
				inventory[inventory.length - 1].count = count;
				
			//not in inventory, simply added (work for both those with item.count and without)
			}else{
				inventory.push(item);
			}
		}
	
		return inventory;
	},

	addGarment(item){ //because every pieces of clothes is unique
		const inventory = this.get;
		inventory.push(item);
	},
	
	
	drink(item, subject){
		//if( this.count(item.
		POTIONS.drink(item, subject);
		this.remove(item, 1);
	},
	
	//could be inputed ITEM {object} or INDEX {integer} or BLUPRINT {string} (only for consumables!!)
	remove(input, count){
		if(input === undefined) return;

		const inventory = this.get;
		const index = (()=>{
			if(typeof input === "number") return input; //index
			if(typeof input === "string") return inventory.findIndex( a => a.blueprint === input ); //bluepring
			return inventory.findIndex( a => a.id === input.id && a.seed === input.seed); //item
		})();
		
		if(isNaN(index) || index < 0){
			if(debug){
				console.error(`ITEM NOT FOUND IN INVENTORY`);
				console.trace();
				console.log(index);
				console.log(input);
			}
		}
		
		inventory[index].count ??= 1; //obviously there has to be at least one???
			
		//count explicitly defined
		if(count){ 
			inventory[index].count -= count;
			
		//count based on item.count
		}else if(input?.count){
			inventory[index].count -= input.count;
			
		//defaultly only 1 item is removed
		}else{
			inventory[index].count--;
		}
			
			//probably redundant
			/*
			if(inventory[index].count < 0){
				if(debug){
					alert(`ERROR: You cannot have "${inventory[index].count}" of "${inventory[index].name}"!`);
					console.error(`ERROR: You cannot have "${inventory[index].count}" of "${inventory[index].name}"!`);
				}
			}
			*/
		
		//if "0" removes from the inventory
		if(inventory[index].count === 0){   //CHECK <0 should be pervented elsewhere
			inventory.splice(index, 1);
		}		
	},
	
	removeGarment(item){
		const inventory = this.get;
		const index = inventory.findIndex( a => a.id === item.id && a.seed === item.seed);
		inventory.splice(index, 1);
	},
	
	recreate(array){
		const inventory = this.get;
		inventory.length = 0;
		array.forEach( item => this.add(item) );
	},
	
	recount(array){ //TODO - NO FUCKING CLUE!!!
//console.error(array);
		const inventory = this.get;
		inventory.forEach( (a,i) => a.count = array[i] );
	},
	
	
	
}




/*
export function inventoryAdd(what, count = 1){
	const inventory = reality === 3 ? ext.gameInventory : ext.inventory;
	
	if(typeof what === "number"){ //i.e. potion or consumable
		const id = what;
		const index = inventory.findIndex( a => a.id === id );
		if(index === -1){
			
		}else{
			inventory[index].count += count;
		};
		
	}else if(typeof what === "object"){ //i.e clothes
		inventory.push(what);
	}
}
*/


/*
export function remove(what){ //what is id or 
	
}





*/
