import * as DA from "Libraries/da";
import {createCharacter} from "./create";
//import {create} from "../wardrobe/main";

export function attachPart(what, subject = PC, mod){
	//right x left keywords force the side
	if( what.search("Left") !== -1 ){ //TODO should not be search but exact position
		what = what.replace("Left","");
		actual(what, "left");
	}else if( what.search("Right") !== -1 ){
		what = what.replace("Right","");
		actual(what, "right");
	
	//usually both sides
	}else if([
		"Ears",
		"ElfEars",
		"CatEars",
		
		"Hoof",
		"Hoofs",
		"HoofHorse",
		
		"Feet",
		"FeetHuman",
		
		"PupilHuman",
		"PupilCat",
		"Pupil",
		"Pupils",
		
		"DemonicWings",
	].includes(what) ){
		actual(what, "right");
		actual(what, "left");
	}else{
		actual(what, "right");
	}
	
	function actual(what, side){
		//more easily rememberable names replaced by actual names of DAD parts
		const part = (() => {switch(what){
			case "Ears":
			case "ElfEars": return "EarsElf2";
			case "CatEars": return "CatEars";
			
			case "PupilCat": return "PupilCat";
			case "PupilHuman":
			case "Pupil": return "PupilHuman";
			
			case "Hoof":
			case "Hoofs":
			case "HoofHorse": return "HoofHorse";
			
			case "Feet":
			case "FeetHuman": return "FeetHuman";
			
			case "PubicHair": return "FemalePubiHair";
			
			case "Breasts": return "ChestHuman2";
			case "Penis": return "PenisHuman";
			case "Testicles": return "TesticlesHuman";
			case "Vagina": return "VaginaHuman";
			
			//DemonicTail
			//CatTail
			//"DemonicWings",
			//StraightHorns
			//"CurvedHorns"
			default: return what;
		}})();
		
		
		//face
		if([
			"CatEars",
			"EarsElf2",
			"PupilHuman",
			"PupilCat",
		].includes(part) ){
			const tempPart = DA.Part.create(DA[part], {
				side,
				mod, 
			});
			subject.attachPart(tempPart, subject.faceParts);
			
		//decorative 
		}else if([
			"DemonicTail",
			"CatTail",
			"DemonicWings",
			"StraightHorns",
			"CurvedHorns",
			"FemalePubiHair",
		].includes(part) ){
			const tempPart = DA.Part.create(DA[part], {
				side,
				...mod, 
			});
			subject.attachPart(tempPart, subject.decorativeParts);
			
		//body
		}else{
			const tempPart = DA.Part.create(DA[part], {
				side,
				...mod, 
			});
			subject.attachPart(tempPart);
		}
		
	}

}
		
export function removePart(location, subject = PC){
	location = location.replace("+",""); //TODO NOT SURE IF IT DOESNT BROKE SOMETHING
	
	//TODO - tempfix
	if(location === "horns"){
		removePart("right horns", subject, false);
	}
	
	//TODO maybe even others?
	if(location === "ears"){
		removePart("right ears", subject, false);
		removePart("left ears", subject, false);
		//update();
		return;
	}
	
	switch(location){
		case "right ear": location = "right ears"; break;
		case "left ear":  location = "left ears";  break;
		case "right eye": location = "right eyes"; break;
		case "left eye":  location = "left eyes";  break;
		default: //empty;
	}

	if( subject.faceParts.findIndex( a => a.loc == location ) !== -1 ){
		subject.removePart(location, subject.faceParts); 
	}else if( subject.decorativeParts.findIndex( a => a.loc == location ) !== -1 ){
		subject.removePart(location, subject.decorativeParts); 
	}else{
		subject.removePart(location);
	}

}





//TODO - SHOULD NOT BE IN CANVAS/BODY - TEMPFIX
import {showLess, showAll} from "../wardrobe/update";
import {emo} from "../wardrobe/emotions";
import {updateDraw} from "../wardrobe/update";

//facial expression and clothes can chane avatar dimensions - have to be removed before anything is calculated
function setClearState(subject){
	if(!subject?.display) return undefined; //sujbect probably not DAD object (TODO)
	const saved = {
		emotion: subject.emotion,
		top: subject.display.top,
		bot: subject.display.bot,
		//leg: subject.display.leg,
		//TODO - jew & leg - BEWARE atm don't cause any issues but it might be necessary to add them in future
	}
	showLess(0, 0, undefined, undefined, subject); //clothes may change Mods (especially fucking shoes)
	emo("neutral"); //neutral facial expression
	return saved;
}

function restoreNormalState(saved, subject){
	if(saved === undefined) return;
	emo(saved.emotion);
	showLess(saved.top, saved.bot, saved.leg, undefined, subject);
}





//changes avatar dimensions ("subject.Mods") of "output" to dimensions of "input"
export function updateMods(output, input = PC, clearState = false){ //TODO - INPUT = {}
	if(!input?.Mods) return;
	
	if(clearState){
		for(const key of Object.keys(input.Mods) ) {
			output.Mods[key] = input.Mods[key];
		} 
		
	}else{
		const save_out = setClearState(output);
		const save_in = setClearState(input);
		
		for(const key of Object.keys(input.Mods) ) {
			output.Mods[key] = input.Mods[key];
		} 
		
		restoreNormalState(save_out, output);
		restoreNormalState(save_in, input);
	}
}

export function updateApex(output, input = PC){
	if(input?.apex === undefined) return;
	Object.keys(input.apex).forEach( a => {
		output.apex[a] = input.apex[a];
	});
}
	
function updateBasedim(output,input = PC){
	if(!input?.basedim) return;
	for (const key of Object.keys(input.basedim) ) {
		output.basedim[key] = input.basedim[key];
	} 
}




export function updateOriginalMods(output = PC, input = PC){
	if(!input?.Mods) return;

	const save_out = setClearState(output);
	const save_in = setClearState(input);
	
	output.Mods = cloneOriginalCharacter(output).Mods; //createCharacter(output.seed, output.bias, output.randomness).Mods;
	
	for(const key of Object.keys(input.Mods) ) {
		output.Mods[key] = input.Mods[key];
	} 

	restoreNormalState(save_out, output);
	restoreNormalState(save_in, input);
}



export function onlyChangedMods(subject = PC, original){

//SAVE TEMPATE
	const saved = setClearState(subject);
	const saved_org = setClearState(original);
	
	//CALCULATE DIFFERENCE
	original ??= cloneOriginalCharacter(subject);

	const Mods = {};
	function round(b){
		return Math.round(b * 1000) / 1000;
	}
	
	
	Object.keys(subject.Mods).forEach( a => {
		if( round(subject.Mods[a]) !== round(original.Mods[a]) ) Mods[a] = round(subject.Mods[a]);
	});
	
	//RESTORE subject
	restoreNormalState(saved, subject);
	restoreNormalState(saved_org, original);
	
	return Mods;
}







 

export function cloneCharacter(template = PC){
	const temp = createCharacter(template.seed, template.bias, template.randomness);
	updateMods(temp, template);
	updateBasedim(temp, template);
	updateApex(temp, template);
	return temp;
}

//TODO - not abstract!
//TODO HORRIBLE TEMPFIX
import {returnOriginalAvatar} from "../game/gameCharacters";
export function cloneOriginalCharacter(template = PC){
	/*
	
	console.log("dummy")
	console.log(dummy.Mods.height)
	
	if(template.key){ //PC & NPCS have user-editable dimensions which are a part of original dimensions
		updateMods(dummy, template.key === "kat" ? {Mods: ext.user} : NPC[template.key].user, true);
	}
	*/
	return returnOriginalAvatar(template.key);
}


export function cloneFigurine(template = PC){
	const temp = createCharacter(template.seed, template.bias, template.randomness);
	updateMods(temp, template);
	updateBasedim(temp,template);


	temp.isFigurine = true;

	temp.basedim.hairStyle = 0;
	temp.Mods.hairStyle = 0;
	temp.name = "Dummy";
	temp.Mods.skinHue = 40-template.Mods.skinHue;
	temp.Mods.skinSaturation = 30-template.Mods.skinSaturation;
	temp.Mods.skinLightness = 50-template.Mods.skinLightness;
	
	const lng = temp.faceParts.length;
	for (let i=lng-1; i>0; i--){
		//if(!(temp.faceParts[i].loc == "left eyes" || temp.faceParts[i].loc == "right eyes"))continue;
		
		if(temp.faceParts[i].loc == "right ears")continue;
		if(temp.faceParts[i].loc == "left ears")continue;
		
		if(temp.faceParts[i].loc == "right brow")continue;
		if(temp.faceParts[i].loc == "left brow")continue;
		
		if(temp.faceParts[i].loc == "right eyes")continue;
		if(temp.faceParts[i].loc == "left eyes")continue;
		
		
		if(temp.faceParts[i].loc == "lips")continue;
		if(temp.faceParts[i].loc == "mouth")continue;
		
		if(temp.faceParts[i].loc == "nose")continue;
		//	if(temp.faceParts[i].loc == "nose")continue;
	
		temp.removePart(temp.faceParts[i].loc,temp.faceParts);
	}
	
	
	temp.attachPart(DA.Part.create(DA.LipsHuman, {
		side: "right",
		fill: "hsla(0.0,0.0%,90%, 1)",
	}), temp.faceParts);
	
//TODO - nipples do not work - attaching them not works, check DAD
	temp.attachPart(DA.Part.create(DA.NipplesHuman, {
		side: "right",
		fill: "hsla(0.0,0.0%,90%, 1)",
	}));


	temp.basedim.hairLightness = 100;

	temp.removeAllClothing();
	return temp;
}
	