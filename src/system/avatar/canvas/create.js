import * as DA from "Libraries/da";
import {rg} from "Libraries/random";
import {attachPart} from "./body";
import {default_PC} from "System/variables";

//CREATE CHARACTER -  creates characters based on seed, may includes some adjustments
export function createCharacter(seed = rg.date, bias = 0.5, randomness = 0.5){
	//creates the DAD object 
	const pc = randomCharacter(seed, bias, randomness);
	
	//saves variables sed for generation
	pc.seed = seed;
	pc.bias = bias;
	pc.randomness = randomness;
		
	//used in DAD but completely useless, out
	pc.baseVitals = {};
	pc.vitals = {};
	
	//SEED
	rg.i(seed);
	
	//gender
	pc.gender = bias < 0 ? 0 : 1;
	pc.sex = pc.gender;
	
	//pc.money = 13;
	//pc.Mods.irisLightness += 20;
	
	if(bias>=0){
		pc.basedim.hairStyle = Math.ceil(rg.g * 6);
		pc.basedim.hairLength = 60 + Math.ceil(rg.g * 30);
	}
		
	pc.weapon = {};
	pc.ink = [];
		
	//FIXES
	pc.basedim.faceWidth -= 1;
	
	//GENITALS
	if(pc.gender === 1){ //female
		pc.basedim.penisSize    = 0;
		pc.basedim.testicleSize = 0; 			
		pc.basedim.vaginaSize   = rg.range(50, 100); 
	}else if(pc.gender === 0){ //male
		pc.basedim.penisSize    = rg.range(40, 100); 
		pc.basedim.testicleSize = rg.range(45, 65); 
		pc.basedim.vaginaSize   = 0;
	}
	
	attachPart("Vagina", pc);
	attachPart("Testicles", pc);	
	attachPart("Penis", pc);	

	//PUBIC HAIR (todo, don't rememer why mods)
	pc.Mods.pubicLength   =  8; //rg.range(7.0, 11);
	pc.Mods.pubicWidthTop =  4; //rg.range(  6,  8);
	pc.Mods.pubicWidthBot =  0.3; // rg.range(0.5,  1);
	
	pc.basedim.pubicLength   = 0;
	pc.basedim.pubicWidthTop = 0;
	pc.basedim.pubicWidthBot = 0;
	
	//TAIL & WINGS
	pc.basedim.tail = 0;
	pc.Mods.tail = 0;
	
	pc.basedim.wings = 0;
	pc.Mods.wings = 0;
	
	//HORNS
	pc.basedim.hornyness = 0;
	pc.Mods.hornyness = 0;

	//EARS
	pc.basedim.earlobeLength = 1;
		
//	attachPart("ElfEars", pc);
	attachPart("PubicHair", pc);	
	attachPart("Breasts", pc);	


	//WEAPON
	pc.weapon = undefined;
	
	//APEX
	pc.apex = {};
	Object.entries(default_PC.apex).forEach( a => pc.apex[a[0]] = a[1] );
	
	//SKILLS & PERKS (todo - they are game-related so they should not be here)
	pc.perks = [],
	pc.skills = [],
	
	//BASE
	pc.base = {};
	pc.att = {};
	Object.entries(default_PC.base).forEach( a => {
		pc.base[a[0]] = a[1];
		pc.att[a[0]] = a[1];
	});
	
	//DISPLAY
	pc.display = {
		top: -1, 
		bot: -1, 
		leg: -1, 
		jew: -1,
	};
	
	return pc;

	
}
	
	

/**
	- randomly create a Player
	- taken from DAD, i do not exactly remeber why but I probably did some changes
 */
function randomCharacter(seed, bias, randomness){
	const pc = new DA.Player();

	DA.initiateRandNormal(seed); //dunno
	rg.i(seed);
	
	
	// generated with default (average) stats, perturb with bias
	const dimensions = DA.baseDimDesc[pc.skeleton];
	for (const dim in dimensions) {
		if ( Object.prototype.hasOwnProperty.call(dimensions, dim) ) {
			const dimDesc = dimensions[dim];
			if ( Object.prototype.hasOwnProperty.call(dimDesc, "stdev") === false) {
				throw new Error(`dimension ${  dim  } has no stdev defined`);
			}
			pc.basedim[dim] += DA.randNormal(bias * DA.getBiasMod(dimDesc, dim), dimDesc.stdev * randomness);
		}
	}
	
	// generate numerical statistics
	for (const s in DA.statLimits) {
		if ( Object.prototype.hasOwnProperty.call(DA.statLimits, s) ) {
			const stat = DA.statLimits[s];
			if ( Object.prototype.hasOwnProperty.call(stat, "stdev") === false) {
				throw new Error(`stat ${  s  } has no stdev defined`);
			}
			pc[s] += DA.randNormal(bias * DA.getBiasMod(stat, s), stat.stdev * randomness);
		}
	}
	
	// generate discrete statistics
	for (const s in DA.statDiscretePool) {
		if ( Object.prototype.hasOwnProperty.call(DA.statDiscretePool, s) ) {
			// uniform randomly pick a value from any of the available ones
			const valuePool = DA.statDiscretePool[s];
			pc[s] = valuePool[Math.floor(rg.g * valuePool.length)];
		}
	}

	pc.clampStats();



	for (const m in DA.modLimits) {
		if ( Object.prototype.hasOwnProperty.call(DA.modLimits, m) ) {
			const mod = DA.modLimits[m];
			if ( Object.prototype.hasOwnProperty.call(mod, "stdev") === false ) {
				throw new Error(`mod ${  m  } has no stdev defined`);
			}
			pc.Mods[m] += DA.randNormal(bias * DA.getBiasMod(mod, m), mod.stdev * randomness);
		}
	}

	return pc;
}