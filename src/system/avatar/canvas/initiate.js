import {
	load, 
	getCanvasGroup,
	getCanvas,
	hideCanvasGroup,
	addPattern,
} from "Libraries/da";
import {remove_children} from "Libraries/dh"; //TODO 
import {constants} from "Data/constants"; //TODO 
import {textures} from "Assets/textures";
import {tattoos_critical} from "Assets/tattoos";
/*
console.warn("x")
console.warn(textures)
console.warn(textures[0])

window.xx = textures;
*/

//size of the avatar canvas will be raised by adjustToMax_percent and the avatar will be offsetted by adjustToMax_value
const adjustToMax_percent = 0.5; //percent
const adjustToMax_value = -30; //px

export let canvasGroup; //where is the avatar drawed
export let canvasConfig; //configuration of avatar for drawing
export let portraitCanvas; //used for focused windows
export const loadedTextures = []; //list of textures which were loaded and can be used without issues

let container;
let canvasHolder;
let canvasHeight;
let canvasWidth;

export function initiate(container_id, start_game){
	try{
	  container = document.getElementById(container_id);  
	}catch(err){
		/* eslint-disable no-console */
			console.error(`ERROR: The container for DAD (element with id "${container_id}" NOT found) `);
			console.error(err);
		/* eslint-enable no-console */
	}
	
	
	//TODO - horrible temfix; to make sure at least assets with high-chance to break stuff are preloaded (not all assets are preloaded because there are too many and it would be mostly unnecessary)
	const loadAssets = new Promise((resolve, reject) => { //basically the same thing as load textures 
		const errorReport = [];
		
		const assets = tattoos_critical;
		const keys = Object.keys(assets);
		let assetsToLoad = keys.length;
		
		function areYouDone(){
			assetsToLoad--;
			if(!assetsToLoad){
				if(errorReport.length) console.warn(`WARNING: assets "${errorReport}" NOT loaded!`);
				//console.log(`>Asap assets loaded. `);
				resolve();
			}
		}
			
		function add(key, url) {
			const test = new Image();
			test.src = url;
			test.onload = ()=>{
				/*
				console.log("loaded")
				console.log(test)
				console.log(test.src)
				*/
				//loadedTextures.push(key);
				areYouDone();
			};
			test.onerror = ()=>{
				errorReport.push(key);
				areYouDone();
			};
		}
			
		keys.forEach( key => add(key, assets[key]) ); 
	});
	
	
	const loadTextures = new Promise((resolve, reject) => {
		const errorReport = [];
		const keys = Object.keys(textures);
		let texturesToLoad = keys.length;
		
		function areYouDone(){
			texturesToLoad--;
			if(!texturesToLoad){
				if(errorReport.length) console.warn(`WARNING: textures "${errorReport}" NOT loaded!`);
				console.log(`>Textures loaded. `);
				resolve();
			}
		}
			
		function add(key, url) {
			const test = new Image();
			test.src = url;
			test.onload = ()=>{
				loadedTextures.push(key);
				areYouDone();
			};
			test.onerror = ()=>{
				errorReport.push(key);
				areYouDone();
			};
		}
			
		keys.forEach( key => add(key, textures[key]) ); 
	});

	const loadDAD = new Promise((resolve, reject) => {
		load().then(resolve);
	});

	Promise.all([loadDAD, loadTextures, loadAssets]).then(rebuild).then(function(){
		loadedTextures.forEach( key => addPattern(key, textures[key]) ); 
		/* eslint-disable no-console */
			console.log(`>Dynamic Avatar Drawer initiated. `); //LOPRO
		/* eslint-enable no-console */
		start_game();
	});



}

export function rebuild(){
	remove_children(container);
	
	if( !SETTING.avatar_width || isNaN(SETTING.avatar_width) ) SETTING.avatar_width = constants.setting.avatar_width; //TODO CLUMSY TEMPFIX
	
	const width = SETTING.avatar_width; //container.offsetWidth 100; //TODO - connect to avatar width
	container.style.width = width; // SETTING.avatar_width;
	
	canvasWidth = /*container.offsetWidth*/width + (/*container.offsetWidth*/width * adjustToMax_percent);	//the avatar is bigger, there would be a lot of useless empty space
	canvasHeight = 2*(canvasWidth);
	
//	adjustToMax = ( canvasWidth/2 - container.offsetWidth/2 )/2;
//	adjustToMax_value = ( canvasWidth/2 - container.offsetWidth/2 )/2;
	
	canvasHolder = document.createElement("div");
	canvasHolder.id = "div_canvas";
	container.appendChild(canvasHolder);

/*	
	would work perfectly if absolute: 
	canvasHolder.style.position = "absolute"
	canvasHolder.style.left = canvasHolder.style.left - adjustToMax_value/2;
*/
	
	canvasGroup = getCanvasGroup(canvasHolder, {
		border: "1px",
		width:	canvasWidth,
		height:	canvasHeight,
	});
	
	canvasConfig = {
		// whether the name and gender icon should be rendered
		printAdditionalInfo: false,
		// whether the height and height bar should be rendered
		printHeight        : false,
		printAge: false,
		//whether the side shoe view (if available) should be rendered
		renderShoeSideView : false,
		// vitals
		printVitals: false,
		//the bigger avatar would be too right of the centre
		offsetX: adjustToMax_value, //pretty random numbers but seems to more or less work for values 100-400px						
	}; 
	
	
}
	
	
export function rebuildZoomed(/*, subject = PORTRAIT.subject*/){
		
/*		
//TODO
		//scrolls to the top
		document.body.scrollTop = 0; // For Safari
		document.documentElement.scrollTop = 0; // For Chrome a Firefox
*/
	
	remove_children(container);
	
	canvasHolder = document.createElement("div");
	canvasHolder.id = "div_canvas";
	container.appendChild(canvasHolder);
	
	canvasGroup = getCanvasGroup(canvasHolder, {
		border: "0px",
		width:	canvasWidth * 2,
		height: canvasHeight * 2,
	});
	
	canvasConfig = {
		printAdditionalInfo: false,
		printHeight        : false,
		renderShoeSideView : false,
		offsetX: 0, 		
	}; 
	
	
	portraitCanvas = getCanvas("portrait",{
		// size of the focused window/canvas
		width : container.offsetWidth - 40,
		height: (container.offsetWidth * 2) -40,
		// can add any CSS style here like border
		border: "none",
		// you can also position it absolutely
		position: "relative",
		//top     : "20px",
		left    : "20px",
		// or relative to a parent
		parent: container,
	});
				

	hideCanvasGroup(canvasGroup); 	
	//draw(subject);
}


export function scaled(){ //to adjust proper sizes of tattoo
	if(canvasHolder && canvasHolder.style){
		return parseFloat(canvasHolder.style.width) / 500;
	}else{
		return 1;
	}
}