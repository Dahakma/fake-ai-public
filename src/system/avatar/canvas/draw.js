import {
	Part,
	drawFocusedWindow,
} from "Libraries/da";
import {draw as drawAvatar} from "Libraries/da";
import {canvasGroup, canvasConfig, portraitCanvas, rebuild, rebuildZoomed} from "./initiate";


export function draw(subject = PC){
	PORTRAIT.subject = subject;
	
	if(PORTRAIT.zoomed){
		const zoom = (()=> {
			switch(PORTRAIT.zoom){
				default:
				case "bust": return ["neck","nape"];
				case "face": return ["neck","nape"];
				case "chest": return ["breast","cleavage"]; //TODO - WORKS ONLY WITH WOMEN!!!
				case "groin": return ["vagina","top"]; //TODO - WORKS ONLY WITH WOMEN!!!
			}
		})();
		drawAvatar(canvasGroup, subject, canvasConfig).then(function (exports) {
			drawFocusedWindow(portraitCanvas,
				exports,{
					center: exports[Part.RIGHT][zoom[0]][zoom[1]], //TODO
					width: 50,
					height: 100,
				});
		});
	}else{
		drawAvatar(canvasGroup, subject, canvasConfig);
	}
}


export function drawUnzoomed(subject = PC){
	if(PORTRAIT.zoomed){
		PORTRAIT.zoomed = false;
		PORTRAIT.zoom = "";
		rebuild();
		draw(subject);
	}else{
		draw(subject);
	}
}


export function drawZoomed(zoom, subject = PC){
	if(SETTING.no_zoom) return draw(subject);
	
	if(!PORTRAIT.zoomed || PORTRAIT.zoom !== zoom){
		PORTRAIT.zoom =  zoom ? zoom : "bust";
		PORTRAIT.zoomed = true;
		rebuildZoomed(zoom, subject);
		draw(subject);
	}else{
		draw(subject);
	}
}


//consult DAD/src/util/player
export function animate(subject, value, time = 5000, back = ()=>{}){
	let completionPercent = 0;
	let last = null;
	
	window.requestAnimationFrame(doTransform);	
	
	function transform(percent) {
		let stepPercent = percent;
		if (completionPercent + percent > 1) {
			stepPercent = 1 - completionPercent;
			completionPercent = 1;
		} else {
			completionPercent += percent;
		}

		performTransformation(subject, value, stepPercent);
		return completionPercent;
	}
		
	function performTransformation(entity, modifications, stepPercent){
		for (const prop of Object.keys(modifications) ) {
			if (modifications[prop] !== undefined && entity[prop] !== undefined) {
				switch (typeof modifications[prop]) {
				case "object":
					performTransformation(entity[prop], modifications[prop], stepPercent);
					break;
				case "number":
					entity[prop] += modifications[prop] * stepPercent;
					break;
				default:
					/* eslint-disable no-console */
						console.log(`unrecognized transformation property type ${typeof modifications[prop]} for ${prop}`);
					/* eslint-enable no-console */
				}
			}
		}
	}
		
	function doTransform(timestamp) {
		const delta = (last === null) ? 0 : timestamp - last;
		last = timestamp;
		const stepPercent = delta / time;

		const transformationCompletion = transform(stepPercent);
		draw(subject);
		
		if (transformationCompletion < 1) {
			window.requestAnimationFrame(doTransform);
		} else {
			return back();
		}
	}
	
}



export const PORTRAIT = {
	subject: PC,
	zoomed: false,
	transparent: false,
	highlight: undefined,
	zoom: "",
};




