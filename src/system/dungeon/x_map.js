import {div_container, container, color, slider, link, button, checkbox, divy, title, div_last} from "System/elements";

import {DISPLAY} from "System/gui";
import {div_minimap} from "System/gui_skeleton";

import {KEYBOARD} from "System/keyboard";

import {rg, ra} from "Libraries/random";
import {remove_children} from "Libraries/dh";




const size = 50;
const set = {
	//unknown: "gray", 
	//known: "white",
	line: 5, 
	color: "black",
	highlight: "gray",
	hallway: {
		color: "black",
		width: 20,
	},
	player: {
		radius: 15,
		color: "lime",
	},
	quest: {
		radius: 10,
		color: "cyan",
	},
	treasure: {
		radius: 10,
		color: "yellow",
	},
	enemy: {
		radius: 10,
		color: "red",
	},
}

let active = false;



export function end(){
	active = false;
	DISPLAY.text();
}


import parchment from 'Assets/parchment.png';
//import xtr from 'Assets/tree.png';
//úconst tree = new Image();
//tree.src = xtr;
	
export function map(){
	if(active) return end();
	active = true;
	DISPLAY.subtext();
	container("centered_column");
	title(ext.dungeon.name);
	
	//console.log(div_container);
	const map = document.createElement("canvas");
	const ctx = map.getContext("2d");
	const xxx = ext.dungeon.grid[head.z].length;
	const yyy = ext.dungeon.grid[head.z][0].length;
	map.width = xxx * size;
	map.height = yyy * size;
	map.style.width = xxx * size;
	map.style.height = yyy * size;
	map.style.margin = "auto";
	/*
	let background = new Image();
	background.src = parchment;
	background.onload = () => {
		ctx.drawImage(background, 0, 0, map.width, map.height);
	}
	*/
	map.style.background = `url("${parchment}")`;
	map.style.backgroundSize = "100%";
	
	div_container.append(map);
	map.style.cursor = "pointer";
	map.addEventListener("click", end);
	
	KEYBOARD.override(); //won't overwrite other already assigned keyboard events
	KEYBOARD.attach(end, "m", `KeyQ`);
	
	

	for(let x = 0; x < xxx; x++){
		for(let y = 0; y < yyy; y++){
			const center = {
				x: (0.5 + x) * size,
				y: (0.5 + y) * size,
			}
			draw(ctx, x, y, center);
		}
	}
}




export function minimap(){
	const min = document.createElement("canvas");
	const ctx = min.getContext("2d");
	min.width = 3 * size;
	min.height = 3 * size;
	
	min.style.background = `url("${parchment}")`;
	min.style.backgroundSize = "100%";
	
	remove_children(div_minimap);
	min.style.cursor = "pointer";
	div_minimap.append(min);
	min.addEventListener("click", map);
	
	for(let x = -1; x <= 1; x++){
		for(let y = -1; y <= 1; y++){
			const center = {
				x: (0.5 + x + 1) * size,
				y: (0.5 + y + 1) * size,
			}
			draw(ctx, head.x + x, head.y + y, center, {player:false});
		}
	}
}



const directions = [
	{x: 1, y: 0},
	{x: 0, y: 1},
	{x: -1, y: 0},
	{x: 0, y: -1},
]

const more = [
	{x: 1, y: 1},
	{x: -1, y: 1},
	{x: -1, y: -1},
	{x: 1, y: -1},
]



function draw(ctx, x, y, center, options = {}){
	rg.i(ext.dungeon.seed + x + y);
	//console.log(ext.dungeon.explored[head.z][x][y])
	//if(!ext.dungeon.explored[head.z][x][y]) return;
	
	const half = size / 2;
	const corner = {
		x: center.x - half,
		y: center.y - half,
	}
	
	//UNVISITED
	if(ext.dungeon.explored[head.z][x]?.[y] < 2){
		ctx.fillStyle = `hsla(0, 0%, 0%, ${ext.dungeon.explored[head.z][x]?.[y] === 1 ? 0.2 : 0.4})`; //set.known;
		//ctx.beginPath();
		ctx.fillRect(corner.x, corner.y, size, size);
		//ctx.fill(); 

	};
	
	//NON-EXISTENT
	if(ext.dungeon.explored[head.z][x]?.[y] === undefined) return;
	
	
	
	
	
	
	
	const right = {x: 1, y: 0};
	const up = {x: 0, y: 1};
	const left = {x: -1, y: 0};
	const down = {x: 0, y: -1};
	const mid = {x: 0, y: 0};
	
	function rotate({x, y}, degrees){
		if(!degrees){
			return {x, y};
		}else if(degrees === 180){
			return {x: -x, y: -y};
		}else if(degrees === 90){
			return {x: y, y: -x};
		}else if(degrees === -90){
			return {x: -y, y: x};
		}
	}
	
	//UNEXPLORED
	if(ext.dungeon.explored[head.z][x][y] < 1){
		if(ext.dungeon.powers.display_quest && ext.dungeon.blueprint[head.z][x]?.[y] === "q") mark("quest");
		if(ext.dungeon.powers.display_treasure && ext.dungeon.blueprint[head.z][x]?.[y] === "t") mark("treasure");
		if(ext.dungeon.powers.display_enemy && (ext.dungeon.blueprint[head.z][x]?.[y] === "e" || ext.dungeon.blueprint[head.z][x]?.[y] === "b") ) mark("enemy"); //TODO OBJECTIVE SLAY 
		return;
	}
	
	
	//EMPTY
	if(!ext.dungeon.grid[head.z][x]?.[y]){
		/*
		if(ext.dungeon.theme = "wood"){
			ctx.drawImage(tree, corner.x, corner.y, size, size);
		}
		*/
		return;
	}
	
	if(ext.dungeon.blueprint[head.z][x][y] === "E"){
		entrance();
	}else if(ext.dungeon.blueprint[head.z][x][y] === "<"){
		upstair();
	}else if(ext.dungeon.blueprint[head.z][x][y] === ">"){
		downstair();
	}else if(ext.dungeon.blueprint[head.z][x][y] === "|" || ext.dungeon.blueprint[head.z][x][y] === "%"){
		door();
	}else{
		corridor();
	}
	
	function door(){
		corridor();
		
		ctx.strokeStyle = set.highlight;
		ctx.lineWidth = set.line;
		
		ctx.beginPath();
		ctx.moveTo(center.x, center.y + set.hallway.width);
		ctx.lineTo(center.x, center.y - set.hallway.width);
		ctx.stroke();
		
	}
	
	
	function downstair(){
		ctx.fillStyle = set.color;
		ctx.beginPath();
		ctx.fillRect(corner.x, corner.y, size, size);
		ctx.fill(); 
		
		const adjust = size * 0.1;
		ctx.strokeStyle = set.highlight;
		ctx.lineWidth = set.line;
		
		ctx.beginPath();
		ctx.moveTo(center.x + 3 * adjust, center.y + 3 * adjust);
		ctx.lineTo(center.x + 3 * adjust, center.y + adjust);
		ctx.lineTo(center.x + adjust, center.y + adjust);
		
		ctx.lineTo(center.x + adjust, center.y - adjust);
		ctx.lineTo(center.x - adjust, center.y - adjust);
		
		ctx.lineTo(center.x - adjust, center.y - 3 * adjust);
		ctx.lineTo(center.x - 3 * adjust, center.y - 3 * adjust);
		
		ctx.stroke();
	}
	
	function upstair(){
		ctx.fillStyle = set.color;
		ctx.beginPath();
		ctx.fillRect(corner.x, corner.y, size, size);
		ctx.fill(); 
		
		const adjust = size * 0.1;
		ctx.strokeStyle = set.highlight;
		ctx.lineWidth = set.line;
		
		ctx.beginPath();
		ctx.moveTo(center.x - 3 * adjust, center.y + 3 * adjust);
		ctx.lineTo(center.x - 3 * adjust, center.y + adjust);
		ctx.lineTo(center.x - adjust, center.y + adjust);
		
		ctx.lineTo(center.x - adjust, center.y - adjust);
		ctx.lineTo(center.x + adjust, center.y - adjust);
		
		ctx.lineTo(center.x + adjust, center.y - 3 * adjust);
		ctx.lineTo(center.x + 3 * adjust, center.y - 3 * adjust);
		
		ctx.stroke();
	}
	
	function entrance(){
		ctx.fillStyle = set.color;
		
		
		ctx.beginPath();
		ctx.fillRect(corner.x, corner.y, size, size);
		ctx.fill(); 
		
		
		/*
		ctx.beginPath();
		ctx.moveTo(center.x + adjust, center.y + adjust);
		ctx.lineTo(center.x - adjust, center.y - adjust);
		ctx.stroke();
		*/
		
		
		
		const adjust = size * 0.25;
		ctx.strokeStyle = set.highlight;
		ctx.lineWidth = set.line;
		
	
		
	
		ctx.beginPath();
		ctx.moveTo(center.x + adjust, center.y + adjust);
		ctx.lineTo(center.x - adjust, center.y - adjust);
		ctx.stroke();
		
		ctx.beginPath();
		ctx.moveTo(center.x - adjust, center.y + adjust);
		ctx.lineTo(center.x + adjust, center.y - adjust);
		ctx.stroke();
		
	}
		
	
	
	
		//PLAYER
	
	if(ext.dungeon.powers.display_quest && ext.dungeon.blueprint[head.z][x]?.[y] === "q") mark("quest");
	if(ext.dungeon.powers.display_treasure && ext.dungeon.blueprint[head.z][x]?.[y] === "t") mark("treasure")
	if(ext.dungeon.powers.display_enemy && (ext.dungeon.blueprint[head.z][x]?.[y] === "e" || ext.dungeon.blueprint[head.z][x]?.[y] === "b")) mark("enemy"); //TODO OBJECTIVE SLAY 
	if(options.player !== false && x === head.x && y === head.y) mark("player");
	
	
	function mark(what){
		ctx.fillStyle = set[what].color;
		ctx.beginPath();
		ctx.arc(center.x, center.y, set[what].radius, 0, 2 * Math.PI);
		ctx.fill(); 
	}
	
	
	
	function corridor(){
		
		
		const up = ext.dungeon.grid[head.z][x][y + 1];
		const down = ext.dungeon.grid[head.z][x][y - 1];
		const left = ext.dungeon.grid[head.z][x - 1]?.[y];
		const right = ext.dungeon.grid[head.z][x + 1]?.[y];
		
		//console.log(up,right,down,left);
		
		if(up && down && left && right){
		//	console.warn("+");
			return hallway("+");
		
		}else if(left && right && down){
			return hallway("T")
		}else if(left && right && up){
			return hallway("T", 180);
		}else if(left && down && up){
			return hallway("T", 90);
		}else if(right && down && up){
			return hallway("T", -90);
		
		}else if(up && right){
			return hallway("L");
		}else if(right && down){
			return hallway("L", 90);
		}else if(down && left){
			return hallway("L", 180);
		}else if(left && up){
			return hallway("L", -90);
			
		}else if(up && down){
			return hallway("I");
		}else if(left && right){
			return hallway("I", 90);
			
		}else if(left){
			return hallway("<");
		}else if(up){
			return hallway("<", 90);
		}else if(right){
			return hallway("<", 180);
		}else if(down){
			return hallway("<", -90);
		}
	};
	
		// < I L T + 
	
	
	
	
	function hallway(type, degrees){
		switch(type){
			case "+": 
				line(right, left, up);
				line(up, down, right);
				return;
			case "<":
				line(rotate(left, degrees), mid, rotate(up, degrees));
				return;
			case "I":
				line(rotate(up, degrees), rotate(down, degrees), rotate(right, degrees) );
				return;
			case "L":
				line(rotate(up, degrees), mid, rotate(right, degrees) );
				line(mid, rotate(right, degrees), rotate(down, degrees) );
				return;
				
			case "T":
				line(rotate(left, degrees), rotate(right, degrees), rotate(down, degrees) );
				line(mid, rotate(down, degrees), rotate(right, degrees) );
				return;	
		}
	}
	
	
	/*
	//not explored
	if(!ext.dungeon.explored[head.z][x][y]) return;
	
	//empty
	ctx.fillStyle = set.known;
	ctx.beginPath();
	ctx.fillRect(x * size, y * size, size, size);
	ctx.fill(); 
	
	
	if(!ext.dungeon.grid[head.z][x]?.[y]) return;
	
	
		
	//something
	const center = {
		x: (0.5 + x) * size,
		y: (0.5 + y) * size,
	}
 
 
	ctx.strokeStyle = set.corridor.color;
	ctx.lineWidth = set.corridor.width;
	directions.forEach( a => {
		if(ext.dungeon.grid[head.z][x + a.x]?.[y + a.y]){
			ctx.beginPath();
			ctx.moveTo(center.x - a.x * set.corridor.width/2, center.y - a.y * set.corridor.width/2);
			ctx.lineTo(center.x + a.x * size/2, center.y + a.y * size/2);
			ctx.stroke();
		}
	})
	
	ctx.fillStyle = set.point.color;
	if(x === head.x && y === head.y){
		//context.arc(x,y,r,sAngle,eAngle,counterclockwise);
		ctx.beginPath();
		ctx.arc(center.x, center.y, set.point.radius, 0, 2 * Math.PI);
		ctx.fill(); 
	}
	*/
	
	
	function line(start, end, kkk){
		let width = 10; //set.hallway.width;
		let roughness = 1.5;

		switch(ext.dungeon.theme){
			case "cellar":
				width = 15;
				roughness = 0.2;
				break;
				
			case "wood":
				width = 20;
				roughness = 0.45;
				break;
				
			case "cave":
				width = 15;
				roughness = 0.4;
				break; 
				
			case "ship":
				width = 20;
				roughness = 0.05;
				break; 
			
			case "ruins":
				width = 18;
				roughness = 0.1;
				break;
				
			default:
			case "catacombs":
				width = 18;
				roughness = 0.1;
				break;
		}
		
		

				
		//const roughness = 0.3;
	//	console.warn("L");
		ctx.strokeStyle = "red";
		start = {
			x: center.x + (start.x * half),
			y: center.y + (start.y * half)
		};
		end = {
			x: center.x + ( end.x * half), 
			y: center.y + (end.y * half),
		}
		const adjust = {x: kkk.x * width, y: kkk.y * width};
		
		const _1 = {
			x: start.x + adjust.x, 
			y: start.y + adjust.y, 
		};
		const _2 = {
			x: end.x + adjust.x, 
			y: end.y + adjust.y, 
		};
		const _3 = {
			x: end.x - adjust.x, 
			y: end.y - adjust.y, 
		};
		const _4 = {
			x: start.x - adjust.x, 
			y: start.y - adjust.y, 
		};
		
		
		function zigzag(a, b){
			
			if(kkk.y){ //left -  right
				const tick = (a.x - b.x) / 6;
				let track = a.x; // > b.x ? b.x : a.x;
				function tok(){
					return track -= tick;
				};
				function zig(){ 
					return (a.y - width) + ( width * rg.range(1-roughness, 1+roughness) ); 
				};
				//ctx.lineTo(a.x, a.y);
				ctx.bezierCurveTo(
					tok(), zig(),
					tok(), zig(),
					tok(), zig()
				);
				ctx.bezierCurveTo(
					tok(), zig(),
					tok(), zig(),
					b.x, b.y
				);
				
			}else if(kkk.x){ //up -  down
			//console.warn("ud");
				const tick = (a.y - b.y) / 6;
				let track = a.y; // > b.x ? b.x : a.x;
				function tok(){
					return track -= tick;
				};
				function zig(){ 
					return (a.x - width) + ( width * rg.range(1-roughness, 1+roughness) ); 
				};
				
				/*
				console.log(a.y);
				console.log(tick);
				console.log(tok());
				console.log(tok());
				console.log(tok());
				console.log(tok());
				console.log(tok());
				console.log(b.y);
				*/
				
				//ctx.lineTo(a.x, a.y);
				ctx.bezierCurveTo(
					zig(), tok(),
					zig(), tok(),
					zig(), tok(),
				);
				ctx.bezierCurveTo(
					zig(), tok(),
					zig(), tok(),
					b.x, b.y
				);
			}
		}
		
		
		
		/*//ctx.lineTo(_2.x, _2.y);
		zigzag(_1, _2);
		zigzag(_2, _3);
		zigzag(_3, _4);
		zigzag(_4, _1);
		//zigzag(_1, _2);
		//ctx.lineTo(_3.x, _3.y);
		//zigzag(_2, _3);
		//zigzag(_3, _4);
		//ctx.lineTo(_1.x, _1.y);
		/*
		ctx.lineTo(_3.x, _3.y);
		zigzag(_3, _4);
		
		*/
		//ctx.lineTo(_1.x, _1.y);
		//ctx.stroke();
		
		if(roughness > 0){
			ctx.beginPath();
			ctx.moveTo(_1.x, _1.y);
			zigzag(_1, _2);
			zigzag(_2, _3);
			zigzag(_3, _4);
			zigzag(_4, _1);
			ctx.fillStyle = set.hallway.color;
			ctx.fill();		
		}else{
			ctx.beginPath();
			ctx.moveTo(_1.x, _1.y);
			ctx.lineTo(_2.x, _2.y);
			ctx.lineTo(_3.x, _3.y);
			ctx.lineTo(_4.x, _4.y);
			ctx.lineTo(_1.x, _1.y);
			ctx.fillStyle = set.hallway.color;
			ctx.fill();
		}
	};
	
	
	
	
	

	
	
	
	
}










