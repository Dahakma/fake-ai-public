
 

import {effigy, saveWornOutfit, wearOutfit} from "Avatar/index"; 
import {fellowship} from "Virtual/index"; 
import {dungeons} from "Data/virtual/dungeons"; 
import {ra, rg} from "Libraries/random"; 



const def = {
		enemies: [
			["goblins"], //1
			["goblins"], //2
			["goblins"], //3
			["goblins"], //4
		],
		bosses: [
			["ogre"], //1
			["ogre"], //2
			["ogre"], //3
			["ogre"], //4
		],
		treasures: "chest",
		challenges: "x",
	}
	
function add_source(source, name,  primary, secondary, tertiary){
	/*console.log(name);
	console.log(primary);
	console.log(secondary);
	console.log(tertiary);
	*/
	const a = primary ?? secondary ?? tertiary;
	//console.error(a);
	if(Array.isArray(a)){
		Object.defineProperty(source, name, {
			get(){ return rg.array(a) },
		});
	}else{
		source[name] = a;
	}
}
	
export function spawn(){
	const input = dungeons[ext.dungeon.key];
	const source = {};
	add_source(source, "spawn", input.rooms?.spawn, input.rooms?.enemies, def.enemies[head.act]);

	ext.dungeon.grid.forEach( (a,z) => a.forEach( (b,x) => b.forEach( (c,y) => {
		if(ext.dungeon.blueprint[z][x][y] === "s"){
			ext.dungeon.blueprint[z][x][y] = "e";
			ext.dungeon.grid[z][x][y] = source.spawn;	
		}
	})));
}



function collect(input, quest){
	const SEED = ra.seed;
	rg.i(SEED);
	
	
	let original = Array.isArray(input.grid) ? input.grid : [input.grid];
	
	original = original.map( f => {
		f = f.split("\n").map( a => a.trim() ).filter( a => a ); //split into lines & remove whitespaces
		f = f.map( a => [".", ...a, "."] ); //string into array + horizontal padding
		
		const padding = f[0].map( a => "." ); //vertical padding
		f.unshift(padding);
		f.push(padding);

		console.log(f);

		return f;
	})
	
	console.error(original);
	
	
	
	

	
	const grid = [];
	let entry = {};
	const explored = [];
	const blueprint = [];
	//console.log(xxx, yyy);
	
	/*
	for(let x = 0; x < xxx; x++){
		
		grid[x] = [];
	}
		*/
		
	
	
	
	const source = {};
	add_source(source, "enemies", input.rooms?.enemies, def.enemies[head.act]);
	add_source(source, "bosses", input.rooms?.bosses, def.bosses[head.act]);
	add_source(source, "follower", input.rooms?.follower, ext.follower?.key, "x");
	if(!ext.follower) source.follower = "x";
	
	add_source(source,"challenges", input.rooms?.challenges, /*TODO,*/ "x");
	add_source(source,"treasures", input.rooms?.treasures, /*TODO*,*/ "chest");
	add_source(source,"quest", input.rooms?.quest, ext.quests[quest]?.room, "x");
	
	if(input.extra)	Object.keys(input.extra).forEach( a => add_source(source, a, input.extra[a]) );
	
	//input.rooms.quest ?? ext.quests[id]?.room ?
	/*console.error(source.enemies);
	
	console.log(source);
	*/
	/*	
	let enemies = input.rooms.enemies;
	if(enemies === undefined){
		enemies = default_enemies[head.act];
	}else if( Array.isArray(enemies) ){
		enemies = [enemies]
	};
	
	const default_bosses = [
		["ogre"], //1
		["ogre"], //2
		["ogre"], //3
		["ogre"], //4
	]
	//let bosses = input.bosses || default_bosses[head.act];
	let bosses = input.rooms.bosses;
	if(bosses === undefined){
		bosses = default_enemies[head.act];
	}else if( Array.isArray(bosses) ){
		bosses = [bosses]
	};
	*/
	/*
		E - ENTRANCE
		
		x - hallway
		f - follower moment
		q - quest moment
		t - treasure 
		e - enemy 
		b - boss enemy 
	*/
	
	const xxx = original[0][0].length;
	const yyy = original[0].length;
	const zzz = original.length;
	//BEWARE - original is oriented in wrong directions!
	
	
	for(let z = 0; z < zzz; z++){
		grid[z] = [];
		explored[z] = [];
		blueprint[z] = [];
		for(let x = 0; x < xxx; x++){
			grid[z][x] = [];
			explored[z][x] = [];
			blueprint[z][x] = [];
			for(let y = 0; y < yyy; y++){
				explored[z][x][y] = 0;
				blueprint[z][x][y] = original[z][y][x];
				grid[z][x][y] = (()=>{
					switch(original[z][y][x]){
						case ".": return undefined;
						case "E": 	entry = {z, x, y}
									return "entrance";
						case "s":
						case "x": return "x";
						case "<": return "upstairs"; //TODO
						case ">": return "downstairs"; //TODO
						case "|": return "low_door"; //TODO
						case "%": return "high_door"; //TODO
						
						case "t": return source.treasures;
						case "c": return source.challenges;
						
						case "q": return source.quest; //TODO  - precedence
						case "f": return source.follower;
						
						case "e": return source.enemies;
						case "b": return source.bosses;
						
						case "r": return rg.array([source.treasures, source.challenges, source.enemies]);
						
						
						default:
							//if(source[original[z][y][x]]) return source[original[z][y][x]];
							//console.warn(`Warning: Dungeon creation, room with alias "${original[z][y][x]} not found. `); //SIC
							return "x";

					}
				})();
				
				if(input.extra){
					input.extra.forEach( a => {
						if( blueprint[z][x][y] == a[0] ){ //keep == to allow string or number shenagigans
							grid[z][x][y] = a[1];
							if(a[2]) blueprint[z][x][y] = a[2];
						}
					})
				}
				
				
			}
		}
	}
	
	
	
	console.warn(grid);
	
/*		
	for(let y = 0; y < yyy; y++){
		grid[x] = [];
		console.log(x);
		console.log(a[x]);
	
		for(let x = 0; x < xxx; x++){
			grid[x][y] = a[x][y];
		}
		*
	}*/
	
	//const follower = input.follower ? followers[input.follower] : undefined;
	/*if(input.follower){
		fellowship(input.follower);
	}
	*/
	
	let blocked = undefined;
	const bars = ["hp","eng"];
	
	let oxygen = undefined;
	let oxygen_max = undefined;
	if( ["underwater", "shipwreck","flooded"].includes(input.theme) ){
		blocked = true;
		oxygen = 10;
		oxygen_max = 10;
		bars.push("oxygen");
	}
	
			
	//TODO - RECALCULATE!!!
	
	//console.log(grid);
	return {
		seed: SEED,
		name: input.name,
		//objective: input.objective,
		done: false,
		quest,
		theme: input.theme,
		introduction: input.introduction,
		loot: input.loot,
		//follower,
		grid, 
		blueprint,
		entry, 
		explored, 
		powers: {},
		bars,
		oxygen,
		oxygen_max,
		blocked,
	};
}






export const create = function(what, quest){
	ext.dungeon = {};
	//ext.dungeon.entry = {x: 0, y: 0};
	ext.dungeon = collect(dungeons[what], quest);
	ext.dungeon.key = what;
	
}