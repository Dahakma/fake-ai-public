import {DISPLAY} from "Gui/gui";

export let rose_paused = false;  //prevent to accidentally skippping by too furious clicking by disabling advance for a tiny moment

export function rose_halt(){
	rose_paused = true;
	setTimeout(()=> rose_paused = false, 600);
}

export function rose_off(){
	DISPLAY.rose_off();
}

export function rose_on(){
	DISPLAY.rose_on();
}
