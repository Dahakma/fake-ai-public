//import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt/*, npc, fol, kat, ayy, qaa, qbb, qcc, yel, gal*/} from "Loop/text";


//import {clr, set, done, link, main} from "Loop/engine";
import {main, set, link} from "Loop/engine";

import {DISPLAY} from "Gui/gui";
//import {div_controls, div_text} from "System/gui_skeleton";
import {autosave} from "System/save";

import {capitalise} from "Libraries/dh";
//import {BARS} from "Virtual/index"; 
import {rose_on, rose_off, rose_paused, rose_halt} from "Dungeon/rose"; 
import {create, spawn} from "Dungeon/create"; 
import {room} from "Dungeon/rooms/index"; 
//import {explore} from "Dungeon/map"; 
import {quest} from "Virtual/quest"; 
import {ATTRIBUTES} from "Virtual/index";
import {EFFECTS} from "Virtual/effects";
import {ra, rg} from "Libraries/random";
/*
let victory;
let failure;
let returnToChapter;
*/


/* ENTER & EXIT DUNGEON */

/**
 *	exits dungeon and returns to normal game
*/
export function exit(success){
	success ??= objective()?.done; //if not forced, check if objective acomplished 
	if(ext.dungeon.survive) success = true; //when objective survive, exiting alone is success
	
	const name = ext.dungeon.name; //name remembered for autosave
 
	const {win, fail, chapter} = ext.dungeon.back;
	
	ext.dungeon = undefined; //dungeon deleted
	DISPLAY.dungeon_leave(); //GUI switched to normal view
	main(success ? win : fail, chapter); //normal game is continued
	
	autosave( (success || win === fail) ? `${name} finished` : `${name} exited`); //autosaves
}


/**
 *	creates link for entering the dungeon
 * @param {string} text - displayed text of the link
 * @param {string} id - key of the dungeon
 * @param {integer} win - index of even triggered if dungeon finished successfuly (in the current chapter)
 * @param {integer} [fail = win] - index of even if fail; same as "win" if not defined
 * @param {string} [quest] - key of related quest
 * @param {function} [before] - function triggered before entering
*/
export function enter(text, id, win, fail, quest, before /*input, chapter, target = 101, before*/){
	if(fail === undefined) fail = win; 
	link(text, null, before, () => entrance(id, quest, win, fail) );
}


/**
 *	actually initiates dungeon and enters it 
 * @param {string} id - key of the dungeon
 * @param {string} [quest] - key of related quest
 * @param {integer} win - index of even triggered if dungeon finished successfuly (in the current chapter)
 * @param {integer} [fail = win] - index of even if fail; same as "win" if not defined
*/
function entrance(id, quest, win, fail){
	create(id, quest); //follower used in set.game TODO
	ext.dungeon.back = {
		win,
		fail,
		chapter: head.chapter,
	}
	
	set.game();
	DISPLAY.dungeon();

	teleport(ext.dungeon.entry.z, ext.dungeon.entry.x, ext.dungeon.entry.y, 0); //moves the player to the entrance tile
		
	main(101, "initiate"); //overrrides normal room.entrance
	autosave(ext.dungeon.name); //TODO CHECK
}



/**
 *	enters dungeon again (loading save made in dungeon)
*/
export function reenter(){
	//create(what, quest); //follower used in set.game TODO
	//set.game();
	
	//BARS.create();
	
	DISPLAY.check(); 
	DISPLAY.dungeon();
	
	
	rose_on();
	
	
	//head.autosaved = false; //not necessary for dugeons; only entrance triggers the autosave and the entrance is not triggered by autoload
	//console.log(ext.dungeon);
	//teleport(ext.dungeon.entry.z, ext.dungeon.entry.x, ext.dungeon.entry.y, 0);
	
	main(head.scene, head.chapter); //overrrides normal room.\entrance
}






/* MOVEMENT */

/**
 *	moves player on a specific tile in the dungeon grid
 * @param {integer} x - x dimension 
 * @param {integer} y - y dimension 
 * @param {integer} z - z dimension 
*/
export function teleport(z, x, y){
	head.x = x;
	head.y = y;
	head.z = z;
	move();
}


/**
 * moves player in dungeon grid by ... in ... direction
 * @param {integer} x - x dimension 
 * @param {integer} y - y dimension 
*/
export function advance(x, y){
	if(rose_paused) return; //movement is temporarily blocked
	head.previous_x = head.x;
	head.x += x;
	head.previous_y = head.y;
	head.y += y;
	move();
}


/**
 * moves player in dungeon grid by ... vertically
 * @param {integer} x - x dimension 
 * @param {integer} z - z dimension 
*/
export function stairs(z){
	if(ext.dungeon.grid[head.z + z] === undefined) return;
	head.previous_z = head.z;
	head.z += z;
	move();
}


/**
 * executes movement on the grid, triggers tile, explores other tiles
*/
function move(){
	const room = ext.dungeon.grid[head.z][head.x][head.y];
	explore();
	main(101, room);
}





/* OBJECTIVES */

/**
 * switches dungeon objective to "survive" & disable automatic leaving
*/
export function survival(){
	if(ext.dungeon.quest) quest.done(ext.dungeon.quest);
	ext.dungeon.blocked = true;
	ext.dungeon.survive = true;
	spawn();
}


export function objective(){
	
	const q = ext.quests[ext.dungeon.quest];
	if(!q) return;
	
 
	if(ext.dungeon.survive){
		return {
			done: false,
			desc:`Survive`,
		}
	}else if(q.objective === "eliminate"){
		//const enemies = ["ogre", "goblins", "rats", "pirate"];
		let alive = 0;
		ext.dungeon.grid.forEach( (a, z) => a.forEach( (b, x) => b.forEach( (c, y) => {
			if(ext.dungeon.blueprint[z][x][y] === "e" || ext.dungeon.blueprint[z][x][y] === "b") alive++;
		})));

		if(alive === 0){
			quest.done(ext.dungeon.quest);
			//q.done = true;
		}
		
		//VICTORY CONDITION CHECKED HERE (todo move to end of combant)
		return {
			done: q.done,
			desc:`Eliminate all enemies`,
			cheat: `###${alive} remaining`,
		}
	}else if(q.objective === "collect"){
		//VICTORY CONDITION TESTED WHEN COLLECTING
		return {
			done: q.done,
			desc: q.title,
			hint: `<strong>${q.counter}/${q.desired}</strong>`,
		}		
	}else if(q.objective === "reach"){
		//VICTORY CONDITION CHANGED MANUALY IN ROOM 
		return {
			done: q.done,
			desc: q.title,
			//hint: `<strong>${q.counter}/${q.desired}</strong>`,
		}		
	} 
}


/**
 * turns tile into corpse (when there is enemy on the tile who gets killed)
 * @param {string} [key = "corpse"] - key of the new dungeon room associated with the tile
*/
export function slay(key = "corpse"){
	ext.dungeon.grid[head.z][head.x][head.y] = key;
	ext.dungeon.blueprint[head.z][head.x][head.y] = "x";
	//rose();
}


/**
 * turns tile into generic hallway (when there is one-time even on the tile)
 * @param {string} [key = "x"] - key of the new dungeon room associated with the tile
*/
export function deplete(key = "x"){
	ext.dungeon.grid[head.z][head.x][head.y] = key;
	ext.dungeon.blueprint[head.z][head.x][head.y] = "x";
	//rose();
}







/**
 * returns seeded pseudo-random number based on dungeon seed and current position 
*/
export function rgi(){
	rg.i(ext.dungeon.seed + head.x + head.y + head.z);
}





















function effects(){
	let pause = false;
	PC.effects.forEach( eff => {
		//delivers damage
		const actual_damage = ATTRIBUTES.resist_action_damage(eff.att, PC);
		const dyno_damage = ATTRIBUTES.damage_to_dyno(actual_damage);
		Object.entries(dyno_damage).forEach( ([key,value]) => ATTRIBUTES.change(key, value, PC) ); 
		
		//alerts running out
		if(eff.ticks <= 1){
			txt(`I lost the effect <strong>${eff.name}</strong>. `);
			pause = true;
		}
	})
	EFFECTS.tick(PC);
	
	if(ext.follower){
		ext.follower.effects.forEach( eff => {
			//delivers damage
			const actual_damage = ATTRIBUTES.resist_action_damage(eff.att, ext.follower);
			const dyno_damage = ATTRIBUTES.damage_to_dyno(actual_damage);
			Object.entries(dyno_damage).forEach( ([key,value]) => ATTRIBUTES.change(key, value, ext.follower) ); 
			
			//alerts running out
			if(eff.ticks <= 1){
				txt(`@fol lost the effect <strong>${eff.name}</strong>. `);
				pause = true;
			}
		})
		EFFECTS.tick(ext.follower);
	}
	
	if(pause) rose_halt();
	
}

export function machina(){
	
	
	if(ext.dungeon.oxygen !== undefined && ext.dungeon.blueprint[head.z][head.x][head.y] !== "E"){
		//console.error(ext.dungeon.oxygen);
		//console.warn(BARS.items.oxygen.value);
		if(ext.dungeon.oxygen > 0){	
			const change = -1;
			ext.dungeon.oxygen += change;
			//BARS.change(`oxygen`, change);
		}else{
			ATTRIBUTES.change("hp", -10);
			ATTRIBUTES.change("hp", -10, ext.follower);
		}
	}
	
	effects();
	
	if(PC.dyno.hp <= 0){
		ext.dungeon = undefined; //TODO!!
		return  main(101, "death");
	}
	
	/*
	if(debug) console.log("<"+scene+","+chapter+">"); //SIC
	if(!scene) return;
	head.chapter = chapter;
	head.scene = scene;
	*/
	//TODO - ENTRANCE IS INITIALLY TRIGGERED TWICE (MOVE & MANUALLY)


	
	//clr();
	/*
	if(room[chapter] === undefined){
		console.warn(`Warning! Dungeon room "${chapter}" not found! `); //SIC
		room.x(scene, mod);
	}else{
		room[chapter](scene, mod);
	}
	*/
	//rose();
	
	
	//done();
	
}




export function map(){
	DISPLAY.subtext("map");
}


export function explore(degree = 1){
	
	
	
	
	
	//visited 
	ext.dungeon.explored[head.z][head.x][head.y] = 2; //TODO - should be here?
	
	
	//explored
	const x_start = head.x - degree;
	const y_start = head.y - degree;
	const x_end = head.x + degree;
	const y_end = head.y + degree;
	for(let x = x_start; x <= x_end; x++){
		for(let y = y_start; y <= y_end; y++){
			if(ext.dungeon.explored[head.z][x]?.[y] !== undefined && ext.dungeon.explored[head.z][x][y] < 2){
				 ext.dungeon.explored[head.z][x][y] = 1;
			}
		}
	}
	/*
	if(degree === 1){
		const x = head.x;
		const y = head.y;
		ext.dungeon.explored[head.x][head.y] = true;
		
		[...directions, ...more].forEach( a => {
			if(ext.dungeon.explored[x + a.x]?.[y + a.y] !== undefined){
				 ext.dungeon.explored[x + a.x][y + a.y] = true;
			}
		})
		
	}
	*/
}