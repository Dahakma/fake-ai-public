import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";
import {bold, hint} from "Loop/text";

import {deplete, slay} from "Dungeon/engine";
import {rose_on, rose_off, rose_halt} from "Dungeon/rose";
import {loot, loot_multiple, sluttiness} from "Avatar/index";
import {origin_based, dismiss} from "Virtual/index";
import {quest} from "Virtual/quest"; 
import {combat} from "Gui/combat/combat/combat";

export const yelaya = {
	yelaya(input){
		switch(input){
		default:
		case 101:
			mile.feyd_yelaya++;
			rose_halt();
			switch(mile.feyd_yelaya){
			case 2:
				yel(`That was pretty gnarly! `);
				kat(`Well, we showed them! `);
				yel(`Yeah... I'm not very used to death and carnage. `);
				kat(`I thought you're an experienced adventurer? `);
				yel(`Of course I am! I just prefer to stay away from the combat.`);
				if(ext.class === 2 && mile.immersion){
					kat(`That's a very cowardly southerner way of thinking! `);
				}else{
					kat(`That sounds reasonable! Although I'm afraid we won't be able to evade enemies in these dark tunnels. `);
				}
				break;
			case 3:
				if(PC.att.char === 0){
					yel(`Shame we're crawling through dirty underground tunnels. For once I'm the prettier one and it's completely useless! `);
				}else{
					txt(`We were descending into darkness. `);
				}
				break;
			case 4:
				if(mile.immersion > 0){
					kat(origin_based(
						`I managed to survive and escape but everybody I knew was dead. `
						, `In conclusion, it wasn't easy to grow on the streets. `
						, `That bastard broke my heart and ruined my whole life. `
						, `And that's why I became a stripper and harlot. `
						, `If you ask me, he had that coming and I would do it again! But the people were obviously furious. `
						, `And that's how my poor little s${String.fromCharCode(105)}ster died. `
						, `But those bastards turned against me and wanted to kill me! So obviously I did not stay. `
						, `I don't let my father tell me what to do!`
						, `They treated me horribly and degraded me every day. And as the final humiliation let me go, broken and without my honor. `
						, `It didn't go wrong! It went horribly right! I'm still disgusted by what I helped to create and wished I could take it back! `
						, `I knew that reading those forgotten tones might not be the brightest idea. But I was curious and thought my will was strong enough. Now I know better.  `
						, `It was a fun life but I guess I should know it couldn't last forever. `,
					));
					yel(`Seven hells! That's awful! `);
					kat(`My tragic past is very dark! `);
					yel(`It definitely is!`);
					kat(`Why did <i>you</i> became an adventurer? `);
				}else{
					kat(`I have to kill the evil wizard. `);
					yel(`Which one?`);
					kat(`The evilest one!`);
					yel(`Huh. That indeed wasn't a long story. `);
					kat(`I told you! Why did <i>you</i> became an adventurer? `);
				}
				yel(`Well, after my father died and my oldest brother threw me out, I had nowhere to go. And I thought being an adventurer might be more fun than being a whore.`);
				if(ext.class === 1){
					kat(`Yeah.`);
					yel(`No offense! I didn't mean to offend you! There is nothing wrong with being a whore! `);
					kat(`I'm not offened! I'm not an active harlot anymore! `);
				}else{
					kat(`That's pretty dark too!`);
				}
				break;
			case 5: 
				{
					const slut = sluttiness(PC).details;
					if(!slut.topless && !slut.bottomless) return txt(`I went first and @yel followed me. `);
								
					if(slut.topless && slut.bottomless){
						yel(`So... why are you nude? I'm sorry, I don't want to be intrusive but I can't help myself! I'm trying my best to not stare at your pussy and breasts but it isn't easy, those are really nice tits! `);
					}else if(slut.bottomless){
						yel(`So... why don't you wear any pants? I'm sorry, I don't want to be intrusive but I can't help myself! I'm trying my best to not just stare at your pussy! `);
					}else{
						yel(`So... why are you topless? I'm sorry, I don't want to be intrusive but I can't help myself! I'm trying my best to not stare at your breasts but it isn't easy, those are really nice tits! `);
					}
					if(ext.game_money < 15){
						kat(`Well, I can't afford better clothes.`);
						yel(`Oh!`);
					}else{
						if(mile.slut > 1 && mile.immersion < 0){
							kat(`Don't worry! I don't mind NPC's like you staring at my nude body. `);
							yel(`Okay?`);
						}else{
							kat(`Why not?`); 
							yel(`Right.`);
						}
					}
					kat(`It took you long before you asked. `);
					if(ext.class === 2){
						yel(`I thought it was a barbarian thing and didn't want to sound racist, maybe you felt too hot or something like that. Or you got tricked by a witch and was too embarrassed to admit that. `);
					}else if(ext.race === 0){
						yel(`I didn't want to make things weird. `);
					}else{
						yel(`I thought it was ${ext.race === 1 ? "elvish" : "orc"}  thing and didn't want to sound racist. `);
					}
				}
				break;
							
			case 6:
				yel(`AHHHHH!`);
				kat(`WHAT!?!`);
				yel(`...wait, false alarm! It was just a bat!`);
				kat(`You seem to be pretty twitchy? `);
				yel(`Duh, we're in the middle of a dark, monster-filled dungeon!`);
				kat(`You aren't an experienced adventurer, right? `);
				yel(`Well... not especially. `);
				kat(`And I suspect you didn't even arrive from Sintre? `);
				yel(`Okay! I was born in Feyd Mora and this is my first adventure!`);
				kat(`And you lied about the treasure, just to trick me to go with you?`);
				yel(`I didn't lie about the treasure! I swear! My grandfather told me about it! It has to be here! `);
				break;
					
			default: 
				txt(`Together we were exploring the cavern. `);
				break;
			}
			deplete();
			break;
				
		}
	},
	
	
	yelaya_final(input){
		switch(input){
		case 101: 
			rose_off();
			kat(`Wait! Did you hear that!?! `);
			yel(`What?!?`);
			kat(`Like... very scary and evil groaning!?!`);
			yel(`Oh! That has to be the Beast! `);
			kat(`The Beast!?!`);
			yel(`Yeah, the immortal magical Beast guarding the treasure! `);
			kat(`Why haven't you told me?!`);
			yel(`I'm sorry! I completely forgot about that part of the grandfather's story!`);
			link(`FIGHT!`, 102);
			break;
			
		case 102: 
			combat("y_beast");
			txt(`The beast was dead! `);
			link(`Continue.`, 110);
			break;
				
		default:
		case 110:
			rose_off();
			txt(`After the epic fight, we managed to defeat the Beast. And the coffer with the treasure was in our reach. However, as soon as it fell dead to the cavern floor, the ground began to shake. `);
			kat(`WHAT IS GOING ON?!`);
			yel(`I HAVE NO IDEA!`);
			txt(`Stones began falling down from the ceiling, we were confused and terrified. And then with a loud crack, the earth opened and devoured poor Yelaya. `);
			yel(`Ahhhh! `);
			txt(`Or not, not yet. She was still hanging above the bottomless chasm and her fingers were slipping. Oh no! I noticed the coffer slipping towards the edge of the chasm too! `);
			link(`Save Yelaya!`, 120);
			link(`Save the coffer!`, 130);
			break;
				
		case 120:
			//ext.quests.yelaya.reached = true;
			rose_on();
			quest.done("yelaya");
			mile.feyd_yelaya_saved = 1;
			slay();
				
			if(mile.immersion < 0){
				txt(`After a brief hesitation, I rushed to save my companion. She was just a soulless NPC but saving her was a heroic thing to do and I hoped the game will reward me for it somehow. `);
			}else{
				kat(`YELAYA!`);
				//TODO
			}
			txt(`I cried and hastily grabbed her hand in the last moment. I helped her out and she immediately tightly hugged me, tears in her eyes. `);
			yel(`You saved me! `);
			kat(`You're welcome!`);
			yel(`Where is the coffer?!`);
			txt(`It was gone. Only now Yelaya began crying for real. `);
			break;
				
		case 130:
			//ext.quests.yelaya.reached = true;
			rose_on();
			quest.done("yelaya");
			mile.feyd_yelaya_saved = -1;
			slay();
			dismiss();
				
			txt(`Without hesitation I reached for the coffer and save it. Unfortunatelly it was too late for Yelaya. Her fingers slipped and she felt down to her death. The chasm had to be very deep, I could hear Yelaya's fading frightened screaming for several seconds. `);
			if(mile.immersion <= 0){
				txt(`I did not care. She was just a NPC. The game tried to trick me and take away my treasure but I did not fall for such an obvious, cheap, emotional manipulation. `);
			}else{
				kat(`YELAYA! `);
				txt(`I cried but she was gone. I felt awful, maybe I made a wrong decision? But I desperately needed the money, the fate of the realm depended on me! `);
			}
				
			loot(2, "legendary");
			break;
		}
		
	},
}