import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";
import {bold, hint} from "Loop/text";

import {deplete, slay} from "Dungeon/engine";
import {rose_on, rose_off, rose_halt} from "Dungeon/rose";
import {loot, loot_multiple, sluttiness} from "Avatar/index";
import {origin_based, dismiss} from "Virtual/index";
import {quest} from "Virtual/quest"; 
import {roll} from "Virtual/index";
//import {train} from "Virtual/training"; 


const source = {
	evasion: {
		title: "Evasion",
		text: "Each combatant has a chance to evade the first attack in the round aimed at her. The player's <i>Evasion</i> is based on her <i>Dexterity</i>.",
	},
	reflex: {
		title: "Reflex",
		text: "The <i>Reflex</i> is used to calculate initiative in combat. Characters with the fastest (lowest) reflex are allowed to act first. The player's <i>Reflex</i> is based on her <i>Dexterity</i>. ",
	},
	critical: {
		title: "Critical hit",
		text: "The <i>Critical hit</i> is a lucky hit that completely surprised the opponent (ignoring her <i>Evasion</i>) and hits exactly the right spot (bypassing her <i>Armor</i>).  ",
	},
	looting: {
		title: "Looting",
		text: "All found items are automatically added to player's inventory. Clicking on them will equip/use them immediately. ",
	},
	
	elixirs: {
		title: "Elixirs",
		text: "Potions refill player's health or energy or boost her attributes with temporal effects. However, they could have (un)desirable side effects. ",
	},
	
	lockpick: {
		title: "Lockpicking",
		text: "Player can try to use your lockpicking skill to open doors and chests. The chance for success is based on the level of player's skill and the difficulty of the task. Usually she has as many attempts as she wants but each failed one costs her energy or health. ",
	},
	
	follower: {
		title: "Follower",
		text: "The follower can help you in combat or stand in for you during skill checks. The follower could be dismissed in <i>Follower Screen</i>.",
	},
	
	presence: {
		title: "Presence",
		text: "Characters with a higher presence tend to attract more enemy attention. ",
	},
	
	armor: {
		title: "Armor",
		text: "Armor level, based on equipped  armor, will reduce physical damage caused by enemy attacks by × percent. ",
	},
	
	encumbrance: {
		title: "Encumbrance",
		text: "Having equipped heavier armor than the <i>strength</i> of the character allows will have negative impact on <i>evasion</i> and <i>reflex</i>. ",
	},
}

export const train = function(key){
	
	if(mile[`train_${key}`]) return;
	mile[`train_${key}`] = true;
	
	if(source[key]) txt(`❖ <strong>${source[key].title}</strong><br>${source[key].text}`);

}



export const tutorial = {
	tutorial_armor(){
		rose_halt();
		train("armor");
		train("encumbrance");
		deplete();
	},
	
	tutorial_follower(){
		rose_halt();
		train("follower");
		train("presence");
		txt(`In the sidebar or by pressing [f] you can access the <i>Follower screen</i>. `);
		deplete();
	},
	
	
	tutorial_combat(){
		rose_halt();
		train("evasion");
		train("critical");
		train("reflex");
		txt(`In the sidebar or by pressing [c] you can access the <i>Character screen</i>. `);
		deplete();
	},
	
	tutorial_lockpick(index){
		switch(index){
		case 101: 
			rose_halt();
			train("lockpick");
			txt(`The way forward was blocked by locked door. `);
			roll(`Lockpick the door. `, "lockpick", -2, 110, 111, "eng", -5);
			break;
			
		default:
		case 110: 
			txt(`I manage to open the door. `);
			deplete();
			break;
				
		case 111: 
			txt(`I failed to open the door. `);
			roll(`Lockpick the door. `, "lockpick", -2, 110, 111, "eng", -5);
			break;
		}
	},
	tutorial_loot(){
		rose_halt();
		loot(1, "weapon");
		train("looting");
		txt(`In the sidebar or by pressing [i] you can access your <i>Inventory</i>. `);
		
		deplete();
	},
	tutorial_elixirs(){
		rose_halt();
		loot(2, "elixir");
		train("elixirs");
		deplete();
	},
}