import {primary} from "./generic/primary";
import {enemies} from "./generic/enemies";
import * as hallway from "./generic/hallway";
import * as chest from "./generic/chest";

import {yelaya} from "./act_1/yelaya";
import {tutorial} from "./act_1/tutorial";
import {other_1} from "./act_1/other_1";

import {ste} from "./act_2/stede";
import {lys} from "./act_2/lyssa";
import {gal} from "./act_2/galawar";
import {sir} from "./act_2/sirael";

import {other} from "./generic/other";
import {doors} from "./generic/doors";
import {wood} from "./generic/wood";
import {ship} from "./generic/ship";
import {minigames} from "./generic/minigames";

import {ghost_ship} from "./act_2/ghost_ship";
import {sea} from "./act_2/sea";
import {manor} from "./act_2/manor";

export const room = {
	...primary,
	...hallway,
	...enemies,
	
	...chest,
	...doors,
	
	...wood,
	...ship,
	...minigames,
	
	...ghost_ship,
	...sea,
	...manor,
	
	lys,
	ste,
	gal,
	sir,
	...yelaya,
	...tutorial,
	
	...other,
	...other_1,
}

