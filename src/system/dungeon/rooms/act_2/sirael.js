import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal, fol} from "Loop/text";
import {bold, hint} from "Loop/text";

import {deplete, slay} from "Dungeon/engine";
import {loot, loot_multiple, sluttiness} from "Avatar/index";
import {origin_based} from "Virtual/index";
import {dyke} from "Virtual/text";
import {rose_on, rose_off, rose_halt} from "Dungeon/rose";


	
/*
1 - never awakened
2 - meh
3 - fine
4 - put down
5 - Sirael
6 - put down
7 - jerk


*/	
	
export const sir = function(input){
	switch(input){
		default:
		case 101:
			mile.goth_sir++;
			rose_halt();
			switch(mile.goth_sir){
				case 1:
					if(mile.immersion < 0){
						kat(`So, who exactly you are? I know orcs and elves but in no movie I've seen anybody like you.`);
					}else{
						kat(`So, who exactly you are? I don't think I've seen anybody similar to you in the whole Beletriad. `);
					}
					fol(`I am a homunculus. `);
					kat(`What does that mean? `);
					fol(`I am an artificial being created by Master Arazor.`);
					kat(`So he wasn't making it up! He really created you. `);
					fol(`Indeed, he did. `);
					break;
					
				case 2:
					kat(`How exactly do you create a sentient being? `);
					fol(`I do not know the exact process but it is very complicated and even to Master Arazor it took decades of studying and experiments and the process is still not completely safe.`);
					kat(`Are you the only homunculus he created? `);
					fol(`No. I'm his fifth female servant. `);
					kat(`So he has five ${dyke() ? "attractive " : ""}maids fully at his disposal? `);
					fol(`No. He has four. So far he made seven. The First never awakened, the Third and the Fourth had to be put down because they were wrong and could not behave. `);
					kat(`That's harsh!`);
					fol(`I suppose.`);
					break;
					
				case 3:
					rose_off();
					kat(`The collar means you're Arazor's slave? `);
					fol(`No, I'm his creation, not a slave. He created me and I serve him.`);
					kat(`Sounds like slavery to me. Would he let you go if you wanted to leave? Or do you have to serve him forever? `);
					fol(`But I do not want to leave. And I enjoy serving him. `);
					if(mile.immersion < 0){
						kat(`Damn. The insane logic of AIs always makes my head hurt.`);
						fol(`That was not my intention. `);
					}
					kat(`And you would do anything he would tell you? `);
					fol(`Yes. I think so. `);
					kat(`Even like jumping down from the top of a tower?`);
					fol(`He would never order me to do that. Why would he? Seems like a pretty pointless endeavor. `);
					kat(`It's a hypothetical question. What if he ordered you to kill yourself? `);
					fol(`Why would he order me to do that? Making me was an extremely complicated process. But if you are asking, I would die for Master Arazor if it was necessary.`);
					kat(`That's awful! `); // I'm not sure if he deserves that. 
					fol(`Or die for you, since Master Arazor ordered me to follow your orders and protect you. `);
					kat(`Oh. That's nice!`);
					fol(`But Master Arazor would not be very happy if you let me die. `);
						
		
					link(`It's good to follow reasonable orders of authorities, like your master or your captain. Especially our captain.`, 301);
					link(`You should try to be more than just Arazor's slave! You should be free and do whatever you want!`, 302);
					break;
								
				case 4:
					kat(`So, you're like a complete woman if you know what I mean? `);
					fol(`Yes. Complete woman with the exception I cannot procreate. `);
					kat(`I knew it! Just another perverted nerd!`);
					fol(`Do you wish to see?`);
					if(mile.slut > 9){
						kat(`Sure!`);
						txt(`@sir readjusted her clothes to let me see her pussy. It was smooth and looked pretty ordinary. `);
						fol(`Do you wish to touch it? `);
						kat(`Sure, why not. `);
						txt(`I shrugged and gently checked her with my fingers. `);
						kat(`You can tell your master he did a good job. `);
						fol(`Thank you!`);
					}else{
						kat(`No! That won't be necessary!`);
						txt(`I had to stop the accommodating homunculus. `);
					}
					if(mile.slut < 4){
						kat(`And do you like, you know, have sex with Arazor? `);
					}else{
						kat(`And Arazor fucks you? `);
					}
					fol(`I serve him in any way he asks, sometimes alone, sometimes with my s${String.fromCharCode(105)}sters. `);
					break;
					
				default:	
					mile.goth_sir = 5;
					
			}
			deplete();
			break;
			
		case 301:
			mile.goth_sir_indy--;
			rose_on();
			fol(`I know. I am happy I can serve Master Arazor and you!`);
			break;

		case 302:
			mile.goth_sir_indy++;
			rose_on();
			fol(`But I want to serve Master Arazor!`);
			kat(`Come on! You have to want more than being just his maid and receptionst!`);
			break;
			
	}
}





