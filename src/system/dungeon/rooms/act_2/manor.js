import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";
import {bold, hint} from "Loop/text";

import {rg} from "Libraries/random";
import {roll, boost} from "Virtual/index";
import {deplete, rgi} from "Dungeon/engine";
import {rose_halt, rose_on, rose_off} from "Dungeon/rose";
import {dibz, loot, loot_multiple} from "Avatar/index";



function search(){
	roll(`Look for a secret door. `, "int", counter.door, 110, 111, "eng", -5, `Let @fol to look for a secret door. `, 210, 211);
	boost("int", "eng");
	if(hardbug) link(`## Find door`, 110);
}



export const manor = {
	dead_end(input, mod){
		switch(input){
			case 101:
				rgi();
				counter.door = mod ?? rg.array([2, 3, 4]);
				txt(`Out of nowhere the tunnel ended. Which was pretty suspicious. `);
				search();
				break;
				
			default:
			case 110: 
				txt(`Eureka! I found a secret switch. A panel masked as a solid wall moved away and revealed a hidden cache. `);
				loot_multiple([2,"money", "elixir", "elixir", "weapon", "armor"], [1, "legendary"])
				deplete();
				break;
				
			case 111: 
				txt(`I carefully checked every wall but I did not find anything. `);
				search();
				break;
				
			case 210: 
				txt(`I let @fol to try @his luck. We were both surprised when @he immediately found a secret switch.  A panel masked as a solid wall moved away and revealed a hidden cache. `);
				loot_multiple([2,"money", "elixir", "elixir", "weapon", "armor"], [1, "legendary"])
				dibz();
				deplete();
				break;
				
			case 211: 
				txt(`I let @fol to try @his luck. @He was not very lucky. `);
				search();
				break;
			
			}
	},
	
	burned_cabined(){
		txt(`Among the burned rubble @we discovered a half-buried but unlooted medical cabinet.`);
		loot_multiple(["elixir"], ["xAnti"]);
		rose_halt();
		deplete();
	},
	
	
	burned_treasury(){
		txt(`This room probably served as a treasury. Unfortunately, what was not destroyed by fire was already looted long ago. `);
		txt(`@We searched through the ash and found only several coins. `);
		loot("money");
		rose_halt();
		deplete();
	},
	
	burned_beams(input){
		switch(input){
			case 101:
				rose_off();
				txt(`The bare, charred beams survived the fire but did not promise too much structural stability. `);
				txt(`I found that when I accidentally leaned against them and several collapsed on top of me. `);
				roll(`Dodge! `, "evasion", 2, 102, 103, "hp", -30);
				break;
			
			default:
			case 102: 
				txt(`In the last second, I jumped away and just heard the debris collapsing behind me. `);
				rose_on();
				deplete();
				break;
			
			case 103:
				txt(`I was alive but buried under a pile of debris. `);
				roll(`Push charred beams away. `, "str", 2, 110, 111, "eng", -5, `@FOL! HELP! `, 210, 211);
				boost("str", "eng");
				break;
				
			case 110:
				txt(`It was exhausting but I was able to push the broken beams away and soot-covered I rose. `);
				rose_on();
				deplete();
				break;
				
			case 111:
				txt(`I pushed hard but the beams were still tightly squeezing me. `);
				roll(`Push charred beams away. `, "str", 2, 110, 111, "eng", -5, `@FOL! HELP! `, 210, 211);
				boost("str", "eng");
				break;
				
			case 210:
				txt(`@fol tried to save me and quickly pushed the broken beams away until I was free. `);
				rose_on();
				deplete();
				break;
				
			case 211:
				txt(`@fol was desperately trying to save me but I was still stuck under the debris. `);
				roll(`Push charred beams away. `, "str", 2, 110, 111, "eng", -5, `@FOL! HELP! `, 210, 211);
				boost("str", "eng");
				break;	
		}
	},	
	
}









