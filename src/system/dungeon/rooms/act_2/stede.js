import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal, fol} from "Loop/text";
import {bold, hint} from "Loop/text";

import {deplete, slay} from "Dungeon/engine";
import {loot, loot_multiple, sluttiness} from "Avatar/index";
import {origin_based} from "Virtual/index";
import {rose_on, rose_off, rose_halt} from "Dungeon/rose";

export const ste = function(input){
	switch(input){
		default:
		case 101:
			mile.goth_ste++;
			rose_off();
			switch(mile.goth_ste){
				default: 
					//EMPTY
					rose_on();
					break;
					
				case 2:
					kat(`So how did you become a captain? `);
					fol(`That's a long and tangled story and I doubt anybody would want to hear it. `);
					kat(`I do! And if I get bored I'll just click through it and skip it! `);
					fol(`I was born in Sintre in a very respected and influential family. Unfortunately, my father squandered most of our fortune and I was forced to marry an old, wealthy widow.`);
					kat(`That's awful! Was she at least good-looking?`);
					fol(`She was very attractive. And in bed, she taught me about things I didn't even know were possible!`);
					kat(`That sounds good!`);
					fol(`But she was a nagging, arrogant bitch and nothing I did was good enough for her. `);
					kat(`That sounds bad!`);
					fol(`Yeah, it was! My father died when I was still young and I had to take care of the family business. And I was pursued by bad luck. I invested in ships that sunk and rented fields destroyed by hailstorms. I was falling into deeper and deeper debt and my wife was getting more and more irksome. `);
					kat(`What did you do? `);
					fol(`I told her and the kids goodbye, spent all my remaining money on a caravel and became a privateer. I naively hoped that will bring me honor and fame and wealth. `);
					chapter(`Continue. `, `x`, 101, rose_on);
					//kat(`That's all? `);
					//fol(`Yeah. `);
					break;
					
				case 3: 
					kat(`You don't have to be always so gloomy.`);
					fol(`Why not? Life is suffering and then you'll die! `);
					kat(`But there are entertaining bits in between?`);
					fol(`Like what? Your endeavors will fail, your friends will betray you and your lovers will leave you behind when they find somebody new. `);
					kat(`My friends will never betray me! And I know how to keep my lovers!`);
					fol(`You're young and naive! `);
					chapter(`Continue. `, `x`, 101, rose_on);
					break;
					
				case 4:
					kat(`And then you were a captain of your own ship?`);
					fol(`Yeah. I didn't have any experience with sailing and took a while before I gained the respect of the crew. But we were very lucky, raided the coast and captured many ships. I should've known it can't last!`);
					kat(`It's still pretty amazing you became so successful despite your lack of experience!`);
					fol(`I had a natural talent for sailing and navigation. And then I met Meril and everything went to hell. She was the most beautiful and deadly woman I ever met...`);
					kat(`I'm standing right here!`);
					fol(`Amazing imposing blonde with perfect body and insatiable sexual lust! And she fell in love with me! Back then I didn't suspect anything even though I should. `);
					kat(`It didn't go well? `);
					fol(`She pushed me into riskier missions and we often failed, losing men without accomplishing anything. And she was whore who was sleeping with half of the crew, only to make me jelaous! My men were laughing at me behind my back! `);
					kat(`What a bitch!`);
					fol(`Then things got even worse, of course. Horrible weather, no booty, scurvy...`);
					kat(`... do you know lemons help with that? `);
					fol(`With what?`);
					kat(`With scurvy. Because of lack of vitamin C. @bio told us that in biology. `);
					fol(`Oh. I'll try remember it. The crew was getting restless. I had a plan to turn our luck around, a very good and reliable one. But those stupid bastards organized a mutiny and took away my ship!`);
					kat(`That's horrible! `);
					chapter(`Continue. `, `x`, 101, rose_on);
					break;		
			}
			deplete();
		
			break;
	}
}





