import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, fol, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";

import {slay, deplete, exit, objective, stairs, survival} from "Dungeon/engine";
import {roll, ATTRIBUTES} from "Virtual/index";
import {ra} from "Libraries/random";
import {capitalise} from "Libraries/dh";
//import {map, explore} from "Dungeon/map";
import {dibz, loot, loot_multiple} from "Avatar/index";
import {We, we, fol_name, he, him, his, He} from "Virtual/text";
import {rose_on, rose_off} from "Dungeon/rose";
import {quest} from "Virtual/quest"; 

import {BARS} from "Virtual/index"; 


export const sea = {
	fortress_stumble(input){
		const damage = 30;
		switch(input){
			case 101:
				if(ext.follower){
					rose_off();
					txt(`We climbed on the top of the ruins. There was a wonderful view on the old fortress, the small island, dark blue Dewyrain sea and our <i>${mile.ship}</i>. From this distance, the mighty caravel looked like a little toy. `);
					txt(`${fol_name()} was for a moment dazzled by the setting sun and made a fatal step back. The crumbled mortar and stones on the edge of the wall gave way. I could try to save ${him()} but it was already too late there was a risk ${he()} will only drag me down with ${him()}. `);
					roll(`Grab ${him()}. `, "str", ext.follower.att.str, 110, 111, "hp", -damage);
					link(`Scream. `, 200);
					link(`Watch ${him()} fall. `, 201);
				}else{
					txt(`I climbed on the top of the ruins. There was a wonderful view on the old fortress, the small island, dark blue Dewyrain sea and <i>${mile.ship}</i>. From this distance, the mighty caravel looked like a little toy. `);
				}
				break;
			
			default:
			case 110: 
				rose_on();
				txt(`Without hesitation I dashed forward and grabbed ${fol_name()}. For a brief dreadful moment we were ballancing on the edge of the wall but we did not fell down. `);
				kat(`Be more careful! `);
				fol(`Thanks for saving me! `);
				break;
				
			case 111: 
				rose_on();
				ATTRIBUTES.change("hp", -damage, ext.follower);
				txt(`Without hesitation I dashed forward and desperately grabbed ${fol_name()}. However, ${his()} heavy body toppled us over and fell down, landing in thorny bushes right next to a pile of rubble which would definitely killed us. `);
				break;
				
			case 200:
				rose_on();
				ATTRIBUTES.change("hp", -damage, ext.follower);
				kat(`Nooo! ${fol_name()}! `);
				txt(`I loudly screamed as I watch ${him()} fall. ${He()} fortunately landed in thorny bushes, right next to a pile of rubble which would definitely killed ${him()}.`);
				break;
				
			case 201:	
				rose_on();
				deplete();
				ATTRIBUTES.change("hp", -damage, ext.follower);
				txt(`I curiously watch ${him()} fall down. Ouch, that had to hurt! Luckily ${he()} landed in thorny bushes and not on the pile of rubble right next to them. `);
				break;
		}
	},
	
	
	fortress_not_abandoned(){
		if(ext.follower){
			txt(`${We()} were exploring the cellars and dungeons under the abandoned fortress. Some tunnels were filled with cobwebs and debris, others were suspiciously passable. `);
			fol(`Look! `);
			txt(`In the mud in the tunnel were freshly-looking footprints. It seemed the fortress might not be completely abandoned. `);
		}else{
			txt(`${We()} were exploring the cellars and dungeons under the abandoned fortress. Some tunnels were filled with cobwebs and debris, others were suspiciously passable. I began to suspect the fortress might not be completely abandoned. `);			
		}
	},
		
		
		
	fortress_smugglers(){
		mile.goth_smugglers = 1;
		txt(`${We()} discovered a secret den of smugglers! The contraband, probably cargo stolen by pirates, was temporarily hiden her before being shipped somewhere else. `);
		loot(4,"clothes","armor","elixir","elixir");
		deplete();
	},	
		
		
	karg_tomb(){
		txt(`${We()} finally reached the main burial chamber. The ceiling was low but the room was otherwise quite spacious. The walls were covered by epic scenes of brutal battles and opulent banquets. The majority of the grave goods crumbled away or were of questionable value but there were still a few very nice pieces ${we()} decided to take with us. `);
		txt(`${We()} searched everything but it eventually became obvious the scepter was not there. ${We()} found only a scroll written in an unknown language. The only hope was that Arazor might be able to translate it. `);
		
		mile.goth_arazor = 5;
		quest.done(`karged`);
		loot_multiple([1,"legendary"], [3,"weapon","armor"]);
		deplete();
	},
		
	karg_queen(){
		txt(`${We()} opened a passage into a burial chamber. In the middle of the small room decorated with epic murals showing glory and wealth of Aquilonian court was standing a golden sarcophagus depicting a beautiful woman. `);
		loot_multiple([1,"legendary"], [3,"clothes"]);
		deplete();
	},
	
	aqu_tomb(){
		txt(`${We()} entered the main burial chamber. The walls were covered with murals in old Aquilonian style, around them were placed luxurious grave goods and in the middle of the room was standing a massive stone sarcophagus belonging to an unknown Aquilonian noble. `);
		loot_multiple([1,"legendary"], [3,"weapon","armor"]);
		deplete();
	},
		
		
	
	shipwreck_treasury(){
		txt(`I was stunned! The sunken ship had to transport a tribute or plunder! The wealth was scattered on the floor, a most of it unfortunatelly fell through the ghastly breach in the hull and forever ended on the dark bottom, hundreds meters under the reefs. `); 
		mile.ship_wreck = 3;
		loot_multiple(["money"], ["legendary"], ["armor"], [2,"elixir"] ); 
		deplete();
	},
		
	
	goth_dorf(){
		txt(`<strong>We cut down a tree. </strong>`);
		quest.collect("goth_dorf");
		deplete();
	},
	
	colaps(index, mod){
		
		
		
	},
	
	
	
	
	
	oskyl(){
		mile.goth_arazor = 7;
		quest.done("oskyl");
		exit();
	},
	
	
	light(){
		txt(`Light!!! I could see light! The exit had to be close! `);
	},
	
	leave(){
		if(ext.dungeon.oxygen !== undefined){
			ext.dungeon.oxygen = ext.dungeon.oxygen_max;
		}
		txt(`${We()} finally reached the exit! `);
		link(`Leave the dungeon. `, null, undefined, ()=> exit(true));	
	},	
	
	
	
	
	
	
	
	
	
}