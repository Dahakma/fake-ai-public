import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, fol, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";

import {slay, deplete, end, objective, stairs, survival, explore, map} from "Dungeon/engine";
import {roll} from "Virtual/index";
import {We, we, he, fol_name} from "Virtual/text";
import {ra} from "Libraries/random";
//import {map, } from "Dungeon/map";
import {dibz, loot, loot_multiple} from "Avatar/index";
import {rose_on, rose_off} from "Dungeon/rose";
import {combat} from "Gui/combat/combat/combat";


export const ghost_ship = {
	ghost_mizzenmast(){
		fol(`What is this!?`)
		txt(`The mizzenmast was covered by long scratches. `);
	},
	
	ghost_captain(input){
		switch(input){
			default:
			case 101:
				txt(`Based on the size, tasteful furnishings and present navigation equipment, this has to be the captain's cabin. `);
				link(`Captain's log. `, 102);
				break;
			
			case 102: 
				
				txt(`<i>28th Astron</i>`);
				txt(`<i>The ship is still becalmed. There is absolutely no wind nor current and we are stuck. We continued kedging but it did not help and all hands are exhausted. The calmness of the sea seems supernatural. Sailors blame our ventomancer and suggested skinning him alive but Gwynt swore he is trying his best and that the air is strangely impotent and lacking any energy. He tried to focus until his nose bleed and he passed out but he was not able to summon even the weakest breeze. </i>`);

				link(`The last entry. `, 103);
				break;
		
			case 103: 
				txt(`<i>5th Thrimidge</i>`);
				txt(`<i>The situation is starting to look serious. I was forced to half the water rations again. The crew is tired and irritable and it is not easy to keep up the morale. Gwynt is gloomy and has not spoken with anybody in days. We have to hope the gods will favor us and the weather will change soon. We do not have supplies for more than another week.  </i>`);
				txt(`That's all!?!`);
				link(`Loot the cabin. `, 104);
				break;
				
			case 104:
				//loot_multiple([1, "money"], [2, "clothes", "elixir"]); TODO
				loot_multiple(["money"], [1, "elixir"], [1, "weapon", "armor"]);
				deplete();
				break;
		}
	},
	
	ghost_blood(){
		if(ext.follower){
			txt(`@fol stopped me and pointed on the deck and bulkheads. They were covered with brown-red splatter. It could be blood. Or anything else. `);
		}else{
			txt(`I noticed brown-red splatter covering the deck and bulkheads. It could be blood. Or anything else. `);
		}
	},
	
	ghost_trashed(){
		txt(`This cabin was completely trashed. Pieces of broken furniture were scattered all around. What the hell happened here? `);
	},
	
	ghost_bad(){
		if(ext.follower){
			fol(`I have a very bad feeling. `);
			kat(`Yeah. `);
			txt(`We explored two-thrids of the ship and still did not find anything substantial. There were signs of violence but no bodies. `);
		}else{
			txt(`I started to have a really bad feeling about this. I explored two-thirds of the ships and I still was not able to find anything substantial. There were signs of violence but no bodies. `);
		}
	},
	
	ghost_below(){
		txt(`${We()} descended to bilge. We were deep below the water line, there was no natural light, the air here felt stale and smelled with decay. `);
	},
	
	
	ghost_rats(){
		if(ext.follower){
			fol(`Have you noticed there are no rats? `);
			kat(`That's a good thing, I don't like rats. `);
			fol(`Or is it?!? `);
		}else{
			txt(`I continue further, stumbling over coiled ropes. `);
		}
	},
	
	ghost_gwynt(input){
		switch(input){
			default:
			case 101: 
				present(
					[2, `Chosen`, `rags`, `creepy`],
				);
				rose_off();
				txt(`I was overwhelmed by eerie dread. Something terrible was about to happen. And then something in the darkness, on the very edge of the weak flickering light emanated by ${we("our torches","my torch")}, moved. What I assumed was a pile of rags or coiled cables began briskly creeping towards us. `);
				txt(`I was frozen with fear, unable to move, but I recognized those gaunt twisted limbs were arms and legs! The creature straightened in front of us. It was something like a human but he was scrawny and his skin was pale and covered with filth. His eyes were gone. His eye sockets were filled with dirt and dried up blood. `);
				kat(`W... what are you? `);
				npc(2, `I was Gwynt. `);
				txt(`His voice was hoarse but intelligible and serene. `);
				kat(`W... where's the rest of the crew?`);
				npc(2, `I tried to save them. `);
				kat(`Where are they?`);
				npc(2, `They didn't want to be saved. `);
				txt(`His empty stare was unnerving. I made a step back but he made a step forward, invading my personal space. `);
				kat(`W... what happened? `);
				npc(2, `I did what I had to to survive. It was the only way. Perform the ritual. Made the deal. `);
				link(`Deal?! `, 102);
				break; 
				
				
			case 102:
				kat(`Deal? What deal? `);
				txt(`He began madly laughing. `);
				npc(2, `With <i>the Thing</i> under the waves. It accepted me as one of the <i>chosen</i>. `);
				kat(`And the crew? `);
				npc(2, `They were weak and cowardly to swear the dark oath. They were sacrificed. Their bodies were devoured and spirits harnessed. `);
				kat(`O... okay... By devoured you mean.... oh my... but maybe you could go with me and tell everything to the port authorities? `);
				npc(2, `I won't ever leave this ship. Our spirits are interwinded. And neither will you. `);
				link(`Fight! `, 200);
				break; 
				
			case 200:
				combat("chosen");
			/*	//TODO
				link(`Next. `, 300);
				break;
				
			case 300:
			*/
				rose_on();
				txt(`${We()} slayed the monster. Or so ${we()} thought. ${we("Our weapons","My weapon")}  mauled its body beyond recognition and the dark ichor was flowing from the countless devastating injuries. But the thing which was once human named Gwynt only laughed. `);
				if(ext.follower){
					npc(2, `F̰̥̯ͯo̙͙̘̹̘ͫ͋̂̐̍̚ō̱l̘̹̑͑͗͆s͛̐̃̌!̘̫̩͈̩̌̄ ̭͔͓͖̞̜̀ͫͪ͌͆ͣͅD̲̠̜̖́o̦̫̐̊̓̉̅͗ ͈̺ͣͬy̭̮̋ͬo̳̺̭̱͗̚ṵ͚̗̹̖̎̆͊̔ͪ ̳ͯ͑ͫ̈́̐̚r̮̱͎̖̎̌e͔̘̠̫͕a̟͚̰̝̒̏̓͐͗̆lͯ̉l̖̝̱̤̮͓̆ͅỳ ̖̈́̇͌͊͂͐ͤt̮̟̖͎͉̝͓̾̓͌ͦhͭ̔ͬͩ̓̂i̜ͅn̖͇͑̾k̳̠̒ͣ̍ ͈̦͎Ȉ̙͓̠̙̹͕̄ͣͮͤ͆͐ ̖͔̮͛c̲͐o͔ŭ̯̱̣̹̰̄l͙̲̗̑͊̿͆̌ḏ̏̅̄ͪ̔ ̗͇̂̉͂̈́ͯ̈́ͤb͙̗͕̜͓ͧ͋ͤe̜͙̭̝̞̲ ̭ͬk̟̗̗͕̬̘͉̾̀̓i̝̠͙͎̮̥̾ͅl͖̱̱͊̂ͨͩ́ͯͤl̟ͬè̮̹͎̹̘ͯͮ̂̆́d̅̈̍?̳̬̺̞ͫ̍ͧ̾!͐̍͑ͨ ͕͉̯̭͉͕ͤ̿ͬ͗̍́W͈̯̹̩͚̮̍ͮ̒h͗a̫̔̓̑̆͋ͭť̳̜͉̩̓̑ͨ͛̈́́ ̻̦͈̲i̗͕̬̬̱̙̰̓ͭͣ͗̀̓s̫ͩ̽̇ ̩̲̬̠ͬͧͩ̉͆ͨ͗d̋̅̏͒ͩě̤͕̪͗̽ͧ̑̀ͧa̯͓̗̲ͤͫd͈̥͔̹͔ ̦ͥ͑̇͆̌̓̍m̾ͫa̖͇͎̯̟̮̖͋̄̏̈yͥ̾̽̒̑ ͉̝͉͇̫̲͈n̼͉͎͊̽͛e̼̹̝͙̬͊ͧ̐̽̐ͭv̜͈͉̜͊̊̎͊e̝̲̯̯͓͖͋̏ͬ̅̌̿ͯr̜͇̺͈̠̃̚ ̦͓͓̖̯̜ͤ͌d̩̹̞̃͗͑͛̌ͤi͔̺͓̺̣̫̙ͯ̋̆e̻̦̮̤̬̽!̻̮ͮͤ̿̊̐ͭ̽ `);
				}else{
					npc(2, `F̣͓̙̍͋͛͊ͪ̀ỏ͓̮̖͕̣ͣ̓ͬ͌ͨ̚ọ̰̹͈̓ͤl̳̻!̼̼̲ͬͫ͑ͬ͆̀̚ ̳̥̘̞̈́ͤ͊͗̄ͧͅD̲͓̞͓͉͎̋̒̿̋ͤõ̯ͨ̏ͨͩ̿ ̲̋̓̉ͪy̱̠͓̣̌ͫ̿̚o̥͖̖̲̩͑̌̂u͚̣̥̝̰̥̣ ͈͔̜̜ͯͨ̊̃̈r͇ͭͨͭ̒̋ͣͪe̪̠͙͉̊̓͆ͣ̒̚a̗͓̳͉͔͎l̯͍̮ͣͣ͌lͨý̔ ͪ̇ͨt̻̲̒h̦̟ͫ̓́̿̌̇i͒̇ͨ̋̍n̗k̜͕̮̠ͬͧ̈́ͩ̚ ͖̦͇̔͒̂I̻̝͎͎̤̭̅̌̏̊̈̉ͅ ̯̘̻͍̓ͮ̉̾c̤̓͂o̹̜̮ͅul͉ͮ̈́ͭ͑̄̈ḍ̥̤̠̟̑ͯ̊ ̖̜̝͈͇̈́ͪͯb͇͖̼͍̮̮̓ė͕̥ ̟̤ͮk̹̭͈̱̘̈́͌í͖̻͇̞̱͛l̫ͨͤͧlͤͦͭẹ̖̰ͪͣ̚d̹͔̹͍̝͓̏͛͛͆̇?͍̹̠!͎̠̥͓̭ W͑ͦ̽͗ͨ̍h͔͍̯ͭǎ͉͙͎̅̓̅ͦ͊̑ṭ̼͓̂ͨ̇̓̏ ͈̝̻̦͉̮̐̐͊̚ͅĭ̼ͮ̽s̖̮ ̪̫̰̘͖͓͔̋ͦ̾ͧͦd̩͗̐ͦͦ̿ͭê͈͖̗͖̲̦̍ͅä́͑̿̏̋̆d͈͓͉̙̻̥͍ ̗͙̟̜̝̽ͤͅm͔̯͉̝̲̱͂̈́̐̀̍̚̚a̞̥̬y̩ ̬̼͗ͨ̃̒n̘̻͇̗̎̽ͪ͂͑ẻ̯̻̭͓͈ͅv̎̾̇̏ȇͪ͊̓̾̍͐ř͉̟̓ͨ ͓̤̭͍̳d͓̤͇̺̰̦̤̿͋̋͌͐i͓͕̎ė!̝͉͙̃̊ `);
				}
				
				txt(`Its wounds were rapidly healing. ${We()} had to get out of the ship when it was still incapacitated! `);
				loot("legendary");
				survival();
				deplete();
				break;
		}		
	},
	
	
	ghost_hatch(input){
		switch(input){
			case 101: 
				txt(`I spotted a harch in the deckhead. I tried it open but it was almost impossible to lift it. There had to be something heavy on top of it.  `);
				roll(`Open the hatch. `, "str", 2, 110, 111, "eng", -3, `Let ${fol_name()} to open the hatch. `, 110, 111);
				break;
			
			default:
			case 110: 
				txt(`We managed to open the hatch. `);
				link(`Climb up. `, undefined, undefined, ()=> stairs(1) );	
				break;
			
			case 111: 
				txt(`We were not able to open the hatch. `);
				roll(`Open the hatch. `, "str", 2, 110, 111, "eng", -3, `Let ${fol_name()} to open the hatch. `, 110, 111);
				break;
		}
	},
	
	
	
	
	 
	
	
	
	xxx(index, mod){
		switch(index){
			case 101:
				counter.level = mod ?? ra.array([3, 2, 2, 1, 1, 1, 0, 0]);
				txt(`I noticed one ${[`sizable`,`tall`,`very tall`,`gigantic`][counter.level]} tree had the lowest branchest within my reach and overall looked very climbable. Maybe it might be worth climbing up and looking around?`);
				roll(`Climb up. `, "dex", counter.level - 1 , 110, 111, "hp", -3, `Let ${fol_name()} to climb up. `, 210, 211);
				break;
			
			default:
			case 110:
				txt(`From the top of the tree was a wonderful view. `);
				explore(3 + counter.level);		
				map();
				break;
			
			case 111:
				kat(`AHHHHH!`);
				txt(`My hands slipped and after a ${[`very short`,`short`,`long`,`very long`][counter.level]} fall, luckily slowed by thinner branches, I painfully landed back on the ground. `);
				roll(`Climb up. `, "dex", counter.level - 1 , 110, 111, "hp", -3, `Let ${fol_name()} to climb up. `, 210, 211);
				break;
				
			
			
			case 210:
				kat(`WHAT DO YOU SEE!`);
				fol(`A LOT OF THINGS!`);
				txt(`Screamed @fol from somewhere high above me. `);
				explore(3 + counter.level);
				map();
				break;

			case 211:
				fol(`AHHHHH!`);
				txt(`After a ${[`very short`,`short`,`long`,`very long`][counter.level]} fall, @fol landed back on the ground. `);
				kat(`Are you alive!!?`);
				fol(`I think so... `);
				roll(`Climb up. `, "dex", counter.level - 1 , 110, 111, "hp", -3, `Let ${fol_name()} to climb up. `, 210, 211);
				break;
		}
	},
		
	
}