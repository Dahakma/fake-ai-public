import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, fol, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";
import {bold, hint} from "Loop/text";

import {deplete, slay} from "Dungeon/engine";
import {loot, loot_multiple, sluttiness} from "Avatar/index";
import {origin_based} from "Virtual/index";
import {rose_on, rose_off, rose_halt} from "Dungeon/rose";

export const lys = function(input){
	switch(input){
		default:
		case 101:
			mile.goth_lys++;
			rose_off();
			switch(mile.goth_lys){
				default: 
					//EMPTY
					rose_on();
					break;
					
				case 3:
					kat(`So what were you doing before becoming a dancer? `);
					fol(`Various other things. `);
					kat(`Don't be so mysterious! ${mile.slut < 4 ? "I don't mind if you were doing something embarassing or perverted. " : "Come on! Don't be shy! If you were doing something kinky, I want to know all the details! "}`);
					fol(`Before becoming a dancer, I was a pirate. `);
					kat(`A pirate!?!`);
					fol(`Yeah. It was fun. Sailing, plundering, the whole Dewyrain coast feared us! `);
					txt(`I was not sure how to feel about it. I thought pirates were cool until they captured me. `);
					kat(`Were you good at pirating? `);
					fol(`Are you serious!?! I was the best! My cutlass was deadly but not as deadly as my cunningness. Plus I slept with both the captain and the quartermaster so I didn't have to work very hard and always got the best loot! `);
					kat(`Why did you stop with pirating? Did your ship sunk or something like that? `);
					fol(`I wish! `);
					chapter(`Continue. `, `x`, 101, rose_on);
					break;
					
				case 4:
					if(mile.goth_stripper > 0){
						if(mile.slut > 5){
							fol(`I've seen you dancing and you were great. `);
							kat(`Really!? Thank you. I quite enjoyed it. `);
							fol(`You're beautiful and sexy and know how to move. If you were only a bit better, I would be already hiring somebody to break your legs and free me from your competition.`);
							kat(`Ehh... thanks for the warning. `);
							fol(`Nah, I was just joking! A bit more practice and you'll be the best! Your body is a tool and you have to learn to utilise it!`);
							kat(`Hmmm. I think you might be right. You don't have to be ashamed for being sexy! `);
						}else{
							fol(`I've seen you dancing and you were great. `);
							kat(`Do you really think so? It was embarassing. I felt dumb and clumsy. `);
							fol(`You just need more practice! Your body is a tool and you have to learn to utilise it!`);
							kat(`I don't think I share your philosophy. `);
						}
					}else{
						if(mile.slut < 3){
							fol(`You should try to dance on the stage! `);
							kat(`I'm sorry, it isn't for me! Don't get me wrong, there's nothing wrong with it but...I guess I'm too shy to expose myself in front of so many people!`);
							fol(`You can't be shy! Your body is a tool and you have to learn to utilise it!`);
							kat(`I don't share your philosophy. `);
							//TODO links to raise slutyness
						}else{
							fol(`You definitely should try to dance on the stage! `);
							kat(`I'm considering it. It might be an interesting experience. I never did anything like that!`);
							fol(`Good! You can't be shy! Your body is a tool and you have to learn to utilise it!`);
							kat(`Interesting philosophy!`);
						}
					}
					chapter(`Continue. `, `x`, 101, rose_on);
					break;

				case 5:
					kat(`So why did you left your pirate life behind? `);
					fol(`Basically, I flew too close to the sun. I got too cocky and wanted everything for myself, to decide where will we sail and get the biggest share of the booty. I wanted to be in charge!`);
					kat(`To become captain? `);
					fol(`Exactly. When the former captain disgraced himself, I schemed to get on the top. And I became the captain for like two weeks.`);
					kat(`And then? `);
					fol(`I underestimated how jelaous and envious people are. I made a lot of enemies. They claimed that I am whore who got where I was just by spreading her legs. That as a woman I'm incompetent to be in charge. That I was too close to the previous captain and was co-responsible for his failures. That I kept more loot than way my just share and interpreted the Articles very loosely. `);
					kat(`Damn! What a bunch of jerks! Did they throw you out? `);
					fol(`No. The quartermaster was still angry that I stabbed him in his back during the elections even though he for some reason thought I loved him. He convinced the crew my transgressions were too serious and I had to pay for them. So they sold me to slavery. `);
					kat(`That's awful!`);
					fol(`Obviously, I was too good looking to become a carrier or oarsman. After changing hands several times I ended in the best brothel in Gothred. I guess it wasn't so bad but I didn't enjoy being a slave so I escaped. Now I keep my head low, work as a dancer in a sleazy port establishment and look for a new opportunity. `);
					chapter(`Continue. `, `x`, 101, rose_on);
					break;

				case 6:
					mile.goth_whoring = 1;
					fol(`You know there are better ways when you need to quickly make a lot of money.`);
					kat(`What do you mean? `);
					fol(`There are plenty of lonely sailors willing to pay very well for pleasant company. `);
			//TODO - isnt't too high???
					if(mile.slut > 7){
						mile.goth_whoring = 2;
						kat(`Really? How much? And what are the best places? `);
						txt(`I asked. I was just curious, I was not thinking about actually becoming a harlot, of course. Even though I badly needed money, if I wanted to finish this game as soon as possible, I needed the best weapons and gear. `);
					}else if(mile.slut < 3){	
						txt(`I blushed when I realised what she was implying. `);
						kat(`You can't be serious! I'm not a whore! `);
						fol(`Don't get your panties in a twist, it was just a friendly suggestion.`);
						kat(`I would never do anything like that! I don't need money that badly!`);
					}else{
						kat(`I'm not going to fuck sailors for money!!!`);
						fol(`Why not?!`);
						kat(`Because... because... because I don't want to!`);
						fol(`Fair enough. But you don't have to be such a prude. `);
						kat(`I'm not prude... I'm just picky. `);
					}
					chapter(`Continue. `, `x`, 101, rose_on);
					break;
				
				
				case 7:
					kat(`What will you do next? For what opportunity are you looking?`);
					fol(`Well, are sorts of people are visiting House of Earthly Delights. So I hope I'll be able to charm somebody powerful and help him on his way up, reaping all the benefits, of course.`);
					kat(`Another pirate captain? `);
					fol(`Preferably no. Even though I kinda miss the pirate life. And I learned my lesson.`);
					kat(`Less greed and backstabbing? `);
					fol(`Yeah! Or maybe just doing it less conspicuously.`);
					chapter(`Continue. `, `x`, 101, rose_on);
					break;
					
				case 8:
					fol(`Does the danger make you so horny too?`);
					if(mile.slut < 5){
						kat(`...wait, what? What are you talking about!?`);
						fol(`That the threat and violence makes my pussy wet! `);
						if(mile.slut < 1){
							kat(`O.. oh!`);
							txt(`I was stunned, not sure how to respond. `);
						}else{
							kat(`Good for you? `);
						}
						txt(`@lys loudly laughed.`);
					}else{
						kat(`Damn yeah, it does!`);
					}
					txt(`Are you by chance one of those women who enjoy female company?  Don't get me wrong, I prefer hard cocks but I know that on the quest you can't be picky. `);
					
					if(mile.slut < 5){
						if(mile.lesbian <= 1){
							kat(`I'm not very much into girls...`);
						}else{
							kat(`I... I'm not sure anymore...`);
						}
					}else{
						if(mile.lesbian <= 1){
							kat(`I prefer cocks too.`);
						}else{
							kat(`I'm not picky. `);
						}
					}
					
					fol(`What do you think? Should we take a break to release the pressure? `);
					
					link(`Absolutely! `, 802);
					link(`Fine. `, 801);
					link(`It might be fun. `, 801);
					link(`Maybe later.`, 800);
					link(`No!`, 800);
					break;
			}
			
			deplete();
			break;
		
		case 800:
				rose_on();
				fol(`Fine! Let's focus on our adventure - for now!`);
				break;
				
				
		case 801:
				fol(`What is that supposed to mean?!`);
				kat(`That I'm okay with having sex with you. ${mile.immersion <= 0 ? "I usually don't have sex with women I don't know well but you're an NPC so it's okay. " : ""} `);
				txt(`The redhead frowned and lifted an admonitory finger.`);
				fol(`No! To have sex with me you have to want it. `);
				kat(`But I want it!`);
				fol(`And not just want it, you have to crave it. Need it. More than anything. Be crazy with lust. `);
				kat(`But...`);
				txt(`She pressed her lips against mine. Then she let me go and smirked:  `);
				fol(`For now, let's continue with our adventure!`);
				kat(`But...`);
				txt(`I gasped. I definitely craved it now. But @lys was already marching forward. `);
				chapter(`Continue. `, `x`, 101, rose_on);
				break;
				
		case 802: 
				//todo sex
				chapter(`Continue. `, `x`, 101, rose_on);
				break;
				
	}
}





