import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, fol} from "Loop/text";
import {bold, hint} from "Loop/text";

import {deplete, slay} from "Dungeon/engine";
import {loot, loot_multiple, sluttiness} from "Avatar/index";
import {POTIONS} from "Avatar/game/potions";
import {origin_based, class_based} from "Virtual/index";
import {rose_on, rose_off, rose_halt} from "Dungeon/rose";

export const gal = function(input){
	switch(input){
		case 600:
				fol(`Thank you for your advice! `);
				break;
		
		default:
		case 101:
			rose_halt();
			mile.goth_gal++;
			switch(mile.goth_gal){
				default: 
					//EMPTY
					break;
					
				case 2:
					kat(`So, do I get that right you're somebody important? `);
					fol(`Why do you think so? `);
					kat(`The soldiers on the galley were very respectful. And you seem to be rich. `);
					fol(`Well, that's relative. I'm more definitely important than a random dirty plebian. I quess I'm somehow imporant in Beletriad. I'm not very much important in Gothred even though I still have a few friends here. And I'm not rich, I just have a meager allowance to cover for my basic expenses. `);
					kat(`I would want such meager allowance! And why you are you in Gothred and not Beletriad?`);
					fol(`Why is anybody in Gothred? I'm kinda not welcomed in Gothred anymore. I have too many enemies there.`);
					break;
					
			
				case 3:

					if( PC.getDim("breastSize") > 36 ){
						fol(`I just want to say I love your tits! And how big they are! They aren't natural, are they? `);
						kat(`Not entirely. I drank a few elixirs. `);
						fol(`No reason to be ashamed for that! I think the result is impressive! `);
					}else if(PC.getDim("breastSize") < 12 ){
						//const id = POTION.id("x");
						fol(`Sorry, for bothering you but I'm unable to stop thinking about your tits, respective, about the lack of them. You should try ${POTIONS.name("xSuc")}. You know, the ${POTIONS.color("xSuc")} elixir. I think it would really help. `);
						kat(`I did not ask for your dumb opinion!`);
					}else{
						txt(`We were advancing further. `);
					}
					break;
				
				case 4: 
						fol(`Do you have any siblings? `);
						if(mile.immersion > 2){
							if(ext.class === 2 && ext.origin === 2){
								kat(`Yeah, I used to have a younger s${String.fromCharCode(105)}ster but she got killed because of me!`);
							}else{
								if(ext.class === 2){
									kat(`Plenty, I guess northern families are pretty big. `);
								}else{
									kat(`I don't think I do. `);
								}
							}
						}else if(mile.immersion >= 0){
							kat(`I have one older brother who is at uni. Wait, you mean here? ${ext.class === 2 && ext.origin === 2 ?  "I think I had a younger sister but now she's dead. " : "I'm not sure. "}`);
						}else{
							kat(`I have one older brother who is at uni. He's fine. `);
						}
						fol(`I have - well had two brothers. One was murdered. `);
						kat(`I'm sorry to hear that. `);
						fol(`Well, we weren't very close. `);
						kat(`What about your other brother? `);
						fol(`I believe it was him who ordered his assassination. And mine. `);
						kat(`Oops. But you seem to be doing fine. `);
						fol(`Well, it isn't so easy to kill me!`);
						kat(`Is that why did you leave Beletriad? `);
						fol(`Among other things.`);
						break;
						
				case 5:
						fol(`So you're the chosen one destined to defeat the Evil Overlord? `);
						if(mile.immersion < 0){
							kat(`Yeah, that's the point of this game. `);
						}else{
							kat(`Yeah, I believe so. `);
						}
						fol(`How does it feel? Knowing your life has a single and very important purpose? `);
						kat(`It feels great!`);
						txt(`It did. The world of the game was so simple and uncomplicated, I was the hero, doing heroic things and heading towards one particular goal.`);
						fol(`Do you ever have doubts? Wouldn't be better ${class_based("continue being whore with no responsibilities? ", "staying in the Northern wasteland? ", "continue serving to the Church and gods? ")}`);
						if(mile.immersion < 0){
							kat(`No doubts! I'm the main character of this story. `);
							fol(`That's an interesting point of view. Everybody is the main character of his story. And his story should be worth listening!`);
							kat(`I didn't meant it that way!`);
						}else{
							kat(`It was impossible to stay after what I did.`);
							fol(`Hmmmm.... I do understand. The course of our lives was forever changed. Sometimes there's no way back. `);
							kat(`Yeah! We can only go forward!`);
						}
						
						break;
						
				case 6:
						fol(`I'm not sure whether I should return to the Beletriad. I don't want to get stuck in Gothred for the rest of my life. But returning to Beletriad may make the rest of my life extremely short. `);
						kat(`Why do they want you dead? `);
						fol(`I believe my uncle would want me to run the family business. But he died and you know, our family is big and some relatives are too arrogant and greedy and want everything for themselves. `);
						kat(`Even willing to kill you? `);
						fol(`Yeah. My family is a mess. `);
						
						link(`I know nothing about your family and the particular situation but I am absolutely sure you're the best person to run your uncle's business!`, 600); 
						link(`Well, you have to decide that for yourself. I'm not sure whether is your uncle's business worthy of risking your life.`, 600);
						break;
			
			}
			deplete();
			break;
			
	}
}





