import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, fol, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";

import {slay, deplete, objective, stairs, rgi} from "Dungeon/engine";
import {roll, boost} from "Virtual/index";
import {rg} from "Libraries/random";

import {dibz, loot, loot_multiple, draw} from "Avatar/index";
import {We, we, he, fol_name} from "Virtual/text";
import {rose_on, rose_off} from "Dungeon/rose";

export const ship = {
	armory(){
		txt(`This was definitely an armory, various weapons or pieces of armors were stacked in racks or hanged on bulkheads. `);
		loot_multiple(["armor"], ["weapon"]);
		deplete();
	},
	
	storage(){
		txt(`This cabin had to serve as a some kind of storage. `);
		loot(2,"clothes","elixir");
		deplete();
	},
	
	ship_treasury(){
		txt(`This cabin had to serve as a some kind of storage. `); //TODO
		loot_multiple(["money"], [2,"armor","elixir"] ); //TODO 
		deplete();
	},
	
	
	captain(){
		txt(`I quessed this expecially luxurious and spatious cabin belonged the the captain of the ship. `);
		loot_multiple(["money"], [2, "clothes", "elixir", "armor", "weapon"], ["legendary"] ); //TODO
		//loot(["4,"clothes","money","elixir");
		deplete();
	},
	
	sickbay(){
		txt(`Based on the medical equipment I assumed this cabin was a sick bay where they took care of wounded. Or performed twisted experiments on captives. `);
		loot_multiple([1,"xHP"], [1, "xEng"], ["elixir"]); 
		//loot(,"elixir");
		deplete();
	},
	
	cargo(input, mod){
		function search(){
			roll(`Search the cargo. `, "int", counter.level, 110, 111, "eng", -10, `Let @fol to search the cargo. `, 210, 211);
			if(hardbug) link("## Find. ", 110);	
			boost("int", "eng");
		}
		
		switch(input){
			case 101:
				rgi();
				txt(`The ship hold was filled with countless casks, crates and jars. `);
				counter.level = mod ?? rg.array([3, 3, 2, 2, 2, 1, 1, 1, 0, 0]);
				search()
				break;
				
			default:
			case 110:
				txt(`I recognised the runes on several crates containing useful things. `);
				loot(2, "elixir", "clothes", "elixir", "clothes", "weapon", "armor"); //TODO
				deplete();
				break;	
			
			case 111:
				txt(`I randomly opened a few crates but was not able to find anything useful. `);
				search()
				break;
				
			case 210:
				txt(`@fol recognized symbols on several crates whichc might be useful. `);
				loot(2, "elixir", "clothes", "elixir", "clothes", "weapon", "armor"); //TODO
				dibz(ext.follower);
				deplete();
				break;
				
			case 211:
				txt(`@fol randomly opened a few crates but there was nothing useful inside. `);
				search();
				break;
		}	
	},
	
	
	seaweed(input, mod){
		function free(){
			roll(`Free yourself. `, "dex", counter.level, 110, 111, "eng", -5, `Signal @fol to help you. `, 210, 211);
			boost("dex", "eng");
			if(hardbug) link("##Free yourself. ", 110);	
		}
		
		switch(input){
			case 101:
				rose_off();
				txt(`Something grabbed my leg! I began to panic but then relised I just got tangled in seaweed. `);
				counter.level = mod ?? 3; // ra.array([3, 2, 2, 1, 1, 1]);
				free();
				break;
				
			default:
			case 110:
				rose_on();
				txt(`I untagled the seaweed and freed my leg. `);
				deplete();
				break;	
			
			case 111:
				txt(`I tried to escape the seaweed, unsuccessfully. `);
				free();
				break;
				
			case 210:
				rose_on();
				txt(`@fol untangled me and we could continue with the exploration. `);
				deplete();
				break;
				
			case 211:
				txt(`@fol desperately tried to help but I almost got entangled even worse than before. `);
				free();
				break;
		}	
		
		
	},
	
	
	
}