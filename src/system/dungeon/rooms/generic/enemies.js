import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";

import {rose_on, rose_off, rose_paused} from "Dungeon/rose"; 
import {slay, deplete, end, objective, stairs} from "Dungeon/engine";
import {ATTRIBUTES} from "Virtual/index";
import {ra} from "Libraries/random";
import {combat} from "Gui/combat/combat/combat";


export function fight(enemies, name, what){
	combat(enemies)
	txt(`@We defeated ${name}. `);

	slay(what);
	rose_on(); //probably redundant; in the case you bug out of the combat
}


export const enemies = {
	draugr(){
		fight({
			draugr: 1,
		},"draugrs");
	},
	
	draugrs(){
		fight({
			draugr: 2,
		},"draugrs");
	},
	
	smugglers(){
		//TODO
		fight({
			pirate: [1,3],
			strong_pirate: [0,2],
			fast_pirate: [0,2],
		},"pirates");
		//fight("rat");
	},
	
	spiders(){
		fight({
			spider: [2,4],
		},"spiders");
	},
	
	//WRAITHS
	wraiths(){
		fight({
			wraith: [2,4],
		},"wraiths");
	},
	
	//PIRATES
	pirates(){
		fight({
			pirate: [1,2],
			strong_pirate: 1,
			fast_pirate: 1,
		},"pirates");
	},
	/*
	pirates(){
		fight({
			pirate: [1,3],
			strong_pirate: [0,2],
			fast_pirate: [0,2],
		},"pirates");
	},
	
	pirates_small(){
		fight({
			pirate: [1,2],
			strong_pirate: 1,
			fast_pirate: 1,
		},"pirates");
	},
	
	pirates_big(){
		fight({
			pirate: [2,4],
			strong_pirate: [1,2],
			fast_pirate: [1,2],
		},"pirates");
	},
	*/
	
	//GOBLINS
	goblins(){
		fight({
			goblin: [2,4],
		}, "a warband of goblins");
	},
	
	ogre(){
		fight("ogre", "an ogre");
	},
	
	//RATS
	rat(){
		fight("rat", "a rat");
	},
	two_rats(){
		fight({
			rat: 2,
		}, "two rats");
	},
	rats(){
		fight({
			rat: [2,4],
		}, "a mischief of rats");
	},
	rous(){
		fight("rous", "rat of unusual size");
	},
	
}