import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";
import {bold, hint} from "Loop/text";

import {ra} from "Libraries/random";
import {roll} from "Virtual/index";
import {deplete} from "Dungeon/engine";

import {We, we, he, fol_name} from "Virtual/text";
import {dibz, loot, loot_multiple} from "Avatar/index";

import {tiles} from "Minigames/tiles";
//import {rose_on, rose_off} from "Dungeon/rose";
import {DISPLAY} from "Gui/gui";


export const minigames = {
 
	//TODO
	tiles(index, mod){
		switch(index){
			case 101: 
				txt(`The way forward was blocked by intricate puzzle. `);
				link("Solve the puzzle.", 102);
				break;
				
			case 102: 
				DISPLAY.rose_off();
				tiles( ()=> main(103), "div_text");
				link("Back.", 101, DISPLAY.rose_on);
				break;
			
			default:
			case 103: 
				txt(`I solved the puzzle! `);
				deplete();
				DISPLAY.rose_on();
				break;
		}
	},
}
