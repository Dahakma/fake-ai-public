import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, fol, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";

import {slay, deplete, objective, stairs, map, explore, rgi} from "Dungeon/engine";
import {rose_halt} from "Dungeon/rose";
import {roll} from "Virtual/index";
import {boost} from "Virtual/boost";
import {rg} from "Libraries/random";
//import {} from "Dungeon/map";
import {dibz, loot, loot_multiple} from "Avatar/index";

export const wood = {
	stash(){
		rose_halt();
		txt(`In a tree hollow I discovered a secret stash. `);
		loot("clothes", "money","elixir","elixir");
		deplete();
	},
	
	tree(index, mod){
		function climb(){
			roll(`Climb up. `, "dex", counter.level , 110, 111, "hp", -10, `Let ${ext.follower?.name} to climb up. `, 210, 211);
			boost("dex", "hp");
		}
		
		switch(index){
			case 101:
				rgi();
				counter.level = mod ?? rg.array([3, 2, 2, 1, 1, 1, 0, 0]);
				txt(`I noticed one ${[`sizable`,`tall`,`very tall`,`gigantic`][counter.level]} tree had the lowest branchest within my reach and overall looked very climbable. Maybe it might be worth climbing up and looking around?`);
				climb();
				break;
			
			//PC success
			default:
			case 110:
				txt(`From the top of the tree was a wonderful view. `);
				explore(3 + counter.level);		
				map();
				break;
			
			//PC fail
			case 111:
				kat(`AHHHHH!`);
				txt(`My hands slipped and after a ${[`very short`,`short`,`long`,`very long`][counter.level]} fall, luckily slowed by thinner branches, I painfully landed back on the ground. `);
				climb();
				break;
				
			//follower success
			case 210:
				kat(`WHAT DO YOU SEE!`);
				fol(`A LOT OF THINGS!`);
				txt(`Screamed ${name[ext.follower.key]} from somewhere high above me. `);
				explore(3 + counter.level);
				map();
				break;
			
			//follower fail
			case 211:
				fol(`AHHHHH!`);
				txt(`After a ${[`very short`,`short`,`long`,`very long`][counter.level]} fall, ${name[ext.follower.key]} landed back on the ground. `);
				kat(`Are you alive!!?`);
				fol(`I think so... `);
				climb();
				break;
		}
	},

	
}