/*
	generic hallway based on dungeon theme
*/

import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";
import {ra} from "Libraries/random";







export const x = function(){
	if(ext.dungeon.theme === "cellar"){
		if(ext.follower){
			txt(ra.array([
				`We were walking through a damp cellar.  `,
				`We were carefully advancing forward, danger could be lurking behind every corner. `,
			]));
		}else{
			txt(ra.array([
				`I was walking through a damp cellar.  `,
				`I was carefully advancing forward, danger could be lurking behind every corner. `,
			]));
		}
		
	}else if(ext.dungeon.theme === "wood"){
		if(ext.follower){
			txt(ra.array([
				`We were walking through a forest.`,
				`We pushed through thorny bushes. `,
				`We walked among tall and majestic fantasy trees. `,
			]));
		}else{
			txt(ra.array([
				`I was walking through a forest. `,
				`I pushed through thorny bushes.`,
				`I walked among tall and majestic fantasy trees. `,
			]));
		}
		
	}else if(ext.dungeon.theme === "ship"){
		if(ext.follower){
			txt(ra.array([
				`We explored another cabin.`,
				`The ship made me feel claustrophobic, the corridors were narrow and the deckhead was low. `,
				`The unnerving creaking of the wooden ship made me feel very tense.  `,
			]));
		}else{
			txt(ra.array([
				`I explored another cabin. `,
				`The ship made me feel claustrophobic, the corridors were narrow and the deckhead was low. `,
				`The unnerving creaking of the wooden ship made me feel very tense.  `,
			]));
		}
		
	}else if(ext.dungeon.theme === "cave"){
		if(ext.follower){
			txt(ra.array([
				`We were carefully exploring the dark cavern. `,
				`Without fear we were continuing further. `,
				`I walked first, ${ext.follower.name} was covering the back. `,
			]));
		}else{
			txt(ra.array([
				`I was walking through a cavern. `,
				`I was carefully advancing forward, danger could be lurking behind every corner. `,
			]));
		}
	
	}else if(ext.dungeon.theme === "ruins"){ //TODO
		if(ext.follower){
			txt(ra.array([
				`We were carefully exploring the ruins. `,
				`Without fear we were continuing further. `,
				`I walked first, ${ext.follower.name} was covering the back. `,
			]));
		}else{
			txt(ra.array([
				`I was walking through the ruins. `,
				`I was carefully advancing forward, danger could be lurking behind every corner. `,
			]));
		}
	}
	
	
}