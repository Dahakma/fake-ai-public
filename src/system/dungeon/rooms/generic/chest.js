import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";
import {bold, hint} from "Loop/text";

import {ra, rg} from "Libraries/random";
import {roll, boost} from "Virtual/index";
import {deplete, rgi} from "Dungeon/engine";
import {dibz, loot, loot_multiple} from "Avatar/index";

export const chest = function(input, mod){
	function size(){
		switch(counter.size){
			default:
			case 0: 
				return  rg.array(["small","tiny"]);
			case 1:
				return "";
			case 2:
				return rg.array(["large","big"]);
			case 3: 
				return rg.array(["huge"]);
		}
	}
			
	function lock(){
		return [
			"a <b>primitive</b> ", //0
			"a <b>simple</b> ",  //1
			"a <b>common</b> ",  //2
			"an <b>ordinary</b>",  //3
			"a <b>fine</b> ", //4
			"an <b>unusual</b> ",  //5
			"a <b>complicated</b> ", //6
			"a <b>sophisticated</b> ", //7
			"a <b>dwarven</b> ", //8
		][counter.lock];
	}
			
			
	function desc(){
		//TODO - MORE
		
		const a = [`In one of the alcoves I discovered a ${size()} chest locked with ${lock()} lock. `];
		
		if(ext.dungeon.theme === "cellar"){
			if(counter.size === 0) a.push(`On a shelf was placed right in front of my eyes was placed a jewellery box secured with  ${lock()} lock. `);
		}
		
		if(ext.dungeon.theme === "underwater"){
			a.push(`As I was swimming my hand accidentally hit and discovered a ${size()} chest locked with ${lock()} lock. `);
		}
		
		if(ext.dungeon.theme === "ruins"){
			if(counter.size === 0) a.push(`Among the other derbris way lying a  jewellery box, still locked with ${lock()} lock. `);
			a.push(`I noticed a ${size()} half-burried by rubble. I uncovered it and examined the ${lock()} lock. `);
		}
		
		if(ext.dungeon.theme === "ship" || ext.dungeon.theme === "shipwreck"){
			if(counter.size === 2) a.push(`Next to the bulkhead was a large seaman's chest locked with ${lock()} lock. `);
			if(counter.size === 0) a.push(`On a shelf attached to the bulkhead I discovered a jewellery box secured with  ${lock()} lock. `);
		}
		
		return txt( rg.array(a) );
	}
			
			
	function lockpick(){
		roll(`Lockpick it. `, "lockpick", counter.level - 1, 110, 102, "eng", -5, `Let ${ext.follower?.name} to lockpick it. `, 210, 202);
		boost("lockpick", "eng");
		if(hardbug)link("## Unlock. ", 110);
		
	}
	
	function treasure(player){
		switch(counter.size){
			default:
			case 2: //big
				switch(counter.level){
					default:
					case 0: //junk
						switch( rg.integer(0,1,2) ){
							default:
							case 0: 
								txt(`Inside the chest were stored clothes. `);
								return loot(2, "clothes");
							case 1:
								txt(`The chest was full of mostly useless junk. `);
								return loot(1, "clothes", "elixir", "money");
							case 2: 
								txt(`The huge chest was completely empty! `);
								return;
						}
								
					case 1: //ordinary
						switch( rg.integer(0,2) ){
							default:
							case 0: 
								txt(`The chest contained various personal belongings. `);
								return loot_multiple(["elixir", "armor", "money"], ["clothes"]);
							case 1:
								txt(`The chest was full of junk but there were several interesting things. `);
								return loot(2, "clothes", "elixir", "armor", "money");
							case 2:
								txt(`The large chest contained a set of completely random items.  `);
								return loot(3, "elixir", "elixir", "money", "clothes", "clothes", "weapon", "armor");
						}
						
					case 2: //legendary 
						switch( rg.integer(0,1,2) ){
							default:
							case 0: 
								txt(`The chest contained military equipment. `);
								return loot(2, "armor", "armor", "weapon");
							case 1:
								txt(`The chest contained personal belonging of a wealthy individual. `);
								return loot_multiple([2, "elixir", "armor", "clothes", "clothes", "weapon"], ["money"]);
							case 2:
								txt(`I opened the large chest and could not believe my luck! `);
								return loot_multiple(["legendary"], [1, "elixir", "armor", "weapon"])
						}
				}
				
			case 1: //medium
				switch(counter.level){
					default:
					case 0: //junk
						switch( rg.integer(0,3) ){
							default:
							case 0: 
								txt(`The medium chest was empty! `);
								return;
							case 1:
								txt(`The medim chest contained a few pieces of clothes. `);
								return loot("clothes");
							case 2:
								txt(`The chest was full of garbage but I was eventually able to dug out several coins. `);
								return loot("money");
							case 3:
								txt(`The chest was filled with mostly useless junk. `);
								return loot(1, "clothes", "elixir");
						}
								
					case 1: //ordinary
						switch( rg.integer(0,2) ){
							default:
							case 0: 
								txt(`The chest contained a piece of armor. `);
								return loot("armor");
							case 1:
								txt(`The chest contained a weapon.  `);
								return loot("weapon");
							case 2:
								txt(`The medium chest contained a set of completely random items.  `);
								return loot(2, "elixir", "elixir", "money", "clothes", "clothes", "weapon", "armor");
						}
						
					case 2: //legendary 
						switch( rg.integer(0,2) ){
							default:
							case 0: 
								txt(`The chest contained combat supplies. `);
								return loot_multiple([2, "elixir"], ["weapon", "armor"]);
							case 1:
								txt(`The chest contained somebody's personal items. `);
								return loot_multiple(["elixir", "weapon"], ["clothes"], ["money"]);
							case 2:
								txt(`I smiled when I opened the little chest and saw the content. `);
								return loot("legendary");
						}
				}
				
			case 0: //small
				switch(counter.level){
					default:
					case 0: //junk
						switch( rg.integer(0,2) ){
							default:
							case 0: 
								txt(`The box was empty. Damn! `);
								return;
							case 1:
								txt(`The little box contained only several coins. `);
								return loot("money");
							case 2:
								txt(`The little box was full of broken vials, only one potion survived. `);
								return loot("elixir");
						}
								
					case 1: //ordinary
						switch( rg.integer(0,1) ){
							default:
							case 0: 
								txt(`The chest contained medical supplies including serveral potions. `);
								return loot(2, "elixir");
							case 1:
								txt(`The oblong chest was a weapon case.  `);
								return loot(1, "weapon");
						}
						
					case 2: //legendary 
						switch( rg.integer(0,2) ){
							default:
							case 0: 
								txt(`The chest contained nice collections of several potions. `);
								return loot(3, "elixir");
							case 1:
								txt(`The chest contained somebody's personal treasures. `);
								return loot_multiple([2, "elixir"], ["money"]);
							case 2:
								txt(`I gasped when I opened the little chest and saw the content. `);
								return loot("legendary");
						}
				}
		}
	}
	
	switch(input){
		case 101:
			rgi();
			counter.level = rg.array([0, 0, 1, 1, 2]);
			counter.size = rg.array([0, 1, 2]);
			counter.lock = (()=>{
				switch(head.act){
					default:
					case 1: //feyd
						switch(counter.level){
							default:
							case 0: //junk
								return  mod ?? rg.array([0, 1, 2]);
							case 1: //ordinary
								return  mod ?? rg.array([1, 2, 3]);
							case 2: //legendary
								return  mod ?? rg.array([4, 5, 6]);
						}
						
					case 2: //goth
						switch(counter.level){
							default:
							case 0: //junk
								return  mod ?? rg.array([0, 1, 2, 3]);
							case 1: //ordinary
								return  mod ?? rg.array([2, 3, 4]);
							case 2: //legendary
								return  mod ?? rg.array([5, 6, 7]);
						}
						
					case 3: //west
						switch(counter.level){
							default:
							case 0: //junk
								return  mod ?? rg.array([0, 1, 2, 3]);
							case 1: //ordinary
								return  mod ?? rg.array([2, 3, 4, 5]);
							case 2: //legendary
								return  mod ?? rg.array([6, 7, 8]);
						}
				}
			})(); 
			
			
			//txt("In one of the alcoves I discovered a sturdy locked chest. It could contain nothing or some wealth beyond measure. Or another stupid potion. Also it could be booby-trapped. ");
			desc();
			lockpick()
			break;
		
		//PC fail
		default:	
		case 102:
			txt("I was not able to open the chest.");
			lockpick();
			break;
			
		//PC success
		case 110:
			txt("I carefully opened the chest. ");
			treasure(true);
			deplete();
			break;
			
		//follower  failure	
		case 202:
			txt(`@fol was not able to open the chest. `);
			lockpick();
			break;
			
		//follower success
		case 210:
			txt(`@fol carefully opened the chest. `);
			treasure(false);
			dibz(ext.follower);
			deplete();
			break;
			
			
	}
	

	
}