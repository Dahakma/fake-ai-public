import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";


import {ra} from "Libraries/random";
import {roll, origin_based} from "Virtual/index";

import {loot, loot_multiple, sluttiness} from "Avatar/index";
import {rose_halt} from "Dungeon/rose";

import {slay, deplete, survival} from "Dungeon/engine";
import {quest} from "Virtual/quest"; 

export const other = {	
	
	adventurer(){
		txt(`@We discovered remains of an unfortunate adventurer. `);
		loot_multiple([1,"weapon"], [1,"armor"], [1,"clothes"], [1,"elixir"]);
		deplete();
		rose_halt();
	},
	
	
}