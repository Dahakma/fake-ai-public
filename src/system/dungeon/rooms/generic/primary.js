import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";

import {slay, deplete, exit, objective, stairs} from "Dungeon/engine";

import {ra} from "Libraries/random";

import {BARS} from "Virtual/index"; 
import {We, we, he, fol_name} from "Virtual/text";

export const primary = {
	initiate(){
		
		//DUNGEON THEME
		let text = ext.follower ? `Together with @fol we ` : `I `;
		text += (()=>{ switch(ext.dungeon.theme){
			case "cave": 
				return `descended undeground and entered the cavern. `;
			case "catacombs":
				return `entered the catacombs. `;
			case "cellar":
				return `descended into the cellar. `;
			case "ruins":
				return `entered the ruins. `;
			case "ship":
				return `climbed up and boarded the ship. `;
			case "wood":
				return `reached the first trees and entered the forest. `;
			case "underwater":
				return `dived deep under the water. `;
			case "flooded":
				return `took a deep breath and sunk under the water. `;
			case "shipwreck":
				return `dived and swam towards the wreck. `;
			default: 
				return `entered the dungeon. `;
		}})();
		
		//OVERRIDEN BY ext.dungeon.instructions
		if(ext.dungeon.introduction) text = ext.dungeon.introduction;
		
		//QUEST
		if(ext.dungeon.quest){
			text += ext.follower ? `Our ` : `My `;
			text += `quest was to <i>${ext.quests[ext.dungeon.quest].title}</i>. `;
		}
		
		//QUIP
		if(mile.immersion > 1){
			if(ext.class === 1){
				//TODO
			}else if(ext.class === 2){
				text += ra.array(["For glory! ", "For fame and honor! ", "I hoped the enemies were already trembling! "]); 
			}else if(ext.class === 3){
				//TODO
			}
		}else if(mile.immersion < -1){
			//TODO
		}
			
		txt(text);
		link(`Leave the dungeon. `, null, undefined, exit);	
	},
	
	entrance(){
		if(ext.dungeon.oxygen !== undefined){
			txt(`@We surfaced and took a very deep breath. `);
			//BARS.refill(`oxygen`);
			//BARS.change(`oxygen}`, ext.dungeon.oxygen_max);
			ext.dungeon.oxygen = ext.dungeon.oxygen_max;
		}else{
			txt(`@We returned back to the dungeon entrance. `);
		}
		link(`Leave the dungeon. `, null, undefined, exit);	
	},
	
	corpses(){
		txt(`On the ground were lying corpses of an enemy I slew. `)
	},
	
	corpse(){
		txt(`On the ground was a corpse of an enemy I slew. `)
	},
	
	upstairs(){
		if(ext.dungeon.theme === "flooded" || ext.dungeon.theme === "shipwreck"  || ext.dungeon.theme === "underwater"){
			link(`Swim up. `, null, undefined, ()=> stairs(1) );	
		}else if(ext.dungeon.theme === "ship"){
			link(`Ladder up. `, null, undefined, ()=> stairs(1) );	
		}else{
			link(`Go upstairs. `, null, undefined, ()=> stairs(1) );	
		}
	},
	
	downstairs(){	
		if(ext.dungeon.theme === "flooded" || ext.dungeon.theme === "shipwreck"  || ext.dungeon.theme === "underwater"){
			link(`Swim down. `, null, undefined, ()=> stairs(-1) );	
		}else if(ext.dungeon.theme === "ship"){
			link(`Ladder down. `, null, undefined, ()=> stairs(-1) );	
		}else{
			link(`Go downstairs. `, null, undefined, ()=> stairs(-1) );	
		}
	},
	
	/*
	downstairs(){
		link("Go downstairs. ", 101, null, ()=> stairs(-1) );	
	},
	*/
	
	
}