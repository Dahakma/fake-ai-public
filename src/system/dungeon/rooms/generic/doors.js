import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, yel, gal} from "Loop/text";
import {bold, hint} from "Loop/text";

import {rg} from "Libraries/random";
import {roll, boost} from "Virtual/index";
import {deplete, rgi} from "Dungeon/engine";


//import {We, we, he, fol_name} from "Virtual/text";
import {dibz, loot, loot_multiple} from "Avatar/index";


function try_door(){
	roll(`Lockpick it. `, "lockpick", counter.lock, 110, 111, "eng", -5, `Let @fol to lockpick it. `, 210, 211);
	roll(`Break through. `, "str", counter.door, 310, 311, "eng", -10, `Let @fol to break through. `, 410, 411);
	boost("lockpick", "str", "eng");
	if(hardbug) link(`## Open door`, 110);
}

function try_portal(){
	roll(`Decipher  the symbols `, "int", counter.door, 110, 111, "eng", -5, `Let @fol to decipher the symbols. `, 210, 211);
	roll(`Attempt to bypass the mechanism. `, "dex", counter.door, 310, 311, "eng", -10, `Let @fol to try_door to bypass the mechanism. `, 410, 411);
	boost("int", "dex", "eng");
	if(hardbug) link(`## Open door`, 110);
}



function actual_doors(index){
	/*
		LEGACY
		-3 primitive
		-2 simple
		-1 common
		0
		+1 fine
		+2 unusual
		+3 complicated
		+4 sophisticated
		+5 dwarven
		
		-3 rotten
		-2 battered
		-1 old 
		0
		1 wooden
		2 sturdy
		3 reinforced
		4 metal
	*/	
	
	
	switch(index){
		case 101: 
			{				
				const lock = [
					"a <b>primitive</b> ", //0
					"a <b>simple</b> ",  //1
					"a <b>common</b> ",  //2
					"a ",  //3
					"a <b>fine</b> ", //4
					"an <b>unusual</b> ",  //5
					"a <b>complicated</b> ", //6
					"a <b>sophisticated</b> ", //7
					"a <b>dwarven</b> ", //8
				][counter.lock];
				
				const door = [
					"a <b>rotten</b> ", 
					"a <b>battered</b> ",
					"an <b>old</b> ", 
					"a ", 
					"a <b>wooden</b> ", 
					"a <b>sturdy</b> ", 
					"a <b>reinforced</b> ", 
					"a <b>metal</b> ",
				][counter.door];
			
				txt(`The way forward was blocked by ${door}door secured by ${lock}lock. `);
			}
			try_door();
			break;
		
		default:
		case 110: 
			txt(`I manage to lockpick and open the door. `);
			deplete();
			break;
			
		case 111: 
			txt(`I failed to open the door. `);
			try_door();
			break;
			
		case 210: 
			txt(`@fol lockpicked the door. `);
			deplete();
			break;
			
		case 211: 
			txt(`@fol failed to lockpick the door. `);
			try_door();
			break;
			
			
		case 310: 
			txt(`Several hits with my weapon managed to chip the wood and I got through. `);
			deplete();
			break;
			
		case 311: 
			txt(`The door was sturdier than it look like and survived all my brutal attacks. `);
			try_door();
			break;
			
		case 410: 
			txt(`@fol successfully used brute force to destroy the door. `);
			deplete();
			break;
			
		case 411: 
			txt(`@fol tried @his best but the door still resisted. `);
			try_door();
			break;
	}	
}
	
	
function actual_portals(index){
	switch(index){
		case 101: 
			{
				const door = [
					"commonplace", //0
					"familiar ",
					"simple",
					"strange", //3
					"unknown",
					"complicated",
					"mysterious",
					"incomprehensible", //7
				][counter.door];
				txt(`The passage forward was blocked by a massive stone gate covered by <b>${door}</b> symbols. `);
			}
			try_portal();
			break;
		
		default:
		case 110: 
			txt(`I manage solve the puzzle and open the gate. `);
			deplete();
			break;
			
		case 111: 
			txt(`My attempt to solve the riddle failed miserably. `);
			try_portal();
			break;
			
		case 210: 
			txt(`After a long deliberation @fol pressed several symbols and openned the gate.. `);
			deplete();
			break;
			
		case 211: 
			txt(`@fol stared on the door for a while and then @he just helplessly shrugged. `);
			try_portal();
			break;
			
			
		case 310: 
			txt(`I manage to access the inner mechanism with my lockpick and activate it manually.`);
			deplete();
			break;
			
		case 311: 
			txt(`I was not able to reach the mechanism.  `);
			try_portal();
			break;
			
		case 410: 
			txt(`@fol managed to bypass the puzzle and activate the mechanism manually. `);
			deplete();
			break;
			
		case 411: 
			txt(`@fol was not able reach the mechanism. `);
			try_portal();
			break;
	}
}
	

export const doors = {
	/*
		0 rotten
		1 battered
		2 old 
		3
		4 wooden
		5 sturdy
		6 reinforced
		7 metal
		
		
		0 primitive
		1 simple
		2 common
		3
		4 fine
		5 unusual
		6 complicated
		7 sophisticated
		8 dwarven
	*/	
	
	
	low_door(index, mod){
		if(index === 101){
			rgi();
			switch(head.act){
				default:
				case 1: //feyd
					counter.lock = mod ?? rg.array([0, 0, 1, 2]);
					counter.door = mod ?? rg.array([0, 1, 2]);
					break;
				case 2: //goth
					counter.lock = mod ?? rg.array([0, 1, 1, 2, 3]);
					counter.door = mod ?? rg.array([0, 1, 1, 2, 3]);
					break;
				case 3: //west
					counter.lock = mod ?? rg.array([0, 1, 2, 2, 3]);
					counter.door = mod ?? rg.array([0, 1, 2, 2, 3]);
					break;
			}
		}
		actual_doors(index);
	},
	
	
	high_door(index, mod){
		if(index === 101){
			rgi();
			switch(head.act){
				default:
				case 1: //feyd
					counter.lock = mod ?? rg.array([2, 3, 4, 5]);
					counter.door = mod ?? rg.array([2, 3, 4, 5]);
					break;
				case 2: //goth
					counter.lock = mod ?? rg.array([3, 4, 5, 6]);
					counter.door = mod ?? rg.array([3, 4, 5, 6]);
					break;
				case 3: //west
					counter.lock = mod ?? rg.array([4, 5, 6, 7]);
					counter.door = mod ?? rg.array([4, 5, 6, 7]);
					break;
			}
		}
		actual_doors(index);
	},
	
	
	low_portal(index, mod){
		if(index === 101){
			rgi();
			switch(head.act){
				default:
				case 1: //feyd
					counter.door = mod ?? rg.array([0, 1, 2]);
					break;
				case 2: //goth
					counter.door = mod ?? rg.array([0, 1, 1, 2, 3]);
					break;
				case 3: //west
					counter.door = mod ?? rg.array([0, 1, 2, 2, 3]);
					break;
			}
		}
		//counter.door = mod ?? ra.array([0, 1, 1, 2, 2, 3]);
		actual_portals(index);
	},
	
	
	high_portal(index, mod){
		if(index === 101){
			rgi();
			switch(head.act){
				default:
				case 1: //feyd
					counter.door = mod ?? rg.array([2, 3, 4, 5]);
					break;
				case 2: //goth
					counter.door = mod ?? rg.array([3, 4, 5, 6]);
					break;
				case 3: //west
					counter.door = mod ?? rg.array([4, 5, 6, 7]);
					break;
			}
		}
		actual_portals(index);
	},
	
}


