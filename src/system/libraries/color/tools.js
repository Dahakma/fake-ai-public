import {nameList} from "./list";
import {HEXtoHSLA, HEXtoRGB} from "./convert";
import {color} from "./config";
import {stringifyHSLA, stringifyRGB} from "./parse";
import {randomColorList} from "./random";


//const cnt; 

function container(content = ""){	//TODO MESSY AND PROVISIONAL
	const div = document.createElement("div");
	div.id = "cnt";
	div.innerHTML = content;
	document.body.appendChild(div);  
	
	const exit = document.createElement("div");
	exit.innerHTML = `EXIT`;
	exit.style.cursor = "pointer";
	//exit.style.fontWeight: "bold"; //TODO - doesnt work for some reason
	
	exit.addEventListener("click", ()=> document.getElementById("cnt").remove() );
	div.appendChild(exit);  
	
	
	const exitB = document.createElement("div");
	exitB.innerHTML = `EXIT`;
	exitB.style.cursor = `pointer`;
	exitB.addEventListener("click", ()=> document.getElementById("cnt").remove() );
	div.insertBefore(exitB, div.firstChild);
	
	cnt = div;
	
}

export function help(input){
	container();
	cnt.innerHTML =  `<div>
		<div onclick = "COLOR.listColors"> List named colors </div>
		<div onclick = "COLOR.randomHelp"> List random colors </div>
	</div>`;
	
}

/*

export function displayRandom(input){
	color = colorList[input];
	if(!color) return;
	const resolution = 100;
	const step = color.map( a => {
		return a[1] - a[0] / resolutions;
	})
	
	const container = document.createElement("div");
	div.id = "cnt";
	div.innerHTML = content;
	document.body.appendChild(div);  
	
	//CANVAS		
	canvas =  document.createElement("CANVAS");
	canvas.width = 500;
	canvas.height = 500;
	canvas.step = 
	canvas.style.top = 0;
	canvas.style.left = 0;
	
	container = document.getElementById(container);
	while (container.firstChild) {
		container.removeChild(container.firstChild);
	}
	container.appendChild(canvas);
	
	let ctx = canvas.getContext("2d");
	ctx.fillStyle = "gray";
	ctx.fillRect(0,0, canvas.width, canvas.height);
	
	
}
*/



export function listColors(input){
	let tab = `<table>
		 <tr>
			<th></th>
			<th>Name</th>
			<th>HEX</th>
			<th>RGB</th>
			<th>HSLA</th>
		</tr>
	
	`;
	
	for(const key in nameList){
		if(input && key !== input){
			continue;
		}
		
		for(let i = 0; i < nameList[key].length; i++){
			tab += `<tr>
				<td>
					<div style = "background: ${nameList[key][i][0]}; height: 1em; width: 2em;">
				</td>
				<td>
					${nameList[key][i][1]}
				</td>
				<td>
					${nameList[key][i][0]}
				</td>
				<td>
					${stringifyRGB( HEXtoRGB(nameList[key][i][0]) )}
				</td>
				<td>
					${ stringifyHSLA( HEXtoHSLA(nameList[key][i][0]) ) }
				</td>
				<td>
					<div style = "background: ${nameList[key][i][0]}; height: 1em; width: 2em;">
				</td>
			</tr>`;
		}
	}
	
	tab += `</table>`;
	container(tab);
}