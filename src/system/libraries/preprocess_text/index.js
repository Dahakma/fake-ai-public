import {distort} from "./distort";
import {units} from "./units";
import {markdown} from "./markdown";

/**
 *	preprocesses text
 * @param {string} text - text input
 * @param {Object} options - options for transformation
 *
 * @param {string} [options.language = navigator.language] - default could be manually overrriden ("en-US"; "en-GB"; "cs-CZ"; "de-DE" etc.)
 * @param {string} [options.english = "uk"]  -  "uk" or "us" -  prefer UK or US english ("metre" or "meter"); defaultly "uk" unless the language is "en-US"
 *
 * @param {Object|boolean|undefined} [options.fix] - 
 * @param {boolean} [options.fix.typos = true] - remove assumed typos (e.g. " abc ,,  de;gh.  " => "abc, de; gh.")
 * @param {boolean} [options.fix.numbers = false] - transform all numbers to local format
 *
 * @param {Object|boolean|undefined} [options.markdown] - change markdown-like tags into html
 * @param {boolean} [options.markdown.block = true] - not just inline like <em> or <strong> but also block elemens like <h1>, <li> or <p>
 * @param {boolean} [options.markdown.index = false] - if markdown.block, creates clickable index of <h2>
 *
 * @param {Object|boolean|undefined} [options.units = undefined] - handles measurement units ("1@mi" => "0.621 kilometres")
 * @param {string} options.units.symbol - symbol denoting units (could be same as the replace.symbol)
 * @param {string} [options.units.system = "si"] - "si" or "us" - US customary
 * @param {boolean} [options.units.full = false] - "m" or "metre"
 *
 * @param {Array|Object|boolean|undefined} [options.replace = false] - replaces particular strings
 * @param {string} options.replace.pattern - regex pattern
 * @param {function} options.replace.fce - replacing function, input is the word
 *
 * @param {Object|boolean|undefined} [options.distort = false] - turns text into mess
 * @param {integer} options.shuffle - shuffle letters in word (0 - no; 1 - little; 2 - all)
 * @param {boolean} options.tldr - replace middle of text with "blah..."
 * @param {integer} options.capitals - randomly capitalise letters (0 - no, 10 - max)
 * @param {boolean} options.roman - replaces  numbers with roman
 */
export function preprocess_text(text, options = {
	language, 
	english, 
	
	fix: {
		typos: true, 
		numbers: false, 
	},
	
	markdown: {
		block: true,
		index: false,
	},	

	replace: undefined,
	distort: false,
	units: false,

}){
	
	if(typeof text !== "string"){
		return undefined;
	}

	options.language ??= window.navigator.language; 
	options.english ??= options.language === "en-US" ? "us" : "en";
	options.fix ??= {
		typos: true, 
		numbers: false, 
	};
	options.markdown ??= {
		block: true,
		index: false,
	};


	if(options?.fix?.typos) text = fix_typos(text);
	if(options?.units) text = units(text, options.units, options); //before markdown & replace
	if(options?.markdown) text = markdown(text, options.markdown, options);
	if(options?.fix?.numbers) text = fix_numbers(text, options.markdown, options);

	if(options?.replace){
		if( Array.isArray(options.replace) ){
			options.replace.forEach( a => {
				text = replace(text, a);
			});
		}else{
			text = replace(text, options.replace);
		}
	}
	if(options?.distort) text = distort(text, options.distort); //last 

	return text;
}


//const test = "John has a lovely dog. 78_700@ok Beautiful maiden goes to a fair. ";
/*
const test = `
100@m
100@cm
100@km
	0.09290304@ft2 = 1 m2
	10@mi = 16.0934 km
	212@f = 100 c
	100@f = 37.77 c
	35.3147@ft3 = 1 m3
	1@ft 10@ft
	100@ft2 = 1076 m2
`;
 

console.warn(  preprocess_text(test, {
	trim: true,
	units: {
		symbol: "@",
		system: "us",
		//language: "us",
		full: true,
	},
}));
*/

/*
const test = "9879 1_000_000 4_456 4.456_123";

console.error(  preprocess_text(test, {
	//language: "en-US", 
	markdown: true,
}));
 
 console.log("d");
*/


//REPLACER 
function replace(text, options){
	//input = input.replace(/@(\w+)/gm, smail);

	const {fce, pattern} = options;
	if(typeof fce !== "function" || typeof pattern !== "string") return text;
	
	function exec(match, p1){
		return fce(p1);
	}

	const rex = new RegExp(pattern, `gm`); //TODO - ADD ESCAPE
	return text.replace(rex, exec); 
}


 




//format numbers
import {stringify_number} from "./util";
function fix_numbers(text, options, all_options){
	function exec(match, group){
		const newest = group.replaceAll("_", "");
		if(newest === group && !options.numbers) return group; //nothing to change 
		return stringify_number(+newest, all_options);
	}
	const rex = new RegExp(`(\\d[_\\.\\d]*)`, `gm`); //TODO - ADD ESCAPE
	return text.replace(rex, exec); 
}	


/*
	`abc,,   qwe;;;` => `abc, qwe;` - replace multipled `,`, `;` and ` `
	`abc , ;` => `abc,;` - removes ` ` before `,` or `;`
	`abc,qwe;yxc` => `abc, qwe; yxc` - forces ` ` behind `,` and `;`; ignores `.`; ignores when digits on both sides (`1,000` => `1,000)
	`  abc  ` => `abc` - outer trim
*/

function fix_typos(input){
	if(!input || typeof input !== "string") return ``; //redundant
	let output = input;

	 // "abc,,   qwe;;;" => "abc, qwe;";
	//TODO - THIS VERSION INCLUDE ALL WHITESPACE (inc line break) //output = output.replace(/[,]{2,}|[;]{2,}|\s{2,}/gm, a => a[0] );
	//output = output.replace(/[,]{2,}|[;]{2,}|\ {2,}/gm, a => a[0] );
	
//TODO ; REMOVED TEMPFIX
output = output.replace(/[,]{2,}|\ {2,}/gm, a => a[0] );
//Krak&#243;w

	// "abc , qwe; " =>  "abc, qwe; "
	output = output.replace(/\s+,/gm, `,`);  //TODO - aren't those regexes redundant? 
	//output = output.replace(/\s+([,;])/gm, (match, p1) => p1);
//TODO ; REMOVED TEMPFIX
	output = output.replace(/\s+([,])/gm, (match, p1) => p1);

	// "abc,qwe;yxc" => "abc, qwe; yxc" 
	//output = output.replace(/([^\d\W]+)([,;])(\w+)|(\w+)([,;])([^\d\W]+)/gm, (match, p1, p2, p3, p4, p5, p6) => p1 ? `${p1}${p2} ${p3}` : `${p4}${p5} ${p6}` );
//TODO ; REMOVED TEMPFIX
output = output.replace(/([^\d\W]+)([,])(\w+)|(\w+)([,])([^\d\W]+)/gm, (match, p1, p2, p3, p4, p5, p6) => p1 ? `${p1}${p2} ${p3}` : `${p4}${p5} ${p6}` );
	/*
		¨[^\d\W] will match any non-digit and (non-non-word) i.e. a word character
		([^\d\W]+),(\w+)  matches "asdf,123"
		(\w+),([^\d\W]+) matches "123,asdf"
		"123,123" are ignored
		"asdf,asdf" are matched always
	*/

	// "  abc   " => "abc";
	output = output.trim(); 


	//TODO ASAP Lookbehind issues
	//output = output.replace(/(?<=\w)[,;](?=\w)/gi, `$& `); // "abc,qwe" => "abc, qwe"

	//output = output.replace(/[.]\s[.]/gi, a => a[0] ); // "abc. ." => "abc."; //TODO - not sure if bright idea 
	//output = output.replace(/(?<=\w\.)[a-z]/g, a => ` ${a.toUpperCase()}` ); // "abc.qwe " => "abc. Qwe";
	
	return output;
}

