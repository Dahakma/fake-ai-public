import {stringify_number, round} from "./util";


const list = {
	cm: {
		full: `centimetres`,
		us:  `in`, 
		to_us: a => round(a * 0.3937, 2),
	},
	
	m: {
		full: `metres`,
		us: `ft`,
		to_us: a => round(a * 3.2808399),
	},
	
	m2: { //TODO
		short: `m^2`,
		full: `square metres`,
		us: `ft2`,
		to_us: a => round(a * 10.76391042),
	},
	
	m3: { //TODO
		short: `m^3`,
		full: `cubic metres`,
		us: `ft3`,
		to_us: a => round(a * 35.3146667),
	},
	
	km: {
		full: `kilometres`,
		us: `mi`,
		to_us: a => round(a * 0.62137,2),
	},
	
	km2: { //TODO
		short: `km^2`,
		full: `square kilometres`,
		us: `mi2`,
		to_us: a => round(a * 0.386102159, 2),
	},
	
	c: {
		short: `°C`,
		full: `degree Celsius`,
		plural: `degrees Celsius`,
		us: `f`,
		to_us: a => round( a * (9/5)+32  ) ,
	},
	
	
	
	in: {
		full: `inch`,
		plural: `inches`,
		si: `cm`,
		to_si: a => round(a * 2.54),
	},
	
	ft: {
		full: `foot`,
		plural: `feet`,
		si: `m`,
		to_si: a => round(a * 0.3048, a > 99 ? 0 : 2)
	},
	
	ft2: {
		full: `square foot`,
		plural: `square feet`,
		si: `m2`,
		//to_si: a => round(a / 0.09290304),¨
		to_si: a => round(a / 10.764),
	},
	
	ft3: {
		full: `cubic foot`,
		plural: `cubic feet`,
		si: `m3`,
		to_si: a => round(a * 0.0283168466),
	},
	
	mi: {
		full: `miles`,
		si: `km`,
		to_si: a => round(a * 1.609344, 2),
	},
	
	mi2: {
		full: `square miles`,
		si: `km2`,
		to_si: a => round(a * 2.589988),
	},
	
	
	f: {
		short: `°F`,
		full: `degree Fahrenheit`,
		plural: `degrees Fahrenheit`,
		si: `c`,
		to_si: a => round( (a-32) * (5/9)  ) ,
	},
	
	/*
	fm: {
		full: `metres`,
		rename: `feet`,
		count: a => dh.round(a * 0.3048), //count calculate ft to m
		recount: a => a, //recount stays same
	},
	
	
	
	mkm: {
		full: `kilometres`,
		rename: `miles`,
		count: a => dh.round(a * 1.609344, 2), //count miles to km  //TODO TEST
		recount: a => a, //recount stays same
	},
	*/
	
};
	
	
	
export function units(text, options = {
	symbol: "@",
	system: "si",
	language: "uk",
	full: false,
	recount: true,
}, all_options){
	

	const system = options.system ?? "si";
	const recount = options.recount ?? true;
	
	
	
	const symbol = "@";
	function exec(match, value, unit){
		const data = list[unit];
		value = +value.replaceAll("_", "");

		if(!data || isNaN(value)) return `${value} ${unit}`; //something is broken
		
		//recount to si x us customary
		if(recount){
			if(system === "si" && data.to_si){
				value = data.to_si(value);
				unit = data.si;	
				
			}else if(system === "us" && data.to_us){
				value = data.to_us(value);
				unit = data.us;
			}
		}
		
		//name of the unit
		if(options?.full){
			if(value == 1){
				unit = list[unit].full;
			}else{
				if(list[unit].plural){
					unit = list[unit].plural;
				}else if(list[unit].full.slice(-1) !== "s"){
					unit = `${list[unit].full}s`;
				}else{
					unit = list[unit].full;
				}
			}
			
			if(options?.language === "us"){
				unit = unit.replace("metre", "meter")
			}
		}else if(list[unit].short){
			unit = list[unit].short;
		}
		

		value = stringify_number(value, all_options); //TODO
		return `${value} ${unit}`
	}
	
//ASAP IMPORTANT
//const rex = new RegExp(`(\\d+?[_\\.\\d]?)${symbol}(\\w+)`, `gm`); //TODO - which one??????

	const rex = new RegExp(`(\\d[_\\.\\d]*)${symbol}(\\w+)`, `gm`); //TODO - ADD ESCAPE
	return text.replace(rex, exec); 
}





