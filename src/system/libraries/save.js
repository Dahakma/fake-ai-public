const autosave_slots = 3;
const default_saving_slots = autosave_slots + 12 - 1; //autosaves + regular slots - starts at 0

//TODO SCROLL TO TOP

export function menu(gamekey, div, prepare_save, finish_load, export_note, style, back){
	const container = (()=> {
		let container = document.getElementById("div_saves_container");
		if(!container){
			container = document.createElement("DIV");
			container.id = "div_saves_container";
			container.className = style.container;
			div.append(container);	
		}else{
			container.innerHTML = "";
		}
		return container;
	})();

	
	//list with references
	let list;
		
		//localStorage disabled:
		try{
			list = localStorage.getItem(`${gamekey}_save_list`);
		}catch(err){
			container.innerHTML = `Local Storage (the place where are saves saved in your browser) is disabled! `;
			return;
		}
		
		//list initiated? 
		if(list === null){
			list = [];
			list.length = default_saving_slots;
			localStorage.setItem(`${gamekey}_save_list`, JSON.stringify(list) );
		}else{
			list = JSON.parse(list);
		}	

	//restart - trigger again this function to display changes
	function restart(){
		menu(gamekey, container, prepare_save, finish_load, export_note, style, back);
	}
	
	//clicable stuff
	function link(name, fce, block = false){
		const temp = document.createElement("DIV");
		temp.innerHTML = name;
		temp.className = style.click;
		if(!block){
			//temp.style.displey = "inline";
			temp.style.gridColumn = "auto";
			//temp.style.width = "auto";
			//temp.style.gridColumn = "1 / -1"; /* when in grid, takes whole row */
		}else{
			temp.style.gridColumn = "1 / -1"; /* when in grid, takes whole row */
		}
		container.append(temp);
		temp.addEventListener("click",function(){
			fce();
		}, false);
	}
		
	//slots
	function slot(index, before = "", after = "", editable = true){
		//label
		const a = list[index];
		

		const text = document.createElement("DIV");
		text.className = style.text;
		text.style.display = "flex";
		text.style.flexDirection = "column";
		container.append(text);
		
		const title = document.createElement("DIV");
		text.append(title);
		
		if(!a){
			title.innerHTML = `${before} - empty - ${after}`;
		
		}else{
			title.innerHTML = `${before}${a.name}${after}`;
			
			const date = new Date(+a.date);
			
			const temp = document.createElement("DIV");
			temp.style.fontSize = "90%";
			temp.innerHTML = date.toLocaleTimeString();
			text.append(temp);
		}
		
		
		//save x load
		if(!a && editable){
			 link(`Save`, function(){
				const temp = prepare_save();
				save(gamekey, temp.savegame, index, temp.name);
				restart();
			 });
		}else if(a){
			link(`Load`, function(){
//console.warn(gamekey, index)
				const temp = load(gamekey, index);
//console.warn(temp)
				finish_load(temp);
			 });
		}else{
			link(``,()=>{});
		}
		
		//save as x delete
		if(!a && editable){
			link(`Save As`, function(){
				const temp = prepare_save();
				temp.name = prompt("The name of the save:", temp.name);
				save(gamekey, temp.savegame, index, temp.name);
				restart();
			 });
		}else if(a && editable){
			link(`Delete`, function(){
				remove(gamekey, index);
				restart();
			 });
		}else{
			link(``,()=>{});
		}
		
	}
	
	//remove useless empty slots but keep the last one
	while(list.length > default_saving_slots && !list[list.length - 1] ){
		list.pop();
	}
	list.push(null);


/*TODO - SMALL AMOUT OF SAVES!!!! */
	//fast save & quit
	link(`Save to the last slot`, function(){
		const temp = prepare_save();
		save(gamekey, temp.savegame, list.length - 1, temp.name);
		back();				
	}, true);
	
	
	//latest save	
	const last = latest(gamekey);
		if(last !== -1){
			if(last < autosave_slots){
				//slot(last, `LATEST: `, `(Autosave ${last + 1})`, false);
				link(`Load the last save (Autosave ${last + 1}. ${list[last].name})`, function(){
					const temp = load(gamekey, last);
					finish_load(temp);
				}, true);
				const manual = latest(gamekey, autosave_slots);
				if(manual !== -1){
					//slot(manual, `LATEST MANUAL: ${manual - 2}. `, ``, false);
					link(`Load the last manual save (${manual - 2}. ${list[manual].name})`, function(){
						const temp = load(gamekey, manual);
						finish_load(temp);
					}, true);
				}
			}else{
				//slot(last, `LATEST: ${last - 2}. `, ``, false);
				link(`Load the last save (${last - 2}. ${list[last].name})`, function(){
					const temp = load(gamekey, last);
					finish_load(temp);
				}, true);
			}
		}

	//autosaves
	for(let i = 0; i < autosave_slots; i++){
		if(!list[i]) continue;
		slot(i, ``, `(Autosave ${i + 1})`, false);
	}

	//regular slots 
	for(let i = autosave_slots; i < list.length; i++){
		slot(i, `${i - 2}. `);
	}
	
	

	link(`Delete All Saves`, function(){
		if( confirm("All saves will be irreversibly deleted! ") ){
			for(let i = 0; i <= list.length; i++){
				remove(gamekey, i);
			}
			localStorage.removeItem(`${gamekey}_save_list`); //restored as empty during restart
			restart();
		}
	 }, true);
			 
	link(`Export to File`, function(){
		export_to_file(gamekey, export_note);
	 }, true);		 
	
	link(`Import from File`, function(){
		import_from_file(gamekey, restart);
	 }, true);		 
	 
		/*	
			<br>
			<span class=\"savebutton\" onclick=\"SAVE.exportCharacter()\">Export character </span>
			<span class=\"savebutton\" onclick=\"SAVE.importCharacter()\">Import character </span>
		*/
}




 
//EXPORT
function export_to_file(gamekey, {name, note}){
	//content
	const list = JSON.parse(  localStorage.getItem(`${gamekey}_save_list`)  );
	const snapshots = JSON.parse( localStorage.getItem(`${gamekey}_snapshots`) ?? "{}"  ); //SNAPSHOTS - SHOULD NOT BE IN THIS FILE
	
	let file_content = {
		name,
		note,
		list,
		snapshots, //SNAPSHOTS - SHOULD NOT BE IN THIS FILE
		savegames: list.map( (a,index) => {
			if(!a) return null;
			return  JSON.parse(  localStorage.getItem(`${gamekey}_save_${index}`) );
		}),
		
	};
	
	file_content = JSON.stringify(file_content);
	

	//name
	const date = new Date().valueOf();
	const file_name = "Fake AI Saves "+date;
	
	
	//console.log(file_content);


	//magic
	//Basically appropriate code from https://thiscouldbebetter.wordpress.com/2012/12/18/loading-editing-and-saving-a-text-file-in-html5-using-javascrip/
	const textToSaveAsBlob = new Blob([file_content], {type:"text/plain"}); //creates blob or something
	const textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob); //creates url or something

	const download_link = document.createElement("a"); //creates invisible download link and clicks on it
	Object.assign(download_link,{
		download: file_name,
		innerHTML: "Download File",
		href: textToSaveAsURL,
		onclick: destroyClickedElement,
	});	
	download_link.style.display = "none";
	document.body.appendChild(download_link);
	download_link.click();
	
}

function destroyClickedElement(event){
	document.body.removeChild(event.target);
}

//IMPORT
function import_from_file(gamekey, back){
	if( !confirm(`Warning! All current saves will be rewritten by the saves from the file! `) ) return;

	const pick_file = document.createElement("input");
	pick_file.type = "file";
	pick_file.id = "id_load";
	pick_file.style.display = "none";
	document.body.appendChild(pick_file);	
	
	pick_file.oninput = (e)=> {
		const fileToLoad = document.getElementById("id_load").files[0];
		const fileReader = new FileReader();
		fileReader.onload = function(fileLoadedEvent) {
				let savefile = fileLoadedEvent.target.result;
				
				//parse json
				try{
					savefile = JSON.parse(savefile);
				}catch(err){
				}
				let {name, note, list, savegames} = savefile;
				
				//incompatible file
				if(
					   !name || typeof name !== "string" 
					|| !note || typeof note !== "string" 
					|| !list || typeof list !== "object"
					|| !savegames || typeof savegames !== "object"
				){
					/* eslint-disable no-alert, no-console */
					//TODO
						console.warn(fileLoadedEvent.target.result);
						alert(`The format of the file is incompatible! `);
					/* eslint-enable no-alert, no-console */
					return;
				}
				
				//list
				list = JSON.stringify(list);
				localStorage.setItem(`${gamekey}_save_list`, list);
				
				
				localStorage.setItem(`${gamekey}_snapshots`, JSON.stringify(savefile.snapshots) );  //SNAPSHOTS - SHOULD NOT BE IN THIS FILE
				
				
				//savegames
				savegames.forEach( (a,i) => {
					localStorage.setItem(`${gamekey}_save_${i}`, JSON.stringify(a) );
				});
				
				
				back(); //restarts the save menu

		};
		
		fileReader.readAsText(fileToLoad, "UTF-8");
	};
		
	pick_file.click();	
}


//autosaves; there are three autosaves 
export function autosave(gamekey, prepared){
	//moves old savegames
	let list = localStorage.getItem(`${gamekey}_save_list`);
	list = JSON.parse(list);

	for(let i = autosave_slots - 1; i > 0; i--){
		list[i] = list[i - 1];
		const savegame = localStorage.getItem(`${gamekey}_save_${i - 1}`);
		localStorage.setItem(`${gamekey}_save_${i}`, savegame);
	}
	
	list = JSON.stringify(list);
	localStorage.setItem(`${gamekey}_save_list`, list);
	
	//saves
	save(gamekey, prepared.savegame, 0, prepared.name);
}	
	
	
	
//loads the latest autosave or starts the game if there is none
//alternative - alternative function which will be triggered in case there is no save () 
export function autoload(gamekey, alternative){
	
	let list = localStorage.getItem(`${gamekey}_save_list`);
		
	//INITIATED? - list does not exist = saves are called for the first time = is initiated
	if(list === null){
		list = [];
		list.length = default_saving_slots;
		localStorage.setItem(`${gamekey}_save_list`, JSON.stringify(list) );
	}else{
		list = JSON.parse(list);
	}
		
	
	//if all saves are empty
	if(!list.filter(a => a).length) return null; 
		
	//loads
	const index = latest(gamekey);
	return load(gamekey, index);
}


function latest(gamekey, start = 0, end){
	let list = localStorage.getItem(`${gamekey}_save_list`);
	list = JSON.parse(list);
	let index = -1;
	let date = 0;
				
	for(let i = start; i < (end || list.length); i++){
		if(!list[i]) continue; //undefined
		if(list[i].date > date){
			date = list[i].date;
			index = i;
		}
	}
	return index;
}


function save(gamekey = "", savegame, index, name){
	if(typeof savegame !== "object") return;
	
	//save
	savegame = JSON.stringify(savegame);
	localStorage.setItem(`${gamekey}_save_${index}`, savegame);
	
	//save reference to 
	const date = new Date().valueOf();
	if(name === undefined) name = `save${date}`;
	let list = localStorage.getItem(`${gamekey}_save_list`);
	list = JSON.parse(list);
	list[index] = {name, date};
	list = JSON.stringify(list);
	localStorage.setItem(`${gamekey}_save_list`, list);
}


function load(gamekey = "", index){
	const savegame = localStorage.getItem(`${gamekey}_save_${index}`);
	if(savegame === null) return; //should not happen
	return JSON.parse(savegame);
}



function remove(gamekey = "", index){
	//save
	localStorage.removeItem(`${gamekey}_save_${index}`);
	
	//reference
	let list = localStorage.getItem(`${gamekey}_save_list`);
	list = JSON.parse(list);
	list[index] = null;
	list = JSON.stringify(list);
	localStorage.setItem(`${gamekey}_save_list`, list);
}