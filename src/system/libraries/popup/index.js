import { writable, derived } from 'svelte/store';

function create_popup(input){
	const { subscribe, set, update, get } = writable(input);

	return {
		subscribe,
		update,
		
		alert: (text = "Beware!", fce = ()=>{}) => update(a => {
			a.text = text;
			a.display = true;
			a.window = "alert";
			a.fce = fce;
			return a;
		}),
		
		note: (text = "Beware.")  => update(a => {
			a.text = text;
			a.display = true;
			a.note = "down";
			a.window = "note";
			a.animate =  "down";
			a.progress = false;
			//a.fce = fce;
			return a;
		}),
		
		
		confirm: (text = "???", fce = ()=>{}) => update(a => {
			a.text = text;
			a.display = true;
			a.window = "confirm";
			//a.yes = yes;
			//a.no = no;
			a.fce = fce;
			return a;
		}),
		
		prompt: (text = "???",  value = "", fce = ()=>{}, options) => update(a => {
			a.text = text;
			a.display = true;
			a.window = "prompt";
			a.default_value = value; 
			a.fce = fce;
			a.options = options;
			return a;
		}),
		
		
		options: (text = "???", options = [], fce = ()=>{}) => update(a => {
			a.text = text;
			a.display = true;
			a.window = "options";
			a.options = options;
			a.fce = fce;
			return a;
		}),
		
		
		splitter: (text, max = 1, fce = ()=>{}, value = undefined) => update(a => {
			a.text = text;
			a.display = true;
			a.window = "splitter";
			a.max = max;
			a.default_value = value; 
			//a.yes = yes;
			//a.no = no;
			a.fce = fce;
			return a;
		}),
		
		done: (verdict) => update(a => {
			a.fce(verdict);
			/*
			if(verdict){
				if(a.yes) a.yes();
			}else{
				if(a.no) a.no();
			}
			*/
			a.text = "";
			a.display = false;
			a.window = "";
			a.fce = undefined;
			a.options = undefined;
			a.default_value = "";
			//a.yes = undefined;
			//a.no = undefined;
			return a;
		}),
	}
}	
		

export const popup = create_popup({
	display: false,
	text: "empty",
	window: "alert",
	max: 0, 
	default_value: "",
	fce: undefined,
	options: undefined,
	
	//yes: undefined,
	//no: undefined,
})