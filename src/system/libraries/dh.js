(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["dh"] = factory();
	else
		root["dh"] = factory();
})(self, function() {
return /******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ 336:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "and": () => /* reexport */ and,
  "and_some": () => /* reexport */ and_some,
  "arabic2roman": () => /* reexport */ arabic2roman,
  "base10_to_base64": () => /* reexport */ base10_to_base64,
  "base64_to_base10": () => /* reexport */ base64_to_base10,
  "between": () => /* reexport */ between,
  "big_base10_to_base64": () => /* reexport */ big_base10_to_base64,
  "big_base64_to_base10": () => /* reexport */ big_base64_to_base10,
  "capitalise": () => /* reexport */ capitalise,
  "cartesian2polar": () => /* reexport */ cartesian2polar,
  "changed_properties": () => /* reexport */ changed_properties,
  "check_image": () => /* reexport */ check_image,
  "clamp": () => /* reexport */ clamp,
  "clone": () => /* reexport */ clone,
  "copy_properties": () => /* reexport */ copy_properties,
  "decapitalise": () => /* reexport */ decapitalise,
  "equals": () => /* reexport */ equals,
  "female_surname": () => /* reexport */ female_surname,
  "gradual": () => /* reexport */ gradual,
  "haversine": () => /* reexport */ haversine,
  "id2index": () => /* reexport */ id2index,
  "is_empty": () => /* reexport */ is_empty,
  "number2word": () => /* reexport */ number2word,
  "percent": () => /* reexport */ percent,
  "perpendicular": () => /* reexport */ perpendicular,
  "pick_index": () => /* reexport */ pick_index,
  "pick_multiple": () => /* reexport */ pick_multiple,
  "pick_one": () => /* reexport */ pick_one,
  "plural": () => /* reexport */ plural,
  "pm": () => /* reexport */ pm,
  "pms": () => /* reexport */ pms,
  "polar2cartesian": () => /* reexport */ polar2cartesian,
  "random": () => /* reexport */ random,
  "random_once": () => /* reexport */ random_once,
  "remove_children": () => /* reexport */ remove_children,
  "remove_duplicates": () => /* reexport */ remove_duplicates,
  "round": () => /* reexport */ round,
  "s": () => /* reexport */ s,
  "shuffle": () => /* reexport */ shuffle,
  "unique": () => /* reexport */ unique,
  "var_name": () => /* reexport */ var_name,
  "vowel": () => /* reexport */ vowel,
  "weighted_random": () => /* reexport */ weighted_random,
  "witchcraft": () => /* reexport */ witchcraft,
  "x_items": () => /* reexport */ x_items,
  "x_plural": () => /* reexport */ x_plural
});

;// CONCATENATED MODULE: ./src/wip.js
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/**
	gradual(input, value, output, include)
	- input {array [min , max] or [min, mid, max]}
	- value {integer}
	- output {array [min, max] or [min, mid, max]}
	- 
	- finds where the `value` lies on the range `input` and returns the corresponting value on `output` range
	- etc. gradual([0, 10], 5, [200, 400]) = 300
	- include {integer}
		- `undefined` - will calculate even beyond `output` ranges 
			gradual([10,20], 1, [100, 200]) = 10
		- `1` when the `value` is lower than `input[0]` return 0 (only values above are included)
		- `-1` when the `value` is higher than `input[2]` (respective `input[1]` when only two values are set) return 0 (only values below are included)
		- `0` any `value` outsidde the input range is not included 
*/
var gradual = function gradual(_ref, value, _ref2, include) {
  var _ref3 = _slicedToArray(_ref, 3),
      in_min = _ref3[0],
      in_mid = _ref3[1],
      in_max = _ref3[2];

  var _ref4 = _slicedToArray(_ref2, 3),
      out_min = _ref4[0],
      out_mid = _ref4[1],
      out_max = _ref4[2];

  if (in_max === undefined) {
    in_max = in_mid;
    in_mid = (in_max + in_min) * 0.5;
  }

  ;

  if (out_max === undefined) {
    out_max = out_mid;
    out_mid = (out_min + out_max) * 0.5;
  }

  ;

  if (value < in_mid) {
    value -= in_min;

    if (value <= 0) {
      if (include === 1 || include === 0) return 0; //return out_min;
    }

    var percent = value / (in_mid - in_min);
    return out_min + percent * (out_mid - out_min);
  } else {
    value -= in_mid;

    if (value > in_max) {
      if (include === -1 || include === 0) return 0; //return out_max;
    }

    var _percent = value / (in_max - in_mid);

    return out_mid + _percent * (out_max - out_mid);
  }
};
;// CONCATENATED MODULE: ./src/utility.js
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || utility_unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function utility_unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return utility_arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return utility_arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return utility_arrayLikeToArray(arr); }

function utility_arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var SEED;
/**
	seed-based random number 
	see: http://indiegamr.com/generate-repeatable-random-numbers-in-js/ 
*/

function random(seed) {
  var max = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var min = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

  if (seed === undefined) {
    //not initiated
    if (typeof SEED === "undefined") {
      var d = new Date();
      seed = d.getTime();
      SEED = seed;
    } else {//normal round
    }
  } else {
    //seed set
    if (seed < 100) seed *= 1234; //TODO RANDOM TEMP

    SEED = seed;
  }

  SEED = (SEED * 9301 + 49297) % 233280;
  var random = SEED / 233280;
  return min + random * (max - min);
}
;
function random_once(seed) {
  if (seed === undefined) return Math.random();
  return random(seed);
}
;
function remove_duplicates(items) {
  return _toConsumableArray(new Set(items));
}
;// CONCATENATED MODULE: ./src/array.js
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }


/**
	Randomize array element order in-place.
	Using Durstenfeld shuffle algorithm (Fisher-Yates)
*/
//TODO - changes the original array, good or bad?

function shuffle(array, seed) {
  if (array === undefined) return;
  random(seed); //initiates RNG

  for (var i = array.length - 1; i > 0; i--) {
    var j = Math.floor(random() * (i + 1));
    var _ref = [array[j], array[i]];
    array[i] = _ref[0];
    array[j] = _ref[1];
  }

  return array;
}
function pick_one(array, seed) {
  if (!Array.isArray(array)) {
    array = Array.from(arguments); //array = [array];

    seed = void 0;
  }

  ;
  var index = Math.floor(array.length * random_once(seed));
  return array[index];
}
function pick_index(array, seed) {
  if (_typeof(array) !== "object") {
    array = Array.from(arguments); //array = [array];

    seed = void 0;
  }

  ;
  return Math.floor(array.length * random_once(seed));
}
function pick_multiple() {
  var array = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var how_many = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var exclusive = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
  var seed = arguments.length > 3 ? arguments[3] : undefined;
  var input = array.map(function (a) {
    return a;
  });
  var output = [];

  for (var i = 0; i < how_many && input.length; i++) {
    var index = pick_index(input, random(seed));
    output.push(input[index]);
    if (exclusive) input.splice(index, 1); //const temp = input[  pick_index(input, random(seed) )  ];
    //array = array.filter( a => a !== temp );
  }

  return output;
}
/**
		takes array (of number) and returns index of one of item 
		with random chance based on its value
	*/

function weighted_random(array, seed) {
  //cleans values 
  var input = array.map(function (a) {
    if (a === undefined) return 0;
    if (isNaN(a)) return 0;
    if (a < 0) return 0;
    return a;
  });
  var range = input.reduce(function (a, b) {
    return a + b;
  });
  var result = random_once(seed) * range;
  range = 0;

  for (var ii = 0; ii < input.length; ii++) {
    range += input[ii];
    if (result <= range) return ii;
  }

  ;
}
; //search in array of object with property id the index of the one with inputed id

function id2index(array, id) {
  for (var i = array.length - 1; i >= 0; i--) {
    if (array[i].id === id) return i;
  }

  return -1;
}
;
;// CONCATENATED MODULE: ./src/numbers.js

/**
	change format of numbers from 10000 to 10 000, no idea how it works 
*/
//TODO

function witchcraft(num) {
  num = num.toString();
  return num.replace(/\B(?=(?:\d{3})+(?!\d))/g, " ");
}
function round(input) {
  var places = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  if (isNaN(input)) input = parseFloat(input); //TODO - mkes sense? 
  //TODO CHECK INVALID

  input = input * Math.pow(10, places);
  input = Math.round(input); //math round is iffy; TO DO

  input = input / Math.pow(10, places);
  return input;
}
;
/**
	 * Clamp a number between a minimum and maximum value
	 * @param {number} num
	 * @param {number} min
	 * @param {number} max
	 * @returns {number} Clamped number
	 * stolen from Drawpoint
 */

function clamp(input, min, max) {
  return input < min ? min : input > max ? max : input;
}
function between(min, max, seed) {
  return min + random_once(seed) * (max - min);
} //TODO REFACTOR

function number2word(input) {
  if (isNaN(input)) input = parseInt(input);
  var number = input;
  var result = "";

  if (input < 0) {
    number = Math.abs(number);
    result = "minus ";
  } //extra case 


  if (number > 999 && number < 2000) {
    var hundreds = Math.floor(number / 100);
    result += basicProcedure(hundreds) + " hundred ";
    number -= hundreds * 100;
  } else {
    //thousands
    if (number > 999) {
      var thousands = Math.floor(number / 1000);
      result += basicProcedure(thousands) + " thousand";
      number -= thousands * 1000;

      if (number > 99 && number % 100) {
        result += ", ";
      } else if (number > 99 || number % 100) {
        result += " and ";
      }
    } //hundreds


    if (number > 99) {
      var _hundreds = Math.floor(number / 100);

      result += simpleNumbers(_hundreds) + " hundred";
      number -= _hundreds * 100;
      if (number > 0) result += " and ";
    }
  }

  result += basicProcedure(number);

  function basicProcedure(input) {
    var result = "";

    if (input == 0) {//DO NOTHING 
    } else if (input < 20) {
      result += simpleNumbers(input);
    } else {
      var tens = Math.floor(input / 10);
      result += doubleNumbers(tens);
      var ones = Math.floor(input / 10 % 1 * 10);
      if (ones > 0) result += "-" + simpleNumbers(ones);
    }

    ;
    return result;
  }

  ;

  function doubleNumbers(input) {
    switch (input) {
      case 2:
        return "twenty";

      case 3:
        return "thirty";

      case 4:
        return "forty";

      case 5:
        return "fifty";

      case 6:
        return "sixty";

      case 7:
        return "seventy";

      case 8:
        return "eighty";

      case 9:
        return "ninety";
    }

    ;
  }

  ;

  function simpleNumbers(input) {
    switch (input) {
      case 0:
        return "zero";

      case 1:
        return "one";

      case 2:
        return "two";

      case 3:
        return "three";

      case 4:
        return "four";

      case 5:
        return "five";

      case 6:
        return "six";

      case 7:
        return "seven";

      case 8:
        return "eight";

      case 9:
        return "nine";

      case 10:
        return "ten";

      case 11:
        return "eleven";

      case 12:
        return "twelve";

      case 13:
        return "thirteen";

      case 14:
        return "fourteen";

      case 15:
        return "fifteen";

      case 16:
        return "sixteen";

      case 17:
        return "seventeen";

      case 18:
        return "eighteen";

      case 19:
        return "nineteen";
    }

    ;
  }

  ;
  return result;
}
; //percent(0.5) == 50%

function percent(input) {
  var places = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var output = round(input * 100, places);
  return "".concat(output, "%");
}
;
function arabic2roman(number) {
  //https://www.selftaughtjs.com/algorithm-sundays-converting-roman-numerals/
  //TO DO - UNTESTED
  var result = '';
  var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
  var roman = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];

  for (var i = 0; i <= decimal.length; i++) {
    // looping over every element of our arrays
    while (number % decimal[i] < number) {
      // keep trying the same number until it won't fit anymore      
      result += roman[i]; // add the matching roman number to our result string

      number -= decimal[i]; // remove the decimal value of the roman number from our number
    }
  }

  return result;
} //https://slavik.meltser.info/convert-base-10-to-base-64-and-vise-versa-using-javascript/	

var base64 = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-";
function base10_to_base64(num) {
  num = +num;
  if (isNaN(num)) return undefined;
  var base = base64.length;
  var out = "";
  var r;

  while (num) {
    r = num % base;
    num -= r;
    num /= base;
    out = base64.charAt(r) + out;
  }

  return out;
}
function base64_to_base10(str) {
  str = "" + str;
  var base = base64.length;
  var out = 0;
  var r;

  while (str.length) {
    r = base64.indexOf(str.charAt(0));
    str = str.substr(1);
    out *= base;
    out += r;
  }

  return out;
}
function big_base10_to_base64(string) {
  if (isNaN(string)) return undefined;
  var num = BigInt(string);
  var base = BigInt(base64.length);
  var out = "";
  var r;

  while (num) {
    r = num % base;
    num -= r;
    num /= base;
    out = base64.charAt(Number(r)) + out;
  }

  return out;
}
function big_base64_to_base10(str) {
  str = "" + str;
  var base = BigInt(base64.length);
  var out = BigInt(0);
  var r;

  while (str.length) {
    r = BigInt(base64.indexOf(str.charAt(0)));
    str = str.substr(1);
    out *= base;
    out += r;
  }

  return "".concat(out);
}
function pm(input) {
  return input < 0 ? "-".concat(Math.abs(input)) : "+".concat(input);
}
;
function pms(input) {
  return input < 0 ? "&#10134; ".concat(Math.abs(input)) : "&#10133; ".concat(input);
}
;
;// CONCATENATED MODULE: ./src/text.js
function text_toConsumableArray(arr) { return text_arrayWithoutHoles(arr) || text_iterableToArray(arr) || text_unsupportedIterableToArray(arr) || text_nonIterableSpread(); }

function text_nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function text_unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return text_arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return text_arrayLikeToArray(o, minLen); }

function text_iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function text_arrayWithoutHoles(arr) { if (Array.isArray(arr)) return text_arrayLikeToArray(arr); }

function text_arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }




function capitalise(string) {
  if (!string) return "";
  return string.charAt(0).toUpperCase() + string.slice(1);
}
function decapitalise(string) {
  if (!string) return "";
  return string.charAt(0).toLowerCase() + string.slice(1);
}
function s(string) {
  if (string.endsWith("s") || string.endsWith("z")) return "".concat(string, "'");
  return "".concat(string, "'s");
}
function plural(value, singular, plural) {
  if (value === undefined || value === 1) return singular;
  if (plural !== undefined) return plural;
  var last = singular[singular.length - 1];
  var second_last = singular[singular.length - 2];
  if (last === "s" || last === "z") return singular + "es";
  if (last === "y" && !vowel(second_last)) return singular.slice(0, -1) + "ies";
  if (last + second_last === "sh" || last + second_last === "ch") return singular + "es"; //https://en.wikipedia.org/wiki/English_plurals#Regular_plurals
  //todo ies
  //f ves
  //guy	

  return singular + "s";
}
var asdf_plural = plural;
function x_plural(value, singular, plural) {
  return "".concat(value, " ").concat(asdf_plural(value, singular, plural));
} //TODO plural x plural

function x_items(value, singular, plural) {
  return "".concat(number2word(value), " ").concat(asdf_plural(value, singular, plural));
}
function vowel(input) {
  return !!input.match(/[aeiouAEIOU]/);
}
function and_some(items, limit) {
  items = text_toConsumableArray(new Set(items)).filter(function (a) {
    return a;
  }).map(function (b) {
    return b.trim();
  }); //remove duplicates; removes "" and undefined; removes whitespaces

  items = shuffle(items);
  if (limit) items = items.slice(0, limit);
  return and(items);
}
function and() {
  for (var _len = arguments.length, items = new Array(_len), _key = 0; _key < _len; _key++) {
    items[_key] = arguments[_key];
  }

  if (Array.isArray(items[0])) {
    items = items[0];
  }

  ; //TODO

  items =
  /*[...new Set(items)]*/
  items.filter(function (a) {
    return a;
  }).map(function (b) {
    return b.trim();
  }); // /*remove duplicates;*/ removes "" and undefined; removes whitespaces

  if (!items.length) {
    return "";
  } else if (items.length === 1) {
    return "".concat(items[0]);
  } else if (items.length === 2) {
    return "".concat(items[0], " and ").concat(items[1]);
  } else {
    var output = items[0];

    for (var i = 1; i < items.length - 1; i++) {
      output += ", ".concat(items[i]);
    }

    ;
    output += " and ".concat(items[items.length - 1]);
    return output;
  }

  ;
} //replaces from string spaces, hyphens and upper case letters 

function var_name() {
  var string = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  return string.replace(/\s|-/g, '-').toLowerCase();
}
;
/*
export function combine_strings(items, limit){
if( !Array.isArray(items) ){ //TODO - this should be removed but im not sure if it won't break something
	items = []
	for(let i = 0; i < arguments.length; i++ ) if(arguments[i]) items[items.length]  = arguments[i];
};

items = remove_duplicates(items);
shuffle(items);

if(limit) items = items.slice(0, limit);

return a_b_and_c(items);
}
*/

function abc() {
  var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
  return String.fromCharCode(96 + index);
}

function ABC() {
  var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
  return String.fromCharCode(64 + index);
}

function female_surname(surname, local) {
  var natural = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

  switch (local) {
    case "polish":
    case "pl":
      if (surname.endsWith("ski")) {
        return surname.replace(/ski$/, "ska");
      } else if (surname.endsWith("cki")) {
        return surname.replace(/cki$/, "cka");
      } else if (surname.endsWith("y")) {
        return surname.replace(/y$/, "a");
      } else if (surname.endsWith("ek")) {
        return surname.replace(/ek$/, "kowa");
      } else if (surname.slice(-1) === "a") {
        return surname; //they are usually not changed; all with -ina sound weird (Gajda, Sowa, Sikora)
        //return surname.replace(/a$/,"ina")
      } else if (!vowel(surname.slice(-1))) {
        return "".concat(surname, "owa");
      } else {
        return surname;
      }

    case "slovak":
    case "sk":
    case "czech":
    case "cz":
    case "cs":
      if (surname.endsWith("ek")) {
        return surname.replace(/ek$/, "ková");
      } else if (surname.endsWith("el")) {
        return surname.replace(/el$/, "lová");
      } else if (surname.endsWith("a")) {
        return surname.replace(/a$/, "ová"); //TODO ě
      } else if (surname.endsWith("e")) {
        return surname.replace(/e$/, "ová"); //usually
      } else if (surname.endsWith("í") && natural) {
        return surname;
      } else if (surname.endsWith("o") && natural) {
        //šimko? TODO????
        return surname;
      } else if (surname.endsWith("a")) {
        return surname.replace(/a$/, "ová");
      } else if (surname.endsWith("ý")) {
        return surname.replace(/ý$/, "á");
      } else if (surname.match(/(y|i|oj|yj|ij)$/)) {
        //slavic surnames; // could use their own system Kowalski - Kowalská and Kowalska;  Tolstoj - Tolstá and Tolstaja
        return surname.replace(/(y|i|oj|yj|ij)$/, "á");
      } else {
        return "".concat(surname, "ov\xE1");
      }

    default:
      return surname;
  }
}
;// CONCATENATED MODULE: ./src/object.js
function object_typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { object_typeof = function _typeof(obj) { return typeof obj; }; } else { object_typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return object_typeof(obj); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = object_unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function object_unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return object_arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return object_arrayLikeToArray(o, minLen); }

function object_arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }


/**
		takes an array of keys (array of strings) and the source (object)
		returns new object with specified properties copied from the source 
	*/

function copy_properties() {
  var array = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var source = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var object = {};

  var _iterator = _createForOfIteratorHelper(array),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var key = _step.value;

      if (source[key] !== undefined) {
        object[key] = source[key];
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  return object;
  /*
  //TODO - this should be slower but check it
  return array.reduce( (object,key) => {
  	if(source[key] !== undefined){
  		object[key] = source[key];
  	}
  	return object;
  }, {} );
  */
}
function changed_properties() {
  var original = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var current = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var changed = {};
  Object.entries(current).forEach(function (a) {
    var key = a[0];
    var value = a[1]; //const [key, valute] = a;

    if (current[key] !== original[key]) changed[key] = value;
  });
  /*
  TODO - CHECK
  	Object.keys(current).forEach( a => {
  		if(current[a] !== original[a]) changed[key] = current[a];
  	});
  */

  return changed;
}
function clone(original) {
  if (original === undefined) return undefined; //TODO
  //TODO - COPY THIS OR LODASH https://frontbackend.com/javascript/what-is-the-the-fastest-way-to-deep-clone-an-object-in-javascript

  return JSON.parse(JSON.stringify(original));
}
function unique(array) {
  var input = array.map(function (a) {
    return a;
  });
  var output = [];

  var _iterator2 = _createForOfIteratorHelper(input),
      _step2;

  try {
    var _loop = function _loop() {
      var item = _step2.value;
      if (output.findIndex(function (a) {
        return equals(a, item);
      }) !== -1) return "continue";
      output.push(item);
    };

    for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
      var _ret = _loop();

      if (_ret === "continue") continue;
    }
  } catch (err) {
    _iterator2.e(err);
  } finally {
    _iterator2.f();
  }

  return output;
}
function equals(first, second) {
  //https://stackoverflow.com/questions/7837456/how-to-compare-arrays-in-javascript
  if (typeof first === "function" || typeof second === "function") {
    //I believe this compares whether they both reference the same fuction 
    return first == second;
  }

  ;

  if (first === null || second === null) {
    //null is silly so I rather compere it separately
    return first == second;
  }

  ;

  if (object_typeof(first) === "object" || object_typeof(second) === "object") {
    if (object_typeof(first) !== object_typeof(second)) return false;
    if (Array.isArray(first) !== Array.isArray(second)) return false;
    var fri = Array.isArray(first) ? first : Object.entries(first);
    var sec = Array.isArray(second) ? second : Object.entries(second);
    fri.sort();
    sec.sort();
    if (fri.length != sec.length) return false;

    for (var i = 0, l = fri.length; i < l; i++) {
      if (fri[i] === Object(fri[i])) {
        // recurse into the nested 
        if (!equals(fri[i], sec[i])) return false;
      } else if (fri[i] != sec[i]) {
        return false;
      }
    }

    return true;
  } else {
    //primitive
    return first == second;
  }
}
;// CONCATENATED MODULE: ./src/dom.js
function is_empty(input) {
  return Object.keys(input).length === 0;
}
function remove_children(element) {
  //is string with id - finds the element
  if (typeof element === 'string') {
    var element_id = element;
    element = document.getElementById(element_id);
    if (!element) console.log("Element requested by dh.remove_children with id: " + element_id + " not found!");
  }

  while (element.hasChildNodes()) {
    element.removeChild(element.firstChild);
  }
}
;
function check_image(src, good, bad) {
  if (!src) return good();
  var image = new Image();
  image.src = src;
  image.onerror = bad;
  image.onload = good;
}
;// CONCATENATED MODULE: ./src/geometry.js
function geometry_typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { geometry_typeof = function _typeof(obj) { return typeof obj; }; } else { geometry_typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return geometry_typeof(obj); }

//EVERTHING TODO!!!!!!!

/**
	first finds a point T on the line between points "A" and "B" with distance from "A" "percent"*AB
	then returns point C perpendicular to line AB with distance from the point T "distance"
	
		C
		|
	A---T----B
	   
	perpendicularPoint(
		A - fist point {x,y}
		B - second point {x,y}
		percent - percentage of distance AB (values 0-1)
		distance - perpendicular distance from a point T on line AB and the resulting point C
	)
	returns C
*/
function perpendicular(A, B, percent, distance) {
  var T = findBetween(A, B, percent);
  var angle = cartesian2polar(B, A).theta + Math.PI / 2;
  return polar2cartesian(distance, angle, T);
  /**
  	findBetween(first value,second value, percent)
  	returns value with percent distance between first and second value
  */

  function findBetween(lower, higher) {
    var percent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0.5;

    if (geometry_typeof(higher) === "object" && geometry_typeof(higher.x) !== undefined && geometry_typeof(higher.y) !== undefined && geometry_typeof(lower) === "object" && geometry_typeof(lower.x) !== undefined && geometry_typeof(lower.y) !== undefined) {
      return {
        x: findBetween(lower.x, higher.x, percent),
        y: findBetween(lower.y, higher.y, percent)
      };
    }

    ;
    if (geometry_typeof(higher) == "object") console.log("ERROR! The first variable inputed in findBetween() is not a primitive value (ie is object)! ");
    if (geometry_typeof(lower) == "object") console.log("ERROR! The first variable inputed in findBetween() is not a primitive value (ie is object)! ");
    return lower + (higher - lower) * percent;
  }
}
/**
	transfers point from Cartesian coordinates to Polar
*/

function cartesian2polar(point, origin) {
  if (!origin) origin = {};
  if (!origin.x) origin.x = 0;
  if (!origin.y) origin.y = 0;
  var a = point.x - origin.x;
  var b = point.y - origin.y;
  return {
    r: Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2)),
    theta: Math.atan2(b, a)
  };
}
;
/**
	transfers point from Polar coordinates to Cartesian
	theta is in radians
*/

function polar2cartesian(r, theta, origin) {
  if (!origin) origin = {};
  if (!origin.x) origin.x = 0;
  if (!origin.y) origin.y = 0;
  return {
    x: r * Math.cos(theta) + origin.x,
    y: r * Math.sin(theta) + origin.y
  };
}
; //haversine formula determines the great-circle distance between two points on a sphere given their longitudes and latitudes

function haversine(A, B) {
  var r = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 6371;
  //stolen from: http://www.movable-type.co.uk/scripts/latlong.html	
  //R- Earth's mean radius, km
  var φ1 = A.x * Math.PI / 180; // φ, λ in radians

  var φ2 = B.x * Math.PI / 180;
  var Δφ = (B.x - A.x) * Math.PI / 180;
  var Δλ = (B.y - A.y) * Math.PI / 180;
  var sqhf = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
  var angdis = 2 * Math.atan2(Math.sqrt(sqhf), Math.sqrt(1 - sqhf));
  return r * angdis; //km
}
;// CONCATENATED MODULE: ./index.js










/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop)
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	// module exports must be returned from runtime so entry inlining is disabled
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(336);
/******/ })()
;
});