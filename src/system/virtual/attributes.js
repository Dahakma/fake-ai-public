

//import {BARS} from "./bars";
import {percent, pm} from "Libraries/dh";
import {perks} from "Data/virtual/perks";
//import {att_list, att_sdic} from "./constants";
//import {DISPLAY} from "Gui/gui";
import {allSlots} from "Avatar/index"
//import {death} from "Virtual/death"
import {attributes} from "Data/virtual/attributes";
import {constants} from "Data/constants";
//const {all, derivered, names, main, direct_damage, effect_causing, dyno} = constants.att;


function round(value){
	return Math.round(value * 100) / 100;
}


//TODO ATT CHANGE




	


 


export const ATTRIBUTES = {
//INITIATE
	initiate(subject = PC){
		//effects
		subject.effects = []; //TODO - PROBABLY SHOULD NOT BE HERE
		
		//att 
		subject.att = {};
		Object.keys(attributes).forEach( key => { //SHOULD BE SOLVED VIA CLASS
			Object.defineProperty(subject.att, key, {
				get(){ return ATTRIBUTES.calc(key, subject); },
			});
		});
		
		//dyno
		subject.dyno = {};
		Object.keys(attributes).filter( key => attributes[key].dyno ).forEach( key => subject.dyno[key] = subject.att[key] );
	},
	
	
	
	
//CHANGE DYNO
	change(what, value, subject = PC){
		subject.dyno[what] += value;
		if(subject.dyno[what] > subject.att[what]) subject.dyno[what] = subject.att[what];
		//TODO - NOT SURE ABOUT THIS
		/*
			if removed, has to be checked
			- potions
			- combat potions
			- bars quick potions
			(- rolls)
		*/
	},

//UPGRADE BASE 
	upgrade(what, value, subject = PC){
		subject.base[what] += value;
	},


//ACTION
	action_damage(action, subject = PC){
		const damage = {};
		
//console.error(action);
		Object.keys(attributes).forEach( key => {
			
			if(key === "weapon_total") return; //weapon_total is just theoretical maximal damaged DISPLAYED TO PLAYER, it is NOT USED for any actual calculations
			
			if(attributes[key].combat_damage){
				
//console.log(key);
				//TODO TEST 
				if( action.weapon && key.includes("weapon") && !action.att?.[key] && !action.attx?.[key] ){
					damage[key] = ATTRIBUTES.calc(key, subject, {
						attx: {
							[key]: action.weapon,
						},
					});
					
					if(!damage[key]) delete damage[key]; //TODO - ??????? (BEWARE - DELETE 0.3 ETC.
				}else{
					if(action.att?.[key] || action.attx?.[key] ) damage[key] = ATTRIBUTES.calc(key, subject, action);
				}
				
			}else if(attributes[key].combat_effect){
				//TODO - WEAPONS CAUSE FULL DAMAGE 
				if(action.att?.[key] || action.attx?.[key]) damage[key] = ATTRIBUTES.calc(key, subject, action);
				
			//DYNAMIC (eng, hp) - directly (usually potions
			}else if(attributes[key].dyno){
				if(action.att?.[key]) damage[key] = action.att[key];
			
			}
			
		});

//console.log( ATTRIBUTES.calc(key, subject, action) );

//console.error(damage);
/*		
//console.log(action);
//console.log(subject);
		const weapon = this.weapon_damage(subject, action?.weapon);

//console.log(weapon)	;	
		//console.warn(action);
		const damage = {};
		const dyno = Object.keys(attributes).filter( key => attributes[key].dyno );
		const direct = Object.keys(attributes).filter( key => attributes[key].combat_damage );
		const indirect = Object.keys(attributes).filter( key => attributes[key].combat_effect ); //separated only to have "direct" ones always first when described
		
		//dyno (direct changes to dynamic attributes (e.g. healing))
		dyno.forEach( key => {
			if(action.att?.[key]) damage[key] = action.att[key];
		})
		
		
		//direct damage to HP or ENG
		direct.forEach( key => {
			//with weapon
			if(action?.weapon !== undefined){
				if(weapon[key]) damage[key] = weapon[key];
			//with magic
			}else{
				if(action.att?.[key] || action.attx?.[key]) damage[key] = ATTRIBUTES.calc(key, subject, action);
			}
			if(action.weap?.[key])  damage[key] = ( damage[key] ?? 0 ) + action.weap[key];
		})
		
		//causing effects
		indirect.forEach( key => {
			//with weapon
			if(action?.weapon !== undefined){
				if(weapon?.[key] || action.att?.[key] || action.attx?.[key]) damage[key] = ATTRIBUTES.calc(key, subject, action);
				if(weapon?.[key]) damage[key] += weapon[key];
			//with magic
			}else{
				if(action.att?.[key] || action.attx?.[key]) damage[key] = ATTRIBUTES.calc(key, subject, action);
			}
			//directly action.weapon - TODO WACKY
			//if(action.weap?.[key])  damage[key] = ( damage[key] ?? 0 ) + action.weap[key];
		})
		
		//console.log(damage);
*/
		return damage;
	},

	action_desc(action, subject = PC){
		const damage = this.action_damage(action, subject);
		
		let text = `<strong>${action.name}</strong><br><em>${action.desc}</em><br>`
		Object.keys(damage).forEach( key => {
			text += `${damage[key]}${attributes[key].percent ? "%" : ""} ${attributes[key].name}<br>`;
		})
		
		/*
	//console.error(damage);
		let text = `<strong>${action.name}</strong><br><em>${action.desc}</em><br>`
		
		Object.keys(damage.direct).forEach( key => {
			text += `${damage.direct[key]} ${names[key]}<br>`;
		})
		
		Object.keys(damage.indirect).forEach( key => {
			text += `${percent(damage.indirect[key])} ${names[key]}<br>`;
		})
		*/
		return text;
	},
	
	
//RESITANCE 
	resist_action_damage(damage, subject = PC, critical){
		const keys = Object.keys(damage);
		const actual_damage = {...damage}; //actual damage (damage - resistance)
		keys.forEach( key => {
			actual_damage[key] = this.resistance(actual_damage[key], key, subject, critical);
		});
		return actual_damage;
	},

	resistance(value, key, subject, critical = false){
		if(attributes[key].dyno) return value; //dynamic attributes applied directly without resistance
		if(!attributes[key].counter) return value; //no resistance
		
		let output = value;
		if(attributes[key].counter.attx){
			Object.entries(attributes[key].counter.attx).forEach( ([conkey, mod]) => {
				if(critical && conkey === "armor") return; //critical hits ignore armor
				if(subject.att?.[conkey] === undefined){
					console.error(conkey);
					return;
				}
				output *=  (   100 + (  subject.att[conkey] * mod  )   ) / 100;
			})
		}
		return Math.round(output);
	},
	

//ATTRIBUTES TO DYNO
	damage_to_dyno(damage){
		const actual = {};
		Object.entries(damage).forEach( ([key, value]) => {
			if(attributes[key].dyno){ //already is dynamic - directly 
				actual[key] ??= 0;
				actual[key] += value;
				return;
			}
			const type = attributes[key].combat_damage;
			if(!type) return;
			actual[type] ??= 0;
			actual[type] += -1 * value; //negative because damage
		});
		//hp to eng and such shit
		return actual; 
	},
	
	
	flag(id, subject = PC){
		
		if(!subject) return false;
		
		
//TODO - ONLY EFFECTS 
		if(subject.effects){
			for(const effect of subject.effects){
				if(typeof effect.flags === "string"){
					if(effect.flags === id) return true;
				}
				if( Array.isArray(effect.flags) ){
					 if(effect.flags.includes(id)) return true;
				}
			}
		}
		
		if(subject.perks){
			for(const key of subject.perks){
				const perk = perks[key];
				if(typeof perk.flags === "string"){
					if(perk.flags === id) return true;
				}
				if( Array.isArray(perk.flags) ){
					 if(perk.flags.includes(id)) return true;
				}
			}
		}

		return false;
	},
	
	
//CALCULATION 
	calc(what, subject = PC, additional, ignore_temporal){

		const source = attributes[what];
		if(!source){
			return 0; //TODO
		}
		
		let base = 0;
		let adjust = 0;
		let total = 0;
		let multiply = 1;
		
		function add(effect){
			if( !effect ) return;
			if( effect.condit && !effect.condit(subject) ) return;
			if( effect?.att?.[what] && !isNaN(effect.att[what] ) ){
				adjust += effect.att[what];
			}
			if( effect.attx?.[what] && !isNaN( effect.attx[what] ) ){
				multiply *= effect.attx[what];
			}
		}
		
	//BASE 
		//TODO - UGLY HACK
		if(what === "weapon_damage" && !subject.weapon){
			base = constants.combat.unarmed_damage;			
		//primary (can override derived)
		}else if( subject.base?.[what] !== undefined){
			base = subject.base[what]
		//derived
		}else if( source.derive ){
			base = source.derive.base ?? 0;
			Object.entries( source.derive.att ).forEach( ([key, mod])=> {
				adjust += subject.att[key] * mod;
			});
			
		//default 
		}else{
			base = source.value ?? 0; 
		}

	//ADDITIONAL - directly inputed argument - e.g. combat action
		add(additional);
		
	//PERKS
		if(subject.perks){
			subject.perks.forEach( a => add( perks[a] ) );
		}
		
		
		if(!ignore_temporal){
		//ARTIFACTS
			allSlots.forEach( a => add(subject[a])  );
			
		//EFFECTS
			subject.effects.forEach(add);
		}	
		
			
		
		total = (base + adjust) * multiply;
		if( total < 0 ) total = 0;


		return Math.round(total);
	},


//DESC CALCULATION
	desc(what, subject = PC, comment = "", additional, ignore_temporal){
		const source = attributes[what];
		if(!source){
			return `Error: attribute "${what}" not found!`;
		}
		
		let base = 0;
		let adjust = 0;
		let total = 0;
		let multiply = 1;
		
		let text = ``;
		let text_sum = ``;
		let text_multi = ``;
		

		function basum(vala, name = "base"){
			base = vala;
			adjust = 0;
			text = `${round(vala)} <i>${name}</i><br>`
		}
		function sum(vala, name){
			adjust += vala;
			text_sum = `${text_sum} ${pm(round(vala))} <i>${name}</i><br>`
		}
		function multi(vala, name){
			multiply *= vala;
			text_multi = `${text_multi} × ${round(vala)} <i>${name}</i> <br>`
		}
		
		
		function add(effect){
			if(!effect) return;
			if( effect.condit && !effect.condit(subject) ) return;
			if( effect?.att?.[what] && !isNaN(effect.att[what] ) ){
				sum(effect.att[what], effect.name)
			}
			if( effect.attx?.[what] && !isNaN( effect.attx[what] ) ){
				multi(effect.attx[what], effect.name)
			}
		}
		
	//BASE
		//TODO - UGLY HACK
		if(what === "weapon_damage" && !subject.weapon){
			basum( constants.combat.unarmed_damage, `unarmed`);
		
		
		}else if( subject.base?.[what] !== undefined){
			basum( subject.base[what] ); 
		//derived
		}else if( source.derive ){
			basum( source.derive.base ?? 0 );
			Object.entries( source.derive.att ).forEach( ([key, mod])=> {
				sum(subject.att[key] * mod, `${attributes[key].name} × ${mod}`)
			});
			
		//default 
		}else if(source.value !== undefined){
			basum( source.value ); 
		}
		
		
	//ADDITIONAL (i.e. usually action)
		add(additional);
		
	//PERKS
		if(subject.perks){
			subject.perks.forEach( a => add( perks[a] ) );
		}
	
		if(!ignore_temporal){
		//ARTEFACTS
			allSlots.forEach( a => add(subject[a])  );
			
		//EFFECTS
			subject.effects.forEach( effect => add(effect)  );
		}
		
		total = (base + adjust) * multiply;
		
		if( total < 0 ) total = 0;
		
		if(comment) comment = `<i>${comment}</i><br>`;
		//<strong>${total}</strong> (${base} + ${adjust})<br>
		//<strong>${total}</strong><br>
		return `
			${comment}
			${text}${text_sum}${text_multi}
		`;
	},
	
	
//ITEM
	item_desc(item){
		let text = "";
		if(item?.att){
			Object.keys(item.att).forEach( key => {
				if(text) text += "<br>"
				text += `${attributes[key].name} ${pm(item.att[key])}`
			});
		}
		return text;
	},
	
	
	skill_desc(item){
		let text = "";
		if(!item) return text;
		if(item.desc) text += `<i>${item.desc}</i><br>`;
		if(item.alt) text += `<i>${item.alt}</i><br>`;
		
		let att = {};
		if(item.att) att = {...att, ...item.att};
		let attx = {};
		if(item.attx) attx = {...attx, ...item.attx};
		if(item.weapon) {attx = {...attx, 
			weapon_damage: item.weapon,
			weapon_fire: item.weapon,
			weapon_ice: item.weapon,
		};}

 
		if(Object.entries(att).length){
			Object.entries(att).forEach( ([key, value]) => {
				if(!value) return;
				text += `${attributes[key].name} ${pm(value)}${attributes[key].percent ? "%" : ""}<br>`
			});
		}
		if(Object.entries(attx).length){
			Object.entries(attx).forEach( ([key, value]) => {
				if(!value) return;
				text += `${attributes[key].name} × ${value}<br>`
			});
		}
		if(item.price){
			Object.entries(item.price).forEach( ([key, value]) => {
				text += `${attributes[key].name} ${pm(value)}<br>`;
			});
		}
		if(item.condit) text += `Condition: ${item.condit_desc}<br>`;
		
		return text;
	},
	
	
	
}





 




