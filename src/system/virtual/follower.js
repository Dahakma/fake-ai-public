import {followers} from "Data/npcs/followers"; 
import {effigy, saveWornOutfit, wearOutfit, updateDraw} from "Avatar/index"; 
import {ATTRIBUTES} from "Virtual/attributes";
import {NAME, NPC} from "System/variables";
import {DISPLAY, popup} from "Gui/gui";



export const save = function(){
	if( (ext?.follower?.name ?? ext?.follower?.key) === undefined  ) return; //todo - tempfix for saveing is ext.follower = ext.follower.key
	const {name, key} = ext.follower;

	//save outfit
	saveWornOutfit(`npc_${key}`, `${name}'s outfit`, effigy[key], 0)
	
	//save changed body dimensions & equipment
	effigy.save(ext.follower.key);	
	
}



export const dismiss = function(){
	//if(ext.follower === undefined) return;
	save();
	ext.follower = undefined;
	DISPLAY.check();
}


const foll_att = ["str","dex","char","int","hp","eng","greed","chem"];
const foll_att_def = {
	greed: 2,
	chem: 2,
	hp: 20, //TODO
	eng: 20, //TODO
}


/*
	creates ext.follower by loading values from followers[key] (and possibly loaded from NPC[key])
*/
export const fellowship = function(key){
	if(key === undefined || followers[key] === undefined) return;
	dismiss();
	
	//effigy.reset(key); //TODO?
	
	//TODO - THIS IS extremely BAD! TEMPFIX, WILL PROBABLY FUCK UP EVERYTHING!!
	ext.follower = effigy[key];
	
	ext.follower = Object.assign(ext.follower, {
		key,
		name: NAME[key],
		class: followers[key].class,
		level: followers[key].level,
		att: {},
		base: {},
		sex: NPC[key].sex,
		follower: true,
	});
	foll_att.forEach(a => ext.follower.base[a] = followers[key].base[a] || foll_att_def[a] || 0 );
	
	
	//THE LAST WORN OUTFIT
	//wearOutfit(`npc_${key}`, effigy[key], undefined, true);
	
//console.warn()
	
	
	ext.follower.perks = [...followers[key].perks, ...(NPC[key].perks || []) ];
	ext.follower.skills = [...followers[key].skills, ...(NPC[key].skills || []) ];
		
	//	console.warn(ext.follower);
	//att_recalculate(ext.follower)	
	ATTRIBUTES.initiate(ext.follower); //TODO - stupid
	
//console.warn(ext.outfits[`npc_${key}`])
	wearOutfit(`npc_${key}`, effigy[key] /*ext.follower*/, undefined, true);
	updateDraw(effigy[key] );
	
	ext.follower.dyno = {
		hp: ext.follower.att.hp,
		eng: ext.follower.att.eng,
	}
	
	
	
	/*
	key = key; //TODO - IS NECESSARY? 
		
		follower.att = {};
		follower.att.hp = follower.base.hp; //TODO FUCK THIS TEMPFIX
		follower.att.eng = follower.base.eng; //TODO FUCK THIS TEMPFIX
		follower.att.greed = follower.base.greed; //TODO FUCK THIS TEMPFIX
		follower.att.chem = follower.base.chem; //TODO FUCK THIS TEMPFIX
		
		//TODO PERKS DISABLED
		//Object.keys(follower.base).forEach( a => effigy[key].base[a] = follower.base[a]);
		//effigy[key].perks = [...follower.perks, ...(NPC[key].perks || []) ];
		//effigy[key].skills = [...follower.skills, ...(NPC[key].skills || []) ];
	}
	*/
	DISPLAY.check();
	
	
}