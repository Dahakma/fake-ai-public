import {bold, hint} from "Loop/text";
import {pm} from "Libraries/dh";
import {NAME} from "System/variables";
import {rewards} from "Data/items/prices"; 
import {money} from "Data/items/prices";

/*
self: 0,
cum: [],
sex_m: [0,0,0,0], //empty, irl, meta, game
sex_f: [0,0,0,0], 
oral_s: : [0,0,0,0], 
oral_m: : [0,0,0,0], 
oral_f: : [0,0,0,0], 
*/


//LEGACY; clear references later 
export {
	log_sex,
	bills,
	buttplug,
	dildo,
	dildo_material,
	 spoiler,
	 mas_vic,
	 masochist,
	dyke,
	strapon,
} from "Loop/text_extra";





export function fol_name(){
	return NAME[ext.follower?.key];
}




export function His(male = "His", female = "Her"){
	return ext.follower.sex ? female : male;
}

export function He(male = "He", female = "She"){
	return ext.follower.sex ? female : male;
}

export function his(male = "his", female = "her"){
	return ext.follower.sex ? female : male;
}

export function he(male = "he", female = "she"){
	return ext.follower.sex ? female : male;
}

export function him(male = "him", female = "her"){
	return ext.follower.sex ? female : male;
}

export function We(plural = "We", singular = "I"){
	return ext.follower ? plural : singular;
}
export function we(plural = "we", singular = "I"){
	return ext.follower ? plural : singular;
}


export function wage(sum){
	if(typeof sum === "string") sum = rewards[sum];
	sum = Math.round(sum);
	bold(`${pm(sum)} coins`);
	ext.game_money += sum;
}






export function character_origin(){
	if(ext.class === 1){
		return ["","Camp follower","Urchin","Runaway","Noblewoman"][ext.origin];
	}else if(ext.class === 2){
		return ["","Berserk","Huntress","Shaman","Chief's daughter"][ext.origin];
	}else if(ext.class === 3){
		return ["","Paladin","Hospitaller","Librarian","Herald"][ext.origin];
	}
}
 

export function origin_based(camp, urchin, runaway, princess, berserk, huntress, shaman, chief, paladin, hospitaler, librarian, herald){
	if(ext.class === 1){
		return ["", camp, urchin, runaway, princess][ext.origin];
	}else if(ext.class === 2){
		return ["", berserk, huntress, shaman, chief][ext.origin];
	}else if(ext.class === 3){
		return ["", paladin, hospitaler, librarian, herald][ext.origin];
	}
}


export function class_based(harlot, hellion, fallen, none = ""){
	switch(ext.class){
		case 1: return harlot;
		case 2: return hellion;
		case 3: return fallen;
		default: return none;
	}
}
 