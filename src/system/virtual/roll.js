
import {ra} from "Libraries/random";
import {s, capitalise, pm, unique} from "Libraries/dh";
import {main, alt_link} from "Loop/engine";
import {machina} from "Dungeon/engine";

import {constants} from "Data/constants";
import {ATTRIBUTES} from "Virtual/attributes";
import {attributes} from "Data/virtual/attributes";



export function roll(text, key, level, win, fail, price, price_value, follower_text, follower_win = win, follower_fail = fail){
	if(text) statlink(PC, text, key, level, win, fail, price, price_value);
	if(ext.follower && follower_text) statlink(ext.follower, follower_text, key, level, follower_win, follower_fail, price, price_value)
}


export function auto_roll(text, key, level, win, fail, price, price_value, follower_text, follower_win = win, follower_fail = fail){
	if(text) statlink(PC, text, key, level, win, fail, price, price_value);
	if(ext.follower && follower_text) statlink(ext.follower, follower_text, key, level, follower_win, follower_fail, price, price_value)
}






/*
export function follower_statlink(who, input, key, level, win, fail, price, price_value){
	const who = ext.follower; 
	
	let bonus = 0;
	if(typeof key === "object"){
		bonus = key[1] ?? 0;
		key = key[0];
	}
	
	
	//FINDS DIFFICULTY
	const difficulty = (()=>{
		switch(level){
			default:
			case -1: return "Trivial";
			case 0: return "Very Easy"; 
			case 1: return "Easy"; 
			case 2: return "Medium"; 
			case 3: return "Hard"; 
			case 4: return "Very Hard";
			case 5: return "Extremely Hard"; 
			case 6:	
			case 7:
			case 8: 
			case 9: return "Impossible";
		}
	})();
	
	
	//FINDS CHANCE
	let chance = 50;
	let actual_chance = 50;
	const chanmod = (who.att[key] + bonus) - level;
 
	 if(chanmod<-2){ //0
		chance = 0;
		actual_chance = 0;
	}else if(chanmod==-2){ //10
		chance = 15;
		actual_chance = 20;
	}else if(chanmod==-1){ //30
		chance = 33;
		actual_chance = 40;
	}else if(chanmod==0){ //51
		chance = 51;
		actual_chance = 60;
	}else if(chanmod==1){ //66
		chance = 66;
		actual_chance = 70;
	}else if(chanmod==2){ //85
		chance = 85;
		actual_chance = 90;
	}else if(chanmod==3){ //95
		chance = 95;
		actual_chance = 96;
	}else if(chanmod>3){ //100
		chance = 100;
		actual_chance = 100;
	}
	
	
	//BARS PRICE
	
	let disabled = false;
	let fatal = false;
	let price_desc = "";
	
	if(price ){ // && BARS?.items[key]
		//console.warn( BARS.items)
		//console.log( BARS.items[`${who.key}_${price}`])
		//console.log( BARS.items[`f_${price}`].label)
		//price_desc = BARS.items?.[`f_${price}`]?.label ? BARS.items[`f_${price}`].label : "x"; //TODO
		price_desc = BARS.items?.[`${who.key}_${price}`]?.label ? BARS.items[`${who.key}_${price}`].label : "x"; //TODO
		if(who.att[price] + price_value < 0){
			if(price === "hp"){
				fatal = true;
			}else{
				disabled = true;
			}
		}
	}else{
		//statistic = "";
		//price = "";
	}
	
	
	
	
	//DESCRIPTION 
	const desc = `(${key.toUpperCase()} ${difficulty}) ${input}`;
	let alt = `
		<strong>Chance: ${chance}%</strong>
		<br>Difficulty: ${level}
		<br>${capitalise(att_list[key])}: ${who.att[key]} ${bonus ? pm(bonus) : ""}
	`;
	if(price) alt += `
		<br>Failure: ${price_value} ${price_desc}
	`;
	if(fatal) alt += `
		<br><strong>You will die if you fail!</strong>
	`;
	
	//CREATES LINK
	alt_link(
		desc, //description of the link
		undefined, //has to be undefined to circumvent normal execution 
		alt, //text on mouse hover 
		()=>{}, 
		()=>{
			const resolution = ra.range(0, 100) < actual_chance;
			if(resolution){
				if(ext.dungeon){
					machina(win);
				}else{
					main(win);
				}
			}else{
				if(ext.dungeon){
					machina(fail);
				}else{
					main(fail);
				}
				
				if(price) att_change(price, price_value, ext.follower);
				
			}
		
		}, 
		!disabled,
	);

}
*/

/*
function calculate(who, input, key, level, win, fail, price, price_value){
	let bonus = 0;
	let key2 = "";
	if(typeof key === "object"){
		if(typeof key[1] === "string"){
			key2 = key[1];
		}else{
			bonus = key[1] ?? 0;
		}
		key = key[0];
	}
	
	//FINDS DIFFICULTY
	let difficulty = (()=>{
		switch(level){
			default:
			case -1: return "Trivial";
			case 0: return "Very Easy"; 
			case 1: return "Easy"; 
			case 2: return "Medium"; 
			case 3: return "Hard"; 
			case 4: return "Very Hard";
			case 5: return "Extremely Hard"; 
			case 6:	
			case 7:
			case 8: 
			case 9: return "Impossible";
		}
	})();
	
	
	
	//FINDS CHANCE
	let chance = 50;
	let actual_chance = 50;
	let chanmod = (who.att[key] + bonus) - level;
 
 
	if(key2){
		difficulty *= 2;
		chanmod += who.att[key2];
	}
 
 
	 if(chanmod<-2){ //0
		chance = 0;
		actual_chance = 0;
	}else if(chanmod==-2){ //10
		chance = 15;
		actual_chance = 20;
	}else if(chanmod==-1){ //30
		chance = 33;
		actual_chance = 40;
	}else if(chanmod==0){ //51
		chance = 51;
		actual_chance = 60;
	}else if(chanmod==1){ //66
		chance = 66;
		actual_chance = 70;
	}else if(chanmod==2){ //85
		chance = 85;
		actual_chance = 90;
	}else if(chanmod==3){ //95
		chance = 95;
		actual_chance = 96;
	}else if(chanmod>3){ //100
		chance = 100;
		actual_chance = 100;
	}
	
	
	return{
		key,
		key2,
		difficulty,
		bonus,
		chance,
		actual_chance,
	}
}


export function statlink_auto(who, input, in_key, level, win, fail, price, price_value){
	const {key, key2, difficulty, bonus, chance, actual_chance} = calculate(who, input, in_key, level);
	
	const resolution = ra.range(0, 100) < actual_chance;
	if(resolution){
		
		return true;
	}else{
		
		if(price) ATTRIBUTES.change(price, price_value, who);
		return  false;
	}
	
}
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
export function statlink(subject, input, key, level, win, fail, price, price_value){
	
	let disabled = false;
	let fatal = false;
	let key2 = undefined;
	let subject2 = undefined;
	if(Array.isArray(key)){
		[key, key2] = key;	
	}
	if(Array.isArray(subject)){
		[subject, subject2] = subject;	
	}
	

	
	const desc_price = (()=>{
		if(price){
			if(subject.dyno[price] + price_value < 0){
				if(price === "hp"){
					fatal = true;
				}else{
					disabled = true;
				}
			}
			
			if(subject2 && subject2.dyno[price] + price_value < 0){
				if(price === "hp"){
					fatal = true;
				}else{
					disabled = true;
				}
			}
			
			return `<br>Failure: ${price_value} ${attributes[price].name} ${subject2 ? "each" : ""}`
		}
		return ``;
	})();
	
	
	const desc_difficulty = (()=>{
		switch(level){
			case 0: return "Trivial";
			case 1: return "Very Easy"; 
			case 2: return "Easy"; 
			default:
			case 3: return "Common"; 
			case 4: return "Moderate"; 
			case 5: return "Challenging";
			case 6: return "Hard";
			case 7: return "Very Hard";
			case 8: return "Extremely Hard";
			case 9: return "Impossible";
		}
	})();
	
	const desc_skill = (()=>{
		let temp = "";
		const value = (subject, key) => `${subject.att[key]}${attributes[key].percent ? "%" : ""}`;
		const name = (key) => `${capitalise(attributes[key].name)}`;
		
		if(subject2){
			temp += `<br>${s(subject.name)} ${name(key)}: ${value(subject, key)}`;
			if(key2) temp += `<br>${s(subject.name)} ${name(key2)}: ${value(subject, key2)}`;
			
			temp += `<br>${s(subject2.name)} ${name(key)}: ${value(subject2, key)}`;
			if(key2) temp += `<br>${s(subject2.name)} ${name(key2)}: ${value(subject2, key2)}`;

		}else{
			temp += `<br>${name(key)}: ${value(subject, key)}`;
			if(key2) temp += `<br>${name(key2)}: ${value(subject, key2)}`;
		}
		
		return temp;
	})();
		
	function calculate_skill(subject){
		let temp = subject.att[key];
		if(attributes[key]?.derive?.base) temp -= attributes[key].derive.base; //TODO - BASE IS NOT COUNTED (default 50% chance is basically the base) - TODO counterintuitive
		if(!attributes[key].percent) temp *= 10; 
		
		if(key2){
			let temp2 = subject.att[key2];
			if(attributes[key2]?.derive?.base) temp2 -= attributes[key2].derive.base;
			if(!attributes[key2].percent) temp2 *= 10;
			temp = Math.round( (temp + temp2) / 2 );
		}
		
		temp += 50; //default 50% chance 
		temp -= level * 10; //negative difficulty is added, intentional
		return temp;
	}
	
	const skill = calculate_skill(subject);
/*	const skill2 = (()=>{
		if(subject2 === undefined){
			return 
	})();
*/
	const chance = skill;

	const incapacitated = (()=> {
		if(subject.dyno.hp <= 0){
			disabled = true;
			return `<br><strong>${ext.follower.name} is incapacitated! </strong>`;
		}
		return ``;
	})();
		
	const text2 = key2 ? ` + ${capitalise(attributes[key2].name)}` : "";
	//const text = SETTING.roll_chance ? `<${chance}%> ${input}` : `${input} <small>(${capitalise(attributes[key].name)}${text2}: ${desc_difficulty})</small> `; //&#60; &#62;
	const text = `${input} <small>(${capitalise(attributes[key].name)}${text2}: ${chance}%)</small> `; //&#60; &#62;
	
	const alt = `
		<strong>Chance ${chance}%</strong>
		<br>Difficulty: ${level} (${desc_difficulty})
		${desc_skill}
		${desc_price}
		${incapacitated}
	`;
	
	

	
	//CREATES LINK
	alt_link(
		text, //description of the link
		undefined, //has to be undefined to circumvent normal execution 
		alt, //text on mouse hover 
		()=>{}, 
		()=>{
			const roll = ra.range(0, 100);
			
			if(debug) console.log(`>Diceroll: ${roll} < ${chance} (${roll < chance ? "success" : "fail"})`);
			
			if(roll < chance){
				main(win);
			}else{
				if(price) ATTRIBUTES.change(price, price_value, subject);
				if(PC.dyno.hp < 0){
					ext.dungeon = undefined; //TODO UGLY HACK
					main(101, `death`); //TODO - maybe move to ATTRIBUTES.change
				}else{
					main(fail);
				}				
			}
		
		}, 
		!disabled,
	);

}





