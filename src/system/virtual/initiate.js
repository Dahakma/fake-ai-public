//import {att_list} from "./constants";
import {constants} from "Data/constants";
import {attributes} from "Data/virtual/attributes";

export function initiate_game_character(){ //BEWARE CALLED DRUING CHARACTER CREATION
	ext.game_money = 0; //????
	//ext.level = 0;
	PC.gameInventory = [];
	PC.perks = [];
	PC.sills = [];
	//default
	/*
	PC.att.char = 0;
	PC.att.str = 0;
	PC.att.int = 0;
	PC.att.dex = 0;
	*/
	Object.keys(attributes).forEach( key => {
		//PC.att[a] = 0;
		if(attributes[key].value) PC.base[key] = attributes[key].value;
		
	});
	
	PC.base.char = 1;
	PC.base.str = 1;
	PC.base.int = 1;
	PC.base.dex = 1;
	
	//PC.perks.push("mage");
	PC.perks.push((()=>{
		if(ext.class === 1){
			return ["", "camp", "urchin", "runaway", "princess"][ext.origin];
		}else if(ext.class === 2){
			return ["", "berserk", "huntress", "shaman", "chief"][ext.origin];
		}else if(ext.class === 3){
			return ["", "paladin", "hospitaler", "librarian", "herald"][ext.origin];
		}
	})())
	//class-based 
	if(ext.class === 1){ //harlot
		
		
		PC.base.char++;
		PC.base.char++;
		PC.base.dex++;
		
		
		PC.base.hp = constants.hp.harlot; //TODO
		PC.base.eng = constants.eng.harlot; //TODO
		//ext.alert = 2;
		//ext.power_base = 0;
	}else if(ext.class === 2){ //hellion
		
		PC.base.str++;
		PC.base.str++;
		PC.base.dex++;
		
		PC.base.hp = constants.hp.hellion; //TODO
		PC.base.eng = constants.eng.hellion; //TODO
	//	ext.alert = 0;
	//	ext.power_base = 0;
	}else if(ext.class === 3){ //fallen
		PC.perks.push("mage");
		PC.skills.push("missile");
		
		PC.base.int++;
		PC.base.int++;
		PC.base.char++;
		
		PC.base.hp = constants.hp.fallen; //TODO
		PC.base.eng = constants.eng.fallen; //TODO
	//	ext.alert = 0;
	//	ext.power_base = 2;
	}
	
	/*
		ext.dex++
		ext.dex++
		
		ext.char++
		assasin
	*/
	
	//based on background (origin story)
	/*
	if(ext.origin === 1){
		PC.base.char++;
	}else if(ext.origin === 2){
		PC.base.str++;
	}else if(ext.origin === 3){
		PC.base.int++;
	}else if(ext.origin === 4){
		PC.base.dex++;
	}
		*/
	
	/*
	Object.keys(att_list).forEach( a => { //basically soft att_recalculate()
		PC.att[a] = PC.base[a];
	});
	*/
 
}




















 