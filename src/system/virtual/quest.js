import {success} from "System/sounds";

export const new_act = function(index){
	head.act = index;
	Object.keys(ext.quests).forEach( key => {
		const a = ext.quests[key];
		if(!a.done){
			a.closure = -1;
		}else{
			a.closure = 1;
		}
	})
}

export const quest = {
	initiate(id, title, objective, optional = {}){
		
		ext.quests[id] = {
			title,
			objective,
			done: false,
			closure: 0,
		}
		
		if(optional?.room) ext.quests[id].room = optional.room;
		if(optional?.liason) ext.quests[id].liason = optional.liason;
		
		if(objective === "collect"){
			ext.quests[id].counter = optional.counter ?? 0;
			ext.quests[id].desired = optional.desired ?? 1;
		}
	},
	
	
	collect(id, count = 1){
		if(!ext.quests[id]) return;
		ext.quests[id].counter += count;
		if( ext.quests[id].counter >= ext.quests[id].desired ) quest.done(id);
	},
	
	done(id){
		if(!ext.quests[id] || ext.quests[id].done) return;
		ext.quests[id].done = true;
		success.play();
	},
	
	is_active(id){
		if(!ext.quests[id]) return false; //quest exist
		if(ext.quests[id].closure !== 0) return false; //quest is active 
		return !ext.quests[id].done; //quest is not yet done
	},
	
	is_done(id){
		if(!ext.quests[id]) return false; //quest exist
		if(ext.quests[id].closure !== 0) return false; //quest is active 
		return ext.quests[id].done; //quest is done
	},
	
	is_closed(id){
		if(!ext.quests[id]) return false; //quest exist
		if(ext.quests[id].closure === 1) return true; //quest is succesfully closed
	},
	
	closure(id){
		if(!ext.quests[id]) return;
		ext.quests[id].closure = ext.quests[id].done ? 1 : -1;
	},

}