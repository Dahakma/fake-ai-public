import {main, link} from "Loop/engine";
import {leveling} from "Data/virtual/leveling";
import {constants} from "Data/constants";
import {ATTRIBUTES} from "Virtual/attributes";
import {DISPLAY} from "Gui/gui";
import { writable } from 'svelte/store';

export let list; //object send to level up gui
let fce_back = ()=>{};


export const level_up = function( text, back, force ){
	link(text, null, () => actual_level_up(back, force) );
}

	
export const actual_level_up = function( back, force ){
	
	if(typeof back === "function"){
		fce_back = back;
	}else{
		fce_back = ()=> main(back);
	}
	
	
	if(force){
		ext.level = +force === force ? force : ext.level + 1;
		//Number.isNaN(force) ? ext.level + 1 : force;
		execute();
		return;
	}
	

	//to SECOND LEVEL - after tutorial 
	if(ext.level === 1 && mile.feyd_rats){
		ext.level = 2;
		return execute();
		
	//to THIRD LEVEL - after voyage
	}else if(ext.level === 2){
		if(
			(mile.feyd_yelaya_saved && mile.feyd_roses > 1 && mile.feyd_smuggler > 1) //start of the voyage if player finishe everything in Feyd Mora
			|| mile.goth_boarded //end of the voyage
		){
			ext.level = 3;
			return execute();
		}
	//to FOURTH LEVEL - after karged
	}else if(ext.level === 3){
		let expy = 0;
		
		//both goblins + ghost ship
		if(mile.goth_marshal >= 2 && mile.goth_ghost_ship >= 2) expy++;
		if(mile.goth_dorf) expy++; //datan wood
		if(mile.ship_ydyg >= 4) expy++; //ydyg
		if(mile.ship_wreck >= 3) expy++; //shipwreck
		
		//TODO - expy > 2
		if(expy > 1 || mile.ship_karg >= 3){
			ext.level = 4;
			return execute();
		}
	}
	
	fce_back();
}


function create_list(input){
	const { subscribe, set, update, get } = writable(input);
	
	return {
		subscribe,
		update,
		get, 
			
		learn: (ark, item)  => update(a => {
			a[`select_${ark}`]--;
			a[ark].push(item)
			return a;
		}),

		unlearn: (ark, item)  => update(a => {
			a[`select_${ark}`]++;
			a[ark].splice(a[ark].indexOf(item), 1); 
			return a;
		}),
		
		reset: ()  => update(a => {
			["skills", "spells", "attributes", "perks"].forEach( ark => {
				if( a[`select_${ark}`] === "all" ) return;
				a[`select_${ark}`] += a[ark].length;
				a[ark].length = 0;
			});
			return a;
		}),
		
		
	}
}	
		

const execute = function(){
	const subject = PC;
	const level = ext.level;
	const clas = ext.class;


	const data = {
		skills: [],
		spells: [],
		attributes: [],
		perks: [],
		back: fce_back,
	};

	const source = leveling[clas][level];

	function gather(key){
		let temp = [];
		const actual_key = key === "spells" ? "skills" : key;

		for(let i = level; i > 0; i--){ //also allow skills/perks from all the levels below
			if(leveling?.[clas]?.[i]?.[key]){
				temp = [...temp, ...leveling[clas][i][key] ];
			}
		}

		return temp.filter( a => !subject[actual_key].includes(a) ).reverse(); //removes the ones player already has
	}

	data.hp = constants.hp.level_up; //25
	data.bonus_hp = constants.hp.level_up_bonus * ATTRIBUTES.calc("str", subject, {}, true);
	data.eng = constants.eng.level_up; //25
	data.bonus_eng = constants.eng.level_up_bonus * ATTRIBUTES.calc("int", subject, {}, true);
	
	["skills", "perks", "spells", "attributes"].forEach( key => {
		if( !source[`select_${key}`] ) return;
		data[`select_${key}`] = source[`select_${key}`];
		data[`available_${key}`] = key === "attributes" ? [...(source?.[key] ?? [])] : gather(key);
		if(data[`select_${key}`] === "all") data[key] = data[`available_${key}`]; //learned "all" available
		if(!data[`available_${key}`]?.length) data[`select_${key}`] = 0; //there are no available (will not be displayed)
	});


	list = create_list(data);
	
	DISPLAY.text_clr();
	DISPLAY.text("level_up");
}