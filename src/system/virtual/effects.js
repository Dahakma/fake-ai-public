//object "EFFECT" handling functions related to ".effects"

import {clone} from "Libraries/dh";
import {stock_effects} from "Data/virtual/stock_effects";
import {constants} from "Data/constants";
import {ATTRIBUTES} from "Virtual/attributes";

export const EFFECTS = {
	cancel_combat(subject = PC){
		if(!subject?.effects?.length) return;
		for(let i = 0; i < subject.effects.length; i++){
			if(subject.effects[i].combat){ 
				subject.effects.splice(i, 1);
				i--;
			}
		}
	},
	
	/*
	combat_tick(subject = PC, apply_damage){
		if(!subject?.effects?.length) return;
		for(let i = 0; i < subject.effects.length; i++){
			const eff = subject.effects[i];
			
			//deliver effect
			const actual_damage = ATTRIBUTES.resist_action_damage(eff, subject);
			const dyno_damage = ATTRIBUTES.damage_to_dyno(actual_damage);
			
			//countdown/cancel effect 
			subject.effects[i].ticks--;
			if(subject.effects[i].ticks <= 0){ 
				subject.effects.splice(i, 1);
				i--;
			}
		}
	},
	*/
	
	//decrease "ticks" of all "subject" effects and removes them if "ticks < 0"
	tick(subject = PC){
		if(!subject?.effects?.length) return;
		for(let i = 0; i < subject.effects.length; i++){
			subject.effects[i].ticks--;
			if(subject.effects[i].ticks <= 0){ 
				subject.effects.splice(i, 1);
				i--;
			}
		}
	},
	
	//check if the "subject" has effect with "id"
	check(id, subject = PC){
		return subject.effects.find( a => a.id === id  );
	},
	
	has(id, subject = PC){
		return subject.effects.findIndex( a => a.id === id  ) !== -1;
	},
	
	//addd new "effect" to "subject" (either string key of "stock_effects" or object)
	add(effect, subject = PC){
		
//TEMPFIX TODO
if(effect === "cure"){
	this.remove("poisoned", subject);
	return stock_effects.cure; //TODO
}
		
		if(typeof effect === "string"){
			const id = effect;
			effect = clone(stock_effects[id]);
			effect.id = id;
		}
		if(!effect) return;
		
		effect.ticks ??= constants.ticks; //TODO TEMPFIX
		
		const index = subject.effects.findIndex( a => a.id === effect.id);
		if(index === -1){
			subject.effects.push(effect);
			return subject.effects[subject.effects.length - 1];
		}else{
			//TODO - shoud they be stacked??
			//subject.effects[index].ticks += effect.ticks;
			subject.effects[index].ticks = effect.ticks; //NOT STACKED, RENEWED
			return subject.effects[index];
		}
	},
	
	//removes subjects effect with "id"
	remove(id, subject = PC){
		const index = subject.effects.findIndex( a => a.id === id );
		const temp = subject.effects[index];
		subject.effects.splice(index, 1);
		return temp;
	},
	
}


 


