import {ra} from "Libraries/random";

export function stupid_names(number, database = 1){
		const array = [];
		for(let f = 100; f && array.length < number; f--){
			const temp = stupid_name_generator(database);
			if(!array.includes(temp)) array.push(temp);
		}
		return array;	
	}


function stupid_name_generator(input){
		let letters;
		switch(input){
			default:
			case 1:
				letters =  [
					["C","K"],["a","e"],["e","a"],["v","w"],["y","i"],["r","n"],[" "],
					["M"],["a","e"],["e","a"],["r","rr"],["v","w"],["y","i"],["r","rr",""],["c","k"],
				];
		}
		return letters.map( a => ra.array(a) ).join("");
}
