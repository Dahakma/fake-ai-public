import {POTIONS} from "Avatar/game/potions";
import {INVENTORY} from "Avatar/game/inventory";
import {elixirs} from "Data/items/elixirs/index";
import {stock_effects} from "Data/virtual/stock_effects";
import {attributes} from "Data/virtual/attributes";
import {ATTRIBUTES} from "Virtual/attributes";
import {EFFECTS} from "Virtual/effects";

import {and} from "Libraries/dh";
import {main, alt_link, link} from "Loop/engine";


export function boost(...atts){

	const list = Object.keys(elixirs).filter( key => INVENTORY.has(key) && ext.potions[key]); //has in inventory & knows the effect
	const pc = {};
	const follower = {};
	list.forEach( a => {
		pc[a] = [];
		follower[a] = [];
	});

	const hp = atts.includes("hp") && list.includes("xHP");
	const eng = atts.includes("eng") && list.includes("xEng");
	
	atts = atts.filter( att => att !== "hp" && att !== "eng ");

	function add(key, effect, att){
		if( !EFFECTS.has(effect, PC) ) pc[key].push( attributes[att].name );
		if(ext.follower?.effects && !EFFECTS.has(effect, ext.follower) ) follower[key].push( attributes[att].name );
	}
	
	list.forEach( key => {
		const effect = elixirs[key]?.effect;
		const affected = Object.keys( stock_effects[effect]?.att ?? [] ) ; //effect assigned to the potions

		if(affected){
			atts.forEach( att => { //attribute is based on other attributes
				if( attributes[att]?.derive?.att ){ 
					Object.keys( attributes[att].derive.att ).forEach( a => {
						if( affected.includes(a) ){
							add(key, effect, att); //"att" not "a" - name of the derivered attribute
						}
					})
				}else{
					if( affected.includes(att) ){
						add(key, effect, att);
					}
				}
			})
		}
	});

	Object.keys(pc).forEach( key => {
		if(pc[key].length){
			const boosted_att = and(   [  ...( new Set(pc[key]) )  ]   );
			link(`Drink ${POTIONS.name(key)} to boost ${boosted_att}. `, undefined, ()=> INVENTORY.drink(key, PC) );
		}
		if(follower[key].length){
			const boosted_att = and(   [  ...( new Set(follower[key]) )  ]   );
			link(`Let @fol to drink ${POTIONS.name(key)} to boost ${boosted_att}. `, undefined, ()=> INVENTORY.drink(key, ext.follower) );
		}
	})

	if(hp){
		link(`Drink ${POTIONS.name("xHP")}. `, undefined, ()=> INVENTORY.drink("xHP", PC) );
		if(ext.follower) link(`Let @fol to drink ${POTIONS.name("xHP")}. `, undefined, ()=> INVENTORY.drink("xHP", ext.follower) );
	}
		
	if(eng){
		link(`Drink ${POTIONS.name("xEng")}. `, undefined, ()=> INVENTORY.drink("xEng", PC) );
		if(ext.follower) link(`Let @fol to drink ${POTIONS.name("xEng")}. `, undefined, ()=> INVENTORY.drink("xEng", ext.follower) );
	}	
		
}
