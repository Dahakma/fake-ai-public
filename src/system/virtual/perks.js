//TODO - PERKS ARE NOT USED - REMOVE???

//import {att_list} from "Virtual/index";
import {capitalise, pm} from "Libraries/dh";
import {attributes} from "Data/virtual/attributes";


function perk_desc(what){ //TODO - SHOULD NOT BE HERE
	//if(!perks[id]) return;
	let text = "";
	if(what.att){
		Object.keys(what.att).forEach( key => {
			if(text) text += "<br>"
			text += `${capitalise(attributes[key].names)} ${pm(what.att[key])}`
		});
	}
	return text;
}

export const perks = {
	infeb: {
		name: `infernal beauty`,
		att: {char: 1},
	},
	grace: {
		name: `cat-like grace`,
		att: {dex: 1},
	},
	forti: {
		name: `bovine fortitude`,
		att: {str: 1},
	},
	cunin: {
		name: `mischievous cunningness`,
		att: {int: 1},
	},
	agili: {
		name: `elvish agility`,
		att: {dex: 1},
	},
	testr: {
		name: `test`,
		att: {str: 11},
	},
}



Object.keys(perks).forEach( a => {	
	Object.defineProperty(perks[a], "desc", {
		get(){ return perk_desc(perks[a]) },
	});
})