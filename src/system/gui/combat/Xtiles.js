
import {rg} from "Libraries/random";
import {reset} from "./combat";
function calc_initiative(emy){
	return rg.range(emy.att.reflex * 0.5, emy.att.reflex * 1.5);
}




//export const tiles = [];
export let xxx;
export let yyy;

class Tile {
	constructor(x, y, typ = "empty") {
		this.x = x;
		this.y = y;
		this.typ = typ;
	}
	
	fill(fighter = {}, typ = "enemy"){
		this.typ = typ;
		this.fighter = fighter;
	}
	
	get coordinates(){
		return {
			x: this.x,
			y: this.y,
		};
	}
	
	get is_empty(){
		return this.typ === "empty";
	}
	
	clear(){
		this.typ = "empty";
		this.fighter = {};
	}
}

export function initiate_tiles(enemies){
	const tiles = [];
	
	//adjusts number of tiles to the number of enemies
	xxx = ext.follower ? 5 : 3; //HARD
	yyy = (()=>{
		let yyy = 1 + Math.ceil(enemies.length / xxx);
		if(yyy > 4) yyy = 4;
		return yyy;
	})();
	
	//creates empty tiles
	for(let x = 0; x < xxx; x++){
		tiles[x] = [];
		for(let y = 0; y < yyy; y++){
			//in the first line, odd columns are heroes and even columns are spacers
			if( y === 0 &&  !(x % 2) ){ 
				tiles[x][y] = new Tile(x, y, "spacer");
				continue;
			}
			
			tiles[x][y] = new Tile(x, y, "empty");
			
			/*
			if( y === 3 && [1, 3].includes(x) ){ //last combat line //HARD
				tiles[x][y] = undefined; 
				continue;
			}
			tiles[x][y] = null;
			*/
		}
	}
	
	//places PC and follower on tiles
	tiles[1][0].fill(PC, "ally");
	if(ext.follower) tiles[3][0].fill(ext.follower, "ally"); 
	
	return tiles;
}




export function shake_down(tiles, enemies, lol){
	let tampered = false;
	const center = xxx / 2;
	
	function calc_movement_bias(a){
		if(a.x < center) return -1;
		if(a.x > center) return 1;
		return 0;
	}
	
	
	
	function teleport(emy, x, y){ //absolutely
		console.log(`teleport - ${emy.name} to ${x}; ${y}`);
		if(tiles?.[emy?.x]?.[emy?.y]) tiles[emy.x][emy.y].clear();
		emy.x = x;
		emy.y = y;
		tiles[emy.x][emy.y].fill(emy);
		tampered = true;
	}
	
	function move(emy, x, y){ //relativelly
		console.log(`movement - ${emy.name} by ${x}; ${y}`);
		tiles[emy.x][emy.y].clear();
		emy.x += x;
		emy.y += y;
		tiles[emy.x][emy.y].fill(emy);
		tampered = true;
	}


	function spawn(){
		//TODO
		let spawn_points = [{x: 0, y: yyy-1}, {x: 2, y: yyy-1}] //HARD
		if(ext.follower) spawn_points.push({x: 4, y: yyy-1}); //HARD

console.log(spawn_points);		
		spawn_points = spawn_points.filter( a => {
			console.log(a);
			console.log(tiles[a.x][a.y]);
			console.log(tiles[a.x][a.y].is_empty);
			return tiles[a.x][a.y].is_empty 
		
		}); //only unblocked empty spawn points
		rg.shuffle(spawn_points);
console.warn(spawn_points);		

		const unspawned_indexes = enemies.map( (a,i) => i ).filter( i => enemies[i].x === undefined ); //indexes of unspawned enemies
		
		spawn_points.forEach( point => {
			if(!unspawned_indexes.length) return;
			const index = unspawned_indexes.pop();
			teleport( enemies[index], point.x, point.y );
		})
	}
	
	function advance(){
console.log(enemies);
		const pawns = enemies.map( (a,index) => {return {
				index, 
				initiative: calc_initiative(a),
		}} );
		pawns.sort( (a, b) => a.initiative - b.initiative ); //speed of movement based on initiative


console.log(pawns);

		pawns.forEach( a => {
			const emy = enemies[a.index];
			const {x, y} = emy;
			const bias = calc_movement_bias(emy);	

			if(x % 2){ //the ones more away (1,2)
				if( tiles[x]?.[y - 1].is_empty) return move(emy, 0, -1); //forward
				if(bias && tiles[x + bias]?.[y].is_empty) return move(emy, bias, 0); //side (wants)
				if(bias && tiles[x - bias]?.[y].is_empty) return move(emy, -bias, 0); //side (other blocked)
			}else{ //the ones more closer (0,2,4)
				if( tiles[x]?.[y - 1].is_empty) return move(emy, 0, -1); //forward
				if(bias && tiles[x + bias]?.[y - 1].is_empty) return move(emy, bias, -1); //side (wants)
				if(bias && tiles[x - bias]?.[y - 1].is_empty) return move(emy, -bias, -1); //side (other blocked)
			}
		});
	}



	
	do{

		console.log("=============================================");
		tampered = false;
		advance();
		spawn();
	}while(false /*tampered*/);	

	reset.set(true);
}











	
	


