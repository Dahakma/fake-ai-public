/*
	TODO enemy advancement (easy)

*/

import {get} from 'svelte/store';
import {initiate_combat} from "./initiate";
import {shake_down} from "./movement";
import {deliver, deliver_start_round_effects} from "./deliver";
import {constants} from "Data/constants"

import {attributes} from "Data/virtual/attributes"
import {and} from "Libraries/dh";
import {rg, ra} from "Libraries/random";
import {DISPLAY} from "Gui/gui";
import {EFFECTS} from "Virtual/effects";
import {main} from "Loop/engine";
//export const reset = writable(false);
import {rose_on, rose_off} from "Dungeon/rose";

export let pawns;
export let tiles;
export let status;
export let log;

export let xxx; 
export let yyy;



import {ATTRIBUTES} from "Virtual/attributes";

export function start(enemies, location){
	rose_off();
//PLACEHOLDER 
/*	console.warn("started");
	const wala = [ "ogre", [1,"goblin"]];
	PC.name = "KATx";
	PC.weapon = {
		name: "Katana",
		 
		
	};
	PC.base = {
		hp: 250,
		eng: 100,
		reflex: 80,
		evasion: 41,
		pyro: 2,
		crit: 50,
	};
	PC.dyno.hp = 250;
	ATTRIBUTES.initiate(PC);
*/
	 
	
	//console.error(PC);
	//ext.follower = {name: "yoy"}
	/*
	console.error(PC);
	console.error(PC.dyno);
	console.error(PC.dyno.hp);
	*/
//COMBAT INITIATED 
	({pawns, tiles, xxx, yyy, status, log} = initiate_combat(enemies, Math.random() ));
	next_round();
	DISPLAY.combat();
}
export const combat = start;

export function end(victory = true){
	rose_on();
	EFFECTS.cancel_combat(); //TODO
	DISPLAY.text("");
	
	if(!victory){
		ext.dungeon = undefined; //TODO UGLY HACK
		main(101, "death");
	}else{
		//??
	}
}
	
	
	
export function next_round(){

	//console.warn("next round----------")
	
	shake_down();
//shake_down();
	//status
	
	//log
	log.em(`Round ${get(status).round_index}`);
pawns.round_reset();
	deliver_start_round_effects(pawns, tiles, status, log);
	status.next_round(pawns);
	
	//actions
	next_action();
	//const a = get(pawns);
	//log.attack(a[0], a[2], 40);
}


export function next_action(){
//console.warn("next action---------")

	
	status.next_pawn(pawns); //action of the next character
	const gst = get(status);
	if(gst.trigger_combat_end !== null) return end(gst.trigger_combat_end);
	if(gst.trigger_next_round) return next_round(); //no more actions in this round
	if(SETTING.combat_ai && gst.trigger_ai_action) AI(); //ai performs this action
}


export function mark(coordinates){
	status.hover(coordinates);
	
	// = get(pawns);
	//log.attack(a[0], a[2], 40);
}

export function execute(){
	status.execute();
	deliver(pawns, tiles, status, log);
	//pawns.hp(0, -5);
	setTimeout(next_action, constants.combat.execution_delay);
	
	//constants.combat.delay
	// = get(pawns);
	//log.attack(a[0], a[2], 40);
}

/*
function poster(pawn, text){
	
}

function roll(subject){
	return rg.percent; //
	
};
*/


function simp(){
	//console.error("simp")
	const board = get(tiles);
	const origin = get(pawns)[ get(status).pawn_index ];
	const available = [];
	
	[{x: -1, y: -1}, {x: 0, y: -1}, {x: 1, y: -1}].forEach( a => {
		const x = origin.x + a.x;
		const y = origin.y + a.y;
		//if( board[ origin.x + a.x ]?.[ origin.y + a.y ]?.pawn.ally ){
			if( board[ origin.x + a.x ]?.[ origin.y + a.y ]?.pawn?.ally ){
			const target = board[x][y].pawn;
			const presence = target.att.presence;
//console.warn(target.att.presence);

			available.push({
				x, y,
				weight: rg.range( presence * (1 - constants.combat.presence_spread), presence * (1 + constants.combat.presence_spread) ),
			})
		}
	})
/*	
console.error(available);
{
	console.log( available.map( a => a.weight ) );
}

{
	
	console.log(index);
}
*/
/*
{
	const index = ra.weighted( available.map( a => a.weight ) );
	console.log(index);
}
*/
//console.error("---------");


	//console.log(available);
	if(available.length){
/*		available.sort( (a,b) => a.weight + b.weight );
console.warn( available );		
console.warn( available.sort( (a,b) => a.weight + b.weight ) );
console.warn( available.sort( (a,b) => a.weight - b.weight ) );
console.warn( available );
*/
		const index = ra.weighted( available.map( a => a.weight ) );
		const {x, y} = available[index];
		setTimeout(()=>{
			mark({x,y});
			execute();
		}, constants.combat.ai_delay);
	}else{
		log.add(`<b>${origin.name}</b> waited. `);
		//setTimeout(next_action, constants.combat.ai_delay); 
		next_action();//executes nothing
	}
}


export function AI(){
	simp();
	//status.execute();
	//setTimeout(next_action, constants.combat.delay);
	
	//constants.combat.delay
	// = get(pawns);
	//log.attack(a[0], a[2], 40);
}



/*
	enemies {array of strings or objects} //["ogre", [5,"goblin"]]

*/
 



/*
function calc_action(subject, weapon, attack){


}
*/


















