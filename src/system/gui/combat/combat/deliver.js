import {rg} from "Libraries/random";
import {get} from 'svelte/store';
import {attributes} from "Data/virtual/attributes"
import {stock_effects} from "Data/virtual/stock_effects"
import {ATTRIBUTES} from "Virtual/attributes";
import {EFFECTS} from "Virtual/effects";
import {and, pm, capitalise} from "Libraries/dh";


let deg = "";
let tex = "";
const lgx = {

	//intro
	intro(attacker, action, action_index){
		deg = "";
		//attacker
		tex = `❖<b>${attacker.name}</b> `;
		
		//used action
		if(action_index === 0){
			tex += `attacked `;
		}else{
			//TODO
			tex += `used ${action.name} against `;
		}
	},
	
	//missed
	missed(attacker){
		tex = `<b>${attacker.name}</b> missed. `;
	},
	
	//targets
	targeting(targets){
		tex += `<b>${   and(  targets.map( a => a.name )  )   }</b>`;
		//if(targets.length > 1) 
		tex += `. `;
	},
	
	//next enemy in loop
	next(pawn){
		deg += `<b>${pawn.name}:</b><br>`;
	},
	
	//roll
	roll(title, skill, dice, out){
		deg += `${title}: ${skill} > ${dice} ${out ? "✔️" : "❌"}<br>`;
	},
	
	
	effect(title, original, actual, dice, out){
		deg += `${title}: ${original} => ${actual} > ${dice} ${out ? "✔️" : "❌"}<br>`;
	},
	
	
	
	critical(defender){
		tex += `<b>${capitalise(attributes.critical.name)}!</b> `;
		
	},
	
	evasion(defender){
		tex += `${defender.name} evaded the attack. `;

	},
	
	
	damage(defender, original, actual, total){
		Object.keys(original).forEach( key => {
			if(attributes[key].combat_damage){
				deg += `${attributes[key].name}: ${original[key]} => ${actual[key]}<br>`;
			}
		});
		Object.keys(total).forEach( key => {
			deg += `${attributes[key].name}: ${defender.dyno[key]} <b>${pm(total[key])}</b> = ${defender.dyno[key]+total[key]}<br>`;
		});
		
		const result = [];
		if(total.hp <= 0){
			result.push(`suffered <b>${total.hp}</b> damage`);
		}else if(total.hp > 0){
			result.push(`healed <b>${total.hp}</b> hp`);
		}
		//TODO ENG & LUST
		tex += `${defender.name} ${and(result)}. `;
	},
	
	
};

function roll(who, what, additional){
	const dice = rg.percent;
	const skill = ATTRIBUTES.calc(what, who, additional); //  who.att[what];
	const out = skill > dice;
	lgx.roll(attributes[what].name, skill, dice, out);
	return out;
}

/*
function roll_effect(chance, what){
	const dice = rg.percent;
	const skill = who.att[what];
	const out = skill > dice;
	lgx.roll(attributes[what].name, skill, dice, out);
	return out;
}
*/


export function deliver_start_round_effects(pawns, tiles, status, log){
	//console.log("deliver_start_round_effects");
	get(pawns).forEach( defender => {
		//console.log(defender);
		if(defender.effects){
			defender.effects.forEach( eff => {
				if(!eff.att) return;
				//if(!eff.combat) return;
				deg = "";
				tex = "";
				//console.log(eff);
				
				const damage = eff.att;
				const actual_damage = ATTRIBUTES.resist_action_damage(damage, defender);
				const dyno_damage = ATTRIBUTES.damage_to_dyno(actual_damage);
				lgx.damage(defender, damage, actual_damage, dyno_damage);
				//teg = 
				Object.entries(dyno_damage).forEach( ([key,value]) => pawns[key](defender, value) ); //TODO - THIS IS HARDCODED
	//console.error(defender)
	//console.error(dyno_damage.hp)
				if(dyno_damage.hp) log.health(defender, dyno_damage.hp); //TODO - HARDCODED, MADE DYNAMIC
		
		//	deg += `<b>${pawn.name}:</b><br>`;
				//if(dyno_damage.hp) log.health(defender, dyno_damage.hp); //TODO - HARDCODED, MADE DYNAMIC
				
				tex = `${defender.name} is ${eff.name}`;
				
				log.add(tex, deg);
		
			});
			EFFECTS.tick(defender);
		}
	})
	
}


export function deliver(pawns, tiles, status, log){
	const paw = get(pawns);
	const board = get(tiles);
	const {action_index, pawn_index} = get(status);
	
	const attacker = paw[pawn_index];
	const action = attacker.actions[action_index];
	const damage = action?.damage;
	
	const skill = action?.skill;
	let targets = [];
	
	if(!damage) return; //TODO!!! needlesly redundant?
	
	
	deg = "";
	tex = "";
	
	
	//TODO - HORRIBLE, JUST FOR AI, TODO
	if( ATTRIBUTES.flag("pass", attacker) ){
		log.add(`❖<b>${attacker.name}</b> is stunned. `);
		//lgx.stunned(attacker); //TODO NOT ONLY STUN
		return;
	}
	
	
	
	lgx.intro(attacker, action, action_index);

	//TARGETS
	board.forEach( x => x.forEach( y => { //pawns on market tiles
		if(y.marked) targets.push(y.pawn);
	} ) );
	targets = targets.filter( a => a ).filter( a => a.typ !== "spacer"); //remove obstacles from the array //TODO
	
	

	
	if(!targets.length){ //EXIT - NO TARGET
		lgx.missed(attacker);
		return;
	}else{
		lgx.targeting(targets);
	}
	
	//console.error(targets)
	
	targets.forEach( (defender,i,targets) => {
		//console.warn(defender)
		lgx.next(defender);
/*
console.log(attacker)	
console.log(defender)
*/
		//CRITICAL
		const critical = (()=>{
			if(ATTRIBUTES.flag("autocrit", defender)) return true;
			return roll(attacker, "critical", skill); //TODO - divide calculating and writting down
		})();
		
//console.log(attacker.name)
//console.log(critical)
		if(attacker.ally === defender.ally){
			//help to ally - does not reall count
		}else if(critical){
			defender.evasive_actions--;
			lgx.critical(defender);
			log.critical(defender); //TODO
			
			
		}else if(defender.evasive_actions > 0){ //
			defender.evasive_actions--;
			//EVASION
			const evasion = roll(defender, "evasion");
			if(evasion){
				lgx.evasion(defender);
				log.evaded(defender); //TODO
				return; //CONTINUE - EVADED
			}
		}
		
		
//console.log("x")
		
//console.log("xx")		
//console.error(damage)
//console.warn("damage");
	//	console.warn(damage);
		
		const actual_damage = ATTRIBUTES.resist_action_damage(damage, defender, critical);
//console.log(actual_damage)


		//DYNO DAMAGE
//console.warn("actual_damage");
//		console.warn(actual_damage);
		
		const dyno_damage = ATTRIBUTES.damage_to_dyno(actual_damage, defender);
//console.log("xxx")
//console.log(dyno_damage)
		lgx.damage(defender, damage, actual_damage, dyno_damage);
		Object.entries(dyno_damage).forEach( ([key,value]) => pawns[key](defender, value) ); //TODO - THIS IS HARDCODED
		
//console.warn("actual_damage");
//console.warn(actual_damage);		
		if(dyno_damage.hp) log.health(defender, dyno_damage.hp); //TODO - HARDCODED, MADE DYNAMIC
		
		
		
		
		//DAMAGE EFFECTS 
		function handle_effects(original, actual, subject){
			const possible_effects = Object.keys(actual).filter( key => attributes[key].combat_effect );
			possible_effects.forEach( key => {
//console.log(key);
				const dice = rg.percent;
				const out = actual[key] > dice;
//console.log(out);
				lgx.effect(attributes[key].name, original[key], actual[key], dice, out);
				if(out){
	
//	console.error(attributes[key].combat_effect);
					const temp = EFFECTS.add(attributes[key].combat_effect, subject);
					tex += `${defender.name} ${temp.combat_desc}. `;
					
					//log.effect(subject, "xxxxxx" );
					log.effect(subject, capitalise(temp.name) );
					//TODO
				}
			});
		}
		handle_effects(damage, actual_damage, defender);
		
		
		//SKILL EFFECTS 
		if(action.effects?.length){
			action.effects.forEach( ([chance, key]) => {
				const dice = rg.percent;
				const out = chance > dice;
				
				lgx.effect(stock_effects[key].name, chance, chance, dice, out);
				
				if(out){
					const temp = EFFECTS.add(key, defender);
					tex += `${defender.name} ${temp.combat_desc}. `;
					log.effect(defender, capitalise(temp.name) );
				}
			});
		}
		
		
//console.log("xxxxx")				
		//poster(defender, "olol");
		//poster(defender, "text");
	})
	
	
	
	//DEPLETE (inventory)
	if(action.deplete) action.deplete();
	
	//PRICE
	//TODO TEMPFIX
	if(action?.price?.eng){
		pawns.eng(attacker, action.price.eng)
	}
		
	
	log.add(tex, deg);
}	



		
		
		