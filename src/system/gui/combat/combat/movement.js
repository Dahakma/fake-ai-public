import {tiles, pawns, xxx, yyy} from "./combat";
import {rg} from "Libraries/random";
import {get} from 'svelte/store';
import {constants} from "Data/constants";

export function calc_initiative(emy){ //TODO!!!!!!!!!
	const reflex = emy?.att?.reflex;
	if(!reflex) return 0;
	return rg.range( reflex * (1 - constants.combat.reflex_spread), reflex * (1 + constants.combat.reflex_spread) );
	//return rg.range(emy.att.reflex * 0.5, emy.att.reflex * 1.5);
}


export function shake_down(){
	let board = get(tiles);
	let enemies = get(pawns);
	
	let tampered = false;
	const center = Math.floor(xxx / 2);
	
	/*
		TODO 
		- base bias on att.presence
		- allow to go back? 
		- random erratic movement when the pawn can't advance - good or bad?
	*/
	
	
	function calc_movement_bias(a){
		if(a.x < center) return -1;
		if(a.x > center) return 1;
		return rg.b ? 1 : -1;
	}
	
	function teleport(index, x, y){ //absolutely
		pawns.position(index, x, y);
		tampered = true;
		enemies = get(pawns);
		board = get(tiles);
	}



	function move(index, x, y){ //relativelly
		pawns.position(
			index,
			enemies[index].x + x,
			enemies[index].y + y,
		);
		tampered = true;
		enemies = get(pawns);
		board = get(tiles);
	}


	
	
	function advance(){
		//indexes of spawned enemy pawn which will move in order based on their initiative
		const indexes = enemies.map( (a,index) => {return {
				index, 
				initiative: calc_initiative(a),
				active: a.typ === "enemy" && a.x !== undefined && a.y !== undefined, //enemy & spawned
		}} )
		.filter( a => a.active )
		.sort( (a, b) => a.initiative - b.initiative )
		.map( a => a.index ); //speed of movement based on initiative

		indexes.forEach( i => {
			const {x, y} = enemies[i];
			const bias = calc_movement_bias(enemies[i]);	
				
			//first line drift 
			if(y === 1){
				
				if(ext.follower){
					if(x === 0){
						if(/*bias &&*/ board[1][1].pawn === null) return move(i, 1, 0);
					}
					if(x === 3){
						if(/*bias && */board[2][1].pawn === null) return move(i, -1, 0);
					}	
				//TODO!!! - BIAS
				}else{
					if(x === 0){
						if(/*bias && */board[1][1].pawn === null) return move(i, 1, 0);
					}
					if(x === 2){
						if(/*bias && */board[1][1].pawn === null) return move(i, -1, 0);
					}
				}
				
				return;
			}
			
			if(x % 2){ //the ones more away (1,2)
				if( board[x]?.[y - 1]?.pawn === null) return move(i, 0, -1); //forward
				if(bias && board[x + bias]?.[y]?.pawn === null) return move(i, bias, 0); //side (wants)
				if(bias && board[x - bias]?.[y]?.pawn === null) return move(i, -bias, 0); //side (other blocked)
			}else{ //the ones more closer (0,2,4)
				if( board[x]?.[y - 1]?.pawn === null) return move(i, 0, -1); //forward
				if(bias && board[x + bias]?.[y - 1]?.pawn === null) return move(i, bias, -1); //forward side (wants)
				if(bias && board[x - bias]?.[y - 1]?.pawn === null) return move(i, -bias, -1); //forward side (other blocked)
			}
		});
		
	}
	
	
	function spawn(){
		//TODO
		let spawn_points = [{x: 1, y: yyy - 1}] //HARD
		if(ext.follower) spawn_points.push({x: 3, y: yyy - 1}); //HARD
	
		spawn_points = spawn_points.filter( a => board[a.x][a.y].pawn === null );
		spawn_points = rg.shuffle(spawn_points); //TODO!!

		const unspawned_indexes = enemies.map( (a,i) => i ).filter( i => enemies[i].x === undefined && enemies[i].y === undefined ).reverse(); //indexes of unspawned enemies

		spawn_points.forEach( point => {
			if(!unspawned_indexes.length) return;
			const index = unspawned_indexes.pop();
			teleport(index, point.x, point.y);
		})
		
	}
	
	let failsafe = 0;
	do{
		//console.log("=============================================");
		tampered = false;
		advance();
		spawn();
		if(failsafe++ > 100) break;
	}while(tampered);	//tampered

}