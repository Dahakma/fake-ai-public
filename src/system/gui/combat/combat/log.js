import { writable, derived, get } from 'svelte/store';
import {pm} from "Libraries/dh";


export function create_log(input = []){
	const { subscribe, set, update, get } = writable(input);
	
	return {
		subscribe,
		update,
		get,
		
		
		add: (text, alt)  => update(a => {
			a.push({text, alt});
			return a;
		}),
		
		em: (text)  => update(a => {
			a.push({
				text: `<i>${text}</i>.`,
			});
			return a;
		}),
		
		attack: (attacker, defender, damage)  => update(a => {
			a.push({text: `
				<b>${attacker.name}</b> attacked <b>${defender.name}</b> causing damage <b>${damage} HP</b>.
			`})
			return a;
		}),
		
		
		evaded(defender){
			 prepare_cloud(defender, `Evaded!`, "black")
		},
		
		critical(defender){
			 prepare_cloud(defender, `Critical hit!`, "red")
		},
		
		health(defender, value){
			prepare_cloud(defender, `${pm(value)} HP`, value <= 0 ? "red" : "lime"); //TODO good X bad color
		},
		
		
		effect(defender, text){
			prepare_cloud(defender, text, "red"); 
		},
	}
}



import {constants} from "Data/constants"
const cloud_update_frequency = 50;
const cloud_delay = 500;
const queue = [];

function prepare_cloud(defender, text, color){
	const {x,y} = defender;
	queue[x] ??= [];
	queue[x][y] ??= [];
	queue[x][y].push(()=> cloud(defender, text, color));
	if(queue[x][y].length === 1)  setTimeout( queue[x][y][0], 100 ) // queue[x][y][0](); //TODO!!!!!!!!!!!!!!
	
}

function cloud(defender, text = "text", color = "black"){
	const {x,y} = defender;
	const origin = document.getElementById(`combatant_${x}_${y}`).getBoundingClientRect();
	
	//remove from queue
	queue[x][y].shift();
	
	let font = 18;
	let opacity = 1;
	let top = origin.top + window.scrollY - font;
	
	const chimney = document.createElement("DIV");
	chimney.style.top = top;
	chimney.style.left = origin.left + window.scrollX;
	chimney.style.width = constants.combat.combatant_size;
	chimney.style.zIndex = 10;
	chimney.style.display = "block";
	chimney.style.position = "absolute";
	//chimney.style.background = "yellow";
	chimney.style.textAlign = "center";
	chimney.style.pointerEvents = "none"; //should be clickable through
	document.body.appendChild(chimney);
	
	
	const div = document.createElement("DIV");
	div.style.position = "relative";
	div.style.opacity = opacity;
	div.style.fontSize = font;
	div.style.fontWeight = "bold";
//TODO - GOOD & BAD COLORS 
	div.style.color = color;
	div.style.whiteSpace = "nowrap";
	div.innerHTML = text;
	chimney.appendChild(div);
	
	function update(){
		//chimney
		top -= 5;
		chimney.style.top = top;
		
		//div
		font += 4;
		opacity -= 0.05;
		div.style.opacity = opacity;
		div.style.fontSize = font;
		
		//continue x self-destruct
		if(opacity > 0){
			setTimeout(update, cloud_update_frequency);
		}else{
			chimney.remove();
		}
	}
	update(); //triggered for the first time
	
	
	//trigger another cloud in queue
	if(queue[x][y].length) setTimeout( queue[x][y][0], cloud_delay )
}
		