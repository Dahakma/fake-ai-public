import { writable, derived, get } from 'svelte/store';
import { get as get_value } from 'svelte/store';
import {rg, ra} from "Libraries/random";


import {clone, capitalise} from "Libraries/dh";
import {enemies as adversaries} from "Data/virtual/enemies";
import {constants} from "Data/constants";
import {stock_effects} from "Data/virtual/stock_effects";
import {skills, skills_debug} from "Data/virtual/skills";
import {zones as list_zones} from "Data/virtual/zones";
import {ATTRIBUTES} from "Virtual/attributes";
import {EFFECTS} from "Virtual/effects";
import {INVENTORY} from "Avatar/index";
import {create_log} from "./log";


function create_status(input = {
	round_index: 0,
	pawn_index: 0,
	action_index: 0,
	order_index: -1,
	order: [],
	selected_tile: null,
	selected_locked: false,
	executing: false,
	next_round: true,
	trigger_next_round: false,
	trigger_combat_end: null,
	trigger_ai_action: false,
	/*
	pawn: undefined,
	action: null,
	tile: undefined, 
	*/
}, pawns){
	const { subscribe, set, update, get } = writable(input);
	
	return {
		subscribe,
		update,
		
		check: ()  => update(a => {
			console.error(a);
			return a;
		}),
		
		next: ()  => update(a => {
			//a.pawn_index
			//TODO ORDER INDEX
			//a.pawn = get(pawns);
			//a.action = null;
			return a;
		}),


		//next_pawn
		next_pawn: (pawns) => update(a => {
			const paw = get_value(pawns)

			//PC alive? 
			if(paw[0].dyno.hp <= 0 && !SETTING.immortality){
				a.trigger_combat_end = false; //i.e the PC; false means the combat was lost
				return a;
			}
			//removes dead & check if there are any enemies let 
			let foes = 0;

			for(let i = 1; i < paw.length; i++){
				if(paw[i].ally === undefined) continue; //non-combatant

				if(paw[i].dyno.hp <= 0){
					pawns.remove(i);
					a.order = a.order.filter( b => b.index !== i); //removes actions of the killed pawn from the combat order 
					a.order = a.order.map( b => { //reindexed the orderbecause the index is now all fucked up
						if(b.index > i){
							return {...b, index: b.index - 1};
						}else{
							return b;
						}
					});
					i--;
					//a.order_index--;
				}else if(paw[i].typ === "enemy"){
					foes++;
				}
			}
			// no more enemies
			if(foes === 0){
				a.trigger_combat_end = true; 
				return a;
			}
			

			a.selected_tile = null;
			a.executing = false;
			a.action_index = 0;
			
			a.order_index++;
			if(a.order.length <= a.order_index){ //THIS STUPID THINGS DECIDES WHETHER TO START THE NEW ROUND
				a.trigger_next_round = true;
				a.order_index = -1;
			}else{
				a.trigger_next_round = false;
				a.pawn_index = a.order[a.order_index].index;
				a.trigger_ai_action = !paw[a.pawn_index].ally; 
			}
			return a;
		}),
		
		//currently selected action
		action: (index)  => update(a => {
			a.action_index = index;
			return a;
		}),
		
		//hovered over tile
		hover: (coordinates)  => update(a => {
			a.selected_tile = coordinates;
			return a;
		}),
		
		//clicking on tile
		execute: () => update(a => {
			a.executing = true;
			return a;
		}),
		
		next_round: (pawns)  => update(a => {
			a.round_index++;
			a.order = calc_order( get_value(pawns) );
			//a.order_index = ; //next_pawn() will imediatelly raise that to 0
			return a;
		}),
		
		//clicking on tile
		/*
		kill: (index) => update(a => {
			if(index === 0) a.trigger_combat_end = false; //i.e the PC; false means the combat was lost
			a.order = a.order.filter( b => b.index !== index); //removes actions of the killed pawn from the combat order 
			
			if(foes === 0) a.trigger_combat_end = true; // no more enemies
			return a;
		}),
		*/
	}
}


/*
export function calc_initiative(emy){
	if(!emy?.att?.reflex) return 0;
	return rg.range(emy.att.reflex * (1 - reflex_spread), emy.att.reflex * (1 + reflex_spread) );
}
*/


export function calc_order(pawns){
	//could be rewritten in more efficient way but there will never be more than like 50 pawns so who cares
	const order = [];
	const list = pawns.map( (a,i) => { return {
		name: a.name,
		index: i,
		reflex: a.att?.reflex,
		x: a.x,
		value: 0,
	}}).filter(a => {
		if(a.reflex === undefined) return false; //filters out immovable obstacles
		if(a.x === undefined) return false; //filters out not placed
		return true;
	});
	
	let pawns_indexes = list.map( a => a.index );
	

	function initiative(reflex){
		return rg.range( reflex * (1 - constants.combat.reflex_spread), reflex * (1 + constants.combat.reflex_spread) );
	}
	list.forEach( a => a.value += initiative(a.reflex) ); //default initiatives

	let fail = 100;
	while(pawns_indexes.length){ //the selection will continue until every pawn is added at least once

		list.sort( (a,b) => a.value - b.value ); //smallest (quickest pawn) to biggest
		const {index, reflex, value} = list[0]; 
		pawns_indexes = pawns_indexes.filter( a => a !== index ); //to ensure every pawn has at least one action

		order.push({index, reflex, value});
		list[0].value += initiative(list[0].reflex); //if the pawn is quick enough, it can have several actions in one round

		if(fail--<0) break;
	}
	
	return order;
	
}








	
function create_pawns(input){
	const { subscribe, set, update, get } = writable(input);

	return {
		subscribe,
		update,
		get, 
		
		position: (index, x, y)  => update(pawns => {
			pawns[index].x = x;
			pawns[index].y = y;
			return pawns;
		}),
		
		round_reset: ()  => update(pawns => {
			for(const paw of pawns){
				paw.evasive_actions = paw.att?.evasive_actions;
				//EFFECTS.tick(paw);
				if(ATTRIBUTES.flag("autocrit", paw)) paw.evasive_actions = 0;
			}
			return pawns;
		}),
		
		
		hp: (index, value)  => update(pawns => {
			if(typeof index === "object"){
				index = pawns.findIndex( a => a == index );
			}
			if(pawns[index]?.dyno?.hp) pawns[index].dyno.hp += value;
			if(pawns[index].dyno.hp > pawns[index].att.hp) pawns[index].dyno.hp = pawns[index].att.hp;
			return pawns;
		}),
		
		remove: (index)  => update(pawns => {
			pawns.splice(index, 1);
			return pawns;
		}),
		
		
		eng: (index, value)  => update(pawns => {
			if(typeof index === "object"){
				index = pawns.findIndex( a => a == index );
			}
			if(pawns[index]?.dyno?.eng) pawns[index].dyno.eng += value;
			if(pawns[index].dyno.eng > pawns[index].att.eng) pawns[index].dyno.eng = pawns[index].att.eng;
			return pawns;
		}),
		
		check: a  => update(pawns => {
			return pawns;
		}),
	}
}





export function initiate_combat(enemies, seed, {mixed = true, terrain = "open"} = {}){
	//const enemies = clone(warbands[warband]); //TODO - CHECK
	if(typeof enemies === "string"){
		const temp = enemies;
		enemies = [];
		enemies[temp] = 1;
	}
	
console.error(enemies);
	let to_be_pawns = [];
	
	//initiates combat seed
	rg.i(seed); //TODO - DUNGEON SEED??
	
	//combat zone 
	const xxx = ext.follower ? 4 : 3; //HARD
	const yyy = 4; /*(()=>{
		let yyy = 1 + Math.ceil(enemies.length / xxx);
		if(yyy > 4) yyy = 4;
		return yyy;
	})();*/
	
	//initiate allies
	to_be_pawns = initiate_allies()
	
	//initiates enemies
	const actual_enemies = [];
	Object.entries(enemies).forEach( ([key, value]) => { //could be enemies or arrays with number and enemy
		if( Array.isArray(value) )  value = rg.range( value[0], value[1] )
		while(value-- > 0) actual_enemies.push(key);
		/*
		if( Array.isArray(a)  ){
			let number = a[0];
			const key = a[1];
			if( Array.isArray(number) ) number = rg.range( number[0], number[1] );
			while(number-- > 0) actual_enemies.push(key);
		}else{
			actual_enemies.push(a);
		}
		*/
	});
console.error(actual_enemies);

	
//console.error(actual_enemies)

	if(mixed) rg.shuffle(actual_enemies); //enemies are in random battle order
	to_be_pawns = [...to_be_pawns, ...initiate_enemies(actual_enemies) ];
	
	//initiate obstacles 
	to_be_pawns = [...to_be_pawns, ...initiate_obstacles() ];
	

	//create pawns store
	const pawns = create_pawns(to_be_pawns);
	
	//create status 
	const status = create_status(undefined, pawns); //TODO
	
	//create tiles store
	const tiles = derived([pawns, status], ([pawns, status]) => {
		//tiles
		const tiles = calc_tiles(pawns, xxx, yyy);
		//highlights
		calc_highlighted(tiles, pawns, status);
		return tiles;
	});

	
	const log = create_log();
	log.em(`Combat started`);
	
//console.log("//initiate combat")

	return {
		pawns,
		tiles,
		xxx,
		yyy,
		status,
		log,
	};


	
}





function calc_tiles(pawns, xxx, yyy){
	//creates empty
	const tiles = [];
	for(let x = 0; x < xxx; x++){
		tiles[x] = [];
		for(let y = 0; y < yyy; y++){
			tiles[x][y] = {
				pawn: null,
				//highlight: "",
				active: false,
			}
		}
	}
	
	//fills them with pawns
	if(pawns?.length){
		pawns.forEach( pawn => {
			const {x, y} = pawn;
			if(x !== undefined && y !== undefined){
				tiles[x][y].pawn = pawn; //or just index?
			}
		})
	}
	return tiles;
}




function calc_highlighted(tiles, pawns, status){
	const active_pawn =  pawns[ status.pawn_index ];
	//active pawn (currently performing action)
	{
		//console.log(status.pawn_index)
		//console.log(pawns[ status.pawn_index ])
		const {x, y} = active_pawn;
		//tiles[x][y].highlight = "active";
		tiles[x][y].active = true;
	}
	

	//targeted tiles 
	(()=>{
		const selected = status.selected_tile; //selected targeted tile

		if(selected === null) return;
		const zones = list_zones[   pawns[ status?.pawn_index ]?.actions?.[ status?.action_index ]?.zone   ];

		if(zones === undefined) return;


		const adjust = (()=>{ //origin for calculation of marked tiles could be either the tile with the active subject or the targeted tile
			if(zones.origin === "subject"){
				return {
					x: active_pawn.x,
					y: active_pawn.y,
				}
			}else if(zones.origin === "tile"){
				return {
					x: selected.x,
					y: selected.y,
				}
			}
		})();
		if(!active_pawn.ally) adjust.y *= -1; //enemies are attacking in oposite direction
		
		const marked = zones.combs.find( a => a.on.x + adjust.x === selected.x && a.on.y + adjust.y === selected.y )?.mark;

		if(marked === undefined) return;
		
		marked.forEach( ({x, y}) => {
			x += adjust.x;
			y += adjust.y;
			if(tiles[x]?.[y]){
				//tiles[x][y].highlight = "marked";
				tiles[x][y].marked = true;
			}
		});
	 
	})();
}





function initiate_obstacles(){
	let out = [];
	const spacers = ext.follower ? [[0, 0],[3, 0]] : [[0, 0],[2, 0]];
	//if(ext.follower) spacers.push([4, 0])
	//if(ext.follower)
	out = spacers.map( a => {
		return {
			typ: "spacer",
			x: a[0],
			y: a[1],
		}
	})
	
	return out;
}





function initiate_allies(){
	const allies = [PC];
	if(ext.follower) allies.push(ext.follower);
	const out = allies.map( subject => {
		const temp = {
			typ: "ally",
			ally: true,
			name: subject.name,
			sex: subject.sex,
			att: subject.att,
			weapon: subject.weapon,
			dyno: subject.dyno,
			effects: subject.effects,
			y: 0,
			actions: [],
			
			//weapon: subject.weapon
		}
		
		
		
		temp.actions.push({
			get name(){
				return `Attack with ${subject?.weapon?.name ?? "Fists"}.`;
			},
			zone: (()=>{
				if(subject.weapon?.type === "polearms") return "medium";
				if(subject.weapon?.type === "bows") return "ranged";
				return "close"; 
			})(),
			get damage(){
				const attack = {
					weapon: 1,
				};
				return ATTRIBUTES.action_damage(attack, subject);
				//TODO - SUBJECT OR TEMP??????
			}, 
			get desc(){
				const attack = {
					name: `Attack. `,
					desc: subject.weapon ? `Attack with ${subject.weapon.name}.` : `Attack with your fists.`,
					weapon: 1,
				};
				return ATTRIBUTES.action_desc(attack, subject);
			},
			skill: {},
		})
		
		
		let ski = [...subject.skills];
		if(window.debug) ski = [...ski, ...skills_debug];

		ski.forEach( key => {
			const attack = skills[key];
			//if(!attack.combat) return; //TODO
			if(!attack?.zone) return;
			const effects = attack.effects ? attack.effects.map( a => Array.isArray(a) ? a : [100, a] ) : [];

//console.error(effects)
			temp.actions.push({
				skill: attack,
				name: attack.name ?? key,
				zone: attack.zone, 
				condit: attack.condit,
				price: attack.price,
				get damage(){
					return ATTRIBUTES.action_damage(attack, subject);
				},
				effects,
				get desc(){
					let text = ATTRIBUTES.action_desc(attack, subject);
					if(attack?.price?.eng){
						text += `${attack.price.eng} energy<br>`;
					}
					
				
					if(effects.length){
						for(const [prob, key] of effects){
							text += `${prob}% ${stock_effects[key].name}<br>`;
						}
					}
					return text;
						
					
					//TODO TEMPFIX
					
				},
			});	
		});
		
		
		
//console.error( INVENTORY.get );
		INVENTORY.get.forEach( item => {
//console.log(item);
			if(item.skill){
				const attack = skills[item.skill];
				attack.name = capitalise(item.name);
//console.error(attack);
				if(!attack?.zone) return;
				const effects = attack.effects ? attack.effects.map( a => Array.isArray(a) ? a : [100, a] ) : [];

				temp.actions.push({
					skill: attack,
					deplete(){
						INVENTORY.remove(item.blueprint);
					},
					get name(){
						return `${capitalise(item.name)} (${INVENTORY.count(item.blueprint)})`;
					},
					
					zone: attack.zone, 
					condit(subject){
						return INVENTORY.count(item.blueprint) /*&& attack.condit(subject)*/; //TODO
					},
					effects,
					//price: attack.price,
					get damage(){
						return ATTRIBUTES.action_damage(attack, subject);
					},
					get desc(){
						/*
						if(attack?.price?.eng){
							return `${ATTRIBUTES.action_desc(attack, subject)} ${attack.price.eng} energy`;
						}
						*/
						let text = ATTRIBUTES.action_desc(attack, subject);
						if(effects.length){
							for(const [prob, key] of effects){
								text += `${prob}% ${stock_effects[key].name}<br>`;
							}
						}
						return text;
						//TODO TEMPFIX
						
					},
				});
			}
		})

		
		//potions
		/*
		actions.push({	
			name: use name
			desc: use health potion
		})
		*/
		
		
		//attributes
		//Object.keys(source.att).forEach( key => temp.att[key] = source.att[key] );
		
		//dynamic attributes
		//temp.dyno.hp = source.dyno.hp; //based on current PC.dyno.hp not default PC.att.hp!!!
		//temp.dyno.eng = source.dyno.eng;
		
		/*
		if(source.weapon){
			name: source.weapon.name,
			
			
		}else{
			actions.push({
				name: `Punch`,
			})
		}
		*/
		
		//temp.dyno.hp = 20;
		//temp.dyno.eng = 20;
		//temp.att.reflex = 20;
		return temp;
	});
	
	out[0].x = 1; //HARDCODED
	if(out[1]) out[1].x = 2; //HARDCODED
	
	return out;
}
	
function initiate_enemies(enemies){
	
	const unigue_list = {}; 
	function unique_name(blueprint){ //ensure there are no enemies with the same name (second goblin => "Goblin #2") //beware - assumes no enemies with the same blueprint has the same name (adversaries[blueprint].name)
		if(unigue_list[blueprint]){
			unigue_list[blueprint]++;
			return `#${unigue_list[blueprint]}`;
		}else{
			unigue_list[blueprint] = 1;
			return `#1`;
		}
	}
	function unique_remove_unnecessary(){ //no need to specify "Ogre #1" when there's only one
		out.forEach( a => {
			if(unigue_list[a.blueprint] === 1){
				a.name = a.name.replace(`#1`,``);
			}
		})
	}
	
	
	const out = enemies.map( e => {
		const subject = adversaries[e];
		const temp = {
			typ: "enemy",
			ally: false,
			name: `${subject.name} ${unique_name(e)}`,
			sex: subject.sex ?? 0,
			blueprint: e,
			weapon: {...subject.weapon},
			base: {
				hp: 10,
				eng: 0,
				...subject.base,
			},
			//att: {},
			//dyno: {},
			x: undefined,
			y: undefined,
			actions: [],
		};
		
		//shorhand weapon damage
		if(temp.weapon?.damage && !temp.weapon?.att?.weapon_damage){
			temp.weapon.att ??= {};
			temp.weapon.att.weapon_damage = temp.weapon.damage
		}
		
		//attributes
		ATTRIBUTES.initiate(temp);
		//Object.keys(source.att).forEach( key => temp.att[key] = source.att[key] );
		
		const attack = {
			name: `Attack. `,
			desc: subject.weapon ? `Attack with ${subject.weapon.name}.` : `Attack with your fists.`,
			weapon: 1,
			att: subject.att, //TODO!!!
		};
				
				
		temp.actions.push({
			name: `Attack. `,
			skill: attack,
			zone: (()=>{
				return "close"; 
				//medium for polearms
				//ranged for bows 
			})(),
			
			get damage(){
			
				return ATTRIBUTES.action_damage(attack, temp);
			}, 
			get desc(){
				
				return ATTRIBUTES.action_desc(attack, temp);
			},
		});
		
		if(subject.skills){
			subject.skills.forEach( key => {
				const attack = skills[key];
				//if(!attack.combat) return; //TODO
				temp.actions.push({
					name: attack.name,
					zone: attack.zone,
					price: attack.price,
					condit: attack.condit,
					get damage(){
						return ATTRIBUTES.action_damage(attack, subject);
					},
					get desc(){
						return ATTRIBUTES.action_desc(attack, subject);
					},
					
				});	
			});
		}
		
		//dynamic attributes
		temp.dyno.hp = temp.att.hp;
		if(temp.att.eng) temp.dyno.eng = temp.att.eng;
		
		return temp;
	});
	
	
	unique_remove_unnecessary();
	
	//console.error("out")
	//console.error(out)
	
	return out;
}