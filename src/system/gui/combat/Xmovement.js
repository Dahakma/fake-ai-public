export const tiles = [];
export let xxx;
export let yyy;

class Tile {
	constructor(x, y, typ = "empty") {
		this.x = x;
		this.y = y;
		this.typ = typ;
	}
	
	fill(typ = "empty", fighter = {}){
		this.typ = typ;
		this.fighter = fighter;
	}
	
	get coordinates(){
		return {
			x: this.x,
			y: this.y,
		};
	}
	
	get is_empty(){
		this.typ === "empty"
	}
	
	clear(){
		this.typ = "";
		this.fighter = {};
	}
}

function initiate_tiles(enemies){
	//adjusts number of tiles to the number of enemies
	xxx = ext.follower ? 5 : 3; //HARD
	yyy = (()=>{
		let yyy = 1 + Math.ceil(enemies.length / xxx);
		if(yyy > 4) yyy = 4;
		return yyy;
	})();
	
	//creates empty tiles
	for(let x = 0; x < xxx; x++){
		tiles[x] = [];
		for(let y = 0; y < yyy; y++){
			//in the first line, odd columns are heroes and even columns are spacers
			if( y === 0 &&  !(x % 2) ){ 
				tiles[x][y] = new Tile(x, y, "spacer");
				continue;
			}
			
			tiles[x][y] = new Tile(x, y, "empty");
			
			/*
			if( y === 3 && [1, 3].includes(x) ){ //last combat line //HARD
				tiles[x][y] = undefined; 
				continue;
			}
			tiles[x][y] = null;
			*/
		}
	}
	
	//places PC and follower on tiles
	tiles[1][0].fill("ally", PC);
	if(ext.follower) tiles[3][0].fill("ally", ext.follower); 
	
}




