/*
	CREATES DIV WITH ALT TEXT VISIBLE ON HOVER (created & updates & deletes)
*/
import {constants} from "Data/constants"

export const ULTRA = {
	initiate(div, text, dud = false){
		if(!div) return; //TODO?
		ULTRA.clr(div);
		div.ultra = function(){ 
			//function is assigned to element to make sure there is always only one; (impossible to remove anonymous function form evenListener)
			return typeof text === "function" ? text() : text;
		};
		
		div.addEventListener("mouseenter", ULTRA.on);
		div.addEventListener("mousemove", ULTRA.update);
		div.addEventListener("mouseleave", ULTRA.off);
		if(dud){
			div.style.cursor = "help";
		}else{
			div.addEventListener("click", ULTRA.off);
		}
	},
	
	clr(div){
		div.ultra = undefined;
		div.removeEventListener("mousemove", ULTRA.on);
		div.removeEventListener("mousemove", ULTRA.update);
		div.removeEventListener("mouseleave", ULTRA.off);
	},
	
	on(event){
		ULTRA.off();
		ULTRA.delay = setTimeout(function(){ 
			const text  = event.target.ultra();
			if(!text) return; //there is no text to display
			ULTRA.div = document.createElement("DIV");
			ULTRA.div.id = "div_ultra";
			ULTRA.div.style.display = "block";
			ULTRA.div.innerHTML = text;
			document.body.appendChild(ULTRA.div);
			ULTRA.update(event);
		}, constants.tooltip_delay);
	},
	
	off(){
		clearTimeout(ULTRA.delay);
		if(!ULTRA.div) return;
		ULTRA.div.remove();
		ULTRA.div = undefined;
	},
	
	update(event){
		if(!ULTRA.div) return;
		const x = event.clientX;
		const y = event.clientY;
		ULTRA.div.style.left = (x + 20);
		ULTRA.div.style.top = (y + 20);
	},

};
export const tooltip = ULTRA.initiate;