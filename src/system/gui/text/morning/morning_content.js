import {wearRandomGarment, draw, violates, wears, countSpecificGarments} from "Avatar/index";
import {remove_nightie, get_dressed, random} from "./morning_random.js";
import {remove, sluttiness, garmentIs, createWear} from "Avatar/index";
import {time} from "System/time";
import {main} from "Loop/engine";
import {DISPLAY} from "Gui/gui";
import {get} from "svelte/store";

//CONTINUE to the next event 
import {next_event, active_timeslot} from "Taskmaster/scheduler"; 
import {autosave, snapshot} from "System/save";

const limits = {
	bra: 0, //not allowed to progress 
	e_bra: 1,  //embarrassing
	n_bra: 3, //would be naughty
	breasts:  2, //almost exposed (transparent clothes)
	e_breasts: 3, 
	panties: 1,
	e_panties: 2, 
	n_panties: 5, //TODO
	panties_exposed: 1, //visible panties (too short skirt)
	e_panties_exposed: 2,
	pussy: 3, //exposed (too short skirt) & almost exposed (transparent clothes)
	e_pussy: 4,
}

function done(){
	DISPLAY.text("");
	if(get(DISPLAY).temp){
		main(get(DISPLAY).temp);
	}else{
		autosave(/*name*/); //TODO - can work ?????
		if(head.day === 5) snapshot(`day5`,`First Wednesday`);
		if(head.day === 12) snapshot(`day12`,`Second Wednesday`);
		if(head.day === 19) snapshot(`day19`,`Third Wednesday`);
		if(head.day === 26) snapshot(`day26`,`Fourth Wednesday`); //TODO - check if 26th day is Wednesday!!
		if(head.day === 33) snapshot(`day33`,`Fifth Wednesday`);
		if(head.day === 40) snapshot(`day40`,`Sixth Wednesday`);
		//DISPLAY.morning(false);
		next_event();
	}
}

let timeslot = "";
let rules = false;

export function content(){
	timeslot = active_timeslot();
	rules = time.is_workday && (timeslot === "awakening" || timeslot === "morning");

	let allow_to_continue = true;
	const links = [];
	let nolinks = [];
	const notifications = [];
	let appearance = {};
	
	const apear =  sluttiness(PC);
	const details = apear.details;
	


	function add(text = "empty", action, major = false){
		if(major) allow_to_continue = false;
		const fce = typeof action === "string" ? ()=> {
			remove_nightie()
			wearRandomGarment(action, rules, true);
			//updateDraw(); //TODO - redundant?
			DISPLAY.check();
		} : ()=> {
			remove_nightie()
			action();
			//updateDraw(); //TODO - redundant?
			DISPLAY.check();
		};
		
		notifications.push({
			text,
			//style: major ? "major" : "minor",
			//inlineStyle: major ? "font-weight: bold" : "font-weight: normal",
			major,
			fce,
			alt: `Click to quickly solve the issue. `,
		});
	}
	
	

	//PANTIES
	if(!PC.panties){
		if(ext.slut <= limits.panties && !ext.rules.no_panties){
			if( countSpecificGarments("panties") ){
				add(`I cannot go out without panties!`, "panties", true); //MAJOR
			}else{
				add(`<strong>I cannot go out without panties!</strong> OH NO! I was not able to find any! I will have to borrow ones from my mother's underwear drawer. `, ()=> createWear("briefs"), true); //MAJOR
			}
		}else if(ext.slut <= limits.e_panties){
			add("It is embarrassing go out without panties!", "panties");
		}

		if( (details.glimpse || details.pussy || details.hintedPussy) && ext.slut <= limits.pussy ){
			add("I cannot go out with my pussy exposed! ", "lower", true); //MAJOR
		}else if(details.glimpse && ext.slut <= limits.e_pussy ){
			add("It is embarrassing to go out with my pussy exposed. ", "lower"); 
		}

	}else{
		if(violates.sexy_panties && rules){
			add("I have to wear sexy panties. ", "panties");
		}else if(violates.no_panties && rules){
			add("I am not allowed to wear panties. ", ()=> remove("panties", PC, PC.inventory) );
		}else if(ext.slut >= limits.n_panties){  //TODO SLUT5
			add("It would be naughty to go out without panties.  ", ()=> remove("panties", PC, PC.inventory) );
		}

		if(details.glimpse && ext.slut <= limits.panties_exposed ){
			add("I cannot go out flashing my panties! ", "lower", true); //MAJOR
		}else if(details.glimpse && ext.slut <= limits.e_panties_exposed ){
			add("It is embarrassing to go out flashing my panties. ", "lower"); 
		}
	}
	


	
	//BRA
	if(!PC.bra){
		if( (details.breasts || details.hintedBreasts) && ext.slut <= limits.breasts ){
			add("I cannot go out with my breasts exposed!", "upper", true); //MAJOR
		}else if( (details.breasts || details.hintedBreasts) && ext.slut <= limits.e_breasts ){
			add("It is embarrassing to go out with my breasts exposed. ", "upper"); 
		}else if(ext.slut <= limits.bra && !ext.rules.no_bra){
			if( countSpecificGarments("bra") ){
				add("I cannot go out without a bra!", "bra", true); //MAJOR
			}else{			
				add(`<strong>I cannot go out without a bra!</strong> OH NO! I was not able to find any! I will have to borrow one from my mother's underwear drawer. `, ()=> createWear("simpleBra"), true); //MAJOR
			}
		}else if(ext.slut <= limits.e_bra){
			add("It is embarrassing go out without a bra!", "bra");
		}
	}else{
		if(violates.no_bra && rules){
			add("I am not allowed to wear bra. ",  ()=> remove("bra", PC, PC.inventory) );
		}else if(ext.slut >= limits.n_bra){
			add("It would be naughty to go out without a bra.  ",  ()=> remove("bra", PC, PC.inventory) );
		}
	}


	//UPPER
	if(!PC.upper && !PC.upperOuter){
		if( countSpecificGarments("upper") + countSpecificGarments("upperOuter") + countSpecificGarments("upperLower") ){
			add("I cannot go out topless!", "upper", true); //MAJOR
		}else{			
			add(`<strong>I cannot go out topless!</strong> OH NO! I was not able to find any top! I will have to borrow one from my mother's wardrobe. `, ()=> createWear("tightTee"), true); //MAJOR
		}
		
	}else if(violates.sexy_top && rules){
		add("I have to wear a sexy top or a dress.", "upper");
	}

	//LOWER
	if(!PC.lower && !wears.dress){
		if( countSpecificGarments("lower") + countSpecificGarments("upperLower") ){
			add("I cannot go out bottomless!", "lower", true); //MAJOR
		}else{			
			add(`<strong>I cannot go out bottomless!</strong> OH NO! I was not able to find any bottom! I will have to borrow one from my mother's wardrobe. `, ()=> createWear("longSkirt"), true); //MAJOR
		}
	}else if(violates.short_skirt && rules){
		add("I have to wear short skirts or dresses. ", "lower");
	}else if(violates.show_legs && rules){
		add("I am supposed to show my legs.  ", "lower");
	}


	//SHOES
	if(!PC.shoes){
		if( countSpecificGarments("shoes") ){
			add("I cannot go out without shoes!", "shoes", true); //MAJOR
		}else{			
			add(`<strong>I cannot go out without shoes!</strong> OH NO! I was not able to find any! I will have to borrow ones from my mother's shoe cabinet. `, ()=> createWear("kittyHeels"), true); //MAJOR
		}
		
	}


	//SOCKS
	if(violates.sexy_legwear && rules){
		add("I am supposed to wear sexy legwear. ", "socks");
	}else if(ext.slut === 0 && wears.sluttyLegwear){
		add("It is embarrassing to go out in such slutty legwear. ", "socks");
	}

	//COLLAR
	if(violates.collar && rules){
		add("I am supposed to wear a collar. ", "collar"); //TODO - EQUIPS CHOKER
	}else if(violates.choker && rules){
		add("I should wear a choker. ", "collar");
	}

	//NIGHTIE
	if(PC.upper?.subtype === "nightie"){
		notifications.length = 0;
		add("I cannot go out in my nightie!", ()=> random(), true);
		appearance = {
			text: ``,
			alt: ``,
		}
	//SLUTTINESS LEVEL
	}else if(allow_to_continue){
		
		const slut = details.slut;
		
		let text = "";
		let fce;
		

		//prude
		if(slut === 0){
			if(ext.slut === 0){
				text += `I was dressed modestly and comfortably. `
			}else if(ext.slut === 1){  //0 1
				text += `I was dressed modestly. `
			}else if(ext.slut === 2){ //tease
				text += `I was dressed modestly and kinda boringly. I could be wearing something sexier. `;
				fce = ()=> random(ext.slut);
			}else if(ext.slut === 3){
				text += `I was wearing bland and boring clothes. I should definitely change into something sexier. `;
				fce = ()=> random(ext.slut);
			}else{
				text += `I was wearing awfuly bland and boring clothes. I should definitely change into something sluttier! `;
				fce = ()=> random(ext.slut);
			}
		
		//beauty 
		}else if(slut === 1){
			if(ext.slut <= 2){
				text += `I looked great! `; //0 1 2
			}else if(ext.slut === 3){ //slut
				text += `I did not look bad but maybe I could change into someting sexier? `;
				fce = ()=> random(ext.slut);
			}else{
				text += `My outfit did not seem right. I should ged changed into something sluttier. `;
				fce = ()=> random(ext.slut);
			}
		
		//tease 
		}else if(slut === 2){
			if(ext.slut === 0){ //prude
				text += `I looked sexy. Maybe a bit too much sexy? `
				fce = ()=> random(ext.slut);
			}else if(ext.slut <= 3){ //1 2 3
				text += `I looked awesome and sexy! `
			}else if(ext.slut === 4){ //whore
				text += `I looked pretty sexy but maybe I could be wearing something a bit sluttier? `;
				fce = ()=> random(ext.slut);
			}else{
				text += `My outfit was sexy but not slutty enough. `;
				fce = ()=> random(ext.slut);
			}
		
		//slut 
		}else if(slut === 3){
			if(ext.slut === 0){ 
				text += `I was dressed like a slut. I should change into something normal, I should not go out dressed like this!  `
				fce = ()=> random(ext.slut);
			}else if(ext.slut <= 3){ //beauty
				text += `I was dressed like a slut. Maybe I should change into something a bit less suggestive? `
		//}else if(ext.slut === 4){ //2 3 4
			}else{ //2 3 4 5
				text += `I was dressed like a slut. `;
			/*
			}else{ //cumdump
				text += `I was dressed slutty but I could be dressed even sluttier! `;
				fce = ()=> random(ext.slut);
			*/
			}
		
		
		//whore 
		}else if(slut === 4){
			if(ext.slut === 0){ 
				text += `I was dressed like a whore. I absolutely should not go out wearing something so embarrassing and indecent!  `
				fce = ()=> random(ext.slut);
			}else if(ext.slut === 1){ 
				text += `I was dressed like a whore. I should probably change if I did not want to be mistaken for a streetwalker. `
				fce = ()=> random(ext.slut);
			}else if(ext.slut <= 2){ //tease
				text += `I was dressed like a whore. It was very sexy but maybe I went too far? `
				fce = ()=> random(ext.slut);
			}else{ //3 4 5
				text += `I was dressed like a whore. `;
			}
		

		//cumdump
		}else if(slut >= 5){
			if(ext.slut <= 1){ 
				text += `I was dressed like a cumdump with no shame. Going out naked would be almost less humilating than wearing such ridiculously obscene outfit!  `
				fce = ()=> random(ext.slut);
			}else if(ext.slut === 2){ 
				text += `I was dressed like a wanton cumdump. It would be humiliating go out like that. `
				fce = ()=> random(ext.slut);
			}else if(ext.slut === 3){ //slut
				text += `I was dressed like a shameless wanton cumdump. Even I would be a little embarrassed to wear such clothes in public. `
				fce = ()=> random(ext.slut);
			}else{ //3 4 5
				text += `I was dressed like a cumdump with zero shame. `;
			}
			
			
		}
		
			/*
			if(ext.slut === 0){
				text += `I looked sexy. Maybe a bit too much sexy? `
				fce = ()=> random(ext.slut);
			}else if(ext.slut <= 3){
				text += `I looked awesome and sexy! `
			}else if(ext.slut === 4){ //whore
				text += `I looked pretty sexy but maybe I could be wearing something a bit sluttier? `;
				fce = ()=> random(ext.slut);
			}else{
				text += `My outfit was sexy but not slutty enough. `;
				fce = ()=> random(ext.slut);
			}
		*/
		
		
		/*
		const total =  sluttiness(PC);
		if(total.details.slut > ext.slut){
			add(`It is too embarrassing to go out dressed like a ${total.valueDesc}!`, ()=> random(ext.slut) );
		}else if(total.details.slut + 1 < ext.slut){
			add(`I could wear something a bit sexier. `, ()=> random(ext.slut) );
		}
		*/
		appearance = {
			text: `I looked into a mirror - ${text}`,
			alt: apear.desc,
			fce,
		}
		
		
	}else{
		appearance = {
			text: ``,
			alt: ``,
		}
	}
	
	function get_dressed_alt(){
		switch(ext.dressup.get_dressed){
			case "last_worn": return "The same clothes as yesterday";
			case "random": return "Random clothes";
			case "conservative": return "Random conservative";
			case "slutty": return "Random sexy";
			case "custom": return "Random custom";
			case "outfit": return "Random outfit";
			default: return "";
		}
	}
	//LINKS
	//needlessly fucking complicated but it's nearly fucking impossible to make the Svelte rerender the way need
	//I literelly wasted two days on this and it seems to be work okayish; any more tampering is strongly inadvisable
	if(allow_to_continue){
		let tx = `Go out. `;
		if(rules) tx = `Go to school. `;
		if(!time.is_workday && timeslot === "morning") tx = `Weekend.`;
		
		links.push({
			text: tx,
			fce: done,
		})
		
		links.push({
			text: `Get changed.`,
			fce: get_dressed,
			alt: get_dressed_alt(),
		})
	}
	
	nolinks = [{
			text: `Get dressed.`,
			fce: get_dressed,
			alt: get_dressed_alt(),
	}];
		
		
	return {
		appearance,
		allow_to_continue,
		notifications,
		links,
		nolinks,
	}	
}
		
		
		
		