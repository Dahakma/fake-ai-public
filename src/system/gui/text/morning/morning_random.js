import { writable } from 'svelte/store';

export const display_setting = writable(false);

import {var_name} from "Libraries/dh.js";
import {randomMakeup, wearRandomOutfit, wearRandomBrutalClothes, checkOutfit, updateDraw, remove, wearRandomGarment, draw, violates, wears, saveWornOutfit, wearOutfit, wearRandomClothes, removeEverything, wearEverything, create} from "Avatar/index";
import {DISPLAY, popup} from "Gui/gui"


export function setting(){
	display_setting.set(true);
}

export function remove_nightie(){
	if(PC.upper?.subtype === "nightie"){
		removeEverything(PC); 
	}
}

export function last_worn(){
	remove_nightie();
	wearOutfit("auto_last", PC, PC.inventory);
	DISPLAY.check();
}

export function outfit(){
	remove_nightie();
	randomMakeup(PC, false);
	wearRandomOutfit(false); //rules = false
	DISPLAY.check(); //TODO
}

export function get_dressed(){
	switch(ext.dressup.get_dressed){
		case "last_worn": return last_worn();
		case "random": return random();
		case "conservative": return conservative();
		case "slutty": return slutty();
		case "custom": return custom();
		case "outfit": return outfit();
		default: return;
	}
}


export function custom(){
	let slut = ext.dressup.slut ;
	if(slut === -2) slut = undefined;
	if(slut === -1) slut = ext.slut;
	random(slut, ext.dressup.follow_rules, "custom");
}


export function random(slut, rules = true, type){
	remove_nightie();
	randomMakeup(PC, false); //false - won't rerender the avatar (will be rerenderd by wearRandmClothes)
	if(slut === undefined){
		type ??= "advanced";
		wearRandomClothes(type, rules);
	}else{
		wearRandomBrutalClothes(slut, type, rules);
	}
	DISPLAY.check();
}

export function conservative(){
	random(0, false, "conservative")
}

export function slutty(){
	let level = ext.slut + 1;
	if(ext.slut > 5) level = 5;
	random(level, true, "advanced") //slutier than normal
}




export const save_outfit = function(){
	const no = ext.outfits.filter( a => a.reality === head.reality).length + 1;
	popup.prompt(`Enter the name`, `Outfit ${no}`, save);
	
	function save(name){
		if (!name) return;
		const id = var_name(name);
		saveWornOutfit(id, name, PC, head.reality);
		
	}
}