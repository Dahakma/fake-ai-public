//basically to go immediately back by pressing "escape" or assigned key

import {DISPLAY} from "Gui/gui.js";
import {KEYBOARD} from "System/keyboard.js";
import {constants} from "Data/constants";
const keybuttons = constants.keyboard; //TODO - TEMPORARY

/*
	when "esc" or "what" (key of keycode saved in contants.keyboard OR keycode) is pressed, "fce" is triggered
	quickback(what, fce)
*/

export function quickback(what, fce){
	const key = (typeof what === "string" && keybuttons[what]) || what;
	fce = fce ?? DISPLAY.text;
	KEYBOARD.back(fce, key);
}
	