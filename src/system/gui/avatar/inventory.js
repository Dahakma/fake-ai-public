import {DISPLAY, popup, ULTRA} from "Gui/gui";
import {KEYBOARD} from "System/keyboard";
import {POTIONS} from "Avatar/game/potions";
import { writable, get} from 'svelte/store';
import {var_name, capitalise, pm} from "Libraries/dh.js";
import {attributes} from "Data/virtual/attributes.js";
import {INVENTORY} from "Avatar/game/inventory";
import {draw, drawZoomed, drawUnzoomed} from "Avatar/canvas/draw";
import {update, updateDraw} from "Avatar/wardrobe/update";
import {cloneFigurine} from "Avatar/canvas/body";
import {randomClothes, randomGarment, randomOutfit} from "Avatar/wardrobe/random";
import {randomBrutalClothes} from "Avatar/game/gameRelated";
import {
	removeActual, 
	saveWornOutfit,
	wearOutfit,
	removeEverything,
} from "Avatar/wardrobe/main";

import {
	allCategories, 
	allSlots, 
	allPiercings, 
} from "Avatar/wardrobe/all";

import {itemQualityString} from "Avatar/wardrobe/all";

import {constants} from "Data/constants";


export const category = writable(""); //displayed category of clothes
export const wordFilter = writable(""); //string used for search fuction

export let inventory; //svelte store with owned items
export let merchant; //svelte store with traded items 
export let status; //svelte store with some reactive shit
/*
	worn // list of equiped items
	
	wornWealth  //price of all equiped items
	originalWornWealth //price of all equiped items before opening inventory 
	wornWealthChange //difference beween wornWealth and originalWornWealth
	
	orignalMoney //money player has before exange
	money //current actual amount
	moneyChange //originalMoney + money
*/
export let trade = false //is exchanging 
export let game = false; //is playing virtual game
export let follower = false; //is followers inventory
export let outfits = []; //list of outfits
export let figurine; //DAD object, doll-like copy of player used to display clothes
export let tax = false; // % of actual price merchants are willing to pay 
export let subject; //DAD object 

let uncancelable = false; //canceling disabled because potion/consumable used
let done = ()=>{}; //function to trigger when everything is done
let originalMerchant = []; //original unaltered merchants array
let originalInventoryCount = []; //items are not cloned but passed reference?????


export const useGrid = SETTING.grid_inventory; //grid X two columns
//useGrid = true; //todo - because eslint auto fix const


export const receipt = {
	items: [],
	log(item, count, price){
		const index = this.items.findIndex( a => a.id === item.id && a.seed === item.seed);

		if(index === -1){
			this.items.push({
				name: item.name,
				id: item.id,
				seed: item.seed,
				count,
				price, 
			})
		}else{
			this.items[index].count += count;
			this.items[index].price += price;
		}		
		this.items = this.items.filter( a => a.count ); //removes 0 count
	},
	get desc(){
		let text = `<ul>`;
		this.items.forEach( ({count,price,name}) => {
			text += `<li>${count > 0 ? "-" : "+"} ${Math.abs(price)} - ${name}`;
		});
		text += `</ul>`;
		return text; 
	},
	reset(){
		this.items = [];
	},
}


function createStatus(input){
	const { subscribe, set, update, get } = writable(input);
	return {
		subscribe,
		update,
		get,
		initiate: () => update(s => {
			s.worn = allSlots.map( slot => subject[slot] ).filter( a => a );
			
			s.wornWealth = s.worn.reduce( (sum, item) => sum + (item.price ?? 0), 0 );
			s.originalWornWealth = s.wornWealth;
			s.wornWealthChange = 0;
			
			s.originalMoney  = game ? ext.game_money : ext.irl_money;
			s.money = s.originalMoney;
			s.moneyChange = 0;

			s.outfitManager = false;
			s.viewMerchant = trade;
			return s;
		}),
		
		switchViewMerchant: (on) => update(s => {
			if(on === undefined){
				s.viewMerchant = !s.viewMerchant;
			}else{
				s.viewMerchant = on;
			}
			return s;
		}),
		
		switchOutfitManager: (on) => update(s => {
			if(on === undefined){
				s.outfitManager = !s.outfitManager
			}else{
				s.outfitManager = on;
			}
			return s;
		}),
		
		check: () => update(s => {
			s.worn = allSlots.map( slot => subject[slot] ).filter( a => a );
			s.wornWealth = s.worn.reduce( (sum, item) => sum + (item.price ?? 0), 0 );
			s.wornWealthChange = s.wornWealth - s.originalWornWealth;
			return s;
		}),
		money: price => update(s => {
			//TODO - check for NaN?
			s.moneyChange += price;
			s.money = s.originalMoney + s.moneyChange;
			return s;
		}),
	}
}	


function createList(input){
	const { subscribe, set, update, get } = writable(input);

	return {
		subscribe,
		update,
		
		sort: (prop, algorithm = "abc", reversed = false) => update(list => {
			//sorting algorithms
			const algor = { 
				cba(a, b){
					const x = (`${a}`).toLowerCase().split("").reverse().join("");
					const y = (`${b}`).toLowerCase().split("").reverse().join("");
					if (x < y) return -1;
					if (x > y) return 1;
					return 0;
				},
				abc(a, b){
					const x = (`${a}`).toLowerCase();
					const y = (`${b}`).toLowerCase();
					if (x < y) return -1;
					if (x > y) return 1;
					return 0;
				},
				number(a, b){
					a ??= 0;
					b ??= 0;
					if (a < b) return -1;
					if (a > b) return 1;
				},
				
			}
			
			if(typeof algorithm === "string") algorithm = algor[algorithm];
			if( Array.isArray(prop) ){ //TODO - making prop array is really stupid
				  const [first, second] = prop;
					list.sort( (a,b) => algorithm(
						a[first] && a[first][second] ? a[first][second] : undefined, 
						b[first] && b[first][second] ? b[first][second] : undefined,
					) ); 
			}else{
				list.sort( (a,b) => algorithm(a[prop], b[prop])  );
			}
			if(reversed) list.reverse();
			return list;
		}),
		
		add: (item, count) => update(list => {
			if(item.id < 100){
				const index = list.findIndex( a => a.id === item.id && a.seed === item.seed);
				if(index !== -1){ //consumables - multiple exactly same items
					list[index].count ??= 1;
					if(count){ //count is defined X count is based on item.count //TODO - horrible to keep both but unable to decide which is better 
						list[index].count += count;
					}else if(item.count){
						list[index].count += item.count;
					}else{
						list[index].count++;
					}
				}else{
					list.push({...item}); //has to be cloned
					if(count) list[list.length - 1].count = count;
				}
			}else{
				list.push(item);
			}
			
			return list;
		}),
		
		//could be inputed ITEM {object} or INDEX {integer}
		remove: (input, count) => update(list => {
			const index = (()=>{
				if(typeof input === "number") return input;
				return list.findIndex( a => a.id === input.id && a.seed === input.seed);
			})();

			if(list[index] === undefined) return; //TODO!!!! NECESARY? Will cause unexpected beahviour?
			
			if(list[index].id < 100){ //consumable
				list[index].count ??= 1; //obviously has to have at least one? //TODO - THIS MIGHT CAUSE ISSUES 
				if(count){ 
					list[index].count -= count;
				}else if(input.count){
					list[index].count -= input.count;
				}else{
					list[index].count--;
				}
				
				if(list[index].count < 0){
					/* eslint-disable no-alert, no-console */
					if(debug) alert(`ERROR: You cannot have "${list[index].count}" of "${list[index].name}"!`);
					console.error(`ERROR: You cannot have "${list[index].count}" of "${list[index].name}"!`);
					/* eslint-enable no-alert, no-console */
				}
				if(list[index].count === 0){
					list.splice(index, 1);
				}
			}else{
				list.splice(index, 1);
			}
			return list;
		}),
	}
}	

/*
export function shop(merch, back, multiple){
	merch = INVENTORY.merchant(merch, multiple);
	menu(PC, back, merch);
}
*/

export function menu(sub = PC, back = ()=>{}, merch = [], options = {}){
	subject = sub; //has to be first!!!! (following use "subject"!
	game = head.reality === 3;
	saveWornOutfit(`original_clothes`, `clothes worn before opening inventory (auto)`, subject, 0); //saves original clothes - restored if inventory canceled

	figurine = cloneFigurine(subject);
	originalMerchant = merch;
	trade = !!originalMerchant.length;
	follower = sub.key !== "kat"; //TODO WACKY!!
	tax = options.tax ?? (game ? subject.att.barter / 100 : 0); //1.based on shop; 2.based on game stat; 3 selling not allowed
	done = back;
	
	checkOutfits();	
	
	status = createStatus({}); 
	status.initiate();
	
	allSlots.filter( a => subject[a] ).forEach( a => subject[a].owned = true ); //TODO - dumb way to do this
	//TODO - BEWARE - garments equiped via outfit manager are not recognised as owned!!!!
	
	originalInventoryCount = INVENTORY.get.map( a => a.count );
	inventory = createList( INVENTORY.get.map(a=> Object.assign(a,{owned: true}) ) );  //marks owned items with {owned: true} - there is difference between selling owned item and returning just bought item
	merchant = createList( originalMerchant.map(a => Object.assign(a,{owned: false}) ) ); //clones the array (so the original won't be altered and the trade could be canceled) and puts it in svelte store (to display reactively)
	
	inventory.sort("name", "abc"); //TODO - maybe default price?
	if(trade) merchant.sort("name", "abc");
	
	receipt.reset();
	
	//setTimeout(() => {
	DISPLAY.inventory();
	// }, 2000);
	
	if(sub.key === "kat"){
		KEYBOARD.back(cancel, constants.keyboard.inventory, true); 
	}else{
		KEYBOARD.back(cancel, constants.keyboard.follower_inventory, true); 
	}	 
}



export const cancel = function(){
	if(uncancelable) return; //cannot be canceled, has to be approved
		
	//KEYBOARD.force_clear_back();
	KEYBOARD.back_force = false; //BEWARE!! WACKY AND VERY IMPORTANT!  //TODO - NO FUCKING CLUE WHAT IS THE PURPOSE OF THIS!!!!!!!
			//KEYBOARD.escape = undefined; //should be immediately replaced, just to be double sure
				
	//original inventory  & merchant were not changed
	INVENTORY.recount(originalInventoryCount); //item.count was changed because "inventory" references the same objects as "INVENTORY"; 

	removeEverything(subject); 
	wearOutfit(`original_clothes`, subject, [], true); 
	
	update(subject);
	done();
	ULTRA.off(); //disables tooltip (maybe redundant but there were issues with it)
	
	drawUnzoomed(PC);
	DISPLAY.text();
}



export const approve = function(){
	KEYBOARD.force_clear_back(); //TODO?????
	
	if(game){
		//follower complaining 
		if(subject.follower){
			if( get(status).wornWealthChange < 0 ){
				popup.alert(`${subject.name} will not accept this exchange of equipement because ${subject.sex ? "she" : "he"} would lose ${ get(status).wornWealthChange } ☼. `);				
				return;
			}
		}
	}
	
	//money
	if( get(status).money < 0 ){
		popup.alert(`${game ? "Not enough coins! " : "Not enough balance on your account. "} ${SETTING.hint ? "(HINT: Stop being poor)" : ""}`);
		return;
	}else{
		if(game){
			ext.game_money = get(status).money;
		}else{
			//ext.irl_money = get(status).money; //IRL MONEY HANDLED VIA PAYMENT()
		}
	}

	INVENTORY.recreate( get(inventory) );
	
	//TODO - THIS IS PRETTY USELESS ATM
	originalMerchant = get(merchant).map( a => a );
	
	done(game ? undefined : get(status).moneyChange); //IRL MONEY HANDLED VIA PAYMENT() HERE
	ULTRA.off(); //disables tooltip (maybe redundant but there were issues with it)
	
	drawUnzoomed(PC); //todo, is redundant?
	DISPLAY.text(); 
}


//REMOVE EQUIPED ITEM (and move it to inventory)
export const unequip = function(item){
	if(item.locked) return; //locked items can't be unequiped
	
	inventory.add(item); //add the item to inventory
	removeActual(item, subject); //remove worn item from the avatar 
	updateDraw(subject); //rerenders avatar
	status.check(); //updates list of worn items
}

export const unequipAll = function(){
	allSlots.forEach( key => {
		if(!subject[key]) return;
		if(subject[key].locked) return;
		//if(allPiercings.includes(a)) return; //PIERCINGS ARE ATM THE ONLY DIFFERENCE
		inventory.add(subject[key]);
		removeActual(key);
	});
	updateDraw(subject);
	status.check();
}



//EQUIP ITEM FRON INVENTORY (replace equipped items)
const equipActual = function(item){
	let slot = item.slot;
	
	//unequips already worn (fucking complicated because dresses cover two slots)
	if(
		(slot == "upperLower") || (slot == "lower" && subject?.upper?.slot === "upperLower")
	){ //fucking dresses
	
		if(subject.upper?.locked || subject.lower?.locked) return; //LOCK TODO
				
		if(subject.upper){
			inventory.add( subject.upper );
			subject.upper = void 0;
		}
		if(subject.lower){
			inventory.add( subject.lower );
			subject.lower = void 0;
		}
		slot = "upper"
		
	}else if(subject[slot]){
		if(subject[slot].locked) return;
		inventory.add( subject[slot] );
	}
	
	//equips the item and removes it from the inventory 
	subject[slot] = item;
	inventory.remove(item, 1);
}

export const equipAll = function(array){
	array.forEach( item => equipActual(item) );
	updateDraw(subject);
	status.check();
}

export const equip = function(item){
	if(!item) return;
	if(item.slot === "charm") return; //charms cannot be used in inventory
	if(item.slot === "potion" || item.slot === "consumable"){ //consumables could be consumed
		if(trade){
			//cant drink potions while trading
		}else{
			uncancelable = true;
			POTIONS.drink(item, subject); 
			inventory.remove(item, 1);
		}
		return;
	}else{		
		equipActual(item);
		updateDraw(subject);
	}
	status.check();
}



export const random = function(){
	unequipAll();
	const clothes = randomClothes( get(inventory), subject, "", false);
	equipAll(clothes);
}

export const randomRules = function(){
	unequipAll();
	const clothes = randomClothes( get(inventory), subject, "", true);
	equipAll(clothes);
}

export const randomCustom = function(){
	unequipAll();
	let slut = ext.dressup.slut ;
	if(slut === -2) slut = undefined;
	if(slut === -1) slut = ext.slut;
	//const clothes = randomClothes( get(inventory), subject, "", true);
	//const clothes = random(slut, ext.dressup.rules, "custom");
	const clothes = randomBrutalClothes(slut, 10, get(inventory), subject);
	equipAll(clothes);
}

/*
export const randomClothesOutfit = function(){
	const id = randomOutfit(inventory, false); //rules = false
//console.warn(id)
	wearOutfit(id, subject, inventory);
}
*/



//SELL OWNED ITEM		
export const sell = function(item, count){
	ULTRA.off(); //disables tooltip
	
	//if there are multiple items, popups window with selection how many should be sold
	if(item.count && count === undefined){
		if(item.count === 1){
			return sell(item, 1);
		}else{
			return popup.splitter(item.name, item.count, (count)=>{
				if(count) sell(item, count);
			});
		}
	}
	
	//money
	let price = vat(item); //selling owned X canceling purchase
	if(count) price *= count;
	//moneyChange += price;
	status.money(price);
	receipt.log(item, -count ?? -1, price);
	
	//moves item from one list to the other
	merchant.add(item, count);
	inventory.remove(item, count);
}

//BUY MERCHANTS ITEM
export const buy = function(item, count){
	ULTRA.off(); //disables tooltip
	
	//if there are multiple items, popups window with selection how many should be sold
	if(item.count && count === undefined){
		if(item.count === 1){
			return buy(item, 1);
		}else{
			return popup.splitter(item.name, item.count, (count)=>{
				if(count) buy(item, count);
			});
		}
	}
	
	//price
	let price = vat(item); //canceling selling X purchase
	if(count) price *= count;
	status.money(-price)
	receipt.log(item, count ?? 1, -price);
	
	//moves item from one list to the other
	inventory.add(item, count);
	merchant.remove(item, count);
}




export const vat = function(item){
	if(!item?.price) return 0;
	
	if(trade && item.owned){
		if(!tax) return 0; //TODO - wouldnt be item.price better?
		return Math.round(item.price * tax) || 1;
	}else{
		return item.price;
	}
}





export const desc = function(item){
	const currency = game ? "☼" : "€";
	const value = game ? vat(item) : vat(item).toFixed(2);
	const original = (trade && item.owned) ? ` (original price ${game ? item.price : item.price.toFixed(2)}${currency})` : ``;
	const price = `${value}${currency}${original}`;
	/*
	const price = (()=>{
		if(trade){
			if(item.owned){
				return `${vat(item)}${currency} (original price ${item.price}${currency})`;
			}else{
				return `${item.price}${currency}`;
			}
		}else{
			return `${item.price}${currency}`;
		}
	})();
	*/
	
	const artefact = (()=>{
		let text = "";
		if(item.damage) text += `Total damage: ${item.damage}<br>`; //TODO - CALCULATION
		if(item.att){
			Object.entries(item.att).forEach( ([key,value]) => {
				text += `${capitalise(attributes[key].name)}: ${pm(value)}${attributes[key].percent ? "%" : ""}<br>`;
			});
		}
		return text;
	})();
	
	
	const quality = (()=>{
		let text = "";
		if(item.quality && !game){text += `Quality: ${
			itemQualityString[item.quality]
		}<br>`} //TODO - CALCULATION
		return text;
	})();
	
	const system = (()=>{
		if(!window.debug) return ``;
		return `
			#DAD: ${item.dad} <br>
			#Blueprint: ${item.blueprint} <br>
		`;
	})();
	
	
	return `
		<strong>${capitalise(item.name)}</strong><br>
		Price: ${price}<br>
		${quality}
		${artefact}
		${system}
	`
}




/*
	addMenu("⬇️","strip",(function(){
			unequipClothes();
		}),"Strips clothes");
		
		addMenu("⬇️","stripAll",(function(){
			unequipAll();
		}),"Strips everything including piercings");
		
		
		
		//FAI
		addMenu("a🎲","randomAllowed",(function(){
			unequipAll();
			const temp = randomClothes(inventory, subject, "", true);
			equipAll(temp);
			reload();
		}),"Puts on random clothes complying with rules");
		
		
		addMenu("X🎲","random",(function(){
			unequipAll();
			const temp = randomClothes(inventory, subject, "", false);
			equipAll(temp);
			reload();
		}),"Puts on random clothes");
			
			
			*/



export const checkOutfits = function(reload = false){
	outfits = ext.outfits.filter( a => {
		if(head.reality === 1 || head.reality === 2){		
			return a.reality === 1 || a.reality === 2;
		}else if(head.reality === 3){
			return a.reality === 3;
		}else{
			return false;
		}
	});
	category.set(""); //TODO - ASAP - HORRIBLE TEMPFIX
}

export const saveOutfit = function(){
	//const name = prompt(`Enter the name`, `Outfit ${outfits.length + 1}`);
	
	const no = ext.outfits.filter( a => a.reality === head.reality).length + 1;
	popup.prompt(`Enter the name`, `Outfit ${no}`, save);
	
	function save(name){
		if (!name) return;
		const id = var_name(name);
		saveWornOutfit(id, name, subject, head.reality);
		checkOutfits(true); //reloads the variable "outfits" now with the newly saved one
	}
}

/*
function loadOutfit(index){
	unequipAll();
	for(let a of outfits[index].items){
		a = unpackGarment(a);
		const i = inventory.findIndex( b => a.seed === b.seed && a.id === b.id );
		if(i !== -1){
			equip(i);
		}
	}
	//reload("OUTFITS");				
	updateDraw(subject);
}



function deleteOutfit(index){
	outfits.splice(index, 1);
	//reload("OUTFITS");
}
*/
