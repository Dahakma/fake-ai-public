import { writable, derived } from 'svelte/store';
//import {constants} from "System/variables";


export {ULTRA, tooltip} from "Gui/tooltip.js" //TODO
export {popup} from "Libraries/popup/index.js" //TODO

//export {popup} from "Gui/popup.js" //TODO
	
export const allow_to_continue = writable(true);


import {preprocess} from "Loop/preprocess_text";
import {NPC, NAME} from "System/variables";
//console.error(NPC);
//console.error(NAME);
import {popup} from "Libraries/popup/index.js";
import {stored_mile} from "System/variables";

function subslut(a){
	const basic = ["slut","sub","stat"];
	const all = [...basic,
		"sas_sassy", 
		"sas_exploit", 
		"eva_exploit", 
		"eva_nice",
		"a_dom",
		"a_love",
		"a_hate",
		"b_mas",
		"b_cruel",
		"immersion",
		"grades",
	];
	
	const list = [];
	let change = 0; 
	function aye(color, before, plus, minus, after = ""){
			const middle = change > 0 ? plus : minus;
			after = after ? ` ${after}.` :  ". ";
			list.push( preprocess(`<span style = "color: ${color}"> ${before} <strong>${middle}</strong>${after}</span>`) );
		}
	
	
	all.forEach( key => {
		change = (()=>{
			//check if the values changed since the last time (to display fancy triangle)
			if( stored_mile[key] !== mile[key]){
				return stored_mile[key] < mile[key] ? 1 : -1;
			}else{
				return 0;
			}
		})();
		a.subslut[key] = change;
		
		if(change && SETTING.notifications_mile){
			switch(key){
				case "slut": return aye("#f0c", "I felt", "sluttier", "chastier");
				case "sub": return aye("#0084ff", "I felt", "submissive", "dominant");
				case "stat": return aye("#8c00ff", "My reputation", "increased", "dropped");
				default: break;

			}
		}
		
		if(change && SETTING.notifications_test){
			switch(key){

			//SAS
				case "sas_sassy": return aye(NPC.sas.color, `${NAME.sas} felt`, "confident", "discouraged");
				case "sas_exploit": return aye(NPC.sas.color, `${NAME.sas} felt`, "annoyed", "");
				case "sas_love": return aye(NPC.sas.color, `${NAME.sas} `, `loved me more`, `loved me less`);

			//EVEA
				case "eva_nice": return aye(NPC.eva.color, `${NAME.eva} `, "appreciated", "", "that");
				case "eva_exploit": return aye(NPC.eva.color, `${NAME.eva} `, "will remember", "", "that");
				
			//QAA
				case "a_dom": return aye(NPC.qaa.color, `${NAME.qaa} `, "felt", 	"confident", "obliging"); //TODO WROING
				case "a_love": return aye(NPC.qaa.color, `${NAME.qaa} `,  `loved me more`, `loved me less`);
				case "a_hate": return aye(NPC.qaa.color, `${NAME.qaa} `,  `hated me more`, `hated me less`);
				//a_cuck

			//QBB
				case "b_mas": return aye(NPC.qbb.color, `I became more `, `masochistic`, ``);
				case "b_cruel": return aye(NPC.qbb.color, `I became `, `more cruel`, `less cruel`);
	
			//GAME 
				case "immersion": return aye("navy", `I `, `enjoyed`, `disliked`, `the Game`);
				case "grades": return aye("orange", `My <strong>grades</strong> got `, `worse`, `better`);
	
				case "lesbian": return aye("#FF00FF", `I became`, `more`, `less`, `attracted to <strong>girls</strong>`);
				case "maj_exploit": return aye(NPC.maj.color, `${NAME.maj} `,  `will remember`, ``, `that`);
			//	case "sas_sassy": return aye(NPC.sas.color, NAME.sas+" feels", "confident", "discouraged");
				default: break;

			}
		}

	});
	
	if(list.length) popup.note(list.join("<br>"));
		
}


function create_display(input){
	const { subscribe, set, update, get } = writable(input);

	return {
		subscribe,
		update,
	
	
	//CHANGES WINDOW
		subtext_only: window => update(a => {
			a.display.window = window;
			a.display.side = false;
			return a;
		}),
		
		menu: ()=> update(a => {
			a.display.window = "menu";
			a.display.side = false;
			return a;
		}),
		
		dress_up: (temp) => update(a => {
			a.display.window = "text";
			a.display.subwindow = "morning";
			a.display.actions = true;
			a.display.text = true;
			a.temp = temp; //TODO - using temp for this is dumb
			return a;
		}),
		
		minigame: (minigame) => update(a => {
			a.display.window = "text";
			a.display.subwindow = "minigame";
			a.minigame = minigame;
			return a;
		}),
		
		minigame_set: (minigame) => update(a => {
			a.minigame = minigame;
			return a;
		}),
		
		text: (sub) => update(a => {
			a.display.window = "text";
			if(sub?.pointerType) sub = undefined; //input is pointerEvent automatically created by Svelte when left undefined (which I wanted, to leave it undefined)
			if(sub !== undefined){
				a.display.subwindow = sub;
				a.display.actions = true;
				a.display.text = true;
			}
			if(a.display.subwindow === "level_up"){
				a.display.actions = false;
				a.display.text = false;
			}
			a.display.side = true;
			
			return a;
		}),
		
		subtext: window => update(a => {
			if(window === "character") window = head.reality === 3 ?  "character_game" : "character_irl";
			a.display.window = window;
			a.display.side = true;
			return a;
		}),	
		
		combat: () => update(a => {
			a.display.window = "combat";
			return a;
		}),
		
	//AVATAR
		edit_avatar: (subject = PC, subwindow = "basic", back)  => update(a => {
			a.display.window = "edit";
			a.avatar.subwindow = subwindow;
			a.avatar.subject = subject;
			a.avatar.back = back;
			return a;
		}),
		
		tattoo: (options, subject = PC, subwindow = "slot", back)  => update(a => {
			a.display.window = "tattoo";
			a.avatar.options = options;
			a.avatar.subwindow = subwindow;
			a.avatar.subject = subject;
			a.avatar.back = back;
			return a;
		}),
		
		designer: (options, subject = PC, subwindow = "", back)  => update(a => {
			a.display.window = "designer";
			a.avatar.options = options;
			a.avatar.subwindow = subwindow;
			a.avatar.subject = subject;
			a.avatar.back = back;
			return a;
		}),
		
		inventory: ()  => update(a => {
			a.display.window = "inventory";
			/*
			a.avatar.subject = subject; 
			a.avatar.inventory = inventory; //default set in Inventory.svelte
			a.avatar.merchant = merchant;
			a.avatar.back = back;
			*/
			return a;
		}),
		
	//WRITING STORY CONTENT
		text_clr: () => update(a => {
			a.display.window = "text";
			//a.display.subwindow = "";
			a.display.side = true;
			a.display.links = true;
			a.content.paras = [];
			a.content.links = [];
			return a;
		}),
		
		text_content: content => update(a => {
			a.content.paras = content.paras;
			a.content.links = content.links;
			a.content.nolinks = content.nolinks;
			a.content.done = content.done;
			return a;
		}),
		
	
		
		
	//MORNING
	/*
		morning: (active = true) =>  update(a => {
			a.display.morning = active;
			return a;
		}),
		*/
		display_links: value => update(a => {
			a.display.links = value;
			return a;
		}),
		


		
	//DUNGEON
		dungeon: ()  => update(a => {
			a.display.bars = true;
			a.display.rose = true;
			return a;
		}),
		
		dungeon_leave: ()  => update(a => {
			a.display.bars = false;
			a.display.rose = false;
			return a;
		}),
		
		rose_on: ()  => update(a => {
			if(window.ext.dungeon) a.display.rose = true; //TODO - in case map accessible outside of dungeon
			return a;
		}),
		rose_off: ()  => update(a => {
			a.display.rose = false;
			return a;
		}),
		
		bars_on: ob  => update(a => {
			if(ob){
				ext.bars = ob; //TODO USING EXT.BARS IS WRONG!!
			}else{
				ext.bars = undefined;
			}
			a.display.bars = true;
			return a;
		}),
		bars_off: ()  => update(a => {
			a.display.bars = false;
			ext.bars = undefined; //TODO beware
			return a;
		}),
		
	//
	

		
		
		
	//UPDATES THE STORE WITH CURRENT PC & HEAD & STUFF (IMPORANT)
		check: () =>  update(a => {
			subslut(a); //checks subslut stats; has to happen before a.mile is update
			a.head = window.head;
			a.PC = window.PC;
			a.ext = window.ext;
			a.mile = window.mile;
			a.counter = window.counter;
			a.setting = window.SETTING;
			return a;
		}),
		
		
		//puts whatever you want to temp
		temp: t =>  update(a => {
			a.temp = t;
			return a;
		}),
		
		
		//tries to return the store to initial values; 
		//(respective the ones that might break stuff and are not updated immediately) 
		initial: () => update(a => {
			a.display = {
				window: "text",
				links: true,
				side: true,
				avatar: true,
				subslut: true,
				attributes: false,
				morning: false,
				rose: false,
				bars: false,
				text: true,
				actions: true,
				//follower: false,
				//subwindow: "",
			};
			a.subslut = {
				/*previous: {
					sub: mile.sub,
					slut: mile.slut,
					stat: mile.stat,
				},
				change: {*/
					sub: 0,
					slut: 0,
					stat: 0,
				/*},*/
			};
			return a;
		}),
		
		
	}
}	
		
		
export const DISPLAY = create_display({
	PC: {},
	ext: {},
	mile: {},
	head: {},
	counter: {},
	setting: {},
	temp: undefined,
	avatar: {
		subject: PC, //edited avatar
		subwindow: "edit", //
		inventory: [],
		merchant: [],
		back: ()=>{},
	},
	back: ()=>{},
	
	content: {
		paras: [],
		links: [],
		nolinks: [],
		done: ()=>{},
	},
	
	display: {
		window: "text",
		links: true,
		side: true,
		avatar: true,
		subslut: true,
		attributes: false,
		morning: false,
		rose: false,
		bars: false,
		above: false,
		//follower: false,
		subwindow: "",
		minigame: "",
	},
	
	minigame: {},
	
	//track changes of values to display fancy triangles
	subslut: {
		/*
		previous: {
			sub: 0,
			slut: 0,
			stat: 0,
		},
		change: {
			*/
			sub: 0,
			slut: 0,
			stat: 0,
		/*},*/
	},
}); 


export function scroll_top(){
	//scrolls to the top
	document.body.scrollTop = 0; // For Safari
	document.documentElement.scrollTop = 0; // For Chrome a Firefox
}