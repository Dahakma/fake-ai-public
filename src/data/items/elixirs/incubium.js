import {ra} from "Libraries/random";
import {s, gradual} from "Libraries/dh";

export const xInc = {
	name: `incubium`,
	note: `manly, demonic features`,
	effect: `infeb`,
	taste: [
		`The potion was strong and spicy but sinfully delicious. `,
		`The potion was hot and spicy. `,
		`The potion had a spicy flavour. `,
		`The taste of the potion was very strong and spicy. `,
	],
	trans: [
		//PURPLE SKIN
		{	
			name: `demonic purple skin`,
			desc: `My whole body turned purple! Absolutely everywhere! `,
			npc: (n,x) =>  `We both amazed watched how ${s(n)} turned purple! `,
			condit(dim, subject){
				return dim.skinHue < 230 ||  dim.skinHue > 280;
			},
			execute(dim, subject){ return {
				absolute: {
					skinHue: ra.range(250, 280),
					skin: ra.range(3, 12),
					skinSaturation: ra.range(5, 40),
				},
			}},
		},
		
		//INDIGO SKIN
		{
			name: `demonic indigo skin`,
			desc: `My skin began to tingle and I could see the shade changing to dark blue indigo. `,
			npc: (n,x) =>  `We both amazed watched how ${s(n)} turned indigo! `,
			condit(dim, subject){
				return dim.skinHue < 230 ||  dim.skinHue > 280;
			},
			execute(dim, subject){ return {
				absolute: {
					skinHue: ra.range(230, 250),
					skin: ra.range(10, 15),
					skinSaturation: ra.range(0, 40),
				},
			}},
		
		},
		
		//PURPLE HAIR
		{
			name: `purple hair`,
			desc: `My hair began to tingle. `,
			npc: (n,x) =>  `${n} did not notice until I warned ${x ? `her` : `him`} - ${x ? `her` : `his`} hair now were purple. `,
			condit(dim, subject){
				return dim.hairHue < 270 || dim.hairHue > 290;
			},
			execute(dim, subject){ return {
				absolute: {
					hairHue: ra.range(270, 290),
					hairSaturation: ra.range(90, 100),
					hairLightness: ra.range(30, 60),
				},
			}},
			
		},
		
		//MUSCULAR
		{
			name: `muscular`,
			desc: `I became more muscular. `,
			npc: (n,x) =>  `${n} became more buff and muscular. `,
			condit(dim){
				return gradual([0, 10, 30], dim.upperMuscle, [2, 0]);
			},
			execute(dim, subject){ return {  
				relative: {
					//muscles
					upperMuscle: gradual([0, 30], dim.upperMuscle, [10, 5]),
					shoulderWidth: dim.shoulderWidth < 85 ? 1 : 0,
					
					//buffness
					height: gradual([150, 165, 175], dim.height, [1, 0]), //TODO
					neckWidth: dim.neckWidth < 40 ? 2 : 0,
					neckLength: dim.neckLength > 75 ? -2 : 0,
				},
			}},
		},
		
		//HORNS
		{
			name: `horns`,
			desc: `My head started to hurt and when I touched my forehead I found out I am an owner of two straight horns. `,
			npc: (n,x) =>  `Two straight horns sudenly spurted from ${s(n)} head.  `,
			condit(dim, subject){
				return subject.apex.horns !== `straight` || dim.hornyness < 1;
			},
			execute(dim, subject){ return {  
				relative: {
					hornyness: 1.5,
				},
				apex: {
					horns: `straight`,
				},
			}},
			
		},
		
		//WINGS
		{
			name: `wings`,
			desc: `I gained a pair of majestic demonic wings. `,
			npc: (n,x) =>  `Amazed I watched how ${n} spread ${x ? `her` : `his`} new majestic wings.  `,
			condit(dim, subject){
				return 0.5 * !!(dim.wings < 0.5);
			},
			execute(dim, subject){ return {  
				relative: {
					wings: 0.9,
				},
				apex: {
					wings: `demo`,
				},
			}},
		},
		
		//TAIL
		{
			name: `tail`,
			desc: `I drank it and from my behind grew a very long tail. Like what the fuck am I supposed to do with a tail?`,
			npc: (n,x) =>  `${n} gasped in shock. A long, thin tail grew from ${x ? `her` : `his`} backside.  `,
			condit(dim, subject){
				return dim.tail < 0.5 || subject.apex.tail !== `demo`;
			},
			execute(dim, subject){ return {  
				relative: {
					tail: 1,
				},
				apex: {
					tail: `demo`,
				},
			}},
		},
	],
}