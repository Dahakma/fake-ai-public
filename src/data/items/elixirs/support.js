import {ra} from "Libraries/random";
import {gradual} from "Libraries/dh";
import {ATTRIBUTES} from "Virtual/index";
import {EFFECTS} from "Virtual/index";
import {constants} from "Data/constants";

export const xHP = {
	name: `health potion`,
	//support: true,
	taste:[`The potion was tasteless but instantly made me feel better. `],
	trans: [
		{
			name: `heal`,
			desc: `All the pain disappeared and I noticed my health bar was fuller. `,
			npc: (n,x) => `The health bar of ${n} nicely filled up. `,
			condit(dim, subject){
				return true;
			},
			fce(subject){
				ATTRIBUTES.change(`hp`, constants.hp.elixir, subject);
			},
		},
	],
}


export const xEng = {
	name: `energy potion`,
	//support: true,
	taste:[`The potion was a bit sour but very reinvigorating. `],
	trans: [
		{
			name: `energy`,
			desc: `I felt revved up, my weariness was gone and my energy bar a bit fuller. `,
			npc: (n,x) => `The energy bar of ${n} nicely filled up. `,
			condit(dim, subject){
				return true;
			},
			fce(subject){
				ATTRIBUTES.change(`eng`, constants.eng.elixir, subject);
			},
		},
	],
}


export const xAnti = {
	name: `antivenom`,
	support: true,
	taste:[`The was transparent and only a bit bitter. `],
	trans: [
		{
			name: `cure poison`,
			desc: `The potion cured all poisons. `,
			npc: (n,x) => `The potion cured all poisons. `,
			condit(dim, subject){
				return true;
			},
			fce(subject){
				EFFECTS.remove("poisoned");	//TODO
				//if(ext.dungeon?.poison) ext.dungeon.poison = false; //TODO!!!
			},
		},
	],
}


/*
export const poison = {
	name: `poison`,
	support: true,
	taste:[` `],
	effects: [{
			name: ``,
			desc: `<p></p>`,
			condit(dim, subject){
				return true;
			},
	}]
}
*/