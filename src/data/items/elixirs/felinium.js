import {ra} from "Libraries/random";
import {s, gradual} from "Libraries/dh";

export const xFel = {
	name: `felinium`,
	note: `cat features, lean`,
	taste: [`The potion had milky consistency. `], 
	effect: `grace`,
	trans: [
		//LEAN
		{
			name: `lean`,
			desc: `My body became thinner and leaner. `,
			npc: (n,x) =>  `${s(n)} became thinner and leaner.  `,
			condit(dim){
				return gradual([15, 30, 40], dim.upperMuscle, [0, 2]);
			},
			execute(dim, subject){ return {  
				relative: {
					//height
					height: gradual([160, 180], dim.height, [0, -5], 1),
					
					//muscles
					upperMuscle: gradual([10, 40], dim.upperMuscle, [-5, -10], 1),
					shoulderWidth: dim.shoulderWidth > 63 ? -2 : 0,
					
					//buffness
					neckWidth: dim.neckWidth > 42 ? -3 : 0,
					neckLength: dim.neckLength < 70 ? 2 : 0,
					
					//breasts
					breastSize: gradual([15, 40, 120], dim.breastSize, [-2, -10], 1),
					breastPerkiness: gradual([15, 40, 120], dim.breastPerkiness, [0, -3], 1),
					
					waistWidth: dim.waistWidth > 105 ? -3 : 0,
				},
			}},
		},
		
		//EARS
		{
			name: `cat ears`,
			desc: `<p>I gained a new pair of fluffy cat ears. </p>`,
			npc: (n,x) =>  `${n} gain a pair of brand new cat-like ears. `,
			condit(dim, subject){
				return subject.apex.ears !== `cat`;
			},
			execute(dim, subject){ return {  
				relative: {
					earlobeLength: 3,
				},
				apex: {
					ears: `cat`,
				},
					
			}},
		},
		
		//TAIL
		{
			
			name: `cat tail`,
			desc: `My tailbone started to hurt. I could feel a long furry tail slowly and elegantly growing out of my body. `,
			npc: (n,x) =>  `${n} was confused what was going on. Only I could see a long furry tail spurting from ${x ? `her` : `his`} lower back.`,
			condit(dim, subject){
				return subject.apex.tail !== `cat`;
			},
			execute(dim, subject){ return {  
				relative: {
					tail: 1,
				},
				apex: {
					tail: `cat`,
				},	
			}},
			
		},
		
		//PUPILS
		{
			name: `cat pupils`,
			desc: `My vision changed, the colors faded but everything seemed brighter. `,
			npc: (n,x) =>  `When ${n} looked straight at me, I noticed ${x ? `her` : `his`} eyes gained a cat-like vertical pupils.`,
			condit(dim, subject){
				return subject.apex.pupils !== `cat`;
			},
			execute(dim, subject){ return {  
				relative: {
					irisSize: 2,
					eyeSize: 2,
				},
				absolute: {
					irisSaturation: 90,
					irisLightness: 60,
				},
				apex: {
					pupils: `cat`,
				},	
			}},
		},
	],
}