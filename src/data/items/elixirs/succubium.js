import {ra} from "Libraries/random";
import {s, gradual} from "Libraries/dh";

export const xSuc = {
	name: `succubium`,
	note: `sexy, demonic features`,
	effect: `infeb`,
	taste: [
		`The potion was strong and spicy but sinfully delicious. `,
		`The potion was hot and spicy. `,
		`The potion had a spicy flavour. `,
		`The taste of the potion was very strong and spicy. `,
	],
	trans: [

	//RED SKIN
		{
			name: `demonic red skin`,
			desc: `My whole body turned red! I looked like some kind of Martian. `,
			npc: (n,x) =>  `We both amazed watched how ${s(n)} turned red! `,
			condit(dim, subject){
				return dim.skinHue < 290;
			},
			execute(dim, subject){ return {
				absolute: {
					skinHue: ra.range(330, 360),
					skin: ra.range(6, 12),
					skinSaturation: ra.range(10, 40),
				},
			}},
		},
		
	//PINK SKIN
		{
			name: `demonic pink skin`,
			desc: `My skin began to tingle and I could see the shade changing to gaudy pink. `,
			npc: (n,x) =>  `We both amazed watched how the shade of ${s(n)} skin changed to gaudy pink! `,
			condit(dim, subject){
				return dim.skinHue < 290;
			},
			execute(dim, subject){ return {
				absolute: {
					skinHue: ra.range(290, 320),
					skin: ra.range(3, 12),
					skinSaturation: ra.range(0, 40),
				},
			}},
		},
		
	//PINK HAIR
		{
			
			name: `pink hair`,
			desc: `My hair began to tingle. `,
			npc: (n,x) =>  `${n} did not notice until I warned ${x ? `her` : `him`} - ${x ? `her` : `his`} hair now were purple. `,
			condit(dim, subject){
				return dim.hairHue < 310 || dim.hairHue > 340;
			},
			execute(dim, subject){ return {
				absolute: {
					hairHue: ra.range(310, 340),
					hairSaturation: ra.range(90, 100),
					hairLightness: ra.range(40, 60), 
				},
			}},
		},
		
	//TITS
		{
			name: `big boobs`,
			desc: `I stumbled. My breasts were quickly expanding and getting bigger and heavier.`,
			npc: (n,x) =>  `We could only watch how ${s(n)} breast instantly began expanding and were quickly getting bigger and perkier. `,
			condit(dim){
				return gradual([0, 40, 60], dim.breastSize, [2, 0], -1);
			},
			execute(dim, subject){ return {
				relative: {
					breastSize: 13,
					breastPerkiness: dim.breastSize > 30 ? 10 : 5,
				},
			}},
		},
		
	//CURVY
		{
			name: `curvy`,
			desc: `I finished the potion and watched my body gaining new sexy curves. `,
			npc: (n,x) =>  `${n} finished the potion and we could watch ${x ? `her` : `his`} body gaining new sexy curves.`,
			condit(dim){
				return gradual([0, 40, 60], dim.breastSize, [2, 0], -1);
			},
			execute(dim, subject){ return {
				relative: {
					breastSize: 4,
					breastPerkiness: 1,
					waistWidth: dim.waistWidth > 100 ? -4 : 0,
					
					legFem: dim.legFem < 50 ? 8 : 0,
					buttFullness: dim.buttFullness < 45 ? 8 : 0,
					legFullness: dim.legFullness < 45 ? 7 : 0,
				},
			}},
		},
		
	//HORNS
		{
			name: `horns`,
			desc: `My head started to hurt and when I touched my forehead I found out I am an owner of two cute horns. `,
			npc: (n,x) =>  `Two straight horns sudenly spurted from ${s(n)} head.  `,
			condit(dim, subject){
				return subject.apex.horns !== "straight" || dim.hornyness < 1;
			},
			execute(dim, subject){ return {
				relative: {
					hornyness: 1,
				},
				apex: {
					horns: "straight",
				},
			}},
		},
		
		
	//WINGS
		{
			name: `wings`,
			desc: `I gained a pair of majestic demonic wings. `,
			npc: (n,x) =>  `Amazed I watched how ${n} spread ${x ? `her` : `his`} new majestic wings.  `,
			condit(dim, subject){
				return 0.5 * !!(dim.wings < 0.5);
			},
			execute(dim, subject){ return {
				relative: {
					wings: 0.75,
				},
				apex: {
					wings: "demo",
				},
			}},
		},
		
	//TAIL
		{
			name: `tail`,
			desc: `I drank it and from my behind grew a very long tail. Like what the fuck am I supposed to do with a tail?`,
			npc: (n,x) =>  `${n} gasped in shock. A long, thin tail grew from ${x ? `her` : `his`} backside.  `,
			condit(dim, subject){
				return dim.tail < 0.5 || subject.apex.tail !== "demo";
			},
			execute(dim, subject){ return {
				relative: {
					tail: 1,
				},
				apex: {
					tail: "demo",
				},
			}},
		},
	],
}