import {ra} from "Libraries/random";
import {s, gradual} from "Libraries/dh";

export const xMin = {
	name: `minotaurium`,
	note: `big, muscular, horny`,
	effect: `forti`,
	taste:[
		`The potion had milky consistency. `,
	],
	trans: [
		/*
		{
			//STRENGTH
			name: `bovine fortitude`,
			desc: `<p>After I drank the elixir, I felt great, more powerful and tougher. </p>`,
			npc: (n,x) =>  `${n} now seemed stronger and more powerful. `,
			condit(dim, subject){
				return subject.perks.includes(`forti`) ? 0 : 0.05;
			},
			perk: `forti`,
		},
		*/
	//HUGE
		{
			name: `huge`,
			desc: `<p>My perspective suddenly changed. I grew taller.  </p>`,
			npc: (n,x) =>  `${n} grew several centimeters taller. `,
			condit(dim){
				return gradual([150, 165, 175], dim.height, [2, 0]);
			},
			execute(dim, subject){ return {
				relative: {
					height: gradual([150, 165, 175], dim.height, [6, 2]),
					shoulderWidth: dim.shoulderWidth < 87 ? 2 : 0,
					upperMuscle: dim.upperMuscle < 40 ? 4 : 0,
					
					neckWidth: dim.neckWidth < 55 ? 1 : 0,
					neckLength: dim.neckLength > 55 ? -2 : 0,
				},
			}},
		},
	
	//BUFF
		{
			name: `buff`,
			desc: `<p>I drank the potion and could watch my growing new musculature. However, I did not feel much stronger. </p>`,
			npc: (n,x) =>  `${n} became more muscular. `,
			condit(dim){
				return gradual([0, 25, 40], dim.upperMuscle, [2, 0]);
			},
			execute(dim, subject){ return {
				relative: {
					//muscles
					upperMuscle: gradual([0, 40], dim.upperMuscle, [12, 6], -1),
					shoulderWidth: dim.shoulderWidth < 87 ? 2 : 0,
					
					//buffness
					height: gradual([150, 165, 175], dim.height, [2, 1]),
					neckWidth: dim.neckWidth < 55 ? 2 : 0,
					neckLength: dim.neckLength > 55 ? -2 : 0,
					
					breastSize: dim.breastSize > 0 ? -3 : 0,
					waistWidth: dim.waistWidth < 120 ? 5 : 0,
				},
			} },
		},
		
	//HORNS
		{
			name: `horns`,
			desc: `<p>I felt a short sharp pain. I could feel two huge horns growing from my head. </p>`,
			npc: (n,x) =>  `${n} drank the potion and gained a pair or huge curved horns. `,
			condit(dim, subject){
				return subject.apex.horns !== `curvy` || dim.hornyness < 1;
			},
			execute(dim, subject){ return {
				relative: {
					hornyness: 2,
				},
				apex: {
					horns: `curvy`,
				},
			}},	
		},
	],
}