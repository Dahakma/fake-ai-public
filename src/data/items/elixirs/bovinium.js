import {ra} from "Libraries/random";
import {s, gradual} from "Libraries/dh";

export const xBow = {
	name: `bovinium`,
	note: `Tall, muscular, tits, wide hips, horny`,
	taste: [`The potion had milky consistency. `], 
	effect: `forti`,
	trans: [
		//TITS
		{		
			name: `big boobs`,
			desc: `My breast began expanding. `,
			npc: (n,x) =>  `${s(n)} breast began expanding. .  `,
			condit(dim){
				return gradual([0, 50, 100], dim.breastSize, [1.5, 0]);
			},
			execute(dim, subject){ return {
				relative: {
					breastSize: 15,
					breastPerkiness: dim.breastSize > 50 ? 10 : 5,
				},
			}},	
		},
		
		//ASS
		{
			name: `wide hips`,
			desc: `My hips and ass widened. `,
			npc: (n,x) =>  `${s(n)} hips and ass widened.  `,
			condit(dim){
				return gradual([100, 130, 160], dim.hipWidth, [1.5, 0])
			},
			execute(dim, subject){ return {
				relative: {
					buttFullness: dim.buttFullness < 80 ?  8 : 0,
					legFullness: dim.legFullness < 40 ?  10 : 0,
					legFem: dim.legFem < 50 ?  2 : 0,
					hipWidth: dim.hipWidth < 160 ?  8 : 0,
					height: 0.2,
				},
			}},	
		},
		
		//HORNS
		{
			name: `horns`,
			desc: `I felt a short sharp pain. I could feel two huge horns growing from my head. `,
			npc: (n,x) =>  `I watched a pair of curved horns growing from ${s(n)} head.  `,
			condit(dim, subject){
				return subject.apex.horns !== `curvy` || dim.hornyness < 1;
			},
			execute(dim, subject){ return {
				relative: {
					hornyness: 1.7,
				},
				apex: {
					horns: `curvy`,
				},
			}},		
		},
	],
}