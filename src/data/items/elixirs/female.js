import {ra} from "Libraries/random";
import {s, gradual} from "Libraries/dh";

export const xFemale = {
	name: `female essence`,
	note: `womanly`,
	taste:[``],
	trans: [
		//SHORTER
		{	
			name: `short`,
			desc: `I drank it and for a moment I felt a bit dizzy. When I collected myself I noticed the everything around me seemed larger. </p><p>However, the further examination showed it was actually me who shrank. `,
			npc: (n,x) =>  ``,
			condit(dim){
				return 0.5 * +(dim.height > 170);
			},
			execute(dim, subject){ return {  
				relative: {
					height: -4,
					shoulderWidth: dim.shoulderWidth > 65 ? -1 : 0,
					upperMuscle: dim.upperMuscle > 0 ? -2 : 0,
				},
			}},
		},
		
		//SOFT
		{
			name: `soft`,
			desc: ``,
			npc: (n,x) =>  ``,
			condit(dim){
				return gradual([10, 30, 40], dim.upperMuscle, [0, 2]);
			},
			execute(dim, subject){ return {  
				relative: {
					//muscles
					upperMuscle: gradual([10, 40], dim.upperMuscle, [-5, -10]),
					shoulderWidth: dim.shoulderWidth > 65 ? -1 : 0,
					
					//buffness
					//height: gradual([150, 165, 175], dim.height`), [1, 0]), //TODO
					neckWidth: dim.neckWidth > 50 ? -2 : 0,
					neckLength: dim.neckLength < 65 ? 2 : 0,

					waistWidth: dim.waistWidth > 100 ? -2 : 0,

				},
			}},
		},
		
		//BREASTS
		{
			name: `breasts`,
			desc: ``,
			npc: (n,x) =>  ``,
			condit(dim){
				return gradual([0, 20, 50], dim.breastSize, [2, 0], -1);
			},
			execute(dim, subject){ return {
				relative: {
					breastSize: 10,
					breastPerkiness: dim.breastSize > 20 ? 3 : 2,
				},
			}},
			
		},
		
		//NO PENIS
		{
			
			name: `no penis`,
			desc: ``,
			npc: (n,x) =>  ``,
			condit(dim){
				return gradual([0, 50, 200], dim.penisSize, [0, 2]);
			},
			execute(dim, subject){ return {
				relative: {
					penisSize: -60,
					penisThickness: -4,
					testicleSize: -40,
				},
			}},
		},
	],
}