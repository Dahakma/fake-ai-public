import {ra} from "Libraries/random";
import {s, gradual} from "Libraries/dh";

export const xMale = {
	name: `male essence`,
	note: `manly`,
	taste:[``],
	trans: [
		//HEIGHT
		{
			name: `height`,
			desc: `My perspective suddenly changed. I grew taller. `,
			npc: (n,x) =>  ``,
			condit(dim){
				return 0.5 * +(dim.height < 160);
			},
			execute(dim, subject){ return {
				relative: {
					height: 4,
					shoulderWidth: dim.shoulderWidth < 85 ? 1 : 0,
					upperMuscle: dim.upperMuscle < 30 ? 2 : 0,
				},
			}},
		},
		
		//MUSCULAR
		{
			name: `muscular`,
			desc: `I became more muscular. `,
			npc: (n,x) =>  ``,
			condit(dim){
				return gradual([0, 10, 30], dim.upperMuscle`), [2, 0]);
				//return dim.height`) > 150;
			},
			execute(dim, subject){ return {
				relative: {
					//muscles
					upperMuscle: gradual([0, 30], dim.upperMuscle, [10, 5]),
					shoulderWidth: dim.shoulderWidth < 85 ? 1 : 0,
					
					//buffness
					height: gradual([150, 165, 175], dim.height, [1, 0]), //TODO
					neckWidth: dim.neckWidth < 40 ? 2 : 0,
					neckLength: dim.neckLength > 75 ? -2 : 0,
					
					waistWidth: dim.waistWidth < 110 ? 4 : 0,
				},
			}},
		},
		
		//BREASTS
		{		
			name: `no breasts`,
			desc: `Oh! My chest began shrinking! `,
			npc: (n,x) =>  ``,
			condit(dim){
				return gradual([0, 30, 120], dim.breastSize, [0, 2]);
			},
			execute(dim, subject){ return {
				relative: {
					//height: gradual([150, 165, 180], dim.breastSize, [-2, -6]),
					breastSize: -15,
					breastPerkiness: dim.breastSize > 20 ? -3 : -4,
				},
			}},
		},
		
		//PENIS
		{
			name: `penis`,
			desc: ``,
			npc: (n,x) =>  ``,
			condit(dim){
				return gradual([0, 50, 200], dim.penisSize, [1.5, 0]);
			},
			execute(dim, subject){ return {
				relative: {
					//height: gradual([150, 165, 180], dim.breastSize`), [-2, -6]),
					penisSize: 60,
					penisThickness: 4,
					testicleSize: 40,
				},
			}},
		},
	],
}