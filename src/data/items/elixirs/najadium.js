import {ra} from "Libraries/random";
import {s, gradual} from "Libraries/dh";

export const xNaj = {
	name: `najadium`,
	unlisted: true,
	note: `blue`,
	taste:[``],
	trans: [
		//BLUE SKIN
		{
			name: `blue skin`,
			desc: `In the water reflection I noticed my skin was turning blue. The spring had to be enchanted by naiads. `,
			npc: (n,x) =>  `${n} finished drinking and we watched how ${x ? `her` : `his`} skin turned blue.  `,
			condit(dim, subject){
				return dim.skinHue < 150 || dim.skinHue > 250;
			},
			execute(dim, subject){ return {
				absolute: {
					skinHue: ra.range(185, 205),
					skin: ra.range(8, 12),
				},
			}},
		},
		
		//BLUE HAIR
		{
			name: `blue hair`,
			desc: `My hair began to tingle. `,
			npc: (n,x) =>  `${s(n)} hair turned blue! `,
			condit(dim, subject){
				return dim.hairHue < 40 || dim.hairHue > 60;
			},
			execute(dim, subject){ return {
				absolute: {
					hairHue: ra.range(220, 240),
					hairSaturation: ra.range(90, 100),
					hairLightness: ra.range(30, 50),
				},
			}},	
		},
		
		//BLUE LIPS
		{
			name: `blue lips`,
			desc: `<p>In the water reflection I could see that my lips turned blue. </p>`,
			npc: (n,x) =>  `${s(n)} lips turned blue! `,
			condit(dim, subject){
				return subject.apex.game_lips !== `hsla(240, 100%, 30%, 1)`;
			},
			execute(dim, subject){ return {
				lipstick: {
					invisible: -1,
				},
				apex: {
					game_lips: `hsla(240, 100%, 30%, 1)`,
				},
			}},
		},
	],
}