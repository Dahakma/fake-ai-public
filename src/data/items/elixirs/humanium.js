import {ra} from "Libraries/random";
import {s, gradual} from "Libraries/dh";


function origo(subject, what, value){
	if(subject.Mods[what] > subject.original[what]){ //minus
		if(subject.Mods[what] - value > subject.original[what]) value = subject.Mods[what] - subject.original[what];
		return -value;
	}else{ //plus
		if(subject.Mods[what] + value > subject.original[what]) value = subject.original[what] - subject.Mods[what];
		return value;
	}
}


export const xHum = {
	name: `humanium`,
	note: `returns back to original`,
	taste:[
		`The liquid was indistinguishable from water. `, 
		`The potion was clear, without any taste or smell. `,
	],
	trans: [
		//ORG SKIN
		{
			name: `original complexion`,
			desc: `My skin returned to its original shade. `,
			npc: (n,x) =>  `The skin of ${n} regained a more ordinary shade. `,
			condit(dim, subject){
				return Math.round(subject.Mods.skin) !== Math.round(subject.original.skin);
			},
			execute(dim, subject){ return {
				relative: {
					skinHue: subject.original.skinHue - subject.Mods.skinHue,
					skin: subject.original.skin - subject.Mods.skin,
					skinSaturation: subject.original.skinSaturation - subject.Mods.skinSaturation,
				},
			}},
		},
		
		//ORG HAIR
		{
			name: `original hair`,
			desc: `My hair returned to its original color. `,
			npc: (n,x) =>  `The hair of ${n} returned to their original colour (as far as I knew). `,
			condit(dim, subject){
				return Math.round(subject.Mods.hairHue) !== Math.round(subject.original.hairHue);
			},
			execute(dim, subject){ return {
				relative: {
					hairHue: subject.original.hairHue -  subject.Mods.hairHue,
					hairSaturation: subject.original.hairSaturation - subject.Mods.hairSaturation,
					hairLightness: subject.original.hairLightness - subject.Mods.hairLightness, 
				},
			}},
		},
		
		//ORG HEIGHT
		{
			name: `original height`,
			desc: `My height changed closer to my original one. `,
			npc: (n,x) =>  `The height of ${n} returned closer to ${x ? `her` : `his`} original one. `,
			condit(dim, subject){
				return 0.5 * +( subject.Mods.height < subject.original.height - 5 || subject.Mods.height > subject.original.height + 5);
			},
			execute(dim, subject){ return {
				relative: {
					height: subject.Mods.height < subject.original.height ? 5 : -5,
					neckLength: subject.Mods.neckLength < subject.original.neckLength ? 1 : -1, 
				},
			}},
		},
		
		//ORG WITH
		{
			name: `original width`,
			desc: `My proportions became more <i>normal</i>.`,
			npc: (n,x) =>  `${s(n)} figure became less exaggerated. `,
			condit(dim, subject){
				let mod = 0;
				if( subject.Mods.upperMuscle < subject.original.upperMuscle - 5 || subject.Mods.upperMuscle > subject.original.upperMuscle + 5) mod++;
				if( subject.Mods.buttFullness < subject.original.buttFullness - 10 || subject.Mods.buttFullness > subject.original.buttFullness + 10) mod++;
				return mod * 0.5;
			},
			execute(dim, subject){ return {
				relative: {
					//muscles
					upperMuscle: subject.Mods.upperMuscle < subject.original.upperMuscle ? 5 : -5,
					shoulderWidth: subject.Mods.shoulderWidth < subject.original.shoulderWidth ? 2 : -2,
					
					//buffness
					neckWidth: subject.Mods.neckWidth < subject.original.neckWidth ? 2 : -2,
					
					buttFullness: origo(subject, `buttFullness`, 10),
					legFullness: origo(subject, `legFullness`, 8),
					legFem: origo(subject, `legFem`, 4),
					hipWidth: origo(subject, `hipWidth`, 10),
				},
			}},
		},
		
		
		//NO TAIL	
		{
			
			name: `no tail`,
			desc: `My tail slowly disappeared. `,
			npc: (n,x) =>  `${s(n)} tail was shrinking until it fully disappeared. `,
			condit(dim, subject){
				return dim.tail > 0;
			},
			execute(dim, subject){ return {
				absolute: {
					tail: 0,
				},
			}},
		},
		
		//NORMAL EARS 
		{
			name: `normal ears`,
			desc: `My ears returned to ordinary human shape and size. `,
			npc: (n,x) =>  `${s(n)} ears returned to ordinary human shape and size. `,
			condit(dim, subject){
				return dim.earlobeLength > 2;
			},
			execute(dim, subject){ return {
				absolute: {
					earlobeLength: 1,
				},
				apex: {
					ears: `elf`,
				},
			}},		 
		},
		
		//NO WINGS
		{
			name: `no wings`,
			desc: `My wings slowly disappeared. `,
			npc: (n,x) =>  `${s(n)} wings slowly disappeared. `,
			condit(dim, subject){
				return dim.wings > 0;
			},
			execute(dim, subject){ return {
				absolute: {
					wings: 0,
				},
			}},	
		},
		
		//NO HORNS
		{
			name: `no horns`,
			desc: `My horns slowly disappeared. `,
			npc: (n,x) =>  `${s(n)} horns shrinked and disappeared. `,
			condit(dim, subject){
				return dim.hornyness > 0;
			},
			execute(dim, subject){ return {
				absolute: {
					hornyness: 0,
				},
			}},		
		},
		
		//NORMAL LIPS
		{
			name: `normal lips`,
			desc: `My returned to a normal color `,
			npc: (n,x) =>  `I nearly miss that but after a throughout check I realised ${s(n)} eyes returned to their original color. `,
			condit(dim, subject){
				return !!subject.apex.game_lips;
			},
			execute(dim, subject){ return {
				lipstick: {
					invisible: 1,
				},
				apex: {
					game_lips: undefined,
				},
			}},	
		},
	],
}