import {ra} from "Libraries/random";
import {s, gradual} from "Libraries/dh";

export const xGob = {
	name: `goblinium`,
	note: `small, curvy, green`,
	effect: `cunin`,
	taste: [
		`The potion was thick and had an earthly flavour. `,
		`The liquid in the phial was very dense and had an earthy flavour. `,
		`The potion was thick and had a distinctive earthy taste. `,
	],
	trans: [
		//GREENSKING
		{
			name: `green skin`,
			desc: `I drank it and at first it seemed that nothing did happen - and then I noticed my green hands. </p><p>Quick examination showed my skin turned green - absolutely <i>everywhere</i>!`,
			npc: (n,x) => `${s(n)} skin turned green! `,
			condit(dim){
				return dim.skinHue < 80 || dim.skinHue > 120;
			},
			execute(dim, subject){ return {
				absolute: {
					skinHue: ra.range(85, 115),
					skin: ra.range(8, 16),
					skinSaturation: ra.range(-5, 35),
				},
			}},
		},
		
		//SHORT
		{
			name: `short`,
			desc: `I drank it and for a moment I felt a bit dizzy. When I collected myself I noticed the everything around me seemed larger. </p><p>However, the further examination showed it was actually me who shrank. `,
			npc: (n,x) => `${n} suddenly shrink down! `,
			condit(dim){
				return gradual([150, 165, 180], dim.height, [0, 2]);
				//return dim.height`) > 150;
			},
			execute(dim, subject){ return {  
				relative: {
					height: gradual([150, 165, 180], dim.height, [-2, -6]),
				},
			}},
		},
		
		//TITS
		{
			name: `big boobs`,
			desc: `I looked down and could see my breast quickly expanding. `,
			npc: (n,x) => `${s(n)} breasts quickly expanded.  `,
			condit(dim){
				return gradual([0, 40, 80], dim.breastSize, [1, 0]);
			},
			execute(dim, subject){ return {
				relative: {
					breastSize: 12,
					breastPerkiness: dim.breastSize > 30 ? 5 : 3,
				},
			}},
			
		},
		
		//ASS
		{
			name: `thick ass`,
			desc: `I drank it and lurched when my centre of mass changed. </p><p>With horror I found out my butt is plumper and hips wider. `,
			npc: (n,x) => `${s(n)} breasts quickly expanded.  `,
			condit(dim){
				return gradual([0, 25, 50], dim.buttFullness, [1.5, 0])
			},
			execute(dim, subject){ return {
				relative: {
					buttFullness: dim.buttFullness < 80 ?  12 : 0,
					legFullness: dim.legFullness < 40 ?  4 : 0,
					legFem: dim.legFem < 50 ?  2 : 0,
					hipWidth: dim.hipWidth < 140 ?  3 : 0,
					height:-0.2,
				},
			}},	
		},
		
		//CURVYNESS
		{
			name: `curvy`,
			desc: `I drank it and I could feel how am becoming curvier, how is my body gaining mass, especially in the chest area. `,
			npc: (n,x) => `${n} became curvier and rather sexier.  `,
			condit(dim){
				return gradual([0, 40, 70], dim.breastSize, [1, 0]) || gradual([0, 25, 50], dim.buttFullness, [1.5, 0]);
			},
			execute(dim, subject){ return {
				relative: {
					breastSize: 4,
					breastPerkiness: dim.breastSize > 30 ? 2 : 1,
					
					buttFullness: dim.buttFullness < 80 ?  7 : 0,
					legFullness: dim.legFullness < 40 ?  3 : 0,
					legFem: dim.legFem < 50 ?  1 : 0,
					hipWidth: dim.hipWidth < 140 ?  1.5 : 0,
					height:-0.2,
				},
			}},
		},
		
		//GREENLIPS
		{
			name: `green lips`,
			desc: `My lips turned green.  `, //TODO
			npc: (n,x) => `${s(n)} lips turned green! `,
			condit(dim, subject){
				return !subject.apex.game_lips;
			},
			execute(dim, subject){ return {
				lipstick: {
					invisible: -1,
				},
				apex: {
					game_lips: `#006C00`,
				},
			}},
		},
	],
}