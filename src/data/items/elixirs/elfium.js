import {ra} from "Libraries/random";
import {s, gradual} from "Libraries/dh";

export const xElf = {
	name: `elfium`,
	note: `tall, gaunt`,
	effect: `agili`,
	taste:[
		`The potion was clear with a refreshing sweet taste. `, 
		`The potion was pure and sweet. `,
		`The potion tasted kinda nice, it had a rather sweet flavour. `,
		`The potion was transparent and had a rather sweet flavour. `, 
	],
	trans: [
		//Elf SKIN
		{
			name: `elven complexion`,
			desc: `My skin returned to a more human-like shade. `,
			npc: (n,x) => `The skin of ${n} returned to a more human-like shade.  `,
			condit(dim, subject){
				return Math.round(subject.Mods.skinHue) !== Math.round(subject.original.skinHue);
			},
			execute(dim, subject){ return {
				absolute: {
					skinSaturation: (30 + subject.original.skinSaturation),
					skinHue: subject.original.skinHue,
					skin: subject.original.skin,
				},
			}},
		},
		
		//GOLDEN HAIR
		{
			name: `golden hair`,
			desc: `My hair began to tingle. `,
			npc: (n,x) => `The color of ${s(n)} hair changed into golden blonde  `,
			condit(dim, subject){
				return dim.hairHue < 40 || dim.hairHue > 60;
			},
			execute(dim, subject){ return {
				absolute: {
					hairHue: ra.range(40, 60),
					hairSaturation: ra.range(85, 100),
					hairLightness: ra.range(60, 70),
				},
			}},
		},
		
		
		//TALL
		{
			name: `tall`,
			desc: `My perspective suddenly changed. I grew taller. `,
			npc: (n,x) => `${n} suddenly grew taller. `,
			condit(dim){
				return gradual([150, 165, 180], dim.height, [2, 0]);
			},
			execute(dim, subject){ return {  
				relative: {
					height: gradual([150, 165, 180], dim.height, [6, 2]),
					shoulderWidth: dim.shoulderWidth > 63 ? -2 : 0,
					upperMuscle: dim.upperMuscle > 0 ? -2 : 0,
					
					neckWidth: dim.neckWidth > 35 ? -1 : 0,
					neckLength: dim.neckLength < 85 ? 1 : 0,
					
					breastSize: -1,
				},
			}},	
		},
		
		//SLENDER
		{
			name: `slender`,
			desc: `After drinking it I suddenly lose weight and became thinner and slenderer. `,
			npc: (n,x) => `${n} became tinner and slenderer. `,
			condit(dim){
				return gradual([0, 15, 40], dim.upperMuscle, [0, 2]);
			},
			execute(dim, subject){ return {  
				relative: {
					//muscles
					upperMuscle: gradual([0, 40], dim.upperMuscle, [-6, -12], 1),
					shoulderWidth: dim.shoulderWidth > 63 ? -2 : 0,
					
					//buffness
					neckWidth: dim.neckWidth > 35 ? -2 : 0,
					neckLength: dim.neckLength < 85 ? 2 : 0,
					
					//breasts
					breastSize: gradual([5, 40, 120], dim.breastSize, [-2, -12], 1),
					breastPerkiness: gradual([5, 40, 120], dim.breastPerkiness, [0, -4], 1),
					
					waistWidth: dim.waistWidth > 100 ? -5 : 0,
				},
			}},	
		},
		
		//EARS 
		{
			name: `elven ears`,
			desc: `My ears returned to their original shape and size. `,
			npc: (n,x) => `${n} gained pointy ears. `,
			condit(dim, subject){
				return dim.earlobeLength < 3 || subject.apex.ears !== `elf`;
			},
			execute(dim, subject){ return {  
				absolute: {
					earlobeLength: 6,
				},
				apex: {
					ears: `elf`,
				},
					
			}},
		},
	],
}