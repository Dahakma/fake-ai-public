import {xHP, xEng, xAnti} from "./support";
import {xHum} from "./humanium";
import {xGob} from "./goblinium";
import {xElf} from "./elfium";
import {xSuc} from "./succubium";
import {xInc} from "./incubium";
import {xMin} from "./minotaurium";
import {xBow} from "./bovinium";
import {xNaj} from "./najadium";
import {xFel} from "./felinium";
import {xMale} from "./male";
import {xFemale} from "./female";

export const elixirs = {
	xHP,
	xEng,
	xAnti,
	xHum,
	xGob,
	xElf,
	xSuc,
	xInc,
	xMin,
	xBow,
	xNaj,
	xFel,
	xFemale,
	xMale,
};
 