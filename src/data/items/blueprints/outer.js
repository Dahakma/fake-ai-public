import {exgar} from "./exgar";
import {rg} from "Libraries/random";

export const outer = {
	suit : {
		name: "jacket",
		id: 601,
		dad: "wSuperSuit",
		slot:"upperOuter",
		dim(){
			const open = 0.2 + (rg.g * 0.05);
			return {
				armLoose: [0.1, 0.3],
				armCoverage: [0.9, 0.95],
				
				cleavageCoverage: 0.4,
				waistCoverage: [-0.5,0.05],
				
				sideLoose: 1,
				topOpen: open,
				botOpen: open + (0.2 + (rg.g * 0.05)), //TODO
				
				curveCleavageX: 1, //TODO
				cleavageOpeness: 0.3,
				
				lapelCoverage: [0.69, 0.75],
				lapelDifference: [0.6, 0.9],
				lapelWidth: [5, 9],
				lapelHeight: [4, 7],
				
				
				invisible: 0,
				hi_invisible: 0,
			};
		},
		fill: ["navySuit","leatherJacket","nearlyBlack"],
	},	
	
	
	darkJacket : {
		name: "jacket",
		id: 610,
		dad: "wSuperSuit",
		slot:"upperOuter",
		dim(){
			const open = 0.2 + (rg.g * 0.2);
			return {
				waistCoverage: [-0.1, 0.5],
				sideLoose: [0.4, 0.8],
				cleavageCoverage: [0.25, 0.35],
				topOpen: open - 0.05,
				botOpen: open, //TODO
				armCoverage: [0.4, 0.7],
				armLoose: rg.g < 0.4 ? 0.1 : 0,
				
				lapelCoverage: [0.69, 0.75],
				lapelDifference: [0.6, 0.9],
				lapelWidth: [5, 9],
				lapelHeight: [4, 7],
				curveCleavageX: 1, //TODO
				
				invisible: 0,
				hi_invisible: 0,
			};
		},
		fill: ["dark","darkGrey"],
	},	
	
	
	jacket : {
		name: "jacket",
		id: 611,
		dad: "wSuperSuit",
		slot:"upperOuter",
		dim(){
			const open = 0.2 + (rg.g * 0.3);
			return {
				waistCoverage: [-0.1, 0.5],
				sideLoose: [0.4, 0.8],
				cleavageCoverage: [0.25, 0.35],
				topOpen: open - 0.05,
				botOpen: open, //TODO
				armCoverage: [0.4, 0.7],
				armLoose: rg.g < 0.4 ? 0.1 : 0,
				
				lapelCoverage: [0.69, 0.75],
				lapelDifference: [0.6, 0.9],
				lapelWidth: [5, 9],
				lapelHeight: [4, 7],
				curveCleavageX: 1, //TODO
				
				invisible: 0,
				hi_invisible: 0,
			};
		},
		fill: ["bright","dark","full","dark","full"],
	},	
	
	sexyJacket : {
		name: "jacket",
		id: 612,
		dad: "wSuperSuit",
		slot:"upperOuter",
		dim(){
			const open = 0.3 + (rg.g * 0.2);
			return {
				waistCoverage: [0.5, 0.8],
				sideLoose: [0, 0.8],
				cleavageCoverage: [0.23, 0.5],
				topOpen: open - 0.05,
				botOpen: open, //TODO
				armCoverage: [0.4, 0.55],
				
				lapelCoverage: [0.69, 0.75],
				lapelDifference: [0.6, 0.9],
				lapelWidth: [5, 9],
				lapelHeight: [4, 7],
				curveCleavageX: 1, //TODO
				
				invisible: 0,
				hi_invisible: 0,
			};
		},
		fill: ["bright","bright","dark","full","dark","full","darkGrey"],
	},
	

//SWEATERS
	sweater : {
		name: "sweater",
		dad: "wTurtleneck",
		slot:"upperOuter",
		dim(){
			
			//long x short arms
			let armCoverage = 0;
			if(rg.g < 0.6){
				armCoverage = [0.8, 1];
			}else{
				armCoverage = [0.45, 0.65];
			}
			
			//loose
			const sideLoose = [0.7, 1];
			const armLoose = [0.1, 0.2];
			
			//neck x cleavage 
			let cleavageOpeness = 0;
			let cleavageCoverage = 0;
			let curveCleavageX = 0;
				
			if(rg.g < 0.5){
				cleavageOpeness = [-0.1, 0.1];
				cleavageCoverage = [0.05, 0.1];
				curveCleavageX = [0, 4];
			}else{
				const mod = rg.g;
				cleavageOpeness = -0.1 - (0.7 * mod);
				cleavageCoverage =  - (0.021 * mod);
				curveCleavageX = [0, 3];
			}
			
			
			return {
				waistCoverage: [-0.5, 0.3],
				thickness: [1.4, 1.9],
				armCoverage,
				sideLoose,
				armLoose,
				cleavageOpeness,
				cleavageCoverage,
				curveCleavageX,
			};
		},
		fill: ["bright","full","dark","darkGrey"],
	},	
	
	tightSweater : {
		name: "sweater",
		dad: "wTurtleneck",
		slot:"upperOuter",
		dim(){
			
			//long x short arms
			let armCoverage = 0;
			if(rg.g < 0.6){
				armCoverage = [0.8, 1];
			}else{
				armCoverage = [0.45, 0.65];
			}
			
			//loose
			const sideLoose = 0;
			const armLoose = 0;
			
			/*
			if(rg.g < 0.7){
				sideLoose = [0.7, 1];
				armLoose = [0.1, 0.2];
			}
			*/
			
			//neck x cleavage 
			let cleavageOpeness = 0;
			let cleavageCoverage = 0;
			let curveCleavageX = 0;
				
			if(rg.g < 0.5){
				cleavageOpeness = [-0.1, 0.1];
				cleavageCoverage = [0.05, 0.1];
				curveCleavageX = [0, 4];
			}else{
				const mod = rg.g;
				cleavageOpeness = -0.1 - (0.7 * mod);
				cleavageCoverage =  - (0.021 * mod);
				curveCleavageX = [0, 3];
			}
			
			
			return {
				alpha: [0.93, 1],
				waistCoverage: [-0.5, 0.3],
				thickness: [1, 1.3],
				armCoverage,
				sideLoose,
				armLoose,
				cleavageOpeness,
				cleavageCoverage,
				curveCleavageX,
			};
		},
		fill: ["bright","full","dark","darkGrey"],
	},	


	shrug: {
		name: "shrug",
		dad: "Shrug",
		slot:"upperOuter",
		dim(){
			const topStyle = rg.array(["behind", "point", "normal"]);
			const armStyle = rg.array(["loose","short","long"]); 
			
			let cleavageOpeness;
			let cleavageCoverage;
			let curveX;
			if(topStyle === "behind"){
				cleavageOpeness = [0, 0.4];
				curveX = [-1, -5];
			}else if(topStyle === "point"){
				cleavageOpeness = [0.1, 0.9];
				cleavageCoverage = [0.1, 0.15];
				curveX = [0, -5];
			}else{
				cleavageOpeness =[0, 0.4];
				cleavageCoverage = [0.06, 0.12];
			}
			
			let armCoverage;
			let armLoose;
			if(armStyle === "loose"){
				armCoverage = [0.25, 0.5];
				armLoose = [0.25, 0.5];
			}else if(armStyle === "short"){
				armCoverage = [0.15, 0.45];
				armLoose =  0;
			}else{
				armCoverage = [0.55, 1];
				armLoose =  0;
			}
			
			let temp = rg.g;
			temp = rg.g;
			temp = rg.g;
			
			return {
				connectBehindBack: topStyle === "behind",
				connectSinglePoint: topStyle === "point",
				armCoverage,
				armLoose,
				cleavageOpeness,
				cleavageCoverage,
				curveX,
			};
		},
		fill: ["bright","full","dark","darkGrey"],
	},
	
	fishnetShrug: {
		name: "shrug",
		dad: "Shrug",
		slot:"upperOuter",
		dim(){
			return {
				cleavageOpeness: [0, 0.4],
				cleavageCoverage: [0.06, 0.12],
				armCoverage: rg.g < 0.5 ? [0.7, 1] : [0.12, 0.45],
				armLoose: 0,
				thickness: [0.1, 0.25],
			};
		},
		//fill: "nearlyBlack",
		texture: exgar.core_fishnet,
	},	
	

//FANTASY CORSETS
	//HARLOT CORSET
	defHarlotCorset : {
		name: "bodice",
		dad: "wHalfCorset", 
		slot:"upperOuter",
		dim(){ return{
			cupless: true,
			waistCoverage: 0,
			knots: 7,
			topCoverage: 0.65,
		}},
		fill: ["dark"],
	}, 

	harlotCorset : {
		name: "bodice",
		dad: "wHalfCorset", 
		slot:"upperOuter",
		dim(){ return{
			cupless: true,
			waistCoverage: [-0.1,0.75],
			knots: [3,9],
			topCoverage: 0.65,
		}},
		fill: ["dark"],	
	},

}