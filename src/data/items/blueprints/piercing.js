import {exgar} from "./exgar";
import {rg} from "Libraries/random";

//TODO
export const piercing = {
	
	//BELLYPIERCE
	bellyPiercingSimple : {
		name: "bellybutton piercing",
		slot:"bellybutton",
		dad: "BellyPiercingSimple",
		piercing: true,
		dim(){ 
			
			return {
				zoom: "chest",
				radius: rg.range(1.2,1.5),
				
			}; 
		},
		stroke: ["NXgold","NXsilver","NXsteel"],
		fill:["NXgold","NXsilver","NXsteel","bright","bright","bright"],
	},
	

//TODO
	bellyPiercing : {
		name: "bellybutton piercing",
		slot:"bellybutton",
		dad: "BellyPiercingAdvanced",
		piercing: true,
		dim(){ 
			let above = false;
			let above2 = false;
			let bellow = false;
			let bellow2 = false;
			let chain = false;
			let link = false;
			let distance = 1;
			let secondaryRadius = 1;
			switch( rg.integer(5) ){
				case 1:
					above = true;
					above2 = true;
					break;
				case 2:
					above = true;
					distance = 1.1;
					if( rg.g < 0.5 )chain = true;
					break;
				case 3:
					bellow = true;
					if( rg.g < 0.65 ) bellow2 = true;
					break;
				case 4:
					secondaryRadius = rg.range(1.5,2);
					bellow2 = true;
					link = true;
					break;	
				default:
				case 5:
					chain = true;
					if( rg.g < 0.5 ){
						above = true;
						if( rg.g < 0.5 ) above2 = true;
					}
					break;		
			}
		
			return {
				zoom: "chest",
				above,
				above2,
				bellow,
				bellow2,
				chain,
				link,
				secondaryRadius,
				radius: 1.4,
				
			}; 
		},
		stroke: ["NXgold","NXsilver","NXsteel"],
		fill:["NXgold","NXsilver","NXsteel","bright","bright","bright"],
	},
/*	
	bellyPiercingInitial : {
		name: "bellybutton piercing",
		slot:"bellybutton",
		dad: "BellyPiercingAdvanced",
		piercing: true,
		dim: (function(){ 
			let above = true;
			let chain = true;
			let distance = 1.2;
			let secondaryRadius = 1;
			 
		
			return {
				fill: "#6e7fcf", //"#9c09d9",
				stroke: "#6e7fcf", //"#9c09d9",
				above: true,
				above2: false,
				bellow: false,
				bellow2: false,
				chain: true,
				link: false,
				secondaryRadius: 1,
				distance: 1.2,
				radius: 1.4,
				
			}; 
		}),
		//stroke: ["bright"],
		//fill:["bright"],
	},
*/

//NOSE
	nosePiercing : {
		name: "nose piercing",
		slot:"noseSide",
		dad: "StudPiercing2",
		piercing: true,
		dim(){ 
			return {
				zoom: "face",
				radius: rg.range(0.45,0.6),
				side: "right",
				relativeLocation: {
					drawpoint: "nose.out",
					dx: -3.6,
					dy: 1.15,
				},
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","bright","mat"],
	},
	
	noseBridge : {
		name: "nose bridge",
		slot:"noseTop",
		dad: "Bridge",
		piercing: true,
		dim(){ 
			return {
				zoom: "face",
				radius: rg.range(0.4,0.5),
				side: "right",
				relativeLocation: {
					drawpoint: "nose.top",
					dx: -0.3,
					dy: -1.66,
				},
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","bright","mat"],
	},
	
	
	noseRingSimple : {
		name: "nose ring",
		slot:"noseBot",
		dad: "NoseRing",
		piercing: true,
		dim(){ 
			const random = rg.g;
			let open = 0;
			let radius = rg.range(0.4,0.5);
			
			
			if(random < 0.45){
				radius = 0;
				//ring
			}else if(random < 0.75){
				open = 60;
				//horseshoes
			}else{
				//cbr
			}
			
		
			return {
				zoom: "face",
				thickness: rg.range(0.45,0.8),
				radius,
				openness2: open,
			 
				
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","bright","mat"],
	},
	
	noseRing : {
		name: "nose ring",
		slot:"noseBot",
		dad: "NoseRing",
		piercing: true,
		dim(){ 
			const random = rg.g;
			let open = 0;
			let radius = rg.range(0.4,0.5);
			
			
			if(random < 0.45){
				radius = 0;
				//ring
			}else if(random < 0.75){
				open = 60;
				//horseshoes
			}else{
				//cbr
			}
			
		
			return {
				zoom: "face",
				thickness: rg.range(0.45,0.8),
				radius,
				openness2: open,
				 
				
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","bright","mat"],
	},
	
	
	noseChain : {
		name: "nose chain",
		slot:"noseChain",
		dad: "NoseChain",
		piercing: true,
		dim(){ 
			return {
				zoom: "face",
				slack: rg.range(6, 20),
				right: false,
				/*
				slack: rg.range(0.7,1.6),
				thickness: rg.range(0.4,0.8),
				//radius: rg.range(0.45,0.6),
				side: "right",
				relativeLocation: {
					drawpoint: "nose.out",
					dx: -1,
					dy: 1,
				}
				*/
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","bright","mat"],
	},
	
	
//NIPPLES
	nippleBars : {
		name: "nipple bars",
		slot:"nipples",
		dad: "MultipleNipplePiercings",
		piercing: true,
		dim(){
			//let circe = (rg.g < 0.34) ? true : false;
			//let circe = false;
			//let barbar = (rg.g < 0.66 || !circe) ? true : false;
			//let barbar = true;
			
			return {
				zoom: "chest",
				distance: 2,
				number: 2,
				//bar: barbar,
				//ring: circe,
				//radius: rg.range(0.55,0.7),
				radius: rg.range(0.6,0.9),
				//ringRadius: rg.range(1.4,2.1),
				//side: "right",
				/*
				relativeLocation: {
					drawpoint: "nose.out",
					dx: 0,
					dy: 0,
				}
				*/
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	verticalNippleBars : {
		name: "nipple bars",
		slot:"nipples",
		dad: "MultipleNipplePiercings",
		piercing: true,
		dim(){
			return {
				zoom: "chest",
				distance: 2,
				number: 2,
				bar: false,
				verticalBar: true,
				radius: rg.range(0.6,0.9),
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	multipleNipple : {
		name: "multiple nipple piercings",
		slot:"nipples",
		dad: "MultipleNipplePiercings",
		piercing: true,
		dim(){
			//let circe = (rg.g < 0.34) ? true : false;
			//let circe = false;
			//let barbar = (rg.g < 0.66 || !circe) ? true : false;
			//let barbar = true;
			
			return {
				zoom: "chest",
				distance: 2,
				number: 4,
				//bar: barbar,
				//ring: circe,
				//radius: rg.range(0.55,0.7),
				radius: rg.range(0.6,0.9),
				//ringRadius: rg.range(1.4,2.1),
				//side: "right",
				/*
				relativeLocation: {
					drawpoint: "nose.out",
					dx: 0,
					dy: 0,
				}
				*/
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	 
	nippleRings : {
		name: "nipple rings",
		//slot:"nipplesBot",
		slot: "nipples",
		dad: "SimpleNipplePiercings",
		piercing: true,
		dim(){
			//let circe = (rg.g < 0.34) ? true : false;
			const circe = true;
			//let barbar = (rg.g < 0.66 || !circe) ? true : false;
			const barbar = false;
			const cbr = (rg.g < 0.34);
			
			
			return {
				zoom: "chest",
				bar: barbar,
				ring: circe,
				cbr,
				//radius: rg.range(0.55,0.7),
				radius: rg.range(0.6,0.9),
				ringRadius: rg.range(1.4,2.3),
				thickness: rg.range(0.35,0.55),
				//side: "right",
				/*
				relativeLocation: {
					drawpoint: "nose.out",
					dx: 0,
					dy: 0,
				}
				*/
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	
	nippleShields : {
		name: "nipple shields",
		//slot:"nipplesBot",
		slot: "nipples",
		dad: "SimpleNipplePiercings",
		piercing: true,
		dim(){
			const barbar = rg.b;
			return {
				zoom: "chest",
				bar: barbar,
				verticalBar: barbar,
				ring: false,
				cbr: false,
				shield: true,
				thickness: rg.range(0.35,0.55),
				radius: rg.range(0.45,0.65),
				
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	
	nippleStarShields : {
		name: "nipple shields",
		//slot:"nipplesBot",
		slot: "nipples",
		dad: "NippleStar",
		piercing: true,
		dim(){
			
			return {
				zoom: "chest",
				innerRadius: rg.range(1, 1.5),
				outerRadius: rg.range(2.5, 3),
				upwards: (rg.g < 0.5),
				spikes: 4 + Math.floor(rg.g * 3),
				styleOuter: (rg.g < 0.25) ? -1 : 1,
				
			
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	
	nippleChain : {
		name: "nipple chain",
		slot:"nippleChain",
		dad: "NippleChain",
		piercing: true,
		dim(){
			
			return {
				zoom: "chest",
				tense: false,
				chain: true,
				slack: rg.range(2, 6),
				thickness: rg.range(0.35, 0.7),
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	
//GENITAL PIERCINGS
	VCHPiercing : {
		name: "vertical clit hood piercing",
		slot:"vch",
		dad: "VCHPiercing",
		piercing: true,
		dim(){ 
			return {
				zoom: "groin",
				radius: rg.range(0.55, 0.75),				
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	
	HCHPiercing : {
		name: "horizontal clit hood piercing",
		slot:"hch",
		dad: "HCHPiercing",
		piercing: true,
		dim(){ 
			return {
				zoom: "groin",
				radius: rg.range(0.6, 0.9),	
				openness: 100,
				ringRadius: rg.range(1.2, 1.4),	
				thickness: rg.range(0.4, 0.55),	
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	
	christinaPiercing : {
		name: "christina piercing",
		slot:"pubic",
		dad: "ChristinaPiercing",
		piercing: true,
		dim(){ 
			return {
				zoom: "groin",
				radius: rg.range(0.5, 0.9),	
				radius2: rg.range(0.75, 1.2),		
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	
	//TODO
	labiaPiercing : {
		name: "labia piercing",
		slot:"labia",
		dad: "StudPiercing3",
		piercing: true,
		dim(){ 
			return {
				zoom: "groin",
				radius: rg.range(0.8,1),
				side: "right",
				relativeLocation: {
					drawpoint: "vagina.top",
					dx: 1,
					dy: -1,
				},
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel"],
	},
	
	
	labiaPiercings : {
		name: "multiple labia piercings",
		slot:"labiaBot",
		dad: "LabiaPiercings",
		piercing: true,
		dim(){ 
			return {
				zoom: "groin",
				
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	
//FACE 
	tongueSlaveRing: {
		name: "tongue piercing",
		slot:"tongue",
		dad: "TonguePiercing",
		piercing: true,
		dim(){ 
			return {
				radius: 0.03,
				side: "right",
				relativeLocation: {
					drawpoint: "lips.center",
					dx: 0,
					dy: 0,
				},
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel"],
	},
	
	tonguePiercing : {
		name: "tongue piercing",
		slot:"tongue",
		dad: "TonguePiercing",
		piercing: true,
		dim(){ 
			return {
				radius: 0.03,
				side: "right",
				relativeLocation: {
					drawpoint: "lips.center",
					dx: 0,
					dy: 0,
				},
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel"],
	},
	
	eyebrowPiercingLeft : {
		name: "eyebrow piercing",
		slot:"eyebrow",
		dad: "Bridge2",
		piercing: true,
		dim(){ 
			
				return {
					zoom: "face",
					radius: rg.range(0.4,0.6), //0.5,
					rotation: 90,
					distance: 0.9,
					side: "right",
					relativeLocation: {
						drawpoint: "brow.outbot",
						dx: -1.5,
						dy: 0.4,
					},
				};
			
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","bright","mat"],
	},
		
	eyebrowPiercing : {
		name: "eyebrow piercing",
		slot:"eyebrow",
		dad: "Bridge2",
		piercing: true,
		dim(){ 
			const side = rg.g < 0.5 ? "left" : "right";
			if( rg.g<0.55 ){
				return {
					zoom: "face",
					radius: 0.5,
					rotation: 90,
					distance: 0.9,
					side,
					relativeLocation: {
						drawpoint: "brow.outbot",
						dx: -1.5,
						dy: 0.4,
					},
				};
			}else{
				return {
					zoom: "face",
					radius: 0.5,
					distance: 0.66,
					rotation: -8,
					side,
					relativeLocation: {
						drawpoint: "brow.outbot",
						dx: -3,
						dy: 1.15,
					},
				};
			}
		},
		fill: ["NXgold","NXsilver","NXsteel"],
	},
	
	labret : {
		name: "labret piercing",
		slot:"lipsBot",
		dad: "StudPiercing4",
		piercing: true,
		dim(){ 
			return {
				zoom: "face",
				radius: rg.range(0.5,0.7), //0.6
				side: "right",
				relativeLocation: {
					drawpoint: "lips.bot",
					dx: 0,
					dy: -1,
				},
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	medusa : {
		name: "medusa piercing",
		slot:"lipsTop",
		dad: "StudPiercing5",
		piercing: true,
		dim(){ 
			return {
				zoom: "face",
				radius: rg.range(0.5,0.7), //0.6
				side: "right",
				relativeLocation: {
					drawpoint: "lips.top",
					dx: 0.25,
					dy: rg.range(1.4,2.1),
				},
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	monroeRight : {
		name: "monroe piercing",
		slot:"lipsSide",
		dad: "StudPiercing6",
		piercing: true,
		dim(){ 
			return {
				zoom: "face",
				radius: rg.range(0.5,0.7), //0.6
				side: "left",
				relativeLocation: {
					drawpoint: "lips.out",
					dx: -0.2,
					dy: 2.2,
				},
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	
	monroe : {
		name: "monroe piercing",
		slot:"lips",
		dad: "StudPiercing6",
		piercing: true,
		dim(){ 
			return {
				zoom: "face",
				radius: rg.range(0.5,0.7), //0.6
				side: rg.g < 0.5 ? "left" : "right",
				relativeLocation: {
					drawpoint: "lips.out",
					dx: -0.2,
					dy: 2.2,
				},
			}; 
		},
		fill: ["NXgold","NXsilver","NXsteel","bright","mat"],
	},
	
	
}