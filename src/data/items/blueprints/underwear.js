import {exgar, rg} from "./exgar";

export const underwear = {
	
//BRA 
	simpleBra : {
		name: "bra",
		slot:"bra",
		dad: "SuperBra",
		note: "default DAD bra",
		dim(){ 
			if(rg.g < 0.4){
				return {
					thickness: [0.3,0.5], 
					showStrap: true,
					strapBehindBreasts: false,
					neckCoverage: [0.5,0.95],
					strapWidth: [0.8,2.2],
					botStrapCoverage: rg.g < 0.65 ? [0.65, 1] : [1.25, 2],
				}
			}else{
				return {
					thickness: [0.3,0.5], 
					showStrap: false,
					strapBehindBreasts: false,
					botStrapCoverage: rg.g < 0.65 ? [0.65, 1] : [1.25, 2],
				}
			}
		},
		fill: ["nearlyBlack", "nearlyWhite"],// exgar.underwear_fill,
	},
	
	simpleCageBra : {
		name: "bra",
		slot:"bra",
		dad: "CageBra",
		note: "cage bra without cage",
		dim(){ 
			return {
				thickness: [0.4,0.6],
				neckCoverage: [0.45, 0.8],
				topStrapWidth: [0.9, 2],
				botStrapCoverage: rg.b < 0.65 ? [0.7, 1] : [1.25, 2],
				
				cageCenter: false,
				cageCross: false,
				ruffling: false,
			}
		},
		fill: exgar.underwear_fill,
		highlight: ["stroke", "stroke", "darkGrey"],
	},
	
	cageBra : {
		name: "cage bra",
		slot:"bra",
		dad: "CageBra",
		note: "bra possibly with cage",
		dim(){ 
			const fancy = rg.g;
			return {
				thickness: [0.4,0.6],
				neckCoverage: [0.45, 0.8],
				topStrapWidth: [0.9, 1.6],
				botStrapCoverage: rg.g < 0.65 ? [0.7, 1] : [1.25, 2],
				cageSpacing: [1.3, 2.2],
				
				cageCenter: fancy <= 0.33,
				cageCross: fancy > 0.33 && fancy <= 66,
				ruffling: fancy > 66,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
			}
		},
		fill: exgar.underwear_fill,
		highlight: ["stroke", "stroke", "darkGrey"],
	},
	
	sexyCageBra : {
		name: "cage bra",
		slot:"bra",
		dad: "CageBra",
		note: "sexiest version of cage bra",
		dim(){ 
			const fancy = rg.b;
			return {
				thickness: [0.4,0.6],
				neckCoverage: [0.45, 0.8],
				topStrapWidth: [0.9, 1.6],
				botStrapCoverage: rg.g < 0.65 ? [0.7, 1] : [1.25, 2],
				cageSpacing: [1.3, 2.2],
				
				cageCenter: fancy,
				cageCross: !fancy,
				cageDouble: rg.g < 0.5,
				ruffling: rg.g < 0.33,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
			}
		},
		fill: exgar.underwear_fill,
		highlight: ["stroke", "stroke", "darkGrey"],
	},
	
	animalCageBra : { //TODO BASED ON SEXY BRA
		name: "cage bra",
		slot:"bra",
		dad: "CageBra",
		//note: "sexiest version of cage bra",
		dim(){ 
			const fancy = rg.g;
			return {
				thickness: [0.4,0.6],
				neckCoverage: [0.45, 0.8],
				topStrapWidth: [0.9, 1.6],
				botStrapCoverage: rg.g < 0.65 ? [0.7, 1] : [1.25, 2],
				cageSpacing: [1.3, 2.2],
				
				cageCenter: fancy <= 0.33,
				cageCross: fancy > 0.33 && fancy <= 66,
				ruffling: fancy > 66,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				cageDouble: rg.g < 0.33,
			}
		},
		fill: "nerlyBlack",
		texture: exgar.animal,
	},
	
	

	
	
	simplePushUpBra : {
		name: "push-up bra",
		slot:"bra",
		dad: "PushUpBra",
		note: "not frilly frilly bra",
		dim(){ 
			return {
				thickness: 0.9, //has to be the same
				strapWidth: 0.9, //has to be the same
				showStrap: true,
				neckCoverage: [0.5,0.95],
				botStrapCoverage: rg.g < 0.65 ? [0.7, 1] : [1.25, 2],
				cupCoverage: [0, 0.3],
				ruffling: false,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
			};
		},
		fill: exgar.underwear_fill,
		highlight: ["stroke", "stroke", "darkGrey"],
	},
	
	pushUpBra : {
		name: "push-up bra",
		slot:"bra",
		dad: "PushUpBra",
		note: "bra possibly frilly",
		dim(){ 
			return {
				thickness: 0.9, //has to be the same
				strapWidth: 0.9, //has to be the same
				showStrap: true,
				neckCoverage: [0.5,0.95],
				botStrapCoverage: rg.g < 0.65 ? [0.7, 1] : [1.25, 2],
				ruffling: rg.g < 0.5,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				cupCoverage: rg.g < 0.33 ? [-0.5, 1] :  [0, 0.3],
			};
		},
		fill: exgar.underwear_fill,
		highlight: ["stroke", "stroke", "darkGrey"],
	},
	
	sexyPushUpBra : {
		name: "push-up bra",
		slot:"bra",
		dad: "PushUpBra",
		note: "bra possibly frilly",
		dim(){ 
			const fancy = rg.g;
			return {
				thickness: 0.9, //has to be the same
				strapWidth: 0.9, //has to be the same
				showStrap: true,
				neckCoverage: [0.5,0.95],
				botStrapCoverage: rg.g < 0.65 ? [0.7, 1] : [1.25, 2],
				ruffling: fancy < 0.65,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				cupCoverage: fancy > 0.75 ? [-1, -0.25] :  [0, 0.3],
			};
		},
		fill: exgar.underwear_fill,
		highlight: ["stroke", "stroke", "darkGrey"],
	},
	
	animalPushUpBra : { //TODO BASED ON SEXY BRA
		name: "push-up bra",
		slot:"bra",
		dad: "PushUpBra",
		//note: "sexiest version of frilly bra",
		dim(){ 
			return {
				thickness: 0.9, //has to be the same
				strapWidth: 0.9, //has to be the same
				showStrap: true,
				neckCoverage: [0.5,0.95],
				botStrapCoverage: rg.g < 0.65 ? [0.7, 1] : [1.25, 2],
				ruffling: rg.g < 0.66,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				cupCoverage: rg.g < 0.4 ? [-0.5, 1] :  [0, 0.3],
			};
		},
		fill: "nearlyBlack",
		texture: exgar.animal,
	},
	
	
	
	
	
	
	
	
	
	
	
	 
	
	
	

	
	
	
//PANTIES
	simpleBriefs : {
		name: "briefs",
		subtype: "briefs",
		slot:"panties",
		dad: "SuperPanties",
		note: "ordinary less sexy panties",
		dim(){ return {
			thickness: [0.3, 0.4],
			waistCoverage:[0.2,0.46],
			waistCoverageLower:[-0.15,-0.3],
			genCoverage:1,
			topCurve: [-1, -5],
			bow: false,
			ruffling: false,
			topBandWidth: rg.g < 0.6 ? [2, 4] : 0,
		}},
		fill: exgar.bland_underwear_fill,
	},
	
	briefs : {
		name: "briefs",
		subtype: "briefs",
		slot:"panties",
		dad: "SuperPanties",
		note: "ordinary less sexy panties",
		dim(){ 
			const bow = rg.g < 0.33;
			return {
			thickness: [0.3, 0.4],
			waistCoverage:[0.2,0.46],
			waistCoverageLower:[-0.15,-0.3],
			genCoverage:1,
			topCurve: [-1, -5],
			bow,
			ruffling: rg.g < 0.66,
			rufflingThickness: [0.6, 1.2],
			rufflingLength: [1.7, 2.7],
			topBandWidth: !bow && rg.g < 0.5 ? [2, 4] : 0,
		}},
		fill: exgar.underwear_fill,
	},
	
	simpleHipsters : {
		name: "hipster panties",
		subtype: "hipsters",
		slot:"panties",
		dad: "SuperPanties",
		note: "slightly sexier panties",
		dim(){ 
			const top = rg.range(0.1,0.2);
			return {
				thickness: [0.3, 0.4],
				waistCoverage: top,
				waistCoverageLower:[top - 0.2, top -0.4],
				genCoverage:1,
				topCurve: [-3, -6],
				bow: false,
				ruffling: false,
				topBandWidth: rg.g < 0.5 ? [2, 4] : 0,
			}
		},
		fill: exgar.bland_underwear_fill,
	},
	
	hipsters : {
		name: "hipster panties",
		subtype: "hipsters",
		slot:"panties",
		dad: "SuperPanties",
		note: "slightly sexier panties",
		dim(){
			const top = rg.range(0.1,0.2);
			const bow = rg.g < 0.33;
			return {
				thickness: [0.3, 0.4],
				waistCoverage: top,
				waistCoverageLower:[top - 0.2, top -0.4],
				genCoverage:1,
				topCurve: [-3, -6],
				bow,
				ruffling: rg.g < 0.55,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				topBandWidth: !bow && rg.g < 0.6 ? [1, 3] : 0,
			}
		},
		fill: exgar.underwear_fill,
	},
	
	boyshorts : {
		name: "boyshorts",
		subtype: "boyshorts",
		slot:"panties",
		dad: "SuperPanties",
		note: "panties resebling shorts",
		dim(){ return {
			thickness: [0.3, 0.4],
			waistCoverage:[0.1,0.28],
			waistCoverageLower:[-0.5,-0.8],
			curveTopY: 1,
			curveBotY: -1,
			topBandWidth: rg.g < 0.7 ? [2, 5] : 0,
			bow: false,
		}},
		fill: exgar.underwear_fill,
	},
	
	
	
	
	
	
	
	thong : {
		name: "thong",
		subtype: "thong",
		slot:"panties",
		dad: "SuperPanties",
		dim(){ 
			const top = rg.range(-0.025, 0.15);
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.1, top -0.2],
				genCoverage: [0.4,0.7],
				topCurve: [-4, -7],
				bow: rg.g < 0.3,
				ruffling: rg.g < 0.4,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
			}
		},
		fill: exgar.underwear_fill,
	},
	
	hiThong : {
		name: "thong",
		subtype: "thong",
		slot:"panties",
		dad: "SuperPanties",
		note: "thong which should stick above waistband of pants",
		dim(){ 
			const top = rg.range(0.15, 0.35);
			//let bot = top - between(0.06,0.18); 
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.12, top -0.24],
				genCoverage: [0.6,0.9],
				topCurve: [-6, -9],
				bow: false,
				ruffling: rg.g < 0.25,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				curveBotX: [0, -2],
		}},
		fill: exgar.underwear_fill,
	},
	
	
	
	
	gString : {
		name: "g-string",
		subtype: "gString",
		slot: "panties",
		dad: "GString",
		dim(){ 
			const top = rg.range(0.0, 0.35);
			//let bot = top - between(0.06,0.18); 
			return {
				waistCoverage: [0, 0.25],
				curveBotX: [-1, -2.5],
				horizontalCoverage: [0.35, 0.45],
				verticalCoverage: [-0.25, -0.2],
				width: [1.5, 2],
				genCoverage: [0.3, 0.7],
				/*
				waistCoverage: top,
				waistCoverageLower: [top - 0.05, top -0.1],
				genCoverage: [0.3,0.6],
				curveTopX: [-2,-3],
				curveTopY: [-4,-6],
				bow: rg.g < 0.3,
				ruffling: rg.g < 0.3,
				curveBotX: [0, -2],
				curveBotY: [0, 2],
				*/
		}},
		fill: exgar.underwear_fill,
	},
	
	/*
	gString : {
		name: "g-string",
		subtype: "gString",
		slot:"panties",
		dad: "wSuperPanties",
		dim(){ 
			const top = rg.range(0.0, 0.35);
			//let bot = top - between(0.06,0.18); 
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.05, top -0.1],
				genCoverage: [0.3,0.6],
				curveTopX: [-2,-3],
				curveTopY: [-4,-6],
				bow: rg.g < 0.3,
				ruffling: rg.g < 0.3,
				curveBotX: [0, -2],
				curveBotY: [0, 2],
		}},
		fill: exgar.underwear_fill,
	},
	*/
	
	
	dividedPanties : {
		name: "panties",
		subtype: "hipsters",
		slot:"panties",
		dad: "DividedPanties",
		note: "slightly sexier panties",
		dim(){
			const top = rg.range(0.1,0.2);
			const topCoverage = rg.range(0.25, 0.75);
			
			return {
				topCoverage,
				botCoverage: [topCoverage + 0.1, topCoverage + 0.2], 
				thickness: [0.3, 0.4],
				waistCoverage: top,
				waistCoverageLower:[top - 0.2, top -0.4],
				genCoverage: 1,
				topCurve: [-3, -6],
				bow: false,
				ruffling: rg.g < 0.35,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				ribs: false, //todo
				hi_alpha: [0.5, 0.8],
			}
		},
		fill: exgar.underwear_fill,
		highlight: ["stroke", "highlight", "contrast", "darkGrey"],
	},
	
	
	cagePanties : {
		name: "g-string",
		subtype: "gString",
		slot: "panties",
		dad: "CagePanties",
		dim(){ 
			const top = rg.range(0.0, 0.35);
			return {
				waistCoverage: [0, 0.25],
				curveBotX: [-1, -2.5],
				horizontalCoverage: [0.35, 0.45],
				verticalCoverage: [-0.25, -0.2],
				thickness: [1, 2],
				genCoverage: [0.3, 0.7],
				topCoverage: [0, 0.25],
		}},
		fill: exgar.underwear_fill,
	},
	
//TODO
	stringThong : {
		name: "thong",
		subtype: "thong",
		dad: "wTinyPanties",
		slot:"panties",
		note: "thong using experimental DAD asset",
		dim(){  return {
				coverage: 0.6799999999999998,
				midCoverage: 1.4972253622443739,
				waistCoverage: -0.01809680402836289,
				waistCoverageLower: -0.12000000000000002,
				width: -0.40000000000000707,
		}},
		fill: exgar.underwear_fill, //["bright","yoga","black"],
	},
	
	
	crotchlessPanties : {
		name: "open-crotch panties",
		subtype: "crotchless",
		slot:"panties",
		dad: "SuperPanties",
		dim(){
			const top = rg.range(0.1,0.2);
			const bow = rg.g < 0.6;
			return {
				thickness: [0.3, 0.4],
				waistCoverage: top,
				waistCoverageLower:[top - 0.2, top -0.4],
				genCoverage:1,
				topCurve: [-3, -6],
				bow,
				ruffling: rg.g < 0.8,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				topBandWidth: !bow && rg.g < 0.8 ? [1, 3] : 0,
				openCrotch: true,
			}
		},
		fill: exgar.underwear_fill,
	},
	
	crotchlessThong : {
		name: "open-crotch thong",
		subtype: "crotchless",
		slot:"panties",
		dad: "SuperPanties",
		dim(){ 
			const top = rg.range(0, 0.2);
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.1, top -0.2],
				genCoverage: [0.4,0.7],
				topCurve: [-4, -7],
				bow: rg.g < 0.6,
				ruffling: rg.g < 0.8,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				openCrotch: true,
			}
		},
		fill: exgar.underwear_fill,
	},
	
	
	animalHipsters : {
		name: "panties",
		subtype: "hipsters",
		slot:"panties",
		dad: "wSuperPanties",
		note: "slightly sexier panties",
		dim(){
			const top = rg.range(0.1,0.2);
			const bow = rg.g < 0.33;
			return {
				thickness: [0.3, 0.4],
				waistCoverage: top,
				waistCoverageLower:[top - 0.2, top -0.4],
				genCoverage:1,
				topCurve: [-3, -6],
				bow,
				ruffling: rg.g < 0.55,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				topBandWidth: !bow && rg.g < 0.6 ? [1, 3] : 0,
			}
		},
		fill: "nearlyBlack",
		texture: exgar.animal,
	},

	animalThong : {
		name: "thong",
		subtype: "thong",
		slot:"panties",
		dad: "SuperPanties",
		dim(){ 
			const top = rg.range(0, 0.2);
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.1, top -0.2],
				genCoverage: [0.4,0.7],
				topCurve: [-4, -7],
				bow: rg.g < 0.3,
				ruffling: rg.g < 0.4,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
			}
		},
		fill: "nearlyBlack",
		texture: exgar.animal,
	},
	
	
	
	animalGString : {
		name: "g-string",
		subtype: "gString",
		slot: "panties",
		dad: "GString",
		dim(){ 
			const top = rg.range(0.0, 0.35);
			return {
				waistCoverage: [0, 0.25],
				curveBotX: [-0.5, -2.5],
				horizontalCoverage: [0.35, 0.45],
				verticalCoverage: [-0.25, -0.2],
				width: [1.5, 2],
				genCoverage: [0.3, 0.7],
		}},
		fill: "nearlyBlack",
		texture: exgar.animal,
	},
	
	
//MALE
	maleBriefs : {
		name: "briefs",
		subtype: "briefs",
		slot:"panties",
		dad: "SuperPanties",
		dim(){ 
			return {
				waistCoverage: [0, 0.25],
				waistCoverageLower: [-0.5, -0.3],
				genCoverage: 1,
				curveTopY: [0,1],
				//curveBotY: -1,
				//curveBotX: [-2,-3],
				//curveTopY: [-6,-2],
				bow: false,
				ruffling: false,
		}},
		fill: exgar.male_underwear_fill,
	},
	
	boxers : {
		name: "boxers",
		slot:"panties",
		dad: "SuperPanties",
		dim(){ return {
				waistCoverage: [0, 0.25],
				waistCoverageLower: [-1, -0.9],
				genCoverage: [0.2,0.5],
				curveTopY: [0,3],
				//curveBotX: [-2,-3],
				//curveTopY: [-6,-2],
				curveBotY: -2,
				bow: false,
				ruffling: false,
		}},
		fill: exgar.male_underwear_fill,
	},
	

	
	
	
//FANTASY
	fBlandPaties : {
		name: "panties",
		//adjective: "linen",
		slot:"panties",
		dad: "wSuperPanties",
		dim(){ 
			return {
				thickness: 0.4,
				waistCoverage:[0.1, 0.3],
				waistCoverageLower:[-0.05,-0.2],
				//waistCoverageLower:[top - 0.25, top -0.4],
				genCoverage:1,
				curveTopX: -2,
				curveTopY: [-2,0],
				bow: false,
				ruffling: false,
			}
		},
		material: exgar.bland_cloth,
	},
	
	fBlandThong : {
		name: "panties",
		//adjective: "linen",
		slot:"panties",
		dad: "wSuperPanties",
		dim(){ 
			const top = rg.range(-0.05, 0.2);
			//let bot = top - between(0.06,0.18); 
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.1, top -0.2],
				genCoverage: [0.5,0.8],
				curveTopX: [-2,-3],
				curveTopY: [-2,-6],
				curveBotX: [0,-2],
				bow: false,
				ruffling: false,
		}},
		material: exgar.bland_cloth,
	},
	
	fBlandBoxers : {
		name: "underpants",
		slot:"panties",
		dad: "SuperPanties",
		dim(){ return {
				waistCoverage: [0, 0.25],
				waistCoverageLower: [-1, -0.9],
				genCoverage: [0.2,0.5],
				curveTopY: [0,3],
				//curveBotX: [-2,-3],
				//curveTopY: [-6,-2],
				curveBotY: -2,
				bow: false,
				ruffling: false,
				//rg.g < 0.6,
				//ruffliness: 0,
		}},
		material: exgar.bland_cloth,
	},
	
	
	fBlandBra : {
		name: "bra",
		//adjective: "linen",
		slot:"bra",
		dad: "PushUpBra",
		dim(){ 
			return {
				invisible: 0,
				thickness: 0.9, //has to be the same
				strapWidth: 0.9, //has to be the same
				showStrap: true,
				neckCoverage: [0.5,0.95],
				botStrapCoverage: [0.65, 1],
				ruffling: false,
				coverage: [0.3,0.5],
			}
		},
		material: exgar.bland_cloth,
	},
	
	fPaties : {
		name: "panties",
		//adjective: "linen",
		slot:"panties",
		dad: "wSuperPanties",
		dim(){ 
			return {
				thickness: 0.4,
				waistCoverage:[0.1, 0.3],
				waistCoverageLower:[-0.05,-0.2],
				//waistCoverageLower:[top - 0.25, top -0.4],
				genCoverage:1,
				curveTopX: -2,
				curveTopY: [-2,0],
				bow: false,
				ruffling: false,
			}
		},
		fill: "dark",
	},
	
	fThong : {
		name: "panties",
		//adjective: "linen",
		slot:"panties",
		dad: "wSuperPanties",
		dim(){ 
			const top = rg.range(-0.05, 0.2);
			//let bot = top - between(0.06,0.18); 
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.1, top -0.2],
				genCoverage: [0.5,0.8],
				curveTopX: [-2,-3],
				curveTopY: [-2,-6],
				curveBotX: [0,-2],
				bow: false,
				ruffling: false,
		}},
		fill: "dark",
	},
	
	fBoxers : {
		name: "underpants",
		slot:"panties",
		dad: "SuperPanties",
		dim(){ return {
				waistCoverage: [0, 0.25],
				waistCoverageLower: [-1, -0.9],
				genCoverage: [0.2,0.5],
				curveTopY: [0,3],
				//curveBotX: [-2,-3],
				//curveTopY: [-6,-2],
				curveBotY: -2,
				bow: false,
				ruffling: false,
		}},
		fill: "maleDark",
	},

	fBra : {
		name: "bra",
		//adjective: "linen",
		slot:"bra",
		dad: "PushUpBra",
		dim(){ 
			return {
				invisible: 0,
				thickness: 0.9, //has to be the same
				strapWidth: 0.9, //has to be the same
				showStrap: true,
				neckCoverage: [0.5,0.95],
				botStrapCoverage: [0.65, 1],
				ruffling: false,
				coverage: [0.3,0.5],
			}
		},
		fill: "dark",
	},


	magicPanties : {
		name: "panties",
		//adjective: "linen",
		slot:"panties",
		dad: "wSuperPanties",
		dim(){ 
			const top = rg.range(-0.05, 0.2);
			//let bot = top - between(0.06,0.18); 
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.1, top -0.2],
				genCoverage: [0.5,0.8],
				curveTopX: [-2,-3],
				curveTopY: [-2,-6],
				curveBotX: [0,-2],
				bow: false,
				ruffling: false,
		}},
		fill: "full",
		att: exgar.magic_1,
	},
	
	
	

	
}