import {underwear} from "./underwear";

import {elixirs} from "./elixirs";
import {socks} from "./socks";
import {lower} from "./lower";
import {upper} from "./upper";
import {upperLower} from "./upperLower";
import {outer} from "./outer";
import {shoes} from "./shoes";
import {accessory} from "./accessory";
import {jewellery} from "./jewellery";
import {piercing} from "./piercing";

import {story} from "./story";
import {fantasyClothes} from "./fantasyClothes";
import {bimbo} from "./bimbo";
import {weapons} from "./weapons";
import {armor} from "./armor";
import {designs} from "./designs";

export const garment = {
	...elixirs, 
	
	...underwear,
	...socks,
	...lower,
	...upper,
	...upperLower,
	...outer,
	...shoes,
	...accessory,
	...jewellery,
	...piercing,
	
	...story,
	...fantasyClothes,
	...bimbo,
	...weapons,
	...armor,
	...designs,
}


//LEGACY
/*
garment.maleBelt = garment.darkWideBelt;
garment.polyBelt = garment.polyThinBelt;
garment.darkBelt = garment.darkThinBelt;
garment.fantasyLeggings = garment.fLeggings;
garment.fantasyPants = garment.fPants;
garment.fantasyShirt = garment.fShirt;
garment.tunis = garment.fBlouse;
garment.haremTubeTop = garment.haremTop;

garment.blandFantasyPanties = garment.fBlandPaties;
garment.blandFantasyThong  = garment.fBlandThong;
garment.blandFantasyBra = garment.fBlandBra;
*/