import {exgar} from "./exgar";
import {rg} from "Libraries/random";
import {
	glimpseLegCoverage, //0.135
	groinLegCoverage, //0.16
	microSkirtLegCoverage, //0.25
	miniSkirtLegCoverage, //0.35
	shortSkirtLegCoverage, //0.5
} from "Avatar/wardrobe/constants";


export const upperLower = {
	
	dress :{
		name: "dress",
		dad: "wSuperDress",
		slot:"upperLower",
		subtype: "dress",
		note: "medium length",
		dim(){
			const loose = rg.g < 0.6;
			return {
				bustle: rg.g < 0.3,
				legCoverage: [miniSkirtLegCoverage, 0.75],
				legLoose: [0, 0.5],
				
				sideLoose: loose ? [0.1, 0.5] : 0,
				
				curveCleavageY: [-3, 0],
				curveCleavageX: [2,6],
				cleavageOpeness: [0.05,0.3],
				cleavageCoverage: [0.1,0.2],
				
				armCoverage: [0.25, 0.85],
				armLoose: loose ? [0, 0.2] : 0,
				
		}},
		fill: exgar.upper_fill,
	},
	
	sexyDress :{
		name: "dress",
		dad: "wSuperDress",
		slot:"upperLower",
		subtype: "minidress", //TODO!!
		note: "short, deep cleavage",
		dim(){
			return {
				legCoverage: [glimpseLegCoverage, miniSkirtLegCoverage],
				legLoose: [0, 0.5],
				
				sideLoose: 0,
				
				curveCleavageX: [5, 13],
				curveCleavageY: [-5, 0],
				
				cleavageOpeness: [0.25, 0.4],
				cleavageCoverage: [0.29,0.45],
				
				armCoverage: [0.15, 0.3],
				armLoose: 0,
				
		}},
		fill: exgar.upper_fill,
	},
	
	sluttyDress :{
		name: "dress",
		dad: "wSuperDress",
		slot:"upperLower",
		subtype: "minidress", //TODO!!
		note: "shorter, very deep cleavage, maybe trasparent",
		dim(){
			return {
				legCoverage: [glimpseLegCoverage - 0.01, microSkirtLegCoverage],
				legLoose: [0, 0.5],
				
				sideLoose: 0,
				
				curveCleavageX: [5, 13],
				curveCleavageY: [-5, 0],
				
				cleavageOpeness: [0.25, 0.3],
				cleavageCoverage: [0.32,0.8],
				
				alpha: rg.g < 0.4 ? [exgar.min_transparency, 1] : 1,
				
				armCoverage: rg.g < 0.66 ? [0.12, 0.26] : 0,
				armLoose: 0,
				
		}},
		fill: exgar.upper_fill,
	},
	
//MINIDRESS
	longMinidress :{
		name: "dress",
		dad: "wMiniDress",
		slot:"upperLower",
		subtype: "minidress",
		note: "tube top with cups and straps",
		dim(){
			return {
				bustle: rg.g < 0.3,
				showStrap: rg.g < 0.75,
				legCoverage: [miniSkirtLegCoverage, shortSkirtLegCoverage],
				legLoose: [0, 0.5],
				alpha: rg.g < 0.5 ? [exgar.max_transparency, 1] : 1,
				
		}},
		fill: exgar.upper_fill,
	},
	
	sexyMinidress :{
		name: "dress",
		dad: "wMiniDress",
		slot:"upperLower",
		subtype: "minidress",
		note: "tube top with cups and straps",
		dim(){
			return {
				showStrap: rg.g < 0.7,
				legCoverage: [groinLegCoverage, miniSkirtLegCoverage],
				legLoose: 0,
				alpha: rg.g < 0.4 ? [exgar.min_transparency, 1] : 1,
				
		}},
		fill: exgar.upper_fill,
	},
	
	sluttyMinidress :{
		name: "dress",
		dad: "wMiniDress",
		slot:"upperLower",
		subtype: "minidress",
		note: "tube top with cups and straps",
		dim(){
			return {
				showStrap: rg.g < 0.5,
				legCoverage: [glimpseLegCoverage, microSkirtLegCoverage],
				legLoose: 0,
				alpha: rg.g < 0.75 ? [exgar.min_transparency, 1] : 1,
		}},
		fill: exgar.upper_fill,
	},
	
	//TODO - TWEAK CURVEX CURVEY
	longMinidress2 :{
		name: "dress",
		dad: "wMiniDress2",
		slot:"upperLower",
		subtype: "minidress",
		note: "dress with wide cleavage",
		dim(){
			const neck = rg.range(0.2, 0.6);
			return {
				innerNeckCoverage: neck,
				outerNeckCoverage: neck + rg.range(0.4, 0.8),
				cleavageCoverage: [0.15, 0.24],
				bustle: rg.g < 0.3,
				legCoverage: [miniSkirtLegCoverage, shortSkirtLegCoverage],
				legLoose: [0, 0.5],
//alpha: rg.g < 0.5 ? [0.94, 1] : 1,
//TODO - ALPHA??????
				waistCoverage: undefined,
				
		}},
		fill: exgar.upper_fill,
	},
	
	sexyMinidress2 :{
		name: "dress",
		dad: "wMiniDress2",
		slot:"upperLower",
		subtype: "minidress",
		note: "dress with wide cleavage",
		dim(){
			const neck = rg.range(0.6, 1);
			return {
				innerNeckCoverage: neck,
				outerNeckCoverage: neck + rg.range(0.1, 0.6),
				cleavageCoverage: [0.2, 0.34],
				legCoverage: [groinLegCoverage, miniSkirtLegCoverage],
				legLoose: 0,
				waistCoverage: undefined,
				//alpha: rg.g < 0.5 ? [exgar.max_transparency, 1] : 1,
				
		}},
		fill: exgar.upper_fill,
	},
	
	sluttyMinidress2 :{
		name: "dress",
		dad: "wMiniDress2",
		slot:"upperLower",
		subtype: "minidress",
		note: "dress with wide cleavage",
		dim(){
			const neck = rg.range(0.9, 1.1);
			return {
				innerNeckCoverage: neck,
				outerNeckCoverage: neck + rg.range(0.1, 0.4),
				cleavageCoverage: [0.25, 0.36],
				legCoverage: [glimpseLegCoverage, microSkirtLegCoverage],
				legLoose: 0,
				waistCoverage: undefined,
				//alpha: [exgar.min_transparency, 1]
		}},
		fill: exgar.upper_fill,
	},
	
	minidress3 :{
		name: "dress",
		dad: "wMiniDress3",
		slot:"upperLower",
		subtype: "minidress",
		note: "tube top",
		dim(){
			let armCoverage = 0;
			let armLoose = 0;
			const rand = rg.g;
			if(rand < 0.25){ //tight long
				armCoverage =  rg.range(0.35, 1);
			}else if(rand < 0.5){ //short wide
				armCoverage = rg.range(0.3, 0.5);
				armLoose = rg.range(0.2, 0.6);
			}else if(rand < 0.75){ //normal
				armCoverage = rg.range(0.35, 0.9);
				armLoose = rg.range(0.0, 0.2);
			}//none
			
			return {
				armCoverage,
				armLoose,
				chestCoverage: [0.25, 0.75],
				legCoverage: [glimpseLegCoverage, shortSkirtLegCoverage],
				legLoose: rg.g < 0.5 ? 0 : [0, 0.5],
				shoulderCoverage: [-0.05, -0.15],
				//alpha: rg.g < 0.5 ? [exgar.max_transparency, 1] : 1,
				
		}},
		fill: exgar.upper_fill,
	},
	
	dualDress : {
		name: "dual dress",
		dad: "wDualDress",
		slot:"upperLower",
		subtype: "dress",
		dim(){
			
			const curveY = rg.range(-3, 0);
			const curveX = rg.range(3,8);
			const open = rg.range(0.05,0.2);
			const cover = rg.range(0.07,0.12);
			
			return {
				sideLoose: rg.g < 0.5 ? 0 : [0, 0.8],
				armLoose: [0,0.3],
				
				...exgar.dual_shirt_arms,
				
				curveCleavageY: curveY,
				curveCleavageX: curveX,
				curveCleavageY2: curveY,
				curveCleavageX2: curveX,
				
				//waistCoverage: [-0.1, 0.4],
							
				cleavageOpeness: open,
				cleavageOpeness2: [open + 0.2, open + 0.4],
				
				cleavageCoverage: cover,
				cleavageCoverage2: [cover + 0.04, cover +0.08],
				
				legCoverage2: [-0.1, 0.2],
				legCoverage: [microSkirtLegCoverage, 0.69],
				legLoose: [0, 0.69],
			};
			
			
		},
		fill: exgar.upper_fill,
		highlight: ["contrast","monochromatic","darkGrey","nearlyBlack"],
	},
	
	sexyDualDress : {
		name: "dual dress",
		dad: "wDualDress",
		slot:"upperLower",
		subtype: "minidress",
		dim(){
			
			const curveY = rg.range(-5, 0);
			const curveX = rg.range(3, 8);
			const open = rg.range(0.05, 0.2);
			const cover = rg.range(0.1, 0.34);
			
			const legCoverage = rg.range(glimpseLegCoverage, microSkirtLegCoverage); 
			
			return {
				sideLoose: 0,
				armLoose: 0,
				
				...exgar.dual_shirt_arms,
				
				curveCleavageY: curveY,
				curveCleavageX: curveX,
				curveCleavageY2: curveY,
				curveCleavageX2: curveX,
				
				//waistCoverage: [0.5, 1.1],

				cleavageOpeness: open,
				cleavageOpeness2: [open + 0.25, open + 0.5],
				
				cleavageCoverage: cover,
				cleavageCoverage2: [cover + 0.08, cover + 0.12],
				
				legCoverage,
				legCoverage2: [-0.1, legCoverage - 0.1],
				legLoose: rg.g < 0.69 ? 0 : [0, 0.69], //TODO
			};
			
			
		},
		fill: exgar.upper_fill,
		//fishnetTexture: exgar.core_fishnet,
		highlight: ["contrast","monochromatic","darkGrey","nearlyBlack"],
	},
	
	
//NIGHTIE
	nightie :{
		name: "nightie",
		dad: "wSuperDress",
		slot:"upperLower",
		subtype: "nightie",
		dim(){
			return {
				armCoverage: [0.1, 0.35],
				cleavageOpeness: [0.1,0.3],
				
				curveCleavageX: [2,9],
				
				cleavageCoverage:  [0.1,0.2],
				legCoverage: [0.26,0.32],
				legLoose: [0.3,0.4],
				
				alpha: [0.87,0.9],
				
		}},
		fill: ["bright", "full"],
	},
	
	sexyNightie :{
		name: "sexy nightie",
		dad: "wSuperDress",
		slot:"upperLower",
		subtype: "nightie",
		dim(){
			const arm = rg.g < 0.4 ? rg.range(0,0.3) : 0;
		//	let arm = rg.range(0,0.3);
				
			return {
				armCoverage: arm,
				cleavageOpeness: [0.1,0.3],
				
				curveCleavageX: [2,9],
				
				cleavageCoverage:  [0.1,0.2],
				legCoverage: [0.15,0.21],
				legLoose: [0.3,0.4],
				
				alpha: [0.78,0.82],
				
		}},
		fill: ["bright","full"],
	},
	
	naughtyNightie :{
		name: "naughty nightie",
		dad: "wHalterTop",
		slot:"upperLower",
		subtype: "nightie",
		dim(){
			const inner = rg.range(0.1,0.2);
			const outer =  rg.range(inner + 0.1, inner + 0.4);
						
			return {
				cleavageCoverage: [0.15,0.30],
				curveCleavageY: 0,
				curveCleavageX: [6,11],
				innerNeckCoverage: inner,
				outerNeckCoverage: outer,
				waistCoverage: [-0.65,-0.45],	
				
				alpha: [0.70,0.75],
				
		}},
		fill: ["cute","barbie","bright","full"],
	},
	
	
//FANTASY
	nobleGown: {
		name: "noble gown",
		dad: "wDualDress",
		slot: "upperLower",
		note: "deep cleavage & different color of sleeves and skirt",
		dim(){ 
			return{
				armCoverage: [0.9, 0.99],
				armCoverage2: [0.5, 0.6],
				cleavageCoverage: [0.45, 0.69],
				lacing: true,
				cleavageOpeness2: -1,
				cleavageCoverage2: -1,
				legCoverage: [0.75, 0.9],
				legCoverage2: [0.1, 0.2],
				legLoose: [1, 1.5],
		}},
		fill: "full",
		highlight: "monochromatic",
	},
	
	mageRobe : {
		name: "wizard robe",
		dad: "wDualDress",
		slot:"upperLower",
		subtype: "dress",
		dim(){
			
			const curveY = rg.range(-3, 0);
			const curveX = rg.range(3,8);
			const open = rg.range(0.05,0.2);
			const cover = rg.range(0.07,0.12);
			
			return {
				sideLoose: [0.6, 0.9],
				armLoose: [0.4, 0.8],
				
				armCoverage: [0.92,0.96],
				armCoverage2: [0.88,0.90],
				armAdjust: 0.2,
				
				legLoose: [0.7, 0.9],
				legCoverage: [0.85, 0.95],
				legCoverage2: [0.78, 0.83],
				
				
				cleavageOpeness: -0.2,
				cleavageCoverage: [0.06, 0.12],
				curveCleavageX: [2, 8],
				
				cleavageOpeness2: [0.8, 0.9],
				cleavageCoverage2: [0.29, 0.49],
				curveCleavageX2: [-20, -10],
								
			};
			
			
		},
		fill: "darkGrey",
		highlight: "monochromatic",
	},
		
	polyMageRobe : {
		name: "wizard robe",
		dad: "wDualDress",
		slot:"upperLower",
		subtype: "dress",
		dim(){
			
			const curveY = rg.range(-3, 0);
			const curveX = rg.range(3,8);
			const open = rg.range(0.05,0.2);
			const cover = rg.range(0.07,0.12);
			
			return {
				sideLoose: [0.6, 0.9],
				armLoose: [0.4, 0.8],
				
				armCoverage: [0.92,0.96],
				armCoverage2: [0.88,0.90],
				armAdjust: 0.2,
				
				legLoose: [0.7, 0.9],
				legCoverage: [0.85, 0.95],
				legCoverage2: [0.78, 0.83],
				
				
				cleavageOpeness: -0.2,
				cleavageCoverage: [0.06, 0.12],
				curveCleavageX: [2, 8],
				
				cleavageOpeness2: [0.8, 0.9],
				cleavageCoverage2: [0.29, 0.49],
				curveCleavageX2: [-20, -10],
								
			};
			
			
		},
		fill: exgar.upper_fill,
		highlight: ["contrast","monochromatic","monochromatic"],
	},
	
	
	
/*	
	fishnetDualDress : {
		name: "dual dress",
		id: 552,
		dad: "wDualDress",
		slot:"upperLower",
		dim: (function(){
			
			const curveY = rg.range(-5, 0);
			const curveX = rg.range(3, 8);
			const open = rg.range(0.05, 0.2);
			const cover = rg.range(0.05, 0.34);
			
			const fi = true; // rg.g < 0.66;
	//TODO DAD HIALPHA		
			return {
				sideLoose: 0,
				armLoose: 0,
				
				
				
				...exgar.dual_shirt_arms,
				
				curveCleavageY: curveY,
				curveCleavageX: curveX,
				curveCleavageY2: curveY,
				curveCleavageX2: curveX,
				
				waistCoverage: [0.5, 1.0],

				cleavageOpeness: open,
				cleavageOpeness2: [open + 0.25, open + 0.5],
				
				cleavageCoverage: cover,
				cleavageCoverage2: [cover + 0.1, cover + 0.3],
				
				hi_fishness: fi ? 1 : 0,
				hiThickness: fi ?  0.2 : undefined,
				
				hi_alpha: fi? 1 : [exgar.min_transparency, exgar.max_transparency],
				
				legCoverage2: [glimpseLegCoverage, groinLegCoverage],
				legCoverage: [microSkirtLegCoverage, miniSkirtLegCoverage],
				legLoose: 0,
			};
			
			
		}),
		fill: ["darkGrey","darkGrey","dark","full"],
		fishnetTexture: exgar.core_fishnet,
	},	
*/		
		
		/*
	dress :{
		name: "minidress",
		dad: "wSuperDress",
		slot:"upperLower",
		dim: (function(){
			let arm=0;
			let lose=0;
			if(rg.g<0.4)arm=rg.range(0,0.2);
			if(rg.g<0.4)lose=rg.range(0,0.2);
			
			return {
				armCoverage: arm,
				cleavageOpeness: rg.range(0.1,0.6),
				cleavageCoverage:  rg.range(0.1,0.36),
				curveCleavageX: rg.range(2,9),
				legCoverage: rg.range(0.29,0.46),
				legLoose: lose,
				
		}}),
		fill: exgar.upper_fill,
	},
	
	cleavageDress :{
		name: "sexy dress",
		dad: "wSuperDress",
		slot:"upperLower",
		dim: (function(){
			return {
				midLowerCoverage: rg.range(-0.6, 0.08),
				midUpperCoverage: rg.range(-0.7, -0.4),
				
				armCoverage:  rg.range(0.75, 0.95),
				armLoose: 0,
				bustle: true,
				cleavageCoverage: rg.range(0.9, 1),
				cleavageOpeness: rg.range(-0.3, 0.2),
				lacing: true,
				curveCleavageX: rg.range(9, 14),
				curveCleavageY: rg.range(-25, 10),
				
				legCoverage: rg.range(0.43, 0.69),
				legLoose: rg.range(0.75, 1.1),
		}}),
		fill: exgar.upper_fill,
	},
	*/
}