import {exgar, rg} from "./exgar";

export const upper = {

//TUBE TOP 
	longTubeTop : {
		name: "tube top",
		dad: "TubeTop",
		slot:"upper",
		note: "to waist and more",
		dim(){ return {
				chestCoverage: [0.45, 0.95],	
				waistCoverage: [-0.4, 0.4],	
				topStrap: rg.g < 0.35,
			};
		},
		fill: exgar.upper_fill,
	},
	
	tubeTop : {
		name: "tube top",
		dad: "TubeTop",
		slot:"upper",
		note: "bit of midriff",
		dim(){
			return {
				lockStraightBot: true,
				chestCoverage: [0.3, 0.8],	
				waistCoverage: [0.3, 1.0],	
				sideLoose: rg.g < 0.3 ? [0.8, 0.99] : 0,
				topStrap: rg.g < 0.25,
			};
		},
		fill: exgar.upper_fill,
	},
	
	sluttyTubeTop : {
		name: "tube top",
		dad: "TubeTop",
		slot:"upper",
		note: "covers only breasts",
		dim(){
			return {
				lockStraightBot: true,
				chestCoverage: [0.1, 0.3],
				waistCoverage: [0.95, 1.75],
				sideLoose: rg.g < 0.3 ? [0.8, 0.99] : 0,
				topStrap: rg.g < 0.2,
			};
		},
		fill: ["bright","dark"],
	},
	
	sleevedTubeTop : {
		name: "tube top",
		dad: "TubeTopSleeves",
		slot:"upper",
		dim(){ return {
				shoulderCoverage: [-0.4, -0.2],
				armCoverage: [0.4, 0.75],	
				armLoose: rg.g < 0.4 ? 0 : [0.1,0.4],
				chestCoverage: [0.3, 0.8],	
				waistCoverage: [-0.3, 0.9],	
				topStrap: rg.g < 0.25,
			};
		},
		fill: exgar.upper_fill,
	},
	
	transparentTubeTop : {
		name: "tube top",
		dad: "TubeTopSleeves",
		slot:"upper",
		dim(){ return {
				alpha: [exgar.min_transparency, exgar.max_transparency],
				shoulderCoverage: [-0.4, -0.2],
				armCoverage: rg.g < 0.25 ? [0.4, 0.75] : 0,	
				armLoose: rg.g < 0.7 ? 0 : [0.1,0.4],
				chestCoverage: [0.3, 0.8],	
				waistCoverage: [0, 1.69],	
				topStrap: rg.g < 0.2,
			};
		},
		fill: exgar.upper_fill,
	},
	
	
	
	
	
	
	
	triangleTop : {
		name: "triangle top",
		dad: "TriangleTop",
		slot:"upper",
		dim(){
			return {
				lockStraightBot: true,
				chestCoverage: [0.5, 0.7],
				waistCoverage: [1, 1.8],
				botTriangleCoverage: [0.5, 0.7],
				topTriangleCoverage: [0.1, 0.2],
			};
		},
		fill: exgar.upper_fill,
	},
	
//BOT OPEN TOP
	botOpenTop : {
		name: "top",
		dad: "BotOpenTop",
		slot:"upper",
		dim(){
			const inner = rg.range(0, 0.15);
			const outer =  rg.range(inner, 0.5);
			return {
				cleavageCoverage: [0.05, 0.2],
				curveCleavageY: [-3, 0],
				curveCleavageX: [4, 9],
				innerNeckCoverage: inner,
				outerNeckCoverage: outer,
				waistCoverage: [0, 0.8],
				sideLoose: rg.g < 0.3 ? [0.3, 0.5] : 0,
				connectBottom: rg.g < 0.6,
				connectVertical: rg.g < 0.2,
				botOpeness: [0.2, 0.4],
				botCoverage: [0.2, 0.25],
				breastConnection: [0, 0.6],
				botCurveX: [2, 7],
				botCurveY: [-4, 0],
			};
		},
		fill: exgar.upper_fill,
	},
	
	
//HALTER TOP	
	longHalterTop : {
		name: "top",
		dad: "HalterTop",
		slot: "upper",
		dim(){
			const inner = rg.range(0, 0.15); //TODO - everything copy of basic top except waistCoverage
			const outer =  rg.range(inner + 0.05, 0.6);
			return {
				cleavageCoverage: [0.1, 0.3],
				curveCleavageY: 0,
				curveCleavageX: [6, 11],
				innerNeckCoverage: inner,
				outerNeckCoverage: outer,
				waistCoverage: [-0.4, 0.4],
				sideLoose: rg.g < 0.25 ? [0.2, 0.4] : 0,
			};
		},
		fill: exgar.upper_fill,
	},
	
	halterTop : {
		name: "halter top",
		dad: "HalterTop",
		slot:"upper",
		dim(){
			const inner = rg.range(0, 0.15);
			const outer =  rg.range(inner, 0.6);
			return {
				cleavageCoverage: [0.1, 0.34],
				curveCleavageY: 0,
				curveCleavageX: [6, 11],
				innerNeckCoverage: inner,
				outerNeckCoverage: outer,
				waistCoverage: [0.4, 1],	
			};
		},
		fill: exgar.upper_fill,
	},
	
	turtleTop : {
		name: "halter top",
		dad: "HalterTop",
		slot:"upper",
		dim(){
			const rand = rg.g;
			const cleavageCoverage = 0.03 + (-0.09 * rand);
			const innerNeckCoverage = -0.2 + (-0.4 * rand);
			return {
				curveCleavageX: 0,
				curveCleavageY: 0,
				cleavageCoverage,
				innerNeckCoverage,
				waistCoverage: [0.25, 1.25],
				outerNeckCoverage: [0.2, 1.2],
			};
		},
		fill: exgar.upper_fill,
	},
	
	//LEGACY
	sexyHalterTop : {
		name: "halter top",
		dad: "wHalterTop",
		slot:"upper",
		dim(){
			const inner = rg.range(0, 0.15);
			const outer =  rg.range(inner, 0.3);
			return {
				cleavageCoverage: [0.21, 0.35],
				curveCleavageY: [0, -6],
				curveCleavageX: [6, 11],
				innerNeckCoverage: inner,
				outerNeckCoverage: outer,
				waistCoverage:[0.25, 0.95],	
			};
		},
		fill: exgar.upper_fill,
	},
	
/*	
	sluttyHalterTop : {
		name: "halter top",
		dad: "wHalterTop",
		slot:"upper",
		dim(){
			const random = rg.g;
			
			let outer;
			let inner;
			let cleavage;
			let waist;
	
			//short
			if(random < 0.6){
				inner = rg.range(0.01, 0.35);
				outer = inner + rg.range(0.1, 0.2);
				cleavage = rg.range(0.28, 0.4);
				waist = rg.range(0.95, 1.5);	
				
			//deep	
			}else{
				//const random = rg.g;
				cleavage = rg.range(0.5, 0.7);
				waist = rg.range(0.0, 0.2)
				inner = rg.range(0, 0.1);
				outer = inner + rg.range(0.05, 0.15);
			}
		
			return {
				cleavageCoverage: cleavage,
				curveCleavageY: rg.range(0,-6),
				curveCleavageX: rg.range(6,11),
				lockStraightBot: rg.b,
				
				innerNeckCoverage: inner,
				outerNeckCoverage: outer,
				
				waistCoverage: waist,
			};
		},
		fill: exgar.upper_fill,
	},
*/	
	
	
	sluttyHalterTop : {
		name: "halter top",
		dad: "HalterTop",
		slot:"upper",
		dim(){
			const inner = rg.range(0.01, 0.35);
			const outer = inner + rg.range(0.1, 0.2);
			return {
				cleavageCoverage: [0.25, 0.5],
				curveCleavageY: [0,-6],
				curveCleavageX: [6, 11],
				//lockStraightBot: rg.b, //????
				
				innerNeckCoverage: inner,
				outerNeckCoverage: outer,
				
				waistCoverage: [0.9, 1.6],
			};
		},
		fill: exgar.upper_fill,
	},
	
	deepHalterTop : {
		name: "halter top",
		dad: "HalterTop",
		slot:"upper",
		dim(){
			const inner = rg.range(0, 0.1);
			const outer = inner + rg.range(0.05, 0.15);
			const deep = rg.g < 0.66;
			return {
				cleavageCoverage: deep ? [0.9, 0.6] : [0.45, 0.75],
				curveCleavageY: [0, -6],
				curveCleavageX: [6, 11],
				//lockStraightBot: rg.b,
				
				innerNeckCoverage: inner,
				outerNeckCoverage: outer,
				
				waistCoverage: deep ? [-0.25, 0.5] : [0.5, 1.2],
			};
		},
		fill: exgar.upper_fill,
	},
	
	
	
	shrugTop: {
		name: "shrug top",
		dad: "ShrugTop",
		slot: "upper",
		dim(){
			const shoulder = rg.g < 0.33;
			return {
				cleavageOpeness: [0, 0.4],
				cleavageCoverage: [0.06, 0.12],
				curveCleavageX: [5, 10],
				curveCleavageY: [-3, 0],
				
				armCoverage: (!shoulder && rg.g < 0.4) ? [0.12, 0.45] : [0.7, 1],
				armLoose: rg.g < 0.4 ? [0.25, 0.5] : 0,
				
				shoulderCoverage: shoulder ? [0.2, 0.5] : 1,
				shoulderCurveDepth: shoulder ? [3, 7] : 0,
				
				waistCoverage: [0, 1.3],
			};
		},
		fill: exgar.upper_fill,
		highlight: ["stroke", "monochromatic","darkGrey", "contrast"],
	},	
	
	
	blackFishnetShrugTop: {
		name: "shrug top",
		adjective: "fishnet",
		dad: "ShrugTop",
		slot: "upper",
		dim(){
			const shoulder = rg.g < 0.25;
			return {
				cleavageOpeness: [0, 0.4],
				cleavageCoverage: [0.06, 0.12],
				armCoverage: rg.g < 0.5 ? [0.7, 1] : [0.12, 0.45],
				armLoose: 0,
				waistCoverage: [0, 1.3],
			};
		},
		fill: "nearlyBlack",
		highlightTexture: exgar.core_fishnet,
		//highlight: ["stroke", "monochromatic","darkGrey", "contrast"],
	},	
	
//TODO	
	fishnetShrugTop: {
		name: "shrug top",
		dad: "ShrugTop",
		slot: "upper",
		dim(){
			const shoulder = rg.g < 0.25;
			return {
				cleavageOpeness: [0, 0.4],
				cleavageCoverage: [0.06, 0.12],
				armCoverage: rg.g < 0.5 ? [0.7, 1] : [0.12, 0.45],
				armLoose: 0,
				waistCoverage: [0, 1.3],
			};
		},
		fill: exgar.upper_fill,
		highlightTexture: exgar.core_fishnet,
		//highlight: ["stroke", "monochromatic","darkGrey", "contrast"],
	},
	
	
		
	corsetTop: {
		name: "corset",
		dad: "CorsetTop",
		slot: "upper",
		dim(){
			return {
				lacing: false,
				waistCoverage:  [0.25, 0.8],
				sides: rg.g < 0.4,
				sidesCoverage: [0.3, 0.6],
			};
		},
		fill: exgar.upper_fill,
		highlight: ["stroke", "monochromatic","darkGrey", "contrast"],
	},	
	
	blackCorsetTop: {
		name: "corset",
		adjective: "black",
		dad: "CorsetTop",
		slot: "upper",
		dim(){
			return {
				lacing: false,
				waistCoverage:  [0.25, 0.8],
			};
		},
		fill: "nearlyBlack",
		//highlight: ["stroke", "monochromatic","darkGrey", "contrast"],
	},	
	
	
	
//T-SHIRT
	tee : {
		name: "tee",
		dad: "Tee",
		slot:"upper",
		subtype: "tee",
		dim(){
			return {
				sideLoose: 0.8,
				armLoose: [0.2, 0.6],
				armCoverage: [0.3,0.55],
				curveCleavageY: [-3, 0],
				curveCleavageX: [2,6],
				waistCoverage: [-0.4, 0],
				cleavageOpeness: [0.05,0.2],
				cleavageCoverage: [0.1,0.16],
				invisible: 0,

			};
		},
		fill: exgar.upper_fill,
	},
	
	tightTee : {
		name: "tee",
		dad: "Tee",
		slot:"upper",
		subtype: "tee",
		note: "tight, possibly a bit of midriff",
		dim(){ return {
			armCoverage: [0.12, 0.45],
			cleavageOpeness: [0.1, 0.36],
			curveCleavageX: [4, 14],
			curveCleavageY: [-5, 0],
			cleavageCoverage: [0.1, 0.2],
			waistCoverage: [0.0, 0.4],
			
		}},
		fill: exgar.upper_fill,
	},
	
	stripedTightTee : {
		name: "tee",
		adjective: "striped",
		dad: "Tee",
		slot:"upper",
		subtype: "tee",
		note: "tight, possibly a bit of midriff",
		dim(){ return {
			armCoverage: [0.12, 0.45],
			cleavageOpeness: [0.1, 0.36],
			curveCleavageX: [4, 14],
			curveCleavageY: [-5, 0],
			cleavageCoverage: [0.1, 0.2],
			waistCoverage: [0.0, 0.4],
			
		}},
		texture: exgar.striped_all,
		
	},
	
	niceTightTee : {
		name: "tee",
		dad: "ClearSuperTee",
		slot:"upper",
		subtype: "tee",
		note: "tight tee with sleeve bands",
		dim(){
			let armCoverageBias = rg.range(0.02, 0.09);
			const waistCoverageBias = rg.g < 0.33 ? 0 : armCoverageBias * 5;
			const miniSleeve = rg.g < 0.3;
			if(miniSleeve || rg.g < 0.33) armCoverageBias = 0;
			
			return {
				armCoverage: miniSleeve ? [0.05, 0.25] : [0.25, 0.45],
				shoulderCoverage: (!miniSleeve & rg.g < 0.4) ? [0.25, 0.5] : 1,
				cleavageOpeness: [0.1, 0.36],
				curveCleavageX: [4, 14],
				curveCleavageY: [-5, 0],
				cleavageCoverage: [0.1, 0.2],
				waistCoverage: [0.0, 0.4],
				
				armInnerCoverage: 1,
				armCoverageBias,
				waistCoverageBias,
			}
		},
		fill: exgar.upper_fill,
		highlight: ["stroke", "monochromatic","grey", "contrast"],
	},
	
	
	longTee : {
		name: "long tee",
		dad: "Tee",
		slot:"upper",
		subtype: "tee",
		note: "t-shirt with long sleeves",
		dim(){ return {
			armCoverage: [0.65, 1.05],
			cleavageOpeness: [0.1, 0.36],
			curveCleavageX: [4, 14],
			curveCleavageY: [-5, 0],
			cleavageCoverage: [0.1, 0.2],
			waistCoverage: [-0.4, 0.2],
			
		}},
		fill: exgar.upper_fill,
	},
	
	deepTee: {
		name: "tee",
		dad: "ClearSuperTee",
		slot:"upper",
		subtype: "tee",
		dim(){
			const deep = rg.g < 0.66;
			return {
				curveCleavageY: [0, -6],
				curveCleavageX: [6, 11],
				
				cleavageOpeness: [0.1, 0.35],
				cleavageCoverage: deep ? [0.9, 0.6] : [0.45, 0.75],
				waistCoverage: deep ? [-0.25, 0.5] : [0.5, 1.2],

				shoulderCoverage: rg.g < 0.33 ? [0.1, 0.4] : 1,
				shoulderCurveDepth: [4, 8],
				
				armInnerCoverage: rg.g < 0.33 ? 0 : 1,
			};
		},
		fill: exgar.upper_fill,
	},
	
	sheerTee : {
		name: "tee",
		dad: "Tee",
		slot:"upper",
		subtype: "tee",
		dim(){ return {
			alpha: exgar.transparent,
			armCoverage: [0.12, 0.45],
			cleavageOpeness: [0.1, 0.36],
			curveCleavageX: [4, 14],
			curveCleavageY: [-5, 0],
			cleavageCoverage: [0.1, 0.2],
			waistCoverage: [0.0, 0.4],
			
		}},
		fill: exgar.upper_fill,
	}, 
	
	sluttyTee : {
		name: "tee",
		dad: "Tee",
		slot:"upper",
		subtype: "tee",
		dim(){
			const shortSleeve =  rg.g < 0.5;
			return {
				sideLoose: 0,
				armLoose: 0,
				cleavageOpeness: [0.3, 0.5],
				cleavageCoverage: [0.25, 0.45],
				curveCleavageY: [-6, 0],
				curveCleavageX: [6, 13],
				armCoverage: shortSleeve ?  [0.02, 0.2] : [0.2, 0.45],
				waistCoverage: [0.3, 1.3],
				alpha: rg.g < 0.3 ? exgar.transluncent : 1,
			};
		},
		fill: exgar.upper_fill,
	},
	
	niceSluttyTee : {
		name: "tee",
		dad: "ClearSuperTee",
		slot:"upper",
		subtype: "tee",
		dim(){
			const wide = rg.g < 0.4;
			const shoulder = !wide && rg.g < 0.4;
			const shortSleeve =  !wide && !shoulder && rg.g < 0.75;
				
			return {
				sideLoose: 0,
				armLoose: 0,
				
				cleavageOpeness: wide ? [0.7, 1.3] : [0.3, 0.5],
				cleavageCoverage:  wide ? [0.15, 0.25] : [0.25, 0.45],
				curveCleavageY: [-6, 0],
				curveCleavageX: [6, 13],
				
				armCoverage: shortSleeve ?  [0.02, 0.2] : [0.2, 0.45],
				armInnerCoverage: !wide && !shortSleeve && rg.g < 0.33 ? 0 : 1,
				
				shoulderCoverage: shoulder ? [0.1, 0.4] : 1,
				shoulderCurveDepth: [4, 8],
				
				waistCoverage: [0.6, 1.4],
				//alpha: rg.g < 0.3 ? exgar.transluncent : 1,
			};
		},
		fill: exgar.upper_fill,
		highlight: ["stroke", "monochromatic","darkGrey", "contrast", "stroke"],
	},
	
	
	fishnetTee : {
		name: "tee",
		adjective: "fishnet",
		dad: "wTee",
		slot:"upper",
		subtype: "tee",
		dim(){ 
			const turtle = rg.g < 0.6;	
			return {
				alpha: [exgar.min_transparency, exgar.max_transparency],
				armCoverage: rg.g < 0.5 ? [0.7, 1] : [0.12, 0.45],
				cleavageOpeness: turtle ? [-0.5, 0.15] : [0.1, 0.36],
				curveCleavageX: turtle ? [1, 6] : [4, 14],
				curveCleavageY: turtle ? 0 : [-5, 0],
				cleavageCoverage: turtle ? [0.03, 0.09] : [0.1, 0.2],
				waistCoverage: [0.0, 0.4],
				
				fishness: [0.7, 0.9],
				thickness: 0.2,
			
		}},
		fill: "nearlyBlack",
		fishnetTexture: exgar.core_fishnet,
	}, 
	
	
//DUAL TEE 
	dualTee : {
		name: "tee",
		dad: "ClearSuperTee",
		slot:"upper",
		subtype: "tee",
		dim(){
			
			const curveY = rg.range(-3, 0);
			const curveX = rg.range(3,8);
			
			const shortSleeve =  rg.g < 0.2;
			const shoulder = !shortSleeve && rg.g < 0.25;
			let armInnerCoverage = 0;
			let armCoverage = 0;
			
			if(shortSleeve){
				if(rg.b){
					//short normal
					armInnerCoverage = 1;
					armCoverage = [0.05, 0.2];
				}else{
					//short dark 
					armInnerCoverage = 0;
					armCoverage = [0.05, 0.2];
				}
			}else{
				switch(rg.array(["lfd", "lmd", "lmd", "mn", "md", "m"])){
					default:
					case "lfd":
						//long full dark
						armInnerCoverage = 0;
						armCoverage = [0.3, 0.7];
						break;
					
					case "lmd":
						//long most dark
						armInnerCoverage = [0.1, 1.3];
						armCoverage = [0.85, 0.1];
						break;
						
					case "mn":
						//medium normal
						armInnerCoverage = 1;
						armCoverage = [0.2, 0.45];
						break;
						
					case "md":
						//medium dark 
						armInnerCoverage = 0;
						armCoverage = [0.2, 0.45];
						break;
						
					case "m":
						//medium 
						armInnerCoverage = rg.range(0.2, 0.4);
						armCoverage = [armInnerCoverage + 0.1, armInnerCoverage + 0.4];
						break;
				}
			}
			
			/*
			
			const open = rg.range(0.05,0.2);
			const cover = rg.range(0.07,0.12);
			
			
			const wide = rg.g < 0.4;
			const shoulder = !wide && rg.g < 0.4;
			*/

			return {
				armLoose: rg.g < 0.5 ? 0 : [0.1, 0.3],
				armInnerCoverage,
				armCoverage,
				
				shoulderCoverage: shoulder ? [0.1, 0.4] : 1,
				shoulderCurveDepth: [4, 8],
				
				curveCleavageY: curveY,
				curveCleavageX: curveX,
				curveCleavageY2: curveY,
				curveCleavageX2: curveX,
				
				cleavageCoverage: [0.1, 0.3],
				cleavageInnerLock: rg.g < 0.75,
				cleavageCoverageBias: [0.05, 0.2], //ignored if lock
				cleavageOpeness: [0.1, 0.36],
				cleavageOpenessBias: rg.g < 0.8 ? [0.1, 0.6] : 0, 
				
				waistCoverage: [-0.1, 0.4],
				sideLoose: [0.6,1],
/*				
				cleavageOpeness: open,
				cleavageInnerOpeness: [open + 0.2, open + 0.4],
				
				cleavageCoverage: cover,
				cleavageInnerCoverage: [cover + 0.04, cover +0.08],
				*/

			};
			
			
		},
		fill: exgar.upper_fill,
		highlight: exgar.highlight_fill,
	},
	
	
	sluttyDualTee : {
		name: "tee",
		dad: "ClearSuperTee",
		slot:"upper",
		subtype: "tee",
		dim(){
			
			const curveY = rg.range(-5, 0);
			const curveX = rg.range(3, 8);
			
			const shortSleeve =  rg.g < 0.3;
			const shoulder = !shortSleeve && rg.g < 0.33;
			let armInnerCoverage = 0;
			let armCoverage = 0;
			
			if(shortSleeve){
				if(rg.b){
					//short normal
					armInnerCoverage = 1;
					armCoverage = [0.05, 0.2];
				}else{
					//short dark 
					armInnerCoverage = 0;
					armCoverage = [0.05, 0.2];
				}
			}else{
				switch(rg.array(["lfd", "lmd", "mn", "md", "m"])){
					default:
					case "lfd":
						//long full dark
						armInnerCoverage = 0;
						armCoverage = [0.3, 0.7];
						break;
					
					case "lmd":
						//long most dark
						armInnerCoverage = [0.1, 1.3];
						armCoverage = [0.85, 0.1];
						break;
						
					case "mn":
						//medium normal
						armInnerCoverage = 1;
						armCoverage = [0.2, 0.45];
						break;
						
					case "md":
						//medium dark 
						armInnerCoverage = 0;
						armCoverage = [0.2, 0.45];
						break;
						
					case "m":
						//medium 
						armInnerCoverage = rg.range(0.2, 0.4);
						armCoverage = [armInnerCoverage + 0.1, armInnerCoverage + 0.4];
						break;
				}
			}
			
			/*
			const open = rg.range(0.05, 0.2);
			const cover = rg.range(0.1, 0.34);
			*/
			
			return {
				armLoose: 0,
				armInnerCoverage,
				armCoverage,
				
				shoulderCoverage: shoulder ? [0.1, 0.4] : 1,
				shoulderCurveDepth: [4, 8],
				
				curveCleavageY: curveY,
				curveCleavageX: curveX,
				curveCleavageY2: curveY,
				curveCleavageX2: curveX,
				
				cleavageCoverage: [0.15, 0.4],
				cleavageInnerLock: rg.g < 0.75,
				cleavageCoverageBias: [0.05, 0.2], //ignored if lock
				cleavageOpeness: [0.1, 0.36],
				cleavageOpenessBias: rg.g < 0.8 ? [0.1, 0.6] : 0, 
				
				waistCoverage: [0.5, 1.5],
				sideLoose: 0,

			};
			
			
		},
		fill: exgar.upper_fill,
		//fishnetTexture: exgar.core_fishnet,
		highlight: exgar.highlight_fill,
	},
	
	
	
	fishnetDualTee : {
		name: "tee",
		dad: "ClearSuperTee",
		slot:"upper",
		subtype: "tee",
		dim(){
			
			const curveY = rg.range(-5, 0);
			const curveX = rg.range(3, 8);
			
			//const shortSleeve =  rg.g < 0.3;
			//const shoulder = !shortSleeve && rg.g < 0.33;
			let armInnerCoverage = 0;
			let armCoverage = 0;
			
			
			switch(rg.array(["lfd", "lmd", "md", "md", "m"])){
				default:
				case "lfd":
					//long full dark
					armInnerCoverage = 0;
					armCoverage = [0.3, 0.7];
					break;
				
				case "lmd":
					//long most dark
					armInnerCoverage = [0.1, 1.3];
					armCoverage = [0.85, 0.1];
					break;
			
				case "md":
					//medium dark 
					armInnerCoverage = 0;
					armCoverage = [0.2, 0.45];
					break;
					
				case "m":
					//medium 
					armInnerCoverage = rg.range(0.2, 0.4);
					armCoverage = [armInnerCoverage + 0.1, armInnerCoverage + 0.4];
					break;
			}
			

			return {
				armLoose: 0,
				armInnerCoverage,
				armCoverage,
				
				shoulderCoverage: 1,
				
				curveCleavageY: curveY,
				curveCleavageX: curveX,
				curveCleavageY2: curveY,
				curveCleavageX2: curveX,
				
				cleavageCoverage: [0.06, 0.12],
				cleavageInnerLock: false,
				cleavageCoverageBias: [0.05, 0.2], //ignored if lock
				cleavageOpeness: [0.05, 0.3],
				cleavageOpenessBias: rg.g < 0.8 ? [0.1, 0.6] : 0, 
				
				waistCoverage: [0.2, 1.5],
				sideLoose: 0,

			};
		},
		fill: ["darkGrey","nearlyBlack","tintedDark","dark","full","darkGrey","nearlyBlack"],
		highlightTexture: exgar.core_fishnet,
	},
/*
	//TODO TODO
	fishnetDualTee : {
		name: "dual T-shirt",
		dad: "wDualTee",
		slot:"upper",
		subtype: "tee",
		dim(){
			
			const curveY = rg.range(-5, 0);
			const curveX = rg.range(3, 8);
			const open = rg.range(0.05, 0.2);
			const cover = rg.range(0.05, 0.34);
			
			return {
				sideLoose: 0,
				armLoose: 0,
				
				...exgar.dual_shirt_arms,
				
				curveCleavageY: curveY,
				curveCleavageX: curveX,
				curveCleavageY2: curveY,
				curveCleavageX2: curveX,
				
				waistCoverage: [0.5, 1.0],

				cleavageOpeness: open,
				cleavageOpeness2: [open + 0.25, open + 0.5],
				
				cleavageCoverage: cover,
				cleavageCoverage2: [cover + 0.1, cover + 0.3],
				
				hi_fishness: 1,
				hi_thickness: 0.2,
				
				//hiFishness: 0,
				//hiThickness: 0.5,
			};
			
		},
		fill: ["darkGrey","darkGrey","dark","full"],
		fishnetTexture: exgar.core_fishnet,
	},
*/	
	
	blackTee : {
		name: "tee",
		dad: "Tee",
		slot:"upper",
		subtype: "tee",
		dim(){ return {
			armCoverage: [0.12, 0.3],
			cleavageOpeness: [0.1, 0.36],
			curveCleavageX: [4, 14],
			curveCleavageY: [-5, 0],
			cleavageCoverage: [0.1, 0.2],
			waistCoverage: [-0.1, 0.4],
			
		}},
		fill: ["nearlyBlack", "darkGrey"], //TODO
	}, 
	
	blackNiceTee : {
		name: "tee",
		dad: "ClearSuperTee",
		slot:"upper",
		subtype: "sexyTee",
		note: "covers less waist, fancier sleeves, usually open shoulders",
		dim(){
			let armCoverageBias = rg.range(0.02, 0.09);
			if(rg.g < 0.5) armCoverageBias = 0;
			const looseArms = rg.g < 0.33;
			
			return {
				armInnerCoverage: 1,
				armCoverageBias,
				armCoverage: looseArms ? [0.25, 0.6] : [0.15, 0.35] ,
				shoulderCoverage: [0.1, 0.4],
				armLoose: looseArms ? [0.25, 0.75] : 0,
				
				cleavageOpeness: [0.1, 0.36],
				curveCleavageX: [4, 14],
				curveCleavageY: [-5, 0],
				cleavageCoverage: [0.175, 0.275],
				
				waistCoverage: [0.4, 0.8],
				
				
				waistCoverageBias: -1, //TODO
				
				/*
				
				armInnerCoverage: 1,
			armCoverage: [0.12, 0.45],
			
			cleavageOpeness: [0.1, 0.36],
			curveCleavageX: [4, 14],
			curveCleavageY: [-5, 0],
			cleavageCoverage: [0.15, 0.25],
			
			waistCoverage: [0.6,  0.9],
			shoulderCoverage:  [0.1, 0.4] ,
			
			shoulderCurveDepth: [4, 8],
			
			*/
			}
		},
		//fill: exgar.upper_fill,
		fill: ["nearlyBlack", "darkGrey"], //TODO
	},
	
//MANLY
	manlyTee : {
		name: "T-shirt",
		dad: "Tee",
		slot:"upper",
		subtype: "tee",
		note: "t-shirt with manly colors (no pink)",
		dim(){
			return {
				sideLoose: 1,
				armLoose: [0.2, 0.8],
				armCoverage: [0.3,0.55],
				curveCleavageY: [-3, 0],
				curveCleavageX: [2,6],
				waistCoverage: [-0.4, 0],
				cleavageOpeness: [0.05,0.2],
				cleavageCoverage: [0.1,0.16],
				invisible: 0,

			};
		},
		fill: exgar.manly_upper_fill,
	},
	
	manlyDualTee : {
		name: "dual T-shirt",
		dad: "ClearSuperTee",
		slot:"upper",
		subtype: "tee",
		dim(){
			const armco = rg.range(0.2,0.4);
			const curveX = rg.range(3,8);
			const open = rg.range(0.05,0.2);
			const cover = rg.range(0.07,0.12);
			const curveY = rg.range(-3, 0);
			
			return {
				sideLoose: [0.6,1],
				armLoose: [0.1,0.6],
				
				armCoverage: armco + rg.range(0.15,0.3),
				armInnerCoverage: armco,
				
				curveCleavageY: curveY,
				curveCleavageX: curveX,
				curveCleavageY2: curveY,
				curveCleavageX2: curveX,
				
				waistCoverage: -0.35, //úrg.range(-0.2,0.3],
							
				
				cleavageOpeness: open,
				cleavageInnerOpeness: open + rg.range(0.2,0.4),
				
				cleavageCoverage: cover,
				cleavageInnerCoverage: cover + rg.range(0.08,0.12),
				
				/*
				hue2: 1,
				satur2: 40,
				light2: 10,
				alpha2: 1,
				*/
				hi_fishness: 0,
			};
		},
		fill: exgar.manly_upper_fill,
		highlight: ["stroke","darkGrey","nearlyBlack", "monochromatic"],
	},
	
	manlyBlackTee : {
		name: "T-shirt",
		dad: "Tee",
		slot:"upper",
		subtype: "tee",
		dim(){
			return {
				sideLoose: 1,
				armLoose: [0.2, 0.8],
				armCoverage: [0.3,0.55],
				curveCleavageY: [-3, 0],
				curveCleavageX: [2,6],
				waistCoverage: [-0.4, 0],
				cleavageOpeness: [0.05,0.2],
				cleavageCoverage: [0.1,0.16],
				invisible: 0,

			};
		},
		fill: ["nearlyBlack", "darkGrey"], //TODO
	},
	
	
//FANTASY
	fBikini : {
		name: "top",
		dad: "wBikiniTop", 
		slot:"upper",
		dim(){ return{
			radius: [3,6],
			curveCleavageY: [-2, -14],
		}},
		material: exgar.bland_cloth,
	}, 
	
	fPolyBikini : {
		name: "top",
		dad: "wBikiniTop", 
		slot:"upper",
		dim(){ return{
			radius: [3,6],
			curveCleavageY: [-2, -14],
		}},
		fill: exgar.fantasy_upper_fill,
	}, 
	
	haremTop : { //sleeved, short, loose
		name: "sheer top",
		dad: "TubeTopSleeves",
		slot:"upper",
		dim(){ return {
				shoulderCoverage: [-0.2, -0.02],
				shoulderCurveDepth: 0,
				armCoverage: [0.35, 0.45],	
				armLoose: [0.6, 0.8],
				waistCoverage: [0.6, 0.9],	
				sideLoose: [1.1, 1.5],
				chestCoverage: [0.3, 0.6],	
				alpha: [exgar.min_transparency, exgar.max_transparency],
				topStrap: false,
			};
		},
		fill: exgar.upper_fill,
	},
	
	
//HARLOT	
	defHarlotTop : {
		name: "blouse",
		dad: "wTubeTop", 
		slot:"upper",
		dim(){ return{
			armCoverage: 0.54,
			armLoose: 0.4,
			chestCoverage: 0.66,
			shoulderCoverage: 0,
			sideLoose: 1,
		}},
		material: exgar.bland_cloth,
	}, 

	harlotTop : {
		name: "blouse",
		dad: "wTubeTop", 
		slot:"upper",
		dim(){ return{
			armCoverage: [0.36,0.77],
			armLoose: rg.g < 0.3 ? 0 : [0.1,0.6],
			chestCoverage: [0.12,0.55],
			waistCoverage: [-0.33,0.66],
			shoulderCoverage: 0,
			sideLoose: [0,1],
		}},
		material: exgar.bland_cloth,
	}, 
	
	polyHarlotTop : {
		name: "blouse",
		dad: "wTubeTop", 
		slot:"upper",
		dim(){ return{
			armCoverage: [0.36,0.77],
			armLoose: rg.g < 0.3 ? 0 : [0.1,0.6],
			chestCoverage: [0.12,0.55],
			waistCoverage: [-0.33,0.66],
			shoulderCoverage: 0,
			sideLoose: [0,1],
		}},
		fill: exgar.fantasy_upper_fill,
	}, 
	
	
//HELLION
	defBarbaricBra : {
		name: "bra",
		dad: "wBarbaricBra",
		slot:"upper",
		dim(){ return{
			//fill: "hsla(33, 45%, 35%, 1)",
			//highlight: "hsla(33, 80%, 10%, 1)",
		}},
		fill: "leather",
	}, 
	
	barbaricBra : {
		name: "bra",
		dad: "wBarbaricBra",
		slot: "upper",
		dim(){ return{
			hi_thickness: [3,6], 
		}},
		fill: exgar.leather_fill,
	}, 
	
	fancyBarbaricBra : {
		name: "bra",
		dad: "wBarbaricBra",
		slot: "upper",
		dim(){ return{
			hi_thickness: [3,6], 
		}},
		material: exgar.rare_leather,
	}, 
	
	
//FALLEN
	defFallenTop : {
		name: "top",
		dad: "wFallen",
		slot: "upper",
		dim(){ return{
			botOutCoverage: 0.9,
			botInCoverage: 0.05,
			beltWidth: 6,
			waistCoverage: 0.3,
		}},
		fill: "purple",
		highlight: "darkLeather",
	},
	
	fallenTop : {
		name: "top",
		dad: "wFallen",
		slot: "upper",
		dim(){ return{
			botInCoverage: [0,0.2],
			botOutCoverage:[0.6,0.8],
			topInCoverage: [0.0,0.1],
			topOutCoverage: [0.1,0.5],
			middle: [0,5],
			bellyX: [-9,-18],
			waistCoverage: 0.3,
			beltWidth: 6,
		}},
		fill: exgar.fallen_fill,
		highlight: "darkLeather",
	},
	
	fBlouse : {
		name: "blouse",
		dad: "wTee", 
		slot:"upper",
		dim(){ return{
			armCoverage: rg.range(0.4,0.95),
			armLoose: rg.range(0,0.4),
			lacing: true,
			cleavageOpeness: rg.range(0.0,0.25),
			chestCoverage: rg.range(0.13,0.27),
			waistCoverage: rg.range(-0.5,0.4),
			sideLoose: rg.range(0,0.3),
		}},
		material: exgar.bland_cloth,
	}, 
	
	fPolyBlouse : {
		name: "blouse",
		dad: "wTee", 
		slot:"upper",
		dim(){ return{
			armCoverage: rg.range(0.4,0.95),
			armLoose: rg.range(0,0.4),
			lacing: true,
			cleavageOpeness: rg.range(0.0,0.25),
			chestCoverage: rg.range(0.13,0.27),
			waistCoverage: rg.range(-0.5,0.4),
			sideLoose: rg.range(0,0.3),
		}},
		fill: exgar.fantasy_upper_fill,
	}, 
	
	fShirt : {
		name: "shirt",
		dad: "wTee",
		slot:"upper",
		dim(){
			return {
				lacing: true,
				sideLoose: 1,
				armLoose: [0.2, 0.8],
				armCoverage: [0.55,0.9],
				curveCleavageY: [-3, 0],
				curveCleavageX: [1, 4],
				waistCoverage: [-0.4, 0],
				cleavageOpeness: [0.05,0.2],
				cleavageCoverage: [0.1,0.16],
				invisible: 0,

			};
		},
		material: exgar.bland_cloth,
	},
	
	fPolyShirt : {
		name: "shirt",
		dad: "wTee",
		slot:"upper",
		dim(){
			return {
				lacing: true,
				sideLoose: 1,
				armLoose: [0.2, 0.8],
				armCoverage: [0.55,0.9],
				curveCleavageY: [-3, 0],
				curveCleavageX: [1, 4],
				waistCoverage: [-0.4, 0],
				cleavageOpeness: [0.05,0.2],
				cleavageCoverage: [0.1,0.16],
				invisible: 0,

			};
		},
		fill: exgar.fantasy_upper_fill,
	},
	
	
	

}