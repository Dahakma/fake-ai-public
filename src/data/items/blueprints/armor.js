import {rg, exgar} from "./exgar";

export const armor = {
	
//LEATHER ARMOR 
	leatherArmor: {
		name: "armor",
		dad: "wCorset",
		slot: "upperOuter",
		type: "cuirass",
		dim(){return{
			waistCoverage: [-0.1, 0.4],
		}},
		material: exgar.leather,
		//highlight: "darkGrey",
		
	},
	
	leatherVambraces: {
		name: "vambraces",
		dad: "wVambraces",
		slot: "vambraces",
		type: "vambraces",
		dim(){return{
		}},
		material: exgar.leather,
	},
	
	leatherPauldrons: {
		name: "pauldrons",
		dad: "wPauldron",
		slot: "pauldrons",
		type: "pauldrons",
		dim(){return{
		}},
		material: exgar.leather,
	},
	
	leatherSkirt: {
		name: "skirt",
		dad: "wCingulum",
		slot: "lowerOuter",
		type: "cingulum",
		dim(){return{
			waistCoverage: 0.33,
			number: 5,
			crest: [0.8, 2],
			space: 0,//WARDROBE.between(0,1),
			pointy: [4, 8],
			margin: [0, 2],
			length: [25, 40],
		}},
		material: exgar.leather,
	},
	
	
//RARE LEATHER ARMOR 
	fancyLeatherArmor: {
		name: "armor",
		dad: "wCorset",
		slot: "upperOuter",
		type: "cuirass",
		dim(){return{
			waistCoverage: [-0.1, 0.4],
		}},
		material: exgar.rare_leather,
		//highlight: "darkGrey",
		
	},
	
	fancyLeatherVambraces: {
		name: "vambraces",
		dad: "wVambraces",
		slot: "vambraces",
		type: "vambraces",
		dim(){return{
		}},
		material: exgar.rare_leather,
	},
	
	fancyLeatherPauldrons: {
		name: "pauldrons",
		dad: "wFancyPauldron",
		slot: "pauldrons",
		type: "pauldrons",
		dim(){return{		
		}},
		material: exgar.rare_leather,
	},
	
	fancyLeatherSkirt: {
		name: "skirt",
		dad: "wCingulum",
		slot: "lowerOuter",
		type: "cingulum",
		dim(){return{
			waistCoverage: 0.33,
			number: 5,
			crest: [0.8, 2],
			space: 0,//WARDROBE.between(0,1),
			pointy: [4, 8],
			margin: [0, 2],
			length: [25, 40],
		}},
		att: exgar.minor_magic_1,
		material: exgar.rare_leather,
	},	
	
	magicFancyLeatherArmor: {
		name: "armor",
		dad: "wCorset",
		slot: "upperOuter",
		type: "cuirass",
		dim(){return{
			waistCoverage: [-0.1, 0.4],
		}},
		att: exgar.magic_1,
		material: exgar.rare_leather,
		//highlight: "darkGrey",
		
	},
	
	magicFancyLeatherVambraces: {
		name: "vambraces",
		dad: "wVambraces",
		slot: "vambraces",
		type: "vambraces",
		dim(){return{
		}},
		att: exgar.minor_magic_1,
		material: exgar.rare_leather,
	},
	
	magicFancyLeatherPauldrons: {
		name: "pauldrons",
		dad: "wFancyPauldron",
		slot: "pauldrons",
		type: "pauldrons",
		dim(){return{		
		}},
		att: exgar.minor_magic_1,
		material: exgar.rare_leather,
	},
	
//OTHER LEATHER
	leatherCuirass: {
		name: "cuirass",
		dad: "wCuirass",
		slot: "upperOuter",
		type: "cuirass",
		dim(){return{
		}},
		material: exgar.leather,
	},
	
	
//BRONZE
	bronzeCuirass: {
		name: "cuirass",
		dad: "wCuirass",
		slot: "upperOuter",
		type: "cuirass",
		dim(){return{
		}},
		material: "bronze",
	},
	
	bronzeGorget : {
		name: "gorget",
		dad: "wGorget",
		slot: "neck",
		type: "gorget",
		dim(){ 
			const bot = rg.g;
			const top =  rg.g;
			return{
				cleavageCoverage: rg.range(0.14, 0.19, bot),
				neckBotCoverage: rg.range(0.66, 0.99, bot),
				cleavageTopCoverage: rg.range(0, 0.029, top),
				neckTopCoverage: rg.range(-0.21, 0.0, top),
				//groove: WARDROBE.Rnd() < 0.4 ? true : false,
		}},
		material: "bronze",
	},
	
	bronzeVambraces: {
		name: "vambraces",
		dad: "wVambraces",
		slot: "vambraces",
		type: "vambraces",
		dim(){return{
		}},
		material: "bronze",
	},
	
	bronzePauldrons: {
		name: "pauldrons",
		dad: "wPauldron",
		slot: "pauldrons",
		type: "pauldrons",
		dim(){return{	
		}},
		material: "bronze",
	},
	
	bronzeGreaves: {
		name: "greaves",
		dad: "wGreaves",
		slot: "greaves",
		type: "greaves",
		dim(){return{
		}},
		material: "bronze",
	},
	

	magicBronzeCuirass: {
		name: "cuirass",
		dad: "wCuirass",
		slot: "upperOuter",
		type: "cuirass",
		dim(){return{
		}},
		att: exgar.major_magic_1,
		material: "bronze",
	},
	
	magicBronzeGorget : {
		name: "gorget",
		dad: "wGorget",
		slot: "neck",
		type: "gorget",
		dim(){ 
			const bot = rg.g;
			const top =  rg.g;
			return{
				cleavageCoverage: rg.range(0.14, 0.19, bot),
				neckBotCoverage: rg.range(0.66, 0.99, bot),
				cleavageTopCoverage: rg.range(0, 0.029, top),
				neckTopCoverage: rg.range(-0.21, 0.0, top),
				//groove: WARDROBE.Rnd() < 0.4 ? true : false,
		}},
		att: exgar.minor_magic_1,
		material: "bronze",
	},
	
	magicBronzeVambraces: {
		name: "vambraces",
		dad: "wVambraces",
		slot: "vambraces",
		type: "vambraces",
		dim(){return{
		}},
		att: exgar.minor_magic_1,
		material: "bronze",
	},
	
	magicBronzePauldrons: {
		name: "pauldrons",
		dad: "wPauldron",
		slot: "pauldrons",
		type: "pauldrons",
		dim(){return{	
		}},
		att: exgar.minor_magic_1,
		material: "bronze",
	},
	
	magicBronzeGreaves: {
		name: "greaves",
		dad: "wGreaves",
		slot: "greaves",
		type: "greaves",
		dim(){return{
		}},
		att: exgar.minor_magic_1,
		material: "bronze",
	},
	
	
//STEEL
	steelCuirass: {
		name: "cuirass",
		dad: "wCuirass",
		slot: "upperOuter",
		type: "cuirass",
		dim(){return{
		}},
		material: "steel",
	},
	
	steelGorget : {
		name: "gorget",
		dad: "wGorget",
		slot: "neck",
		type: "gorget",
		dim(){ 
			const bot = rg.g;
			const top =  rg.g;
			return{
				cleavageCoverage: rg.range(0.14, 0.19, bot),
				neckBotCoverage: rg.range(0.66, 0.99, bot),
				cleavageTopCoverage: rg.range(0, 0.029, top),
				neckTopCoverage: rg.range(-0.21, 0.0, top),
				//groove: WARDROBE.Rnd() < 0.4 ? true : false,
		}},
		material: "steel",
	},
	
	steelVambraces: {
		name: "vambraces",
		dad: "wVambraces",
		slot: "vambraces",
		type: "vambraces",
		dim(){return{
		}},
		material: "steel",
	},
	
	steelPauldrons: {
		name: "pauldrons",
		dad: "wPauldron",
		slot: "pauldrons",
		type: "pauldrons",
		dim(){return{	
		}},
		material: "steel",
	},
	
	steelGreaves: {
		name: "greaves",
		dad: "wGreaves",
		slot: "greaves",
		type: "greaves",
		dim(){return{
		}},
		material: "steel",
	},
	
//STEEL
	glassCuirass: {
		name: "cuirass",
		dad: "wCuirass",
		slot: "upperOuter",
		type: "cuirass",
		dim(){return{
		}},
		material: "glass",
	},
	
	glassVambraces: {
		name: "vambraces",
		dad: "wVambraces",
		slot: "vambraces",
		type: "vambraces",
		dim(){return{
		}},
		material: "glass",
	},
	
	glassPauldrons: {
		name: "pauldrons",
		dad: "wPauldron",
		slot: "pauldrons",
		type: "pauldrons",
		dim(){return{	
		}},
		material: "glass",
	},
	
	glassGorget : {
		name: "gorget",
		dad: "wGorget",
		slot: "neck",
		type: "gorget",
		dim(){ 
			const bot = rg.g;
			const top =  rg.g;
			return{
				cleavageCoverage: rg.range(0.14, 0.19, bot),
				neckBotCoverage: rg.range(0.66, 0.99, bot),
				cleavageTopCoverage: rg.range(0, 0.029, top),
				neckTopCoverage: rg.range(-0.21, 0.0, top),
				//groove: WARDROBE.Rnd() < 0.4 ? true : false,
		}},
		material: "glass",
	},
	
	glassGreaves: {
		name: "greaves",
		dad: "wGreaves",
		slot: "greaves",
		type: "greaves",
		dim(){return{
		}},
		material: "glass",
	},
	
}