import {exgar} from "./exgar";
import {rg} from "Libraries/random";
import {
	glimpseLegCoverage, //0.135
	groinLegCoverage, //0.16
	microSkirtLegCoverage, //0.25
	miniSkirtLegCoverage, //0.35
	shortSkirtLegCoverage, //0.5
	stockingsLegCoverage,
} from "Avatar/wardrobe/constants";



	

	
export const bimbo = {
	
//BRA
	bimboCageBra : {
		name: "bra",
		slot:"bra",
		dad: "CageBra",
		dim(){ 
			const fancy = rg.b;
			return {
				thickness: [0.4,0.6],
				neckCoverage: [0.45, 0.8],
				topStrapWidth: [0.9, 1.6],
				botStrapCoverage: rg.g < 0.65 ? [0.7, 1] : [1.25, 2],
				cageSpacing: [1.3, 2.2],
				
				cageCenter: fancy,
				cageCross: !fancy,
				cageDouble: rg.g < 0.5,
				ruffling: rg.g < 0.33,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
			}
			/*
			if(rg.g <0.7){
				return {
					invisible: 0,
					thickness: [0.4,0.6],
					neckCoverage: [0.5,0.95],
					strapWidth: [0.7,1.5],
					botStrapCoverage: [0.8, 1.1],
					cage: true,
					ruffling: false,
					cageSpacing: [1,2],
				}
			}else{
				return {
					invisible: 0,
					thickness: [0.4,0.6],
					strapWidth: [0.8,2.2],
					botStrapCoverage: [0.8, 1.1],
					cage: false,
					ruffling: rg.g > 0.6,
				}
			}
			*/
		},
		fill: exgar.bimbo_fill,
	},
	
	bimboPushUpBra : {
		name: "bra",
		slot:"bra",
		dad: "PushUpBra",
		dim(){ 
			const fancy = rg.g;
			return {
				thickness: 0.9, //has to be the same
				strapWidth: 0.9, //has to be the same
				showStrap: true,
				neckCoverage: [0.5,0.95],
				botStrapCoverage: rg.g < 0.65 ? [0.7, 1] : [1.25, 2],
				ruffling: fancy < 0.65,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				cupCoverage: fancy > 0.75 ? [-1, -0.25] :  [0, 0.3],
			};
			/*
			return {
				invisible: 0,
				thickness: 0.9, //has to be the same
				strapWidth: 0.9, //has to be the same
				showStrap: true,
				neckCoverage: [0.5,0.95],
				botStrapCoverage: [0.65, 1],
				ruffling: rg.g > 0.3,
				coverage: [0.3,0.5],
			};
			*/
		},
		fill: exgar.bimbo_fill,
	},
	
	
//PANTIES
	bimboHipsters : {
		name: "hipster panties",
		subtype: "hipsters",
		slot:"panties",
		
		dad: "SuperPanties",
		dim(){
			const top = rg.range(0.1,0.2);
			const bow = rg.g < 0.33;
			return {
				thickness: [0.3, 0.4],
				waistCoverage: top,
				waistCoverageLower:[top - 0.2, top -0.4],
				genCoverage:1,
				topCurve: [-3, -6],
				bow,
				ruffling: rg.g < 0.66,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				topBandWidth: !bow && rg.g < 0.6 ? [1, 3] : 0,
			}
		},
		fill: exgar.bimbo_fill,
	},
	
	
	bimboThong : {
		name: "thong",
		slot:"panties",	
		dad: "SuperPanties",
		dim(){ 
			const top = rg.range(-0.025, 0.15);
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.1, top -0.2],
				genCoverage: [0.4,0.7],
				topCurve: [-4, -7],
				bow: rg.g < 0.3,
				ruffling: rg.g < 0.5,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
			}
		},
		fill: exgar.bimbo_fill,
	},

	bimboGString : {
		name: "g-string",
		slot:"panties",
		dad: "GString",
		dim(){ 
			const top = rg.range(0.0, 0.35);
			return {
				waistCoverage: [0, 0.25],
				curveBotX: [-1, -2.5],
				horizontalCoverage: [0.35, 0.45],
				verticalCoverage: [-0.25, -0.2],
				width: [1.5, 2],
				genCoverage: [0.3, 0.7],
		}},
		fill: exgar.bimbo_fill,
	},

	bimboCagePanties : {
		name: "g-string",
		subtype: "gString",
		slot: "panties",
		dad: "CagePanties",
		dim(){ 
			const top = rg.range(0.0, 0.35);
			return {
				waistCoverage: [0, 0.25],
				curveBotX: [-1, -2.5],
				horizontalCoverage: [0.35, 0.45],
				verticalCoverage: [-0.25, -0.2],
				thickness: [1, 2],
				genCoverage: [0.3, 0.7],
				topCoverage: [0, 0.25],
		}},
		fill: exgar.bimbo_fill,
	},
	
	bimboCrotchlessThong : {
		name: "open-crotch thong",
		subtype: "crotchless",
		slot:"panties",
		dad: "SuperPanties",
		dim(){ 
			const top = rg.range(0, 0.2);
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.1, top -0.2],
				genCoverage: [0.4,0.7],
				topCurve: [-4, -7],
				bow: rg.g < 0.6,
				ruffling: true,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				openCrotch: true,
			}
		},
		fill: exgar.bimbo_fill,
	},
	
//SOCKS
	bimboSocks : {
		name: "socks",
		dad: "SuperSocks",
		slot:"socks",
		dim(){ return{
			legCoverage:[0.8,1],
			thickness: [0.5,1],
		}},
		fill: exgar.bimbo_fill,
	},
	
	bimboStockings : {
		name: "stockings",
		subtype: "stockings",
		slot:"socks",
		dad: "Stockings",
		note: "colorful",
		dim(){ return {
			bandWidth: [5,10],
			legCoverage: [0.24, stockingsLegCoverage - 0.01],
			alpha: [0.4, 0.9],
			thickness: [0.3, 0.5],
		}},
		fill: exgar.bimbo_fill,
	},
	
	bimboGarterStockings : {
		name: "stockings",
		subtype: "stockings",
		dad: "StockingsGarter",
		slot:"socks",
		note: "stockings with garter belt",
		dim(){ return{
			bandWidth: [3, 9],
			beltWidth: [6, 10],
			legCoverage: [0.17, 0.25],
			thickness: 0.2,
			waistCoverage: [0.2, 0.34],	
			alpha: [0.7, 0.9],
		}},
		fill: exgar.bimbo_fill,
	}, 
	
	
	bimboPantyhose : {
		name: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		
		dim(){ return {
			fishness: 0,
			waistCoverage: [0.2,0.6],
			alpha: [0.4,0.9],	
			thickness: [0.25, 0.35],
			openWidth: 0,
		}},
		fill: exgar.bimbo_fill,
	},
	
	bimboTights : {
		name: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		
		dim(){ 
			const open = rg.g < 0.35 
			return {
			//fishness: 0,
				waistCoverage: [0.2,0.4],
				alpha: [0.7, 0.9],		
				thickness: [0.5, 0.7],
				openWidth: open ? [0.9, 1] : 0,
				openHeight: open ? [0.7, 0.9] : 0,
				openSide: true,
		}},
		fill: exgar.bimbo_fill,
	},	
	
	
	bimboOpenPantyhose : { //TODO!!!
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		note: "light color, crotch open",
		dim(){ return {
			//fishness: 0,
			waistCoverage: [0.2,0.4],
			alpha: [0.7, 0.9],	
			thickness: [0.5, 0.7],
			openWidth: [0.9, 1],
			openHeight: [0.7, 0.9],
			openSide: true,
		}},
		fill: exgar.bimbo_fill, //socks_fill,
	},	
	
	
//LOWER
	bimboMiniSkirt : {
		name: "miniskirt",
		dad: "wSuperSkirt",
		slot:"lower",
		
		dim(){ return {
			button: false,
			innerLoose: 1.1,
			outerLoose: rg.g < 0.4 ? [0, 0.8] : [0.9, 1.5],
			waistCoverage: [-0.05, 0.15],
			legCoverage: [microSkirtLegCoverage + 0.01, miniSkirtLegCoverage - 0.01],
		}},
		fill: exgar.bimbo_fill,
	},
	
	/*
	bimboMicroSkirt : {
		name: "microskirt",
		dad: "wSuperSkirt",
		slot:"lower",
		
		dim(){ return {
			button: rg.g < 0.3,
			innerLoose: 1.1,
			outerLoose: rg.g <0.4 ? [0, 0.8] : [0.9, 1.5],
			waistCoverage:[-0.1, 0.05],
			legCoverage: [0.11, 0.18],
		}},
		fill: exgar.bimbo_fill,
	},
	*/
	
	bimboMicroSkirt : {
		name: "microskirt",
		dad: "SuperSkirt",
		slot:"lower",
		note: "longer than microSkirt, loose",
		dim(){ return {
			button: false,
			innerLoose: 1.1,
			//outerLoose: rg.g <0.4 ? [0, 0.8] : [0.9, 1.5],
			outerLoose:  [0.9, 1.5],
			waistCoverage:[-0.1, 0.1],
			legCoverage: [0.2, microSkirtLegCoverage - 0.01],
		}},
		fill: exgar.bimbo_fill,
	},
	
	bimboNanoSkirt : {
		name: "nanoskirt",
		dad: "SuperSkirt",
		slot:"lower",
		note: "shorter than regular nanoSkirt, always shows panties",
		dim(){ return {
			button: false,
			innerLoose: 1.1,
			outerLoose: rg.g <0.4 ? [0, 0.8] : [0.9, 1.5],
			waistCoverage:[-0.1, 0.05],
			legCoverage: [0.11,  glimpseLegCoverage],
			//legCoverage: [0.11, groinLegCoverage],
		}},
		fill: exgar.bimbo_fill,
	},
	
	bimboBorderlineSkirt : {
		name: "microskirt",
		dad: "SuperSkirt",
		slot:"lower",
		note: "shorter than microSkirt, longer than nanoSkirt, always risk of panties",
		dim(){ return {
			button: false,
			innerLoose: 1.1,
			outerLoose: rg.g <0.4 ? [0, 0.8] : [0.9, 1.5],
			waistCoverage:[-0.1, 0.1],
			legCoverage: [glimpseLegCoverage,  groinLegCoverage],
			//legCoverage: [glimpseLegCoverage, microSkirtLegCoverage - 0.01],
		}},
		fill: exgar.bimbo_fill,
	},
	
	
	
	
	
	bimboLeggings : {
		name: "leggings",
		dad: "wSuperLeggins",
		slot:"lower",
		
		dim(){
			return {
				waistCoverage: [-0.08, 0.24],
				legCoverage: [0.85, 1.00],
				innerLoose: 0,
				outerLoose: 0,
				thickness: [0.7, 0.8],
				beltWidth: rg.g < 0.34 ? 0 : [0, 4],
				lampasse: 0,
		}},
		fill: exgar.bimbo_fill,
		highlight: ["monochromatic","darkGrey"],
	}, 
	
	bimboGymLeggings : {
		name: "leggings",
		dad: "wSuperLeggins",
		slot:"lower",
		
		dim(){
			return {
				waistCoverage: [-0.08, 0.24],
				legCoverage: [0.85, 1.00],
				innerLoose: 0,
				outerLoose: 0,
				thickness: [0.7, 0.8],
				beltWidth: rg.g < 0.4 ? [0, 5] : [0, 10],
				lampasse: rg.g < 0.25 ? 0 : [2, 4.5],
				
		}},
		fill: exgar.bimbo_fill,
		highlight: ["monochromatic","grey","contrast"],
	}, 
	
	
//UPPER
	bimboTubeTop : {
		name: "tube top",
		dad: "TubeTop",
		slot:"upper",
		dim(){
			return {
				alpha: rg.g < 0.33 ? [exgar.max_transparency, 1] : 1,
				chestCoverage: [0.1, 0.6],	
				waistCoverage: [0.3, 1.69],	//TODO HOW MUCH SLUTTY
				sideLoose: rg.g < 0.3 ? [0.8, 0.99] : 0,
			};
		},
		fill: exgar.bimbo_fill,
	},
	
	/*
	cleavageInnerOpeness: 0, 
	armInnerCoverage: 0,
	waistInnerCoverage: 0,
	zipper: false, 
	cleavageInnerLock: false,
	*/
	
	bimboTee : {
		name: "tee",
		
		dad: "ClearSuperTee",
		slot:"upper",
		dim(){ return {
			armCoverage: [0.12, 0.45],
			cleavageOpeness: [0.1, 0.36],
			curveCleavageX: [4, 14],
			curveCleavageY: [-5, 0],
			cleavageCoverage: [0.1, 0.2],
			waistCoverage: [0.2, 1],
			shoulderCoverage: rg.g < 0.33 ? [0.1, 0.4] : 1,
			armInnerCoverage: rg.g < 0.33 ? 0 : 1,
			shoulderCurveDepth: [4, 8],
			alpha: rg.g < 0.3 ? exgar.transluncent : 1,
			
		}},
		fill: exgar.bimbo_fill,
	}, 
	
	
	bimboNiceTee : {
		name: "tee",
		note:  "not slutty",
		dad: "ClearSuperTee",
		slot:"upper",
		dim(){ return {
			armInnerCoverage: 1,
			armCoverage: [0.12, 0.45],
			
			cleavageOpeness: [0.1, 0.36],
			curveCleavageX: [4, 14],
			curveCleavageY: [-5, 0],
			cleavageCoverage: [0.15, 0.25],
			
			waistCoverage: [0.6,  0.9],
			shoulderCoverage:  [0.1, 0.4] ,
			
			shoulderCurveDepth: [4, 8],
			
		}},
		fill: exgar.bimbo_fill,
	}, 
	
	bimboTriangleTop : {
		name: "triangle top",
		dad: "TriangleTop",
		slot:"upper",
		dim(){
			return {
				lockStraightBot: true,
				chestCoverage: [0.5, 0.7],
				waistCoverage: [1, 1.8],
				botTriangleCoverage: [0.5, 0.7],
				topTriangleCoverage: [0.1, 0.2],
			};
		},
		fill: exgar.bimbo_fill,
	},
	
	bimboSluttyTubeTop : {
		name: "tube top",
		dad: "TubeTop",
		slot:"upper",
		dim(){
			return {
				alpha: [exgar.max_transparency, 1] ,
				chestCoverage: [0.1, 0.6],	
				waistCoverage: [1.3, 1.6],	//TODO HOW MUCH SLUTTY
				sideLoose:  0,
			};
		},
		fill: exgar.bimbo_fill,
	},
	
//TODO
	bimboHalterTop : {
		name: "halter top",
		
		dad: "wHalterTop",
		slot:"upper",
		dim(){
			const random = rg.g;
			
			let outer;
			let inner;
			let cleavage;
			let waist;
			
			//wide
			if(random < 0.4){
				outer =  rg.range(0.9, 1.2);
				inner = outer - rg.range(1.2, 2);
				cleavage = rg.range(0.25,0.36);
				waist = rg.range(0.6,1.2);
			//short
			}else if(random < 7){
				outer = rg.range(0.15,0.1);
				inner = outer - rg.range(1.2, 3);
				cleavage = rg.range(0.2,0.3);
				waist = rg.range(0.95,1.5);
			//deep	
			}else{
				//const random = rg.g;
				cleavage = rg.range(0.5, 0.7);
				waist = rg.range(0.0, 0.2);
				
				inner = rg.range(0, 0.1);
				outer = inner + rg.range(0.05, 0.15);
			}
			
			
			return {
				cleavageCoverage: cleavage,
				curveCleavageY: rg.range(0,-6),
				curveCleavageX: rg.range(6,11),
				
				innerNeckCoverage: inner,
				outerNeckCoverage: outer,
				
				waistCoverage: waist,
			};
		},
		fill: exgar.bimbo_fill,
	},
	
	
	bimboBotOpenTop : {
		name: "top",
		dad: "BotOpenTop",
		slot:"upper",
		dim(){
			const inner = rg.range(0, 0.15);
			const outer =  rg.range(inner, 0.5);
			return {
				cleavageCoverage: [0.05, 0.2],
				curveCleavageY: [-3, 0],
				curveCleavageX: [4, 9],
				innerNeckCoverage: inner,
				outerNeckCoverage: outer,
				waistCoverage: [0, 0.8],
				sideLoose: rg.g < 0.3 ? [0.3, 0.5] : 0,
				connectBottom: rg.g < 0.6,
				connectVertical: rg.g < 0.2,
				botOpeness: [0.2, 0.4],
				botCoverage: [0.2, 0.25],
				breastConnection: [0, 0.6],
				botCurveX: [2, 7],
				botCurveY: [-4, 0],
			};
		},
		fill: exgar.bimbo_fill,
	},
	
//OUTER 
	bimboJacket : {
		name: "jacket",
		
		dad: "wSuperSuit",
		slot:"upperOuter",
		dim(){
			const open = 0.3 + (rg.g * 0.2);
			return {
				waistCoverage: [0.5, 0.8],
				sideLoose: [0, 0.8],
				cleavageCoverage: [0.23, 0.5],
				topOpen: open - 0.05,
				botOpen: open, //TODO
				armCoverage: [0.4, 0.55],
				
				lapelCoverage: [0.69, 0.75],
				lapelDifference: [0.6, 0.9],
				lapelWidth: [5, 9],
				lapelHeight: [4, 7],
				curveCleavageX: 1, //TODO
				
				invisible: 0,
				hi_invisible: 0,
			};
		},
		fill: exgar.bimbo_fill,
	},
	
//DRESS
	bimboDress :{
		name: "dress",
		
		dad: "wSuperDress",
		slot:"upperLower",
		dim(){
			return {
				legCoverage: [glimpseLegCoverage, miniSkirtLegCoverage],
				legLoose: [0, 0.5],
				
				sideLoose: 0,
				
				curveCleavageX: [5, 13],
				curveCleavageY: [-5, 0],
				
				cleavageOpeness: [0.25, 0.4],
				cleavageCoverage: [0.29,0.45],
				
				alpha: rg.g < 0.3 ? [exgar.min_transparency, 1] : 1,
				
				armCoverage: rg.g < 0.8 ? [0.12, 0.3] : 0,
				armLoose: 0,
				
		}},
		fill: exgar.bimbo_fill,
	},
	
	
	bimboMinidress :{
		name: "dress",
		
		dad: "wMiniDress",
		slot:"upperLower",
		dim(){
			return {
				showStrap: rg.g < 0.5,
				legCoverage: [glimpseLegCoverage, miniSkirtLegCoverage],
				legLoose: 0,
				alpha: rg.g < 0.3 ? [exgar.min_transparency, 1] : 1,
		}},
		fill: exgar.bimbo_fill,
	},
	
	bimboMinidress2 :{
		name: "dress",
		
		dad: "wMiniDress2",
		slot:"upperLower",
		dim(){
			const neck = rg.range(0.7, 1);
			return {
				innerNeckCoverage: neck,
				outerNeckCoverage: neck + rg.range(0.1, 0.5),
				cleavageCoverage: [0.2, 0.35],
				legCoverage: [glimpseLegCoverage, miniSkirtLegCoverage],
				legLoose: 0,
				//alpha: [exgar.min_transparency, 1]
		}},
		fill: exgar.bimbo_fill,
	},
	
	
	bimboMinidress3 :{
		name: "dress",
		
		dad: "wMiniDress3",
		slot:"upperLower",
		dim(){
			let armCoverage = 0;
			let armLoose = 0;
			const rand = rg.g;
			if(rand < 0.25){ //tight long
				armCoverage =  rg.range(0.35, 1);
			}else if(rand < 0.5){ //short wide
				armCoverage = rg.range(0.3, 0.5);
				armLoose = rg.range(0.2, 0.6);
			}else if(rand < 0.75){ //normal
				armCoverage = rg.range(0.35, 0.9);
				armLoose = rg.range(0.0, 0.2);
			}//none
			
			return {
				armCoverage,
				armLoose,
				chestCoverage: [0.25, 0.75],
				legCoverage: [glimpseLegCoverage, shortSkirtLegCoverage],
				legLoose: rg.g < 0.5 ? 0 : [0, 0.5],
				shoulderCoverage: [-0.05, -0.15],
				//alpha: rg.g < 0.5 ? [exgar.max_transparency, 1] : 1,
				
		}},
		fill: exgar.bimbo_fill,
	},
	
	
	
	
	
	
	
	
	
	
	
	
	
	blackBorderlineSkirt : {
		name: "microskirt",
		dad: "SuperSkirt",
		slot:"lower",
		note: "shorter than microSkirt, longer than nanoSkirt, always risk of panties",
		dim(){ return {
			button: false,
			innerLoose: 1.1,
			outerLoose: rg.g <0.4 ? [0, 0.8] : [0.9, 1.5],
			waistCoverage:[-0.1, 0.1],
			legCoverage: [glimpseLegCoverage,  groinLegCoverage],
			//legCoverage: [glimpseLegCoverage, microSkirtLegCoverage - 0.01],
		}},
		fill: "nearlyBlack",
	},
	
	
		
	blackCagePanties : {
		name: "g-string",
		subtype: "gString",
		slot: "panties",
		dad: "CagePanties",
		dim(){ 
			const top = rg.range(0.0, 0.35);
			return {
				waistCoverage: [0, 0.25],
				curveBotX: [-1, -2.5],
				horizontalCoverage: [0.35, 0.45],
				verticalCoverage: [-0.25, -0.2],
				thickness: [1, 2],
				genCoverage: [0.3, 0.7],
				topCoverage: [0, 0.25],
		}},
		fill: "nearlyBlack",
	},
	
	blackThong : {
		name: "thong",
		subtype: "thong",
		slot:"panties",
		dad: "SuperPanties",
		dim(){ 
			const top = rg.range(-0.025, 0.15);
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.1, top -0.2],
				genCoverage: [0.4,0.7],
				topCurve: [-4, -7],
				bow: rg.g < 0.3,
				ruffling: rg.g < 0.4,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
			}
		},
		fill: "nearlyBlack",
	},
	
	
	blackHipsters : {
		name: "hipster panties",
		subtype: "hipsters",
		slot:"panties",
		dad: "SuperPanties",
		note: "slightly sexier panties",
		dim(){
			const top = rg.range(0.1,0.2);
			const bow = rg.g < 0.33;
			return {
				thickness: [0.3, 0.4],
				waistCoverage: top,
				waistCoverageLower:[top - 0.2, top -0.4],
				genCoverage:1,
				topCurve: [-3, -6],
				bow,
				ruffling: rg.g < 0.55,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				topBandWidth: !bow && rg.g < 0.6 ? [1, 3] : 0,
			}
		},
		fill: "nearlyBlack",
		//fill: exgar.underwear_fill,
	},
	
	blackCageBra : {
		name: "cage bra",
		slot:"bra",
		dad: "CageBra",
		note: "blakc sexyCageBra",
		dim(){ 
			const fancy = rg.b;
			return {
				thickness: [0.4,0.6],
				neckCoverage: [0.45, 0.8],
				topStrapWidth: [0.9, 1.6],
				botStrapCoverage: rg.g < 0.65 ? [0.7, 1] : [1.25, 2],
				cageSpacing: [1.3, 2.2],
				
				cageCenter: fancy,
				cageCross: !fancy,
				cageDouble: rg.g < 0.66,
				ruffling: false,
				/*rg.g < 0.33,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				*/
			}
		},
		fill: "nearlyBlack",
		//fill: exgar.underwear_fill,
		//highlight: ["stroke", "stroke", "darkGrey"],
	},
	
	
	
	blackPleatedSkirt : {
		name: "pleated skirt",
		dad: "PleatedSkirt",
		slot:"lower",
		note: "bimboMicroSkirt length + pleatedMicroSkirt plates",
		dim(){ return {
			button: false,
			pleatedNumber: rg.integer(2,4),
			pleatedPointy: [2,7],
			innerLoose: 1.1,
			outerLoose:  [0.9, 1.5],
			waistCoverage:[-0.1, 0.1],
			legCoverage: [0.25, 0.35],
		}},
		fill: "nearlyBlack", //fill: exgar.bimbo_fill,
	},
	
	
	blackChoker : {
		name: "choker",
		slot:"collar",
		dad: "wChoker2",
		note: "choker using different DAD pattern",
		dim(){ 
				return {
					heart: false,
					radius: [1.3, 2.1],
				}; 
		},
		fill: "nearlyBlack", 
		highlight: "util_metal_fill",
	},
	
	bimboChoker : {
		name: "choker",
		slot:"collar",
		dad: "wChoker2",
		note: "choker using different DAD pattern",
		dim(){ 
				return {
					heart: true,
					radius: [1.3, 2.1],
					//thickness: between(0.4,0.7),
				}; 
		},
		fill: exgar.bimbo_fill,
	},
	
	bimboCollar : {
		name: "collar",
		slot:"collar",
		dad: "Collar",
		dim(){ 
				return {
					radius: 3, 
				}; 
		},
		fill: exgar.bimbo_fill,
		//fill: "darkLeather", //TODO 
		//highlight: "monochromatic",
	},
	
	
	
}