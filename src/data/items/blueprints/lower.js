import {exgar} from "./exgar";
import {
	glimpseLegCoverage, //0.135
	groinLegCoverage, //0.16
	microSkirtLegCoverage, //0.25
	miniSkirtLegCoverage, //0.35
	shortSkirtLegCoverage, //0.5
} from "Avatar/wardrobe/constants";
import {rg} from "Libraries/random";

//TODO - beltWidth should be included everywhere (but breaks legacy items saved from previous versions)
export const lower = {
//SKIRTS 
	longSkirt : {
		name: "long skirt",
		dad: "SuperSkirt",
		slot:"lower",
		note: "below knees",
		dim(){ 
			const button = rg.g < 0.25;
			return {
				button,
				beltWidth: button ? 2 : [1.5, 10],
				bustle: rg.g < 0.25,
				innerLoose: 1.1,
				outerLoose: [1, 1.5],
				waistCoverage: [0.45,0.8],
				legCoverage: [shortSkirtLegCoverage + 0.01, 0.8],
			}
		},
		fill: exgar.lower_fill,
	}, 
	
	pencilSkirt : {
		name: "pencil skirt",
		dad: "SuperSkirt",
		slot:"lower",
		note: "tight medium to miniskirt",
		dim(){ return {
			button: rg.g < 0.25,
			innerLoose: 1.1,
			outerLoose: 0,
			waistCoverage: [0.2,0.6],
			legCoverage: [0.25,0.55],
		}},
		fill: [...exgar.lower_fill, "darkGrey"],
	},
	
	shortSkirt : {
		name: "skirt",
		dad: "SuperSkirt",
		slot:"lower",
		note: "little above knees",
		dim(){ return {
			button: rg.g < 0.25,
			bustle: rg.g < 0.25,
			innerLoose: 1.1,
			outerLoose: rg.g < 0.4 ? [0, 0.8] : [0.9, 1.6],
			waistCoverage: [0.05,0.45],
			legCoverage: [miniSkirtLegCoverage + 0.01, shortSkirtLegCoverage - 0.01],
		}},
		fill: exgar.lower_fill,
	}, 
	
	miniSkirt : {
		name: "miniskirt",
		dad: "SuperSkirt",
		slot:"lower",
		note: "mid-thigh",
		dim(){ return {
			button: rg.g < 0.3,
			innerLoose: 1.1,
			outerLoose: rg.g < 0.4 ? [0, 0.8] : [0.9, 1.6],
			waistCoverage: [0.0, 0.15], //TODO - could be -0.05 or 0.2
			legCoverage: [microSkirtLegCoverage + 0.01, miniSkirtLegCoverage - 0.01],
		}},
		fill: exgar.lower_fill,
	},
	
	microSkirt : {
		name: "microskirt",
		dad: "SuperSkirt",
		slot:"lower",
		note: "upper-thigh",
		dim(){ return {
			button: rg.g < 0.3,
			innerLoose: 1.1,
			outerLoose: rg.g <0.4 ? [0, 0.8] : [0.9, 1.5],
			waistCoverage:[-0.1, 0.1],
			legCoverage: [glimpseLegCoverage, microSkirtLegCoverage - 0.01],
		}},
		fill: exgar.lower_fill,
	},
	
	nanoSkirt : {
		name: "nanoskirt",
		dad: "SuperSkirt",
		slot:"lower",
		note: "shows panties",
		dim(){ return {
			button: rg.g < 0.3,
			innerLoose: 1.1,
			outerLoose: rg.g <0.4 ? [0, 0.8] : [0.9, 1.5],
			waistCoverage:[-0.1, 0.05],
			legCoverage: [0.11, groinLegCoverage],
		}},
		fill: exgar.lower_fill,
	},
	
	
	pleatedShortSkirt : {
		name: "pleated skirt",
		dad: "PleatedSkirt",
		slot:"lower",
		note: "pleated, little above knees",
		dim(){ return {
			pleatedNumber: rg.integer(2,4),
			pleatedPointy: [2,7],
			innerLoose: 1.1,
			outerLoose: [1.2, 1.6],
			waistCoverage: [0.05,0.4],
			legCoverage: [miniSkirtLegCoverage + 0.01, shortSkirtLegCoverage - 0.01],
		}},
		fill: exgar.lower_fill,
		highlight: ["stroke"], //TODO
	}, 
	
	pleatedMiniSkirt : {
		name: "pleated skirt",
		dad: "PleatedSkirt",
		slot:"lower",
		note: "pleated, mid-thigh",
		dim(){ return {
			pleatedNumber: rg.integer(2,4),
			pleatedPointy: [2,7],
			innerLoose: 1.1,
			outerLoose: [1.2, 1.6],
			waistCoverage: [-0.05, 0.15],
			legCoverage: [microSkirtLegCoverage + 0.01, miniSkirtLegCoverage - 0.01],
		}},
		fill: exgar.lower_fill,
		highlight: ["stroke"], //TODO
	}, 
	
	pleatedMicroSkirt : {
		name: "pleated skirt",
		dad: "PleatedSkirt",
		slot:"lower",
		note: "pleated, upper-thigh",
		dim(){ return {
			pleatedNumber: rg.integer(2,4),
			pleatedPointy: [2,7],
			innerLoose: 1.1,
			outerLoose: [1.2, 1.6],
			waistCoverage:[-0.1, 0.05],
			legCoverage: [glimpseLegCoverage, microSkirtLegCoverage - 0.01],
		}},
		fill: exgar.lower_fill,
		highlight: ["stroke"], //TODO
	}, 
	
	animalMicroSkirt : {
		name: "microskirt",
		dad: "SuperSkirt",
		slot:"lower",
		note: "upper-thigh",
		dim(){ 
			const button = rg.g < 0.3;
			return {
				button,
				beltWidth: button ? 2 : [1, 6], 
				innerLoose: 1.1,
				outerLoose: rg.g <0.4 ? [0, 0.8] : [0.9, 1.5],
				waistCoverage:[-0.1, 0.05],
				legCoverage: [glimpseLegCoverage, microSkirtLegCoverage - 0.01],
			}
		},
		texture: exgar.lower_tex,
		highlight: "nearlyBlack",
	},
	
//PANTS
	jeans : {
		name: "jeans",
		dad: "Jeans",
		slot:"lower",
		dim(){
			const degree = rg.g;
			return {
				waistCoverage: [0.1, 0.3], 
				legCoverage: [0.9, 1.0],
				innerLoose: 0.1 + 0.4 * degree,
				outerLoose: 0.2 + 0.7 * degree,
				open: 0,
				buttonColor: "orange",
				highlight: "orange", //TODO
				thickness: [0.9, 1],
		}},
		fill: "jeans",
	}, 
	
	tightJeans : {
		name: "jeans",
		dad: "Jeans",
		slot:"lower",
		dim(){
			return {
				waistCoverage: [0.0, 0.15],
				legCoverage: [0.9, 1.0],
				innerLoose: 0,
				outerLoose: 0,
				open: 0,
				buttonColor: "orange", //TODO
				highlight: "orange",
				thickness: [0.9, 1],
		}},
		fill: "jeans",
	}, 
	
	sluttyJeans : {
		name: "jeans",
		dad: "wJeans",
		slot:"lower",
		dim(){
			return {
				waistCoverage: [-0.05, 0.1],
				legCoverage: [0.9, 1.0],
				innerLoose: 0,
				outerLoose: 0,
				open: [3, 6],
				buttonColor: "orange", //TODO
				highlight: "orange",
				thickness: [0.9, 1],
		}},
		fill: "jeans",
	}, 
	
	pants : {
		name: "pants",
		dad: "SuperPants",
		slot:"lower",
		dim(){
			const degree = rg.g;
			return {
				waistCoverage: [0.0, 0.3],
				legCoverage: [0.9, 1.00],
				innerLoose: 0.1 + 0.4 * degree,
				outerLoose: 0.2 + 0.7 * degree,
				beltWidth: [1, 5],
				thickness: 0.75,
		}},
		fill: exgar.dark_lower_fill,
		highlight: ["stroke", "monochromatic","darkGrey", "contrast"],
	}, 
	
	leggings : {
		name: "leggings",
		dad: "SuperLeggings",
		slot:"lower",
		dim(){
			return {
				waistCoverage: [-0.08, 0.24],
				legCoverage: [0.85, 1.00],
				innerLoose: 0,
				outerLoose: 0,
				thickness: [0.5, 0.8],
				beltWidth: rg.g < 0.34 ? 0 : [3, 7],
				lampasse: 0,
				lampasseOut: 0,
		}},
		fill: exgar.lower_fill,
		highlight: ["monochromatic","darkGrey"],
	}, 
	
	
	gymLeggings : {
		name: "leggings",
		dad: "SuperLeggings",
		slot:"lower",
		note: "leggings with lampasses",
		dim(){
			return {
				waistCoverage: [-0.08, 0.24],
				legCoverage: [0.85, 1.00],
				innerLoose: 0,
				outerLoose: 0,
				thickness: [0.7, 0.8],
				beltWidth: rg.g < 0.4 ? 0 : [3, 7],
				lampasse: [2, 5],
				lampasseOut: rg.g < 0.33 ? 0 : [1, 2],
				
		}},
		fill: exgar.lower_fill,
		highlight: ["monochromatic","grey","contrast"],
	}, 
	
	animalLeggings : {
		name: "leggings",
		dad: "SuperLeggings",
		slot:"lower",
		dim(){
			return {
				waistCoverage: [-0.08, 0.24],
				legCoverage: [0.85, 1.00],
				innerLoose: 0,
				outerLoose: 0,
				thickness: [0.7, 0.8],
				beltWidth: rg.g < 0.34 ? 0 : [0, 4],
				lampasse: 0,
		}},
		texture: exgar.lower_tex,
		highlight: "nearlyBlack",
	}, 
	
	jeansShorts : {
		name: "shorts",
		dad: "wJeans",
		slot:"lower",
		dim(){
			return {
				waistCoverage: [0.0, 0.2],
				legCoverage: [0.14, 0.27],
				innerLoose: 0,
				outerLoose: 0,
				open: rg.g < 0.5 ? 0 : 0, // [3, 6]
				buttonColor: "orange",
				highlight: "orange",
				thickness: [0.7, 0.9],
		}},
		fill: "jeans",
	}, 
	
	sexyJeansShorts : {
		name: "hotpants",
		dad: "wJeans",
		slot:"lower",
		dim(){
			const daisyDukes = rg.g < 0.5;
			return {
				waistCoverage: daisyDukes ? [0.42, 0.54] : [0.01, 0.1],
				legCoverage: daisyDukes ? [0.03, 0.06] : [0.06, 0.12],
				outerLoose: 0,
				open: rg.g < 0.6 ? [5, 8] : 0,
				buttonColor: "orange",
				thickness: [0.7, 0.9],
				innerLoose: rg.g < 0.6 ? 0.5 : 0,
		}},
		fill: "jeans",
		highlight: "red",
	}, 
	
	shorts : {
		name: "shorts",
		dad: "wSuperLeggins",
		slot:"lower",
		dim(){
			const fancy = rg.g;
			const degree = rg.g;
			return {
				waistCoverage: [-0.1, 0.2],
				legCoverage: [0.14, 0.27],
				innerLoose: 0,
				outerLoose: 0,
				thickness: [0.7, 0.8],
				beltWidth: fancy < 0.40 ? [2, 6] : 0,
				lampasse: (fancy > 0.80 || fancy < 0.2) ? [2, 5] : 0,
				lampasseOut: ( (fancy > 0.80 || fancy < 0.2) && rg.g > 0.66) ? 0 : 2,
				
		}},
		fill: exgar.dark_lower_fill,
		highlight: ["stroke", "monochromatic","darkGrey"],
	}, 
	
	sexyShorts : {
		name: "hotpants",
		dad: "wSuperLeggins",
		slot:"lower",
		dim(){
			const fancy = rg.g;
			const degree = rg.g;
			return {
				waistCoverage: [-0.1, 0.1],
				legCoverage: [0.05, 0.15],
				innerLoose: 0,
				outerLoose: 0,
				thickness: [0.7, 0.8],
				beltWidth: fancy < 0.30 ? [3, 7] : 0,
				lampasse: fancy > 0.70 ? [2, 5] : 0,
				lampasseOut: (fancy > 0.66 && rg.g > 0.66) ? 0 : 2,
		}},
		fill: exgar.dark_lower_fill,
		highlight: ["stroke", "monochromatic","darkGrey"],
	}, 
	
	/*
	sexyShortsTexture : {
		name: "hotpants",
		dad: "wSuperLeggins",
		slot:"lower",
		dim(){
			const degree = rg.g;
			return {
				waistCoverage: [-0.1, 0.1],
				legCoverage: [0.05, 0.15],
				innerLoose: 0,
				outerLoose: 0,
				thickness: [0.7, 0.8],
		}},
		texture: [],
	}, 
	*/
	
//FANTASY SKIRTS
	defHarlotSkirt : {
		name: "short skirt",
		dad: "wSuperSkirt", 
		slot:"lower",
		note: "default for harlot",
		dim(){ return{
			innerLoose: 1.2,
			legCoverage: 0.23,
			outerLoose: 1.2,
			waistCoverage: 0.33,
		}},
		fill: exgar.lower_fill,
	}, 
	
	harlotSkirt : {
		name: "short skirt",
		dad: "wSuperSkirt", 
		slot:"lower",
		note: "mini & micro skirt",
		dim(){ return{
			innerLoose: 1.2,
			legCoverage: [0.16,0.32],
			outerLoose: [0.14,0.8],
			waistCoverage: [-0.1,0.5],
		}},
		fill: exgar.lower_fill,
	}, 
	
	maidSkirt : {
		name: "skirt",
		dad: "wSuperSkirt", 
		slot:"lower",
		note: "knees and above",
		dim(){ return{
			innerLoose: 1.2,
			legCoverage: [0.3, 0.5],
			outerLoose: [0.5,1.5],
			waistCoverage: [-0.1,0.5],
		}},
		fill: exgar.lower_fill,
	}, 
	
	haremSkirt : {
		name: "skirt",
		dad: "wSuperSkirt",
		slot:"lower",
		note: "transparent",
		dim(){ return {
			button: false,
			bustle: false,
			innerLoose: 1.1,
			outerLoose: [1.2, 1.6],
			waistCoverage: [0.1, 0.2],
			legCoverage: [0.45, 0.5],
			alpha: [exgar.min_transparency, exgar.max_transparency],
		}},
		fill: exgar.lower_fill,
	}, 
	
	
//FANTASY PANTS
	fLeggings : {
		name: "leggings",
		//dad: "wSuperLeggins",
		dad: "wLacedLeggins",
		slot:"lower",
		dim(){return {
			waistCoverage: [-0.05, 0.3],  
			legCoverage: [0.8, 1.0],
			zipDeep: [0.16, 0.24],
			
			lampasse: 0,
			
			innerLoose: 0,
			outerLoose: 0,
		}},
		fill: ["leather","darkLeather"],
	}, 
	
	fFancyLeggings : {
		name: "leggings",
		//dad: "wSuperLeggins",
		dad: "wLacedLeggins",
		slot:"lower",
		dim(){return {
			waistCoverage: [-0.05, 0.3],  
			legCoverage: [0.8, 1.0],
			zipDeep: [0.16, 0.24],
			
			lampasse: 0,
			
			innerLoose: 0,
			outerLoose: 0,
		}},
		material: exgar.rare_leather,
	}, 
	
	fPants : {
		name: "pants",
		//dad: "wSuperLeggins",
		dad: "wLacedLeggins",
		slot:"lower",
		dim(){return {
			waistCoverage: [-0.05, 0.3],  
			legCoverage: [0.8, 1.0],
			zipDeep: [0.16,0.24],
			
			lampasse: 0,
			
			innerLoose: [0.3, 0.6],
			outerLoose: [0.3, 0.6],
		}},
		fill: ["leather","darkLeather"],
	}, 
	
	
//FANTASY LOINCLOTHS
	defBarbaricLoincloth : {
		name: "loincloth",
		dad: "wLoincloth",
		slot: "lowerOuter",
		subtype: "loincloth",
		dim(){ return{
			beltWith: 3,
			waistCoverage: 0.3,
			topCoverage: 0.99,
			//fill: "hsla(33, 45%, 35%, 1)",
			//highlight: "hsla(33, 80%, 10%, 1)",
		}},
		fill:"leather",
	}, 
	
	barbaricLoincloth : {
		name: "loincloth",
		dad: "wLoincloth",
		slot: "lowerOuter",
		subtype: "loincloth",
		dim(){ 
			const top = rg.range(0.2,0.9);
			const bot = rg.range(0.1,0.2)
			const xCurve = (-16)*(top-bot)-5
			return{
			beltWith: [1,6],
			waistCoverage: [-0.07,0.56],
			legCoverage: [0.35,0.6],
			topCoverage: top,
			bottomCoverage: bot, 
			curveX: xCurve,
		}},
		fill: exgar.leather_fill,
	}, 
	
	fancyBarbaricLoincloth : {
		name: "loincloth",
		dad: "wLoincloth",
		slot: "lowerOuter",
		subtype: "loincloth",
		dim(){ 
			const top = rg.range(0.2,0.9);
			const bot = rg.range(0.1,0.2)
			const xCurve = (-16)*(top-bot)-5
			return{
			beltWith: [1,6],
			waistCoverage: [-0.07,0.55],
			legCoverage: [0.35,0.6],
			topCoverage: top,
			bottomCoverage: bot, 
			curveX: xCurve,
		}},
		material: exgar.rare_leather,
	}, 
	
	defFallenLoincloth : {
		name: "skirt",
		dad: "wLoincloth",
		slot: "lower",
		subtype: "loincloth",
		dim(){ return{
			beltWidth: 6,
			bottomCoverage: 0.2,
			curveX: -18,
			legCoverage: 0.74,
			topCoverage: 0.8,
			waistCoverage: 0.3,
		}},
		fill: "purple",
		highlight: "darkLeather",
	},
	
	fallenLoincloth : {
		name: "skirt",
		dad: "wLoincloth",
		slot: "lower",
		subtype: "loincloth",
		dim(){ return{
			beltWidth: 6,
			bottomCoverage: 0.2,
			curveX: -18,
			legCoverage: [0.45,0.8],
			topCoverage: 0.5,
			waistCoverage: 0.3,
		}},
		fill: exgar.fallen_fill,
		highlight: "darkLeather",
	},
	
	
	clothLoincloth : {
		name: "loincloth",
		dad: "wLoincloth",
		slot: "lower",
		subtype: "loincloth",
		dim(){ return{
			beltWith: 1,
			waistCoverage: 0.3, //TODO
			topCoverage: 0.99,  //TODO
		}},
		material: exgar.bland_cloth,
	}, 

}