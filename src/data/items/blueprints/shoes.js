import {exgar} from "./exgar";
import {rg} from "Libraries/random";



export const shoes = {
//SNEAKERS
	blackSneakers :{
		name: "sneakers",
		dad: "wSimpleSneakers",
		slot: "shoes",
		note: "dark colors, white shoelace",
		dim(){return {
			tongue: [4, 7],
			tip: [-4,1],
		}},
		fill: ["dark","dark","darkGrey","nearlyBlack"],
		highlight: "dirtyWhite", //TODO WHITE
	},
	
	orangeSneakers :{
		name: "sneakers",
		dad: "wSimpleSneakers",
		slot: "shoes",
		note: "dark colors, cool shoelace",
		dim(){return {
			tongue: [4, 7],
			tip: [-4,1],
		}},
		fill: ["darkGrey","nearlyBlack"],
		highlight: "bright",
	},
	
	whiteSneakers :{
		name: "sneakers",
		dad: "wSimpleSneakers",
		slot: "shoes",
		note: "light colors, dark shoelace",
		dim(){return {
			tongue: [4, 7],
			tip: [-4,1],
		}},
		fill: ["light","dirtyWhite","cute"],
		highlight: ["nearlyBlack","darkGrey","dark"],
	},
	
	
//HEELS
	kittyHeels :{
		name: "heels",
		dad: "wClosedToeStrappedPumps",
		slot: "shoes",
		subtype: "heels",
		note: "low heels",
		dim(){return {
			straps: rg.array([0,0,1,1,2,2,3]),
			space: [3, 6],
			shoeHeight: [0.2, 1.19],
			toeCoverage: [0, 1],
			shoeTightness: [8, 12],
		}},
		fill: ["full","dark","full","dark","darkGrey"],
	},
	
	highHeels :{
		name: "high heels",
		dad: "wClosedToeStrappedPumps",
		slot: "shoes",
		subtype: "heels",
		dim(){return {
			straps: rg.array([0,0,1,1,1,2,2,3,3,4,5,6,7]),
			space: [3, 6],
			shoeHeight: [1.2, 4],
			toeCoverage: [0, 1],
			shoeTightness: [8, 12],
		}},
		fill: ["full","dark","full","dark","darkGrey"],
	},
	
	
//BOOTS
	orangeBoots :{
		name: "boots",
		dad: "wHighSneakers",
		slot: "shoes",
		subtype: "boots",
		dim(){return {
			tongue: [4, 7],
			tip: [-4,1],
			legCoverage: [0.69, 0.88],
		}},
		fill: ["darkGrey","nearlyBlack"],
		highlight: "bright",
	},
	
	/*
	fxBoots :{
		name: "boots",
		dad: "wHighSneakers",
		slot: "shoes",
		dim: (function(){return {
			tongue: [4, 7],
			tip: [-4,1],
			legCoverage: [0.69, 0.88],
		}}),
		fill:"full",
		highlight: "monochromatic",
	},
	*/
	
	darkBoots :{
		name: "boots",
		dad: "wHighSneakers",
		slot: "shoes",
		subtype: "boots",
		dim(){return {
			tongue: [4, 7],
			tip: [-4,1],
			legCoverage: [0.69, 0.88],
		}},
		fill: ["darkGrey","nearlyBlack","nearlyBlack","darkLeather","leather"],
	},
	
	polyBoots :{
		name: "boots",
		dad: "wHighSneakers",
		slot: "shoes",
		subtype: "boots",
		dim(){return {
			tongue: [4, 7],
			tip: [-4,1],
			legCoverage: [0.69, 0.88],
		}},
		fill: ["dark"],
	},
	
	
	darkHighBoots :{
		name: "boots",
		dad: "wHighSneakers",
		slot: "shoes",
		subtype: "boots",
		dim(){return {
			tongue: [4, 7],
			tip: [-4,1],
			legCoverage: [0.36, 0.8],
		}},
		fill: ["darkGrey","nearlyBlack","darkLeather","leather"],
	},
	
	polyHighBoots :{
		name: "boots",
		dad: "wHighSneakers",
		slot: "shoes",
		subtype: "boots",
		dim(){return {
			tongue: [4, 7],
			tip: [-4,1],
			legCoverage: [0.36, 0.8],
		}},
		fill: ["dark"],
	},
	
	/*
	
	BimboHeels :{
		name: "high heels",
		dad: "wClosedToeStrappedPumps",
		slot: "shoes",
		dim: (function(){return {
			shoeHeight: WARDROBE.between(3,5),
			toeCoverage: WARDROBE.between(0,1),
			shoeTightness: WARDROBE.between(8,12),
		}}),
		//size: 3,
		fill: ["barbie","barbie","deepRed"],
	},
	
	
	RidiculousHeels : {
		name: "high heels",
		slot: "shoes",
		dad: "ClosedToeStrappedPumps",
		dim: (function(){ return {
			basePointiness: 7.0000000000000036,
			heelPointiness: 3.8971429878757338,
			heelTipWidth: 5.679626594804873,
			platformHeight: -0.6334225585191575,
			shoeHeight: 6.20571402424854,
			shoeTightness: 4,
			strapWidth: 2.7861457328646893,
			toeCoverage: -1,
		}}),	
		fill: ["purple","carmine"]
	},
	
	
	*/
	

//FANTASY
	
	
	
	
	
	
}