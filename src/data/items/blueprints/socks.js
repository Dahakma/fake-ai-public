import {exgar, rg} from "./exgar";
import {stockingsLegCoverage} from "Avatar/wardrobe/constants";

export const socks = {
	
//SOCKS
	socks : {
		name: "socks",
		subtype: "socks",
		dad: "wSuperSocks",
		slot:"socks",
		dim(){ return{
			legCoverage:[0.8,1],
			thickness: [0.5,1],
		}},
		fill: exgar.socks_fill,
	},

	neutralSocks : {
		name: "socks",
		subtype: "socks",
		dad: "wSuperSocks",
		slot:"socks",
		note: "socks with neutral non-girly color (no pink etc.)",
		dim(){ return{
			legCoverage:[0.94,1],
			thickness: [0.5,1],
		}},
		fill: exgar.neutral_socks_fill,
	},

	
	
	
//HIGH SOCKS
	lightHighSocks : {
		name: "high socks",
		subtype: "highSocks",
		dad: "wSuperSocks",
		slot:"socks",
		note: "light colors",
		dim(){ return{
			legCoverage: [0.6, stockingsLegCoverage + 0.01],
			thickness: [0.8, 1],
		}},
		fill: "light",
	}, 
	
	brightHighSocks : {
		name: "high socks",
		subtype: "highSocks",
		dad: "wSuperSocks",
		slot:"socks",
		note: "saturated colors",
		dim(){ return{
			legCoverage: [0.6, stockingsLegCoverage + 0.01],
			thickness: [0.8, 1],
		}},
		fill: "bright",
	}, 
	
	darkHighSocks : {
		name: "high socks",
		subtype: "highSocks",
		dad: "wSuperSocks",
		slot:"socks",
		note: "dark colors or black",
		dim(){ return{
			legCoverage: [0.6, stockingsLegCoverage + 0.01],
			thickness: [0.8, 1],
		}},
		fill: exgar.dark_fill,
	}, 	

	stripedHighSocks : {
		name: "high socks",
		adjective: "striped",
		subtype: "highSocks",
		dad: "wSuperSocks",
		slot:"socks",
		note: "stripped pattern",
		dim(){ return{
			legCoverage: [0.6, stockingsLegCoverage + 0.01],
			thickness: [0.8, 1],
		}},
		//fill: "bimbo",
		//fishnetTexture: exgar.stockings_fishnet,
		texture: exgar.striped_bright,
	}, 
	
	
	stripedDarkHighSocks : {
		name: "high socks",
		adjective: "striped",
		subtype: "highSocks",
		dad: "SuperSocks",
		slot:"socks",
		note: "stripped pattern",
		dim(){ return{
			legCoverage: [0.6, stockingsLegCoverage + 0.01],
			thickness: [0.8, 1],
		}},
		texture: exgar.striped_dark,
	}, 
	
	
	
//STOCKINGS
	stockings : {
		name: "stockings",
		subtype: "stockings",
		slot:"socks",
		dad: "wStockings",
		note: "nylon, black or grey",
		dim(){ return {
			bandWidth: [5,10],
			legCoverage: [0.24,  stockingsLegCoverage - 0.01],
			alpha: [0.4, 0.9],
			thickness: [0.3, 0.5],
		}},
		fill: ["nylon", "nearlyBlack", "darkGrey", "grey", "nearlyWhite"],
	},
	
	polyStockings : {
		name: "stockings",
		subtype: "stockings",
		slot:"socks",
		dad: "wStockings",
		note: "colorful",
		dim(){ return {
			bandWidth: [5,10],
			legCoverage: [0.24, stockingsLegCoverage - 0.01],
			alpha: [0.4, 0.9],
			thickness: [0.3, 0.5],
		}},
		fill: ["bright","dark","full"],
	},
/*	
	polyPatStockings : {
		name: "stockings",
		subtype: "stockings",
		slot:"socks",
		dad: "wStockings",
		note: "colorful",
		dim(){ return {
			fishness: 0.5,
			bandWidth: [5,10],
			legCoverage: [0.24, stockingsLegCoverage - 0.01],
			alpha: [0.4, 0.9],
			thickness: [0.3, 0.5],
		}},
		fill: ["bright","dark","full"],
	},
*/	
	fishnetStockings : {
		name: "stockings",
		subtype: "stockings",
		adjective: "fishnet",
		dad: "wStockings",
		slot:"socks",
		dim(){ return{
			bandWidth: [5, 10],
			legCoverage: [0.24, stockingsLegCoverage - 0.01],
			thickness: 0.2,
		}},
		texture: exgar.stockings_fishnet,
		highlight: "nearlyBlack",
	},

	semiFishnetStockings : {
		name: "stockings",
		subtype: "stockings",
		dad: "wStockings",
		slot:"socks",
		dim(){ return{
			bandWidth: [5, 10],
			legCoverage: [0.24, stockingsLegCoverage - 0.01],
			thickness: [0.2, 0.4],
			fishness: [0.3, 0.5],
		}},
		fill: ["nearlyBlack", "tintedBlack", "darkGrey"],
		fishnetTexture: exgar.stockings_fishnet,
	},
	
	semiFishnetPolyStockings : {
		name: "stockings",
		subtype: "stockings",
		slot:"socks",
		dad: "wStockings",
		dim(){ return {
			bandWidth: [5,10],
			legCoverage: [0.24, stockingsLegCoverage - 0.01],
			thickness: [0.2, 0.4],
			fishness: [0.3, 0.5],
		}},
		fill: ["bright","dark","full"],
		fishnetTexture: exgar.stockings_fishnet,
	},
	
//
	garterStockings : {
		name: "stockings",
		subtype: "stockings",
		dad: "StockingsGarter",
		slot:"socks",
		note: "stockings with garter belt",
		dim(){ return{
			bandWidth: [3, 9],
			beltWidth: [6, 10],
			legCoverage: [0.17, 0.25],
			thickness: 0.2,
			waistCoverage: [0.2, 0.34],	
			alpha: [0.7, 0.9],
		}},
		//fill: ["nylon","nylon","darkBlack","darkGrey"],
		fill: ["nylon", "nearlyBlack", "darkGrey", "grey", "nearlyWhite"],
	}, 
	
	polyGarterStockings : {
		name: "stockings",
		subtype: "stockings",
		dad: "StockingsGarter",
		slot:"socks",
		note: "stockings with garter belt",
		dim(){ return{
			bandWidth: [3, 9],
			beltWidth: [6, 10],
			legCoverage: [0.17, 0.25],
			thickness: 0.2,
			waistCoverage: [0.2, 0.34],	
			alpha: [0.7, 0.9],
		}},
		fill: ["dark","full","bright"],
	},
	
	fishnetGarterStockings : {
		name: "stockings",
		subtype: "stockings",
		dad: "StockingsGarter",
		slot:"socks",
		note: "stockings with garter belt",
		dim(){ return{
			bandWidth: [3, 9],
			beltWidth: [6, 10],
			legCoverage: [0.17, 0.25],
			thickness: 0.2,
			waistCoverage: [0.2, 0.34],	
			//alpha: [0.7, 0.9],
		}},
		texture: exgar.stockings_fishnet,
		//fill: ["nylon","nylon","darkBlack","darkGrey"],
	}, 
	
//PANTYHOSE
	lightTights : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		note: "thick, light colors",
		dim(){ return {
			//fishness: 0,
			waistCoverage: [0.2,0.6],
			alpha: 1,	
			thickness: [0.7, 0.9],
			openWidth: 0,
		}},
		fill: "light",
	},	
	
	brightTights : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		note: "thick, saturated colors",
		dim(){ return {
			//fishness: 0,
			waistCoverage: [0.2,0.6],
			alpha: 1,	
			thickness: [0.7, 0.9],
			openWidth: 0,
		}},
		fill: "bright", // exgar.light_fill,//socks_fill,
	},
	
	darkTights : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		note: "thick, dark colors or black",
		dim(){ return {
			//fishness: 0,
			waistCoverage: [0.2,0.6],
			alpha: 1,	
			thickness: [0.7, 0.9],
			openWidth: 0,
		}},
		fill: exgar.dark_fill, //neutral_socks_fill,
	},	
	
	pantyhose : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		note: "thin, nylony",
		dim(){ return {
			fishness: 0,
			waistCoverage: [0.2,0.6],
			alpha: [0.7,0.9],	
			thickness: [0.4, 0.4],
			openWidth: 0,
		}},
		fill: ["nearlyBlack","darkGrey","grey"],
	},
	
	polyPantyhose : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		note: "thin, colorful",
		dim(){ return {
			fishness: 0,
			waistCoverage: [0.2,0.6],
			alpha: [0.4,0.9],	
			thickness: [0.25, 0.35],
			openWidth: 0,
		}},
		fill: ["dark","full","bright"],
	},
	
	
	nylonPantyhose : {
		name: "pantyhose",
		adjective: "nylon",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		note: "thin, nylon",
		dim(){ return {
			fishness: 0,
			waistCoverage: [0.2,0.6],
			alpha: [0.4,0.8],	
			thickness: [0.25, 0.35],
			openWidth: 0,
		}},
		fill: "nylon",
	},
	

	
//SLUTY PANTYHOSE 
	
	
	openLightTights : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		note: "light color, crotch open",
		dim(){ return {
			//fishness: 0,
			waistCoverage: [0.2,0.4],
			alpha: [0.7, 0.9],	
			thickness: [0.5, 0.7],
			openWidth: [0.9, 1],
			openHeight: [0.7, 0.9],
			openSide: true,
		}},
		fill: exgar.light_fill, //socks_fill,
	},	
	
	openDarkTights : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		note: "dark color, crotch open",
		dim(){ return {
			//fishness: 0,
			waistCoverage: [0.2,0.4],
			alpha: [0.7, 0.9],		
			thickness: [0.5, 0.7],
			openWidth: [0.9, 1],
			openHeight: [0.7, 0.9],
			openSide: true,
		}},
		fill: exgar.dark_fill, //neutral_socks_fill,
	},	
	
	
	fishnetPantyhose : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		adjective: "fishnet",
		dim(){ return {
			fishness: 0,
			waistCoverage: [0.3,0.6],
			alpha: [0.4,0.9],	
			thickness: [0.25, 0.35],
			openWidth: 0,
			//TODO HEIGHT
		}},
		texture: exgar.stockings_fishnet,
	},
	
	semiFishnetPantyhose : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		dim(){ return {
			waistCoverage: [0.2,0.6],
			fishness: [0.1,0.4],	
			thickness: [0.4, 0.4],
			openWidth: 0,
		}},
		fill: ["nearlyBlack","darkGrey","tintedBlack"],
		fishnetTexture: exgar.stockings_fishnet,
	},
	
	openFishnetPantyhose : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		note: "fishnet, open crotch",
		adjective: "fishnet",
		dim(){ return {
			//fishness: 0,
			waistCoverage: [0.2,0.4],
			alpha: 1,	
			thickness: [0.5, 0.7],
			openWidth: [0.9, 1],
			openHeight: [0.7, 0.9],
			openSide: true,
		}},
		texture: exgar.stockings_fishnet,
	},
	
	crotchOpenFishnetPantyhose : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		note: "fishnet, open crotch",
		adjective: "fishnet",
		dim(){ return {
			//fishness: 0,
			waistCoverage: [0.2,0.4],
			alpha: 1,	
			thickness: [0.5, 0.7],
			openWidth: [0.9, 1],
			openHeight: [0.7, 0.9],
			openSide: false,
		}},
		texture: exgar.stockings_fishnet,
	},
	
	/*
	whorePantyhose : {
		name: "pantyhose",
		subtype: "pantyhose",
		dad: "wPantyhose",  
		slot:"socks",
		id: 274,
		adjective: "fishnet",
		note: "????",
		dim(){ return {
			fishness: 0,
			waistCoverage: [0.2,0.6],
			alpha: [0.4,0.9],	
			thickness: [0.25, 0.35],
			openWidth: 1.25,
			//TODO HEIGHT
		}},
		texture: exgar.stockings_fishnet,
	},
	*/
	
	
	
//FANTASY
	defHarlotStockings : {
		name: "stockings",
		dad: "wStockingsGarter",
		slot:"socks",
		note: "default for harlot",
		dim(){ return{
			bandWidth: 5,
			beltWidth: 8,
			legCoverage: 0.25,
			thickness: 0.2,
			waistCoverage: 0.27,	
		}},
		fill: "gray",
	}, 

	harlotStockings : {
		name: "stockings",
		id: 3132,
		dad: "wStockingsGarter",
		slot:"socks",
		dim(){ return{
			bandWidth: [3, 9],
			beltWidth: [6, 10],
			legCoverage: [0.05, 0.25],
			thickness: 0.2,
			waistCoverage: [0.2, 0.34],	
		}},
		fill: exgar.dark_fill,
	}, 
	

}




