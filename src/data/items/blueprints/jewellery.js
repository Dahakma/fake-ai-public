import {exgar} from "./exgar";
import {rg} from "Libraries/random";

export const jewellery = {
	
	//EARRINGS
	ballEarrings : {
		name: "ball earrings",
		slot:"earrings",
		dad: "BallEarrings",
		dim(){ 
			return {
				zoom: "face",
				size:rg.range(0.8,1.2),
				length:rg.range(8,16),
				alt:rg.range(-0.4,0.4),
			}; 
		},
		stroke: ["NXgold","NXsilver","NXbronze","NXsteel"],
		fill:["NXgold","NXsilver","NXbronze","NXsteel","bright","bright","bright"],
	},
	
	crystalEarrings : {
		name: "crystal earrings",
		slot:"earrings",
		dad: "CrystalEarrings",
		dim(){ 
			return {
				zoom: "face",
				hanger:rg.range(3,5), 
				alt:rg.range(-2,2),
				length:rg.range(5,10),
				width:rg.range(2.2,3.2),
			}; 
		},
		stroke: ["NXgold","NXsilver","NXbronze","NXsteel"],
		fill:["bright"],
	},
	
	triangleEarrings : {
		name: "triangle earrings",
		slot:"earrings",
		dad: "TriangleEarrings",
		dim(){ 
			return {
				zoom: "face",
				length:rg.range(4,10),
				width:rg.range(1,2.8),
			}; 
		},
		stroke: ["NXgold","NXsilver","NXbronze","NXsteel"],
		fill:["bright"],
	},
	
	
	rhombEarrings : {
		name: "rhomb earrings",
		slot:"earrings",
		dad: "RhombEarrings",
		dim(){ 
			return {
				zoom: "face",
				thickness:rg.range(0.75,1),
				alt:rg.range(0.5,0.75),
				length:rg.range(7,14),
				width:rg.range(2,4),
			}; 
		},
		stroke:["NXgold","NXsilver","NXbronze","NXsteel","bright","bright","bright"],
		//fill:["bright"],
	},
	
	
	chainEarrings : {
		name: "chain earrings",
		slot:"earrings",
		dad: "ChainEarrings",
		dim(){ 
			return {
				zoom: "face",
				length:rg.range(8,16),
			}; 
		},
		fill:["NXgold","NXsilver","NXbronze","NXsteel"],
	},
	
	loopEarrings : {
		name: "loop earrings",
		slot:"earrings",
		dad: "LoopEarrings",
		dim(){ 
			return {
				zoom: "face",
				width:rg.range(2,3),
				length:rg.range(8,13),
			}; 
		},
		fill: ["NXgold","NXsilver","NXbronze","NXsteel","bright","bright","bright"],
	},
	
	bimboLoops : {
		name: "loop earrings",
		slot:"earrings",
		dad: "LoopEarrings",
		dim(){ 
			const random = rg.g
			return {
				zoom: "face",
				thickness:rg.range(0.6, 1.0),
				width:rg.range(4,6, random),
				length:rg.range(13, 19, random),
			}; 
		},
		fill: ["NXgold","NXgold","bimbo","bimbo","bright","dark"],
	},
	
	
//NECKLACES
	polyBiChain : {
		name: "necklace",
		slot:"necklace",
		dad: "BiChain",
		dim(){ 
			return {
				zoom: "face",
				beadSize: rg.range(2,4),
				spaceSize: rg.range(8,10),
				neckCoverage: rg.range(0.04,0.14),
				cleavageCoverage: rg.range(0.10,0.18),
			}; 
		},
		
		fill: ["full","bright"],
		stroke: "complimentary",
	},
	
	
	biChain : {
		name: "necklace",
		slot:"necklace",
		dad: "BiChain",
		dim(){ 
			return {
				zoom: "face",
				beadSize: rg.range(2,4),
				spaceSize: rg.range(8,10),
				neckCoverage: rg.range(0.04,0.14),
				cleavageCoverage: rg.range(0.10,0.18),
			}; 
		},
		
		fill: "bright",
		stroke: exgar.jew_metal_fill,
	},
	
	
	metalSimpleChain : {
		name: "chain",
		slot:"necklace",
		dad: "SimpleChain",
		dim(){ 
			return {
				zoom: "face",
				cleavageCoverage: rg.range(0.08, 0.15),
				neckCoverage: rg.range(0.08, 0.12),
				thickness: rg.range(0.6, 0.8),
				dash: rg.range(2.5, 5),
				chain: true,
			};
		},
		fill: exgar.jew_metal_fill,
	},
	
	
	polySimpleChain : {
		name: "chain",
		slot:"necklace",
		dad: "SimpleChain",
		dim(){ 
			return {
				zoom: "face",
				cleavageCoverage: rg.range(0.08, 0.15),
				neckCoverage: rg.range(0.08, 0.12),
				thickness: rg.range(0.5, 0.7),
				dash: rg.range(2.5, 4.5),
				chain: true,
			};
		},
		fill:["brigh","bright","full","dark"],
	},
	
	
	metalTearNecklace : {
		name: "necklace",
		slot:"necklace",
		dad: "TearNecklace",
		dim(){ 
			return {
				zoom: "face",
				size: rg.range(0.8,1.2),
				width: rg.range(2.5,5), //TO DO 
				length: rg.range(0,5), //TO DO - THIS LENGHT DOESNT EVEN MAKE SENSE HWAT IT EVEN MEANS?
				neckCoverage: rg.range(0.05,0.1),
				cleavageCoverage: rg.range(0.09,0.16),
			}; 
		},
		fill: exgar.jew_metal_fill,
	},
	
	
	tearNecklace : {
		name: "necklace",
		slot:"necklace",
		dad: "TearNecklace",
		dim(){ 
			return {
				zoom: "face",
				size: rg.range(0.8,1.2),
				width: rg.range(2.5,5), //TO DO 
				length: rg.range(0,5), //TO DO - THIS LENGHT DOESNT EVEN MAKE SENSE HWAT IT EVEN MEANS?
				neckCoverage: rg.range(0.05,0.1),
				cleavageCoverage: rg.range(0.09,0.16),
			}; 
		},
		fill: ["bright","dark"],
		stroke: ["dark","monochromatic","dark"],
	},


	doubleNecklace : {
		name: "necklace",
		slot:"necklace",
		dad: "DoubleNecklace",
		dim(){ 
			return {
				zoom: "face",
				cleavageCoverage: rg.range(0.10,0.18),
				cleavageCoverageTop: rg.range(0.06,0.09),

				//TO DO - FROM TEAR NECKLACE
				size: rg.range(0.8,1),
				width: rg.range(2.5,4), //TO DO 
				length: rg.range(0.5,4), //TO DO - THIS LENGHT DOESNT EVEN MAKE SENSE HWAT IT EVEN MEANS?

			}; 
		},
		fill: exgar.jew_metal_fill,
	},
	
	
	multiNecklace : {
		name: "chain",
		slot:"necklace",
		dad: "MultiNecklace",
		dim(){
			let distance;
			let cleavageCoverage;
			const multiple = Math.ceil( rg.g*3 );
			switch(multiple){
				default:
				case 1:
					cleavageCoverage = rg.range(0.08,0.15);
					distance = rg.range(5,10);
					break;
				case 2:
					cleavageCoverage = rg.range(0.08,0.14);
					distance = rg.range(5,8);
					break;
				case 3:
					cleavageCoverage = rg.range(0.08,0.12);
					distance = rg.range(4,6);
					break
					
			}
			return {
				zoom: "face",
				cleavageCoverage,
				distance,
				multiple,
				neckCoverage: rg.range(0.08,0.12),
				thickness: rg.range(0.5, 0.75),
				chain: true, //rg.g<0.5 ? true : false,
				dash: rg.range(2.5, 4.5),
			}; 
		},
		fill: ["brigh","bright","full","dark", ...exgar.jew_metal_fill], 
	},
	
	
	beadNecklace : {
		name: "bead necklace",
		slot:"necklace",
		dad: "PearlNecklace",
		dim(){ 
			return {
				zoom: "face",
				neckCoverage: rg.range(0.05, 0.2),
				cleavageCoverage: rg.range(0.10, 0.21),
				beadThickness: rg.range(1.8,2.4),
				spaceSize: rg.range(8,10),
			}; 
		},
		fill: ["dark","bright"],
	},
	


//PRECIOUS
	preciousSimpleChain : {
		name: "chain",
		slot:"necklace",
		dad: "SimpleChain",
		dim(){ 
			return {
				zoom: "face",
				cleavageCoverage: rg.range(0.08, 0.15),
				neckCoverage: rg.range(0.08, 0.12),
				thickness: rg.range(0.6, 0.8),
				dash: rg.range(2.5, 5),
				chain: true,
			};
		},
		material: exgar.irl_precious_metal,
	},
	
	preciousTearNecklace : {
		name: "necklace",
		slot:"necklace",
		dad: "TearNecklace",
		dim(){ 
			return {
				zoom: "face",
				size: rg.range(0.8,1.2),
				width: rg.range(2.5,5), //TO DO 
				length: rg.range(0,5), //TO DO - THIS LENGHT DOESNT EVEN MAKE SENSE HWAT IT EVEN MEANS?
				neckCoverage: rg.range(0.05,0.1),
				cleavageCoverage: rg.range(0.09,0.16),
			}; 
		},
		material: exgar.irl_precious_metal,
	},


	preciousDoubleNecklace : {
		name: "necklace",
		slot:"necklace",
		dad: "DoubleNecklace",
		dim(){ 
			return {
				zoom: "face",
				cleavageCoverage: rg.range(0.10,0.18),
				cleavageCoverageTop: rg.range(0.06,0.09),

				//TO DO - FROM TEAR NECKLACE
				size: rg.range(0.8,1),
				width: rg.range(2.5,4), //TO DO 
				length: rg.range(0.5,4), //TO DO - THIS LENGHT DOESNT EVEN MAKE SENSE HWAT IT EVEN MEANS?

			}; 
		},
		material: exgar.irl_precious_metal,
	},
	
	
	preciousMultiNecklace : {
		name: "chain",
		slot:"necklace",
		dad: "MultiNecklace",
		dim(){
			let distance;
			let cleavageCoverage;
			const multiple = Math.ceil( rg.g*3 );
			switch(multiple){
				default:
				case 1:
					cleavageCoverage = rg.range(0.08,0.15);
					distance = rg.range(5,10);
					break;
				case 2:
					cleavageCoverage = rg.range(0.08,0.14);
					distance = rg.range(5,8);
					break;
				case 3:
					cleavageCoverage = rg.range(0.08,0.12);
					distance = rg.range(4,6);
					break
					
			}
			return {
				zoom: "face",
				cleavageCoverage,
				distance,
				multiple,
				neckCoverage: rg.range(0.08,0.12),
				thickness: rg.range(0.5, 0.75),
				chain: true, //rg.g<0.5 ? true : false,
				dash: rg.range(2.5, 4.5),
			}; 
		},
		material: exgar.irl_precious_metal,
	},
	
	
//FANTASY
	magicTearNecklace : {
		name: "necklace",
		slot:"necklace",
		dad: "TearNecklace",
		dim(){ 
			return {
				zoom: "face",
				size: rg.range(0.8,1.2),
				width: rg.range(2.5,5), //TO DO 
				length: rg.range(0,5), //TO DO - THIS LENGHT DOESNT EVEN MAKE SENSE HWAT IT EVEN MEANS?
				neckCoverage: rg.range(0.05,0.1),
				cleavageCoverage: rg.range(0.09,0.16),
			}; 
		},
		att: exgar.magic_1,
		material: exgar.precious_metal,
	},
	
	minorMagicTearNecklace : {
		name: "necklace",
		slot:"necklace",
		dad: "TearNecklace",
		dim(){ 
			return {
				zoom: "face",
				size: rg.range(0.8,1.2),
				width: rg.range(2.5,5), //TO DO 
				length: rg.range(0,5), //TO DO - THIS LENGHT DOESNT EVEN MAKE SENSE HWAT IT EVEN MEANS?
				neckCoverage: rg.range(0.05,0.1),
				cleavageCoverage: rg.range(0.09,0.16),
			}; 
		},
		att: exgar.minor_magic_1,
		material: exgar.precious_metal,
	},
	
	majorMagicTearNecklace : {
		name: "necklace",
		slot:"necklace",
		dad: "TearNecklace",
		dim(){ 
			return {
				zoom: "face",
				size: rg.range(0.8,1.2),
				width: rg.range(2.5,5), //TO DO 
				length: rg.range(0,5), //TO DO - THIS LENGHT DOESNT EVEN MAKE SENSE HWAT IT EVEN MEANS?
				neckCoverage: rg.range(0.05,0.1),
				cleavageCoverage: rg.range(0.09,0.16),
			}; 
		},
		att: exgar.minor_magic_1,
		material: exgar.precious_metal,
	},
	
	fMultiNecklace : {
		name: "chain",
		slot:"necklace",
		dad: "MultiNecklace",
		dim(){
			let distance;
			let cleavageCoverage;
			const multiple = Math.ceil( rg.g*3 );
			switch(multiple){
				default:
				case 1:
					cleavageCoverage = rg.range(0.08,0.15);
					distance = rg.range(5,10);
					break;
				case 2:
					cleavageCoverage = rg.range(0.08,0.14);
					distance = rg.range(5,8);
					break;
				case 3:
					cleavageCoverage = rg.range(0.08,0.12);
					distance = rg.range(4,6);
					break
					
			}
			return {
				zoom: "face",
				cleavageCoverage,
				distance,
				multiple,
				neckCoverage: rg.range(0.08,0.12),
				thickness: rg.range(0.5, 0.75),
				chain: true, //rg.g<0.5 ? true : false,
				dash: rg.range(2.5, 4.5),
			}; 
		},
		material: exgar.precious_metal,
	},
	
	
	fTearNecklace : {
		name: "necklace",
		slot:"necklace",
		dad: "TearNecklace",
		dim(){ 
			return {
				zoom: "face",
				size: rg.range(0.8,1.2),
				width: rg.range(2.5,5), //TO DO 
				length: rg.range(0,5), //TO DO - THIS LENGHT DOESNT EVEN MAKE SENSE HWAT IT EVEN MEANS?
				neckCoverage: rg.range(0.05,0.1),
				cleavageCoverage: rg.range(0.09,0.16),
			}; 
		},
		material: exgar.precious_metal,
	},
	
	
	/*
	starNecklace : {
		name: "star necklace",
		slot:"necklace",
		dad: "StarNecklace",
		dim: (function(){ 
			return {
				zoom: "face",
				neckCoverage: rg.range(0.04,0.16),
				cleavageCoverage: rg.range(0.10,0.16),
				
				styleOuter: rg.range(-1,1),
				radius: rg.range(3,3.9),
				spiker: Math.floor( rg.range(5,10) ),
				upwards: rg.g<0.5 ? true : false,
				chain: rg.g<0.5 ? true : false,
			}; 
		}),
		fill:exgar.jew_metal_fill,
	},
	
	
	pentagram : {
		name: "pentagram",
		slot:"necklace",
		dad: "StarNecklace",
		dim: (function(){ 
			return {
				zoom: "face",
				neckCoverage: rg.range(0.04,0.16),
				cleavageCoverage: rg.range(0.10,0.16),
			
				radius: rg.range(3,3.9),
				starThickness: rg.range(0.5,0.7),
				upwards: rg.g<0.5 ? true : false,
				chain: rg.g<0.5 ? true : false,
			}; 
		}),
		fill:["NXgold","NXsilver","NXbronze","NXsteel"],
	},
	*/
	
	
}