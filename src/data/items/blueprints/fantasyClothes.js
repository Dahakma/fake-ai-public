import {exgar} from "./exgar";
import {rg, ra} from "Libraries/random";

export const fantasyClothes = {
	//INITIAL
	/*fBlandPaties : {
		name: "panties",
		adjective: "linen",
		slot:"panties",
		id: 3001,
		dad: "wSuperPanties",
		dim: (function(){ 
			return {
				thickness: 0.4,
				waistCoverage:[0.1, 0.3],
				waistCoverageLower:[-0.05,-0.2],
				//waistCoverageLower:[top - 0.25, top -0.4],
				genCoverage:1,
				curveTopX: -2,
				curveTopY: [-2,0],
				bow: false,
				ruffling: false,
			}
		}),
		price: 3,
		fill: "hemp",
	},
	
	fBlandThong : {
		name: "panties",
		adjective: "linen",
		slot:"panties",
		id: 3002,
		dad: "wSuperPanties",
		dim: (function(){ 
			let top = rg.range(-0.05, 0.2);
			//let bot = top - between(0.06,0.18); 
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.1, top -0.2],
				genCoverage: [0.5,0.8],
				curveTopX: [-2,-3],
				curveTopY: [-2,-6],
				curveBotX: [0,-2],
				bow: false,
				ruffling: false,
		}}),
		price: 3,
		fill: "hemp",
	},

	fBlandBra : {
		name: "bra",
		adjective: "linen",
		slot:"bra",
		id: 3003,
		dad: "wSuperPushUpBra",
		dim: (function(){ 
			return {
				invisible: 0,
				thickness: 0.9, //has to be the same
				strapWidth: 0.9, //has to be the same
				showStrap: true,
				neckCoverage: [0.5,0.95],
				botStrapCoverage: [0.65, 1],
				ruffling: false,
				coverage: [0.3,0.5],
			}
		}),
		price: 3,
		fill: "hemp",
	},
	*/
	

	
	
//HARLOT
//HARLOT SKIRT
	
	
	
	

	
	




	
	

	


	

	
//LOINCLOTH
	

	
//FALLEN


	
//FALEN LOINCLOTH	
	
	
//FALLENT GORGET

	
	



//LOWER
	
	
	
	
	gothShirt : {
		name: "shirt",
		dad: "wTee",
		slot:"upper",
		dim(){
			return {
				lacing: false,
				sideLoose: 1,
				armLoose: [0.2, 0.8],
				armCoverage: [0.55,0.9],
				curveCleavageY: [-3, 0],
				curveCleavageX: [1, 4],
				waistCoverage: [-0.4, 0],
				cleavageOpeness: [0.05,0.2],
				cleavageCoverage: [0.1,0.16],
				invisible: 0,

			};
		},
		fill: "red", //TODO
	},
	
	
	gothCuirass: {
		name: "cuirass",
		//adjective: "bronze",
		dad: "wCuirass",
		slot: "upperOuter",
		dim(){return{
		//	fill: da.getPattern("copper", 120),
		//	stroke: "hsla(13, 50%, 13%, 1)",
			sideLoose: 0.5,
			waistCoverage: [0, 0],
		}},
		fill: "darkGrey",
		//texture: exgar.bronze,
	},
	
	gothGoldPauldrons: {
		name: "pauldrons",
		dad: "wFancyPauldron",
		slot: "pauldrons",
		dim(){return{		
		}},
		material: "gold",
	},
	
	gothPauldrons: {
		name: "pauldrons",
		dad: "wFancyPauldron",
		slot: "pauldrons",
		dim(){return{		
		}},
		fill: "darkGrey",
	},
	
	
	
	/*
	bronzeCuirass: {
		name: "cuirass",
		id: 3337, //TODO
		//adjective: "bronze",
		dad: "wCuirass",
		slot: "upperOuter",
		hp: 5,
		dim: (function(){return{
		//	fill: da.getPattern("copper", 120),
		//	stroke: "hsla(13, 50%, 13%, 1)",
		}}),
		//fill: "darkGrey",
		texture: exgar.bronze,
	},*/
	
	/*
	goldCuirass: {
		name: "cuirass",
		//adjective: "bronze",
		dad: "wCuirass",
		slot: "upperOuter",
		hp: 5,
		dim: (function(){return{
		//	fill: da.getPattern("copper", 120),
		//	stroke: "hsla(13, 50%, 13%, 1)",
		}}),
		//fill: "darkGrey",
		material: "gold",
	},	
	
	*/
	
	
	

	siraelTop : {
		name: "top",
		dad: "wBikiniTop", 
		slot:"upper",
		//adjective: "linen",
		dim(){ return{
			radius: 5,
			curveCleavageY: [-2, -14],
		}},
		
		fill: "indigo",
	}, 	
 
	siraelBottom : {
		name: "skirt",
		dad: "wLoincloth",
		slot: "lower",
		dim(){ return{
			beltWidth: 3,
			bottomCoverage: 0.2,
			curveX: -18,
			legCoverage: 0.75,
			topCoverage: 0.5,
			waistCoverage: 0.05,
		}},
		
		fill: "indigo",
		highlight: "monochromatic",
	},
	
	
	siraelCollar : {
		name: "collar",
		slot:"collar",
		dad: "Collar",
		dim(){ 
				return {
				}; 
		},
		
		fill: "indigo", //TODO 
		highlight: "monochromatic",
	},
	
	
	siraelTopGreen : {
		name: "top",
		dad: "wBikiniTop", 
		slot:"upper",
		//adjective: "linen",
		dim(){ return{
			radius: 5,
			curveCleavageY: [-2, -14],
		}},
		
		fill: "solidGreen",
	}, 	
 
	siraelBottomGreen : {
		name: "skirt",
		dad: "wLoincloth",
		slot: "lower",
		dim(){ return{
			beltWidth: 3,
			bottomCoverage: 0.2,
			curveX: -18,
			legCoverage: 0.75,
			topCoverage: 0.5,
			waistCoverage: 0.05,
		}},
		
		fill: "solidGreen",
		highlight: "monochromatic",
	},
	
	siraelCollarGreen : {
		name: "collar",
		slot:"collar",
		dad: "Collar",
		dim(){ 
				return {
				}; 
		},
		
		fill: "solidGreen", //TODO 
		highlight: "monochromatic",
	},
	
	
	
	
	
	
	
	
	
	
	
 
	
	
	
	
	

 
	
	
}