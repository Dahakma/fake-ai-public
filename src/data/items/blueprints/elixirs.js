

export const elixirs = {
	xHP: {
		slot: "potion",
		type: "health",
		skill: "health",
	},
	
	xEng: {
		slot: "potion",
		type: "energy",
		skill: "energy",
	},
	
	xAnti: {
		slot: "potion",
		type: "antivenom",
		skill: "antivenom",
	},
	
	xBow: {
		slot: "potion",
		type: "bovinium",
	},
	
	xElf: {
		slot: "potion",
		type: "elfium",
	},
	
	xGob: {
		slot: "potion",
		type: "bovinium",
	},
	
	xFel: {
		slot: "potion",
		type: "felinium",
	},
	
	xMale: {
		slot: "potion",
		type: "male",
	},
	
	xFemale: {
		slot: "potion",
		type: "female",
	},
	
	xHum: {
		slot: "potion",
		type: "humanium",
	},
	
	xSuc: {
		slot: "potion",
		type: "succubium",
	},
	
	xInc: {
		slot: "potion",
		type: "incubium",
	},
	
	xMin: {
		slot: "potion",
		type: "minotaurium",
	},
	
	xNaj: {
		slot: "potion",
		type: "najadium",
	},
	
	
	
	charmLightning: {
		name: "Charm of Lightning",
		slot: "charm",
		skill: "charmLightning",
	},
	
	charmBlizzard: {
		name: "Charm of Blizzard",
		slot: "charm",
		skill: "charmBlizzard",
	},
	
	charmFirestorm: {
		name: "Charm of Firestorm",
		slot: "charm",
		skill: "charmFirestorm",
	},
	
	charmBlades: {
		name: "Charm of Blades",
		slot: "charm",
		skill: "charmBlades",
	},
	
	charmDazzle: {
		name: "Charm of Dazzling",
		slot: "charm",
		skill: "charmDazzle",
	},
	
	charmDarkness: {
		name: "Charm of Darkness",
		slot: "charm",
		skill: "charmDarkness",
	},
}