import {exgar, rg} from "./exgar";

export const accessory = {	
//BELTS
	//dark, wide, big buckle
	darkWideBelt : {
		name: "belt", 
		dad: "wBelt",
		slot:"belt",
		note: "dark, wide, big buckle",
		dim(){ return {
			waistCoverage: [0.25, 0.5], //0.4 - 0.7
			beltWidth: [4, 6.5],
			buckleWidth: [3, 5],
			buckleHeight: [0.0, 0.9],
			showBuckle: rg.g < 0.9,
		}},
		fill: ["leather","darkLeather","darkGrey"],
		highlight: exgar.util_metal_fill,
	}, 
	
	//various colors, wide
	polyWideBelt : {
		name: "belt",
		dad: "wBelt",
		slot:"belt",
		note: "various colors, wide",
		dim(){ return {
			waistCoverage: [0.25, 0.5], //0.4 - 0.7
			beltWidth: [5, 7],
			buckleWidth: [3, 5],
			buckleHeight: [0.0, 0.9],
			showBuckle: rg.g < 0.8,
		}},
		fill: ["full", "dark", "full", "dark", "bright"],
		highlight:  ["contrast", "monochromatic", ...exgar.util_metal_fill],
	}, 
	
	//dark, thin
	darkThinBelt : {
		name: "belt",
		dad: "wBelt",
		slot:"belt",
		note: "dark, thin",
		dim(){ return {
			waistCoverage: [0.25, 0.5], //0.4 - 0.7
			beltWidth: [2.5, 4],
			buckleWidth: [2, 4],
			buckleHeight: [-0.05, 0.5],
			showBuckle: rg.g < 0.7,
		}},
		fill: ["leather","darkLeather","darkGrey"],
		highlight: exgar.util_metal_fill,
	}, 
	
	//various colors, thin
	polyThinBelt : {
		name: "belt",
		dad: "wBelt",
		slot:"belt",
		note: "various colors, thin",
		dim(){ return {
			waistCoverage: [0.25, 0.5], //0.4 - 0.7
			beltWidth: [2.5, 4],
			buckleWidth: [2, 4],
			buckleHeight: [-0.05, 0.5],
			showBuckle: rg.g < 0.7,
		}},
		fill: ["full", "dark", "full", "dark", "bright"],
		highlight:  ["contrast", "monochromatic", ...exgar.util_metal_fill],
	}, 
	
//GLASSES
	glasses : {
		name: "glasses",
		slot:"glasses",
		dad: "wGlasses",
		dim(){ 
				return {
					//fill: "hsla(0.0,0.0%,0.0%,0.00)",
					height: [2.2, 3.8],
					eccentricity: [2.5,7],
					thickness: [0.4, 0.7],
				}; 
		},
		stroke: ["nearlyBlack"],
	},
	
	polyGlasses : {
		name: "glasses",
		slot:"glasses",
		dad: "wGlasses",
		dim(){ 
				return {
					//fill: "hsla(0.0,0.0%,0.0%,0.00)",
					height: [2.2, 3.8],
					eccentricity: [2.5,7],
					thickness: [0.4, 0.7],
				}; 
		},
		stroke: ["full","dark"],
	},
	
//FANTASY BELTS
	//dark, wide, big buckle
	fDarkWideBelt : {
		name: "belt", 
		dad: "wBelt",
		slot:"belt",
		note: "game, dark, wide, big buckle",
		dim(){ return {
			waistCoverage: [0.25, 0.5], //0.4 - 0.7
			beltWidth: [4, 6.5],
			buckleWidth: [3, 5],
			buckleHeight: [0.0, 0.9],
			showBuckle: rg.g < 0.9,
		}},
		fill: ["leather","darkLeather","darkGrey"],
		highlight: exgar.util_metal_fill,
	}, 
	
	//various colors, wide
	fPolyWideBelt : {
		name: "belt",
		dad: "wBelt",
		slot:"belt",
		note: "game, various colors, wide",
		dim(){ return {
			waistCoverage: [0.25, 0.5], //0.4 - 0.7
			beltWidth: [5, 7],
			buckleWidth: [3, 5],
			buckleHeight: [0.0, 0.9],
			showBuckle: rg.g < 0.8,
		}},
		fill: ["full", "dark", "full", "dark", "bright"],
		highlight:  exgar.util_metal_fill,
	}, 
	
	//rare leather, wide
	fFancyWideBelt : {
		name: "belt",
		dad: "wBelt",
		slot:"belt",
		note: "game, rare leather, wide",
		dim(){ return {
			waistCoverage: [0.25, 0.5], //0.4 - 0.7
			beltWidth: [5, 7],
			buckleWidth: [3, 5],
			buckleHeight: [0.0, 0.9],
			showBuckle: rg.g < 0.8,
		}},
		material: exgar.rare_leather,
		highlight:  exgar.util_metal_fill,
	}, 
	
	//dark, thin
	fDarkThinBelt : {
		name: "belt",
		dad: "wBelt",
		slot:"belt",
		note: "game, dark, thin",
		dim(){ return {
			waistCoverage: [0.25, 0.5], //0.4 - 0.7
			beltWidth: [2.5, 4],
			buckleWidth: [2, 4],
			buckleHeight: [-0.05, 0.5],
			showBuckle: rg.g < 0.7,
		}},
		fill: ["leather","darkLeather","darkGrey"],
		highlight: exgar.util_metal_fill,
	}, 
	
	//various colors, thin
	fPolyThinBelt : {
		name: "belt",
		dad: "wBelt",
		slot:"belt",
		note: "game, various colors, thin",
		dim(){ return {
			waistCoverage: [0.25, 0.5], //0.4 - 0.7
			beltWidth: [2.5, 4],
			buckleWidth: [2, 4],
			buckleHeight: [-0.05, 0.5],
			showBuckle: rg.g < 0.7,
		}},
		fill: ["full", "dark", "full", "dark"],
		highlight:  exgar.util_metal_fill,
	}, 
	
	
	//various colors, thin
	fFancyThinBelt : {
		name: "belt",
		dad: "wBelt",
		slot:"belt",
		note: "game, rare, thin",
		dim(){ return {
			waistCoverage: [0.25, 0.5], //0.4 - 0.7
			beltWidth: [2.5, 4],
			buckleWidth: [2, 4],
			buckleHeight: [-0.05, 0.5],
			showBuckle: rg.g < 0.7,
		}},
		material: exgar.rare_leather,
		highlight:  exgar.util_metal_fill,
	}, 
	
	
//COLLARS
	choker : {
		name: "choker",
		slot:"collar",
		dad: "wChoker",
		dim(){ 
				return {
					center: rg.g < 0.5,
					neckBotCoverage: 0,
					neckCoverage: [0.15,0.22],
					thickness: [0.4,0.7],
				}; 
		},
		fill: ["black","nearlyBlack","full","dark"], //TODO 
	},
	
	choker2 : {
		name: "choker",
		slot:"collar",
		dad: "wChoker2",
		note: "choker using different DAD pattern",
		dim(){ 
				return {
					heart: rg.g < 0.5,
					radius: [1.3, 2.1],
					//thickness: between(0.4,0.7),
				}; 
		},
		fill: ["black","nearlyBlack","full","dark"], //TODO 
		highlight: ["contrast"],
	},
	
	crossedChoker : {
		name: "choker",
		slot:"collar",
		dad: "CrossedChoker",
		adjectvie: "",
		dim(){ 
				const bot = rg.range(0.03, 0.1);
				return {
					crossings: rg.integer(1,3),
					neckBotCoverage: bot,
					neckCoverage: bot + rg.range(0.1, 0.2),
					thickness: rg.range(0.4, 0.7),
				}; 
		},
		fill: "black",
	},
	
	polyCrossedChoker : {
		name: "choker",
		slot:"collar",
		dad: "CrossedChoker",
		adjectvie: "",
		dim(){ 
				const bot = rg.range(0.03, 0.1);
				return {
					crossings: rg.integer(1,3),
					neckBotCoverage: bot,
					neckCoverage: bot + rg.range(0.1, 0.2),
					thickness: rg.range(0.4, 0.7),
				}; 
		},
		fill: ["dark","full","bright","bright"],
	},
	
	
	everydayCollar : { //TODO
		name: "collar",
		slot:"collar",
		dad: "Collar",
		dim(){ 
				return {
					radius: 2.5, 
				}; 
		},
		
		fill: ["darkLeather", "nearlyBlack", "dark", "dark"], //TODO 
		//highlight: "monochromatic",
	},
	
	//slaveCollar in story 
	
	
//FANTASY COLLARS
	fChoker : {
		name: "choker",
		slot:"collar",
		dad: "wChoker",
		dim(){ 
				return {
					center: rg.g < 0.5,
					neckBotCoverage: 0,
					neckCoverage: [0.15,0.22],
					thickness: [0.4,0.7],
				}; 
		},
		fill: ["black","nearlyBlack","full","dark"], //TODO 
	},
	
	fLeatherChoker : {
		name: "choker",
		slot:"collar",
		dad: "wChoker",
		dim(){ 
				return {
					center: rg.g < 0.5,
					neckBotCoverage: 0,
					neckCoverage: [0.15,0.22],
					thickness: [0.4,0.7],
				}; 
		},
		material: exgar.leather, 
	},
	
	fFancyChoker : {
		name: "choker",
		slot:"collar",
		dad: "wChoker",
		note: "rare leather",
		dim(){ 
				return {
					center: rg.g < 0.5,
					neckBotCoverage: 0,
					neckCoverage: [0.15,0.22],
					thickness: [0.4,0.7],
				}; 
		},
		material: exgar.rare_leather, 
	},

	fMetalChoker : {
		name: "choker",
		slot:"collar",
		dad: "wChoker",
		note: "precious metal",
		dim(){ 
				return {
					center: rg.g < 0.5,
					neckBotCoverage: 0,
					neckCoverage: [0.15,0.22],
					thickness: [0.4,0.7],
				}; 
		},
		//att: [{dex: 10},{char: 10}],
		material: exgar.precious_metal,
	},
	
//GORGETS
	defFallenGorget : {
		name: "gorget",
		dad: "wGorget",
		slot: "neck",
		dim(){ return{
			cleavageTopCoverage: -0.1,
			neckTopCoverage: -1,
		}},
		fill: "darkLeather",
	},
	
	fallenGorget : {
		name: "gorget",
		dad: "wGorget",
		slot: "neck",
		dim(){ 
			const bot =  rg.g;
			const top =  rg.g;
			return{
				cleavageCoverage: rg.range(0.14, 0.19, bot),
				neckBotCoverage: rg.range(0.66, 0.99, bot),
				cleavageTopCoverage: rg.range(0, 0.029, top),
				neckTopCoverage: rg.range(-0.21, 0.0, top),
				groove: rg.g < 0.4,
		}},
		fill: ["dark","darkLeather", ...exgar.fallen_fill],
	},
	
//FINGERLESS GLOVES 


	fingerlessGloves : {
		name: "fingerless gloves",
		slot:"gloves",
		dad: "FingerlessGloves",
		dim(){ 
			return {
				armCoverage: [0.85, 0.9],
				thickness: [0.4, 0.6],
			}; 
		},
		//fill: ["nearlyBlack", "darkGrey", "tintedDark"], 
		fill: ["nearlyBlack"], 
	},
	
	sleeves : {
		name: "sleeves",
		slot: "gloves",
		dad: "GloveSleeve",
		dim(){ 
			return {
				armCoverage: rg.g < 0.65 ? [0.6, 0.7] : [0.33, 0.46],
				alpha: [0.5, 1],
				thickness: [0.4, 0.6],
			}; 
		},
		//fill: ["nearlyBlack", "darkGrey", "tintedDark"], 
		fill: ["lightGrey"], 
	},
	
	fishnetSleeves : {
		name: "sleeves",
		slot: "gloves",
		dad: "GloveSleeve",
		dim(){ 
			return {
				armCoverage: rg.g < 0.65 ? [0.6, 0.7] : [0.33, 0.46],
				thickness: [0.2, 0.4],
			}; 
		},
		texture: exgar.dense_fishnet,
		stroke: "",
	},
	
	longGloves : {
		name: "gloves",
		slot: "gloves",
		dad: "LongGloves",
		dim(){ 
			return {
				armCoverage: [0.33, 0.46],
				alpha: [0.75, 1],
				thickness: [0.4, 0.6],
			}; 
		},
		//fill: ["nearlyBlack", "grey", "mat", "dark"], 
		fill: ["grey"], 
	},
	
	
/*
	defFallenGorget : {
		name: "gorget",
		id: 3331,
		dad: "wGorget",
		slot: "neck",
		hp: 1,
		dim: (function(){ return{
			cleavageTopCoverage: -0.1,
			neckTopCoverage: -1,
		}}),
		price: 8,
		fill: "darkLeather",
	},
	


	fallenGorget : {
		name: "gorget",
		id: 3332,
		dad: "wGorget",
		slot: "neck",
		hp: 1,
		dim: (function(){ 
			const bot =  rg.g;
			const top =  rg.g;
			return{
				cleavageCoverage: rg.range(0.14, 0.19, bot),
				neckBotCoverage: rg.range(0.66, 0.99, bot),
				cleavageTopCoverage: rg.range(0, 0.029, top),
				neckTopCoverage: rg.range(-0.21, 0.0, top),
				groove: rg.g < 0.4 ? true : false,
		}}),
		price: 8,
		fill: ["dark","darkLeather", ...exgar.fallen_fill],
	},
	
	fancyGorget : {
		name: "gorget",
		id: 3333,
		dad: "wGorget",
		slot: "neck",
		hp: 1,
		dim: (function(){ 
			const bot =  rg.g;
			const top =  rg.g;
			return{
				cleavageCoverage: rg.range(0.14, 0.19, bot),
				neckBotCoverage: rg.range(0.66, 0.99, bot),
				cleavageTopCoverage: rg.range(0, 0.029, top),
				neckTopCoverage: rg.range(-0.21, 0.0, top),
				//groove: WARDROBE.Rnd() < 0.4 ? true : false,
		}}),
		material: exgar.rare_leather,
		//price: 16,
		//fill: exgar.rare_leather_fill,
	},
	
	preciousGorget : {
		name: "gorget",
		id: 3334,
		dad: "wGorget",
		slot: "neck",
		hp: 1,
		dim: (function(){ 
			const bot =  rg.g;
			const top =  rg.g;
			return{
				cleavageCoverage: rg.range(0.14, 0.19, bot),
				neckBotCoverage: rg.range(0.66, 0.99, bot),
				cleavageTopCoverage: rg.range(0, 0.029, top),
				neckTopCoverage: rg.range(-0.21, 0.0, top),
				//groove: WARDROBE.Rnd() < 0.4 ? true : false,
		}}),
		material: exgar.precious_metal,
		//price: 16,
		//fill: exgar.rare_leather_fill,
	},
*/

		
	mageHat : {
		name: "wizard hat",
		dad: "MagicHat",
		slot: "hat",
		dim(){
			return {
				brimAngle: 2,
				brimWidth: 5,
				coneHeight: 8.5,
				coneWidth: 0.4,
			};
		},
		fill: "darkGrey",
	},
	
	polyMageHat : {
		name: "wizard hat",
		dad: "MagicHat",
		slot: "hat",
		dim(){
			return {
				brimAngle: 2,
				brimWidth: 5,
				coneHeight: 8.5,
				coneWidth: 0.4,
			};
		},
		fill: ["dark", "full"],
	},
	
}