import {exgar} from "./exgar";
import {rg} from "Libraries/random";

export const story = {
	
//BIKINI
//TODO!!
	swimsuit: {
		dad: "Swimsuit",
		slot: "upperLower", //TODO!! upperLowerInner
		dim(){ return {
			cleavageCoverage: [0.25, 0.35],
			innerNeckCoverage: [0.05, 0.2],
			outerNeckCoverage: [0.2, 0.5],
			waistCoverage: [-0.1, 0.2], 
		}},
		fill: exgar.upper_fill,
	},
	
	decentBikiniTop: {
		dad: "wBikiniTop",
		slot: "bra",
		dim(){ return {
			curveCleavageX: 13,
			curveCleavageY: -12,
			innerNeckCoverage: 0.015609906484431058,
			outerNeckCoverage: 0.43653273044908025,
			radius: 3.900000000000001,
		}},
		fill: exgar.upper_fill,
	},
	
	decentBikiniBottom: {
		dad: "wSuperPanties",
		slot: "panties",
		dim(){ return {
			curveCleavageX: 13,
			curveCleavageY: -12,
			 waistCoverage: 0.22688172043010746,
			waistCoverageLower: -0.24107526881720442,
			radius: 3.900000000000001,
		}},
		fill: exgar.upper_fill,
	},
	
	
	sexyBikiniTop: {
		dad: "wBikiniTop",
		slot: "bra",
		dim(){ return {
			curveCleavageX: 17,
			curveCleavageY: -21,
			innerNeckCoverage: 0.12084061247559341,
			outerNeckCoverage: 0.43653273044908025,
			radius: 2.900000000000001,
		}},
		fill: exgar.upper_fill,
	},
	
	sexyBikiniBottom: {
		dad: "wSuperPanties",
		slot: "panties",
		dim(){ return {
			curveBotY: 2,
			curveTopY: -3,
			genCoverage: 0.9150537634408602,
			waistCoverage: 0.08924731182795687,
			waistCoverageLower: -0.020860215053763564,
			/*
			curveCleavageX: 13,
			curveCleavageY: -12,
			 waistCoverage: 0.22688172043010746,
			waistCoverageLower: -0.24107526881720442,
			radius: 3.900000000000001,
			*/
		}},
		fill: exgar.upper_fill,
	},
	
	
	
	sluttyBikiniTop: {
		dad: "wBikiniTop",
		slot: "bra",
		dim(){ return {
			curveCleavageY: -17,
			innerNeckCoverage: 0.22607131846675554,
			outerNeckCoverage: 0.43653273044908025,
			radius: 1.4000000000000001,
		}},
		fill: exgar.upper_fill,
	},

	sluttyBikiniBottom: {
		dad: "wBikiniBottom",
		slot: "panties",
		dim(){ return {
			curveBotY: 1,
			curveTopY: -5,
			genCoverage: 0.8049462365591398,
			waistCoverage: 0.1,
			waistCoverageLower: -0.04838709677419373,
		}},
		fill: exgar.upper_fill,
	},

//SHORT SKIRTS
	shortSkirt0 : {
		name: "skirt",
		dad: "wSuperSkirt",
		slot:"lower",
		dim(){ return {
			button: 2,
			innerLoose: 1.1,
			legCoverage: 0.5,
			outerLoose: 1.4,
			waistCoverage: 0.45,
		}},
		fill: exgar.lower_fill,
	}, 
	
	shortSkirt1 : {
		name: "short skirt",
		dad: "wSuperSkirt",
		slot:"lower",
		dim(){ return {
			button: 0,
			innerLoose: 1.1,
			legCoverage: 0.4,
			outerLoose: -0.1,
			waistCoverage: 0.45,
		}},
		fill: exgar.lower_fill,
	}, 
	
	shortSkirt2 : {
		name: "miniskirt",
		dad: "wSuperSkirt",
		slot:"lower",
		dim(){ return {
			button: 0,
			innerLoose: 1.1,
			legCoverage: 0.3,
			outerLoose: -0.1,
			waistCoverage: 0.3,
		}},
		fill: exgar.lower_fill,
	}, 
	
	shortSkirt3 : {
		name: "microskirt",
		dad: "wSuperSkirt",
		slot:"lower",
		dim(){ return {
			button: 0,
			innerLoose: 1.1,
			legCoverage: 0.2,
			outerLoose: -0.07,
			waistCoverage: 0.2,
		}},
		fill: exgar.lower_fill,
	},
	
	shortJeansShorts3 : {
		name: "shorts",
		dad: "wJeans",
		slot:"lower",
		dim(){
			return {
				waistCoverage: 0.2,
				legCoverage: 0.27,
				innerLoose: 0,
				outerLoose: 0,
				open: 0,
				buttonColor: "orange",
				thickness: [0.9, 1],
		}},
		fill: "jeans",
		highlight: "red",
	},


//MAID
	maidUpper : {
		name: "maid blouse",
		dad: "wMiniDress2",
		slot:"upper",
		dim(){
			return {
				//alpha: 0.9,
				innerNeckCoverage: [0.8, 1],
				legCoverage: 0.0,
		}},
		fill: "nearlyBlack",
	},
	maidLower : {
		name: "maid skirt",
		dad: "wSuperSkirt",
		slot:"lower",
		dim(){
			return {
				legLoose: 0.5,
				bustle: true, 
		}},
		fill: "nearlyBlack",
	},
	
	maidHat : {
		name: "maid hat",
		dad: "wMaidHeadpiece",
		slot:"hat",
		dim(){
			return {
		}},
		fill: "nearlyWhite",
		stroke: "nearlyBlack",
	},
	
	maidApron : {
		name: "maid apron",
		dad: "wApron",
		slot:"belt",
		dim(){
			return {
		}},
		fill: "nearlyWhite",
		stroke: "nearlyBlack",
	},
	
	
//CHOKER
	improvisedChoker : {
		name: "improvised choker",
		slot:"collar",
		dad: "wChoker",
		dim(){ 
				return {
					neckBotCoverage: 0.1,
					neckCoverage: 0.3,
					thickness: 0.2,
				}; 
		},
		fill: "khaki",
	},
	
	leatherChoker : {
		name: "choker",
		slot:"collar",
		dad: "wChoker",
		dim(){ 
				return {
					center: true,
					neckBotCoverage: 0,
					neckCoverage: [0.15,0.22],
					thickness: [0.4,0.7],
				}; 
		},
		fill: "darkLeather",
	},
	
	
//WRESTLING
	wrestlingBot : {
		name: "bottom",
		slot:"panties",
		dad: "wSuperPanties",
		dim(){ return {
			waistCoverage: 0.15,
			waistCoverageLower: -0.2,
			
		}},
		fill: "royalBlue",
	},
	
	wrestlingTop : {
		name: "top",
		dad: "wHalterTop",
		slot:"upper",
		dim(){
			return {
				cleavageCoverage: 0.27,
				innerNeckCoverage: 0,
				waistCoverage: 1.4,	
				
			};
		},
		fill: "royalBlue",
	},
	
	redWrestlingBot : {
		name: "bottom",
		slot:"panties",
		dad: "wSuperPanties",
		dim(){ return {
			waistCoverage: 0.15,
			waistCoverageLower: -0.2,
			
		}},
		fill: "burgundy",
	},
	
	redWrestlingTop : {
		name: "top",
		dad: "wHalterTop",
		slot:"upper",
		dim(){
			return {
				cleavageCoverage: 0.27,
				innerNeckCoverage: 0,
				waistCoverage: 1.4,	
				
			};
		},
		fill: "burgundy",
	},
	
	
//BDSM GEAR
	bdsmCorset : {
		name: "corset",
		dad: "wCorset", 
		slot:"vest",
		dim(){ return{	
		}},
		fill: ["darkLeather"],
		highlight: "darkGrey",
	}, 
	
	bdsmThong : {
		name: "thong",
		subtype: "thong",
		slot:"panties",
		dad: "wSuperPanties",
		dim(){ 
			const top = rg.range(0.0, 0.2);
			//let bot = top - between(0.06,0.18); 
			return {
				waistCoverage: top,
				waistCoverageLower: [top - 0.1, top -0.2],
				genCoverage: [0.4,0.7],
				curveTopX: [-2,-3],
				curveTopY: [-2,-6],
				bow: rg.g < 0.3,
				ruffling: rg.g < 0.3,
		}},
		fill: "darkLeather",
	},
	
	bdsmGloves : {
		name: "gloves",
		slot:"gloves",
		dad: "wLongGloves",
		dim(){ return {
			armCoverage: [0.3, 0.4],
		}},
		fill: "nearlyBlack",
	},
	
	bdsmBoots :{
		name: "boots",
		dad: "wHighSneakers",
		slot: "shoes",
		dim(){return {
			tongue: [4, 7],
			tip: [-4,1],
			legCoverage: [0.3, 0.4],
		}},
		fill: ["darkLeather"],
		highlight: "darkGrey",
	},
	
	bdsmGorget : {
		name: "gorget",
		dad: "wGorget",
		slot: "neck",
		dim(){ 
			return{
				cleavageTopCoverage: 0.015609906484431058,
				neckBotCoverage: 0.6469941424314047,
				neckTopCoverage: -0.14223615250231225,
		}},
		fill: ["darkLeather"],
		highlight: "darkGrey",
	},
	
	leatherMinidress : {
		name: "minidress",
		dad: "wMiniDress2",
		slot: "upperLower",
		dim(){ 
			return{
				legCoverage: 0.18,
		}},
		fill: "darkLeather",
		highlight: "darkGrey",
	},

	
	latexMinidress : {
		name: "minidress",
		dad: "wMiniDress2",
		slot: "upperLower",
		dim(){ 
			return{
				legCoverage: 0.18,
		}},
		fill: "nearlyBlack",
		highlight: "darkGrey",
	},


	bdsmStockings : {
		name: "stockings",
		dad: "wStockingsGarter",
		slot:"socks",
		dim(){ return{
			bandWidth: 5,
			  beltWidth: 7.5,
			  legCoverage: 0.25,
			  waistCoverage: 0.2,	
		}},
		fill: "nylon",
		highlight: "nearlyBlack",
	}, 
	
	bdsmChoker : { //TODO same as leatherChoker
		name: "choker",
		slot:"collar",
		dad: "wChoker",
		dim(){ 
				return {
					center: true,
					neckBotCoverage: 0,
					neckCoverage: [0.15,0.22],
					thickness: [0.4,0.7],
				}; 
		},
		material: "darkLeather",
	},
	
	
	bdsmHeels :{  //TODO - just copied basic heels
		name: "high heels",
		dad: "wClosedToeStrappedPumps",
		slot: "shoes",
		dim(){return {
			straps: rg.array([0,0,1,1,1,2,2,3,3,4,5,6,7]),
			space: [3, 6],
			shoeHeight: [1, 4],
			toeCoverage: [0, 1],
			shoeTightness: [8, 12],
		}},
		fill: "nearlyBlack",
	},
	

	bdsmCatsuit :{
		name: "catsuit",
		dad: "Catsuit",
		slot: "upperLower",
		dim(){return {
		}},
		fill: "nearlyBlack",
		highlight: "darkGrey",
	},
	
	
	slaveCollar : {
		name: "collar",
		slot:"collar",
		dad: "Collar",
		dim(){ 
				return {
				}; 
		},
		
		fill: "darkLeather", //TODO 
		//highlight: "monochromatic",
	},
	
	
	
	
	whiteTightTee : {
		name: "tee",
		dad: "wTee",
		slot:"upper",
		subtype: "tee",
		dim(){ return {
			armCoverage: [0.12, 0.45],
			cleavageOpeness: [0.1, 0.36],
			curveCleavageX: [4, 14],
			curveCleavageY: [-5, 0],
			cleavageCoverage: [0.1, 0.2],
			waistCoverage: [0.0, 0.4],
			
		}},
		fill: "lighterWhite",
	}, 
	
	
	whitePants : {
		name: "pants",
		dad: "wSuperLeggins",
		slot:"lower",
		dim(){
			const degree = rg.g;
			return {
				waistCoverage: [0.0, 0.3],
				legCoverage: [0.9, 1.00],
				innerLoose: 0.1 + 0.4 * degree,
				outerLoose: 0.2 + 0.7 * degree,
				thickness: [0.7, 0.8],
		}},
		fill: "darkerWhite",
	}, 
	
	
	cybrexPants : {
		name: "pants",
		dad: "SuperLeggings",
		slot:"lower",
		dim(){	return {
			beltWidth: 0,
			highlight: "#f09b00",
			fill: "#333333",
			legCoverage: 0.11,
			waistCoverage: 0.15,
		}},
	}, 
	
	cybrexTee : {
		name: "tee",
		dad: "ClearSuperTee",
		slot:"upper",
		dim(){	return {
			armCoverage: 0.17,
			cleavageCoverage: 0.2,
			waistCoverage: 0.33,
			waistCoverageBias: 0.15,
			fill: "#f09b00",
			highlight: "#333333",
		}},
	}, 


	
	chardBikiniTop: {
		dad: "BikiniTop",
		slot: "bra",
		dim(){ return {
			innerNeckCoverage: 0.15,
			outerNeckCoverage: 0.25,
			radius: 2,
			fill: "#4cd900", //TODO
		}},
	},
	
	chardBikiniBot: {
		dad: "SuperPanties",
		slot: "panties",
		dim(){ return {
			bow: false,
			waistCoverage: 0.1,
			waistCoverageLower: -0.55,
			fill: "#3fa707", //TODO
		}},
	},
	
	chardTrunks: {
		dad: "SuperLeggings",
		slot: "panties",
		dim(){ return {
			beltWidth: 0,
			innerLoose: 0.3,
			lampasse: 0,
			lampasseOut: 0,
			legCoverage: 0.22,
			waistCoverage: 0.15,
			fill: "#3fa707", //TODO
		}},
	},
	

}
	

	
//STORY
	
	
	/*
		VibPanties : {
		name: "vibrating panties",
		slot:"panties",
		dad: "wSuperPanties",
		dim: (function(){ return {
			waistCoverage: 0.15,
			waistCoverageLower: -0.2,
			light: 10,
			satur:50,
			hue:0,
			thickness: 0.3,
		}})
	},
	*/
	
//TODO!!!