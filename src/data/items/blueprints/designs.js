import {exgar, rg} from "./exgar";

export const designs = {
	
	
	designPanties : {
		name: "panties",
		subtype: "hipsters",
		slot:"panties",
		dad: "SuperPanties",
		note: "combo of briefs, boyshorts and hipsters",
		dim(){ return {
			thickness: 0.4,
			waistCoverage: [0, 0.4],
			waistCoverageLower:[-0.7, 0],
			genCoverage:1,
			topCurve: [-5, -1],
			bow: false,
			ruffling: rg.g < 0.34,
			rufflingThickness: [0.6, 1.2],
			rufflingLength: [1.7, 2.7],
			topBandWidth: [0, 7],
		}},
		fill: exgar.underwear_fill,
	},
	
	
	designThong : {
		name: "thong",
		subtype: "thong",
		slot:"panties",
		dad: "SuperPanties",
		dim(){ 
			return {
				waistCoverage: [0, 0.35],
				waistCoverageLower: [-0.2, 0.3],
				genCoverage: [0.4,0.7],
				topCurve: [-9, -4],
				bow: false,
				ruffling: rg.g < 0.3,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				curveBotX: [-4, 3],
		}},
		fill: exgar.underwear_fill,
	},
	
	
		
	designCageBra : {
		name: "cage bra",
		slot:"bra",
		dad: "CageBra",
		dim(){ 
			const fancy = rg.b;
			return {
				thickness: [0.4,0.6],
				neckCoverage: [0.45,0.9],
				strapWidth: [0.7,1.5],
				topStrapWidth: [0.9, 1.6],
				botStrapCoverage: [0.7, 2],
				cageSpacing: [1.3, 2.2],
				
				cageCenter: fancy,
				cageCross: !fancy,
				cageDouble: rg.g < 0.5,
				
				ruffling: false,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
			}
		},
		fill: exgar.underwear_fill,
	},
	
	designPushUpBra : {
		name: "push-up bra",
		slot:"bra",
		dad: "PushUpBra",
		dim(){ 
			return {
				thickness: 0.9, //has to be the same
				strapWidth: 0.9, //has to be the same
				showStrap: true,
				neckCoverage: [0.5,0.95],
				botStrapCoverage: [0.7, 2],
				ruffling: rg.b,
				rufflingThickness: [0.6, 1.2],
				rufflingLength: [1.7, 2.7],
				cupCoverage: [-1, 0.5],
			};
		},
		fill: exgar.underwear_fill,
	},
	
	
	
	
	
	designTubeTop : {
		name: "tube top",
		dad: "TubeTopSleeves",
		slot:"upper",
		dim(){ return {
				armCoverage: [0, 1],
				armLoose: [0, 0.7],
				shoulderCoverage: [-0.15, 0],
				
				chestCoverage: [0.3, 0.8],
				waistCoverage: [-0.4, 0.8],
			};
		},
		fill: exgar.upper_fill,
	},
	
	designSexyTubeTop : {
		name: "slutty tube top",
		dad: "TubeTopSleeves",
		slot:"upper",
		dim(){ return {
				alpha: [0.5, 1],
				
				armCoverage: [0, 1],
				armLoose: [0, 0.7],
				shoulderCoverage: [-0.15, 0],
				
				chestCoverage: [0.1, 0.4],
				waistCoverage: [0, 1.8],
				
				topStrap: rg.g < 0.25,
			};
		},
		fill: exgar.upper_fill,
	},
	
	
	
	
	designHalterTop : {
		name: "halter top",
		dad: "HalterTop",
		slot:"upper",
		dim(){
			return {
				cleavageCoverage: [0, 0.3],
				innerNeckCoverage: [-0.25, 0.25],
				outerNeckCoverage: [0, 0.8],
				waistCoverage: [-0.4, 0.8],	
				
				curveCleavageY: 0,
				curveCleavageX: [6, 11],
			};
		},
		fill: exgar.upper_fill,
	},
	
	designSexyHalterTop : {
		name: "sexy halter top",
		dad: "HalterTop",
		slot:"upper",
		dim(){
			return {
				alpha: [0.5, 1],
				cleavageCoverage: [0, 0.8],
				innerNeckCoverage: [-0.25, 0.25],
				outerNeckCoverage: [0, 1],
				waistCoverage: [-0.2, 1.7],
				
				curveCleavageY: [0, -6],
				curveCleavageX: [6, 11],
			}
		},
		fill: exgar.upper_fill,
	},
	
	/*
	designSluttyHalterTop : {
		name: "slutty halter top",
		dad: "wHalterTop",
		slot:"upper",
		dim(){
			return {
				cleavageCoverage: [0.2, 0.7],
				curveCleavageY: [0, -6],
				curveCleavageX: [6, 11],
				
				innerNeckCoverage: [0, 0.15],
				outerNeckCoverage: [0.05, 0.5],
				
				waistCoverage: [-0.2, 1.5],
			};
		},
		fill: exgar.upper_fill,
	},
	*/
	
	designTee : {
		name: "tee",
		dad: "Tee",
		slot:"upper",
		subtype: "tee",
		note: "combination of tightTee and longTee",
		dim(){ return {
			armCoverage: [0.12, 1.05],
			cleavageOpeness: [-0.4, 0.4],
			curveCleavageX: [4, 14],
			curveCleavageY: [-5, 0],
			cleavageCoverage: [0.01, 0.25],
			waistCoverage: [-0.4, 0.5],
			
		}},
		fill: exgar.upper_fill,
	},
	
	designSexyTee : {
		name: "sexy tee",
		dad: "ClearSuperTee",
		slot:"upper",
		subtype: "tee",
		note: "variant of sluttyTee",
		dim(){ return {
			alpha: [0.5, 1],
			armCoverage: [0, 1.05],
			cleavageOpeness: [-0.4, 1.4],
			curveCleavageX: [4, 14],
			curveCleavageY: [-6, 0],
			cleavageCoverage: [0.2, 0.8],
			waistCoverage: [0, 1.5],
			armInnerCoverage: 1,
			
		}},
		fill: exgar.upper_fill,
	},
	
	
	designSkirt : {
		name: "skirt",
		dad: "SuperSkirt",
		slot:"lower",
		dim(){ return {
			button: rg.g < 0.25,
			bustle: rg.g < 0.25,
			innerLoose: 1.1,
			
			outerLoose: [0, 1.6],
			waistCoverage: [0.1, 0.7],
			legCoverage: [0.25, 0.75],
		}},
		fill: exgar.lower_fill,
	}, 
	
	designMicroSkirt : {
		name: "miniskirt",
		dad: "SuperSkirt",
		slot:"lower",
		dim(){ return {
			bustle: false,
			button: false,
			innerLoose: 1.1,
			
			outerLoose: [0, 1.6],
			waistCoverage: [-0.1, 0.25],
			legCoverage: [0.1, 0.36],
		}},
		fill: exgar.lower_fill,
	}, 
	
}