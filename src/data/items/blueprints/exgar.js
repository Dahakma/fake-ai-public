import {rg} from "Libraries/random";
export {rg};

export const exgar = {
	min_transparency: 0.7,
	max_transparency: 0.9,
	
	transluncent: [0.85, 0.95],
	transparent: [0.65, 0.85],
	
	major_magic_1: [
		{str: 1},
		{dex: 1},
		{int: 1},
		{char: 1},	
	],
	
	minor_magic_1: [
		{evasion: 5},
		{lockpick: 5},
		{hp: 10},
		{eng: 10},
		{presence: 5},
		{presence: -5},
		{reflex: 5},
		{reflex: -5},
	],
	
	magic_1: [
		{str: 1},
		{dex: 1},
		{int: 1},
		{char: 1},
		
		{evasion: 5},
		{lockpick: 5},
		{hp: 10},
		{eng: 10},
		{presence: 5},
		{presence: -5},
		{reflex: 5},
		{reflex: -5},
		
		{evasion: 5},
		{lockpick: 5},
		{hp: 10},
		{eng: 10},
		{presence: 5},
		{presence: -5},
		{reflex: 5},
		{reflex: -5},
		
	],
	
	/*
		"bright" 
		- bright colors 
		- hue: 0 - 360
		- saturation: 85 - 100
		- lightness: 45 - 55
		
		"manlyBright"
		- bright without pink-red girly colurs
		- hue: 0 - 250
		
		"light" //TODO - probably rename pastel
		- pastel colors with lover saturation
		- hue: 0 - 360
		- saturation: 60 - 80
		- lightness: 60 - 80
		
		"dark"
		- darker but still distinktive colours 
		- hue: 0 - 360
		- saturation: 55 - 85
		- lightness: 20 - 45
		
		"full"
		- intense but not too dark nor too bright colours
		- hue: 0 - 360
		- saturation: 70 - 90
		- lightness: 40 - 55
		
		
		"nearlyBlack"
		- very dark grey with faint tint
		- hue: 0 - 360
		- saturation: 5 - 15
		- lightness: 10 - 15 
		
		"nearlyWhite"
		- very light grey with faint tint
		- hue: 0 - 360
		- saturation: 5 - 15
		- lightness: 90 - 95 
		
		"grey",
		- hue: 0
		- saturation: 0
		- lightness: 5 - 95 
		
		"darkGrey",
		- lightness: 5 - 35 
		
		"lightGrey",
		- lightness: 65 - 95
		
		"midGrey",
		- lightness: 30 - 70
		
		"cute"
		- pastel light pink-white shades
		- hue: 325 - 345
		- saturation: 50 - 100
		- lightness: 70 - 90
		
		"barbie"
		- bright pink-purple shades
		- hue: 295 - 335
		- saturation: 75 - 100
		- lightness: 40 - 60
		
		"tintedWhite"
		- white color with slight tint
		- hue: 0 - 360
		- saturation: 30 - 70
		- lightness: 90 - 95
		
		"tintedBlack"
		- lightness: 10 - 15
	
		"tintedLight" //TODO - STUPID NAME  
		- lightness: 80 - 95
		
		"tintedDark" //TODO - STUPID NAME 
		- lightness: 10 - 25
	*/

	
	//MATERIALS
	//made of metal
	//irl_precious_metal: ["irlSilver", "irlSilver", "irlSilver", "irlSilver", "irl14Gold", "irl14Gold", "irl18Gold", "irl24Gold", "irlRoseGold"],
	irl_precious_metal: ["irlSilver", "irlSilver", "irlSilver", "irlSilver", "irl14Gold", "irlGold", "irlGold", "irlGold", "irlRoseGold"],
	precious_metal: ["gold", "silver", "silver", "silver", "electrum", "electrum", "roseGold"],
	rare_leather: ["wyve","hexa","wyve","hexa","levi","levi"],
	leather: ["leather","darkLeather","leather"],
	bland_cloth: ["linen","linen","hemp","hemp","nettle"],
	
	//COLORS
	leather_fill: ["leather","darkLeather","leather"],
	rare_leather_fill: ["wyve","hexa","wyve","hexa","levi"],
	fallen_fill: ["purple","carmine","indigo","pure"/*"pure","purple","carmine","indigo","pure"*/],
	tunic_fill: ["light","dirtyWhite","light","dirtyWhite","hemp","cloth","hemp","cloth"],
	
	manly_upper_fill: ["manlyDark","manlyFull","manlyBright","manlyLight","manlyBright","manlyLight"], //TODO
	upper_fill: ["dark","full","bright","bright","light","light"], //TODO
	highlight_fill: [
		"stroke", "stroke",
		"darkGrey","nearlyBlack", "grey",
		"monochromatic", "monochromatic",
	],
	fantasy_upper_fill: ["dark","full","bright","bright","light","light"], //TODO
	lower_fill: ["dark","full","bright","darkGrey"], //TODO
	dark_lower_fill: ["dark","dark","full","darkGrey"], //TODO
	
	//metal jewellery
	jew_metal_fill: ["NXgold","NXsilver","NXsteel"], //TODO BRASS
	//metal parts of clothes; buckles etc. 
	util_metal_fill: ["NXgold","NXsilver","NXsteel","NXbronze"], //TODO BRASS
	
	//fantasy_underwear_fill: ["dark"],
	//fantasy_male_underwear_fill: ["manlyDark"],
	
	light_fill: ["full", "light", "bright", "tintedLight", "nearlyWhite"],
	dark_fill:  ["dark", "darkGrey", "tintedDark", "nearlyBlack"],
	
	//socks
	socks_fill: ["dark", "full", "darkGrey", "grey"],
	neutral_socks_fill: ["grey", "darkGrey", "manlyDark", "manlyDark", "tintedDark", "nearlyBlack"],
	
	
	bland_fill: ["linen","hemp"/*"linen","gray","dirtyWhite"*/],
	
	bimbo_fill: ["cute","cute","barbie","barbie"],
	bland_underwear_fill: [
		"grey",
		"tintedLight",
		"tintedDark",
		"nearlyWhite",
		"nearlyBlack",
		"light",
		"light",
		"full",
		"full",
		"dark",
		"dark",
	],
	underwear_fill: [
		"nearlyWhite",
		"nearlyBlack",
		"light",
		"full",
		"dark",
		"bright",
	],
	male_underwear_fill: [
		"nearlyWhite",
		"darkGrey",
		"nearlyBlack",
		"light",
		"full",
		"dark",
		"bright",
		//"dirtyWhite","manlyLight","manlyFull","manlyDark","darkGrey"
	],
	stockings_fishnet: [
		["fishnet_1", 39, 69],
		["fishnet_4", 20, 35],
		["fishnet_5", 35, 55],
		["fishnet_6", 30, 50],
	],
	dense_fishnet: [ //TODO
		["fishnet_1", 39, 69],
		["fishnet_4", 20, 35],
		["fishnet_5", 35, 55],
		["fishnet_6", 30, 50],
	],
	striped_dark: [
		["emo",50,100],
		["anarchy",50,100],
		["witch",50,100],
	], 
	
	striped_bright: [
		["bumblebee",50,100],
		["cyany",50,100],
		["forest",50,100],
		["domino",50,100],
		["lgbt",25,50],
		["jamaica",25,50],
		["africa",25,50],
		["indonesia",25,50],
	],
	
	striped_all: [
		["emo",50,100],
		["anarchy",50,100],
		["witch",50,100],
		
		["bumblebee",50,100],
		["cyany",50,100],
		["forest",50,100],
		["domino",50,100],
		["lgbt",25,50],
		["jamaica",25,50],
		["africa",25,50],
		["indonesia",25,50],
	], 
	
	core_fishnet: [
		["fishnet_2", 18, 25],
		["fishnet_3", 15, 20],//TODO ugly
		["fishnet_4", 15, 20],
		["fishnet_5", 18, 23],
		["fishnet_6", 20, 25],
	],
	
	mid_fishnet: [
		["fishnet_2", 20, 30],
		["fishnet_3", 20, 30],
		["fishnet_4", 20, 30],
		["fishnet_5", 20, 30],
		["fishnet_6", 20, 30],
	],
	
	bronze: [
		["copper", 100],
	],
	
	lower_tex: [	
		["leopard", 120],
		["leopard", 120],
		["snake", 50, 70],
		["camo", 100],
	],
	
	animal: [
		["zebra", 110],
		["leopard", 100, 150],
		["leopard", 100, 150],
	],
	
	
	
	get dual_shirt_arms(){
		let armCoverage = 0;
		let armCoverage2 = 0;
		
		const rnd = rg.g;
		//tiny dark sleeves
		if(rnd < 0.2){
			armCoverage = rg.range(0.15, 0.25);
		//long dark
		}else if(rnd < 0.5){
			armCoverage = rg.range(0.2, 0.7);
		//long with dark cuffs
		}else if(rnd < 0.8){
			armCoverage2 = rg.range(0.2, 0.4);
			armCoverage = armCoverage2 + rg.range(0.15, 0.2);
		//medium sleeves with long dark
		}else{
			armCoverage2 = rg.range(0.2, 0.4);
			armCoverage = armCoverage2 + rg.range(0.3, 0.5);
		}
		
		return{
			armCoverage,
			armCoverage2,
		}
	},
	
	get super_shirt_arms(){
		/*
		switch(rg.array([])){
			case 1: //long dark
				return {
					
					
					
				}
				
			case 
			
			
		}
		*/
		
		let armCoverage = 0;
		let armInnerCoverage = 0;
		
		const rnd = rg.g;
		
		//tiny dark sleeves
		if(rnd < 0.2){
			armCoverage = rg.range(0.15, 0.25);
		//long dark
		}else if(rnd < 0.5){
			armInnerCoverage = rg.range(0.2, 0.7);
		//long with dark cuffs
		}else if(rnd < 0.8){
			armInnerCoverage = rg.range(0.2, 0.4);
			armCoverage = armInnerCoverage + rg.range(0.15, 0.2);
		//medium sleeves with long dark
		}else{
			armInnerCoverage = rg.range(0.2, 0.4);
			armCoverage = armInnerCoverage + rg.range(0.3, 0.5);
		}
		
		return{
			armCoverage,
			armInnerCoverage,
		}
	},
}