//basically everything based on "data/armory"
import {exgar} from "./exgar";
import {rg, ra} from "Libraries/random";

export const weapons = {
	
//BLADES
	knife : {
		slot: "weapon",
	}, 
	
	engravedKnife : {
		subtype: "knife",
		adjective: "engraved",
		slot: "weapon",
	}, 
	
	falchion : {
		slot: "weapon",
	}, 
	
	rustyFalchion: {
		subtype: "falchion",
		adjective: "rusty",
		slot: "weapon",
	}, 
	
	sharpFalchion: {
		subtype: "falchion",
		adjective: "sharp",
		slot: "weapon",
	}, 
	
	cutlass : {
		slot: "weapon",
	}, 
	
	rustyCutlass : {
		subtype: "cutlass",
		adjective: "rusty",
		slot: "weapon",
	}, 
	
	sharpCutlass : {
		subtype: "cutlass",
		adjective: "sharp",
		slot: "weapon",
	}, 
	
	masterworkCutlass : {
		subtype: "cutlass",
		adjective: "masterwork",
		slot: "weapon",
	}, 
	
	sword : {
		slot: "weapon",
	}, 
	
	rustySword : {
		subtype: "sword",
		adjective: "rusty",
		slot: "weapon",
	}, 
	
	sharpSword : {
		subtype: "sword",
		adjective: "sharp",
		slot: "weapon",
	}, 
	
	masterworkSword : {
		subtype: "sword",
		adjective: "masterwork",
		slot: "weapon",
	},
	
	icefireSword: {
		slot: "weapon",
		att: {
			weapon_fire: 30,
			weapon_ice: 15,
		},
	},
	
//HAMMERS
	bludgeon : {
		slot: "weapon",
	}, 
	
	crackedBludgeon : {
		subtype: "bludgeon",
		adjective: "cracked",
		slot: "weapon",
	}, 
	
	heavyBludgeon : {
		subtype: "bludgeon",
		adjective: "heavy",
		slot: "weapon",
	}, 
	
	mace : {
		slot: "weapon",
	}, 
	
	crackedMace : {
		subtype: "mace",
		adjective: "cracked",
		slot: "weapon",
	}, 
	
	heavyMace : {
		subtype: "mace",
		adjective: "heavy",
		slot: "weapon",
	}, 
	
	morningstar : {
		slot: "weapon",
	}, 
	
	crackedMorningstar : {
		subtype: "morningstar",
		adjective: "cracked",
		slot: "weapon",
	}, 
	
	heavyMorningstar : {
		subtype: "morningstar",
		adjective: "heavy",
		slot: "weapon",
	}, 
	
	
//POLEARMS
	spear : {
		slot:"weapon",
	}, 	
	
	bentSpear : {
		subtype: "spear",
		adjective: "bent",
		slot:"weapon",
	}, 	
	
	sharpSpear : {
		subtype: "spear",
		adjective: "sharp",
		slot:"weapon",
	}, 	
	
	
	scythe : {
		name: "war scythe",
		slot:"weapon",
	}, 	
	
	bentScythe : {
		name: "war scythe",
		subtype: "scythe",
		adjective: "bent",
		slot:"weapon",
	},
	
	sharpScythe : {
		name: "war scythe",
		subtype: "scythe",
		adjective: "sharp",
		slot:"weapon",
	}, 
	
	poleaxe : {
		slot:"weapon",
	}, 	
	
	bentPoleaxe : {
		subtype: "poleaxe",
		adjective: "bent",
		slot:"weapon",
	},
	
	heavyPoleaxe : {
		subtype: "poleaxe",
		adjective: "heavy",
		slot:"weapon",
	},
	
	
//BOWS
	bow : {
		name: "hunting bow",
		slot: "weapon",
	},
	
	warbow : {
		name: "war bow",
		slot: "weapon",
	},
	
	elvenbow : {
		name: "elven bow",
		slot: "weapon",
	},
	
	dragonbow : {
		name: "dragon bow",
		slot: "weapon",
	},
	
	
//WANDS
	fireDagger: {
		name: "ritual dagger",
		subtype: "dagger",
		slot: "weapon",
		condit(subject){
			return subject.perks.include("mage");
		},
		att: {
			fire: 10,
		},
	},
	
	iceDagger: {
		name: "ritual dagger",
		subtype: "dagger",
		slot: "weapon",
		condit(subject){
			return subject.perks.include("mage");
		},
		att: {
			ice: 10,
		},
	},
	
	windDagger: {
		name: "ritual dagger",
		subtype: "dagger",
		slot: "weapon",
		condit(subject){
			return subject.perks.include("mage");
		},
		att: {
			wind: 10,
		},
	},
	
	fireWand: {
		name: "wand",
		slot: "weapon",
		subtype: "wand",
		condit(subject){
			return subject.perks.include("mage");
		},
		att: {
			fire: 10,
		},
	},
	
	iceWand: {
		name: "wand",
		slot: "weapon",
		subtype: "wand",
		condit(subject){
			return subject.perks.include("mage");
		},
		att: {
			ice: 10,
		},
	},
	
	windWand: {
		name: "wand",
		slot: "weapon",
		subtype: "wand",
		condit(subject){
			return subject.perks.include("mage");
		},
		att: {
			wind: 10,
		},
	},
	/*
	
	
	wand : {
		name: "magic wand",
		id: 4005,
		slot:"weapon",
		price: 13,
		attack: 1,
		type: 4,
		mag: 1,
		
		class: 3,
	},
	*/
	

	
}