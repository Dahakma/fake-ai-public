//import {shuffle} from "Libraries/dh";
import {garment} from "Data/items/blueprints/index";
import {rg, ra} from "Libraries/random";

/*
function select(array, count = 1){ //randomly reduces array to the set number
	while(array.length < count){
		array = [...array, ...array];
	}
	ra.shuffle(array);
	array.length = count;
	return array;
}
*/

export const fantasy = {
//DEFAULT CLOTHES
	harlot: [
		[1,"harlotCorset"],
		[1,"harlotTop"],
		[1,"harlotStockings"],
		[1,"harlotSkirt"],
	],
	
	barbarian: [
		[1,"barbaricBra"],
		[1,"barbaricLoincloth"],
		[1,"fancyBarbaricBra"],
		[1,"fancyBarbaricLoincloth"],
	],
	
	fallen: [
		[1,"fallenTop"],
		[1,"fallenLoincloth"],
	],
	

//CLOTHES
	get clothes_default(){
		return [
			...this.harlot,
			...this.barbarian,
			...this.fallen,
		];
	},
	
	clothes: [
		[1,"fBlouse"],
		[1,"fPolyBlouse"],
		[1,"fShirt"],
		[1,"fPolyShirt"],
		//[1,"fBikini"], //TOOD
		
		[1,"fLeggings"],
		[1,"fPants"],
		[1,"maidSkirt"],
		[1,"fFancyLeggings"],
		
		[1,"fBlandPaties"],
		[1,"fBlandThong"],
		[1,"fBlandBoxers"],

		[1,"fPaties"],
		[1,"fThong"],
		[1,"fBoxers"],
	],
	
	clothes_common: [
		[1,"fBlandPaties"],
		[1,"fBlouse"],
		[1,"fPolyBlouse"],
		[1,"fLeggings"],
	],

	clothes_rare: [ //TODO
		"maidSkirt",
		"fBlandPaties",
		[1,"fShirt"],
		[1,"fPolyBlouse"],
		[1,"fLeggings"],
		[1,"fFancyLeggings"],
	],
	
	clothes_fancy: [
		"fFancyLeggings",
		"nobleGown",
	],


//ACCESSORY
	accessory: [
		"fChoker",
		"fLeatherChoker",
		"fDarkWideBelt", 
		"fDarkThinBelt", 
		"fPolyWideBelt", 
		"fPolyThinBelt",
		//[2,"fallenGorget"],
	],
	
	accessory_belts: [
		"fDarkWideBelt", 
		"fDarkThinBelt", 
		"fPolyWideBelt", 
		"fPolyThinBelt",
	],
	
	accessory_fancy: [
		"fFancyChoker", 
		"fMetalChoker", 
		"fFancyThinBelt", 
		"fFancyWideBelt",
	],


//JEWELLERY 
	jewellery_low: [
		"fFancyChoker", 
		"fMetalChoker",
	],
	
	jewellery: [
		"fMultiNecklace", 
		"fTearNecklace", 
		"fMetalChoker",
	],
	
	jewellery_magic: [
		"minorMagicTearNecklace", 
		"minorMagicTearNecklace", 
		"majorMagicTearNecklace",
	],


//ARMOR
	leather_low: [
		[1,"leatherVambraces"],
		[1,"leatherPauldrons"],
	],
	
	leather_high: [
		//[1,"leatherCuirass"], //TODO??
		[1,"leatherArmor"],
		[1,"leatherSkirt"],
	],
	
	leather_all: [
		//[1,"leatherCuirass"], //TODO??
		[1,"leatherArmor"],
		[1,"leatherSkirt"],
		[1,"leatherVambraces"],
		[1,"leatherPauldrons"],
	],
	
	fancyLeather_low: [
		[1,"fancyLeatherVambraces"],
		[1,"fancyLeatherPauldrons"],
	],
	
	fancyLeather_high: [
		[1,"fancyLeatherArmor"],
		[1,"fancyLeatherSkirt"],
	],
	
	fancyLeather_all: [
		[1,"fancyLeatherArmor"],
		[1,"fancyLeatherSkirt"],
		[1,"fancyLeatherVambraces"],
		[1,"fancyLeatherPauldrons"],
	],
	
	fancyLeather_magic: [
		[1,"fancyLeatherArmor"],
		[1,"fancyLeatherSkirt"],
		[1,"fancyLeatherVambraces"],
		[1,"fancyLeatherPauldrons"],
	],
	
	bronze_all: [
		"bronzeCuirass", 
		"bronzeGorget", 
		"bronzePauldrons", 
		"bronzeVambraces", 
		"bronzeGreaves",
	],
	
	bronze_magic: [
		"magicBronzeGreaves", 
		"magicBronzePauldrons", 
		"magicBronzeVambraces", 
		"magicBronzeGorget",
	],
	
	steel_low: [
		[1, "steelGorget"],
		[1, "steelPauldrons"],
		[1, "steelVambraces"],
		[1, "steelGreaves"],
	],
	
	steel_all: [
		[1, "steelGorget"],
		[1, "steelPauldrons"],
		[1, "steelVambraces"],
		[1, "steelGreaves"],
	],
	
	
//WEAPONS
	weapons_1_low: [
		[2,"knife"],
		"rustyFalchion", 
		"crackedBludgeon", 
		"bentSpear", 
	],
	weapons_1_mid: [
		"falchion",
		"bludgeon", 
		"spear",
		"bow",
	],
	weapons_1_high: [
		"sharpFalchion", 
		"heavyBludgeon", 
		"sharpSpear",
	],
	
	weapons_1: [ //TODO
		"rustyFalchion", "falchion", "sharpFalchion",
		"crackedBludgeon", "bludgeon", "heavyBludgeon",
		"bentSpear", "spear", "sharpSpear",
		"bow",
	],

	
	weapons_2_low: [
		"rustyCutlass",
		"crackedMace",
		"bentScythe",
	],
	
	weapons_2_mid: [
		"cutlass",
		"mace",
		"scythe",
		"warbow",
	],
	
	weapons_2_high: [
		"heavyMace", 
		"sharpCutlass", 
		"masterworkCutlass", 
		"sharpScythe", 
	],
	
	
	
	//TODO
	magic_dagger: ["fireDagger", "iceDagger", "windDagger"],
	magic_wand: ["fireWand", "iceWand", "windWand"],
	
//ELIXIRS 
	//TODO
	hp_eng: ["xHP","xEng","xHP","xEng","xHP","xEng"], 
	elixir_1: ["xElf", "xGob"],
	elixir_2: ["xElf", "xGob", "xMin", "xHum"],
	
//HARDCODED 
	gothred: [
		"iceDagger",
		"rustyCutlass",
		"crackedMace",
		"bentScythe",
		"bow",
	],
	
};