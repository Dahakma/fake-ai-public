/*

import {fantasy} from "./fantasy";
import {story} from "./story";
import {eShop} from "./eShop";
*/
import {stores} from "./stores";
import {test} from "./test";
import {initial} from "./initial";
import {fantasy} from "./fantasy";

export const lists = {
	...test,
	...stores,
	...initial,
	...fantasy,
	
	/*
	...initial,
	
	...story,
	...eShop,
	*/
}