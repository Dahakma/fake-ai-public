import {shuffle} from "Libraries/dh";
import {garment} from "Data/items/blueprints/index";
import {time} from "System/time";
const check = {
	get slut(){
		/*
			0 - prude
			1 - beauty
			2 - tease
			3 - slut 
			4 - whore
			5 - cumdup
		*/
		let slut = ext.slut; //based on sluttiness
		
		//if(slut === 0 && time.week >= 4) slut = 1;
		if(time.week >= 4) slut++; //TODO - is smart??????
		if(time.week >= 5) slut++;
		if(time.week >= 6) slut++;
		
		return ext.slut;
	},
	
	get is_slut(){
		return this.slut >= 2; //tease + slut
	},
	get is_shameless(){
		return this.slut >= 4; //whore + hyperslut
	},
	get long_lower(){ //alowed pants and long skirts
		return !(ext?.rules?.enforced && (ext?.rules?.short_skirt || ext?.rules?.show_legs));
	},
	get pants(){ //allowed pants and shorts 
		return !(ext?.rules?.enforced && ext?.rules?.short_skirt);
	},
	get socks(){ //allowed socks
		return !(ext?.rules?.enforced && ext?.rules?.no_socks);
	},
	get panties(){ //panties allowed
		return !(ext?.rules?.enforced && ext?.rules?.no_panties);
	},
	get only_sexy_panties(){ 
		return ext?.rules?.enforced && ext?.rules?.sexy_panties;
	},
	get sexy_panties_prefered(){ 
		return ext?.rules?.sexy_panties;
	},

	get only_sexy_tops(){ 
		return ext?.rules?.enforced && ext?.rules?.sexy_tops;
	},
	get sexy_tops_prefered(){ 
		return ext?.rules?.sexy_tops;
	},
	filter_sexy_tops(list){
		if(this.only_sexy_tops){
			return list.filter( ([n, blueprint]) =>
				//TODO - ADD ACTUAL FILTER 
				!["tee", "tightTee", "dualTee"].includes(blueprint),
			);
		}else{
			return list;
		}
	},
	
	
	filter_sexy_panties(list){
		if(this.only_sexy_panties){
			return list.filter( ([n, blueprint]) =>
				//TODO - ADD ACTUAL FILTER 
				!["simpleBriefs", "briefs", "simpleHipsters", "boyshorts"].includes(blueprint),
			);
		}else{
			return list;
		}
	},
	
	get bra(){ //allowed bra
		return !(ext?.rules?.enforced && ext?.rules?.no_bra);
	},
}


export const stores = {
	
	upper_alt(){
		let temp = [];
		if(check.is_shameless || (check.is_slut && check.sexy_tops_prefered) ){
			temp.push(
				[2,"blackNiceTee"],
				[2,"sheerTee"],
				[3,"fishnetTee"],
				[1,"fishnetShrugTop"],
				[1,"blackFishnetShrugTop"],
				[2,"blackCorsetTop"],
			);
		}else if(check.is_slut || check.sexy_tops_prefered){
			temp.push(
				[1,"blackTee"],
				[2,"blackNiceTee"],
				[2,"sheerTee"],
				[2,"fishnetShrugTop"],
				[1,"blackFishnetShrugTop"],
				[2,"blackCorsetTop"],
			);
		}else{
			temp.push(
				[2,"blackTee"],
				[2,"blackNiceTee"],
				[1,"fishnetShrugTop"],
				[1,"blackFishnetShrugTop"],
				[1,"blackCorsetTop"],
			);

		}
		temp = check.filter_sexy_tops(temp);
		return temp;
	},
	
	
	upper_second(){
		let temp = [];
		if(check.is_shameless || (check.is_slut && check.sexy_tops_prefered) ){
		temp.push(
				[1, "longTubeTop"],
				[1, "tubeTop"],
				[2, "sleevedTubeTop"],
				[1, "bimboTubeTop"],
				
				[2, "triangleTop"],
				
				[1, "longHalterTop"],
				[1, "halterTop"],
				
				[1, "turtleTop"],
				[1, "shrugTop"],
				
				[1,"tee"],
				[2,"tightTee"],
				[2,"niceTightTee"],
				[1,"bimboTee"],
				[1,"longTee"],
				[1,"dualTee"],
				[2,"fishnetDualTee"],
				
				[2,"sluttyTubeTop"],
				[2,"sluttyHalterTop"],
				[2,"deepHalterTop"],
				
				[1,"botOpenTop"],
				[1,"bimboBotOpenTop"],
				
				[2,"sluttyTee"],
				[2,"sluttyDualTee"],
				[1,"niceSluttyTee"],
				[1,"deepTee"],
				
				[2,"sheerTee"],
				[1,"fishnetTee"],
				[1,"stripedTightTee"], //
			);
		}else if(check.is_slut || check.sexy_tops_prefered){
			temp.push(
				[1, "longTubeTop"],
				[2, "tubeTop"],
				[2, "sleevedTubeTop"],
				[1, "bimboTubeTop"],
				
				[2, "triangleTop"],
				
				[1, "longHalterTop"],
				[2, "halterTop"],
				
				[1, "turtleTop"],
				[1, "shrugTop"],
				
				[1,"tee"],
				[2,"tightTee"],
				[2,"niceTightTee"],
				[1,"bimboTee"],
				[2,"longTee"],
				[1,"dualTee"],
				[2,"fishnetDualTee"],
				
				[1,"sluttyTubeTop"],
				[1,"sluttyHalterTop"],
				[1,"deepHalterTop"],
				
				[1,"botOpenTop"],
				
				[1,"sluttyTee"],
				[1,"sluttyDualTee"],
				[1,"niceSluttyTee"],
				[1,"deepTee"],
				[1,"stripedTightTee"],//
			);
		}else{
			temp.push(
				[2, "longTubeTop"],
				[2, "tubeTop"],
				[2, "sleevedTubeTop"],
				[1, "bimboTubeTop"],
				
				[1, "triangleTop"],
				
				[2, "longHalterTop"],
				[2, "halterTop"],
				
				[1, "turtleTop"],
				[1, "shrugTop"],
				
				[2,"tee"],
				[2,"tightTee"],
				[2,"niceTightTee"],
				[1,"bimboTee"],
				[2,"longTee"],
				[2,"dualTee"],
				[2,"fishnetDualTee"],
				[1,"stripedTightTee"],
			);

		}
		temp = check.filter_sexy_tops(temp);
		return temp;
	},

	upper_discount(){
		let temp = [];
		if(check.is_shameless || (check.is_slut && check.sexy_tops_prefered) ){
		temp.push(
				//tube
				[1,"tubeTop"],
				[3,"sluttyTubeTop"],
				
				//halter
				[1,"halterTop"],
				[3,"sluttyHalterTop"],
				
				//tee
				[2,"tightTee"],
				[3,"sluttyTee"],
				[1,"longTee"],
			);
		}else if(check.is_slut || check.sexy_tops_prefered){
			temp.push(
				//tube
				[1, "longTubeTop"],
				[2, "tubeTop"],
				[2,"sluttyTubeTop"],
				
				//halter
				[1, "longHalterTop"],
				[2, "halterTop"],
				[2,"sluttyHalterTop"],
				
				//tee
				[1,"tee"],
				[3,"tightTee"],
				[2,"sluttyTee"],
				[1,"longTee"],
			);
		}else{
			temp.push(
				//tube
				[2, "longTubeTop"],
				[3, "tubeTop"],
				
				//halter
				[2, "longHalterTop"],
				[3, "halterTop"],
		
				//tee
				[3,"tee"],
				[3,"tightTee"],
				[2,"longTee"],
			);

		}
		temp = check.filter_sexy_tops(temp);
		return temp;
	},
	
	
	
	upper_fa(){ //TODO - REWORK
		let temp = [];
		if(check.is_shameless || (check.is_slut && check.sexy_tops_prefered) ){
			temp.push(
				//tube
				[1, "tubeTop"],
				[3, "sluttyTubeTop"],
				[2, "sleevedTubeTop"],
				
				//halter
				[1, "halterTop"],
				[3, "sluttyHalterTop"],
				[2, "deepHalterTop"],
				
				//tee
				[1,"tightTee"],
				[2,"niceTightTee"],
				[3,"sluttyTee"],
				[3,"niceSluttyTee"],
				[2,"stripedTightTee"],
				[2,"sluttyDualTee"],
				
				//other
				[2, "triangleTop"],
				[2, "transparentTubeTop"],
				[2, "sheerTee"],
				[2,"deepTee"],
			);
		}else if(check.is_slut || check.sexy_tops_prefered){
			temp.push(
				//tube
				[2, "tubeTop"],
				[2, "sluttyTubeTop"],
				[2, "sleevedTubeTop"],
				
				//halter
				[1, "longHalterTop"],
				[2, "halterTop"],
				[2, "sluttyHalterTop"],
				[1, "deepHalterTop"],
				
				//tee
				[1,"tee"],
				[2,"tightTee"],
				[2,"niceTightTee"],
				[2,"sluttyTee"],
				[2,"niceSluttyTee"],
				[2,"stripedTightTee"],
				[2,"dualTee"],
				[2,"sluttyDualTee"],
				
				//other
				[1, "shrugTop"],
				[1, "triangleTop"],
				[1,"deepTee"],
			);
		}else{
			temp.push(
				//tube
				[2, "longTubeTop"],
				[2, "tubeTop"],
				[2, "sleevedTubeTop"],
				
				//halter
				[1, "longHalterTop"],
				[3, "halterTop"],
		
				//tee
				[3,"tee"],
				[3,"tightTee"],
				[2,"niceTightTee"],
				[2,"longTee"],
				[2,"stripedTightTee"],
				[3,"dualTee"],
				
				//other
				[1, "turtleTop"],
			);

		}
		temp = check.filter_sexy_tops(temp);
		return temp;
	},
	
	
	
	upper_boutique(){
		let temp = [];
		if(check.is_shameless || (check.is_slut && check.sexy_tops_prefered) ){
			temp.push(
				//tube
				[1, "tubeTop"],
				[3, "sluttyTubeTop"],
				[3, "sleevedTubeTop"],
				[1, "bimboTubeTop"],
				
				//halter
				[1, "halterTop"],
				[3, "sluttyHalterTop"],
				[3, "deepHalterTop"],
				
				//tee
				[2,"niceTightTee"],
				[4,"niceSluttyTee"],
				[2,"bimboTee"],
				[2,"sluttyDualTee"],
				
				//other
				[2, "turtleTop"],
				[2, "shrugTop"],
				[2, "botOpenTop"],
				[2, "bimboBotOpenTop"],
				[3, "triangleTop"],
				
				[2, "transparentTubeTop"],
				[2, "sheerTee"],
			);
		}else if(check.is_slut || check.sexy_tops_prefered){
			temp.push(
				//tube
				[2, "tubeTop"],
				[2, "sluttyTubeTop"],
				[3, "sleevedTubeTop"],
				[1, "bimboTubeTop"],
				
				//halter
				[1, "longHalterTop"],
				[2, "halterTop"],
				[2, "sluttyHalterTop"],
				[1, "deepHalterTop"],
				
				//tee
				[2,"niceTightTee"],
				[2,"niceSluttyTee"],
				[2,"bimboTee"],
				[1,"longTee"],
				[1,"sluttyDualTee"],
				
				//other
				[2, "turtleTop"],
				[2, "shrugTop"],
				[2, "botOpenTop"],
				[2, "bimboBotOpenTop"],
				[3, "triangleTop"],
				[1,"dualTee"],
				
			);
		}else{
			temp.push(
				//tube
				[2, "longTubeTop"],
				[2, "tubeTop"],
				[3, "sleevedTubeTop"],
				[1, "bimboTubeTop"],
				
				//halter
				[1, "longHalterTop"],
				[3, "halterTop"],
		
				//tee
				[4,"niceTightTee"],
				[2,"bimboTee"],
				[2,"longTee"],
				[2,"dualTee"],
				
				//other
				[2, "turtleTop"],
				[2, "shrugTop"],
			);

		}
		temp = check.filter_sexy_tops(temp);
		return temp;
	},
/*
	socks_lingerie(){
		const temp = [
			[2, "stockings"],
			[1, "polyStockings"],
		];
	},
*/	

	
	underwear_lingerie(){
		let temp = [];
		
		//sexier even on lower level && not the highest not necessary for sexiest
		if(check.panties){
			//prude - no thong
			if(check.slut === 0){
				temp.push(
					[4,"briefs"],
					[2,"hipsters"],
					[3,"boyshorts"],
					[3,"dividedPanties"],
					[1,"bimboHipsters"],
				);
				
				if(check.sexy_panties_prefered){
					temp.push(
						[2,"thong"],
						[2,"hiThong"],
						[1,"bimboThong"],
					);
				}
				
			//basic 
			}else if(check.slut === 1){
				temp.push(
					[3,"briefs"],
					[3,"boyshorts"],
					[3,"hipsters"],
					[3,"dividedPanties"],
					[2,"thong"],
					[2,"hiThong"],
					[2,"bimboHipsters"],
				);	
				
				if(check.sexy_panties_prefered){
					temp.push(
						[1,"cagePanties"],
						[1,"gString"],
					);
				}
				
			//slutty - gString allowed, less briefs
			}else if(check.slut === 2){
				temp.push(
					[2,"briefs"],
					[2,"boyshorts"],
					[3,"hipsters"],
					[3,"dividedPanties"],
					[3,"cagePanties"],
					[3,"thong"],
					[2,"hiThong"],
					[3,"gString"],
					
					[2,"bimboHipsters"],
					[2,"bimboThong"],
					[1,"animalHipsters"],
					[1,"animalThong"],
				);	
			//very slutty - mostly thong and gString
			}else if(check.slut === 3){
				temp.push(
					[2,"briefs"],
					[2,"boyshorts"],
					[2,"hipsters"],
					[3,"dividedPanties"],
					[2,"cagePanties"],
					[3,"thong"],
					[2,"hiThong"],
					[3,"gString"],
					
					[2,"bimboHipsters"],
					[2,"bimboThong"],
					[2,"bimboGString"],
					[1,"animalHipsters"],
					[2,"animalThong"],
					[1,"animalGString"],
					
					[2,"crotchlessPanties"],
				);	
			}else{
				temp.push(
					[1,"briefs"],
					[2,"boyshorts"],
					[2,"hipsters"],
					[2,"dividedPanties"],
					[2,"cagePanties"],
					[3,"thong"],
					[2,"hiThong"],
					[3,"gString"],
					
					[2,"bimboHipsters"],
					[2,"bimboThong"],
					[2,"bimboGString"],
					
					[1,"animalHipsters"],
					[2,"animalThong"],
					[2,"animalGString"],
					
					[2,"crotchlessPanties"],
					[3,"crotchlessThong"],
				);
			}
			
			temp = check.filter_sexy_panties(temp);
		}
			
			
		if(check.bra){
			temp.push(
				[3, "simpleBra"],
				[2, "simpleCageBra"],
				[2, "simplePushUpBra"],
				[3, "cageBra"],
				[3, "pushUpBra"],
				[2, "sexyCageBra"],
				[2, "sexyPushUpBra"],
				[1, "bimboCageBra"],
				[1, "bimboPushUpBra"],
			);
			
			if(check.is_slut){
				temp.push(
					[1, "bimboCageBra"],
					[1, "bimboPushUpBra"],
					[1, "animalCageBra"],
					[1, "animalPushUpBra"],
				);
			}
		}
		
		return temp;
	},
	
	underwear_discount(){
		let temp = [];
		if(check.panties){
			if(check.is_shameless){
				temp.push(
					[1, "simpleBriefs"],
					[2, "simpleHipsters"],
					[1, "boyshorts"],
					[2, "thong"],
					[2, "hiThong"],
				);
			}else if(check.is_slut){
				temp.push(
					[2, "simpleBriefs"],
					[3, "simpleHipsters"],
					[2, "boyshorts"],
					[1, "thong"],
					[1, "hiThong"],
				);
			}else{
				temp.push(
					[4, "simpleBriefs"],
					[2, "simpleHipsters"],
					[3, "boyshorts"],
				);
			}
			temp = check.filter_sexy_panties(temp);
		}
		
		if(check.bra){
			temp.push(
				[2, "simpleBra"],
				[2, "simpleCageBra"],
				[2, "simplePushUpBra"],
			);
		}
		
		
		return temp;
	},
	
	
	underwear_fa(){
		let temp = [];
		if(check.panties){
			if(check.is_shameless){
				temp.push(
					[2, "simpleHipsters"],
					[2, "hipsters"],
					[2, "thong"],
					[2, "hiThong"],
					[2, "gString"],
				);
			}else if(check.is_slut){
				temp.push(
					[1, "simpleBriefs"],
					[2, "briefs"],
					[2, "simpleHipsters"],
					[2, "hipsters"],
					[2, "boyshorts"],
					[2, "thong"],
					[2, "hiThong"],
				);
				
				if(check.sexy_panties_prefered){
					temp.push(
						[2, "gString"],
					)
				}		
			}else{
				temp.push(
					[2, "simpleBriefs"],
					[2, "briefs"],
					[1, "simpleHipsters"],
					[2, "hipsters"],
					[3, "boyshorts"],
				);
				
				if(check.sexy_panties_prefered){
					temp.push(
						[2, "thong"],
						[1, "hiThong"],
					)
				}
			}
			temp = check.filter_sexy_panties(temp);
		}
		
		if(check.bra){
			temp.push(
				[1, "simpleBra"],
				[1, "simpleCageBra"],
				[1, "simplePushUpBra"],
				[2, "cageBra"],
				[2, "pushUpBra"],
			);
		}
		return temp;
	},
	
	underwear_boutique(){
		let temp = [];
		if(check.panties){
			if(check.is_shameless){
				temp.push(
					[2, "briefs"],
					[2, "hipsters"],
					[2, "dividedPanties"],
					[2, "thong"],
					[2, "hiThong"],
					[3, "gString"],
					
					[1,"bimboHipsters"],
					[2,"bimboThong"],
					[2,"bimboGString"],
				);
			}else if(check.is_slut){
				temp.push(
					[2, "briefs"],
					[2, "hipsters"],
					[3, "dividedPanties"],
					[2, "thong"],
					[1, "hiThong"],
					
					[2,"bimboHipsters"],
					[2,"bimboThong"],
				);
			}else{
				temp.push(
					[3, "briefs"],
					[2, "hipsters"],
					[3, "dividedPanties"],
				
					[2,"bimboHipsters"],
				);
				
				if(check.sexy_panties_prefered){
					temp.push(
						[1, "thong"],
						[1, "hiThong"],
						[1, "bimboThong"],
					)
				}
			}
			temp = check.filter_sexy_panties(temp);
		}
		
		if(check.bra){
			temp.push(
				[1, "cageBra"],
				[1, "pushUpBra"],
				[2, "sexyCageBra"],
				[2, "sexyPushUpBra"],
				[1, "bimboCageBra"],
				[1, "bimboPushUpBra"],
			);
		}
		return temp;
	},

	




	lower_alt(){
		const temp = [];
		
		//long pants/skirts
		if(check.long_lower){
			temp.push(
				[1, "tightJeans"],
				[2, "sluttyJeans"],
			);
		}
		
		if(check.pants && check.slut >= 1){
			temp.push(
				[2, "sexyJeansShorts"],
			);
		}
		
		//TODO - GOTH SKIRTS
		
		temp.push(
			[1, "blackBorderlineSkirt"],
			[1, "blackPleatedSkirt"],
		)
		
		
		return temp;
	},
		
		
	lower_second(){
		const temp = [];
		
		//long pants/skirts
		if(check.long_lower){
			temp.push(
				[1, "jeans"],
				[2, "tightJeans"],
				[1, "pants"],
				[2, "leggings"],
				[1, "gymLeggings"],
				[1, "animalLeggings"],
				[1, "sluttyJeans"],
			);
			
			temp.push(
				[1, "longSkirt"],
			);
		}
		
		//shorts
		if(check.pants){
			temp.push(
				[1, "jeansShorts"],
				[1, "shorts"],
			);
			if(check.is_slut){
				temp.push(
					[2, "sexyJeansShorts"],
					[2, "sexyShorts"],
				);
			}
		}
		
		//short skirts
		//prude
		if(check.slut === 0){
			temp.push(
				[2, "shortSkirt"],
				[2, "pencilSkirt"],
				[1, "pleatedShortSkirt"],
			);
			
			if(check.short_skirts_prefered){
				temp.push(
					[2, "miniSkirt"],
				)
			}
				
		//basic 
		}else if(check.slut === 1){
			temp.push(
				[2, "shortSkirt"],
				[1, "pencilSkirt"],
				[2, "miniSkirt"],
				[1, "microSkirt"],
				[1, "pleatedShortSkirt"],
				[1, "pleatedMiniSkirt"],
			);
		//slutty 
		}else if(check.slut === 2){
			temp.push(
				[2, "miniSkirt"],
				[2, "microSkirt"],
				[1, "nanoSkirt"],
				[1, "pleatedMiniSkirt"],
				[1, "pleatedMicroSkirt"],
			);
		//sluttier 
		}else if(check.slut === 3){
			temp.push(
				[1, "miniSkirt"],
				[3, "microSkirt"],
				[2, "nanoSkirt"],
				[1, "pleatedMiniSkirt"],
				[1, "pleatedMicroSkirt"],
				[1, "animalMicroSkirt"],
			);
		}else if(check.slut >= 4){
			temp.push(
				[2, "microSkirt"],
				[3, "nanoSkirt"],
				[1, "pleatedMicroSkirt"],
				[1, "animalMicroSkirt"],
			);
		}

		return temp;
	},
	
	lower_discount(){
		const temp = [];
		
		//long pants/skirts
		if(check.long_lower){
			temp.push(
				[2, "jeans"],
				[1, "tightJeans"],
				[1, "pants"],
				[2, "leggings"],
			);
			
			temp.push(
				[2, "longSkirt"],
			);
		}
		
		//shorts
		if(check.pants){
			temp.push(
				[3, "shorts"],
			);
		}
		
		//short skirts
		if(check.is_shameless){
			temp.push(
				[1, "shortSkirt"],
				[1, "pencilSkirt"],
				[2, "miniSkirt"],
				[3, "microSkirt"],
			);
		}else if(check.is_slut){
			temp.push(
				[1, "shortSkirt"],
				[1, "pencilSkirt"],
				[2, "miniSkirt"],
				[2, "microSkirt"],
			);
		}else{
			temp.push(
				[3, "shortSkirt"],
				[2, "pencilSkirt"],
			);
			
			if(check.short_skirts_prefered){
				temp.push(
					[1, "miniSkirt"],
				)
			}
		}
		
		return temp;
	},
	
	
	lower_fa(){
		const temp = [];
		
		//long pants/skirts
		if(check.long_lower){
			temp.push(
				[2, "jeans"],
				[2, "tightJeans"],
				[2, "pants"],
				[2, "leggings"],
				[2, "gymLeggings"],
				[2, "sluttyJeans"],
			);
			
			temp.push(
				[2, "longSkirt"],
			);
		}
		
		//shorts
		if(check.pants){
			temp.push(
				[2, "jeansShorts"],
				[2, "shorts"],
			);
			if(check.is_slut){
				temp.push(
					[2, "sexyJeansShorts"],
					[2, "sexyShorts"],
				);
			}
		}
		
		//short skirts
		//prude
		if(check.slut === 0){
			temp.push(
				[2, "shortSkirt"],
				[2, "pencilSkirt"],
				[1, "pleatedShortSkirt"],
				[1, "miniSkirt"],
			);
		//basic 
		}else if(check.slut === 1){
			temp.push(
				[2, "shortSkirt"],
				[1, "pencilSkirt"],
				[2, "miniSkirt"],
				[1, "microSkirt"],
				[1, "pleatedShortSkirt"],
				[1, "pleatedMiniSkirt"],
			);
		//slutty 
		}else if(check.slut === 2){
			temp.push(
				[1, "shortSkirt"],
				[1, "pencilSkirt"],
				[2, "miniSkirt"],
				[2, "microSkirt"],
				[1, "pleatedMiniSkirt"],
				[1, "pleatedMicroSkirt"],
			);
		//sluttier 
		}else if(check.slut === 3){
			temp.push(
				[1, "shortSkirt"],
				[1, "pencilSkirt"],
				[2, "miniSkirt"],
				[3, "microSkirt"],
				[2, "nanoSkirt"],
				[1, "pleatedMiniSkirt"],
				[1, "pleatedMicroSkirt"],
			);
		//the sluttiest
		}else if(check.slut >= 4){
			temp.push(
				[1, "pencilSkirt"],
				[2, "miniSkirt"],
				[2, "microSkirt"],
				[3, "nanoSkirt"],
				[1, "pleatedMiniSkirt"],
				[1, "pleatedMicroSkirt"],
			);
		}
		
		return temp;
	},
	
	
	
	lower_boutique(){
		const temp = [];
		
		//long pants/skirts
		if(check.long_lower){
			temp.push(
				[2, "tightJeans"],
				[3, "gymLeggings"],
				[2, "animalLeggings"],
				[3, "sluttyJeans"],
			);
			
			temp.push(
				[4, "longSkirt"],
			);
		}
		
		//shorts
		if(check.pants){
			temp.push(
				[2, "jeansShorts"],
				[2, "shorts"],
			);
			if(check.is_slut){
				temp.push(
					[3, "sexyJeansShorts"],
					[2, "sexyShorts"],
				);
			}
		}
		
		//short skirts
		//prude
		if(check.slut === 0){
			temp.push(
				[2, "shortSkirt"],
				[4, "pencilSkirt"],
				[2, "pleatedShortSkirt"],
				[1, "miniSkirt"],
			);
		//basic 
		}else if(check.slut === 1){
			temp.push(
				[2, "shortSkirt"],
				[3, "pencilSkirt"],
				[2, "miniSkirt"],
				[1, "microSkirt"],
				[2, "pleatedShortSkirt"],
				[2, "pleatedMiniSkirt"],
			);
		//slutty 
		}else if(check.slut === 2){
			temp.push(
				[1, "shortSkirt"],
				[2, "pencilSkirt"],
				[2, "miniSkirt"],
				[2, "microSkirt"],
				[1, "nanoSkirt"],
				[2, "pleatedMiniSkirt"],
				[2, "pleatedMicroSkirt"],
			);
		//sluttier 
		}else if(check.slut === 3){
			temp.push(
				[1, "shortSkirt"],
				[2, "pencilSkirt"],
				[2, "miniSkirt"],
				[3, "microSkirt"],
				[1, "nanoSkirt"],
				[2, "pleatedMiniSkirt"],
				[2, "pleatedMicroSkirt"],
				[2, "animalMicroSkirt"],
			);
		//sluttiest
		}else if(check.slut >= 4){
			temp.push(
				[2, "pencilSkirt"],
				[2, "miniSkirt"],
				[3, "microSkirt"],
				[3, "nanoSkirt"],
				[1, "pleatedMiniSkirt"],
				[3, "pleatedMicroSkirt"],
				[2, "animalMicroSkirt"],
			);
		}
		return temp;
	},
	
	
	

	socks_lingerie(){
		const temp = [
			[2, "stockings"],
			[1, "polyStockings"],
		];
		
		//tease
		if(check.slut >= 1 || ext?.rules?.no_socks){
			temp.push(
				[1, "polyStockings"],
				[1, "semiFishnetStockings"],
				[2, "semiFishnetPolyStockings"],
				
				[1, "fishnetStockings"],
				[2, "garterStockings"],
				
				[2, "bimboStockings"],
				[2, "bimboGarterStockings"],
			);
		}
		
		//slut
		if(check.slut >= 2){
			temp.push(
				[1, "garterStockings"],
				[1, "fishnetGarterStockings"],
				
				[2, "openLightTights"],
				[2, "openDarkTights"],
				[1, "bimboPantyhose"],
				
				[1, "fishnetStockings"],
				[1, "fishnetPantyhose"],
			);
		}
		
		//hyperslut
		if(check.slut >= 3){
			temp.push(
				[1, "fishnetGarterStockings"],
				[1, "fishnetPantyhose"],
				[2, "openFishnetPantyhose"],
				[1, "crotchOpenFishnetPantyhose"],
			);
		}
		
		return temp;
	},
	
	

	
	
	socks_alt(){
		const temp = (()=>{
			//prude //basic //slutty 
			if(check.slut < 3){
				return [
					[2, "brightHighSocks"],
					[2, "darkHighSocks"],
					[3, "stripedDarkHighSocks"],
					
					[1, "darkTights"],
					[1, "brightTights"],
					[1, "lightTights"],
					[3, "polyPantyhose"],
					
					[3, "polyStockings"],
					[2, "fishnetStockings"],
					[2, "fishnetPantyhose"],
				];
			//superslut +
			}else{
				return [
					[2, "brightHighSocks"],
					[1, "darkHighSocks"],
					[2, "stripedDarkHighSocks"],
					
					[1, "darkTights"],
					[1, "brightTights"],
					[2, "polyPantyhose"],
					
					[2, "polyStockings"],
					[2, "fishnetStockings"],
					[2, "fishnetPantyhose"],
					
					[1, "openLightTights"],
					[1, "openDarkTights"],
					[2, "openFishnetPantyhose"],
					[2, "crotchOpenFishnetPantyhose"],
				];
			}
		})();
		
		return temp;
	},
	
	
	socks_discount(){
		const temp = [
			[2, "darkTights"],
			[3, "pantyhose"],
		];
		
		if(check.socks){
			temp.push(
				[4, "socks"],
				[4, "neutralSocks"],
				[3, "nylonPantyhose"],
			);
		}
		return temp;
	},
	
	socks_fa(){
		
		let temp = (()=>{
			//prude
			if(check.slut === 0){
				return [
					[3, "socks"],
					[3, "neutralSocks"],
					
					[1, "lightHighSocks"],
					[1, "darkHighSocks"],
					[2, "stripedHighSocks"],
					
					[2, "lightTights"],
					[2, "darkTights"],
					[2, "pantyhose"],
					[2, "nylonPantyhose"],
				];
			//basic 
			}else if(check.slut === 1){
				return [
					[2, "socks"],
					[2, "neutralSocks"],
					
					[1, "lightHighSocks"],
					[1, "darkHighSocks"],
					[2, "stripedHighSocks"],
					
					[2, "lightTights"],
					[2, "darkTights"],
					[2, "pantyhose"],
					[2, "nylonPantyhose"],
					[1, "polyPantyhose"],
					
					[2, "stockings"],
					[2, "polyStockings"],
				];
			//slutty 
			}else if(check.slut === 2){
				return [
					[1, "socks"],
					[1, "neutralSocks"],
					
					[1, "lightHighSocks"],
					[1, "darkHighSocks"],
					[2, "stripedHighSocks"],
					
					[1, "lightTights"],
					[1, "darkTights"],
					[1, "pantyhose"],
					[2, "polyPantyhose"],
					[2, "nylonPantyhose"],
					
					[2, "stockings"],
					[2, "polyStockings"],
					[1, "semiFishnetPolyStockings"],
					[1, "fishnetStockings"],
				];
			//sluttier 
			}else if(check.slut >= 3){
				return [
					[1, "socks"],
					[1, "neutralSocks"],
					
					[1, "lightHighSocks"],
					[1, "darkHighSocks"],
					[2, "stripedHighSocks"],
					
					[1, "lightTights"],
					[1, "darkTights"],
					[1, "pantyhose"],
					[2, "polyPantyhose"],
					[1, "nylonPantyhose"],
	
					[2, "stockings"],
					[2, "polyStockings"],
					[1, "semiFishnetPolyStockings"],
					[2, "fishnetStockings"],
					[1, "openDarkTights"],
				];
			}
		})();
		
		if(!check.socks){
			temp = temp.filter( a =>
				!["socks","neutralSocks"].temp(a[1]),
			);
		}
		
		
		return temp;
	},
	
	
	socks_boutique(){
		const temp = [];
		
		//slut
		if(check.slut >= 2 || ext?.rules?.no_socks){
			temp.push(
				[1, "pantyhose"],
				[1, "polyPantyhose"],
				[2, "semiFishnetPantyhose"],
				
				[2, "stockings"],
				[2, "semiFishnetStockings"],
				[2, "garterStockings"],
			);
		//superslut
		}else if(check.slut >= 3){
			temp.push(
				[1, "semiFishnetPantyhose"],
				
				[2, "stockings"],
				[2, "semiFishnetStockings"],
				[2, "semiFishnetPolyStockings"],
				[4, "garterStockings"],
				
				[1, "openLightTights"],
				[1, "openDarkTights"],
			);
		//prude
		}else{
			temp.push(
				[2, "pantyhose"],
				[3, "semiFishnetPantyhose"],
				[1, "polyPantyhose"],
				[1, "brightTights"],
			);
		}
		
		return temp;
	},
	
	outer_alt(){
		return [
			[3, "fishnetShrug"],
		];
	},
	
	outer_second(){
		return [
			[2, "shrug"],
		];
	},
	
	outer_boutique(){
		return [
			[4, "shrug"],
		];
	},
	
	shoes(){
		const temp = [];
		if(check.slut < 2){
			temp.push(
				[3, "blackSneakers"],
				[2, "orangeSneakers"],
				[3, "whiteSneakers"],
				
				[4, "kittyHeels"],
				[2, "highHeels"],
				
				[3, "darkBoots"],
				[3, "polyBoots"],
			);
		}else{
			temp.push(
				[3, "blackSneakers"],
				[2, "orangeSneakers"],
				[3, "whiteSneakers"],
				
				[2, "kittyHeels"],
				[6, "highHeels"],
				
				[3, "darkBoots"],
				[3, "polyBoots"],
				
				[2, "darkHighBoots"],
				[2, "polyHighBoots"],
			);
		}
		return temp;
	},
	
	piercings(){
		const temp = [
			PC.apex.bellybutton ? [4,"bellyPiercing"] : "",
		
			PC.apex.eyebrow ? [4,"eyebrowPiercingLeft"] : "",
		
			PC.apex.noseTop ? [4,"noseBridge"] : "",
			//mile.pierced.noseChain ? [4,"NoseChain"] : "",
			PC.apex.noseSide ? [4,"nosePiercing"] : "",
			PC.apex.noseBot ? [4,"noseRing"] : "",
			
			PC.apex.lipsTop ? [4,"medusa"] : "",
			PC.apex.lipsBot ? [4,"labret"] : "",
			PC.apex.lipsSide ? [4,"monroeRight"] : "",
			
			PC.apex.tongue ? [3,"tonguePiercing"] : "",
			
			...(PC.apex.nipples ? [
				[3,"nippleBars"],
				[2,"nippleRings"],
			] : [""]),

			...(PC.apex.nipplesMulti ? [
				[1,"verticalNippleBars"],
				[2,"multipleNipple"],
				[1,"nippleShields"],
				[1,"nippleStarShields"],
			] : [""]),
			
			PC.apex.hch ? [4,"HCHPiercing"] : "",
			PC.apex.vch ? [4,"VCHPiercing"] : "",
			PC.apex.labia ? [4,"labiaPiercing"] : "",	
			PC.apex.pubic ? [4,"christinaPiercing"] : "",
		];	
		return temp.filter(a => a);
	},
	
	precious_jewellery: [
		[4,"preciousSimpleChain"],
		[3,"preciousTearNecklace"],
		[2,"preciousDoubleNecklace"],
		[2,"preciousMultiNecklace"],
	],
	
	jewellery(){ //TODO - DOESNT WORK - overrriden by something else? fantasy?
		return [
			[2,"ballEarrings"],
			[2,"crystalEarrings"],
			[2,"triangleEarrings"],
			[2,"rhombEarrings"],
			[2,"chainEarrings"],
			[2,"loopEarrings"],
			[2,"bimboLoops"],
			
			[2,"polyBiChain"],
			[2,"biChain"],
			[2,"metalSimpleChain"],
			[2,"polySimpleChain"],
			[2,"metalTearNecklace"],
			[2,"tearNecklace"],
			[2,"doubleNecklace"],
			[2,"multiNecklace"],
			[2,"beadNecklace"],
		];
	},
	
	
	bijoux(){
		return [
			[2,"ballEarrings"],
			[2,"crystalEarrings"],
			[2,"triangleEarrings"],
			[2,"rhombEarrings"],
			[2,"chainEarrings"],
			[2,"loopEarrings"],
			[2,"bimboLoops"],
			
			[2,"polyBiChain"],
			[2,"biChain"],
			[2,"metalSimpleChain"],
			[2,"polySimpleChain"],
			[2,"metalTearNecklace"],
			[2,"tearNecklace"],
			[2,"doubleNecklace"],
			[2,"multiNecklace"],
			[2,"beadNecklace"],
		];
	},
	
	optician: [
		[8, "glasses"],
		[12, "polyGlasses"],
	],
	
		
				
	accessory_alt(){
		const temp = [];
		
		temp.push(
			[2, "fingerlessGloves"],
			[2, "sleeves"],
			[2, "fishnetSleeves"],
		);
		
		temp.push(
			[1, "polyWideBelt"],
			[1, "polyThinBelt"],
		);
		
		if(mile.choker || ext?.rules?.choker || check.slut > 1){
			temp.push(
				[2, "crossedChoker"],
				[2, "choker2"],
				[2, "choker"],
				[1, "blackChoker"],
				[1, "polyCrossedChoker"],
			);
		}
		
		if(mile.collar) temp.push( [1,"slaveCollar"], [1,"everydayCollar"] ); 
		return temp;
	},
	
	
	accessory_boutique(){
		const temp = [];
		temp.push(			
			[3, "longGloves"],
		);
		temp.push(
			[1, "darkWideBelt"],
			[1, "polyWideBelt"],
			[1, "darkThinBelt"],
			[1, "polyThinBelt"],
		);
		return temp;
	},
	
	
	accessory_section(){
		const temp = [];
		
		if(mile.glasses){
			temp.push(			
				[1, "glasses"],
				[1, "polyGlasses"],
			);
		}
		
		temp.push(
			[1, "darkWideBelt"],
			[1, "polyWideBelt"],
			[1, "darkThinBelt"],
			[1, "polyThinBelt"],
		);
		
		if(mile.choker || ext?.rules?.choker || check.slut > 1){
			temp.push(
				[1, "choker2"],
				[1, "choker"],
			);
		}
		
		return temp;
	},
	
	fashion_accessory(){
		const temp = [];
		
		//glasses
		if(mile.glasses){
			temp.push(			
				[2, "glasses"],
				[3, "polyGlasses"],
			);
		}
		
		//belts
		temp.push(
			[3, "darkWideBelt"],
			[3, "polyWideBelt"],
			[3, "darkThinBelt"],
			[3, "polyThinBelt"],
		);
		
		//chokers
		if(mile.choker || ext?.rules?.choker ){
			temp.push(
				[2, "crossedChoker"],
				[2, "polyCrossedChoker"],
				[3, "choker2"],
				[3, "choker"],
			);
		}else if(check.slut === 0){
			//nothing
		}else if(check.slut === 1){
			temp.push(
				[1, "crossedChoker"],
				[1, "polyCrossedChoker"],
			);
		}else if(check.slut === 2){
			temp.push(
				[2, "crossedChoker"],
				[1, "polyCrossedChoker"],
				[2, "choker2"],
				[2, "choker"],
			);
		}else{
			temp.push(
				[2, "crossedChoker"],
				[2, "polyCrossedChoker"],
				[3, "choker2"],
				[3, "choker"],
			);
		}
		
		return temp;
	},
		
		
		
		
	//TODO - bimbo stuff is quick fix
		upper_bimbo(){
				return [
					[3,"bimboTubeTop"],
					[3,"bimboTee"],
					[1,"bimboBotOpenTop"],
					[1,"bimboSluttyTubeTop"],
					[1,"bimboNiceTee"],
					[2,"bimboTriangleTop"],
				];
		},
		
		lower_bimbo(){
				return [
					[2,"bimboMiniSkirt"],
					[3,"bimboMicroSkirt"],
					[3,"bimboBorderlineSkirt"],
					[2,"bimboNanoSkirt"],
				];
		},
		
		underwear_bimbo(){
			return [
					[3,"bimboHipsters"],
					[3,"bimboThong"],
					[2,"bimboGString"],
					[2,"bimboCagePanties"],
					[2,"bimboCrotchlessThong"],
					[2, "bimboCageBra"],
					[2, "bimboPushUpBra"],
				];
		},
		
		underwear_alt(){
			return [
					[2,"blackHipsters"],
					[2,"blackCagePanties"],
					[2,"blackThong"],
					[2,"blackCageBra"],
				];
		},
		
		socks_bimbo(){
				return [
					[3, "bimboStockings"],
					[2, "bimboGarterStockings"],
					[3, "bimboPantyhose"],
					[2, "bimboOpenPantyhose"],
				];
		},

		jewellery_bimbo(){
				return [
					[2,"loopEarrings"],
					[2,"bimboLoops"],
			];
		},
			
		accessory_bimbo(){
			const temp = [
				[1, "bimboChoker"],
				[2, "polyCrossedChoker"],
				[1, "crossedChoker"],
			];
			if(mile.collar) temp.push( [1,"bimboCollar"] );
			return temp;
		},
		
		
	//TODO tempfix
	socks_legwear(){
			return [
				[3, "stockings"],
				[3, "polyStockings"],
				[3, "fishnetStockings"],

				[2, "stripedHighSocks"],
				[2, "stripedDarkHighSocks"],
				[2, "brightHighSocks"],
				[2, "darkHighSocks"],
				/* 
				//TODO - I don't remember if those are rated as sexy pantyhose, CHECK!!
				[1, "darkTights"],
				[1, "brightTights"],
				*/
				[1, "fishnetPantyhose"],
			];
	},


	underwear_vr_shop(){
		return [
			[3,"briefs"],
			[2,"boyshorts"],
			[3,"hipsters"],
			[1,"bimboHipsters"],
			[3,"dividedPanties"],
			[2,"thong"],
			[2,"hiThong"],
			[1,"cagePanties"],
			[1,"gString"],
		];
	},
	
	lower_vr_shop(){
		return [
			[2, "shortSkirt"],
			[2, "pencilSkirt"],
			[2, "miniSkirt"],
			[2, "microSkirt"],
			[2, "pleatedShortSkirt"],
			[2, "pleatedMiniSkirt"],
			//[1, "pleatedMicroSkirt"],
		];
	},
	
	
};