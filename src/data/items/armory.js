//WEAPONS

const base_damage = [
	10, //tier 0 - knife
	15, //tier 1 - falchion
	20, //tier 2 - cutlass
	25, //tier 3 - sword
	30, //tier 4 - bastard
	35, //tier 5 - demonic katana
];

export const weapon_types = {
	
	blades: [
		"knife",
		"falchion",
		"cutlass",
		"sword",
		"longsword",
		
		"dagger",
	],
	
	hammers: [
		"bludgeon",
		"mace",
		"morningstar",
		"warhammer",
		
		"wand",
	],

	polearms: [
		"spear",
		"scythe",
		"poleaxe",
		"glaive",
		
		"staff",
	],

	whips: [
		"whip",
		"scourge",
		"meteor",
		"chainsword",
	],

	bows: [
		"bow",
		"warbow",
		"elvenbow",
		"dragonbow",
	],
};

export const weapon_damage = {
	knife: base_damage[0],

	falchion: base_damage[1] * 1.1,
	cutlass: base_damage[2] * 1.1,
	sword: base_damage[3] * 1.1,
	longsword: base_damage[4] * 1.1,
	
	bludgeon: base_damage[1] * 1,
	mace: base_damage[2] * 1,
	morningstar: base_damage[3] * 1,
	warhammer: base_damage[4] * 1,
	
	spear: base_damage[1] * 0.9,
	scythe: base_damage[2] * 0.9,
	poleaxe: base_damage[3] * 0.9,
	glaive: base_damage[4] * 0.9,
	
	whip: base_damage[1] * 0.9,
	scourge: base_damage[2] * 0.9,
	meteor: base_damage[3] * 0.9,
	chainsword: base_damage[4] * 0.9,
	
	bow: base_damage[1],
	warbow: base_damage[2],
	elvenbow: base_damage[3],
	dragonbow: base_damage[4],
	
	dagger: base_damage[0],
	wand: base_damage[1] * 1,
	staff: base_damage[2] * 0.9,
	sceptre: base_damage[3] * 1,
}


export const weapon_damage_modifiers = {
	ancient: 1.1,
	engraved: 1.1,
	rusty: 0.9,
	cracked: 0.9,
	bent: 0.9,
	sharp: 1.2,
	heavy: 1.2,
	accurate: 1.2,
	masterwork: 1.3,
}




export const armor_defence = {
	//MATERIAL
	leather: 2,
	wyve: 3,
	hexa: 2.8,
	levi: 2.6,
	bronze: 4,
	mail: 5,
	steel: 6,
	glass: 6,
	paladin: 7, 
	alloy: 7,
	succubi: 7.25,
	demonic: 8, 
	
	//TYPE
	//dress:
	//minidress:
	cuirass: 3.5,
	bikini: 2,
	
	pauldrons: 1.75,
	cingulum: 1.5,
	
	vambrances: 1.2,
	//boots: 1.1,
	greaves: 1,
	gorget: 0.8,
}


export const armor_weight = {
	//MATERIAL
	leather: 3,
	wyve: 2.8,
	hexa: 2.6,
	levi: 2.4,
	bronze: 4.5,
	mail: 4.5,
	steel: 5,
	glass: 3.2,
	paladin: 5.4, 
	alloy: 2,
	succubi: 2.5,
	demonic: 6, 
	
	//TYPE
	cuirass: 4,
	bikini: 1.8,
	
	pauldrons: 1.5,
	cingulum: 1.75,
	
	vambrances: 1.2,
	//boots: 1.1,
	greaves: 1,
	gorget: 0.8,
}