/*
	0 - irl
	1 - story
	2 - reserved
	3 - game
	4 - weapons
	5 - armor
	6 - game story
	9 - designer
*/

export const ids = {
	
	designCageBra: 9102,
	designPushUpBra: 9103,
	designPanties: 9130,
	designThong: 9140,
	designGString: 9143,
	
	designTubeTop: 9401, 
	designSexyTubeTop: 9402, 
	
	designHalterTop: 9410, 
	designSexyHalterTop: 9411, 
	
	designTee: 9441,
	designSexyTee: 9443,
	
	designSkirt: 9312,
	designMicroSkirt: 9314,
	
	
//CONSUMABLES
//BEWARE - IDS < 100 ARE RESERVED FOR CONSUMABLES (grouped together in inventory)
	xHP: 1,
	xEng: 2,
	xAnti: 3,
	xHum: 10,
	xGob: 11,
	xElf: 12,
	xSuc: 13,
	xInc: 14,
	xMin: 15,
	xBow: 16,
	xNaj: 17,
	xFel: 18,
	xFemale: 19,
	xMale: 20,

	charmLightning: 30,
	charmBlizzard: 31,
	charmFirestorm: 32,
	charmBlades: 33,
	charmDazzle: 34,
	charmDarkness: 35,
	
//UNDERWEAR
	//BRA 
	simpleBra: 101,
	
	cageBra: 102,
	pushUpBra: 103,
	
	sexyCageBra: 104,
	sexyPushUpBra: 105,
	
	simpleCageBra: 106,
	simplePushUpBra: 107,
	
	animalCageBra: 114,
	animalPushUpBra: 115,
	

	
	
	//PANTIES
	briefs: 130,
	hipsters: 131,
	boyshorts: 132,
	simpleBriefs: 133,
	simpleHipsters: 134,
	
	dividedPanties: 135,
	
	
	
	
	thong: 140, 
	hiThong: 141,
	
	gString: 142,
	cagePanties: 143,
	stringThong: 88143,
	
	
	crotchlessPanties: 150,
	crotchlessThong: 151,
	
	animalHipsters: 161,
	animalThong: 162,
	animalGString: 163,
	
	//MALE UNDERWEAR
	maleBriefs: 180,
	boxers: 181,

	//FANTASY UNDERWEAR
	fBlandBra: 3100,
	fBlandPaties: 3130,
	fBlandThong: 3131,
	fBlandBoxers: 3180,
	
	fBra: 3101,
	fPaties: 3133,
	fThong: 3132,
	fBoxers: 3181,
	
	magicPanties: 3140,
	
//SOCKS
	//SOCKS
	socks: 200,
	neutralSocks: 201,
	
	//HIGH SOCKS
	lightHighSocks: 210,
	darkHighSocks: 211,
	brightHighSocks: 212,
	stripedHighSocks: 213,
	stripedDarkHighSocks: 214,
	
	//STOCKINGS
	stockings: 220,
	polyStockings: 221,
	fishnetStockings: 222,
	semiFishnetStockings: 223,
	semiFishnetPolyStockings: 224,
	
	garterStockings: 230,
	polyGarterStockings: 231,
	fishnetGarterStockings: 233,
	
	
	//PANTYHOSE
	lightTights: 250,
	darkTights: 251,
	brightTights: 252,
	pantyhose: 260,
	polyPantyhose: 261,
	nylonPantyhose: 262,
//brightPantyhose: 263,
	openLightTights: 270,
	openDarkTights: 271,
	fishnetPantyhose: 272,
	openFishnetPantyhose: 273,
	crotchOpenFishnetPantyhose: 274,//whorePantyhose: 274, //???
	semiFishnetPantyhose: 275,
	
	//FANTASY
	defHarlotStockings: 3200,
	harlotStockings: 3201,
	
//LOWER
	//SKIRTS 
	longSkirt: 310,
	pencilSkirt: 320,
	shortSkirt: 312,
	miniSkirt: 313,
	microSkirt: 314,
	nanoSkirt: 315,
	
	animalMicroSkirt: 319,
	pleatedShortSkirt: 322,
	pleatedMiniSkirt: 323,
	pleatedMicroSkirt: 324,
	
	//PANTS
	jeans: 350,
	tightJeans: 351,
	pants: 352,
	leggings: 353,	
	gymLeggings: 354,
	animalLeggings: 355,
	sluttyJeans: 356,
	
	//SHORTS
	jeansShorts: 360, //TODO - probably more divided 
	sexyJeansShorts: 361,
	shorts: 362,
	sexyShorts: 363,

	//FANTASY SKIRTS
	defHarlotSkirt: 3300,
	harlotSkirt: 3301,
	maidSkirt: 3302,
	//long skirt?
	haremSkirt: 3303,
		
	//FANTASY PANTS
	fLeggings: 3350,
	fFancyLeggings: 3451,
	fPants: 3452,
	//fancy pants?
	
	//FANTASY LOINCLOTHS
	defBarbaricLoincloth: 3320,
	barbaricLoincloth: 3321,
	fancyBarbaricLoincloth: 3322,
	clothLoincloth: 3325,
	
	defFallenLoincloth: 3330,
	fallenLoincloth: 3331,
	
//UPPER 
	//TUBE TOP
	longTubeTop: 400,
	tubeTop: 401,
	sluttyTubeTop: 402,
	sleevedTubeTop: 403,
	transparentTubeTop: 404,
	
	triangleTop: 407,
	botOpenTop: 408,
	
	
	
	//HALTER TOP
	longHalterTop: 413,
	halterTop: 410,
	sexyHalterTop: 411, //legacy
	sluttyHalterTop: 412,
	deepHalterTop: 414,
	
	turtleTop: 415,
	
	shrugTop: 417,
	fishnetShrugTop: 418,
	blackFishnetShrugTop: 419,
	
	corsetTop: 420,
	blackCorsetTop: 421,
	
	//T-SHIRT
	tee: 440,
	tightTee: 441,
	sheerTee: 442,
	sluttyTee: 443,
	fishnetTee: 444,
	dualTee: 445,
	sluttyDualTee: 446,
	fishnetDualTee: 447,
	blackTee: 448,
	longTee: 449,
	niceTightTee: 450,
	deepTee: 451,
	niceSluttyTee: 452,
	stripedTightTee: 453,
	/*
	
	*/
	
	//MANLY TEE
	manlyTee: 480,
	manlyDualTee: 481,
	manlyBlackTee: 482,
	
	//FANTASY UPPER
	fBikini: 3400,
	fPolyBikini: 3401,
	haremTop: 3402, 
	
	defHarlotTop: 3410,
	harlotTop: 3411,
	polyHarlotTop: 3412,
 
	defBarbaricBra: 3420,
	barbaricBra: 3421,
	fancyBarbaricBra: 3422,
	
	defFallenTop: 3430,
	fallenTop: 3431,
	
	fBlouse: 3440,
	fPolyBlouse: 3441,
	fShirt: 3444,
	fPolyShirt: 3445,
	
	
//UPPERLOWER
	//DRESS
	dress: 500,
	sexyDress: 501,
	sluttyDress: 502,
	longMinidress: 530,
	sexyMinidress : 531,
	sluttyMinidress: 532,
	longMinidress2: 536,
	sexyMinidress2: 537,
	sluttyMinidress2: 538,
	minidress3: 540,
	dualDress: 550,
	sexyDualDress: 551,
	//fishnet dual drass?
	
	//NIGHTIE
	nightie: 590,
	sexyNightie: 591,
	naughtyNightie: 592,
		
	//FANTASY
	nobleGown: 3520,
	mageRobe: 3530,
	polyMageRobe: 3531,
	
	
//OUTER
	suit: 600,
	darkJacket: 610,
	jacket: 611,
	sexyJacket: 612,
	
	shrug: 620,
	fishnetShrug: 622,
	
	//SWEATER
	sweater: 640,
	tightSweater: 641,
	
	//CORSETS
	//?660
	
	//FANTASY CORSET
	defHarlotCorset: 3600,
	harlotCorset: 3601,
	
//SHOES
	blackSneakers: 700,
	orangeSneakers: 701,
	whiteSneakers: 702,
	
	//HEELS
	kittyHeels: 730,
	highHeels: 731,
	
	//BOOTS
	orangeBoots: 750,
	darkBoots: 751,
	polyBoots: 752,
	
	darkHighBoots: 760,
	polyHighBoots: 761,
	
	
//ACCESSORY
	
	
	//BELTS
	darkWideBelt: 800,
	polyWideBelt: 801,
	darkThinBelt: 810,
	polyThinBelt: 811,
	
	//CHOKERS
	choker: 830,
	choker2: 831,
	crossedChoker: 832,
	polyCrossedChoker: 833,
	
	//GLASSES
	glasses: 850,
	polyGlasses: 851,
	
	//GLOVES
	fingerlessGloves: 860,
	sleeves: 861,
	fishnetSleeves: 862,
	longGloves: 863,
	
	//FANTASY BELTS 
	fDarkWideBelt: 3810,
	fPolyWideBelt: 3811,
	fFancyWideBelt: 3812,
	fDarkThinBelt: 3820,
	fPolyThinBelt: 3821,
	fFancyThinBelt: 3822,
	
	//FANTASY CHOKERS
	fChoker: 3830,
	fLeatherChoker: 3831,
	fFancyChoker: 3832,
	fMetalChoker: 3833,
	
	defFallenGorget: 3840,
	fallenGorget: 3841,
	
//JEWELLERY
	//EARRINGS
	ballEarrings: 901,
	crystalEarrings: 902,
	triangleEarrings: 903,
	rhombEarrings: 904,
	chainEarrings: 905,
	loopEarrings: 906,
	bimboLoops: 907,
	
	//NECKLACES
	polyBiChain: 930,
	biChain: 931,
	metalSimpleChain: 932,
	polySimpleChain: 933,
	metalTearNecklace: 934,
	tearNecklace: 935,
	doubleNecklace: 936,
	multiNecklace: 937,
	beadNecklace: 938,
	
	preciousSimpleChain: 939,
	preciousTearNecklace: 940,
	preciousDoubleNecklace: 941,
	preciousMultiNecklace: 942,
	
	
	//PIERCINGS
	bellyPiercingSimple: 950,
	bellyPiercing: 951,
	nosePiercing: 952,
	noseBridge: 953,
	noseRingSimple: 954,
	noseRing: 955,
	noseChain: 956,
	nippleBars: 957,
	multipleNipple: 958,
	nippleRings: 959,
	nippleShields: 960,
	nippleStarShields: 961,
	nippleChain: 962,
	VCHPiercing: 963,
	HCHPiercing: 964,
	christinaPiercing: 965,
	labiaPiercing: 966,
	labiaPiercings: 967,
	tonguePiercing: 968,
	tongueSlaveRing: 969,
	eyebrowPiercingLeft: 970,
	eyebrowPiercing: 971,
	labret: 972,
	medusa: 973,
	monroeRight: 974,
	monroe: 975,
	verticalNippleBars: 976,
	
	//FANTASY NECKLACES
	fTearNecklace: 3930,
	fMultiNecklace: 3931,
	
	magicTearNecklace: 3950,
	minorMagicTearNecklace: 3951,
	majorMagicTearNecklace: 3952,
	
	//HAT
	mageHat: 3990,
	polyMageHat: 3991,
	
//STORY
	shortSkirt0: 1001,
	shortSkirt1: 1002,
	shortSkirt2: 1003,
	shortSkirt3: 1004,
	shortJeansShorts3: 1005,
	
	wrestlingBot: 1010,
	wrestlingTop: 1011,
	redWrestlingBot: 1012,
	redWrestlingTop: 1013,
		
	improvisedChoker: 1020,
	leatherChoker: 1021,
	
	maidUpper: 1030,
	maidLower: 1031,
	maidHat: 1032,
	maidApron: 1033,

	bdsmCorset: 1100,
	bdsmThong: 1101,
	bdsmGloves: 1102,
	bdsmBoots: 1103,
	bdsmGorget: 1104,
	leatherMinidress: 1105,
	latexMinidress: 1106,
	bdsmStockings: 1107,
	bdsmChoker: 1108,
	bdsmHeels: 1109,
	bdsmCatsuit: 1110,
	
	slaveCollar: 1111,
	everydayCollar: 1112,
	
	swimsuit: 1130,
	decentBikiniTop: 1131,
	decentBikiniBottom: 1132,
	sexyBikiniTop: 1133,
	sexyBikiniBottom: 1134,
	sluttyBikiniTop: 1135,
	sluttyBikiniBottom: 1136,
	
	whiteTightTee: 1140,
	whitePants:  1141,
	
	cybrexPants:  1142,
	cybrexTee:  1143,
	
	chardBikiniTop: 1144,
	chardBikiniBot: 1145,
	chardTrunks: 1146,
	
	
//TODO ALT
	blackCagePanties: 1300,
	blackThong: 1301,
	blackHipsters: 1302,
	blackCageBra: 1303,
	blackBorderlineSkirt: 1304,
	blackNiceTee: 1305,
	blackPleatedSkirt: 1306,
	blackChoker: 1307, 
	//BRA
	
	
	bimboCageBra: 1201,
	bimboPushUpBra: 1202,
	bimboHipsters: 1203,
	bimboThong: 1204,
	bimboGString: 1205,
	
	bimboSocks: 1206,
	bimboPantyhose: 1207,
	bimboTights: 1208,
	
	bimboMiniSkirt: 1209,
	bimboMicroSkirt: 1210,
	bimboLeggings: 1211,
	bimboGymLeggings: 1212,
	bimboTubeTop: 1213,
	bimboTee: 1214,
	bimboHalterTop: 1215,
	bimboJacket: 1216,
	bimboDress: 1217,
	bimboMinidress: 1218,
	bimboMinidress2: 1219,
	bimboMinidress3: 1220,

	bimboStockings: 1221,
	bimboGarterStockings: 1222,
	bimboBotOpenTop: 1223,
	
	bimboNiceTee: 1224,
	bimboTriangleTop: 1225,
	bimboSluttyTubeTop: 1226,
	
	bimboNanoSkirt: 1227,
	bimboBorderlineSkirt: 1228,
	bimboCrotchlessThong: 1229,
	bimboOpenPantyhose: 1230,
	
	bimboCagePanties: 1231,
	
	bimboChoker: 1232,
	bimboCollar: 1233,
	
//WEAPONS
	
	knife: 4100,
	engravedKnife: 4101,
	
	falchion: 4110,
	rustyFalchion: 4111, 
	sharpFalchion: 4112,
	
	cutlass: 4120,
	rustyCutlass: 4121, 
	sharpCutlass: 4122,
	masterworkCutlass: 4123,
	
	sword: 4130,
	rustySword: 4131, 
	sharpSword: 4132,
	masterworkSword: 4133,
	
	icefire_sword: 4134, //TODO
	
	longsword: 4140,
	
	bludgeon: 4210,
	crackedBludgeon: 4211,
	heavyBludgeon: 4212,

		
	mace: 4220,
	crackedMace: 4221,
	heavyMace: 4222,
	
	morningstar: 4230,
	crackedMorningstar: 4231,
	heavyMorningstar: 4232,
	
	warhammer: 4240,
		
	spear: 4310,
	bentSpear: 4311,
	sharpSpear: 4312,
	
	scythe: 4320,
	bentScythe: 4321,
	sharpScythe: 4322,
	
	poleaxe: 4330,
	bentPoleaxe: 4331,
	sharp_poleaxe: 4332,
	
	glaive: 4340,
	
	whip: 4410,
	scourge: 4420,
	meteor: 4430,
	chainsword: 4440,
	
	bow: 4510,
	warbow: 4520,
	elvenbow: 4530,
	dragonbow: 4540,
	
	
	fireDagger: 4600,
	iceDagger: 4601, 
	windDagger: 4602,
	fireWand: 4610,
	iceWand: 4611,
	windWand: 4612,
	
	//armor
	leatherCuirass: 5130,
	leatherArmor: 5131,
	leatherSkirt: 5140,
	leatherPauldrons: 5150,
	leatherVambraces: 5160,
	
	fancyLeatherArmor: 5230,
	fancyLeatherSkirt: 5240,
	fancyLeatherPauldrons: 5250,
	fancyLeatherVambraces: 5260,
	
	magicFancyLeatherArmor: 5231,
	magicFancyLeatherPauldrons: 5251,
	magicFancyLeatherVambraces: 5261,
	
	bronzeCuirass: 5330,
	bronzeGreaves: 5340,
	bronzePauldrons: 5350,
	bronzeVambraces: 5360,
	bronzeGorget: 5390,
	
	magicBronzeCuirass: 5331,
	magicBronzeGreaves: 5341,
	magicBronzePauldrons: 5351,
	magicBronzeVambraces: 5361,
	magicBronzeGorget: 5391,
	
	steelCuirass: 5530,
	steelGreaves: 5540,
	steelPauldrons: 5550,
	steelVambraces: 5560,
	steelGorget: 5590,
	
	glassCuirass: 5630,
	glassGreaves: 5640,
	glassPauldrons: 5650,
	glassVambraces: 5660,
	glassGorget: 5690,
	
	
	
//GAME STORY
	gothShirt: 6100,
	gothCuirass: 6101,
	gothGoldPauldrons: 6102,
	gothPauldrons: 6103,
	
	siraelTop: 6120,
	siraelBottom: 6121,
	siraelCollar: 6122,
	siraelTopGreen: 6123,
	siraelBottomGreen: 6124,
	siraelCollarGreen: 6125,
	

};



//CHECKS FOR DUPLICATE IDS:
/* eslint-disable no-console */
if(window.debug){
	const all = Object.values(ids);
	const unique = [...(new Set(all))]; //sets include only unique items
	if(all.length > unique.length){ 
		while(unique.length){
			const id = unique.pop();
			all.splice(all.indexOf(id), 1);
		}
		console.error(`ERROR - duplicate item ids (${all})`);
	}
}
/* eslint-enable no-console */