export const fantasy = {
	/*
	fantasy: [
		
	],
	*/
	
	feyd: [
		[3,"xHP"],
		[3,"xEng"],
		[2,"xGob"],
		[2,"xElf"],
		[2,"xSuc"],
		[2,"xHum"],
		
		[1,"knife"],
		
		[1,"bentSpear"],
		[1,"rustyFalchion"],
		[1,"crackedBludgeon"],
		
		[1,"knife"],
		[1,"bludgeon"],
		[1,"spear"],
		[1,"falchion"],
	
		[3,"barbaricBra"],
		[3,"barbaricLoincloth"],
		[1,"fancyBarbaricBra"],
		[1,"fancyBarbaricLoincloth"],
		[4,"harlotCorset"],
		[3,"harlotTop"],
		[3,"harlotStockings"],
		[3,"harlotSkirt"],
		[3,"fallenTop"],
		[3,"fallenLoincloth"],
		
		[2,"fBlouse"],
		[1,"fPolyBlouse"],
		[1,"fLeggings"],
		
	],

	
	
	goth: [
		[2, "leatherArmor"],
		[2, "leatherVambraces"],
		[2, "leatherPauldrons"],
		"fancyLeatherVambraces",
		
		//"fancyLeatherPauldrons",
	/*
	[5,"bronzeCuirass"],
	[5,"maleLeatherArmor"],
	*/
		[1,"leatherArmor"],
		[1,"leatherVambraces"],
		[1,"leatherPauldrons"],
		[1,"leatherSkirt"],
		[1,"fancyLeatherVambraces"],
		[1,"fancyLeatherPauldrons"], //TODO
		
		"iceDagger",
		"rustyCutlass",
		"crackedMace",
		"bentScythe",
		"bow",
		
		//magic
		[2, "minorMagicTearNecklace"],
		"charmBlizzard",
		"charmFirestorm",
		
		//potions
		[4,"xHP"],
		[3,"xEng"],
		[2,"xGob"],
		[2,"xElf"],
		[2,"xSuc"],
		[2,"xHum"],
		
		//weapons
		[1,"knife"],
		[1,"bludgeon"],
		[1,"spear"],
		[1,"falchion"],
	
		//class-based clothes
		[1,"barbaricBra"],
		[1,"barbaricLoincloth"],
		[1,"fancyBarbaricBra"],
		[1,"fancyBarbaricLoincloth"],
		[2,"harlotCorset"],
		[1,"harlotTop"],
		[2,"polyHarlotTop"],
		[2,"harlotStockings"],
		[2,"harlotSkirt"],
		[2,"harlotSkirt"],
		[2,"fallenTop"],
		[2,"fallenLoincloth"],
		
		//upper 
		[1,"fBlouse"],
		[1,"fPolyBlouse"],
		[1,"fShirt"],
		[1,"fPolyShirt"],
		[1,"fBikini"],
		//[1,"fPolyBikini"],
		[2,"nobleGown"],
		
		//lower
		[2,"fLeggings"],
		[2,"fPants"],
		[2,"maidSkirt"],
		[1,"fFancyLeggings"],
		
		//accessory
		[1,"fDarkWideBelt"],
		[1,"fPolyWideBelt"],
		[1,"fFancyWideBelt"],
		[1,"fDarkThinBelt"],
		[1,"fPolyThinBelt"],
		[1,"fFancyThinBelt"],
		[2,"fChoker"],
		[1,"fLeatherChoker"],
		[2,"fallenGorget"],


		[1,"fBlandPaties"],
		[1,"fBlandThong"],
		[1,"fBlandBoxers"],

		[1,"fPaties"],
		[1,"fThong"],
		[1,"fBoxers"],




/*
[20,"choker2"],
[5,"bronzeCuirass"],
*/
		/*
		
		[3,"tunic"],
		[2,"maidSkirt"],
		[2,"fLeggings"],
		[2,"fPants"],
		
		*/
		
	//[20,"fMetalChoker"],
	//[20,"nobleGown"],
	//[20,"fallenTop"],
	//[20,"fallenTop"],
	//[20,"harlotTop"],
	
	//[3,"xGob"],
	/*
	[10,"brightWideBelt"],
	[10,"polyWideBelt"],
	[10,"polyBelt"],
	[10,"darkBelt"],
	*/
		/*
		[3,"xHP"],
		[3,"xEng"],
		[2,"xGob"],
		[2,"xElf"],
		[2,"xSuc"],
		[2,"xHum"],
		
		[1,"knife"],
		[1,"bludgeon"],
		[1,"spear"],
		[1,"falchion"],
	
		[3,"barbaricBra"],
		[3,"barbaricLoincloth"],
		[1,"fancyBarbaricBra"],
		[1,"fancyBarbaricLoincloth"],
		[4,"harlotCorset"],
		[3,"harlotTop"],
		[3,"harlotStockings"],
		[3,"harlotSkirt"],
		[3,"fallenTop"],
		[3,"fallenLoincloth"],
		
		[3,"tunic"],
		[2,"maidSkirt"],
		[2, "fLeggings"],
		[2, "fPants"],
		*/
	],
	
	
	
	
		
	
	dorf: [
		//[1, "steelCuirass"],
		[1, "steelGorget"],
		[1, "steelPauldrons"],
		[1, "steelVambraces"],
		[1, "steelGreaves"],

		[1, "fancyLeatherArmor"],
		[1, "fancyLeatherVambraces"],
		[2, "fancyLeatherPauldrons"],
			
		[2, "fFancyChoker"],
		[3, "fMetalChoker"],
		//TODO ADD MORE JEWELERY
		
		[2,"fDarkWideBelt"],
		[2,"fPolyWideBelt"],
		[2,"fFancyWideBelt"],
		[1,"fDarkThinBelt"],
		[1,"fPolyThinBelt"],
		[2,"fFancyThinBelt"],
	],
	
	
}