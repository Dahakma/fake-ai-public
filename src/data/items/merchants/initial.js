export const initial = {
	initial: [	//clothes which are initially defaultly in players IRL wardobe/inventory
		[1,"simpleBra"],
		[1,"cageBra"],
		[1,"pushUpBra"],
		[2,"briefs"],
		[1,"boyshorts"],
		[1,"hipsters"],
		
		//socks
		[2, "socks"],
		[2, "neutralSocks"],
		[1, "lightHighSocks"],
		[1, "darkTights"],
		[1, "pantyhose"],
		
		//lower
		[2, "longSkirt"],
		[2, "shortSkirt"],
		//+ skirt quest 
		[1, "jeans"],
		[1, "tightJeans"],
		[1, "leggings"],
		//[1, "shorts"],
		
		//upper 
		[1, "tee"],
		[1, "tightTee"],
		[1, "dualTee"],
		[1, "sexyHalterTop"],
		
		//outer
		[1, "jacket"],
		[1, "sweater"],
		[1, "tightSweater"],
		
		//accessory
		[1, "polyThinBelt"],
		[1, "polyThinBelt"],
		[2, "darkThinBelt"],
		
		//shoes
		//TODO
		[1, "blackSneakers"],
		//[1, "orangeSneakers"],
		[1, "whiteSneakers"],
		
		
		//jewellery
		[1,"bellyPiercing"],
		[1,"ballEarrings"],
		[1,"crystalEarrings"],
		[1,"triangleEarrings"],
		[1,"rhombEarrings"],
		
		[1,"metalTearNecklace"],
		[1,"polyBiChain"],
		[1,"beadNecklace"],
		[1,"doubleNecklace"],
		[1,"metalSimpleChain"],
		
		//dress
		/*
		[1,"dress"],
		[1,"longMinidress"],
		[1,"longMinidress2"],
		*/
	],
	
	president: [	//clothes added to IRL wardrobe when picked backgound
		//underwear
		[1,"briefs"],
		[1,"briefs"],
		
		//socks
		[1, "polyPantyhose"],
		[1, "pantyhose"],
		
		//lower
		[2, "pencilSkirt"],
		
		//upper 
		[1, "dualTee"],
		[1, "tee"],
	
		//outer
		[2, "sweater"],
		[2, "darkJacket"],

	],
	
	athlete: [	//clothes added to IRL wardrobe when picked backgound
		//underwear
		[1,"boyshorts"],
		[1,"hipsters"],
		
		//socks
		[1, "lightHighSocks"],
		[1, "darkHighSocks"],
		
		//lower
		[1, "leggings"],
		[1, "jeansShorts"],
		
		//upper 
		[1, "sexyHalterTop"],
		[2, "halterTop"],

		//outer
		[1, "jacket"],
	],
	
	partygirl: [	//clothes added to IRL wardrobe when picked backgound
		//underwear
		[2,"thong"],
		
		//socks
		[1, "stockings"],
		[1, "polyStockings"],
		
		//lower
		[1, "miniSkirt"],
		[1, "microSkirt"],
		
		//upper 
		[1, "tubeTop"],
		[1, "longTubeTop"],

		//outer
		[1, "sexyJacket"],
	],
	
}