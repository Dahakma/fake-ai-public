export const story = {
	
	legwear: [ //sexy legwear used for task
		//high socks
		[2, "lightHighSocks"],
		[2, "darkHighSocks"],
		
		//stocking
		[2, "stockings"],
		[3, "polyStockings"],
		[2, "fishnetStockings"],
		
		//open tight
		[2, "openLightTights"],
		[2, "openDarkTights"],

		//fishnet pantyhose
		[2, "fishnetPantyhose"],
		
		//more
		[1, "openFishnetPantyhose"],
		[1, "whorePantyhose"],
	],
	
	
	sexy_panties: [ //sexy panties task
		[6,"thong"],
		[4,"hiThong"],
		[4,"gString"],
		[3,"stringThong"],
	],
	
}