//import {shuffle} from "Libraries/dh";
import {garment} from "Data/items/blueprints/index";

/*
	ext.slut - general slut level, dynamically calculated based on mile.slut
	0 - prude
	1 - tease
	2 - slut
	3 - superslut 
	(4 - hyperslut)
*/


export const eShop = {
	testShop(){
	/*
		return [
			[5,"animalLeggings"],
			[5,"fishnetStockings"],
			[5,"animalBra2"],
			[5,"animalBra3"],
			
			[5,"animalHipsters"],
			[5,"animalThong"],
			
		];
		*/
		
		return Object.keys(garment).map( a => {
			return [2, a];
		});
		
	},
	
//VIRTUAL SHOP
	//EVERTING 
	shop(){
		return [
			...this.bra(),
			...this.panties(),
			...this.socks(),
			...this.upper(),
			...this.lower(),
			...this.accessory(),
			...this.outer(),
			...this.shoes(),
			...this.jewellery(),
			...this.dress(),
			...this.piercing(),
		];
	},
	
	//DRESS
	dress(){
//TODO FILTERING
//TODO MORE?			
		const temp = (()=>{
			//prude - no thong
			if(ext.slut === 0){
				return [
					[2,"dress"],
					[1,"dualDress"],
					[2,"longMinidress"],
					[2,"longMinidress2"],
				];
			//basic 
			}else if(ext.slut === 1){
				return [
					[1,"dress"],
					[1,"dualDress"],
					[2,"longMinidress"],
					[2,"longMinidress2"],
					[1,"sexyMinidress"],
					[1,"sexyMinidress2"],
					[1,"minidress3"],
				];	
			//slutty - gString allowed, less briefs
			}else if(ext.slut === 2){
				return [
					[1,"sexyDualDress"],
					[1,"longMinidress"],
					[1,"longMinidress2"],
					[1,"sexyMinidress"],
					[1,"sexyMinidress2"],
					[2,"minidress3"],
					[1,"sluttyMinidress"],
					[1,"sluttyMinidress2"],
				];	
			//very slutty - mostly thong and gString
			}else if(ext.slut >= 3){
				return [
					[1,"sexyDualDress"],
					[1,"longMinidress"],
					[1,"longMinidress2"],
					[1,"sexyMinidress"],
					[1,"sexyMinidress2"],
					[2,"minidress3"],
					[2,"sluttyMinidress"],
					[2,"sluttyMinidress2"],
				];	
			}else{
				return [];
			}
		})();
		
		return temp;
	},
	
	
	
	//PIERCCING
	piercing(){
		const temp = [
			PC.apex.bellybutton ? [4,"bellyPiercing"] : "",
		
			PC.apex.eyebrow ? [4,"eyebrowPiercingLeft"] : "",
		
			PC.apex.noseTop ? [4,"noseBridge"] : "",
			//mile.pierced.noseChain ? [4,"NoseChain"] : "",
			PC.apex.noseSide ? [4,"nosePiercing"] : "",
			PC.apex.noseBot ? [4,"noseRing"] : "",
			
			PC.apex.lipsTop ? [4,"medusa"] : "",
			PC.apex.lipsBot ? [4,"labret"] : "",
			PC.apex.lipsSide ? [4,"monroeRight"] : "",
			
			PC.apex.tongue ? [3,"tonguePiercing"] : "",
			
			PC.apex.nipples ? [5,"nippleBars"] : "",
		
			PC.apex.hch ? [4,"HCHPiercing"] : "",
			PC.apex.vch ? [4,"VCHPiercing"] : "",
			PC.apex.labia ? [4,"labiaPiercing"] : "",	
			PC.apex.pubic ? [4,"christinaPiercing"] : "",
//TODO
		
		];
		
		
		return temp.filter(a => a);
	},
	
	
	//JEWELLERY
	jewellery(){
		return [
			[2,"ballEarrings"],
			[2,"crystalEarrings"],
			[2,"triangleEarrings"],
			[2,"rhombEarrings"],
			[2,"chainEarrings"],
			[2,"loopEarrings"],
			[2,"bimboLoops"],
			
			[2,"polyBiChain"],
			[2,"biChain"],
			[2,"metalSimpleChain"],
			[2,"polySimpleChain"],
			[2,"metalTearNecklace"],
			[2,"tearNecklace"],
			[2,"doubleNecklace"],
			[2,"multiNecklace"],
			[2,"beadNecklace"],
		];
	},
	
	
	//BRA
	bra(){
		if(ext?.rules?.enforced && ext?.rules?.no_bra) return [];
		return [
			[3,"bra1"],
			[3,"bra2"],
			[3,"bra3"],
		];
	},
	
	//PANTIES
	panties(){
		if(ext?.rules?.enforced && ext?.rules?.no_panties) return [];
		let temp = (()=>{
			//prude - no thong
			if(ext.slut === 0){
				return [
					[5,"briefs"],
					[3,"boyshorts"],
					[2,"hipsters"],
				];
			//basic 
			}else if(ext.slut === 1){
				return [
					[3,"briefs"],
					[2,"boyshorts"],
					[3,"hipsters"],
					[2,"thong"],
					[1,"stringThong"],
				];	
			//slutty - gString allowed, less briefs
			}else if(ext.slut === 2){
				return [
					[2,"briefs"],
					[2,"boyshorts"],
					[2,"hipsters"],
					[2,"thong"],
					[1,"hiThong"],
					[2,"gString"],
					[2,"stringThong"],
				];	
			//very slutty - mostly thong and gString
			}else if(ext.slut >= 3){
				return [
					[1,"briefs"],
					[1,"boyshorts"],
					[3,"hipsters"],
					[2,"thong"],
					[2,"hiThong"],
					[3,"gString"],
					[2,"stringThong"],
				];	
			}else{
				return [];
			}
		})();
					
		if(ext?.rules?.enforced && ext?.rules?.sexy_panties){
			temp = temp.filter( a =>
				a[1] !== "briefs" && a[1] !== "boyshorts",
			);
		}
		if(ext?.rules?.sexy_panties){
			temp.push(
				[1,"hipsters"],
				[1,"thong"],
				[1,"hiThong"],
			);
		}
		return temp;
	},
	
	//SOCKS
	socks(){
		let temp = (()=>{
			//prude
			if(ext.slut === 0){
				return [
					[3, "socks"],
					[3, "neutralSocks"],
					
					[1, "lightHighSocks"],
					[1, "darkHighSocks"],
					
					[1, "lightTights"],
					[1, "darkTights"],
					[2, "pantyhose"],
				];
			//basic 
			}else if(ext.slut === 1){
				return [
					[3, "socks"],
					[3, "neutralSocks"],
					
					[1, "lightHighSocks"],
					[1, "darkHighSocks"],
					
					[1, "lightTights"],
					[1, "darkTights"],
					[2, "pantyhose"],
					[1, "polyPantyhose"],
					
					[2, "stockings"],
					[1, "polyStockings"],
				];
			//slutty 
			}else if(ext.slut === 2){
				return [
					[2, "socks"],
					[2, "neutralSocks"],
					
					[1, "lightHighSocks"],
					[1, "darkHighSocks"],
					
					[1, "lightTights"],
					[1, "darkTights"],
					[2, "pantyhose"],
					[1, "polyPantyhose"],
					
					[2, "stockings"],
					[1, "polyStockings"],
					[2, "fishnetStockings"],
					[1, "openLightTights"],
					[1, "openDarkTights"],
				];
			//sluttier 
			}else if(ext.slut >= 3){
				return [
					[2, "socks"],
					[2, "neutralSocks"],
					
					[1, "lightHighSocks"],
					[1, "darkHighSocks"],
					
					[1, "lightTights"],
					[1, "darkTights"],
					[2, "pantyhose"],
					[1, "polyPantyhose"],
	
					[2, "stockings"],
					[1, "polyStockings"],
					[2, "fishnetStockings"],
					[1, "openLightTights"],
					[1, "openDarkTights"],
					[1, "fishnetPantyhose"],
					[1, "openFishnetPantyhose"],
					[1, "whorePantyhose"],
				];
			}else{
				return [];
			}
		})();
	
		if(ext?.rules?.enforced && ext?.rules?.no_socks){
			temp = temp.filter( a =>
				!["socks","neutralSocks"].temp(a[1]),
			);
		}
		
		if(ext?.rules?.no_socks && ext.slut <= 1){
			temp.push(
				[1, "lightHighSocks"],
				[1, "darkHighSocks"],
				[1, "lightTights"],
				[1, "darkTights"],
				[1, "pantyhose"],
			);
		}
		
		if(ext?.rules?.enforced && ext?.rules?.sexy_stockings){
			temp = temp.filter( a =>
				!["socks","neutralSocks","lightTights","darkTights","pantyhose","polyPantyhose"].temp(a[1]),
			);
		}
		
		if(ext?.rules?.sexy_stockings && ext.slut <= 1){
			temp.push(
				[1, "lightHighSocks"],
				[1, "darkHighSocks"],
				[1, "stockings"],
				[1, "polyStockings"],
				[1, "openLightTights"],
				[1, "openDarkTights"],
			);
		}
		
		return temp;
	},
	
	//LOWER
	lower(){
		//long skirts & long pants:
		const long_allowed = !(   ext?.rules?.enforced && (ext?.rules?.short_skirt || ext?.rules?.show_legs)   );
		//pants & shorts:
		const pants_allowed = !(  ext?.rules?.enforced && ext?.rules?.short_skirt   );
		
		const temp = [];
		
	//longs skirt (neither short skirts nor show legs
		if(long_allowed){
			temp.push(
				[4, "longSkirt"],
			);
		}else{
			temp.push(
				[1, "miniSkirt"], //extra miniskirt when rules enforced (even for low slut)
			);
		}
		
		
	//short skirt (always)
		//prude
		if(ext.slut === 0){
			temp.push(
				[3, "shortSkirt"],
				[2, "pencilSkirt"],
			);
		//basic 
		}else if(ext.slut === 1){
			temp.push(
				[4, "shortSkirt"],
				[2, "pencilSkirt"],
				[2, "miniSkirt"],
				[1, "microSkirt"],
			);
		//slutty 
		}else if(ext.slut === 2){
			temp.push(
				[3, "shortSkirt"],
				[2, "pencilSkirt"],
				[4, "miniSkirt"],
				[2, "microSkirt"],
				[1, "nanoSkirt"],
			);
		//sluttier 
		}else if(ext.slut >= 3){
			temp.push(
				[2, "shortSkirt"],
				[1, "pencilSkirt"],
				[3, "miniSkirt"],
				[3, "microSkirt"],
				[3, "nanoSkirt"],
			);
		}
		
	
	//pants
		if(long_allowed){
			temp.push(
				[3, "jeans"],
				[2, "pants"],
				[2, "tightJeans"],
				[2, "leggings"],
				[1, "gymLeggings"],
			);
		}else if(pants_allowed){ //extra shorts when rules enforced (even for low slut)
			temp.push( 
				[1, "jeansShorts"],  
				[1, "shorts"],
			);
		}
		
	//shorts	
		if(pants_allowed){
			//prude
			if(ext.slut === 0){
				temp.push(
				);
			//basic 
			}else if(ext.slut === 1){
				temp.push(
					[1, "jeansShorts"],
					[1, "shorts"],
				);
			//slutty 
			}else if(ext.slut === 2){
				temp.push(
					[1, "jeansShorts"],
					[1, "shorts"],
					[1, "sexyJeansShorts"],
					[1, "sexyShorts"],
				);
			//sluttier 
			}else if(ext.slut >= 3){
				temp.push(
					[2, "sexyJeansShorts"],
					[2, "sexyShorts"],
				);
			}	
		}
		
		return temp;
	},
	
	//UPPER
	upper(){
		const temp = [];
		
		//SHIRTS 
		if( !(ext?.rules?.enforced && ext?.rules?.sexy_tops) ){
			temp.push(
				[2, "tee"],
				[1, "tightTee"],
				[2, "dualTee"],
			);
		}
		
		//TOPS & SEXY SHIRTS 
		//prude
		if(ext.slut === 0){
			temp.push(
				[3, "longTubeTop"],
				[1, "shortTubeTop"],
				[2, "sleevedTubeTop"],
				
				[3, "halterTop"],
				[2, "sexyHalterTop"],
			);
		//basic 
		}else if(ext.slut === 1){
			temp.push(
				[2, "longTubeTop"],
				[2, "shortTubeTop"],
				[2, "sleevedTubeTop"],
				
				[2, "halterTop"],
				[2, "sexyHalterTop"],
				[1, "sluttyHalterTop"],
				
				[1, "sluttyTee"],
				[1, "sluttyDualTee"],

			);
		//slutty 
		}else if(ext.slut === 2){
			temp.push(
				[1, "longTubeTop"],
				[2, "shortTubeTop"],
				[1, "sluttyTubeTop"],
				[2, "sleevedTubeTop"],
				
			//	[1, "halterHalterTop"],
				[1, "halterTop"],
				[2, "sexyHalterTop"],
				[2, "sluttyHalterTop"],
				
				[2, "sluttyTee"],
				[2, "sluttyDualTee"],
				[2, "fishnetDualTee"],
				[2, "sheerTee"],
			);
		//sluttier 
		}else if(ext.slut >= 3){
			temp.push(
				[2, "shortTubeTop"],
				[2, "sluttyTubeTop"],
				[2, "sleevedTubeTop"],
				
				[1, "sexyHalterTop"],
				[3, "sluttyHalterTop"],
				
				[3, "sluttyTee"],
				[3, "sluttyDualTee"],
				[2, "fishnetDualTee"],
				[3, "sheerTee"],
				[2, "fishnetTee"],
			);
		}
		
		return temp;
	},
	
	//ACCESSORY
	accessory(){
		const temp = [];
		
		if(mile.glasses){
			temp.push(			
				[1, "glasses"],
				[2, "polyGlasses"],
			);
		}
		
		
		
		temp.push(
			[2, "darkWideBelt"],
			[2, "polyWideBelt"],
			[2, "darkThinBelt"],
			[2, "polyThinBelt"],
		/*
			maleBelt"],
			[2, "polyWideBelt"],
			[3, "polyBelt"],
			[3, "darkBelt"],
			*/
		);
		
		
		if(mile.choker || ext?.rules?.choker ){
			temp.push(
				[1, "crossedChoker"],
				[3, "choker2"],
				[3, "choker"],
			);
		}else if(ext.slut === 0){
			//nothing
		}else if(ext.slut === 1){
			temp.push(
				[1, "crossedChoker"],
			);
		}else if(ext.slut === 2){
			temp.push(
				[1, "crossedChoker"],
				[2, "choker2"],
				[2, "choker"],
			);
		}else{
			temp.push(
				[1, "crossedChoker"],
				[3, "choker2"],
				[3, "choker"],
			);
		}
		
		return temp;
	},
	
	//OUTER
	outer(){
		const temp = [];
		if(ext.slut < 2){
			temp.push(
				[1, "jacket"],
				[1, "darkJacket"],
				[1, "sexyJacket"],
				
				[2, "sweater"],
				[1, "tightSweater"],
			);
		}else{
			temp.push(
				[1, "jacket"],
				[2, "sexyJacket"],
				
				[1, "sweater"],
				[2, "tightSweater"],
			);
		}
		return temp;
	},
	
	
	//SHOES
	shoes(){
		const temp = [];
		if(ext.slut < 2){
			temp.push(
				[2, "blackSneakers"],
				[1, "orangeSneakers"],
				[2, "whiteSneakers"],
				
				[2, "kittyHeels"],
				[1, "highHeels"],
				
				[2, "darkBoots"],
				[2, "polyBoots"],
			);
		}else{
			temp.push(
				[2, "blackSneakers"],
				[1, "orangeSneakers"],
				[2, "whiteSneakers"],
				
				[4, "highHeels"],
				
				[2, "darkBoots"],
				[2, "polyBoots"],
				
				[2, "darkHighBoots"],
				[2, "polyHighBoots"],
			);
		}
		return temp;
	},
	
	
	
	//BIMBO
	bimbo(){
		//low
		return [
			[2,"bimboBra2"],
			[2,"bimboBra3"],
			
			[3,"bimboHipsters"],
			[2,"bimboThong"],
			[1,"bimboGString"],
			
			[3,"bimboSocks"],
			[2,"bimboPantyhose"],
			[2,"bimboTights"],
			
			[3,"bimboMiniSkirt"],
			[2,"bimboMicroSkirt"],
			
			[1,"bimboLeggings"],
			[1,"bimboGymLeggings"],
			
			//[2,"bimboHalterTop"],
			[2,"bimboTubeTop"],
			[3,"bimboTee"],
			
			[2,"bimboJacket"],
			
			[1,"bimboDress"],
			[1,"bimboMinidress"],
			[1,"bimboMinidress2"],
			[1,"minidress3"],
		];
	},
}