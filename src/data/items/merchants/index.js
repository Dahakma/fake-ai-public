import {initial} from "./initial";
import {fantasy} from "./fantasy";
import {story} from "./story";
import {eShop} from "./eShop";

export const merchants = {
	...initial,
	.//..fantasy,
	...story,
	//...eShop,
}