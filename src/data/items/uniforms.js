/*
	clothes set worn by avatars 
*/

//reused groups
const groups = {
	m_underwear: ["maleBriefs", "boxers", "boxers"],
	m_socks: ["lightHighSocks","darkHighSocks","neutralSocks"], 
	m_lower: ["jeans"], 
	m_upper: ["manlyTee", "manlyTee", "manlyDualTee"],
	
	f_bra: ["simpleBra","cageBra","pushUpBra"],
	f_panties: ["briefs","hipsters","hipsters","boyshorts"],
	f_socks: ["neutralSocks"], //TODO
	f_lower: ["shortSkirt", "miniSkirt", "longSkirt", "leggings", "tightJeans", "jeans", "jeans"], 
	f_upper: ["tee","tightTee","dualTee","halterTop","halterTop"],
	
	f_earrings: ["ballEarrings", "crystalEarrings", "triangleEarrings", "rhombEarrings", "chainEarrings", "loopEarrings"],
	f_earrings_nice: ["ballEarrings", "crystalEarrings", "triangleEarrings"],
	f_earrings_sexy: ["chainEarrings", "loopEarrings"],
	
	necklace: ["doubleNecklace", "multiNecklace", "tearNecklace"], //TODO!!!!!
	sneakers: ["blackSneakers", "orangeSneakers", "whiteSneakers"],
	socks: ["neutralSocks"],
}

export const uniforms = {

//IRL GENERIC
	male_generic: [
		groups.m_underwear, 
		groups.m_socks_, 
		groups.m_upper, 
		groups.m_lower, 
		groups.sneakers, 
	],
	
	female_generic: [
		groups.f_bra,
		groups.f_panties,
		groups.f_socks,
		groups.f_upper,
		groups.f_lower,
		groups.sneakers,
	],
	
	yoga: [ //excercise clothes
		groups.f_bra,
		groups.f_panties,
		groups.f_socks,
		"gymLeggings",
		["halterTop", "tightTee"],
	],
	
	female_underwear: [
		["simpleBra","cageBra","pushUpBra"],
		["briefs","hipsters","thong","boyshorts","hiThong"],
		["lightHighSocks","darkHighSocks","polyStockings","stockings"],
	],
	
	sexy: [ //a bit sexier clothes
		groups.f_bra,
		groups.f_earrings,
		["longTubeTop", "sleevedTubeTop", "sexyHalterTop", "tightTee", "tightTee"],
		["miniSkirt", "miniSkirt"],
		["hipsters", "thong"],
		["garterStockings", "stockings", "darkTights", "pantyhose", "fishnetStockings"],
	],
	
	slutty: [ //a bit slutier clothers
		groups.f_bra,
		groups.f_earrings_sexy,
		["longTubeTop", "sleevedTubeTop", "sexyHalterTop", "tightTee", "tightTee"], //TODO!!
		["miniSkirt", "miniSkirt"],
		["thong", "hiThong", "gString", "stringThong"],
		["stockings", "polyStockings", "fishnetStockings", "garterStockings", "pantyhose", "polyPantyhose", "fishnetPantyhose"],
	],
	
	poledancer: [ //gym clothes for poledancing training
		groups.f_bra,
		groups.f_earrings,
		["hipsters","thong"],
		["shorts","sexyShorts"],
		["sexyHalterTop"],
	],
	
	
	
//IRL CHARACTERS
	
	//Juraj - first nerd
	qaa(){ return [
		groups.m_underwear, 
		groups.m_socks, 
		groups.m_upper, 
		groups.m_lower, 
		groups.sneakers,
	]},
	
	//Zdeno - second nerd
	qbb(){ return [
		mile.b_choker ? "choker" : "",
		groups.m_underwear, 
		groups.m_socks_, 
		"manlyBlackTee",
		groups.m_lower, 
		groups.sneakers, 
	]},
	
	//Matej - third nerd
	qcc(){ return [
		groups.m_underwear, 
		groups.m_socks_, 
		groups.m_upper, 
		groups.m_lower, 
		groups.sneakers, 
		["glasses"],
	]},
	
	//AI
	ayy: [ //huge slut
		["sexyHalterTop","sluttyTee","sluttyDualTee","tubeTop"],
		["jeansShorts","miniSkirt","microSkirt"],
		["polyStockings","polyStockings","socks"],
		["hiThong","hipsters","thong"],
		["cageBra","pushUpBra"],
		["blackSneakers", "orangeSneakers", "whiteSneakers"],
	],
	
	//Saša
	sas(){ //classy
		const piercing = (()=>{ //piercings - based on events in the game
			if(mile.girls_piercings === 16) return "nippleBars";
			if(mile.girls_piercings === 17) return "nippleRings";
			
			if(mile.girls_piercings === 12) return "eyebrowPiercingLeft";
			if(mile.girls_piercings === 13) return "noseRing";
			if(mile.girls_piercings === 14) return "monroeRight";
			return "";
		})();
		
		return[
			mile.sas_no_bra ? "" : groups.f_bra,
			groups.f_panties,
			groups.f_socks,
			["tightTee","dualTee","halterTop","longHalterTop","longHalterTop"],
			groups.sneakers,
			groups.f_earrings_nice,
			["shortSkirt","longSkirt"], //likes skirts
			piercing,
		]
	},
	
	//Eva
	eva(){ //a bit sexy
		const piercing = (()=>{ //piercings - based on events in the game
			if(mile.girls_piercings === 6) return "nippleBars";
			if(mile.girls_piercings === 7) return "nippleRings";
			
			if(mile.girls_piercings === 2) return "eyebrowPiercingLeft";
			if(mile.girls_piercings === 3) return "nosePiercing";
			if(mile.girls_piercings === 4) return "medusa";
			return "";
		})();
		
		return[
			mile.eva_no_bra ? "" : groups.f_bra,
			groups.f_panties,
			groups.f_socks,
			groups.f_upper,
			groups.sneakers,
			groups.f_lower,
			groups.f_earrings,
			piercing,
		]
	},
	
	//Žanet
	zan: [ //a bit slut
		groups.f_bra,
		groups.f_panties,
		groups.f_socks,
		groups.f_earrings_sexy,
		groups.necklace,
		["longTubeTop", "sleevedTubeTop", "sexyHalterTop", "tightTee", "tightTee"],
		["miniSkirt", "miniSkirt"],
		"polyGlasses",
		["hipsters", "thong"],
		"nosePiercing",
	],
	
	//Peter 
	pet(){ //cool
		return [...this.male_generic, "suit"];
	},
	
	//Světa - poledancer
	sve: [ //a bit trashy
		groups.f_earrings_sexy,
		["halterTop", "tightTee"],
		["thong", "hiThong", "gString", "stringThong"],
		"animalLeggings",
		groups.f_socks,
	],
	
	//Bára - nerd's cousin
	bar: [ //a bit slut
		groups.f_bra,
		groups.f_socks,
		"tightTee", 
		"miniSkirt",
		"hiThong",
		"noseRing",
		"eyebrowPiercingLeft",
		"polyBiChain",
		"loopEarrings",
	],
	
	//Justýna - woman from bar
	jus: [
		"neutralSocks",
		"blackTee", 
		"orangeSneakers",
		"hipsters",
		"tightJeans",
		"metalSimpleChain",
		"ballEarrings",
	],
	
	//Majerová - literature teacher
	maj: [ //old teacher like
		groups.f_bra,
		"briefs",
		"darkTights", 
		//blouse
		"sweater",
		"pencilSkirt",
		"kittyHeels",
		"glasses",
	],
	
	//Bosáková - gym teacher
	ped: [ //sporty clothes
		groups.f_bra,
		groups.f_panties,
		groups.f_socks,
		groups.sneakers,
		["gymLeggings","leggings"],
		["halterTop","longHalterTop"],
	],
	
	
	
//FANTASY CHARACTERS
	
	//Yelaya
	yel: [
		"spear",
		"fShirt",
		"fPants",
	],
	
	//Galawar
	gal: [ 
		"rustyCutlass",
		"fShirt",
		"fPants",
		"fFancyWideBelt",
		"fBoxers",
	],
	
	//Stede
	ste: [ 
		"crackedMace",
		"fShirt",
		"fPants",
		"fDarkWideBelt",
		"fBoxers",
	],
	
	//Lyssa
	lys: [
		"rustyCutlass",
		"harlotTop",
		"fLeggings",
		"fDarkThinBelt",
		"fThong",
	],
	
	//Sirael
	sir: [
		"siraelTop", "siraelBottom", "siraelCollar",
	],
	
	sirGreen: [
		"siraelTopGreen", "siraelBottomGreen", "siraelCollarGreen",
	],
	
	
	
//GENERIC FANTASY NPC's
	noblewoman: [ //fancy
		"nobleGown",
		"harlotStockings",
		["doubleNecklace", "multiNecklace", "tearNecklace"],
	],
	
	rags: [
		"clothLoincloth",
	],
	
	maid: [
		"fBlouse",
		"harlotStockings",
		"maidSkirt",
	],
	
	commoner: [
		"fShirt",
		"fPants",
	],

	guard: [
		"fShirt",
		"bronzeCuirass",
		"fPants",
	],
	
	//Gothred Guard
	gothGuard: [
		"gothShirt",
		"steelCuirass",
		"fPants",
	],
	
	gothOfficer: [
		"gothShirt",
		"gothCuirass",
		"gothPauldrons",
		"fPants",
		"fDarkWideBelt",
	],
	
	gothGeneral: [
		"gothShirt",
		"gothCuirass",
		"gothGoldPauldrons",
		"fPants",
		"fFancyWideBelt",
	],
	
	goth: [
		"fantasyShirt",
		//"bronzeCuirass",
		"fPants",
	],
	
	pirate: [
		"fShirt",
		"leatherCuirass",
		"fPants",
	],
	
	captain: [
		"fShirt",
		"leatherCuirass",
		"fPants",
		"fDarkWideBelt",
	],
	
	harem: [
		"haremTop","thong","haremSkirt",
	],
	
	mage: [
		"mageRobe","mageHat",
	],
	
	
//GAME STORY
	//initial clothes worn by the PC
	initial: [
		"hipsters",
		"cageBra",
		"socks",
		"jeans",
		["tightTee","dualTee","tee"],
		"orangeSneakers",
		["ballEarrings","crystalEarrings","triangleEarrings","rhombEarrings"],
		"bellyPiercing",
	],
	
	
	//bdsm gear - TODO
	leatherCorset: [
		"bdsmCorset","bdsmThong","bdsmGloves","bdsmBoots","bdsmChoker",
	],
	
	latexCatsuit: [
		"bdsmChoker", "bdsmCatsuit","bdsmHeels",
	],

	leatherMinidress: [
		"bdsmGorget", "leatherMinidress","bdsmStockings","bdsmHeels",
	],
	
	latexMinidress: [
		"bdsmChoker", "latexMinidress","bdsmStockings","bdsmBoots",
	],
	
	cybrex: [
		"cybrexPants", "cybrexTee",
	],
	
	chardGirl: [
		"chardBikiniTop", "chardBikiniBot",
	],
	
	chardBoy: [
		"chardTrunks",
	],
	
	stockingsSlave: [
		"slaveCollar", "garterStockings",
	],
	
	skimpyAI: [
		"bdsmChoker", "garterStockings", "cageBra", "gString",
	],
};












