
/*
	key: {
		name,
		icon,
		id, //id is id of bank account (used in system/bank_account.js) - unique values between 50 and 99
		quality,
		items,
		max, //maximal number of items in the shop
		quality, 
			//default 0
			//cheap 1 
			//discount 2
			//fine 3
			//brand 4
			//premium 5
			//designer 6
	},
*/
export const shops = {
	test: {
		name: `Test`,
		icon: `🚧`,
		id: 50,
		items: `test`,
		quality: [3,4,4,5,6],
	},
	
	test2: {
		name: `Test 2`,
		icon: `🚧`,
		id: 50,
		items: `test2`,
		quality: 0,
	},
	
	
	test3: {
		name: `Test 3`,
		icon: `🚧`,
		id: 50,
		items: `test3`,
		quality: 0,
	},
	
	test4: {
		name: `Test 4`,
		icon: `🚧`,
		id: 50,
		items: `test4`,
		quality: 0,
	},
	
	test5: {
		name: `Test 5`,
		icon: `🚧`,
		id: 50,
		items: `test5`,
		quality: 0,
	},
	
	testShop: {
		name: `Test shop`,
		icon: `🚧`,
		id: 51,
		quality: 0,
		items: "testShop", 
	},
	
	accessories: {
		name: `Fashion Accessories`,
		note: `belts, jewellery, etc.`,
		icon: `👒`,
		id: 52,
		quality: [2,3,3,4,5],
		items: [`fashion_accessory`, `jewellery`],
	},
	
	gold: {
		name: `Goldsmith's Shop`,
		note: `expensive jewellery`,
		icon: `💍`,
		id: 53,
		quality: [4,5,6],
		items: [`precious_jewellery`],
	},
	
	optician: {
		name: `Optician`,
		note: `glasses`,
		icon: `👓`,
		id: 54,
		quality: [3,4,4,5,6],
		items: `optician`,
	},
		
	legwear: {
		name: `Legwear`,
		note: `socks & stockings`,
		icon: `🧦`,
		id: 55,
		quality: [2,3,3,4],
		items: [
			`socks_legwear`,
		],
	},
	
	lingerie: {
		name: `Naughty Lingerie`,
		note: `underwear`,
		icon: `👙`,
		id: 56,
		max: 25,
		quality: [3,3,4,4,5,6],
		items: [
			`socks_lingerie`,
			`underwear_lingerie`,
		],
	},
	
	jewellery: {
		name: `Jewellery & Bijoux`,
		note: `jewellery`,
		icon: `💎`,
		id: 57,
		items: [`piercings`, `bijoux`],
	},
	
	alt: {
		name: `Alternative Fashion`,
		note: `goth/emo/punk stuff`,
		icon: `💀`,
		id: 58,
		max: 25, //TODO
		quality: [3,3,4,5],
		items: [
			`piercings`, 
			`socks_alt`, 
			`accessory_alt`,
			`outer_alt`,
			`upper_alt`,
			`underwear_alt`,
		],
	},
	
	shoes: {
		name: `Shoes`,
		note: `shoes`,
		icon: `👠`,
		id: 59,
		max: 20, //BEWARE
		quality: [2,3,3,4,4,5],
		items: [`shoes`],
	},
	
	second: {
		name: `Second-hand`,
		note: `various pre-owned clothes`,
		icon: `👖`,
		id: 61,
		max: 40, //BEWARE
		quality: [1,1,2,3],
		items: [
			`lower_second`,
			`outer_second`,
			`upper_second`,
		],
		options: {
			tax: 0.5, //allows selling things for half of the price
		},
	},
	
	discount: {
		name: `Discount store`,
		note: `cheap simple clothes`,
		icon: `🎽`,
		id: 62,	
		quality: 2,
		max: 50, //BEWARE
		items: [
			`underwear_discount`,
			`socks_discount`,
			`lower_discount`,
			`upper_discount`,
		],
	},
	
	fa: {
		name: `F&A Fashion`,
		note: `affordable average clothes`,
		icon: `👕`,
		id: 63,
		max: 60, //BEWARE
		quality: [2,3,3,4],
		items: [
			`accessory_section`, 
			`underwear_fa`,
			`socks_fa`,
			`lower_fa`,
			`upper_fa`,
		],
	},
	
	boutique: {
		name: `Boutique`,
		note: `expensive fancy clothes`,
		icon: `👗`,
		id: 64,
		max: 50, //BEWARE
		quality: [4,5,6],
		items: [
			`accessory_boutique`, 
			`underwear_boutique`,
			`socks_boutique`,
			`lower_boutique`,
			`upper_boutique`,
			`outer_boutique`,
		],
	},
	
	bimbo: {
		name: `Bimbo Fashion`,
		note: `bimbo clothes`,
		icon: `🎀`,
		id: 65,
		quality: [3,4],
		max: 25, //TODO
		items: [
			`upper_bimbo`, 
			`lower_bimbo`,
			`underwear_bimbo`,
			`socks_bimbo`,
			`jewellery_bimbo`,
			`accessory_bimbo`,
		],
	},
	
	piercing: {
		name: `Piercing Parlor`,
		//note: `bimbo clothes`,
		icon: `♦️`,
		id: 66,
		//quality: [3,4],
		items: [
			`piercings`,
		],
	},
	
	vr_shop_lingerie: {
		name: `Virtual Shop`,
		note: `underwear`,
		icon: `🛍️`,
		id: 67, //vr_shop!!
		max: 18,
		quality:  3,
		items: [
			`underwear_vr_shop`,
		],
	},
	
	vr_shop_upper: {
		name: `Virtual Shop`,
		note: `upper`,
		icon: `🛍️`,
		id: 67, //vr_shop!!
		max: 15,
		quality:  3,
		items: [
			`upper_second`,
		],
	},
	
	vr_shop_lower: {
		name: `Virtual Shop`,
		note: `lower`,
		icon: `🛍️`,
		id: 67, //vr_shop!!
		max: 15,
		quality:  3,
		items: [
			`lower_vr_shop`,
		],
	},
	
//FANTASY SHOPS
	feyd: {
		name: ``,
		icon: ``,
		id: 0,
		items: [
			`weapons_1_low`,
			`weapons_1_mid`,
			`clothes_default`,
			`clothes_common`,
			`hp_eng`,
		],
	},
	
	goth: {
		name: ``,
		icon: ``,
		id: 0,
		items: [
			`weapons_1_mid`,
			`leather_all`,
			`fancyLeather_low`,
			`clothes_default`,
			`clothes`,
			`gothred`,
			`hp_eng`,
			`hp_eng`,
			`elixir_1`,
		],
	},
	
	dorf: {
		name: ``,
		icon: ``,
		id: 0,
		items: [
			`fancyLeather_all`,
			`steel_low`,
			`clothes`,
			`accessory`,
			`accessory_belts`,
			//TODO CHOKERS
		],
	},
	
	
	

//STORY SHOPS



};