/*
tightTee

*/

export const titles = {
	armCoverage: "sleeves",
	armLoose: "sleeves width",
	
	cleavageCoverage: "cleavage depth",
	//waistCoverage: "waist coverage",
	waistCoverageLower: "thickness",
	chestCoverage: "chect coverage",
	topStrap: "top strap",
	
	alpha: "opacity",
	
	topCurve : "top curve",
	topBandWidth: "top band",
	curveBotX: "bottom curve", 
	
	cupCoverage: "cups",
	botStrapCoverage: "bottom strap",
	
	cleavageOpeness: "cleavage width",
	outerNeckCoverage: "shoulder coverage", 
	neckCoverage: "shoulder coverage", 
	
	ruffling: "lace", 
	cageSpacing: "spacing",
	cageCenter: "centered cage",
	cageCross: "crossed cage",
	cageDouble: "double",
	
	outerLoose: "width",
	waistCoverage: "waist",
	legCoverage: "length",
	bustle: "bustle", //TODO
}

export const designs = {
	designTee: [
		"cleavageOpeness",
		"cleavageCoverage",
		"waistCoverage", 
		"armCoverage",
	],
	
	designSexyTee: [
		"alpha",
		"cleavageOpeness",
		"cleavageCoverage",
		"waistCoverage", 
		"armCoverage",
	],
	
	
	
	designHalterTop: [
		"outerNeckCoverage",
		"cleavageCoverage",
		"waistCoverage", 
	],
	
	designSexyHalterTop: [
		"alpha",
		"outerNeckCoverage",
		"cleavageCoverage",
		"waistCoverage", 
	],
	/*
	designSluttyHalterTop: [
		"outerNeckCoverage",
		"cleavageCoverage",
		"waistCoverage", 
	],
	*/
	designTubeTop: [
		"armCoverage",
		"armLoose",
		"waistCoverage", 
	],
	
	designSexyTubeTop: [
		"alpha",
		"armCoverage",
		"armLoose",
		"waistCoverage", 
	],
	
	designCageBra: [
		"neckCoverage", 
		"botStrapCoverage",
		"ruffling",
		"cageCenter",
		"cageCross",
		"cageDouble",
		"cageSpacing",
			
	],
	
	designPushUpBra: [
		"neckCoverage",
		"botStrapCoverage",
		"cupCoverage",
		"ruffling",
		
	],
	
	designPanties: [
		"waistCoverage",
		"waistCoverageLower",
		"topCurve",
		"topBandWidth",
		"bow",
		"ruffling",
	],
	
	designThong: [
		"waistCoverage",
		"waistCoverageLower",
		"topCurve",
		//"curveBotX",
		"bow",
		"ruffling",
	],
	
	
	designSkirt: [
		"outerLoose",
		"waistCoverage",
		"legCoverage",
		"bustle",
	],
	
	designMicroSkirt: [
		"outerLoose",
		"waistCoverage",
		"legCoverage",
	],
	
}
