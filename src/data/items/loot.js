import {rewards} from "./prices";
import {fantasy} from "./lists/fantasy";




const temp_loot = {
	
	//FEYD
	act_1: {
		armor: [
			...fantasy.accessory,
		],
		weapon: [
			...fantasy.weapons_1_low,
			...fantasy.weapons_1_mid,
		],
		clothes: [
			...fantasy.clothes_default,
			...fantasy.clothes_rare,
		],
		elixir: [
			...fantasy.hp_eng,
			...fantasy.elixir_1,
		],
		legendary: [
			...fantasy.magic_dagger,
			...fantasy.weapons_1_high,
			...fantasy.jewellery_low,
		],
		money: rewards.act_1,
	},
	
	yelaya: {
		legendary: [
			...fantasy.jewellery,
		],
	},
	
	tutorial: {
		weapon: [
			"knife",
		],
		elixir: [
			"xHP",
		],
	},
	
	//GOTH
	act_2: {
		armor: [
			...fantasy.leather_all,
			...fantasy.leather_all,
			...fantasy.fancyLeather_all,
		],
		weapon: [
			...fantasy.weapons_1,
			...fantasy.weapons_2_low,	
			...fantasy.weapons_2_mid,
		],
		clothes: [
				...fantasy.clothes_default,
				...fantasy.clothes_rare,
				...fantasy.clothes_fancy,
				...fantasy.accessory,
				...fantasy.accessory_fancy,
			],
		elixir: [
			...fantasy.hp_eng,
			...fantasy.elixir_2,	
			"charmBlizzard","charmFirestorm", //TODO
		],
		legendary: [
			"magicPanties",
			...fantasy.magic_wand,
			...fantasy.fancyLeather_magic,
			...fantasy.steel_low,
			...fantasy.jewellery,
			...fantasy.jewellery_magic,
		],
		money: rewards.act_2,
	},
	
	aquilonian: {
		armor: [
			...fantasy.bronze_all,
		],
		elixir: [
			...fantasy.hp_eng,
			...fantasy.elixir_2,	
			"xSuc", "xSuc",
			"charmBlizzard","charmFirestorm", //TODO
		],
		legendary: [
			...fantasy.bronze_magic,
			...fantasy.jewellery_magic,
		],
	},
	
	shipwreck: {
		legendary: [
			"magicPanties",
			"magicFancyLeatherPauldrons", "magicFancyLeatherVambraces", //TODO
			"steelCuirass", "magicTearNecklace",//TODO - add more
		],
		money: rewards.act_2_shipwreck,
	},
	
	
}




export const loot = [];
//because some items might be in format [2,"blueprint"] 
//TODO THIS IS STUPID but I am to tired and dont care; fuck this
Object.keys(temp_loot).forEach( act => {
	loot[act] = {};
	Object.keys(temp_loot[act]).forEach( typ => {
		loot[act][typ] = [];
		temp_loot[act][typ].forEach( item => {
			if( Array.isArray(item) && !isNaN(item[0]) ){
				for(let i = 0; i < item[0]; i++){
					loot[act][typ].push(item[1]); 
				}
			}else{
				loot[act][typ].push(item);
			}
		})
	})
});
