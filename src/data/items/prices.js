/**
	prices of all things in the game
*/

export const modifiers = {
	//MASTERS 
	get master(){return head.reality === 3 ? SETTING.game_economy : SETTING.irl_economy}, //prices of all items are multiplied by the master; to make global economical changes
	
	//TODO - not sure if those modifiers are actually used
	elixirs: 1, //prices of all elixirs are multiplied by this
	weapons: 1, //of all weapons
	charms: 1, //or all charms (pre-made spells usable even by non-wizzards)
	armors: 1, //of all armors
	
	//material modifiers (the price of item is multiplied by the modifier)
	//BEWARE armors, rare leather, gold items, etc. should have the same base price because the price difference is caused by the material modifier!
	
	//quality 
	quality: [
		1, //default 0
		0.25, //cheap 1 
		0.5, //discount 2
		1, //fine 3
		2, //brand //4
		5, //premium //5
		10, //designer //6
		3, //created //7
	],
	
	//trinkets
	iron: 2,
	copper: 1,
	
	//precious metals
	silver: 5,
	electrum: 8,
	roseGold: 10,
	gold: 12,
	mithril: 20,
	

	irlSilver: 1,
	irlGold: 15,
	irlRoseGold: 10, 
	/*
	irl14Gold: 47, //0.585
	irl18Gold: 60, //0.75
	irl24Gold: 80,
	irlRoseGold: 53, //0.75% gold => 60 but copper is cheaper than silver
	*/
	
	//mainly armor
	bronze: 2.8,
	mail: 4.5,
	steel: 5,
	glass: 7,
	paladin: 8,
	gilded: 9,
	alloy: 12,
	succubi: 11,
	demonic: 10,
	/*
	bronze: 1,
	mail: 1.5,
	steel: 2,
	glass: 2.5,
	paladin: 3,
	gilded: 3.5,
	alloy: 4.25,
	succubi: 4.5,
	demonic: 4,
	*/
	
	//leather clothes
	leather: 1,
	wyve: 1.9, //wyvern leather
	hexa: 1.8, //hexapuma
	levi: 1.7, //leviathan
	
	//raw undyed fabric
	linen: 0.8, 
	hemp: 0.8,
	nettle: 0.8,
	
	//weapons adjustements
	ancient: 2,
	engraved: 1.8,
	rusty: 0.8,
	cracked: 0.8,
	bent: 0.8,
	sharp: 1.4, 
	heavy: 1.4, 
	accurate: 1.4, 
	masterwork: 1.6,
	
}


//TODO!!!
//one bonus attribute (e.g. +1 dex) costs
/*
	the price rises exponentially
	eg:
	+1 => x1
	+2 => x2
	+3 => x4
	+4 => x16
*/
export const primary_attributes_prices = {
	str: 50,
	dex: 60,
	int: 50,
	char: 50,
}

export const secondary_attributes_prices = { //TODO!!
	hp: 15,
	eng: 15,
	evasion: 30,
	lockpick: 24,
	reflex: 21,
	//presence: 20,
}


export const money = {
	game_initial: 0, //initial amount of coins in the virtual game
	irl_initial: 189, //initial cash on your IRL bank account
	pocket_money: 100, //weekly received pocket money from parents 
	pocket_money_increased: 160, //pocket money if good behaviour
	pocket_money_decreased: 60, //pocket money for bad behaviour 
	
	karaoke: 100, //6 hours of work, 150 kč per hour; and they were in trouble
	karaoke_tips: 80, //dunno
	karaoke_tits: 40, //500 kč, seems right for rich assholes
	plant_manager: 40, //TODO? 400 kč seems right
	promo_mall: 70,  //TODO - 4 x 200 = 800 = 36
	promo_mall_extra: 30,  //
	trade_show: 140, //dunno
	trade_show_extra: 20, //dunno
	farmers_market: 200, 
	
	cucumber: 22, 
	dye: 4, 
	boobs_total: 3000, //total price of surgery 
	boobs: 100, //weekly price of surgery
	dildo: [
		40, //nerd money for dildo
		34, //small 1
		42, //glass 2
		50, //realistic 3
		60, //huge 4 
		74, //fantasy 5
		86, //dragon 6
		98, //horse 7
	],
	buttplug: [
		24, //0 small default
		40, //1 medium/big steel
	],
	strapon: [
		0, //empty
		55, //TODO - RANDOM TEMP NUMBER
		68,
		92, //TODO
	],
	chastity_cage: 30, //TODO random
	striptease: 200, //basic striptease around 2500czk/100euro + Světa kept her share - but would be weird to make less than trade show?
	//4000czk vibrator; 6000czk lesbian show
	stripshow: 380, //4000czk ??
	stripshow_lapdance: 70,


	requiem_shot: 5,
	bj_low: 40,
	bj_mid: 60,
	bj_high: 100,
	
	chair_bj_low: 30, //TODO - NO FUCKING IDEA
	chair_bj_mid: 40,
	chair_bj_high: 60,
	
	
	tattoo_first: 20,
	tattoo: 40,
	tattoo_claim: 60,
	
	a_family_bj: 50, //dunno
	a_family_sex: 120, //dunno
	
	shop_gold: 750, //unlocks luxury stores 

	haircut: 40, // 500czk-, could be over 1000 dunno
	hairext: 120, //1000czk; actually is 10.000czk+
	hairdying: 80, //866czk (it is actually around 1500)
}


//rewards for quests, jobs and other activites; base values, often adjusted by further modifiers 
export const rewards = {
	//looted money [min, max]
	act_1: [10, 30],
	act_2: [10, 60],
	act_2_shipwreck: [60, 140],
	
	//Feyd Mora
	feyd_rats: 30, 
	feyd_roses: 30,
	feyd_smuggler: 30,
	
	//Gothred
	goth_ghost_ship: 90,
	goth_goblins: 80,
	goth_smugglers: 80,
	goth_manor: 76,
	goth_gal_money: 20, //money you can borrow from Galawar
	arm_wrestling: 14,
	dancing: 40, 
	whoring: 50,
	goth_dorf: 140, //reward for lumber
	sir_probed: 60, //Sirael probed in Duradudos
}
	



//because there are many similar items; to change the price of them all at the same time
export const groups = {
	
	
	
	
	
	//potions
	elixir: 40,

	//accessory
	fallenTop: 42,
	fantasyTop: 30, //TODO
	loincloth: 36,
	barbaricBra: 32,
	bodice: 38,
	
	
	//armor
	cuirass: 128,
	bikini: 92,
	
	pauldrons: 76,
	cingulum: 64,
	
	vambraces: 56,
	greaves: 48,
	gorget: 40,
	
	//weapons
	blades: 90,
	hammers: 80,
	whips: 84,
	polearms: 70, 
	bows: 76, 
	wands: 120, 

	
	
//UNI
	//also, I fogot Amazon prices don't include VAT, which is 20%, so some prices are even wronger
	//the numbers in comments are real, the actual used numbers are x2 because of future inflation

	//underwear
	bra: 20, //15 ? good ones go for 40 - 60 usd; even 300; czech normal 150-400 czk
	panties: 10, //seems like 2.5 - 10; expensive 25
	
	//socks
	socks: 2, //maybe 2? 
	highSocks: 14, //lower sockier 5 - 8? fancier stockingier 10+ even 20+; czech average 8?
	stockings: 17, //cheapest 8-9, nice 18?; cz cheapest 6, avg 9
	garterStockings: 30, //garter belt alone is 8+
	tights: 10, //dunno, something like pantyhose
	pantyhose: 10, //3 cheapest; ordinary averages 7; thicker with pattern 9
	sexyPantyhose: 18, //dunno, something like pantyhose
	
	//upper
	tee: 24, 
	top: 24, //TODO - same price as tee
	/*
		second hand  +40 czk -> 1.5 euro
		lidl 90-120 czk -> +4 euro
		tesco f&f 130 czk -> +5 euro
		alleged average price of printed shirt  +25 usd 
		alleged  premium +50 usd
	*/
	//top 9 - 35 - up to 60
	//long sleeves nice around 30
	blouse: 48,
	
	//lower 
	pants: 48,
	leggings: 42, //340 - 1200
	shorts: 36,   //400-700
	
	skirt: 46, //15 - 40, commonly even 100 and even more

	//upperLowever
	dress: 60, //cheap seems to be 25
	
	//shoes
	shoe: 100, //lot for crappy shoes, not enough for good sneakers;  MSRP price of sneakers in 2021 is $110.15 while the average available lowest price is $66.75 -> 50 * quality = 100 so I guess its fine
	boot: 120, //dunno
	
	//accessory
	glasses: 60, //it should be way more
	belt: 20,
	choker: 12, //you can get a cheap one under 1 buck; nice 10-12 
	gloves: 20, //TODO
	
	//jewellery
	earrings: 12, //no fucking clue
	piercing: 14, //seems to be 90 - 300 czk -> 4 - 12
	necklace: 10, //no fucking clue
	
	//TODO
	precious: 10, //price boosted by quality!!!
	//precious: 22, //price for silver piece
	
	blazer: 84, //35 - 50 cheap

	
}	




export const prices = {

	base: 1, //when not defined //TODO!
	
//USER DESIGNS
	designCageBra: groups.bra * 1.1,
	designPushUpBra: groups.bra * 1.1,
	designPanties: groups.panties * 1,
	designThong: groups.panties * 1.3,
	designGString: groups.panties * 1.3,
	
	designTubeTop: groups.tee,
	designSexyTubeTop: groups.tee * 1.1,
	
	designHalterTop: groups.tee,
	designSexyHalterTop: groups.tee * 1.1,
	
	designTee: groups.tee,
	designSexyTee: groups.tee * 1.1,
	
	designSkirt:  groups.skirt,
	designMicroSkirt:  groups.skirt * 0.9,
	
//UNDERWEAR 
	//bra
	simpleBra: groups.bra * 0.9,
	simpleCageBra: groups.bra * 0.9,
	simplePushUpBra: groups.bra * 0.9,
	cageBra: groups.bra,
	pushUpBra: groups.bra,
	sexyCageBra: groups.bra * 1.1,
	sexyPushUpBra: groups.bra * 1.1,
	animalCageBra: groups.bra * 1.15,
	animalPushUpBra: groups.bra * 1.15,
	bimboCageBra: groups.bra * 1.1,
	bimboPushUpBra: groups.bra * 1.1,
	
	briefs:  groups.panties * 1,
	hipsters: groups.panties * 1,
	boyshorts: groups.panties * 1,
	simpleBriefs: groups.panties * 0.9,
	simpleHipsters: groups.panties * 0.9,
	dividedPanties: groups.panties * 1.2,
	
	thong: groups.panties * 1.3,
	hiThong: groups.panties * 1.3,
	gString: groups.panties * 1.3,
	cagePanties: groups.panties * 1.4,
	
	crotchlessPanties: groups.panties * 1.5,
	crotchlessThong: groups.panties * 1.5,
	
	animalHipsters: groups.panties * 1.1,
	animalThong: groups.panties * 1.4,
	animalGstring: groups.panties * 1.5,
	
	bimboHipsters: groups.panties * 1.1,
	bimboThong: groups.panties * 1.4,
	bimboGString: groups.panties * 1.5,
	bimboCrotchlessThong: groups.panties * 1.6,
	bimboCagePanties: groups.panties * 1.6,
	
	
	blackHipsters: groups.panties,
	blackThong: groups.panties,
	blackCagePanties: groups.panties,
	blackCageBra: groups.bra,
	/*

	maleBriefs:
	boxers:
	*/
	
//SOCKS
	//socks
	socks: groups.socks,
	neutralSocks: groups.socks,
	bimboSocks: groups.socks * 1.1,
	
	//high socks
	lightHighSocks: groups.highSocks,
	darkHighSocks: groups.highSocks,
	brightHighSocks: groups.highSocks,
	stripedHighSocks: groups.highSocks * 1.3,
	stripedDarkHighSocks: groups.highSocks * 1.3,
	
	//stockings
	stockings: groups.stockings,
	polyStockings: groups.stockings,
	fishnetStockings: groups.stockings,
	semiFishnetStockings: groups.stockings * 1.1,
	semiFishnetPolyStockings: groups.stockings * 1.1,
	
	bimboStockings: groups.stockings * 1.2,
	
	garterStockings: groups.garterStockings,
	polyGarterStockings: groups.garterStockings,
	fishnetGarterStockings: groups.garterStockings,
	bimboGarterStockings: groups.garterStockings * 1.1,
	

	//pantyhose
	lightTights: groups.tights,
	darkTights: groups.tights,
	brightTights: groups.tights,
	pantyhose: groups.pantyhose,
	polyPantyhose: groups.pantyhose * 1.1,
	nylonPantyhose: groups.pantyhose * 0.9,
	semiFishnetPantyhose: groups.pantyhose * 1.1,
	bimboPantyhose: groups.pantyhose * 1.1,
	
	openLightTights: groups.sexyPantyhose,
	openDarkTights: groups.sexyPantyhose,
	fishnetPantyhose: groups.sexyPantyhose,
	openFishnetPantyhose: groups.sexyPantyhose,
	crotchOpenFishnetPantyhose: groups.sexyPantyhose,
	bimboOpenPantyhose: groups.sexyPantyhose,
	
//LOWER 
	
	longSkirt: groups.skirt,  //TODO - modifiers for all skirts to be adjusted
	pencilSkirt: groups.skirt * 1.1,
	shortSkirt: groups.skirt,
	miniSkirt: groups.skirt,
	microSkirt: groups.skirt * 0.95, 
	nanoSkirt: groups.skirt * 0.9,
	
	animalMicroSkirt: groups.skirt * 1.2,
	pleatedShortSkirt: groups.skirt * 1.1,
	pleatedMiniSkirt: groups.skirt * 1.1,
	pleatedMicroSkirt: groups.skirt,
	
	jeans: groups.pants,
	tightJeans: groups.pants,
	pants: groups.pants,
	leggings: groups.leggings,
	gymLeggings: groups.leggings,
	animalLeggings: groups.leggings * 1.2,
	sluttyJeans: groups.pants * 1.1,
	
	jeansShorts: groups.shorts,
	sexyJeansShorts: groups.shorts,
	shorts: groups.shorts,
	sexyShorts: groups.shorts,

	bimboMiniSkirt: groups.skirt,
	bimboMicroSkirt: groups.skirt * 0.95,
	bimboBorderlineSkirt: groups.skirt * 0.975,
	bimboNanoSkirt: groups.skirt * 0.9,
	
	blackBorderlineSkirt: groups.skirt * 0.975,
	blackPleatedSkirt: groups.skirt,
	
//UPPER 
	longTubeTop: groups.tee,
	tubeTop: groups.tee,
	sluttyTubeTop: groups.tee * 1.1,
	sleevedTubeTop: groups.tee * 1.25,
	transparentTubeTop: groups.tee * 1.1,
	bimboTubeTop: groups.tee * 1.2,
	
	triangleTop: groups.tee * 1.33,
	botOpenTop: groups.tee * 1.4,
	bimboBotOpenTop: groups.tee * 1.5,
	
	longHalterTop: groups.tee,
	halterTop: groups.tee,
	sluttyHalterTop: groups.tee * 1.1,
	deepHalterTop: groups.tee * 1.1,
	
	tee: groups.tee,
	tightTee: groups.tee,
	bimboTee: groups.tee * 1.25,
	niceTightTee: groups.tee * 1.5,
	stripedTightTee: groups.tee * 1.25,
	sluttyTee: groups.tee * 1.1,
	niceSluttyTee: groups.tee * 1.3,
		
	longTee: groups.tee,
	deepTee: groups.tee * 1.1,
	sheerTee: groups.tee * 1.2,
	
	dualTee: groups.tee * 1.25,
	sluttyDualTee: groups.tee * 1.25,
	fishnetDualTee: groups.tee * 1.3,
	blackTee: groups.tee,
	fishnetTee: groups.tee,
	blackNiceTee: groups.tee * 1.1,
	
	turtleTop: groups.tee * 1.3,
	
	shrugTop: groups.blouse * 1.1,
	fishnetShrugTop: groups.blouse * 1.1,
	
	
	shrug: groups.blazer * 0.75,
	fishnetShrug: groups.blazer * 0.6,
	blackFishnetShrugTop: groups.blazer * 0.6,
	
	corsetTop: groups.blazer * 0.75, //TODO corset
	blackCorsetTop: groups.blazer * 0.75, //TODO corset
		
	bimboNiceTee: groups.tee,
	bimboTriangleTop: groups.tee,
	bimboSluttyTubeTop: groups.tee,
	
//SHOES 
	blackSneakers: groups.shoe,
	orangeSneakers: groups.shoe * 1.05,
	whiteSneakers: groups.shoe * 0.95,
	
	//heels
	kittyHeels: groups.shoe,
	highHeels: groups.shoe,
	
	//boots
	orangeBoots: groups.boot,
	darkBoots: groups.boot,
	polyBoots: groups.boot,
	
	darkHighBoots: groups.boot * 1.3,
	polyHighBoots: groups.boot * 1.3,

	



//ACCESSORY
	//glasses
	glasses: groups.glasses,
	polyGlasses: groups.glasses,
	
	//belts
	darkWideBelt: groups.belt * 1.1,
	polyWideBelt: groups.belt * 1.1,
	darkThinBelt: groups.belt * 0.9,
	polyThinBelt: groups.belt * 0.9,
	
	fDarkWideBelt: groups.belt * 1.1,
	fPolyWideBelt: groups.belt * 1.1,
	fFancyWideBelt: groups.belt * 1.1,
	fDarkThinBelt: groups.belt * 0.9,
	fPolyThinBelt: groups.belt * 0.9,
	fFancyThinBelt: groups.belt * 0.9,
	
	//chokers
	choker: groups.choker,
	choker2: groups.choker,
	crossedChoker: groups.choker,
	
	polyCrossedChoker: groups.choker * 1.1,
	blackChoker: groups.choker * 1.2,
	bimboChoker: groups.choker * 1.5,
	bimboCollar: groups.choker * 2, //TODO
	
	slaveCollar: groups.choker * 2, //TODO
	everydayCollar: groups.choker * 2, //TODO
	
	//gloves
	fingerlessGloves: groups.gloves,
	sleeves: groups.gloves,
	fishnetSleeves: groups.gloves,
	longGloves: groups.gloves,
	
	fChoker: groups.choker,
	fLeatherChoker: groups.choker, //price adjusted by material
	fFancyChoker: groups.choker, //price adjusted by material
	fMetalChoker: groups.choker, //price adjusted by material
	
//JEWELLERY
	//earrings
	ballEarrings: groups.earrings,
	crystalEarrings: groups.earrings,
	triangleEarrings: groups.earrings,
	rhombEarrings: groups.earrings,
	chainEarrings: groups.earrings,
	loopEarrings: groups.earrings,
	bimboLoops: groups.earrings,
	
	//necklaces
	polyBiChain: groups.necklace,
	biChain: groups.necklace,
	metalSimpleChain: groups.necklace * 0.8,
	polySimpleChain: groups.necklace,
	metalTearNecklace: groups.necklace,
	tearNecklace: groups.necklace,
	doubleNecklace: groups.necklace * 1.9,
	multiNecklace: groups.necklace * 2.2,
	beadNecklace: groups.necklace * 1.6,
	
	preciousSimpleChain: groups.precious * 0.8,
	preciousTearNecklace: groups.precious,
	preciousDoubleNecklace: groups.precious * 1.9,
	preciousMultiNecklace: groups.precious * 2.2,
	
	//piercings
	bellyPiercingSimple: groups.piercing,
	bellyPiercing: groups.piercing * 1.8,
	nosePiercing: groups.piercing,
	noseBridge: groups.piercing,
	noseRingSimple: groups.piercing,
	noseRing: groups.piercing,
	noseChain: groups.piercing,
	nippleBars: groups.piercing,
	multipleNipple: groups.piercing * 1.8,
	nippleRings: groups.piercing,
	nippleShields: groups.piercing * 1.3,
	nippleStarShields: groups.piercing * 1.3,
	nippleChain: groups.piercing,
	VCHPiercing: groups.piercing,
	HCHPiercing: groups.piercing,
	christinaPiercing: groups.piercing,
	labiaPiercing: groups.piercing,
	labiaPiercings: groups.piercing,
	tonguePiercing: groups.piercing,
	tongueSlaveRing: groups.piercing,
	eyebrowPiercingLeft: groups.piercing,
	eyebrowPiercing: groups.piercing,
	labret: groups.piercing,
	medusa: groups.piercing,
	monroeRight: groups.piercing,
	monroe: groups.piercing,
	verticalNippleBars: groups.piercing,
		
	//fantasy jewellery
	magicTearNecklace: groups.necklace, //actual price based on material and bonus attributes!
	minorMagicTearNecklace: groups.necklace,
	majorMagicTearNecklace: groups.necklace,
	fTearNecklace: groups.necklace,
	fMultiNecklace: groups.necklace,
	
	
	
	//IRL
	
	
	//BELTS
	
	
	//potions
	xHP: groups.elixir * 0.8,
	xEng: groups.elixir * 0.8,
	xElf: groups.elixir,
	xGob: groups.elixir,
	xHum: groups.elixir * 1.1,
	xSuc: groups.elixir * 1.2,
	xMin: groups.elixir * 1.1,
	
	//charms
	charmLightning: 13,
	charmBlizzard: 19,
	charmFirestorm: 21,
	charmBlades: 12,
	charmDazzle: 8,
	charmDarkness: 10,
	
	//underwear
	fBlandPaties: groups.panties,
	fBlandThong: groups.panties * 0.9,
	fBlandBoxers: groups.panties * 1.2,
	fBlandBra: groups.bra,

	fPaties: groups.panties,
	fThong: groups.panties * 0.9,
	fBoxers: groups.panties * 1.2,
	fBra: groups.bra,
	magicPanties: groups.panties,
	
	//socks
	defHarlotStockings: groups.stockings,
	harlotStockings: groups.stockings,
	
	//lower
	defHarlotSkirt: groups.skirt * 0.8,
	harlotSkirt: groups.skirt * 0.8, //short
	maidSkirt: groups.skirt, //medium
	haremSkirt: groups.skirt * 2, //trasparent
	
	defBarbaricLoincloth: groups.loincloth,
	barbaricLoincloth: groups.loincloth,
	fancyBarbaricLoincloth: groups.loincloth,
	clothLoincloth: groups.loincloth * 0.7,
	defFallenLoincloth: groups.loincloth * 1.2,
	fallenLoincloth: groups.loincloth * 1.2,
	
	fPants: groups.pants, //loose
	fLeggings: groups.pants, //tight
	fFancyLeggings: groups.pants,
	
	//upper
	fBikini: groups.top, //bikiny top, whitish
	fPolyBikini: groups.top, //colorful
	haremTop: groups.top * 1.6, //transparent tube top
	
	defHarlotTop: groups.top, //semi-slutty tube top with sleeves
	harlotTop: groups.top,
	polyHarlotTop: groups.top * 1.2,
	
	defBarbaricBra: groups.barbaricBra, //looks like bra but in armor slot
	barbaricBra: groups.barbaricBra,
	fancyBarbaricBra: groups.barbaricBra,

	fallenTop: groups.fallenTop, //two vertical straps across breasts 
	defFallenTop: groups.fallenTop,

	fShirt: groups.blouse, //more for men, whitish
	fPolyShirt: groups.blouse * 1.2, //colorful
	fBlouse: groups.blouse, //more for women, whitish
	fPolyBlouse: groups.blouse * 1.2, //for women, colorful
		
	//upperLower
	nobleGown: groups.dress * 2.8, 
	mageRobe: groups.dress * 2.1,
	polyMageRobe: groups.dress * 2.1,
	
	//upperOuter
	defHarlotCorset: groups.bodice,
	harlotCorset: groups.bodice,
	
	//shoes
	//TODO!!!
	
	


	
	defFallenGorget: groups.gorget,
	fallenGorget: groups.gorget,
	
	
	//story 
	siraelTop: groups.fallenTop * 1.3,
	siraelBottom: groups.loincloth * 1.4,
	siraelCollar: groups.gorget * 0.9, 
	
	//weapons 
	knife: groups.blades / 2,
		
	falchion: groups.blades * (2 ** 0),
	cutlass: groups.blades * (2 ** 1),
	sword: groups.blades * (2 ** 2),
	longsword: groups.blades * (2 ** 3),
	
	bludgeon: groups.hammers * (2 ** 0),
	mace: groups.hammers * (2 ** 1),
	morningstar: groups.hammers * (2 ** 2),
	warhammer: groups.hammers * (2 ** 3),
		
	spear: groups.polearms * (2 ** 0),
	scythe: groups.polearms * (2 ** 1),
	poleaxe: groups.polearms * (2 ** 2),
	glaive: groups.polearms * (2 ** 3),
	
	whip: groups.whips * (2 ** 0),
	scourge: groups.whips * (2 ** 1),
	meteor: groups.whips * (2 ** 2),
	chainsword: groups.whips * (2 ** 3),
	
	bow: groups.bows * (2 ** 0),
	warbow: groups.bows * (2 ** 1),
	elvenbow: groups.bows * (2 ** 2),
	dragonbow: groups.bows * (2 ** 3),
	
	dagger: groups.wands / 2,
	wand: groups.wands * (2 ** 0),
	staff: groups.wands * (2 ** 0),
	sceptre: groups.wands * (2 ** 0),
	
	//armor 
	//could be done in simpler way but this maitains the same format as other thing & changes could be done to each specific pieces
	leatherCuirass: groups.cuirass,
	leatherArmor: groups.cuirass,
	leatherSkirt: groups.cingulum,
	leatherPauldrons: groups.pauldrons,
	leatherVambraces: groups.vambraces,
	
	fancyLeatherArmor: groups.cuirass,
	fancyLeatherSkirt: groups.cingulum,
	fancyLeatherPauldrons: groups.pauldrons,
	fancyLeatherVambraces: groups.vambraces,
	
	magicFancyLeatherArmor: groups.cuirass,
	magicFancyLeatherPauldrons: groups.pauldrons,
	magicFancyLeatherVambraces: groups.vambraces,
	
	bronzeCuirass: groups.cuirass,
	bronzePauldrons: groups.pauldrons,
	bronzeVambraces: groups.vambraces,
	bronzeGreaves: groups.greaves,
	bronzeGorget: groups.gorget,
	
	magicBronzeCuirass: groups.cuirass,
	magicBronzePauldrons: groups.pauldrons,
	magicBronzeGreaves: groups.greaves,
	magicBronzeGorget: groups.gorget,
	
	steelCuirass: groups.cuirass,
	steelPauldrons: groups.pauldrons,
	steelVambraces: groups.vambraces,
	steelGreaves: groups.greaves,
	steelGorget: groups.gorget,
	
	glassCuirass: groups.cuirass,
	glassPauldrons: groups.pauldrons,
	glassVambraces: groups.vambraces,
	glassGreaves: groups.greaves,
	glassGorget: groups.gorget,
};
