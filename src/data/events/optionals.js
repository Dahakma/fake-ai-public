/**
	list of optional events; displayed names & weeks when they are available & conditions under which are available & associated story chapters which are triggered (e.g. story["advice_1"])
*/

import {time} from "System/time";

export const optionals = [

 
//INITIATE THE GAME
	{
		name: `play the game`,
		weeks: [1],
		weekends: [1],
		note: `seeking advice`,
		type: `advice`,
		condit(){
			if(!mile.advice_1) return 1;
		},
		event: `advice_1`,		
	},
		
		
//GAME ADVICE
	{
		name: `seeking another advice`,
		note: `seeking advice 2`,
		weeks: [2,3], //TODO
		weekends: [1,2], //check if forced to happen
		type: `advice`,
		condit(){
			return (!mile.advice_2 && mile.game_progress >= 22) ? 1 : 0;
			//if(!mile.advice_2 && mile.goth_arazor >= 2) return 1; //TODO ADD CONDITIONS
		},
		event: `advice_2`,	
	},
	{
		name: `seeking help with the game (TODO: should follow virtual events)`,
		note: `seeking advice 3`,
		weeks: [3,4], //TODO
		weekends: [2,3],
		type: `advice`,
		condit(){
			return (!mile.advice_3 && mile.game_progress >= 28) ? 1 : 0;
			//TODO TEMP - UPDATE WHEN YOU UPDATE THE GAME STORY
		},
		event: `advice_3`,	
	},	
		
		
//AYY 
	{
		name: `meet AI`,
		note: `chat with possible fingering`,
		weeks: [2,3],
		weekends: [1,2,3],
		type: `ayy`,
		condit(){
			return !mile.y_unfriendly && !mile.y_cyberlesbo_2;
		},
		event: `y_meeting_1`,		
	},
	{
		name: `meet AI`,
		note: `chat about the game`,
		weeks: [2,3],
		weekends: [1,2,3],
		type: `ayy`,
		condit(){
			return !mile.y_unfriendly && head.act === 2 && !mile.y_game_1; //TODO!! GAME ADVANCE
		},
		event: `y_game_1`,
	},
	{
		name: `meet AI`,
		note: `oral sex`,
		weeks: [2,3],
		weekends: [2,3],
		type: `ayy`,
		condit(){
			if(!mile.y_oral && mile.y_cyberlesbo_2 > 0) return 1;
		},
		event: `y_oral`,	
	},
	{		
		name: `meet AI`,
		note: `strap-on sex`,
		weeks: [3,4],
		weekends: [2,3,4],
		type: `ayy`,
		condit(){
			if(mile.y_oral > 0 && !mile.y_strapon) return 1;
		},
		event: `y_strapon`,	
	},
	{
		name: `chat with AI`,
		note: `discussing week 2`,
		weeks: [],
		weekends: [2],
		type: `ayy`,
		condit(){
			if(!mile.y_week_2 && time.day === "sun") return 1;
		},
		event: `y_week_2`,	
	},
	{		
		name: `meet AI`,
		note: `domina`,
		weeks: [3,4],
		weekends: [3,4], //todo
		type: `ayy`,
		condit(){
			if(
				!mile.y_dominatrix && mile.y_strapon > 0 && (mile.a_dom > 1 || mile.b_domina) 
			) return 1;
		},
		event: `y_dominatrix`,	
	},
	{		
		name: `meet AI`,
		note: `forced`,
		weeks: [3,4,5],
		weekends: [3,4,5],
		type: `ayy`,
		condit(){
			if(
				!mile.y_forced && mile.y_strapon > 0 
				&& (mile.y_strapon + mile.y_cyberlesbo_2 + mile.y_oral) <= 4 //TODO CHECK!!
				&& mile.sub > 1
			) return 1;
		},
		event: `y_forced`,	
	},
	{
		name: `chat with AI`,
		note: `discussing week 3`,
		weeks: [],
		weekends: [3],
		type: `ayy`,
		condit(){
			if(!mile.y_week_3 && time.day === "sun") return 1;
		},
		event: `y_week_3`,	
	},
	{		
		name: `meet AI`,
		note: `anal`,
		weeks: [3,4,5],
		weekends: [4,5],
		type: `ayy`,
		condit(){
			if(mile.buttplug && !mile.y_anal) return 1;
		},
		event: `y_anal`,	
	},

	
	
//GIRLS
	{
		name: `shopping with girls`,
		weeks: [1,2,3],
		weekends: [1,2,3],
		type: `sas`,
		condit(){
			if(mile.no_bra  && !mile.girls_shopping) return 1;
		},
		event: `girls_shopping`,
	},
	{
		name: `watch movie with girls`,
		weeks: [1,2,3],
		weekends: [1,2,3],
		type: `sas`,
		condit(){
			if(!mile.girls_movie && mile.advice_1) return 1; //not before game initiation
		},
		event: `girls_movie`,
	},
	
	{
		name: `get @sas a tattoo`,
		type: `sas`,
		weeks: [4,5],
		//weekends: [4], //TODO
		condit(){
			return mile.tattoo && !mile.sas_tattoo;
		},
		event: `sas_tattoo`,
	},
	
	{
		name: `Visit cosmetic clinic with @sas`,
		note:  `@sas boobjob`,
		type: `sas`,
		weeks: [4,5,6],
		weekends: [4,5,6],
		condit(){
			//TODO - add delay
			return !mile.sas_boobjob && (mile.girls_boobjob === -1 || mile.girls_boobjob > 0)
		},
		event: `clinic_sas_boobjob`,
	},
	
	{
		name: `@sas's apology`,
		note: `alt start friendship with benefits [NEW]`, //foloowes trickedby girls
		type: `sas`,
	//	weeks: [4,5,6],
//		weekends: [4,5,6],
		condit(){
			if(!mile.sas_apology && mile.girls_tricked) return true;
		},
		event: `sas_apology`,
	},
	
	{
		name: `Date with @sas`,
		type: `sas`,
		weeks: [4,5,6],
		weekends: [4,5,6],
		condit(){
			if(!mile.sas_date){
				if(mile.sas_apology > 0) return true; //alt path (tricked by girls)
				if(mile.sas_benefits > 0) return true; //video task path
			}
			//return mile.video_sas_result && !mile.sas_date; //TODO - ADD ALTERNATIVE PATH THAN VIDEO TASK
		},
		event: `sas_date`,
	},
	
	{
		name: `Watch movie with girls`,
		note: `sexually messing with @sas`,
		type: `sas`,
		weeks: [4,5,6],
		weekends: [4,5,6],
		condit(){
			return mile.sas_date > 0 && !mile.sas_eva_date;
		},
		event: `sas_eva_date`,
	},
	
	{
		name: `Sex with @sas`,
		note: `strapon (getting serious) [NEW]`,
		type: `sas`,
		weeks: [5,6],
		weekends: [5,6],
		condit(){
			if(mile.a_tripoint && !mile.a_burned){ //if dating AAA, waits for jelaousy random event 
				return mile.sas_a_jealousy && !mile.sas_strapon && mile.sas_eva_date && mile.strapon;
			}else{
				return !mile.sas_strapon && mile.sas_eva_date && mile.strapon;
			}
		},
		event: `sas_strapon`,
	},

	{
		name: `Threesome with @sas`,
		note: `(as Juraj's reward for election help) [NEW]`,
		type: `sas`,
		//weeks: [5,6],
		//weekends: [5,6],
		condit(){
			if(!mile.sas_a_threesome && (mile.chair_a_help === 11 || mile.chair_a_help === 12) ) return 1;
		},
		event: `sas_a_threesome_reward`,
	},
	
	{
		name: `Threesome with @sas`,
		note: `(triggered by Saša) [NEW]`,
		type: `sas`,
		//weeks: [5,6],
		//weekends: [5,6],
		condit(){
			//basic conditions not met (not yet happened, friends with benefits with sas & dating juraj)
			if(mile.sas_a_threesome || mile.sas_date !== 1 || !mile.a_tripoint) return 0; 
			//threesome as reward is possible and is prefered
			if( (mile.chair_a_help > 0 && mile.chair_a_help !== 13) && mile.chair_elected <= 2 ) return 0;
			//default
			if(mile.sas_dom || mile.sas_sassy > 4) return 1;
		},
		event: `sas_a_threesome_sas`,
	},
	
	
//EVA 
	{
		name: `Text with @eva's boyfriend`,
		note: `[NEW]`,
		type: `eva`,
		weeks: [4,5,6],
		weekends: [4,5,6],
		condit(){
			if(mile.girls_tricked) return 0; //makes no sense after tricked event
			if(!mile.tits_pics_detail && mile.clinic_boobjob > 0) return 1;
		},
		event: `tits_pics`,
	},
	
	{
		name: `Chat with the AI about the Girls`,
		note: `TRICKED BY GIRLS [NEW]`,
		type: `eva`,
		weeks: [4,5,6],
		weekends: [4,5,6],
		condit(){
			//if(mile.sas_sassy > 5) return 0; //TODO TEMP DISABLED
			//TODO - disable unfriendly ai 
			if(mile.girls_tricked) return 0; //aready done
				
			if(mile.tits_pics_eva || mile.eva_exploit > 4){ //TODO should be at least 5
				return 1;
			}
		},
		event: `girls_tricked`,
	},
	
	
	{
		name: `Seduce @eva's boyfriend`,
		note: `[NEW]`,
		type: `eva`,
		weeks: [5,6],
		weekends: [4,5,6],
		condit(){
			//TODO - add cooldown
			if(mile.girls_tricked && !mile.eva_boyfriend){
				if(mile.girls_tricked === 2) return 1;
				if(mile.girls_tricked === 1 && mile.slut > 6) return 1;
			}
		},
		event: `eva_boyfriend`,
	},




	
//AAA 
	{	
		name: `meet @qaa`,
		note: `another apology attempt`,
		weeks: [2],
		weekends: [1,2],
		type: `qaa`,
		condit(){
			return !mile.a_another_apology && mile.a_apology === -1;
		},
		event: `a_another_apology`,	
	},
	{
		name: `meet @qaa`,
		note: `discussing relationship`, //first date 
		weeks: [2],
		weekends: [1,2],
		type: `qaa`,
		condit(){
			return !mile.a_meeting_relationship && !mile.a_first_date && mile.a_apology === 1;
		},
		event: `a_first_date`,
	},
	
	//love
	{
		name: `watch movie with @qaa`,
		weeks: [2,3],
		weekends: [2,3],
		type: `qaa`,
		condit(){
			return !mile.a_movie && mile.a_dating;
		},
		event: `a_movie`,
	},
	{
		name: `date with @qaa`,
		weeks: [2,3],
		weekends: [2,3],
		type: `qaa`,
		condit(){
			return !mile.a_date && mile.a_dating;
		},
		event: `a_date`,
	},
	
	//3
	{
		name: `date and sex with @qaa`,
		weeks: [3,4],
		weekends: [2,3],
		type: `qaa`,
		condit(){
			return !mile.a_first_sex && mile.a_dating && mile.a_date;
		},
		event: `a_first_sex`,
	},	
	{
		name: `sex with @qaa`,
		weeks: [3,4,5],
		weekends: [3,4],
		type: `qaa`,
		condit(){
			return !mile.a_tripoint && mile.a_dating && mile.a_first_sex;
		},
		event: `a_tripoint`,
	},
	{
		name: `go to cinema with @qaa`,
		weeks: [3,4,5],
		weekends: [3,4], //TODO!!
		type: `qaa`,
		condit(){
			return mile.a_tripoint && !mile.a_cinema;
		},
		event: `a_cinema`,
	},
	
	// 3.5
	{
		name: `invited by @qaa`,
		weeks: [4,5],
		weekends: [3,4], //TODO!!
		type: `qaa`,
		condit(){
			return mile.a_tripoint && !mile.a_family;
		},
		event: `a_family_initiation`,
	},
	{
		name: `@qaa forced watch`,
		weeks: [4,5],
		weekends: [3,4,5],
		type: `qaa`,
		condit(){
			return mile.a_tripoint === 3 && !mile.a_forced_watch;
		},
		event: `a_forced_watch`,
	},
	
	
	//4
	{
		name: `@qaa's family event`,
		weeks: [4,5], //TODO - SHOULD NOT HAPPEN DURING WEEK
		weekends: [4,5], //TODO!!
		type: `qaa`,
		condit(){
			return mile.a_family === 1;
		},
		event: `a_family_action`,
	},
	
	{
		name: `buttsex with @qaa`,
		weeks: [4,5],
		weekends: [4,5],
		type: `qaa`,
		condit(){
			if(mile.a_burned) return false;
			return mile.a_tripoint && !mile.a_anal;
		},
		event: `a_anal`,
	},

	{
		name: `@qaa's reaction to enlarged breasts`,
		type: `qaa`,
		condit(){
			if(mile.a_burned) return false;
			return mile.a_dating && !mile.a_boobjob && mile.clinic_boobjob;
		},
		event: `a_boobjob`,
	},

	//5
	{
		name: `sex with @qaa`,
		weeks: [5],
		weekends: [4,5],
		type: `qaa`,
		condit(){
			//TODO - base on relationship
			if(mile.a_burned) return false;
			return mile.a_tripoint && !mile.a_long_sex;
		},
		event: `a_long_sex`,
	},
	
	
	{
		name: `threesome with @qaa`,
		note: `[NEW]`, 
		weeks: [5, 6],
		weekends: [5, 6],
		type: `qaa`,
		condit(){
			if(mile.a_burned) return false;
			return !mile.a_threesome && mile.a_tripoint && mile.y_strapon; 
		},
		event: `a_threesome`,
	},
	
	{
		name: `sex with @qaa`,
		note: `pegging [NEW]`, 
		weeks: [5, 6],
		weekends: [5, 6],
		type: `qaa`,
		condit(){
			if(mile.a_burned) return false;
			if(mile.a_strapon || mile.into_pegging < 0 || mile.strapon === 0) return false;
			return mile.a_tripoint && mile.a_dom > -2; //TODO - atm enabled even for mostly dominant qaa
		},
		event: `a_strapon`,
	},


	{
		name: `caging @qaa`,
		note: `[NEW]`, 
		weeks: [5,6],
		weekends: [5,6], 
		type: `qaa`,
		condit(){   //TODO - doesn't impact other events!!! 
			if(mile.a_chastity || mile.a_burned) return false;
			if(mile.a_tripoint && mile.a_dom > 3 && mile.chastity_cages) return true; //TODO - stricter conditions? 
		},
		event: `a_chastity`,
	},

	//HATE
	{
		name: `needle @qaa`,
		weeks: [2,3,4],
		weekends: [2,3,4],
		type: `qaa`,
		condit(){
			return !mile.a_needle && mile.a_burned;
		},
		event: `a_needle`,
	},

	{
		name: `solve the issue with the project`, //hate alternative to a_family_initiation
		condit(){
			if(mile.a_project === 1) return 1;
		},
		event: `a_family_project`,
	},
	

	{
		name: `seduce @qaa's father`,
		weeks: [4,5,6],
		weekends: [], //not happening during weekend
		type: `qaa`,
		condit(){
			return mile.a_father === 1;
		},
		event: `a_father_action`,
	},
	
	{
		name: `shopping with @qaa`,
		weeks: [4,5,6],
		weekends: [4,5,6], 
		type: `qaa`,
		condit(){
			return !mile.a_shopping && mile.a_dating; //TODO - a_dating???
		},
		event: `a_shopping`,
	},
	
		
//BBB
	{
		name: `meet @qbb`,
		note: `chat about submissivenes`,
		weeks: [2,3],
		weekends: [1,2],
		type: `qbb`,
		condit(){
			return !mile.b_meeting_1;
		},
		event: `b_meeting_1`,		
	},
	{	
		name: `meet @qbb`,
		note: `chatting about knots`,
		weeks: [2,3],
		weekends: [2,3],
		type: `qbb`,
		condit(){
			return !mile.b_meeting_2;
		},
		event: `b_meeting_2`,		
	},
	//tripoint:
	{
		name: `deal with @qbb`,
		note: `dom x sub path (TODO: should follow virtual events)`, //TODO
		weeks: [3,4,5],
		weekends: [3,4,5],
		type: `qbb`,
		condit(){
			return !mile.b_tripoint; //TODO - IN VIRTUAL GAME EVENTS
		},
		event: `b_tripoint`,		
	},
	//BBB SLAVE	
	{	
		name: `visit @qbb`,
		note: `bitch`,
		weeks: [4,5],//TODO 3RD OR 4TH WEEK?????
		weekends: [3,4,5],
		type: `qbb`,
		condit(){
			return mile.b_slave && !mile.b_bitch;
		},
		event: `b_bitch`,		
	},
	{
		name: `going out with @qbb`,
		note: `on leash`,
		weeks: [4,5], //TODO 3RD OR 4TH WEEK?????
		weekends: [4,5], //TODO - NOT WEEKEND - EVENING
		type: `qbb`,
		condit(){
			return mile.b_slave && !mile.b_groceries;
		},
		event: `b_groceries`,		
	},
	
	{
		name: `shopping with @qbb`,
		note: `@kat submissive`,
		weeks: [4,5,6],
		weekends: [4,5,6], 
		type: `qbb`,
		condit(){
			return mile.b_groceries && !mile.b_shopping_sub; //TODO - maybe without groceries? 
		},
		event: `b_shopping_sub`,
	},
	
	{
		name: `threesome with @qbb`,
		note: `@kat submissive [NEW]`,
		weeks: [5,6],
		weekends: [5,6], 
		type: `qbb`,
		condit(){
			return !mile.b_threesome_sub && mile.b_slave && (mile.y_oral > 0 || mile.y_unfriendly);
			
		},
		event: `b_threesome_sub`,
	},


	//BBB DOMINA	
	{
		name: `let @qbb serve you`,
		note: `cunnilingus`,
		weeks: [4,5],
		weekends: [3,4,5],
		type: `qbb`,
		condit(){
			return mile.b_domina && !mile.b_service;
		},
		event: `b_service`,			
	},
	{	
		name: `play dominatrix to @qbb`,
		//note: `sex`,
		weeks: [4,5],
		weekends: [4,5],
		type: `qbb`,
		condit(){
			return mile.b_domina && !mile.b_dominatrix && mile.b_service;
		},
		event: `b_dominatrix`,	
	},
	
	{
		name: `shopping with @qbb`,
		note: `@kat dominant`,
		weeks: [4,5,6],
		weekends: [4,5,6], 
		type: `qbb`,
		condit(){
			return mile.b_dominatrix && !mile.b_shopping_dom; //reguire @mis initiation (in b_dominatrix)
		},
		event: `b_shopping_dom`,
	},
	
	{
		name: `punishing @qbb`,
		weeks: [5,6],
		weekends: [4,5,6], 
		type: `qbb`,
		condit(){
			return mile.b_dominatrix && !mile.b_punishment; //reguire @mis initiation (in b_dominatrix)
		},
		event: `b_punishment`,
	},
	
	{
		name: `caging @qbb`,
		weeks: [5,6],
		weekends: [4,5,6], 
		type: `qbb`,
		condit(){
			return mile.b_dominatrix && !mile.b_chastity && mile.chastity_cages; //reguire @mis initiation (in b_dominatrix)
		},
		event: `b_chastity`,
	},
	
	{
		name: `threesome with @qbb`,
		note: `@kat dominant [NEW]`,
		weeks: [5,6],
		weekends: [5,6], 
		type: `qbb`,
		condit(){
			return !mile.b_threesome_dom &&  mile.b_chastity && mile.y_oral > 0;
			
		},
		event: `b_threesome_dom`,
	},
	





//CCC
	{	
		name: `meet @qcc`,
		note: `pretend interest or grope`,
		weeks: [2,3],
		weekends: [1,2],
		type: `qcc`,
		condit(){
			return !mile.c_grope && !mile.c_interest;
		},
		event: `c_meeting_1`,		
	},
	{
		name: `meet @qcc`,
		note: `talking`,
		weeks: [2,3],
		weekends: [2,3],
		type: `qcc`,
		condit(){
			if(mile.c_meeting_talk) return false;
			if(mile.c_interest) return 1; //interest
			if(mile.c_grope && mile.c_meeting_bigtits) return 1; //grope bigtits
		},
		event: `c_meeting_talk`,
	},
	{
		name: `meet @qcc`,
		note: `breast expansion`,
		weeks: [2,3],
		weekends: [2,3],
		type: `qcc`,
		condit(){
			return mile.c_grope && !mile.c_meta_boobs && !mile.c_meeting_bigtits; //BEWARE - both c_meta_boobs and c_meeting_bigtits
		},
		event: `c_meeting_bigtits`,
	},
	{
		name: `meet @qcc`,
		note: `avatar alteration`,
		weeks: [3,4,5],
		weekends: [3,4,5], //TODO
		type: `qcc`,
		condit(){
			if(mile.c_avat) return 0;
			if(mile.c_meeting_talk === 3 || mile.c_meeting_talk === 4) return true; //Kate's body was discussed 
			if(mile.c_meeting_bigtits && mile.c_meeting_talk) return true; //Kate's body was not discussed but her breasts got expanded before
		//todo access from tg route!!!
		},
		event: `c_avatar`,
	},
	{
		name: `meet @qcc`,
		note: `boobjob`,
		weeks: [4,5],
		weekends: [3,4,5], //TODO
		type: `qcc`,
		condit(){
			if(mile.c_boobjob) return 0;
			if(mile.c_avat && mile.meta_boobs > 0) return true; //Kate's avatar was improved and breasts enlarged
			if(mile.c_friend > 1 && mile.c_meeting_talk === 1) return true; //friends route, just silly sugestion
			//todo tg route
		},
		event: `c_boobjob`,
	},
	{
		name: `cosmetic clinic`,
		note: `boobjob`,
		weeks: [4,5],
		weekends: [4,5], //TODO
		type: `qcc`, //TODO - SHOULD BE QCC EVENT??
		condit(){
			return !mile.clinic_boobjob && mile.c_boobjob;
		},
		event: `clinic_boobjob`,
	},		
		
	{
		name: `shopping with @qcc`,
		note: `(bimbo)`,
		weeks: [4,5,6],
		weekends: [4,5,6], 
		type: `qcc`,
		condit(){
			return mile.clinic_boobjob && !mile.c_shopping; 
		},
		event: `c_shopping`,
	},
		
		
		

	
//CHAIRMAN
	{	
		name: `chat with @qcc`,
		note: `complain about @ven's candidacy`,
		weeks: [2,3,4],
		weekends: [2,3,4],
		type: `qcc`,
		condit(){
			if(mile.vendy_running && !mile.vendy_running_complain && mile.c_friend > 0) return 1; //TODO - higher friendship
		},
		event: `vendy_c_discussion`,		
	},
	{
		name: `chat with @qaa`,
		note: `complain about @ven's candidacy`,
		weeks: [2,3,4],
		weekends: [2,3,4],
		type: `qaa`,
		condit(){
			if(mile.vendy_running && !mile.vendy_running_complain && !mile.a_burned) return 1; //TODO - A HATE????
		},
		event: `vendy_a_discussion`,
		
	},
		
		
	{	
		name: `discuss elections with @qcc`,
		note: `[NEW]`,
		weeks: [4,5,5],
		weekends: [4,5,6],
		type: `qcc`,
		condit(){
			if( !mile.chair_c_help && mile.vendy_running_complain === 3 && 
			mile.eva_running) return 1;
		},
		event: `chair_c_help`,		
	},
	{
		name: `discuss elections with @qaa`,
		note: `[NEW]`,
		weeks: [4,5,5],
		weekends: [4,5,6],
		type: `qaa`,
		condit(){
			if(mile.a_burned) return 0;
			if( !mile.chair_a_help && mile.vendy_running_complain === 1 && 
			mile.eva_running) return 1;
		},
		event: `chair_a_help`,
		
	},
	
	{
		name: `discuss elections with @qbb`,
		note: `(@kat submissive) [NEW]`,
		weeks: [4,5,5],
		weekends: [4,5,6],
		type: `qbb`,
		condit(){
			if(!mile.chair_b_help && mile.eva_running && mile.b_slave) return 1;
		},
		event: `chair_b_help_sub`,
		
	},
	
	{
		name: `discuss elections with @qbb`,
		note: `(@kat dominant) [NEW]`,
		weeks: [4,5,5],
		weekends: [4,5,6],
		type: `qbb`,
		condit(){
			if(!mile.chair_b_help && mile.eva_running && mile.b_domina) return 1;
		},
		event: `chair_b_help_dom`,
	},
	
	
	
//SCHOOL 
	{
		name: `study biology`,
		weeks: [1,2],
		weekends: [1,2],
		condit(){
			if(!mile.study_biology && !mile.biology) return 1; 
		},
		event: `study_biology`,
		
	},
	{
		name: `study chemistry with @qaa`,
		weeks: [3,4], //TODO
		weekends: [2,3,4],
		condit(){
			if(mile.a_first_sex && !mile.study_chemistry) return 1; 
		},
		event: `study_chemistry`,
		
	},

//POLEDANCING / STRIPPER
	{
		name: `poledancing classes`,
		note: `striptease`,
		weeks: [3,4,5],
		weekends: [3,4,5],
		condit(){
			if(mile.poledancing === 1 && !mile.sveta_stripper /*&& mile.slut > 5*/) return 1; 
		},
		event: `sveta_stripper`,
	},
	
	{
		name: `striptease`,
		weeks: [4,5,6],
		weekends: [4,5,6],
		condit(){
			if(mile.sveta_stripper && !mile.poledancing_teacher) return 1; 
		},
		event: `sveta_stripper_party`,
	},


	{
		name: `poledancing classes`,
		note: `stripshow`,
		weeks: [4,5,6],
		weekends: [4,5,6],
		condit(){
			//TODO - add timer, several days after the previous stripping event 
			if(mile.poledancing_teacher && !mile.stripshow)  return 1; 
		},
		event: `stripshow`,
	},

	{
		name: `stripshow`,
		weeks: [5,6],
		weekends: [4,5,6],
		condit(){
			if(mile.stripshow > 0 && !mile.stripshow_action)  return 1; 
		},
		event: `stripshow_action`,
	},


//TOM ZAN 
	{
		name: `date with @tom`,
		weeks: [5,6],
		weekends: [5,6],
		condit(){
			if(mile.tom_date === 1 ) return 1; 
		},
		event: `tom_date`,
	},
	{
		name: `@tom's second chance`,
		note: `threeway with @zan`,
		weeks: [5,6],
		weekends: [5,6],
		condit(){
			if(mile.tom_date > 1 && !mile.tom_zan_threeway) return 1; 
		},
		event: `tom_zan_threeway`,
	},
	
//WORK
	{		
		name: `work as plant manager`,
		weeks: [1,2],
		weekends: [1], 
		type: `work`,
		condit(){
			if(!mile.plant_manager) return 1;
		},
		event: `plant_manager`,	
	},
	
	{		
		name: `work at a mall`,
		weeks: [2,3], 
		weekends: [1,2], 
		type: `work`,
		condit(){
			if(!mile.promo_mall) return 1;
		},
		event: `promo_mall`,	
	},
		
	{
		name: `work at bar`,
		weeks: [2,3,4],  //TODO
		weekends: [], //happens on afternoon??
		type: `work`, //or eva ??? TODO 
		condit(){
			//TODO EVA SAS BURNED
			if(!mile.karaoke) return 1;
		},
		event: `karaoke`,	
	},
	
	{		
		name: `work at trade show`,
		weeks: [2,3, 4], 
		weekends: [2, 3, 4], 
		type: `work`,
		condit(){
			if(!mile.trade_show && mile.promo_mall) return 1;
		},
		event: `trade_show`,	
	},
	
	{		
		name: `work at farmers' market`,
		weeks: [4,5], 
		weekends: [3,4,5], 
		type: `work`,
		condit(){
			if(mile.trade_show && !mile.farm_eric) return 1;
		},
		event: `farmers_market`,	
	},
	
	
	
//GENERAL
	{	
		name: `go shopping`,
		condit(){
			return true;
		},
		event: `shop_mall`,
	},
	{	
		name: `shopping online`,
		condit(){
			return true;
		},
		event: `shop_online`,
	},
	{
		name: `test shop`,
		condit(){
			return false;
		},
		event: `shop_test`,		
	},
	
];