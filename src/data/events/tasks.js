/**
	list of all tasks; names & weeks when they are available & calculation of their weight & timeslot when story chapters (e.g. story["short_skirt_initiation"]) are triggered
*/

import {time} from "System/time";
import {wears} from "Avatar/wardrobe/types";
import {niceness} from "Loop/text"; // positive - nerds are nice; negative - nerds are cruel 

export const tasks = [

//WEEK 1
	{
		name: "short skirt",
		weeks: [1,2,3],
		condit(){
			if(!mile.short_skirt && !mile.show_legs) return 2;
		},
		events: {
			yesterday: {
				eveningNew: "short_skirt_initiation",
			},
			today: {
				awakening: "short_skirt_action",
				morningReaction: "short_skirt_reaction",
			},
		},
	},
	{
		name: "show legs",
		weeks: [1,2,3],
		condit(){
			if(!mile.short_skirt && !mile.show_legs) return 2;
		},
		events: {
			yesterday: {
				eveningNew: "show_legs_initiation",
			},
			today: {
				awakening: "show_legs_action",
				morningReaction: "show_legs_reaction",
			},
		},
	},
	{
		name: "sexy selfie",
		weeks: [1],
		condit(){
			if(!mile.sexy_selfie) return 1;
		},
		events: {
			yesterday: {
				anyNew: "sexy_selfie_initiation",
				lateEvening: "sexy_selfie_action",
			},
			today: {			
				morningReaction: "sexy_selfie_reaction",
			},
		},
	},
	{
		name: "porn review",
		weeks: [1,2,3],
		condit(){
			if(mile.porn_review){
				return 0; //already done
			}else if(time.week === 1 && head.day < 4){//not before first Wednesday 
				return time.mid_week;
			}else{
				return 1;
			}
		},
		events: {
			yesterday: {
				morningNew: "porn_review_initiation",
				lateEvening: "porn_review_action",
			},
			today: {
				morningReaction: "porn_review_reaction",	
			},
		},
	},
	{	
		name: "cucumber",
		weeks: [1,2],
		condit(){
			if(mile.cucumber || ext.slut !== 0) return 0; //low slut
			return time.week === 1 ? time.mid_week : 1;
		},
		events: {
			afterSchool: "cucumber",
		},
	},
	{
		name: "makeup",
		//weeks: [1,2,3,4],  //all weeks 
		condit(){
			if(mile.makeup){
				return 0; //already done
			}else if(time.week === 1){
				return time.mid_week;
			}else{
				return 2; //TODO - UNLOCKS CHAIRMAN EVENTS!!
			}
		},
		events: {
			yesterday: {
				anyNew: "makeup_initiation",
			},
			today: {
				morning: "makeup_action",
			},
		},
	},
	{
		name: "flash panties", 
		weeks: [1,2],
		condit(){
			//BEWARE - ASSUMES WEARING PANTIES 
			if(mile.saw_panties || mile.flash_panties){
				return 0; //already done
			}else if(time.week <= 1){
				return time.late_week;
			}else{
				return 1;
			}
		},
		events: {
			morningReaction: "flash_panties",
		},
	},
	{
		name: "sexy top",
		weeks: [1,2,3,4],
		condit(){
			if(mile.sexy_top || !(mile.short_skirt || mile.show_legs)  ){ //todo - check if this condition works
				return 0; //already done
			}else if(time.week === 1){
				return time.late_week;
			}else{
				return 1;
			}
		},
		events: {
			yesterday: {
				anyNew: "sexy_top",
			},
			today: {
				morningReaction: "sexy_top_reaction",
			},
		},
	},
	{
		name: "no bra",
		//weeks: [1,2,3,4], //all weeks
		condit(){
			if(mile.no_panties) return 0; //no panties will ban all underwear
			//TODO - redundant for slut that does not wear bra anyway
			if(mile.no_bra){
				return 0; //already done
			}else if(time.week <= 1){
				return time.late_week;
			}else{
				return 1;
			}
		},
		events: {
			yesterday: {
				anyNew: "no_bra",
			},
			today: {
				morningReaction: "no_bra_reaction",
				noon: "no_bra_consequences", 
			},
		},
		
	},
	
		
//WEEK 2
	{
		name: "dildo",
		weeks: [2,3],
		condit(){
			if(mile.dildo) return 0;
			//high slut task the second week; low slut the following ones
			if(time.week === 2){
				return ext.slut > 0 ? 1 : 0.5;
			}else{
				if(ext.slut === 0) return  1;
				if(ext.slut === 1) return  0.5;
			}
		},
		events: {
			yesterday: {
				morningNew: "dildo_initiation", 
				afterSchool: "dildo_action",
			},
			today: {
				morningReaction: "dildo_reaction",
			},
		},
	
	},
	{
		name: "sexy legwear",
		weeks: [2,3,4,5], 
		condit(){
			return (!mile.legwear_type && (mile.short_skirt || mile.show_legs)  ) ? 1 : 0; 
		},
		events: {
			yesterday: {
				anyNew: "sexy_legwear_initiation",
				lateEvening: "legwear_shop",
			},
			today: {
				morningReaction: "sexy_legwear_reaction",
			},
		},
	},
	{
		name: "sexy panties", 
		weeks: [2,3,4],
		condit(){
			//if they saw her wearing unsexy panties
			if(!mile.sexy_panties && mile.saw_panties && !mile.saw_sexy_panties) return 1;
		},
		events: {
			yesterday: {
				morningNew: "sexy_panties_initiation",
				lateEvening: "sexy_panties_shop",
			},
			today: {
				morningReaction: "sexy_panties_action",
			},
		},	
	},
	{
		name: "bikini",
		weeks: [2,3,4], //4??
		condit(){
			if(!mile.bikini && mile.pool_initiated){
				if(time.week === 2){
					return time.mid_week;
				}
				return 1;
			}
		},
		events: {
			yesterday: {
				anyNew: "bikini_initiation", //???
			},
			today: {			
				morning: "bikini_preparation",
				//morningReaction: "bikini_reaction", //initiated in "bikini_preparation"
				classes3: "bikini_action",
			},
		},
	},
	{
		name: "tan",
		weeks: [2,3,4,5],
		condit(){
			if(!mile.tan && !mile.eva_burned && !mile.sas_burned) return 0.5; //TODO - is pretty lame
		},
		events: {
			today: {
				morningReaction: "tan_initiation",
				noon: "tan_invitation",
				afterSchool: "tan_action",
			},
		},
	},
	{
		name: "flash breasts in class",
		weeks: [2,3],
		condit(){
			if(mile.flash_breast_class) return 0;
			if(time.week === 2){
				return  ext.slut > 0 ? 1 : 0; //second week high slut
			}else{
				return 1;
			}
		},
		events: {
			morningReaction: "flash_breast_class_initiation", 
			classes: "flash_breast_class_action",
		},
	},
	{
		name: "topless picture",
		weeks: [2,3], //only 2nd and 3rd week, bland
		condit(){
			if(!mile.topless_picture && mile.sexy_selfie !== 4){ //didn't already send topless selfie
				if(time.week === 2){
					return time.late_week;
				}
				return 0.9;
			}
		},
		events: {
			yesterday: {
				anyNew: "topless_picture_initiation", //???
				lateEvening: "topless_picture_action",
			},
			today: {			
				morningReaction: "topless_picture_reaction",
			},
		},
	},
	{
		//alternative to pink hair by qcc during advice_2 (mile.advice_2 === 3)
		name: "dye",
		weeks: [2,3,4], //TODO make 5 compatible
		condit(){
			if(!mile.dye && mile.advice_2 && mile.advice_2 !== 3 && !mile.hair_crazy) return 0.3;
			return 0;
		},
		events: {
			yesterday: {
				morningNew: "dye_initiation",
				afterSchool: "dye_action",
			},
			today: {
				morningReaction: "dye_reaction",
			},
		},
	},
	{
		//alternative to getting choker by qbb during advice_2 (mile.advice_2 === 2)
		//disabled by getting choker during shopping with qbb (mile.choker === true)
		name: "choker",
		weeks: [2,3,4],
		condit(){
			if(mile.collar) return 0;  //disabled by collar
			if(!mile.choker && mile.advice_2 && ext.slut === 0) return 1;
			if(!mile.choker && mile.advice_2 && ext.slut === 1) return 0.5;
			return 0;
		},
		events: {
			yesterday: {
				anyNew: "choker_initiation", 
				lateEvening: "choker_shop",
			},
			today: {
				morningReaction: "choker_reaction",
			},
		},	
	},
	
	
//WEEK 3	
	{
		name: "piercings",
		weeks: [3,4,5], //TODO - maybe move to 2?
		condit(){
			if(!mile.piercings) return 2; //BLOCKS piercing content
		},
		events: {
			yesterday: {
				morningNew: "piercings_initiation", 
				afterSchool: "piercings_action",
			},
			today: {			
				morningReaction: "piercings_reaction",
			},
		},
	},
	{
		name: "open legs",
		weeks: [3,4,5],
		condit(){
			if(!mile.open_legs && wears.skirt) return 0.8;
		},
		events: {
			today: {
				morningReaction: "open_legs_initiation", //TODO - morningNew or morningReaction?
				classes: "open_legs_action",
				classes3: "open_legs_action_2",
			},
		},
	},
	{
		name: "shaved pubic hair",
		weeks: [3,4,5],
		condit(){
			if(!mile.shaven && mile.saw_hairy) return 1; 
		},
		events: {
			yesterday: {
				anyNew: "shaven_initiation", //???
				lateEvening: "shaven_action",
			},
			today: {			
				morningReaction: "shaven_reaction",
			},
		},
	},
	{
		name: "vibrating panties",
		weeks: [3,4,5],
		condit(){
			if(!mile.vibrating_panties && mile.advice_2) return 3;
			if(!mile.vibrating_panties) return 1;
		},
		events: {
			today: {
				awakening: "vibrating_panties_initiation",
				morning: "vibrating_panties_action",
				classes2: "vibrating_panties_action_2",
				noon: "vibrating_panties_lunch",
				afterSchool: "vibrating_panties_action_3",
				eveningReaction: "vibrating_panties_reaction",
			},
		},
	},
	{
		name: "poledancing",
		weeks: [3,4,5],
		condit(){
			if(!mile.poledancing) return  2; //BLOCKS stripper content
		},
		events: {
			today: {
				morningReaction: "poledancing_initiation",  //TODO or morningNew?
				afterSchool: "poledancing_action",
			},
		},
	},
	
	{
		name: "tattoo",
		weeks: [3,4,5], 
		condit(){
			if(mile.tattoo) return 0;
			if(time.week === 3){
				return time.late_week;
			}else{
				return 2; //BLOCKS tattoo content
			}
		},
		events: {
			yesterday: {
				morningNew: "tattoo_initiation", 
				afterSchool: "tattoo_action",
			},
			today: {			
				morningReaction: "tattoo_reaction",
			},
		},
	},
	{
		name: "nude picture",
		weeks: [3,4,5],
		condit(){
			if(!mile.nude_picture){
				if(time.week === 3){
					return time.late_week;
				}else{
					return 1;
				}
			}
		},
		events: {
			today: {
				morningReaction: "nude_picture_initiation", //TODO
				//morningNew: "nude_picture_initiation",
				classes: "nude_picture_action",
			},
		},
	},
	{
		name: "video dildo ",
		weeks: [3,4,5],
		condit(){
			if(mile.video_dildo){
				return 0; //already done
			}else{
				if(time.week === 3){
					return time.late_week;
				}else{
					return 1;
				}
			}
		},
		events: {
			yesterday: {
				morningNew: "video_dildo_initiation",
				lateEvening: "video_dildo_action",
			},
			today: {
				morningReaction: "video_dildo_reaction",	
			},
		},
	},
	
	
	
/*
	{
		name: "piercings",
		weeks: [3,4,5],
		condit(){
			if() return 1; 
		},
		events: {
			yesterday: {
				anyNew: "piercing_initiation", //???
				afterSchool: "piercing_action",
			},
			today: {			
				morningReaction: "piercing_reaction",
			},
		},
	},
*/
	
	
	
//WEEK 4
	{
		name: "buttplug",
		weeks: [4,5], //TODO 3 OR 4????
		condit(){
			//TODO - LATE WEEK
			if(mile.buttplug === 0) return 3; //BLOCKS anal content
		},
		events: {
			yesterday: {
				anyNew: "buttplug_initiation", 
			},
			today: {
				awakening: "buttplug_morning",
				morning: "buttplug_journey",
				morningReaction: "buttplug_reaction",
				classes2: "buttplug_consequences",
			},
		},
	},
	
	{
		name: "piercings 2",
		weeks: [4,5],
		condit(){
			if(!mile.piercings || mile.piercings_2) return 0; 
			const count = wears.countPiercings; //tries to guess how much player likes piercings based on how many equiped
			if(count < 3) return 0.5;
			if(count === 3) return 0.8;
			if(count === 4) return 1.2;
			if(count >= 5) return 2.4; //max - initial earrrings and navel + 3 piercings from task
			
		},
		events: {
			yesterday: {
				morningNew: "piercings_2_initiation", 
				afterSchool: "piercings_2_action",
			},
			today: {			
				morningReaction: "piercings_2_reaction",
			},
		},
	},
	
	{
		name: "video @sas ",
		weeks: [4,5,6], //5 approved serious X less serious variant
		condit(){
			if(mile.girls_tricked) return 0; //makes no sense after tricked event
			if(
				!mile.video_sas_task //not yet done
				&& (mile.video_dildo || mile.blackmail) //blackmail material
				&& mile.sas_boobjob //after sasa's boobjob 
				/*&& mile.sas_kiss*/
			){
				if(mile.tits_pics_eva || mile.eva_exploit > 4){ //conditions for tricked even passed (is better event)
					return 0.66;
				}else{
					return 1;
				}
			}else{
				return 0;
			}
		},
		events: {
			yesterday: {
				morningNew: "video_sas_initiation",
				lateEvening: "video_sas_action",
			},
			today: {
				morningReaction: "video_sas_reaction",	
			},
		},
	},
	
 

	{
		name: "no_panties",
		weeks: [4, 5, 6],  //5 approved (nicer version)
		condit(){
			if(mile.no_panties) return 0; //TODO - assume short skirt or shorts
			if(mile.slut > 10) return 1; //TODO
			return 0.8;
		},
		events: {
			today: {
				morningReaction: "no_panties",  //BEWARE -  more events added during the no_panties event
			},
		},
	},
	

	{
		name: "tattoo 2",
		weeks: [4,5,6],  //5 approved (showed she likes tattoos)
		condit(){
			if(!mile.tattoo) return 0;
			if(mile.tattoo_2) return 0;
			if( niceness() > 0 ){
				if(mile.sas_tattoo_kat) return 1;
				return 0;
			}else{
				if(mile.sas_tattoo_kat) return 1.5;
				return 0.75;
			}
		},
		events: {
			yesterday: {
				morningNew: "tattoo_2_initiation", 
				afterSchool: "tattoo_2_action",
			},
			today: {			
				morningReaction: "tattoo_2_reaction",
			},
		},
	},


	{
		name: "mandatory masturbation",
		note: "[NEW]",
		weeks: [4,5,6],  //5 NOT YET APPROVED - TODO
		condit(){
			if( (mile.dildo || mile.video_dildo) && !mile.mandatory_masturbation ) return 1;
		},
		events: {
			yesterday: {
				morningNew: "mandatory_masturbation_initiation", 
			},
			today: {			
				morning: "mandatory_masturbation_dildo",
				morningReaction: "mandatory_masturbation_action",
				//mandatory_masturbation_action_waver could be added to classes2
			},
		},
	},
	
	
//WEEK 5
	{
		name: "collar",
		weeks: [5,6],   //todo 4?   //5 approved (submissive)
		condit(){
			//TODO - adjust submisiveness demand 
			if(!mile.collar && mile.sub >= 18) return 2;
			if(!mile.collar && mile.sub >= 14) return 1;
			if(!mile.collar && mile.sub > 10) return 0.5;
			if(!mile.collar_task && mile.sub > 10)  return 0.5; //is already wearing collar voluntaringly
		},
		events: {
			today: {
				morningReaction: "collar_initiation", 
				classes: "collar_action",
			},
		},
	},


//WEEKEND TASKS
	{
		name: "wrestling",
		weekend_task: 1,
		condit(){
			return null;
		},
		events: {
			today: {
				classes3: "wrestling_initiation",
				evening: "wrestling_action",
			},
			tomorrow: {
				earlyMorning: "y_fight_reaction", //TODO!!
			},
		},
	},
	{
		name: "game night",
		weekend_task: 2,
		condit(){
			return null;
		},
		events: {
			today: {
				classes3: "game_night_initiation",
				evening: "game_night_action",
			},
		},
	},
	{
		name: "kissing girls",
		weekend_task: 3,
		condit(){
			return null;
		},
		events: {
			today: {
				classes3: "kissing_girls_initiation",
				evening: "kissing_girls_action",
			},
		},
	},
];







/*
	
	
*/