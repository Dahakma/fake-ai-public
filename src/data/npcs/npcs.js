//having prefix means the defalut name will be "prefix + surname"
export const default_npcs = {
	kat : {
		name: "Kate",
		gamename: "Kate",
		altname: "Katarína",
		surname: "Pokorná",
		color: "#c20051", 
		desc: "me, the beautiful , smart and feisty heroine of this story",
		sex: 1,
	},
	qaa: {
		name: "Juraj",
		color: "#110df2",
		desc: "nerd with a crush",
		sex: 0,
	},
	qbb: {
		name: "Zdeno",
		color: "#318802", //"#158000",
		desc: "creepy nerd",
		sex: 0,
	},
	qcc: {
		name: "Matej",
		color: "#C2600F", //"#c9501d",
		desc: "clumsy nerd",
		sex: 0,
	},
	sas: {
		name:  "Saša",
		surname: "Kerekesová",
		altname: "Alexandra", 
		color: "#F20D68", //#110df2",
		desc: "my best friend, a bit needy",
		sex: 1,
	},
	
	eva: {
		name:  "Eva",
		surname: "Šebestová",  // Gombitová
		color:  "#D51515", //"#c20091",
		desc:  "my best friend, a bit bitch",
		sex: 1,
	},
	
	pet: {
		name: "Peter",
		color: "#026d73",
		desc: "pretty cool guy",
		sex: 0,
	},
	ayy:{
		name: "AI",
		color: "#c20091",
		desc: "the titular AI",
		sex: 1,
	},
	
	anj: {
		name: "Andrej",
		color: "#026d73",
		desc: "hacker",
		sex: 0,
	},
	
	
	
	tom:{
		name: "Tom",
		color: "#009905", //"#158000",
		desc: "jock",
		sex: 0,
	},
	zan:{
		name: "Žanet",
		color: "#e67700", //"#c20091",
		desc: "skank",
		sex: 1,
	},
	ven:{
		name: "Vendy",
		altname: "Vendula",
		surname: "Pauhofová",
		color: "#c20091",
		desc: "wannabe",
		sex: 1,
	},
	
	
	
//MINOR
	ada: {
		name: "Sam",
		altname: "Samuel",
		color: "#158000", //TODO
		desc: "@eva's boyfriend",
		sex: 0,
		minor: true,
	},
	
	
	mik: {
		name: "Mike",
		altname: "Michal",
		color: "#158000",
		desc: "cool handsomes guy",
		sex: 0,
		minor: true,
	},
	
	kub: {
		name: "Kubo",
		color: "#c9501d",
		desc: "ex-boyfriend, well-endowed but jerk",
		sex: 0,
		minor: true,
	},
	voj: {
		name: "Vojta",
		color: "#026d73",
		desc: "@tom's friend",
		sex: 0,
		minor: true,
	},
	
	dav: {
		name: "Dávid",
		color: "#c9501d",
		desc: "annoying guy, interested in dating @sas",
		sex: 0,
		minor: true,
	},
	
	sve:{
		name: "Světa",
		altname: "Světlana",
		color: "#c20091",
		desc: "poledancer & stripper",
		sex: 1,
		minor: true,
	},
	
	jus: {
		name: "Justýna",
		color: "#026d73",
		desc: "600 years old vampire",
		sex: 1,
		minor: true,
	},
	
	
	
//TEACHERS
	mol:{
		surname: "Molnár",
		color: "#026d73",
		desc: "sleazy German teacher",
		prefix: "Mr.",
		sex: 0,
	},
	
	maj:{
		surname: "Majerová",
		color: "#026d73",
		desc: "old strict literature teacher",
		prefix: "Mrs.",
		sex: 1,
	},
	
	/*
		Iveta
		Zuzana
		Monika
	*/
	
	ped: {
		surname: "Bosáková",
		altname: "Lenka",
		color: "#026d73",
		desc: "laidback physic education teacher",
		prefix: "Ms.",
		sex: 1,
	},
	
	dur: {
		surname: "Ďuro",
		color: "#026d73",
		desc: "boring history teacher",
		prefix: "Mr.",
		sex: 0,
	},
	
	bio: {
		surname: "Liška",
		color: "#026d73",
		desc: "no-nonsense biology teacher",
		prefix: "Mr.",
		sex: 0,
	},
	
	asy: {
		surname: "Franz",
		color: "#026d73",
		desc: "deputy principal",//bumbling
		prefix: "Mr.",
		sex: 0,
	},
	
	
	/*
	geo: {
		surname: "Liška",
		color: "#026d73",
		desc: "no-nonsense biology teacher",
		prefix: "Mr.",
		sex: 1,
	},
	*/
	bar: {
		name: "Bára",
		color: "#c20091",
		desc: "nerd's counsin",
		sex: 1,
		minor: true,
	},
	
//GENERIC
	npc_1:{
		name: "npc_1",
		color: "#026d73",
		desc: "NPC 1",
		sex: 0,
		generic: true,
	},
	npc_2:{
		name: "npc_2",
		color: "#158000",
		desc: "NPC 2",
		sex: 0,
		generic: true,
	},
	npc_3:{
		name: "npc_3",
		color: "#c9501d",
		desc: "NPC 3",
		sex: 0,
		generic: true,
	},
	npc_4:{
		name: "npc_4",
		color: "#c20091",
		desc: "NPC 4",
		sex: 1,
		generic: true,
	},
	npc_5:{
		name: "npc_5",
		color: "#c20091",
		desc: "NPC 5",
		sex: 1,
		generic: true,
	},
	
	npc_6:{
		name: "npc_6",
		color: "#026d73",
		desc: "NPC 6",
		sex: 1,
		generic: true,
	},
	
//FANTASY
	yel:{
		fantasy: true,
		name: "Yelaya",
		parent: "npc_4",
		desc: "wanabe adventurer",
		sex: 1,
	},
	
	gal:{
		fantasy: true,
		name: "Galawar",
		parent: "npc_2",
		desc: "prodigal noble",
		sex: 0,
	},
	
	lys:{
		fantasy: true,
		name: "Lyssa",
		parent: "npc_3",
		desc: "pirate bitch",
		sex: 1,
	},
	
	ste:{
		fantasy: true,
		name: "Stede",
		parent: "npc_5",
		desc: "failed captain",
		sex: 0,
	},
	
	sir:{
		fantasy: true,
		name: "Sirael",
		parent: "npc_6",
		desc: "wizard's homunculus",
		sex: 1,
	},
}