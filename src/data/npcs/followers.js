/**
	stats of npc which can join you as your followers in the virtual game
*/

export const followers = {
	yel: {
		//name: "Yelaya",
		class: "adventurer",
		level: 2,
		base: {
			str: 2,
			dex: 2,
			int: 1,
			char: 1,
			hp: 70,
			eng: 30,
			greed: 2,
			chem: 3,
		},
		perks: [],
		skills: [
			"swift",
			"brutal",
		],
	},
	gal: {
		//name: "Galawar",
		class: "aristocrat",
		level: 3,
		base: {
			str: 4,
			dex: 1,
			int: 2,
			char: 3,
			hp: 140,
			eng: 80,
			greed: 1,
			chem: 1,
		},
		perks: [],
		skills: [
			"swift",
			"brutal",
			"precise",
			"stunning",
			"protect",
		],
	},
	lys: {
		class: "seductress",
		level: 3,
		base: {
			str: 2,
			dex: 3,
			int: 1,
			char: 4,
			hp: 100,
			eng: 100,
			greed: 4,
			chem: 3,
		},
		perks: [
			"danger",
		],
		skills: [
			"hide",
			"accurate",
		],
	},
	ste: {
		class: "navigator",
		level: 3,
		base: {
			str: 3,
			dex: 2,
			int: 3,
			char: 1,
			hp: 120,
			eng: 90,
			greed: 3,
			chem: 3,
		},
		perks: [],
		skills: [
			"swift",
			"brutal",
			"precise",
			"stunning",
			"protect",
		],
	},
	sir: {
		class: "homunculus",
		level: 3,
		base: {
			str: 2,
			dex: 2,
			int: 4,
			char: 2,
			hp: 80,
			eng: 120,
			greed: 3,
			chem: 3,
		},
		perks: [
			"mage",
			"nov_ice",
		],
		skills: [
			"blades",
			"freeze",
			"frostbite",
			"blizzard",
			"dazzle",
		],
	},
}