/**
	physical properties of displayed DAD avatars
*/

export const avatars = {
	kat: {
		bias: 0.8,
		randomness: 0.4,
		absolute: {
		},
		relative: {
		},
		adjust(){
		},
	},
	
	ayy: {
		bias: 0.8,
		randomness: 0.4,
		apex: {
			irl_lips: "#c60101",
			irl_mascara_bot: "#38001e",
			irl_mascara_top: "#4d0025",
			irl_nails: "#000000",
		},
		adjust(){
			//todo - what about the basedim?
			Object.keys(ext.meta).forEach( a => this.Mods[a] = ext.meta[a] ); //to ensure AI looks the same way as metaverse PC 
			this.Mods.pubicLength = 0; //have no pubic hair
		},
	},
	
//NERDS 
	//QAA
	qaa: {
		bias: -0.05,
		relative: {
			upperMuscle: -4,
			height: -2,
		},
		absolute: {
			breastSize: -1,
			hairStyle: 2,
			hairLength: [25, 35],
		},
		meta: {
			relative: {
				upperMuscle: 7, //more muscular 
				height: 2,
			},
		},
	},
	
	//QBB - lanky, tall, thin, long black hair
	qbb: {
		bias: -0.1,
		relative: {
			height: 4, //tall
			upperMuscle: -5, 
			shoulderWidth: -5, //thin, lanky
			neckWidth: -5,
			waistWidth: -10,
			hipWidth: -5,
		},
		absolute: {
			breastSize: -1,
			hairStyle: 4,
			hairLength: [30, 40],
			hairLightness: 13, //dark hair
		},
		meta: {
			relative: {
				upperMuscle: 5,
				shoulderWidth: 2.5,
			},
		},
	},
	
	//QCC - androgynous, chubbier
	qcc: {
		bias: -0.01,
		relative: {
			height: -4,
			upperMuscle: -6,
			waistWidth: 10,
			hipWidth: 5,
		},
		absolute: {
			breastSize: -1,
			hairStyle: 5,
			hairLength: [30, 40],
		},
	},
	
	
//GIRLS
	//Eva - darker hair, curvier
	eva: {
		absolute: {
			hairLightness: [24, 38], //dark hair
			breastSize: [15, 25], //curvy
			hairStyle: 1,
			pubicLength: 0, //shaved
			eyelashLength: 3,
		},
		relative: {
			legFullness: 4, //curvy
			hipWidth: 5,
			waistWidth: 5,
		},
		meta: {
			absolute: {
				hairHue: 10,
				hairSaturation: 100,
				hairLightness: 30,
			},
			relative: {
				waistWidth: -10, //thinner
				faceFem: 10, //hotter 
			},
		},
		apex: {
			irl_lips: "#82030d", //maybe heavier makeup?
			irl_mascara_bot:  "#240000",
			irl_mascara_top: "#1f0000",
			irl_nails: "#800000",
		},
	},
	
	//Saša - taller, light hair, small breasts
	sas: {
		absolute: {
			breastSize: [3, 10], //small breasts
			hairLength: [75, 90],
			hairHue: [25, 32], 
			hairLightness: [40, 60], //light haired
			hairSaturation: [40, 50],
			hairStyle: 5,
		},
		relative: {
			height: 5, //she's lanky
		},
		meta: {
			relative: {
				hairLength: 30, //boosted hair
				breastSize: 5, //insecure about breasts
			},
		},
		adjust(){
			if(mile.sas_makeup){
				Object.assign(this.apex, {
					irl_lips: "#a20242",
					irl_mascara_bot: "#650124",
					irl_mascara_top: "#6b0025",
					irl_nails:  "#00c2b5",
				})
			}
		},
	},
	
	//Vendy - les sexier
	ven: {
		absolute: {
			breastSize: [3, 10],
			waistWidth: [115, 120],
			pubicLength: [11, 13], //bushy
			pubicWidthTop: 12,
		},
		relative: {
			faceFem: -5, //uglier
		},
		meta: { //desperately sexier
			absolute: {
				breastSize: 35, //overblown breasts
				breastPerkiness: 7,
			},
			relative: {
				waistWidth: -10, //thinner
				faceFem: 15, //hotter 
			},
		},
	},
	

//MINOR NPCS
	maj: {
		absolute: {
			hairStyle: 1,
		},
	},
	
	dur: {
		absolute: {
			hairStyle: 0,
		},
		relative: {
			upperMuscle: 8,
			height: 6,
		},
	},
	
	ped: {
		absolute: {
			upperMuscle: [12, 14],
			skin: [8,13],
		},
	},
	

//FANTASY
	lys: {
		absolute: {
			hairLength: [85, 95],
			hairHue: 0,
			hairLightness: [35, 45],
			hairSaturation: [85, 95],
			height: [165, 170],
			upperMuscle: [10, 13],
		},
		relative: {
			breastSize: 20,
			breastPerkiness: 5,
		},
	},


	sir: {
		absolute: {
			skin: 10,
			skinHue: 20,
			skinSaturation: 20,
			skinLightness: 0,
			
			hairHue: [45, 50],
			hairLightness: 40, //[40, 60],
			hairSaturation: 70, //[40, 50],
			
			irisHue: 220,
			irisLightness: 70,
			irisSaturation: 100,
			
			pubicLength: 0,
			//hairStyle: 5,
		},
		relative: {
			//height: 3,
		},
		apex: {
			game_lips: "hsla(50, 69%, 36%, 1)",
		},
	},
	

	//justýna
	jus: {
		absolute: {
			hairLightness: [10, 15],
			hairSaturation: [10, 15],
			skin: 1, //-1.5,
			irisLightness: 70,
			irisSaturation: 90,
		},
		apex: {
			tat_bothSleeves: "sleeveCircles;0;-4;1.3",
			irl_lips: "hsla(331, 100%, 17%, 1)",
		},
		relative: {
			//height: 3,
			limbalRingSize: 4,
		},
	},
	
	bar: {
		apex: {
			irl_lips: "#040255",
			irl_mascara_bot: "#00108a",
			irl_mascara_top:  "#010579", 
			irl_nails: "#150061",
		},
	},
	
	//světa
	sve: {
		absolute: {
			hairHue: [45, 50],
			hairLightness: [45, 55],
			hairSaturation: [85, 90],
		},
		adjust(){
			this.Mods.pubicLength = 0;
			this.Mods.height = PC.Mods.height;
			this.basedim.height = PC.basedim.height;
		},
		apex: {
			irl_lips: "hsla(343, 100%, 35%, 1)",
			irl_mascara_top:  `black`,
			irl_mascara_bot:  `black`,
		},
		relative: {
			breastSize: 15,
			breastPerkiness: 7,
			upperMuscle: 5,
		},
	},
	
	
	bimbo: {
		absolute: {
		},
		apex: {
			irl_lips: "hsla(343, 100%, 35%, 1)",
			irl_mascara_top:  `black`,
			irl_mascara_bot:  `black`,
		},
		relative: {
			breastSize: 40,
			breastPerkiness: 21,
			waistWidth: -10,
			faceFem: 20,
			buttFullness: 10,
			legFem: 5,
		},
	},
	
	
//GENERIC NPCS
	creepy: {
		bias: -0.1,
		relative: {
			neckWidth: -5,
			waistWidth: -10,
		},
		absolute: {
			hairStyle: 4,
			hairLength: 120,
			skin: -1.5,
			irisLightness: 10,
			irisHue: 5,
			irisSaturation: 100,
			irisSize: 50,
			breastSize: -1, //TODO - redundant
		},
		
	},
	
	
	dork: {
		bias: -0.1,
		relative: {
			height: -2,
			upperMuscle: -4,
			shoulderWidth: -3,
			neckWidth: -3,
			waistWidth: -8,
		},
		absolute: {
			breastSize: -1,
		},
	},
	
	guard: {
		bias: -0.45,
	},
	
	
	
	//TODO
	tim: {
		//bias: -0.9,
		absolute: {
			skin: [18, 22],
			upperMuscle: 30,
		},
	},
	eric: {
		//bias: -0.95,
		absolute: {
			skin: 10,
			upperMuscle: 30,
		},
	},
	andry: {
		bias: 0.9,
		absolute: {
			skin: 12,
			breastPerkiness: 6,
			breastSize: 34,
		},
		apex: {
			irl_lips: "#82030d", //maybe heavier makeup?
			irl_mascara_bot:  "#240000",
			irl_mascara_top: "#1f0000",
			irl_nails: "#800000",
		},
		//TODO TATTOO
	},
};