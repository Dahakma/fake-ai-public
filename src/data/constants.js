/**
	various constant variables
*/

export const constants = {
	tooltip_delay: 600, //ms; delay before is the tooltip displayed
	
//SETTING
	setting: {
		avatar_width: 300, //px; avatar width
		minimap_size: 50, //px; size of one minimap tile
		map_size: 50, //px; size of one map tile
		hint: true, //display hints 
		keyboard: true, //enable keyboard 
		difficulty: 0,
		show_names: true, //show names of npcs before their direct speech 
		local: "slovak", //country where the game takes place;
		/*
			"english" - Sheffield
			"german" - Vienna
			"polish" - Wrocław
			"slovak" - Bratislava
			//TODO 
			"czech" - Ústí nad Labem
			"hungarian"
		*/
		
		
		no_virtual_game: false, //skipst the parts with the virtual game 
		
		bgr2: true, //use background 2
		bgr3: true, //use background 3
		
		debug: false, //game in debugging mode
		softbug: false, //game in soft cheatmode
		hardbug: false, //game in hard cheatmode
		
		//display_avatars: true,
		//display_triangles: true, //change of stats
		//rollback: true,
		
		taskmaster: false,
		taskmaster_random: false,
		alchemist: false, //always direct control of potions
		noclip: false, //dungeon noclip mode (allowed to walk throgh walls)
		immortality: false, //PC cannot be killed 
		combat_ai: true, //AI controls actions of enemy pawns 
		roll_chance: false,
		no_emotions: false, //disable facial expressions
		
		grid_inventory: true, //inventory is one table instead of two columns
		irl_economy: 1, //price of all irl items multiplied by this value
		game_economy: 1, //price of all game items multiplied by this value 

//TODO - DISABLE NOTIFICATION FOR NON-PATREON		
		notifications_payment: true,
		notifications_save: true,
		notifications_test: true,
		notifications_mile: true,
		no_zoom: false,

	},
	
	
//CSS
	css: {
		bgr: "#c7c9d1", //"hsla(228, 10%, 80%, 1)", //hsl(228, 4%, 80%)",
		bgr2: "#c7c9d1", //"hsla(228, 10%, 80%, 1)", //"hsl(228, 4%, 70%)",
		bgr3: "#8C8F9C", //"hsla(228, 10%, 80%, 1)", //"hsl(228, 4%, 70%)",
		highlight: "#1B0330", //"#1b0330", //org? "hsla(222, 90%, 10%, 1)", //"hsl(0, 0%, 18%)",
		text: "##070808", //"hsl(0, 0%, 0%)",
		good: "#00e600", //hsla(120, 100%, 45%, 1);
		bad: "#e60000", // hsl(0°, 100%, 45%); 
	},
	
	
//KEYBOARD - todo
	keyboard: {
		character: "c",
		inventory: "i",
		follower: "f",
		follower_inventory: "j",
		load: "l",
		map: "m",
	},
	
	
//HP & ENERGY 
	hp: {
		harlot: 40, //default HP of harlot etc.
		hellion: 50,
		fallen: 30,
		
		elixir: 100, //HP healed by healing potion
		
		level_up: 25, //HP gained each new level
		level_up_bonus: 5, //bonus HP gained per 1 point of strength
	},
	
	eng: {
		harlot: 40, //default Eng of harlot etc.
		hellion: 30,
		fallen: 50,
		
		elixir: 100, //Eng restored by energy potion
		
		level_up: 25, //Eng gained each new level
		level_up_bonus: 5, //bonus Eng gained per 1 point of intelligence
	},
	
//EFFECTS
	ticks: 30, //how long the effect last if not specified otherwise
	
//POTIONS
	elixirs: {
		transformation_speed: 1500, //ms; default speed of the transformation animation
		learn_minimal: 5, //minimal amout of potions to drink before having chance to lear about the potion (the name is displayed instead of color)
		learn_maximal: 10, //100% chance to learn about the potion
	},
	
	
//COMBAT	
	combat: {
		execution_delay: 500, //ms; how long it takes to execute combat action
		ai_delay: 800, //ms; how long the AI "think" before deciding combat action
		combatant_size: 150, //px; tile with the combatant //TODO
		unarmed_damage: 5, //default damage dealt when no weapon is equipped (fists)
		reflex_spread: 0.25, //actual value of att.reflex used to calculate combat order will be in range between "att.reflex * (1 - constants.combat.reflex_spread)" and "att.reflex * (1 + constants.combat.reflex_spread)"
		presence_spread: 0.25, //actual value of att.presence used to calculate attractivness for enemies 
	},
	
	
//ATTRIBUTES


//STATS
	/*
		"mile.slut" is exact numeric value
		"ext.slut" is divided into a few levels (simplifying the checking)
	*/

	//SLUTTINESS
	slut: {
		min: -6, //minimal possible value
		max: 30, //maximal possible value
		//value of "mile.slut" <= "constants.slut.limits[0]" will return "ext.slut" = 0
		limits: [
			 1, //0 - prude
			 5, //1 - beauty
			12, //2 - tease
			20, //3 - slut
			28, //4 - whore
		], //5 - cumdump
		//value of "mile.slut" > "constants.slut.limits[]" will return "ext.slut" = 5
	},
	

	//SUBMISIVENESS
	sub: {
		min:  -15,
		max: 20,
		limits: [
			-10, //0 despotic
			 -4, //1 dominant
			  2, //2 confident
			  7, //3 humble
			 15, //4 meek
		], //5 pathetic
	},
	
	
	//STATUS
	stat: {
		min:  -14,
		max: 7,
		limits: [
			-11, //0 pariah
			 -7, //1 outcast
			 -4, //2 falling
			 -2, //3 cool
			  1, //4 popular
			  5, //5 princess
		], //6 queen
	},


}