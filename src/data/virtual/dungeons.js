/**
	blueprints of dungeons 
*/

export const dungeons = {

//FEYD MORA
	cellar: {
		name: "Rat-infested Cellar",
		theme: "cellar",
		loot: "tutorial",
		rooms: {
			enemies: "rats",
			bosses: "rous",
		},
		grid: `
			...x4x.6x5..
			Ex12.3.x.xb.
			.....xx5....
		`,
		extra: [
			[1, "tutorial_combat"],
			[2, "rat", "e"],
			[3, "tutorial_loot"],
			[4, "tutorial_lockpick","|"],
			[5, "tutorial_elixirs"],
			[6, "two_rats", "e"],
		],
		
	},

	feyd_roses: {
		name: "Woods around Feyd Mora",
		theme: "wood",
		rooms: {
			quest: "roses",
			bosses: ["ogre"],
			enemies: ["rats","goblins"],
			challenges: ["tree"],
			treasures: ["stash"],
		},
		grid: `
			bxx.xt...xq.
			.xqxxxxcqxx.
			..x..xe...t.
			..xcqxxx..x.
			..E.xq.xexq.
		`,
	},
	
	yelaya: {
		name: "Mysterious cavern",
		theme: "cave",
		loot: "yelaya",
		rooms: {
			enemies: ["goblins"],
			bosses: ["ogre"],
			quest: "yelaya_final",
			follower: "yelaya",
		},
		grid: `
			...E....t....t.e
			.tx1x%t.xft.xxfx
			.x....efx...x.x.
			.xbfxxx.x|txf.xf
			..t...txf...x..q
		`,
		extra: [
			[1, "tutorial_follower"],
		],
	},
	
//GOTHRED
	ship: {
		name: "Plague Storm, pirate ship",
		theme: "ship",
		introduction: `We had no idea how big the ship might be nor where we were at the moment but we were about to find out. `,
		rooms: {
			enemies: ["pirates"],
			treasures: ["cargo", "storage", "chest"],
		},
		grid: [`
			.1.<.x.t
			x5%x|4|x
			.x.t.E.x
		`,`
			.x.>.x%3
			xx|xex<.
			.t.x.5%2
		`,`
			.xxxx.x.
			xxxex|><
			.xxxx.x.
		`,`
			.....xxx
			..x..xx>
			.....xxx
		`],
		extra: [
			[1, "armory", "t"],
			[2, "captain", "t"],
			[3, "sickbay", "t"],
			[4, "tutorial_follower"],
			[5, "tutorial_armor"],
		],
	},


	ghost_ship: {
		name: "Barnham Dyn, mysterious ship",
		theme: "ship",
		//introduction: `We had no idea how big the ship might be nor where we were at the moment but we were about to find out. `,
		rooms: {
			spawn: "wraiths",
			treasures: ["cargo","cargo","chest"],
		},
		grid: [`
			.<.5.x.4
			48séx0xx
			.7.t.9.t
		`,`
			.>.7.x%3
			t6sx5x<.
			.4.f.x|2
		`,`
			.fxxx.t.
			xxxfs|><
			.xExx.7.
		`,`
			.....xxf
			..x..x1>
			.....fxx
		`],
		extra: [
			[1, "ghost_mizzenmast"],
			[2, "ghost_captain"],
			[3, "ship_treasury", "t"],
			[4, "storage", "t"],
			[5, "ghost_blood"],
			[6, "ghost_bad"],
			[7, "ghost_trashed"],
			[8, "ghost_below"],
			[9, "ghost_hatch", "<"],
			[0, "ghost_gwynt", "q"],
			["é", "ghost_rats"],
		],
	},
	
	
	goth_manor: {
		name: "Old Haversham Manor",
		theme: "ruins",
		//introduction: `We had no idea how big the ship might be nor where we were at the moment but we were about to find out. `,
		rooms: {
			enemies: ["spiders"],
			treasures: ["adventurer", "chest", "burned_cabinet", "burned_treasury"],
			challenges: ["burned_beams"],
		},
		grid: [`
			<........
			xxx......
			..1.....
			.........
			.........
		`,`
			>.xxxxx.x
			xfx...xxe
			..x<x<x..
			xxxxxxxxx
			t.xxExx.t
		`,`
			.fxxxxxt.
			..xxcxx..
			.tx>.>fx.
			..x...x..
			.txexxxc.
		`],
		extra: [
			[1, "dead_end"],
		],
	},
	
	
	shipwreck: {
		name: "Shipwreck",
		theme: "shipwreck",
		loot: "shipwreck",
		rooms: {
			challenges: ["seaweed"],
			treasures: ["cargo","chest"],
		},
		grid: [`
			.......
			.....t<
			......
		`,`
			.1.x.<.
			.xxxxc>
			...c.t.
		`,`
			.x.c.>.
			txxExxx
			.c.x.x.
		`],
		extra: [
			[1, "shipwreck_treasury"],
		],
	},
	
	
	goth_goblins: {
		name: "Gothred countryside",
		theme: "wood",
		rooms: {
			enemies: ["goblins"],
			bosses: ["ogre"],
			challenges: ["tree"],
			treasures: ["stash"],
		},
		grid: `
			..txfxe.x.t
			..xx..cxxtx
			Exf....f...
			..xx..xext.
			...cxxb.t..
		`,
	},

	hdar_jungle: {
		name: "Jungle of H'dar Ğizra",
		theme: "wood",
		rooms: {
			quest: "goth_dorf",
			enemies: ["goblins"],
			bosses: ["ogre"],
			challenges: ["tree"],
			treasures: ["stash"],
		},
		grid: `	
			.xxxxx...xqx........
			.xq..xqxxx.xfxtxxqxe
			.x.b....txxcx......x
			.x.xxcx.......qxxx.q
			.t.x.x..b.x.t.t..x.x
			.fx..x..e.qxxxcxxqxx
			.x..xfxqcxx...x.....
			tce..x..x....xqx....
			.xqxfx..E...xqtqx...
		`,
	},
	
	
	oskyl: {
		name: "Labyrinth of Oskyl",
		theme: "cave",
		rooms: {
			quest: "oskyl",
			enemies: ["spiders"],
			treasures: ["chest","adventurer"],
		},
		grid: [`	
			..................
			....xxxxx..xxxxxx.
			...xx...xxex..xx..
			...x.........xx...
			...xx.......xx....
			....x......xx.....
			..xxq.....xx......
			..x......xx.......
			..t1x|xxxx.......
			....<.............
			..................
			..................
		`,`	
			..................
			....xxx...........
			....x.............
			....xtxx%xxxx.....
			.....x............
			...<xx............
			.....x....xx......
			..xxxxxxxxx.......
			..x...............
			..xx>.............
			..................
			..................
		`,`
			xxxfxxxxex.xxxxxxx
			x.....x....x...x.x
			x..xx.xxfxxxxx.e..
			xx%x......x..x.xtx
			.x...txxx.x......x
			Ex.>xx..x...xxfxxx
			.x.....xxxxxx....x
			xx.xxxx........x.x
			.fxx..xxxfxxtxxx.x
			tx.....x..x....x.x
			.xxxx.....e....xex
			xx..x%xtx.xfxx.x..
		`],
		extra: [
			//[1, "tiles","%"], //TODO
			[1, "high_portal","%"],
		],
	},
	
	
	flooded_oskyl: {
		name: "Flooded Labyrinth",
		theme: "flooded",
		rooms: {
			//quest: "oskyl",
			//enemies: ["spiders"],
			treasures: ["chest","adventurer"],
		},
		grid: `	
			....2xxx1..
			..x.....x..
			..xxtxxxx..
			.t...x....x
			.xxxxExxxxx
			.x.x..x....
			...txxxxx..
			.....x.....
			....xx1xxx2
		`,
		extra: [
			[1, "light"],
			[2, "leave"],
		],
	},
	
	abandoned_fortress: {
		name: "Abandoned fortress",
		theme: "ruins",
		loot: "aquilonian",
		rooms: {
			enemies: ["spiders"],
			bosses: ["smugglers"],
			treasures: ["chest","cargo"],
		},
		grid: [`
			......t..
			....xb3t.
			...xx.t..
			...x.....
			...<.....
			.........
		`,`
			.......x.
			..txxxx<x
			...x...x.
			...1xxxxe
			...>.....
			.........
		`,`
			xx.....xe
			x<xxxxx>x
			.x.....x..
			.x.....x.
			fxxxExxfx
			xx.x.x.xx
		`,`
			tx.......
			x>2xx....
			.f.......
			.x.......
			.........
			.........
		`],
		extra: [
			[1, "fortress_not_abandoned", "q"],
			[2, "fortress_stumble"],
			[3, "fortress_smugglers", "t"],
		],		
	},

	//Karged
	karged: {
		name: "The Tombs of Karged",
		theme: "catacombs",
		introduction: `The crew dug through the debris until we reached the entrance of the ancient Aquilonian tomb. `,
		loot: "aquilonian",
		rooms: {
			enemies: ["spiders","draugrs"],
			treasures: ["chest"],
		},
		grid: [`
			.............
			.............
			.............
			.........t...
			...2%x<x|x1..
			.........t...
			.............
			.............
			.............
		`,`
			.xxxxxxxxrfx.
			.x....x....x.
			.f.xxxxexf.x.
			.x.x.....x.x.	
			Ex%x..>x|e|xt
			.x.x.....x.x.
			.x.txfxxex.x.
			.f....x....f.
			.xxrxxxxxxxx.
		`],
		extra: [
			[1, "karg_tomb", "q"],
			[2, "karg_queen", "t"],
			["%", "high_portal"],
			["|", "low_portal"],
		],	
	},
	
	
	karged_2: {
		name: "The Tombs of Karged",
		theme: "catacombs",
		loot: "aquilonian",
		rooms: {
			enemies: ["spiders","draugrs"],
			treasures: ["chest"],
		},
		grid: [`
			.........
			.........
			.........
			....2x|x<
			.........
			.........
			.........
		`,`
			....E....
			....x....
			..x|x%x..
			txf...ex>
			..x|f%x..
			....e....
			....t....
		`],
		extra: [
			[2, "karg_queen", "t"],
			["%", "high_portal"],
			["|", "low_portal"],
		],	
	},
	
	
	karged_3: {
		name: "The Tombs of Karged",
		theme: "catacombs",
		loot: "aquilonian",
		rooms: {
			enemies: ["spiders","draugrs"],
			treasures: ["chest"],
		},
		grid: [`
			.......
			...<...
			.t|x%1.
			.......
			.......
		`,`
			.x|e%x.
			xx.>.xt
			.e...x.
			tx.<.xx
			.xfxfx.	
		`,`
			...E...
			.x|x|x.
			tx...xx
			.f...x.
			xx.>.xt
			.xxexf.
		`],
		extra: [
			[1, "aqu_tomb", "t"],
			//[2, "karg_queen", "t"],
			["%", "high_portal"],
			["|", "low_portal"],
		],	
	},
	
	aquilonian_tomb: {
		name: "Ancient Aquilonian tomb",
		theme: "catacombs",
		introduction: `I would not be able to find the ancient Aquilonian tomb without the villager's exact directions. It looked just like a random small hill. But further inspection revealed the entrance was not blocked and people (or other things) could easily enter - or leave. `, 
		loot: "aquilonian",
		rooms: {
			enemies: ["draugrs"],
			treasures: ["chest"],
		},
		grid: [`
			........
			........
			.<x%1...
			........
			........
		`,`
			xfx.xex.
			x.xtx.x.
			x>....xE
			f.xtx.x.
			xex.xxf.
		`],
		extra: [
			[1, "aqu_tomb", "t"],
			["%", "high_portal"],
			["|", "low_portal"],
		],	
	},
	
	
}









