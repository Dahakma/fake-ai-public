
export const leveling = [
	[],
//HARLOT
	[
		{},{}, //0 and 1 empty to make the index corespond with the level 
		//level 2
		{
			perks: ["reaction", "merchant", "thief"],
			select_perks: 1,
		},
		//level 3
		{
			skills: ["hide"],
			select_skills: "all",
			perks: ["inconspicuous"],
			select_perks: 1,
		},
		//level 4
		{
			skills: ["pin", "accurate", "feint"],
			select_skills: "all",
			perks: ["danger"],
			select_perks: 1,
			attributes: [
				{str: 1},
				{dex: 1},
				{int: 1},
				{char: 1},
			],
			select_attributes: 1,
		},
	],

//HELLION
	[
		{},{},
		//level 2
		{
			skills: ["swift", "brutal"],
			select_skills: "all",
		},
		//level 3
		{
			select_skills: "all",
			skills: ["stunning", "precise"],
			perks: ["reaction"],
			select_perks: 1,
		},
		//level 4
		{
			skills: ["pin"],
			select_skills: "all",
			perks: ["prof_blades", "prof_hammers", "prof_polearms", "prof_bows", "carrier"],
			attributes: [
				{str: 1},
				{dex: 1},
				{int: 1},
				{char: 1},
			],
			select_attributes: 1,
		},
	],
	
//FALLEN
	[
		{},{},
		//level 2
		{
			spells: ["blades", "flames", "dazzle"],
			select_spells: 2,
		},
		//level 3
		{
			spells: [
				"freeze", "ignite", "blast",
				"frostbite","inferno", 
				"blizzard", "firestorm",
			],
			select_spells: 3,
		},
		//level 4
		{
			spells: ["blastwave"],
			select_spells: 3,
			perks: ["nov_pyro", "nov_ice", "nov_wind"],
			select_perks: 1,
			attributes: [
				{str: 1},
				{dex: 1},
				{int: 1},
				{char: 1},
			],
			select_attributes: 1,
		},
	],
]
