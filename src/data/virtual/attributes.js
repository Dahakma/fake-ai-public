//import {constants} from "Data/constants";

/*
	name {string} - displayed name
	alt {string} - description if the name is not clear enough
	sidebar {boolean} - if `true` considered main stat displayed in side menu
	dyno {boolean} - dynamic, if `true` could be represented by a bar in dungeon & could be dynamically changed; basically HP and ENG
	percent {boolean} - this attribute is considered percentual (value should be around 100; added % to descriptions)
	derive {object} - this attribute is calculated based on other atrributes
		- BEWARE OF THE ORDER TO NOT CREATE INFINITE LOOPS
		-```
			derive: {
				base: 40,
				att: {
					char: 20,
					dex: 20,
				}
			}
		```
		- = 40 + (char * 20) + (dex * 20)
	- counter {object} - when used against another character, these attributes are used to counter
		```
		weapon: {
			counter: {
				att: {
					armor: -1,
				},
			},
		}
		```
		- total caused damage = ally.att.weapon * (   (  100 + ( enemy.att.armor * -1 )  ) / 100   ) //??
	
	combat_damage {string} - type of damage this causes (HP, ENG, basically one of dyno attributes)
		- when this attribute is used in dyno, will be transcalculated into attribute[attribute.combat_damage]
	character_screen {string} - where to display in character menu
		- "primary", "secondary", "dynamic", "magic"
		- (BEWARE - some are disabled by ext.tutorial)
	value {number} - default base value if not defined otherwise (i.e. subject.base[key])
		- if `der` not defined, `derive` is used
		- if `base` not used then `0`
	
*/

export const attributes = {

//PRIMARY ATTRIBUTES
	str: {
		name: `strength`,
		sidebar: true,
		character_screen: "primary",
		value: 0,
	},
	dex: {
		name: `dexterity`,
		sidebar: true,
		character_screen: "primary",
		value: 0,
	},
	int: {
		name: `intelligence`,
		sidebar: true,
		character_screen: "primary",
		value: 0,
	},
	char: {
		name: `charisma`,
		sidebar: true,
		character_screen: "primary",
		value: 0,
	},
	
//DYNAMIC ATTRIBUTES
	hp: {
		name: `health`,
		dyno: true,
		character_screen: "dynamic",
		value: 200,
	},
	
	eng: {
		name: `stamina`,
		dyno: true,
		character_screen: "dynamic",
		value: 200,
	},
	
	
	
	/*
	luck: { //should be hidden
		name: `luck`,
		stat: true,
		percent: true,
		value: 100, 
	},
	*/
	
//SECONDARY ATTRIBUTES
	barter: {
		name: `haggling`,
		alt: `The percentage of the actual price merchants are willing to pay for sold items. `,
		percent: true,
		character_screen: "secondary",
		derive: {
			base: 40,
			att: {
				char: 5,
			},
		},
	}, 
	
	lockpick: {
		name: `lockpicking`,
		alt: `The ability to open locked chests and doors. `,
		percent: true,
		character_screen: "secondary",
		derive: {
			base: 0, //??
			att: {
				dex: 10, //TODO 5?
			},
		},
	},
	
	dance: {
		name: `dancing`,
		alt: `The dancing skill (& modifier for money earned by dancing.`,
		percent: true,
		character_screen: "secondary",
		derive: {
			base: 0,
			att: {
				char: 10,
				dex: 10,
			},
		},
	},
	
	burden: {
		name: "encumbrance",
		alt: "The weight of worn equipment slowing the character down. ",
		percent: true,
		character_screen: "secondary",
		derive: {
			base: -5,
			att: {
				str: -5,
			},
		},
	},
			
	evasion: {
		name: "evasion",
		alt: "The chance to evade the first enemy attack in the combat round. ",
		percent: true,
		character_screen: "secondary",
		derive: {
			base: 5,
			att: {
				dex: 5,
				burden: -1,
			},
		},
	},
	
	reflex: {
		name: "reflex",
		alt: "The base to calculate the initiative, precedence in combat; smaller is better. ",
		//percent: true,
		character_screen: "secondary",
		derive: {
			base: 100,
			att: {
				dex: -5,
				burden: 1,
			},
		},
	},
	
	presence: {
		name: "presence",
		alt: "How much are enemies attracted to the character. ",
		character_screen: "secondary",
		derive: {
			base: 100,
			att: {
				//char: 5,
			},
		},
	},
	
	
	//MAGIC
/*
	pyro: {
		name: "pyromancy",
		alt: "Ability to cast fire spells. ",
		character_screen: "magic",
		value: 1,
	},
	
	cryo: {
		name: "cryomancy",
		alt: "Ability to cast ice spells. ",
		character_screen: "magic",
		value: 1,
	},
	
	aero: {
		name: "aeromancy",
		alt: "Ability to cast air spells. ",
		character_screen: "magic",
		value: 1,
	},
*/
	
	//COMBAT
/*
	weapon: { //TODO - us used to calculated to physical
		name: "damage",
		alt: "Damage caused by the equipped weapon. ",
		character: "primary",
		combat_damage: "hp",
		counter: {
			attx: {
				armor: -1,
			},
		},
		value: 10, //THIS IS UNARMED DAMAGE
	},
*/

	weapon_damage: {
		name: "physical damage",
		alt: "Damage caused by the equipped weapon. ",
		combat_damage: "hp",
		//character_screen: "primary",
		counter: {
			attx: {
				armor: -1,
			},
		},
		
	},
	
	weapon_fire: {
		name: "fire damage",
		alt: "Fire damage caused by the equipped weapon. ",
		combat_damage: "hp",
		counter: {
			attx: {
				fire_resist: -1,
			},
		},
	},
	
	weapon_ice: {
		name: "ice damage",
		alt: "Fire damage caused by the equipped weapon. ",
		combat_damage: "hp",
		counter: {
			attx: {
				ice_resist: -1,
			},
		},
	},
	
	
	weapon_total: {
		name: "Total weapon damage",
		alt: "The maximal possible damage caused by the primary weapon. ",
		character_screen: "primary",
		combat_damage: "hp",
		counter: {
			attx: {
				armor: -1,
			},
		},
		derive: {
			att: {
				weapon_damage: 1,
				weapon_ice: 1,
				weapon_fire: 1,
			},
		},
	},
	
	
	armor: {
		name: "armor",
		alt: "The physical attacks are reduced by X%. ",
		character_screen: "primary",
		percent: true,
	},
	
	critical: { //should be hidden
		name: `critical hit`,
		alt: "Chance to score a lucky hit ignoring enemy evasion and armor. ",
		percent: true,
		character_screen: "primary",
		value: 3, 
	},
	
	
	
	
	//DAMAGE
	/*
	damage: {
		name: "damage",
		alt: "Physical damage ",
		combat_damage: "hp",
		counter: {
			attx: {
				armor: -1,
			},
		},
	},
	*/
	
	/*
	physical: {
		name: "damage",
		alt: "Physical damage ",
		combat_damage: "hp",
		counter: {
			attx: {
				armor: -1,
			},
		},
	},
	*/
	

/*	
	drain: {
		name: "energy drain",
		combat_damage: "eng",
		derive: {
			att: {
				//life: 10,
			},
		},
		counter: {
			attx: {
				ice_resist: -1,
			},
		},
	},
*/	
	
	wind: {
		name: "magic wind damage",
		alt: "base magic wind damage",
		character_screen: "magic",
		combat_damage: "hp",
		
		/*
		derive: {
			att: {
				aero: 10,
			},
		},
		*/
		counter: {
			attx: {
				armor: -1,
			},
		},
	},
	
	fire: {
		name: "magic fire damage",
		alt: "base magic fire damage",
		character_screen: "magic",
		combat_damage: "hp",
		counter: {
			attx: {
				fire_resist: -1,
			},
		},
	},

	ice: {
		name: "magic ice damage",
		alt: "base magic ice damage",
		character_screen: "magic",
		combat_damage: "hp",
		/*
		derive: {
			att: {
				cryo: 10,
			},
		},
		*/
		counter: {
			attx: {
				ice_resist: -1,
			},
		},
	},
	
	poison: {
		name: "poison damage",
		combat_damage: "hp",
		counter: {
			attx: {
				poison_resist: -1,
			},
		},
	},
	
	
	//DAMAGE RESISTANCE
	fire_resist: {
		name: "fire resistance",
		percent: true,
	},
	
	ice_resist: {
		name: "ice resistance",
		percent: true,
	},
	
	poison_resist: {
		name: "poison resistance",
		percent: true,
	},
	
	stun_resist: {
		name: "poison resistance",
		percent: true,
	},
	
	
	//EFFECTS CHANCE
	on_fire: {
		name: "chance to set on fire",
		percent: true,
		combat_effect: "ablaze",
		counter: {
			attx: {
				fire_resist: -1,
			},
		},
	},
	
	on_ice: {
		name: "chance to froze",
		percent: true,
		combat_effect: "frozen",
		counter: {
			attx: {
				ice_resist: -1,
			},
		},
	},
	
	on_poison: {
		name: "chance to poison",
		percent: true,
		combat_effect: "poisoned",
		counter: {
			attx: {
				poison_resist: -1,
			},
		},
	},
	
	stun: {
		name: "chance to stun",
		percent: true,
		combat_effect: "stunned",
		counter: {
			attx: {
				stun_resist: -1,
			},
		},
	},
	
	//EFFECTS RESISTANCE
	
	
	
	//COMBAT MINOR - REMOVE
	evasive_actions: {
		name: "evasions per round",
		note: "The number of evasions the character can perform during one combat round. ",
		value: 1,
	},
	
	
	//NPC only
	greed: {
		name: "greed",
		alt: "Desire to keep the loot for oneself. ",
	},
	
	chem: {
		name: "alchemical attraction",
		alt: "Desire to drink potions. ",
	},
	
	
	
}