export const perks = {

//MAJOR
	mage: {
		name: `Spellcaster`,
		alt: `Character is able to control magical power and cast spells`,
		att: {
			fire: 10,
			ice: 10,
			wind: 10,
		},
	},
	
//origin
	camp: {
		name: `origin: Camp follower`,
		att: {
			str: 1,
		},
	},
	
	urchin: {
		name: `origin: Street urchin`,
		att: {
			dex: 1,
		},
	},
	
	runaway: {
		name: `origin: Runaway`,
		att: {
			int: 1,
		},
	},
	
	princess: {
		name: `origin: Noblewoman`,
		att: {
			char: 1,
		},
	},
	
	berserk: {
		name: `origin: Berserk`,
		att: {
			str: 1,
		},
	},
	
	huntress: {
		name: `origin: Huntress`,
		att: {
			dex: 1,
		},
	},
	
	shaman: {
		name: `origin: Shaman`,
		att: {
			int: 1,
		},
	},
	
	chief: {
		name: `origin: Chief's daughter`,
		att: {
			char: 1,
		},
	},
	
	paladin: {
		name: `origin: Paladin`,
		att: {
			str: 1,
		},
	},
	
	hospitaler: {
		name: `origin: Hospitaler`,
		att: {
			dex: 1,
		},
	},
	
	librarian: {
		name: `origin: Librarian`,
		att: {
			int: 1,
		},
	},
	
	herald: {
		name: `origin: Herald`,
		att: {
			char: 1,
		},
	},


//HELLION
	reaction: {
		name: `fast reactions`,
		att: {
			reflex: -10,
		},
	},
	
	merchant: {
		name: `merchant`,
		att: {
			barter: 10,
		},
	},
	
	thief: {
		name: `thief`,
		att: {
			lockpick: 10,
		},
	},
	
	inconspicuous: {
		name: `inconspicuous`,
		att: {
			presence: -10,
		},
	},
	
	carrier: {
		name: `strong back`,
		att: {
			burden: -10,
		},
	},
	
	danger: {
		name: `danger savvy`,
		desc: `Detects enemies in immediate vincity. `,
		flags: `beware`,
	},
	
	prof_blades: {
		name: `proficiency: Blades`,
		att: {
			weapon_damage: 5,
		},
		condit_desc: "Equipped blade. ",
		condit(subject){
			return subject?.weapon?.type === "blades";
		},
	},
	
	prof_hammers: {
		name: `proficiency: Hammers`,
		att: {
			weapon_damage: 5,
		},
		condit_desc: "Equipped hammer. ",
		condit(subject){
			return subject?.weapon?.type === "hammers";
		},
	},
	
	prof_polearms: {
		name: `proficiency: Polearms`,
		att: {
			weapon_damage: 5,
		},
		condit_desc: "Equipped polearm. ",
		condit(subject){
			return subject?.weapon?.type === "polearms";
		},
	},
	
	prof_bows: {
		name: `proficiency: Bows`,
		att: {
			weapon_damage: 5,
		},
		condit_desc: "Equipped bow. ",
		condit(subject){
			return subject?.weapon?.type === "bows";
		},
	},
	
	nov_pyro: {
		name: `novice of Pyromancy`,
		att: {
			fire: 10,
		},
		condit_desc: "Spellcaster. ",
		condit(subject){
			return subject.perks.includes("mage");
		},
	},
	
	nov_ice: {
		name: `novice of Cryomancy`,
		att: {
			ice: 10,
		},
		condit_desc: "Spellcaster. ",
		condit(subject){
			return subject.perks.includes("mage");
		},
	},
	
	nov_wind: {
		name: `novice of Aeromancy`,
		att: {
			wind: 10,
		},
		condit_desc: "Spellcaster. ",
		condit(subject){
			return subject.perks.includes("mage");
		},
	},
	
}