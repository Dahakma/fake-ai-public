import {constants} from "Data/constants";
/*
	`weapon: 0.5`
	is shorthand for 
	```
	attx: {
		weapon_damage: 0.5,
		weapon_fire: 0.5,
		weapon_ice: 0.5,
	}
	```
*/
export const skills = {
	
	
	danger: {
		name: "Danger savvy. ",
		desc: "Detects close enemies. ",
	},
	
//POTIONS
	health: {
		zone: "ally",
		desc: "Refills health. ",
		att: {
			hp: constants.hp.elixir,
		},
	},
	
	energy: {
		zone: "ally",
		desc: "Refills energy. ",
		att: {
			eng: constants.eng.elixir,
		},
	},

	antivenom: {
		zone: "ally",
		desc: "Cure poison. ",
		effects: [
			"cure",
		],
	},

		
		
		
//CHARMS
	charmLightning: {
		zone: "ranged_square",
		att: {
			ice: 20,
			on_ice: 25,
		},
	},
	
	charmBlizzard: {
		zone: "ranged_square",
		desc: "Attacks 4 enemies causing them ice damage. ",
		att: {
			ice: 20,
			on_ice: 50,
		},
	},
	
	charmFirestorm: {
		zone: "ranged_anchor",
		desc: "Attacks 4 enemies causing them fire damage. ",
		att: {
			fire: 20,
			on_fire: 50,
		},
	},
	
	charmBlades: {
		//name: "
		zone: "ranged_line",
		desc: "Attack line of three enemies causing them 30 physical damage. ",
		att: {
			physical: 30,
		},
	},
	

	
	charmDazzle: {
		//name: "Dazzling charm",
		//inventory: "charmDazzle",
		zone: "two_ranged_lines",
		att: {
			stun: 40,
		},
	},	
	
	charmDarkness: {
		//name: "Darkness charm",
		//inventory: "charmDazzle",
		zone: "two_ranged_lines",
		att: {
			stun: 40,
		},
	},
	
		
	/*	
	instakill: {
		name: "###Instant kill",
		desc: "Kills the enemy",
		zone: "ranged_square",
		att: {
			physical: 20,
			//fire: 999,
			crit: 50,
			//crit: 200,
		},
		attx: {
		// ice: 10,
		//	crit: 100,
		},
	},
	*/
	
	
	
	
//HELLION
	swift: {
		name: "Swift Attack",
		desc: "Attacks the closest three enemies, dealing each half of the base damage. ",
		zone: "line",
		weapon: 0.5,
		price: {
			eng: -10,
		},
		condit_desc: "Has melee weapon. ",
		condit(subject){
			if(subject?.weapon?.type === "bows") return false;
			return subject.weapon; //equiped weapon
		},
	},
	
	brutal: {
		name: "Brutal Assault",
		desc: "Attack dealing double damage to single enemy. ",
		zone: "close",
		weapon: 2,
		price: {
			eng: -12,
		},
		condit_desc: "Has melee weapon. ",
		condit(subject){
			if(subject?.weapon?.type === "bows") return false;
			return subject.weapon; //equiped weapon
		},
	},
	
	stunning: {
		name: "Stunning Strike",
		desc: "Attack with high chance to stun the opponent. ",
		zone: "close",
		weapon: 1,
		att: {
			stun: 50,
		},
		price: {
			eng: -10,
		},
		condit_desc: "Has melee weapon. ",
		condit(subject){
			if(subject?.weapon?.type === "bows") return false;
			return subject.weapon; //equiped weapon
		},
	},
	
	precise: {
		name: "Precise Hit",
		desc: "Well-calculated attack with high chance to become a critical hit. ",
		zone: "close",
		weapon: 1,
		att: {
			critical: 50,
		},
		price: {
			eng: -12,
		},
		condit_desc: "Has melee weapon. ",
		condit(subject){
			if(subject?.weapon?.type === "bows") return false;
			return subject.weapon; //equiped weapon
		},
	},
	
	
	accurate: {
		name: "Accurate Shot",
		desc: "Well-calculated shot with high chance to become a critical hit. ",
		zone: "ranged",
		weapon: 1,
		att: {
			critical: 50,
		},
		price: {
			eng: -10,
		},
		condit_desc: "Has bow. ",
		condit(subject){
			if(subject?.weapon?.type === "bows") return true;
			//return subject.weapon; //equiped weapon
		},
	},
//SPELLS
	missile: {
		name: "Magic Missile",
		desc: "Forms a dart out of thin air and hits enemy with base wind damage. ",
		zone: "ranged",
		attx: {
			wind: 1,
		},
		price: {
			eng: -5,
		},
	},
	
	blades: {
		name: "Blades",
		desc: "Turns air into razor-sharp blades and sends them against line of three enemies, dealing them half of the base wind damage. ",
		zone: "ranged_line",
		attx: {
			wind: 0.5,
		},
		price: {
			eng: -9,
		},
	},
	
	flames: {
		name: "Flames",
		desc: "Sends flames to the closes two enemies, dealing them base fire damage with low chance to set them ablaze. ",
		zone: "two_close",
		attx: {
			fire: 1,
		},
		att: {
			on_fire: 20,
		},
		price: {
			eng: -10,
		},
	},
	
	
	dazzle: {
		name: "dazzle",
		desc: "Spell with high chance to stun enemy. ",
		zone: "close",
		att: {
			stun: 80,
		},
		price: {
			eng: -8,
		},
	},
	
	
//CAUSE EFFECT
	freeze: {
		name: "Freeeze",
		desc: "Deals base ice damage to one enemy with high chance to freeze him. ",
		zone: "ranged",
		attx: {
			ice: 1,
		},
		att: {
			on_ice: 80,
		},
		price: {
			eng: -12,
		},
	},
	
	ignite: {
		name: "Ignite",
		desc: "Deals base fire damage to one enemy with high chance to set him on fire. ",
		zone: "ranged",
		attx: {
			fire: 1,
		},
		att: {
			on_fire: 80,
		},
		price: {
			eng: -14,
		},
	},
	
	blast: {
		name: "blast",
		desc: "Deals base wind damage to close enemy with high chance to stun him. ",
		zone: "close",
		attx: {
			wind: 1,
		},
		att: {
			stun: 80,
		},
		price: {
			eng: -16,
		},
	},
	
	
	
	
//DOUBLE DAMAGE
	frostbite: {
		name: "Frostbite",
		desc: "Deals double ice damage to one enemy with medium chance to freeze him. ",
		zone: "ranged",
		attx: {
			ice: 2,
		},
		att: {
			on_ice: 60,
		},
		price: {
			eng: -20,
		},
	},
	
	inferno: {
		name: "Inferno",
		desc: "Deals double fire damage to one enemy with medium chance to set him ablaze. ",
		zone: "ranged",
		attx: {
			fire: 2,
		},
		att: {
			on_fire: 60,
		},
		price: {
			eng: -20,
		},
	},
	
	
//AREA
	blizzard: {
		name: "Blizzard",
		desc: "Engulf four enemies with flames, dealing them base fire damage with low chance to set them ablaze. ",
		zone: "ranged_square",
		attx: {
			ice: 1,
		},
		att: {
			on_ice: 20,
		},
		price: {
			eng: -28,
		},
	},
	
	firestorm: {
		name: "Firestorm",
		desc: "Engulf four enemies with flames, dealing them base fire damage with low chance to set them ablaze. ",
		zone: "ranged_anchor",
		attx: {
			fire: 1,
		},
		att: {
			on_fire: 20,
		},
		price: {
			eng: -30,
		},
	},
	
	blastwave: {
		name: "Blastwave",
		desc: "Conjures devastating blastwave, dealing half of base wind damage to six enemies with low chance to stun them. ",
		zone: "two_lines",
		attx: {
			wind: 1,
		},
		att: {
			stun: 20,
		},
		price: {
			eng: -35,
		},
	},
	
	
	feint: {
		name: "Feint",
		desc: "Fakes attack to distract enemies and makes them unable to evade. ",
		zone: "two_lines",
		price: {
			eng: -5,
		},
	},
	
	
	
	instakill: {
		name: "Instakill",
		desc: "Kills the enemy",
		zone: "ranged_square",
		att: {
			physical: 999,
		},
		attx: {
			crit: 100,
		},
	},
	
	
	protect: {
		name: "Protect",
		desc: "Protect ally by trying to attract enemy attention to herself. ",
		zone: "ally_not_self",
		effects: [
			"protected",
		],
		price: {
			eng: -10,
		},
	},
	
	hide: {
		name: "Hide behind",
		desc: "Hide behind ally who has to deal with most of the enemy attention. ",
		zone: "ally_not_self",
		effects: [
			"protecting",
		],
		price: {
			eng: -10,
		},
	},
	
	
//

	pin: {
		name: "Pin Down",
		desc: "Overwhelm enemy with a fast series of light attacks, dealing him half base damage and making him vulnarable to other attacks for the rest of the round.  ",
		zone: "close",
		weapon: 0.5,
		effects: [
			"pinned",
		],
		price: {
			eng: -10,
		},
		condit_desc: "Has weapon. ",
		condit(subject){
			//if(subject?.weapon?.type === "bows") return false;
			return subject.weapon; //equiped weapon
		},
	},
	
	
	
	
	
	
	taunt: {
		name: "Taut",
		desc: "Provoke ",
		zone: "ranged_square",
		att: {
			physical: 999,
		},
		attx: {
			crit: 100,
		},
		effect: "taunt",
	},
	
	

/*	
	taunt: {
		name: "Taut",
		desc: "Provoke ",
		zone: "ranged_square",
		att: {
			physical: 999,
		},
		attx: {
			crit: 100,
		},
		effect: "taunt",
	},
*/
	/*
	💢
	⚔️
	📌
	line: {
		name: "Line attack",
		desc: "Attack first line, causing each enemy half of base damage",
		zone: "line",
		weapon: 0.5,
		price: {
			eng: -20,
		},
		condit(subject){
			return subject.weapon; //equiped weapon
		},
	},
	
	firesword: {
		name: "Fire sword",
		desc: "Attack first line, causing each enemy half of base damage",
		zone: "line",
		weapon: 0.5,
		weap: {
			fire: 4,
		},
		condit(subject){
			return subject.weapon; //equiped weapon
		},
	},
	
	
	flamer: {
		name: "Flame strike",
		desc: "Attack first line, causing each enemy half of base damage",
		zone: "line",
		 
		
		att: {
			fire: 10,
			on_fire: 50,
			on_ice: 100,
			on_poison: 50,
			stun: 80, 
			//ablaze: 0.1,
		},
	},
	
	tele: {
		name: "Telekinetic push",
		desc: "Hits the close enemy",
		zone: "ranged_line",
		att: {
			physical: 1,
			stun: 50,
		},
		condit: ()=> true,
	},	
	*/
}



export const skills_debug = [];
Object.keys(skills).forEach( key => {
	const temp = {...skills[key]};
	temp.name = temp.name ? `###${temp.name}` : `###${key}`;
	skills[`_${key}`] = temp;
	skills_debug.push(`_${key}`);
})

//console.error(skills_debug);
//console.log(skills);