
const anchored_to_tile = {
	ranged: [
		{
			on: {x: 0, y: 0},
			mark: [{x: 0, y: 0}],
		},
	],
	
	ranged_line: [
		{
			on: {x: 0, y: 0},
			mark: [
				{x: 0, y: 0},
				{x: -1, y: 0},
				{x: 1, y: 0},
			],
		},
	],
	
	ranged_anchor: [
		{
			on: {x: 0, y: 0},
			mark: [
				{x: 0, y: 0},
				{x: -1, y: 0},
				{x: 1, y: 0},
				
				{x: 0, y: 1},
			],
		},
	],
	
	ranged_square: [
		{
			on: {x: 0, y: 0},
			mark: [
				{x: 0, y: 0},
				{x: 1, y: 0},
				{x: 0, y: 1},
				{x: 1, y: 1},
			],
		},
	],
	
	two_ranged_lines: [
		{
			on: {x: 0, y: 0},
			mark: [
				{x: 0, y: 0},
				{x: -1, y: 0},
				{x: 1, y: 0},
				{x: 0, y: 1},
				{x: -1, y: 1},
				{x: 1, y: 1},
			],
		},
	],
}


const anchored_to_subject = {
	ally_not_self: [
		{
			on: {x: -1, y: 0},
			mark: [{x: -1, y: 0}],
		},
		{
			on: {x: 1, y: 0},
			mark: [{x: 1, y: 0}],
		},
		/*
		{
			on: {x: -2, y: 0},
			mark: [{x: -2, y: 0}],
		},
		{
			on: {x: 0, y: 0},
			mark: [{x: 0, y: 0}],
		},
		{
			on: {x: 2, y: 3},
			mark: [{x: 2, y: 3}],
		},
		*/
	],
	
	ally: [
		{
			on: {x: -1, y: 0},
			mark: [{x: -1, y: 0}],
		},
		{
			on: {x: 0, y: 0},
			mark: [{x: 0, y: 0}],
		},
		{
			on: {x: 1, y: 0},
			mark: [{x: 1, y: 0}],
		},
		/*
		{
			on: {x: -2, y: 0},
			mark: [{x: -2, y: 0}],
		},
		{
			on: {x: 0, y: 0},
			mark: [{x: 0, y: 0}],
		},
		{
			on: {x: 2, y: 3},
			mark: [{x: 2, y: 3}],
		},
		*/
	],
	
	close: [
		{
			on: {x: -1, y: 1},
			mark: [{x: -1, y: 1}],
		},
		{
			on: {x: 0, y: 1},
			mark: [{x: 0, y: 1}],
		},
		{
			on: {x: 1, y: 1},
			mark: [{x: 1, y: 1}],
		},
	],
	
	two_close: [
		{
			on: {x: -1, y: 1},
			mark: [
				{x: -1, y: 1},
				{x: 0, y: 1},
			],
		},
		{
			on: {x: 0, y: 1},
			mark: [
				{x: 0, y: 1},
				{x: 1, y: 1},
			],
		},
		{
			on: {x: 1, y: 1},
			mark: [
				{x: 0, y: 1},
				{x: 1, y: 1},
			],
		},
	],
	
	medium: [
		//close
		{
			on: {x: -1, y: 1},
			mark: [{x: -1, y: 1}],
		},
		{
			on: {x: 0, y: 1},
			mark: [{x: 0, y: 1}],
		},
		{
			on: {x: 1, y: 1},
			mark: [{x: 1, y: 1}],
		},
		
		//+ another row
		{
			on: {x: -1, y: 2},
			mark: [{x: -1, y: 2}],
		},
		{
			on: {x: 0, y: 2},
			mark: [{x: 0, y: 2}],
		},
		{
			on: {x: 1, y: 2},
			mark: [{x: 1, y: 2}],
		},
	],
	
	
	line: [
		{
			on: {x: -1, y: 1},
			mark: [{x: -1, y: 1}, {x: 0, y: 1}, {x: 1, y: 1}],
		},
		{
			on: {x: 0, y: 1},
			mark: [{x: -1, y: 1}, {x: 0, y: 1}, {x: 1, y: 1}],
		},
		{
			on: {x: 1, y: 1},
			mark: [{x: -1, y: 1}, {x: 0, y: 1}, {x: 1, y: 1}],
		},
	],	
	
	
	two_lines: [
		{
			on: {x: -1, y: 1},
			mark: [
				{x: -1, y: 1}, {x: 0, y: 1}, {x: 1, y: 1},
				{x: -1, y: 2}, {x: 0, y: 2}, {x: 1, y: 2},
			],
		},
		{
			on: {x: 0, y: 1},
			mark: [
				{x: -1, y: 1}, {x: 0, y: 1}, {x: 1, y: 1},
				{x: -1, y: 2}, {x: 0, y: 2}, {x: 1, y: 2},
			],
		},
		{
			on: {x: 1, y: 1},
			mark: [
				{x: -1, y: 1}, {x: 0, y: 1}, {x: 1, y: 1},
				{x: -1, y: 2}, {x: 0, y: 2}, {x: 1, y: 2},
			],
		},
	],	
}



/*
	origin - origin is calculated from
		"subject" - the currently active combatant (to attack enemies in certain distance for PC)
		"tile" - the tile mouse is pointing on (to hit any enemy with ranged skill)
	combs - combinations	
		on - coordinates of tile mouse is pointing on
		mark - coordinates of tiles which are highlighted
*/

export const zones = (()=>{
	const az = {};
	
	Object.keys(anchored_to_subject).forEach( key => {
		az[key] = {
			origin: "subject",
			combs: anchored_to_subject[key],
		}
	});
	
	Object.keys(anchored_to_tile).forEach( key => {
		az[key] = {
			origin: "tile",
			combs: anchored_to_tile[key],
		}
	});
	
	return az;
})();