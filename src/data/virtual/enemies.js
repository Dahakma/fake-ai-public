/**
	list of enemies encounterable in dungeons
*/

/*
	`damage: 0.5`
	is shorthand for 
	```
	att: {
		weapon_damage: 0.5,
	}
	```
*/

//`name`s should be unique
export const enemies = {
	dummy: {
		name: "Dummy",
		base: {
			hp: 20,
			//reflex: 100,
			//evasion: 10,
		},
		weapon: {
			name: "teeth",
			damage: 1,
		},
	},
	
	tough_dummy: {
		name: "Tough dummy",
		base: {
			hp: 200,
			evasion: 0,
			armor: 50,
		},
		weapon: {
			name: "teeth",
			damage: 1,
		},
	},
	
	
	
	rat: {
		name: "Rat",
		base: {
			hp: 14,
			reflex: 90,
			evasion: 10,
		},
		weapon: {
			name: "teeth",
			damage: 3,
		},
	},
	
	rous: {
		name: "Rat of unsuall size",
		base: {
			hp: 30,
			reflex: 120,
			evasion: 10,
		},
		weapon: {
			name: "sharp teeth",
			damage: 7,
		},
	},
	
	
	goblin: {
		name: "Goblin",
		base: {
			hp: 20,
			reflex: 100,
			evasion: 10,
			armor: 0,
		},
		weapon: {
			name: "rusty sabre",
			damage: 5,
		},
	},
	
	
	y_beast: {
		name: "The Beast",
		base: {
			hp: 100,
			reflex: 100,
			evasion: 60,
			armor: 0,
		},
		weapon: {
			name: "deadly talons",
			damage: 18,
		},
	},
	
	ogre: {
		name: "Ogre",
		base: {
			hp: 70,
			reflex: 200,
			evasive_actions: 0,
			fire_resist: 50,
		},
		weapon: {
			name: "huge club",
			damage: 28,
		},
	},
	
	
	
	
	fast_pirate: {
		name: "Fast pirate",
		base: {
			hp: 30,
			reflex: 80,
			evasion: 40,
		},
		weapon: {
			name: "cutlass",
			damage: 10,
			effects: {},
		},
	},
	
	strong_pirate: {
		name: "Strong pirate",
		base: {
			hp: 40,
			reflex: 120,
			evasion: 5,
			armor: 40,
		},
		weapon: {
			name: "mace",
			damage: 15,
		},
		att: {
			stun: 40,
		},
	},
	
	pirate: {
		name: "Pirate",
		base: {
			hp: 30,
			reflex: 100,
			evasion: 10,
		},
		weapon: {
			name: "cutlass",
			damage: 10,
			effects: {},
		},
	},
	
	
	wraith: {
		name: "Wraith",
		base: {
			hp: 6,
			reflex: 100,
			evasion: 80,
			armor: 80,
			critical: 80,
			fire_resist: 80,
			ice_resist: 80,
			wind_resist: 80,
		},
		weapon: {
			name: "touch from the other side",
			damage: 10,
		},
		/*
		att: {
			stun: 40,
		},
		*/
	},
	
	chosen: {
		name: "Chosen",
		base: {
			hp: 120,
			reflex: 50,
			evasion: 80,
			critical: 100,
		},
		weapon: {
			name: "teeth and nails",
			damage: 20,
		},
	},
	
	
	spider: {
		name: "Huge Spider",
		base: {
			hp: 33,
			reflex: 90,
			evasion: 33,
		},
		weapon: {
			name: "mandibles",
			damage: 6,
		},
		att: {
			on_poison: 33,
		},
	},
	
	
	draugr: {
		name: "Draugr",
		base: {
			hp: 100,
			armor: 50,
			fire_resist: 50,
			ice_resist: 50,
			wind_resist: 50,
			reflex: 125,
			//evasion: 33,
			evasive_actions: 0,
			critical: 10,
		},
		weapon: {
			name: "sword",
			damage: 30,
		},
		att: {
			stun: 10,
		},
		
	},
	
	
	
};