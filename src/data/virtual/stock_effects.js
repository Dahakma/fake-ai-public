export const stock_effects = {
//POTIONS
	forti: {
		name: `bovine fortitude`,
		att: {str: 1},
	},
	agili: {
		name: `elvish agility`,
		att: {dex: 1},
	},
	cunin: {
		name: `mischievous cunningness`,
		att: {int: 1},
	},
	
	
	
	infeb: {
		name: `infernal beauty`,
		att: {char: 1},
	},
	grace: {
		name: `cat-like grace`,
		att: {dex: 1},
	},
	
	
	
	

	ablaze: {
		combat: true,
		name: `ablaze`,
		combat_desc: `was set ablaze`,
		icon: `🔥`,
		ticks: 3,
		att: {
			fire: 5,
		},
	},
	
	frozen: {
		combat: true,
		name: `frozen`,
		combat_desc: `was frozen`,
		icon: `❄️`,
		ticks: 3,
		attx: {
			reflex: 1.33,
			evasion: 0.5,
		},
	},
	
	stunned: {
		combat: true,
		name: `stunned`,
		combat_desc: `was stunned`,
		icon: `💫`,
		ticks: 2,
		flags: ["pass", "autocrit"],
	},
	
	pinned: {
		combat: true,
		name: `pinned`,
		combat_desc: `is pinned down`,
		icon: `📌`,
		ticks: 1,
		flags: ["autocrit"],
	},
	
	poisoned: {
		//combat: true,
		name: `poisoned`,
		icon: `☠️`,
		combat_desc: `was poisoned`,
		ticks: 10,
		att: {
			poison: 5,
		},
	},
	
	protected: {
		combat: true,
		name: `protected`,
		combat_desc: `is protected`,
		icon: `🔘`,
		ticks: 5,
		att: {
			presence: -50,
		},
		//flags: ["pass", "autocrit"],
	},
	
	protecting: {
		combat: true,
		name: `protecting`,
		combat_desc: `is protecting`,
		icon: `⚔️`,
		ticks: 5,
		att: {
			presence: 50,
		},
		//flags: ["pass", "autocrit"],
	},
	
	
	
	//TODO
	cure: {
		name: `cure`,
		combat_desc: `was cured`,
	},
	
	
	
//support potions
	/*
	hp: {
		name: "Health Potion",
		dyno: {
			hp: 8,
		},
	}
	
	hp: {
		name: "Health Potion",
		dyno: {
			hp: 8,
		},
	}
	*/
	/*
	
	
	
	bull_strength: {
		ticks: 10,
		att: {
			str: 1,
		}
		
	}, 
	
	
	
	
	
	
	
	
	
	
	
	
	*/
};