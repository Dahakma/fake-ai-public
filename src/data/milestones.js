/**
	default values of milestones tracking the progress through the game
	actual values are a global variable `window.mile`
*/

export const default_mile = {
	
	
//// MAIN //////////////////////////////////////////////////
	
	sub: 0, //how submissive Kate is (negative dominant)
		/*
			INTRO
			+/- horrified/outraged
			- refuse AI
			+ kneel 
			WEEK 1
			- longest skirt
				+ but sorry
			+ short skirt /todo
			- scattered freshmen
			+ fight beg for mercy
			TODO
		*/
	
	slut: 0, //how slutty, sexually adventurous Kate is (negative prude)
		/*
			+/- suck cock hacker/strongly refuse
			+ makeout with AI
			+ shameless stripping
			
			+ shortest skirt
			+ feyd nude
			+ slutty selfie
			+ topless selfie
			- freshmen embarrassed for no bra
			TODO
		*/
		
	stat: 0, //status, how popular Kate is among her peers
		/*
			+ being class chairman
			- not adressed freshmen making fun of her
			- admited she was on date with AAA to girls after being caught
			- admitted dating with AAA
			+ eva convinced not wear bra too, trendsetter
			+ helping vendy / mocking vendy 
			- vendy is running
			- mocked by vendy after she decided to run
			- arguing with eva when she runs
			- distilation accident, being slutty or embarrassed
			- bar kissing girls accident
			- bar kissing girls accident puss exposed 
			- swimming skimpy/trasnparent bikini
			- rv_lower_repo gym pants
			
			TODO 
		*/
	
	immersion: 0, //how much Kate enjoys the game (negative - she does not enjoy it at all)
		/*
			TODO
		*/
	
	
	lesbian: 0, //how much is Kate interested in other girls 
		/*	
			+/- sex with AI (intro)
			+ likes lesbian scenes (review task)
			
			+/- AI oral sex
			+/- AI strapon sex
			
			+/- likes making out with zan (kissing task)
			
			-- AI weekend 3
			
			+boatsex Lyssa
			+boatsex Sirael
		*/

	


//// TASKS ///////////////////////////////////////////////////////////
	
//WEEK 1 TASKS
	short_skirt: 0,
	show_legs: 0, 
	/*
		1 long
		2 short
		3 mini/hot pants
		4 micro
	*/
	sexy_selfie: 0,
	/*
		1 normal
		2 sexy
		3 underwear
		4 topless
	*/
	porn_review: false, //true - done
	porn_review_favourite: 0,
	/*
		1 brutal gangbang
		2 lesbians
		3 passionate hero sex
	*/
	porn_review_end: 0,
	/*
		1 masterpiece
		2 I cum so much
		3 pathetic pervs
	*/
	cucumber: 0, //1 long; 2 thick; 3 big & curvy
	makeup: false,
	flash_panties: false,
	sexy_top: false,
	no_bra: false,
	



//WEEK 2 TASKS
	dildo_task: 0, //new bought dildo; 
	dildo: 0, //currently owned biggest dildo
	/*
		1 little blue; 
		2 glass; 
		3 bbc; 
		4 big purple; 
		5 beastly
		6 dragon
		7 horse
	*/
	pink_dildo: false, //nerds know about her pink dildo
	dildo_qcc_money: false, //dildo task; qcc borrowd money for dildo
	
	dildo_0: true, //trusty pink dildo
	dildo_1: false,
	dildo_2: false,
	dildo_3: false, //bbc
	dildo_4: false,
	dildo_5: false, //fantasy
	dildo_6: false,
	dildo_7: false, //horse

	/*
		case 1: return "ergonomic blue dildo";
		case 2: return "sleek glass dildo";
		case 3: return "thick black dildo";
		case 4: return "huge purple dildo";
		case 5: return "obscene fantasy dildo";
		case 6: return "monstrous dragon dildo";
		case 7: return "absolutely enormous horsecock-shaped dildo";
		case 0: return "trusty pink dildo"
	*/	
		
	
	
	legwear_type: 0, 
	/*
		-1 fail
		1 high sock
		2 stockings
		3 pantyhose
		4 sexy pantyhose
	*/
	legwear_reveal: 0, 
	/*
		0 - unknown
		1 - revealed
		2 - fully revealed
	*/
	
	sexy_panties: 0,
	/*
		-1 failed the task
		1 wore sexy panties
		2 wore no panties
	*/
	sexy_panties_control_fail: false, //failed inspections for wearing sexy underwear 
	sexy_panties_control_pass: false, //passed inspections for wearing sexy underwear 
	
	bikini: 0,
	/*
		1 prude (but replaced with transparent)
		2 sexy
		3 slutty
	*/
	
	tan: 0,
	/*
		1 light
		2 medium
		3 dark
	*/

	flash_breast_class: 0,
	/*
		1 hesitantly
		2 shamelessly
	*/
	
	topless_picture: 0, 
	/*
		1 bare tits
		2 with degrading text
	*/
	
	dye: false, //true task dyed hair (hair could be dyed without the task (mile.advice_2 == 3)
	dye_friend: 0,
	/*
		0 - go alone
		10 - sasa no dye
		11 - sasa dye nice
		12 - sasa dye crazy 
	*/
	dye_message: 0, //send message about dyed hair to 1 qaa; 3 qcc; 4 sas; 5 eva
	hair_color: "", //string with hair color
	hair_crazy: false, //pink or green, not normal like auburn or blonde //TODO red?
	
	choker: false, //task choker; also could be asked to wear choker by mile.advice_2 == 2 
					//TODO - advice changes to true too, works differently than mile.dye!!!!
	collar: false, //task collar or shopping with qbb, TODO
	collar_task: false, //ext.rules.collar 
	
//WEEK 3
	piercings: 0,
	/*
		1 tongue
		2 face
		3 nipples
		4 genitals
	*/
	
	open_legs: false, //task to always had spread legs when siting
	open_legs_depanties: false, //panties were impounded because of rules violations
	open_legs_creep: "", //creepshot worn skirt
	open_legs_creep_panties: "", //creepshot worn panties
	
	shaven: false, //pubic hair shaven
	vibrating_panties: 0,
	/*
		4 loves confident
		3 hates confident
		2 hates sub
		1 loves sub
	*/
	
	poledancing: 0, //1 done
	
	buttplug: 0,
	/*
		1 - buttplug task finished & owns small buttplug
		2 - owns medium plug
	*/
	anal: 0, 
	/*
		0 - virgin
		1 - tried
		2 - loves it
	*/
	into_anal: 0, 
	/*
		+1/+2 tried/loves
		+/- AAA anal
		++/+/- ayy anal
		+ steel buttplug (sex shop)
	*/
	into_pegging: 0, 
	/*
		+/- aaa strapon
	*/
	
	
//WEEKEND TASK
	fight: 0, 
	/*
		1 won the fight with AI
		-1 lost it 
	*/
	
	game_night_maid: 0,
	/*
		-1 refused to wear maidgear
		1 agreed
		2 agreed intrigued
	*/
	olives: false, //black olives
	
	
	drunk: 0, //kiss task drunkeness counter
	qcc_drunk: 0, //qcc got drunk during kissing task; 0 no; 1 drunk; 2 vomited
	qcc_sas: 0, // 1 - qcc & sas interacted during kiss task
	sas_kiss: 0, //kissing sas during kiss task
	/*
		1 - forced
		2 - convinced
		3 - she initiated
	*/
	eva_kiss: 0,
	/*
		1 - refused lightly
		2 - refused harshly
		3 - refused while berating kate
	*/
	tom_zan_kiss: 0,
	/*
		2 makeout with zan
		1 zan better
		-1 tom better
		-2 makeout with tom
	*/
	qaa_kiss_angry: false, //angry argument with qaa
	just_kiss: false, //kissed justine
	kiss_crash: false, //embarrassed while task

	prostitution: 0, //TODO STUIPD NAME OF VARIABLE
	/*
		1 agreed
		-1 awkwardly refused
		-2 resolutely refused
		-3 angrily refused
	*/
	prostitute_nego: 0,
	/*
		-1 negotiate for price and failed
		1 succed the second try
		2 succeed
	*/
	
	
	whore: 0,
	/*
		kiss - drunk - blowjob stranger
		bj or sex - a_family - cousin
	*/
	
	
	
	
 
// VIRTUAL GAME //////////////////////////////////////////

	game_progress: 0,
	/*
		0 - intro
		10 - Feyd Mora visited
		20 - Voyage to Gothred
		21 - arrived to Gothred
		22 - Meeting with Arazor, now searching for the crew
		23 - has ship, sailing
		24 - Karged explored, next step Oskyl
		28 - Gothred finished
	*/

//FEYD MORA
	feyd_keryk: false, //met with her contact Keryk
	feyd_keryk_name: "",//Keryk's actual name
	feyd_keryk_attempts: 0, //number of attempts to find him
	feyd_rats: false, //finished first dungeon
	
	feyd_topless: false, //walked topless through Feyd Mora
	feyd_bottomless: false, //walked bottomless through Feyd Mora
	
	feyd_smuggler: 0, //trick smuggler and lure him out
	/*
		-1 - failed
		0 - not initiated
		1 - initiatied 
		2 - finished
	*/
	
	feyd_roses: 0, //collecting roses for maid quest
	/*
		0 - not initiated
		1 - initiated 
		2 - delivered to maid
		3 - delivered to maid for extra price
		4 - sold to noblewoman
	*/
	
	feyd_yelaya: 0,
	/*
		0 - not initiated
		1 - agreed to join quest
		further boost relationship
		6 - admited she isn't adventurer
		10 - feyd storyline finished
	*/
	
	feyd_yelaya_saved: 0, //1 she was saved; -1 she died


//GOTHRED
	goth_boarded: undefined, 
	/*
		1 - got boarded by pirates without fight
		2 - tryied to convince seamen to fight but failed
		3 - fighted but lost 
	*/
	goth_gal_punch: false, //accidentally punched Galawar
	goth_gal_promise: false, //promised sex to Galawar
	goth_gal_sex: false, //had sex with Galawar on the ship
	goth_gal_money: false, //borowed money from Galawar
	got_gal: 0,
	/*
		1 - met galawar in tavern
		further boost relationship
	*/	
	goth_stripper: 0, // -1 - stripping suggested; 1 danced; 2 stripped; 3 stripped fully;
	goth_marshal: 0, 
	/*
		1 - met marshal; 
		2 - slayed goblins
		3 - spider manor initiated
		4 - spiders finished, no more quests
	*/
	goth_ste: 0, 
	/*
		1 - met with stede 
		futher boost relationship
	*/
	goth_lys: 0,
	/*
		1 - met lyssa
		2 - chatted with lyssa
		further boost relationship 
	*/
	
	goth_arm: 0, //how many times tried arm wrestling
	goth_arm_win: 0, //how many times successfully
	
	goth_sir: 0,
	goth_sir_indy: 0,
	/*
		+ refused probing against her will
		- let her probed
		+ you should not be slave
		- you sould follow authorities
		+ reluctant to sexually use her
		+ asked Galawar to be nice on her
	*/
	
	goth_sirael_v: false,
	goth_sir_probed: 0,
	/*
		-2 - strong refusal
		-1 - diagreed
		1 - people noticed Sirael
		2 - agreed 	
	*/
	
	goth_arazor: 0,
	/*
		1 - meeting arranged
		2 - discussed, searching crew
		3 - ship bought
		4 - ship ready to sail
		5 - scroll discovered
		6 - scroll solved - labyrinth
		7 - scepter discovered
	*/
	goth_ghost_ship: 0,
	/*
		1 - initiated 
		2 - done
	*/
	
	goth_smugglers: 0,
	/*
		1 - discovered
		-1 - we won't tell anybody
		2 - we'll tell authorities
		3 - rewarded by Marshal
	*/
	goth_dorf: false, //true - delivered lumber to arzenal Datan Gulnas
	
	goth_whoring: 0, 
	/*
		-1 disable
		1 whoring mentioned; 
		2 enabled
		3 still undecided
		4 decided to become whore
		5 eagerly decided to become a whore
	*/
	goth_whoring_jons: 0,
	goth_whoring_money: 0,
	
//SHIP
	ship_anchor: undefined, //if and where the ship is anchored
	ship_x: undefined, //x position on the map of Dewyrain sea
	ship_y: undefined, //y position on the map of Dewyrain sea
	ship_name: "Plague Storm", //the name of your ship
	ship_gal: false, //Galawar member of the crew
	ship_ste: false, //Stede member of the crew
	ship_lys: false, //Lyssa member of the crew
	ship_sir: false, //Sirael member of the crew
	
	ship_drama: 0,
	/*
		1 - Stede complained about Lyssa
		2 - Lyssa complained about Stede
	*/
	ship_boatsex: 0,
	/*
		-2 - do not care about Lyssa flirting
		-1 - refused Lyssa
		1 - had sex with Lyssa
		2 - making Lyssa jelaous by flirting with Stede
		3 - more flirting with Stede
		4 - more sex with Lyssa
		5 - horny but nobody interested
	*/
	ship_boatsex_ste: false, //had sex with Stede
	
	ship_sirsex: 0,
	/*
		-1 - refused sex with Sireal
		1 - had sex with Sirael
	*/
	ship_sirgal: 0,
	/*
		-1 Galawar stays away from 
		1 - Galawar creeped out
		2 - revealing they slept togetehr
	*/
	
	/*
		automatically changed when are the islands discovered/explored
		0 - undiscovered; 
		1 - discovered;
		2 - explored
	*/
	ship_karg: 0, 
	/*
		0,1,2
		3 - solved
		4 - explored 2nd tomb
		5 - explroed 3rd tomb
	*/
	ship_ydyg: 0, 
	/*
		0,1,2
		3 - quest given
		4 - quest solved
	*/
	ship_dorf: 0,
	ship_hdar: 0,
	
	ship_wreck: 0, //TODO
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	












///// Intro //////////////////////////ú
	meta_boobs: 0, //breast size in metaverse
	boobs: 0, //breast size irl
	determined: false, //many hacking attempts 
	smart: 0, // -1 - resourceful; 0 - determined; 1 - intelligent
	
	hacking_solution: 0, 
	/*
		1 - suck cock
		2 - show boobs
		3 - vague promises
	*/
	
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	sav_tits: false,
	saw_pussy: false, //nerds seen her vagina
	saw_hairy: false, //nerd seen her pubic hair,
	saw_panties: false, 
	saw_sexy_panties: false,
	saw_commando: false,
	saw_tongue_piercing: false,
	saw_nipple_piercing: false,
	saw_genital_piercing: false,
	
	rumor_beaver: false, 
	rumor_shaved: false,
	/*
		3 caused by upskirt
		4 caused by fall in bar kiss
	*/
	rumor_commando: 0,  //doesn't wear panties
	rumor_plug: 0,  //wears buttplug 
	rumor_clit: 0,  //has genital piercings 
	rumor_nipples: false, //pierced nipples //TODO - probably useless 
	
	
	rumor_qaa: false, //dating qaa
	
	
	
	rumor_nudes: false,
	rumor_video: false,
	rumor_video_dildo: false,
	
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//sexy_panties: 0, //-1 failed; 1 sexy panties; 2 no panties;
	
	

	
	
		
	




	
	
	

	//STORY
	advice_2: 0, //advice how to play the game; 1 aaa; 2 bbb; 3 ccc;
	/*
		qaa - glasses
		qbb - choker
		qcc - hair 
	*/
	
	
	
	
	//STORY
	advice_1: 0, //advice how to play the game; 1 aaa; 2 bbb; 3 ccc;
	
	
	
	//RANDOM
	horny_freshmen: false, //freshman had stupid comments 
	horny_freshmen_beaver: 0,
	/*
		-1 - disabled to not conflict with others
		1 - embarrassed
		2 - walked away
		3 - answered
		4 - showed
	*/
	horny_freshmen_piercing: 0,
	/*
		1 - embarrassed
		2 - beraeted
		3 - told them
		4 - reluctantly showed them
		5 - shamelessly showed them
	*/


	pool_initiated: false, //swimming pool is introduced
	





	//RANDOM
	

	
//WEEK 3 
	



	
	plugged: 0, 
	/*
		1 - wears small buttplug right now
	*/
	vibrated: 0,
	/*
		1 - wears vibrating panties
	*/
	had_anal: "", //key who removed her anal virginity
	
	
	
	classes_late: 0,
	/*
		+ buttplug
		+ violation no pants
		+ chairman gang bj
	*/
	school_masturbation: 0,
	/*
		+ buttplug
		+ vibrating panties
	*/
	

	
	
	
	visually_follows_rules: true, //on the first sight the pc follows all rules (enables deeper checking, like checking panties)
	freepass: false, //rule-breaking is ignored (because it was already addressed)
	/*
		flash panties
		no bra
		sexy_top_reaction
	*/
	obedience: 0,
	/*
		w1
		-1, 0, 1, 2 short skirt/show legs
		-1, 0, 1, 2 selfie
		-1 flash_panties Rule Violation
		-1 sexy top initiation RV 
		-1 sexy top action RV
		-1, -2 no bra failing
		
		w2
		-1, 1, +1 sexy legwear
		
		-1 sexy panties fail / check even fail
		+1 sexy panties no panties
		
	
	*/
	clothes_warnings: 0,
	/*
		+flash_panties Rule Violation
		+no_bra
		+sexy top initiation RV
		//sexy_panties_failure - is punished immediately
	*/
	
	rv_demerit: false, //the fist level of punishment (strong warning)
	rv_lower_repo: false, //pants reposesed, 
	rv_ruler: false, //spanked with ruler
	rv_ruler_broken: false, //ruler broken while spanking
	rv_impound: false, //unsexy clothes taken away
	rv_impound_reaction: 0,//1 coooperation; 2 resistance; 3 crying
	rv_impound_rules_count: 0, //how many rules were active while impound event //TODO - SECOND IMPOUND IF MORE RULES BROKEN
	//rv_impound_2
	
	blackmail: false, //nerds admitted they had control over clouds
	/*
		violation > blackmail
		//game_night_initiation
		//rules_violation_ruler //TODO
		//rules_violation_impound
		b_tripoint
	*/
	cloud_break: false, //Kate breaks rules even after blackmail, Nerds feel entitled to break them too and check the cloud
	
//EVA & SASA 
	girls_shopping: false, //going shopping with girls 
	girls_music_video: 0, //1 approve slutty music video; -1 disaproves
	girls_piercings: 0, // >=10 sas piercings; >0 eva piercing
	
	girls_boobjob: 0, 
	/*
		-2 - eva made her feel bad
		-1 - eva made her fell bad but sas defended
		1 - calmly resisted eva
		2 - angrily argued with eva
	*/

	//SAS
	sas: 0,
	sas_exploit: 0,
	/*
		+ movie night made fun
		+ girls shopping
		rv_lower_repo
			+ stupid questions
			+ her skirt 
		+ forced haircolor /refused
		+ push into piercings
		requiem
			+ date - unsupportive
			+ kissed forcefully
			+ annoyed saved from tom
	*/
	sas_sassy: 0,
	/*
		+rv_lower_repo???
		+ girls shopping
		+ distilation embarrasement
		- no haircolor
		+ hair color
		+ requiem - date - supportive
		+ promo_mall try again
	*/
	sas_love: 0,
	/*
		+ sas_date
		+ handholding
		+ i love you
	*/
	sas_girls_shopping: 0, //shopping in virtual mall: 1 - told her to be more confident; 2 - told her to follow you lead
	
	sas_rv_explanation: 0, //1 none; 2 flimsy
	sas_rv_help: 0, //1 bring spare; 2 give skirt
	
	
	sas_boobjob: 0, //result of boobjob
	sas_boobs: 0, //total actual size (in future could be influenced by other things
	/*
		1 small
		2 medium 
		3 large
	*/
	sas_boobjob_reaction: 0,
	/*
		1 - doubled against eva
		2 - stayed out of it
		3 - tryied to calm them down
	*/	
	
	sas_benefits: 0, //video task 
	/*
		-1 - no sex 
		1 - sas's idea
		2 - not lesbian but sex 
		3 - trying to seduce 
		
	*/
	
	sas_cool: false, //TODO
	sas_makeup: false, //sas is wearing everyday makeup
	sas_commando: false, //sas not wearing panties
	sas_commando_kat: 0, //sasa is aware about kat not wearing panties
	
	sas_date: 0,
	/*
		-1 friends
		1 friends with benefits
		2 lesbians?
	*/
	sas_hand: false, //handholding
	
	sas_eva_date: 0,
	/*
		1 close door
		2 open door
	*/
	
	
	sas_dom: false, //sas is dominant
	sas_burned: false, //not talking with sasa
	sas_angry: false, //sasa is angry; passive-agreassive
	
	sas_halt: false, //relationship with sasa is halted
	sas_gf: false, //dating saša
	
	sas_eva_burned: false, //sas not talking with eva 
	


	sas_strapon: false, 
	sas_strapon_love: 0, 
	/*
		1 - love you too
		2 - eventually love
		3 - I think I love
		4 - not sure 
		5 - no
	*/
	sas_strapon_break: 0, 
	/*
		1 - doesnt want to
		2 - will eventually
		3 - blackmail
	*/ 
	sas_strapon_stop: 0, 
	/*
		1  tried to stop sas 
		2
		3	
		4 thanks for help
	*/
	sas_strapon_a_burned: 0,
	/*
		1 love
		2 reasonable
		3 hate 
	*/

	sas_blackmail: 0,
	/*
		1 - passive (knows)
		2 - tried to stop
		3 - joined 
	*/
	
	
	sas_a_threesome: 0,
	/*
		-1 kat refused
		1 submissive qaa
		2 dominant qaa
		3 dominant qaa and dominant sas
	*/


	//EVA
	eva: 0,
	eva_exploit: 0,
	/*
		+ party - tits - fat
		+ evea_movie
		+ wobbly table
		+ promo_mall scold
		
	*/
	
	eva_nice: 0,
	eva_girls_shopping: 0, //1 - look great; 2 - loose the bra; resulting in:
	
	eva_burned: false, //not talking with eva
	
	
	eva_no_bra: false,
	sas_no_bra: false,
	
	
	tits_pics: 0, 
	/*
		1 - reluctant
		2 - concern about eva
		3 - okay
	*/
	tits_pics_detail: 0, 
	/*
		-1 - refused
		1 - just tits
		2 - tits & face
		3 - whole body 
	*/
	
	tits_pics_eva: 0, 
	/*
		1 - lied to eva when confronted
		2 - was sorry
	*/
	
//AYY
	ayy: 0, //relation with AI
	/*
		+ y_meeting_1
	
	*/
	y_friend: 0,
	/*
		+ came to save her
		- meanly refused AI sexual advances
		-/+ deleted AI
		
		+/- agreed/disagreed to be friend after fight
		
	*/
	y_lover: 0,
	/*
		+/- intro got sexual with AI
		+ dominated after fight
		+ first meeting sex (y_cyberlesbo_2)
		+ oral sex
		+ strapon
		+ forced 
		+ anal
	*/
	y_sub: 0, // kate is submissive to AI (negative dominant //TODO - REDUNDANT WITH RELUCTANT? 
	/*
		- intro angry
		+ intro no but yes
		+ submissive after fight
		+ forced willing
		+/- taking/giving
		- dominstrix
	*/
	y_reluctant: 0,  //TODO NEW VARIABLE
	/*
		+ intro not but yes
		+ cyberlesbo_2 bad idea
		+ oral not sure
		+ strapon reluctant
	*/


	y_forkill: false, //told AI she's going to kill her
	y_cyberlesbo: 0, // -2 sharply refused AI sexual advanced; -1 refused; 1 agreed; 2 strongly agreed
	y_deleted: false, //deleted the AI
	
	y_unfriendly: false, //refused AI friendship
	y_cyberlesbo_2: 0, //3 agreed with sex enthusiastically; 2, 1 agreed; -1 disagreed
	y_game_1: false, //talked with AI about the game 
	y_oral: 0, //-1 refused oral sexy; 1 reluctantly agreed; 2 agreed; 3 entusiastically agreed 
	y_strapon: 0,  //-1 refused sex; 1 reluctantly agreed; 2 agreed; 3 entusiastically agreed 
	y_forced: 0, //1 no; 2 no?; 3 yes?; 4 yeah
	y_forced_reaction: false,
	
	y_anal: 0, 
			/*
				-1 - refused
				1 - yes
				2 - yeah!
				3 - fucked AI 
			*/
		y_anal_size: 0, 
			/*
				1- little
				2- medium
				3- big
				4- huge
			*/		
	
	
	y_dominatrix: 0, 
	/*
		1 was nice
		2 was evil
	*/
		//WEEK 2
		yw_vendy: 0,
		/*
			1 week 2
			2 week 3
		*/
		yw_a_love: 0,
		/*
			-1 all part of the plan
			1 don't know
			2 like him
		*/
		yw_c_perfect: 0,
		yw_b_jerk: 0,
		/*
			3 masochist
			2 slave 
			1 domina
		*/

		//WEEK 3
		yw_a_sex: 0,
		yw_b_slave: 0,
		yw_b_domina: 0,
		yw_c_avat: 0,
		yw_kiss: 0,
		yw_whore: 0,

	
	
	
//// QAA //////////
	aaa: 0,
	
	a_burned: false, //burned relationship (hate)
	a_hate: 0, 
	/*
		1 apology refused
		+ second apology refused / 1 dating refused
		+ needle
		+ family event
	*/
	
	a_dating: false, //TODO !!!!!
	a_girlfriend: false,
	/*
		date reaction sas eva
		intro family event
	*/
	
	a_love: 0,
	/*
		+ danube date
		+ rain date
		+ love you too
		+ publicly
		+ first sex
		+ tripoint
		+ cinema bj
		+/- anal
		+ forced to watch with other guy
		+ long sex
		+/- long sex uni together?
		+ family event agreed to be girlfriend
		+ family event sex
		- a_boobs
	*/
	
	a_dom: 0, 
	/*
		kate is dominant force in the relationship
		+ kiss is enough (advice_1)
		+ our dating is secret
		+ movie night, making him continue / scolding him and leaving
		+ first sex dominant
		- first sex submissive
		+ tripoint, I will teach you
		- tripoint, be more confident
		+ tripoint, open relationshp, 
		+/- cinema what to watch
		+ open about fucking other guy
		+ suggested fucking other guy
		+ and was mean while fucking the other guy
		- saved from black sheep
	*/
	
	a_cuck: 0,
	/*
		//BEWARE - don't go negative
		+ needle - pic with ex
		+ tripoint, open relationshp, previous boyriends
		+ admitting having sex with other guy
		+ forcing to watch sex with other guy
		+ farmers black
	*/
	
	a_public : 0, 
	/*
		1 - hints
		2 - suspricion
		3 - friends know
		4 - girls know
		5 - few peple know
		6 - peple know
		10 - everbody knows

		4 - sas_a_jealousy
	*/
	
	
	
	
	
	a_apology: 0, //1 accepted; -1 refused
	a_another_apology: 0, 
	/*
		1 yes, chance to date
		-1 maybe
		-2 no, thanks
		-3 no, never
	*/
	a_meeting_relationship: 0,
	
	a_movie: 0, 
	/*
		1 - bj
		2 - confront and leave
		3 - confront and continue
		4 - pretend asleep
	*/
	
	a_date: 0,
	/*
		1 - deflected
		2 - I love you too!
	*/
	a_date_girls: 0,
	/*
		X1 - escape
		2 - ashamed
		3 - excuses
		4 - too fast
		5 - date!
	*/
//TODO - A_BOYFRIEND
	a_date_react: 0,
	/*
		1 - not dating
		2 - maybe
		3 - dating
	*/
	a_first_sex: 0,
	/*
		1 - take him (charge
		2 - take me
	*/
	a_tripoint: 0,
	/*
		1 - submissive gf
		2 - dominant gf
		3 - cuck
	*/
	
	a_needle: 0,
	/*
		-1 nothing
		1 boyfriend
		2 sexy
		3 topless
	*/
	
	a_family: 0,
	/*
		-1 canceled
		1 initiated
		2 done
	*/
	a_family_slut: 0, //slut level
	a_family_high: false, //got high
	a_family_cousin: 0,
	/*
		1 love
		2 classmate
		3 whore
	*/
	a_family_cousin_whore: 0,
	/*
		1 sex for money no
		2 maybe
		3 yes
		4 yeah
		5 whore just company
		6 whore also sex
	*/
	a_family_car: 0,
	/*
		1 bj
		2 sex
		3 bj for money
		4 sex for money
	*/
	a_family_know_sheep: false,
	
	a_anal: 0, //1 yes; -1 refused
	a_forced_watch: 0,
	/*
		3 told before
		2 told after
		1 not told
		-1 did not have sex
	*/
	a_forced_watch_jerk: 0,
	/*
		2 allowed
		1 not allowed
	*/
	
	a_father: 0,
	/*
		-1 - disabled
		1 - initiated 
		2 - quit sex immediatelly after qaa's arrival
		3 - continued sex even after caught
	*/
	a_father_daddy: false, //true - calls qaa's father daddy
	
	a_shopping: 0, 
	/*
		1 - reserved nonsexual reward
		2 - bj
		3 - sex
	*/

	a_threesome: 0,
	/*
		-1 refused 
		1 kat in control
		2 okay 
		3 pushed by AI
		4 very pushed by AI
		11 a bit dominant
		12 dominant
	*/

	a_strapon: 0,
	/*
		0 - nice
		1 - challenging
		2 - bossy
		3 - dominant
	*/
	a_strapon_gentle: false,  

///// QBB //////////////
	bbb: 0,
	b_dom: 0,
	/*
		+ not let anybody mistreat me
		+ ayy weekend 2
		+ advice 3 slap
		+ advice 3 show him whos in charge
		+ tripoint refuse
		-- tripoint slave
		++ tripoint domina
		+ oral service
		+ playing dominatrix
	*/
	b_vic: 0,
	/*
		+ willingly no mistreatment
		+ ayy weekend 2
		(+ advice 3 bj)
		(++/-- tripoind slave/domina)
		+ ayy weekend 3
		+ groceries collar reluctant
		+ bitch fucked rebelious/reluctant
		+ hates shopping
	*/
	b_mas: 0,
	/*
		+ maybe mistreatement
		+ ayy weekend 2
		(+ advice 3 bj)
		(++/-- tripoind slave/domina)
		+ ayy weekend 3
		+ groceries collar eager 
		+ bitch fucked eager/teasing
		+ likes shopping
	*/
	b_cruel: 0,
	/*
		+/- service made him
		+ ayy weekend 3
		+/- b_dominatrix make him cum/not good enough
		+/- b_punishment
	*/
	b_slave: false, //slave path (tripoint)
	b_domina: false, //mistress path (tripoint)
	
	b_meeting_1: 0, //1 dom; 2 vic; 3 mas;
	b_meeting_2: 0, //tied; 1 threatens; 2 begs
	b_tripoint: 0, 
	/*
		1 - refused 
		2 - called bluff
		3 - refused rudely
		4 - attacked him
		5 - became a slave
	*/
	b_bitch: 0,
	/*
		//-2 - protests
		-1 - defiant
		1 - begs
		2 - teases
	*/
	b_groceries: 0, 
	/*
		1 - reluctantly
		2 - resignation
		3 - eagerly
	*/
	b_groceries_2: 0, 
	/*
		-1 - yes to sucking stoners
		1 - quiet
		2 - no
		3 - NO!
	*/
	
	
	b_service: 0,  //1 - convinced; 2 - made him
	
	b_dominatrix: 0, //-1 berate; 1 rewarded; 2 rewarded with bj
	b_dominatrix_f: false, //feet worship
	b_gear: 0,
	/*
		"leatherMinidress"
		"latexCatsuit"
		"leatherCorset"
		"latexMinidress"
	*/
	
	b_choker: false, //kate made him wear choker (shopping_dom)
	b_locked: false, //atm wearing chastity cage
	
	b_shopping_sub: 0,
	/*
		1 - hate
		2 - dislike
		3 - neutral
		4 - neutral sexy
		5 - likes
		6 - loves
	
	*/
	
	b_shopping_dom: 0, 
	/*
		1 - reserved nonsexual reward
		2 - cuniling
		3 - bj
	*/
	
	b_chastity: 0,

	b_threesome_dom: false,
	b_threesome_sub: 0, 
	/*
		1 - nice
		2 - mean
	*/
		
	chastity_cages: 0, //nubmer of chastity cages in the inventory 
	
	//QCC
	ccc: 0, //total interactions
	/*
		+ c_advice_1 (pink)
		- c_advice_1 fai
		+ c_meeting_1 (interest x boobs)
		+ c_meeting_talk
		+ c_meeting_bigtits
		+ c_advice_2 (pink hair)
		+ c_advice_3 (panties)
		+ c_avatar
		+ c_boobjob
		+ c_clinic_boobjob
	*/
	c_friend: 0, //friendship
	/*
		+ 1 c_meeting_first interest
		+ 1 c_meeting_first interest high immersion
	*/
	c_bimbo: 0, //bimbofication
	/*
		+ c_meeting_first boobs
		+ c_meeting_talk discussing body
		+ c_meeting_bigtits
		+ c_advice_2 pink hair
		+ sexy pantyhose or stockings
	*/
	c_tg: 0, //tf transformation
	/*
		+ c_meeting_talk discussing female avatars
		+ forced to put on panties (advice 3)
	*/
	
	

	//advice 1 (pink item)
	c_advice_1_item: "", //what pink item she wore (slot)
	c_advice_1_under: false, //whether it was underwear
	
	//meeting 1
	c_interest: false,  //solve animosity by pretending interest or b
	c_grope: false, //solve animosity by  boobs
	
	c_meeting_talk: 0, /*
		1 - friend direction
		2 - tg direction
		3 - bimbo direction agree
		4 - bimbo direction disagree
	*/
	
	c_meeting_bigtits: 0, //triked into breast expansion (1 2 3)
	c_meta_boobs: 0, //breast in metaverse expanded by qcc (1, 2, 3) 
	
	//advice 3 (panties)
	c_panties_worn: false, //panties whe was waering
	c_panties_on: false, //made him wear them
	
	
	//avatar alteration
	c_avat_waist: 0,
	c_avat_butt: 0,
	c_avat_hair: 0,
	c_avat_tits: 0,
	c_avat: 0,
	/*
		1 - almost nothing
		2 - medium
		3 - almost everything
	*/			
				
	c_boobjob: 0, //suggested boobjob: 3 yes; 2 maybe; 1 no
	
	c_shopping: 0,
	/*
		1 - thank you
		2 - kiss
		3 - bj
	*/
	
	
	clinic_piercings: false, //true - doctor saw her nipple piercings
	clinic_boobjob: 0,
	/*
		-1 - think about it
		1 - small
		2 - medium
		3 - big
	*/
	clinic_minor: 0,
	/*
		-1 refused
		1 lips
		2 belly
		3 ass
		TODO 
	*/
	
	
//// GRADES /////////////////////////////////////////////////////////
	grades: 3,
	/*
		1/2/4 based on hacking minigame (intro)
		+2/+1/-1/-2 biology study + test
		+2/+1/-1/-2  history test (flash breasts task)
		+2/+1/-1/-2 study chemistry (aaa love)
		-4/-3/-2/0/+2 - German classes (vibrating panties task)
		//+ late to class (rules violation)
	*/
	bulgaria: false,
	study_biology: 0, 
	/*
		1 studied
		-1 friend
		-2 masturbation
	*/
	biology: 0, 
	/*
		-2 worst
		-1 bad
		1 good
		2 best 
	*/
	
	distilation_nerd: 0,
	/*
		1 - qaa
		2 - qbb
		3 - qcc
	*/
	distilation_end: 0,
	/*
		1 - embarrassed
		2 - confident
		3 - shameless
	*/
	
	
	improper_clothes: false, //was called for wearing whorish clothes
	improper_clothes_sub: 0,
	/*
		1 - sub
		2 - neutral
		3 - dominant
	*/
	improper_clothes_slut: 0,
	/*
		1 - prude
		2 - neutral
		3 - slut
	*/
	improper_clothes_show: false, //true - showed them pussy/panties
	improper_clothes_no_panties: false, //true - they know she doesn't wear panties
	
	
	
	
//// CHAIRMAN ////////////////////

	vendy_makeup: 0, 
	/*
		-2 offended her; 
		-1 refused; 
		1 agreed to help her with makeup; 
		2 helped her
	*/
	wobbly_table: 0, 
	/*
		1 solved in eva's favor; 2 solved in qbb favor
	*/
	vendy_running: 0,
	/*
		1 vendy is running for chariman
		2 and kate made a scene
	*/
	chair_tom_vote: false, //tom votes or ven
	chair_manager: 0, //qaa vendy's campaing manager; 1 polite; 2 rude;
	chair_qbb_vote: false, //qbb votes for ven
	eva_running: 0,
	tom_help: 0,
	/*
		-1 - refused the deal
		1 - reconsider sex
		2 - date
		3 - blowjob
		4 - sex
		5 - kinky sex
		6 - anal
	*/
	tom_date: 0,
	/*
		-1 - refused to honor the deal
		1 - available
		2 - done - TODO
	*/
	
	chair_blowjob: 0,
	/*
		blowjob for votes
		-1 - refused
		1 - agreed
		2 - agreed, vulnerable to gossips
	*/
	chair_orgy: 0,
	/*
		-2 - refused rudely
		-1 - refused 
		0 - not initiated
		1 - strongly against recording
		2 - agaisnt
		3 - weekly
		4 - loves cocks
	*/
	
	chair_orgy_2: 0,
	/*
		-2 - refused rudely
		-1 - refused 
		0 - not initiated
		1 - reluctantly 
		2 - not a whore
		3 - agreed
		4 - extra money
	*/
	
	chair_orgy_sex: 0, 
	/*
		-2
		-1 - no
		1 - yes
		2 - yeah
	*/
	
	chair_orgy_cheap: 0,
	/*
		-1 bj
		1 sex
		2 nympho
	*/
	
	chair_elections_quit: false,
	/*
		-1 - give up
		1 - nver give up
	*/
	
	chair_eva_promise: 0,
	/*
		1 - fair
		2 - angry
		3 - trickery
	*/
	chair_eva_end: 0,
	/*
		1 - peace
		2 - hidden conflict
		3 - open warwar
	*/
	
	chair_support: 0,
	/*
		1 - neutral
		2 - eva
		3 - vendy
	*/
	
	chair_vote: 0,
	/*
		1 - blank
		2 - eva
		3 - vendy
	*/
	
	chair_elected: 0,
	/*
		1 - reelected
		2 - eva
		3 - vendy
	*/
	
	chair_a_help: 0,
	/*
		1 - qaa promised help
		11  - threesome sas yeah
		12 - thresome sas yes
		13 - thresome sas no
	*/
	
	chair_c_help: 0,
	/*
		10 - vague primise
		3 - blowjob
		4 - sex
		5 - kinky sex
		6 - anal
		20 - does not want anything in return
	*/
	
	chair_b_help: 0,
	/*
		-1 refused
		1 reluctantly agreed to slander eva
		2 keenly agreed 
		3 initialy disagrede but changed mind after tricked by girls 
	*/
	

	
	
	
	
	
	locker_nerds: false, //peter party ai
	locker_tom: false, //zanet sex with tom
	locker_eva: false, //ogled eve's rumb
	locker_qaa: false, //dating with qaa
	locker_nipples: false, //nipple piercins
	locker_tan: false, //tan
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	tom_zan: 0,
	/*
		1 - dating together
		4 - break up 
		6 - back together
		todo 8 - threesome 
	*/
	
	mol: 0,
	/*
		1 vibrating panties
		2 mol class 
		3 mol grades
	*/
	mol_perv: 0,
	/*
		-2 - threatened
		-1 - refused firmly
		1 - refused feebly
		2 - submited
		3 - made a deal
		4 - seduced
	*/
	mol_cun: false, //first encounter cunilingus
	
	
	into_feet: 0,
	into_breeding: 0,
	into_rimming: 0,
	/*
		+ y_anal
	*/
	
	
	bimbo_mods: 0, // Kate likes or is hesitant about body modifications
	/*
		++/+/- qcc tricked breast expansion
		+/- qcc pink hair
		+/- qcc avatar improvemets (excited about it)
		+/- qcc avatar improvements (actual improvements)
		- qcc avatar improvements (back to original tits)
		+/- qcc boobjob
		- clinic boobjob reluctant
		+ clinic boobjob big
	*/

	mistress: "Mistress", //string; BEWARE - there are comparison assuming "Mistress" is default value, so you have to change them if you change the default value
	dominatrix: 0,
	/*
		+ qbb dominatrix
	*/
	
	pocket_money: 0, //-1 reduced; 0 default; 1 increased;
	
	
	
	
	video_dildo: 0,
	/*
		nerds demanded:
		1 softcore
		2 something between
		3 hardcore
	*/
	video_dildo_action: 0,
	/*
		1 softcore
		3 hardcore
	*/
	
	
	video_sas_task: 0, 
	/*
		1 want topless pictures
		2 want video
	*/
	
	video_sas_secret: false, //used hidden camera
	video_sas_open: false, //used nonhidden camera
	
	video_sas_result: 0,
	/*
		-1 another chance 
		1 exposed
		2 exposed despite protest
		3 exposed willingly
	*/
	
	
	
	shop_mall: 0, //times shopped in mall
	shop_lingerie: false, //lingerie shop unlocked
	shop_alt: false, 
	shop_bimbo: false, 
	shop_gold: false,
	shop_haircut: false,
	
	hostess: 0,
	/*
		- refused mall promo
		+ won mall promo
		- crazy hair
		+/- knew the product at tech fair
		+ bigger breast 
		+ body shot
		- fucked black
	*/
	hostess_hair: false,
	
	promo_mall: 0,
	/*
		-1 refused
		10 eva lost
		11 eva win
		20 sas lost
		21 sas win
	*/
	
	farm_eric: 0,
	/*
		1 embarrased
		2 sorry
		3 hi
		4 hot body
	*/
	farm_eric_2: 0,
	/*
		1 waited
		2 stripped slowly
		3 stripped fast 
	*/
	farm_black: 0,
	/*
		1 just selfie 
		2 checked cock
		3 sucked
		4 fucked
	*/
	farm_cuck: false, //pic send to qaa
	
	
	
	strapon_1: false, //strapon
	strapon_2: false, //big
	strapon_3: false, //massive
	strapon: 0, //biggest strapon
	
	
	
	tatttoo: "", //tattoo id (basically slot)
	tattoo_2: "",//tattoo id
	tattoo_2_task: 0,
	/*
	1 any
	2 slutty
	*/
	tattoo_2_slutty: false,
	girls_slutty_tattoo: 0,
	/*
		1 cool
		2 meek
		3 wanton
	*/
	
	no_panties: 0,
	/*
		1 delay  //TODO
		2 stripped secretly 
		3 stripped publicly
		4 already none
		5 already none visibly 
	
	*/
	


	poledancing_name: "",
	poledancing_boobs: false, //sve mentioned kats fake boobs
	poledancing_gf: false, //joke about sve asking kate out


	stripshow: 0,
	/*
		-1 no
		1 yes but no sex
		2 yes for money
		3 oh yeah!
	*/

	stripshow_action: 0, 
	/*
		1 gentle
		2 hard
		3 eat out
	*/

	laura_name: 0,
	/*
		bratty
		no
		kat
	*/

	qcc_extend_hair: false,
	dye_blond: 0,
	/*
		-1 not dyed
		numbers shades
	*/
	
	
	girls_tricked_sas: 0,
	/*	
		1 kat is prude
		3 kat is slut
	*/
	
	
	maj_exploit: 0, 
	maj_paper: 0, 
	/*
		1 apologized
		2 blamed eva
	*/
};
 