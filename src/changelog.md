# Build 0.9.54

- more events related to Eva and Saša
- Kate can win the election (in theory, not balanced very well), several events related to the elections
- in avatar editing included all uncurated Dynamic Avatar Drawer dimensions
- added simple Walkthrough with list of possible story arcs (Patreon version, accessible from the *Main Menu*)


# Build 0.9.53

- three threesomes (first nerd; second nerd dominant, second nerd submissive)
- relationship with Saša getting serious, (if you dating her and the first nerd, you might tell her about the blackmail)
- femdom scene with the first nerd
- strap-ons (bought in the sex shops)
- new events marked by [new] in the name

# Version 0.9.50

- fifth week (work in progress)
- class elections (first draft)
- slutty tattoos
- femdom events with the second nerd
- readjusted sluttiness (max sluttiness raised; add new sluttiness level; sluttiness requirement in shops lowered during the fifth week, (to make sexier clothes available even to prudes))
- fifth week unlocks getting any piercings and tattoos during shopping (if both tasks Piercings 1 and Piercings 2 respectively Tattoo 1 and Tattoo 2 are finished) (with enabled cheatmode tattoos could be removed)
- many minor improvements

# Build 0.9.50

- locked work-in-progress content
- experimental notification disabled

# Build 0.9.47

- **continuation of stripper arc**
- **slutty tattoos**
- readjusted sluttinees (max sluttiness raised; add new sluttiness level; sluttiness requirement in shops lowered during the fifth week, (to make sexier clothes available even to prudes))
- readjusted random clothes; slightly improved performance
- Kate cannot leave the house if she is flashing breasts, panties or pussy and her sluttiness is too low
- **pantyhose or panties reaching above the waistband of skirt could be disabled** (tugged down) in the morning setting
- **fifth week unlocks getting any piercings and tattoos** during shopping (if both tasks Piercings 1 and Piercings 2 respectively Tattoo 1 and Tattoo 2 are finished) (with enabled cheatmode tattoos could be removed)
- **random event blonde hair, during shopping hair could be extend** (or dyed with enabled cheatmode)
- several experimental notifications could be stacked together
- ability to clear the schedule (in the side menu) to manually remove hopelessly bugged events
- option to disable avatar zoom (in the main setting) - could be annoying and some people reported bugs I was not able to reproduce

# Build 0.9.46
- fixed reported bugs and typos
- experiment with notification about variable changes sliding down from the top of the screen; it at the moment works only with several selected variables and cannot handle multiple variables changing in one scene, showing only the last one
- I probably will not include it in the game because there are too many variables and their significance varies; could be disabled in *Settings*
- new scene *chairman gangbang 2*

# Version 0.9.456

- several new events (date with Saša, meeting Juraj's father, desperate measures to get more votes, etc.)
- fourth weekend unlocked (still work in progress)
- the ability to clear the schedule (in the Side Menu) to manually remove hopelessly bugged events 
- option to disable avatar zoom (in the Main setting) - could be annoying and some people reported bugs I was not able to reproduce 
- countless typos and minor bugs fixed 

# Version 0.9.453

- fixed many reported bugs and typos

# Build 0.9.45
- removed all 5th week events to trick people into subscribing to my Patreon
- changed name of Eva's boyfriend to Samuel

# Build 0.9.44

- farmer's market job
- improvements to the balance of the game, boosted the probability of tasks blocking content (e.g. all anal - scenes happen after buttplug task), lowered sluttiness requirement for actions that are shamelessly
- slutty but disable cool future scenes
- minor improvements to inventory (default view categories, traded items highlighted, hovering on money - change shows the list of traded items)
- fixed stuck death in combat; combat made slightly easier but I will have to completely rework it anyway
- fixed shops having the same id making a mess in the bank account
- many fixed bugs, typos and broken paths

# Build 0.9.43

- fixed loading of the game and the assets
- broken textures should not break the game
- table of events integrated into the game (*##List of Events* in *Main Menu* when *Hard Cheatmode* is on)
- all sex scenes should be logged to sex stats (*Stats* in *Side Menu*)
- new job arc (working as a hostess/promo girl), two scenes; two scenes with submissive second nerd
- *karaoke* moved from random to optional event; scheduling system allows to go back to previous even (i.e. returning to *afternoon* hub when saying no to *karaoke* offer)
- broken events (*shopping with girls, Sašas boobjob*) fixed
- custom tops ordered online now recognised as valid sexy tops
- hair very slowly grow and could be cut in Hair Salon in the Mall

# Build 0.9.42

- scenes of shopping with nerds (to get clothes for free) + two scenes with Saša
- minor improvements to shops 
- many bugs fixed 
- lame task with collar and related stuff (in morning *Custom setting* could be selected preference for *chokers* and/or *collars* etc.)
- sex stats (in *Stats*) (WIP, not all sex scenes counted atm)

# Build 0.9.41

- many bugs, typos and broken paths fixed
- minor improvements to the Account; moved to side menu because there is an empty space and there are already useless things like *Stats* anyway
- in Morning menu slutiness level visible and saving of outfit available without opening the inventory; more options for Custom random

# Build 0.9.40

- money in real life (= not the virtual game) (account could be checked: Character > Account)
- instead E-shop several shops downtown
- shopping online allows to order custom clothes (wip)
- reworked clothes assets (to be compatible with the newest Dynamic Avatar Drawer), fixed many minor issues, several new clothes (there might be issues with old saved clothes)
- table inventory (Settings > Inventory; might require reload)
- few more scenes; class chairman arc finished (atm the elections might have to be triggered with cheats on), few Saša related events, few tasks

# Version 0.9.34

- fixed major issues with Safari and older browsers + few minor bugs fixed (same as 0.9.30)

# Version 0.9.33

- even more bugs and typos fixed (same as 0.9.29)
- several makeups could be saved, one of them gets randomly picked with clothes (in the makeup window)
- outfits work better; outfit manager (in the inventory)
- random clothes include belts; belts are rendered above shirt (asset of belts rendered  below shirts was used but I do not remember why, hope nothing gets broken)

# Version 0.9.32

- few more new scenes, piercings 2 (freshmen & girls reactions), tattoos, seduce Juraj's father, more reactions to breast enlargement, Eva's boyfriend (retconed to be name dropped in old events, texting with him)
- animal print underwear


# Version 0.9.30

- fixed major issues with Safari and older browsers + few minor bugs fixed (same as 0.9.34)

# Version 0.9.29

- even more bugs and typos fixed

# Version 0.9.28

- many bugs fixed
- few more things foolproofed because some people keep playing the game wrong

# Version 0.9.27

- edited avatar properties are actually loaded when is the game reloaded
- several minor bugs fixed
- little toggling with clothes randomization

# Version 0.9.26

- **whole game reworked** (twice)
- **everything is better**
- **new modular non-linear structure**
- **like 20 tasks, 55 events and redone virtual game** (three weeks of gameplay, not all accessible during every gameplay)


# Build 0.9.25

- **several new scenes with the AI**, plus like two other scenes
- **facial expressions**
- playtested, many bugs removed, minor improvements and tweaks all around, 
- slightly reworked saving of avatars
- minor css tweaks


# Build 0.9.24

- playtested several times, countless bugs removed (including serious ones like variables not being completely cleared when savegame loaded), several broken scenes fixed
- a few new/reworked events (kissing girls reworked into a weekend task, threesome with Tom and Žanet (atm accessible only through debug menu), more events with the third nerd
- scheduling of events reworked, most (hopefully all) conflicts removed; weekend tasks could be triggered the same way as regular tasks
- the virtual game parts are skippable (in Settings); virtual game progress is better adjusted to the overall game progress (to not stay too behind or get too ahead)
- a consistent way to display unavailable choices a) as unavailable greyed-out in soft cheatmode b) clickable in hard cheat mode (some were already displayed in the previous versions but those were ad hoc solutions)
- time and place are displayed even in the virtual game and are used as the default name of saves
- NPC's could be rerolled or edited (in People)

# Build 0.9.23

- time and place displayed at the begging of the scene to make the scene changes less jarring and confusing
- list of people 
- several school-related events
- tasks open legs, vibrating panties and tan
- random locker-room discussion (atm not really working because there are only like two scenes in swimming pool locker rooms)
- better handled grades (available in character screen)
- minor fixes to potions and npc looting
- minigames accessible in the main menu
- many bugs fixed 

# Build 0.9.22

- fixed combat
- fixed autosaving & import of saves
- fixed few typos
- added a striptease scene

# Build 0.9.21

- more contend related to the first nerd
- fixed and reorganised potions, simple potion tester (Main menu/Minigames) is back
- fixed gui related bugs
- fixed clothes related bugs
- fixed names related bugs
- fixed inventory related bugs

# Build 0.9.20

- whole GUI reworked with Svelte framework 
- **new combat system**
- **new system handling skills, attributes, effects, perks and artifacts**
- randomization of clothes reworked
- less cluttered morning menu
- several new random evens regarding school classes and elections
- code refactored
- countless bugs fixed

# Build 0.9.11

- several new, expanded or old events related to nerds 
- fixed most of the issues with displaying avatars of NPCs
- tweaked calculation of sluttiness level & significantly improved various checks of clothes
- randomization of clothes reworked
- pictures of the PC taken every week, currently available in ##Stats

# Build 0.9.10

- **preview of reworked first two weeks**
- **new dynamic system, order of tasks and events is not set but randomly assigned**
- **new dungeon system & followers**

# Build 0.9.09
- reorganized textures & fixed fishnet textures
- messing with scheduling of tasks and to which day they are assigned
- fixed the seed of AI avatar not being properly saved
- real-life character screen
- sluttiness, submissiveness and status rebalanced 
- a couple of scenes, discussion with the second nerd about knots, hair dying (task or favor for third nerd)
- added a simple tile-sliding minigame to Oskyl Labyrinth
- many bugs fixed 

# Build 0.9.08
- post on Patreon titled *Build 0.9.08* is actually 0.9.07 because I cannot count, skipping it will create less chaos

# Build 0.9.07
- **more Gothred content**
- there was a ship-to-ship but I had to remove it because it was really bad
- **cheatmode divided into three** (available in *Settings*):
- *soft* will display canonically available options which are hidden beyond random chance or extra work
- *hard* will display options requiring different stats or different previous choices (potentially game-breaking)
- *debug* just for me, displaying extra error messages 
- saving in dungeons, death
- discussing things with followers in the dungeon will disable the movement for 600ms to prevent missing them because of furious clicking
- pubic hair
- reworked keyboard controls
- avatar dimensions adjustable in Settings, textures scaled to the new size
- minor toggling with css style, save menu nicer
- issues with dimensions and outfits of npc avatars not being saved probably solved
- fixed selection of clothes in shops based on general sluttiness level
- **fixed second nerd content, added a few minor tasks and one scene with AI**
- countless bugs fixed

# Build 0.9.06
- significantly improved dungeons and followers
- more clothes to loot/buy, a few minor issues with assets fixed, prices moved in an easily editable separate file
- Gothred, a few dungeons, Lyssa, Stede, your own ship
- quests (active listed in Character)
- plugged in whoring from I Was Enslaved by an Evil Witch and Turned into her Maid, it is supposed to be more easter egg for players with high sluttiness than actual part of the game
- pressing ctr disables keyboard (to allow ctr + c)
- ###Stats allows to change <i>sluttiness</i> and <i>submisiveness</i> (wip)

# Build 0.9.05
- fixed several game-breaking bugs

# Build 0.9.04
- nerds enforce their rules more strictly
- first few scenes of the background school president (atm default) (makeup > random chat with Vendy > random even with Vendy > complaining to a nerd about Vendy)
- game night Friday event & new minigame & simple pizza recipe
- few minor task and events
- fancier dices, experiment with gradient background 

# Build 0.9.03
- <strong>old saves does not work</strong>
- reworked followers and the player object so they now work the same way and reuse the same functions 
- follower's equipment could be changed, he or she can drink potions too
- many issues related to potions fixed, drinking a potion disables canceling of the inventory menu
- perks
- fixed several minor bugs 
- <i>Go to school</i> & <i>Procrastinate</i> choices move to top (it looks uglier but the idea is that the top choice should be the most default generic one and you can speedrun by pressing 1 or space (but space does not work yet)(honestly that is the only reason why added keyboard control)(in some situations, it other the first choice just means strongly agree or other things))
- choices in the text does not have to be unclicked to be clickable again & they could be randomised (I got tired of clicking on them in <i>Skip intro.</i>)
- clicking on morning clothing issue will instantly (randomly) solve it
- added <i>Force return to event start</i>, all force buttons are potentially game-breaking because they ignore all variables, just move at a specific place
- added <i>Voyage to Gothred</i> quest 
- added weekend events with AI and the third nerd, the other ones are almost finished too
- sexy legwear task & follow up check-up events to the sexy panties task, which are possibly impossible to trigger atm

# Build 0.9.01
- followers (work in progress)
- Yelaya & her quest (Woman in Feyd Mora)
- clicking on the "Health" or "Energy" bar label will automatically drink a potion
- potions (work in progress)
- improved tester accessible Menu > Minigames > Potions

# Build 0.9.0
- code reworked from scratch
- webpack compatible
- names of NPC's could be selected randomly
- avatars of NPC's, work in progress, currently displayed when hovering mouse over their names
- keyboard control
- changelog and credits are in markdown format but loaded into bundle as html via markdown loader I made myself and I am very proud on it
- few new easy tasks, the first week slowed down and blackmail aspect softened a bit
- prototype of new dungeons system 
- wrestling changed into a minigame, it is horrible and I will have to fix it
- skip intro option
- reworked dynamic game loop, task could be picked when in cheatmode

# Version 0.8.81
- Fixed few minor bugs
	
# Version 0.8.80
- Friday/Saturday content
- new dungeon - abandoned temple Hyn Filnach
- on Thursday the player can now decide for either video (i.e accessible even without failing task with Saša) or gloryhole (the choice appears if the nerds are nice and Kate is at least mildly slutty)
- on Friday she can willingly agree her video should be public
- added pauldrons, finally fixed gorgets, shoes, messing with merchants
- added potion returning the character back to the original human form
- I did not notice the orientation of the chainmail was wrong but now it is fixed
- fixed doubling of saves, added easier access to the latest manual save and quicksave (saves game to the latest slot without further questions and shuts down the save menu)
- dungeons made a bit easier, or at least I think so
- fixed code which should deliver extra healing potions to the player playing on the lowest difficulty
- fixed several bugs with potions, attachement/removal of parts is done a different way
- code handling dungeons partially refactored
- many desperate attempts to fix my atrocious code <small>(e.g. tag masochist is so fucked up because 1) it is too easy to miss it, 2) I get constantly confused whether it means she like the second nerd and BDSM or whether it means she still hates it but getting hurt a little arouses her, 3) I thought it was boolean but it was integer so all the content was accessible anyway)</small>
	
# Version 0.8.74
- few minor bugs and typos fixed 
	
# Version 0.8.73
- Wednesday & Thursday & first part of Friday
- updated version of DAD
- piercings reworked 
- vain attempt to make the inventory menu less clumsy
	
# Build 0.8.73
- new version of DAD, a lot of clipping isues fixed, few minor additions (piercings are noticeable under tops, belt with bukles, etc.)
- even more piercings
- the game <i>mostly</i> works even with the local storage disabled 
- default color of links is now dark blue, some minor adjustments of css 
	
# Build 0.8.71
- fixed several bugs with piercings
- fixed severa other minor bugs
	
# Build 0.8.70
- Wednesday & Thursday & first part of Friday
- attempt to rework piercings (wip)
- left menu buttons Focus and Transparent removed because they were useless and only took space, they are now in the inventory menu
- vain attempt to make the inventory menu less clumsy
- cheatmode could be disabled/enabled in Settings
- fixed importing saves from the file

# Version 0.8.55
- I hope I finally fixed the bugs when the clothes got cloned in the inventory
- fixed 4th weekend discussion about larger breasts getting stuck if you already have ones
- fixed 3rd weekend discussion with AI, there was no link to continue if there was nothing to discuss
- fixed several minor bugs causing referencing things which did not happen or happened differently

# Version 0.8.54
- fixed bugged difficulty (default difficulty was not initiated properly and broke the minigame with crystals and possiblys many other things)
- on the low difficulty it is possible to skip crystals in the Wizard's tower even in the non-Patreon version 

# Version 0.8.53
- fixed bugged buying clothes

# Version 0.8.52
- Friday, weekend, Monday 
- nerds more seriously enforce their fashion demands (on the second Thursday they can take away Kate's clothes if she breaks their rules several times)
- retcons: deleted AI is fully restored; during the discussion about buttplug the player can choose Kate's level of anal experience (no longer automatically assigned based on sluttiness); when discussing her previous boyfriends with the first nerd she at least gives him a handjob
- bars displaying sluttiness and submissiveness
- it could be clicked on looted items to immediately use them
- if you know the potion, you can pick the effect (either by drinking enough of them or by learning the knowledge at alchemy shop)
- new tattoo system
- character avatars are exportable/importable, <i>should</i> be compatible with I Was Enslaved by an Evil Witch and Turned into Her Maid and Who Wanna Be a Bimbo
- all saves deletable
- more available clothes (pantyhose, leggings, long shirts, more belts)
- inventory is default sorted when accessed, adjustments of the calculation of the slut factor of the outfit
- dice games include images of dices
- reworked variables, some new added, some redundant removed, it should not cause any visible changes
- many bugs and broken paths fixed
- TODO: Taurn Morna, something with minigames, reworking percing, gaming during the fourth weekend

# Build 0.8.65
- I hope I finally fixed the bugs when the clothes got cloned in the inventory
- fixed 4th weekend discussion about larger breasts getting stuck if you already have ones
- fixed 3rd weekend discussion with AI, there was no link to continue if there was nothing to discuss
- fixed several minor bugs causing referencing things which did not happen or happened differently

# Build 0.8.64
- fixed bugged difficulty (default difficulty was not initiated properly and broke the minigame with crystals and possiblys many other things)
- on the low difficulty it is possible to skip crystals in the Wizard's tower even in the non-Patreon version 
	
# Build 0.8.63
- fixed bugged buying clothes

# Build 0.8.62
- fixed broken clothes randomisation
- fixed few typos

# Build 0.8.60
- Tuesday - alternative tasks (sluts are tasked with less sex, prudes with more)
- Wednesday - consequences of the tasks
- tattoos improved a little (icons, few more degrading ones, custom tattoos could be added if desired (see /MODS/README.txt))
- the color of hair could be changed on Friday
- ##Fourth weekend - testing tool ##Adjust relationships
- Retcon - if you decided to delete AI, she is fully restored from backup because she is needed later in the story.
- empty spaces between . and " trimmed
- many bugs and broken paths fixed

# Build 0.8.51
- added cuckolding scene during the weekend (Go out with Juraj)
- Monday - added new bigger butplug
- tattoos reworked
- Tuesday, new task with augmented Saša

# Build 0.8.50
- weekend events
- new tattoo system (wip)
- nerds more seriously enforce their fashion demands (on the second Thursday they can take away her clothes if she breaks their rules several times)
- retcons - during the discussion about buttplug the player can choose her level of anal experience (no longer automatically assigned based on sluttiness), when discussing her previous boyfriends with the first nerd she at least gives him a handjob, 
- rebalanced stats (e.g sluttiness factor to be allowed to go to school without panties was raised, nightie should be gradually getting sluttier, etc.)
- reworked variables, some new added, some redundant removed, it should not cause any visible changes
- bars displaying sluttiness and submissiveness (wip, I am not sure whether displaying them is a good idea or not)
- it could be clicked on looted items to immediately use them
- if you know the potion, you can pick the effect (either by drinking enough of them or by learning the knowledge at alchemy shop)
- character avatars are exportable/importable, <i>should</i> be compatible with I Was Enslaved by an Evil Witch and Turned into Her Maid and Who Wanna Be a Bimbo
- all saves deletable
- more available clothes (pantyhose, leggings, long shirts, more belts)
- inventory is default sorted when accessed, adjustments of the calculation of the slut factor of the outfit
- character creation more orderly
- dice games include images of dices
- I wanted to write a walkthrough but it did not go well, an uncompleted cryptic torso (not) explaining stats could be found in ##Walkthrough > Sluttyness or ##Walkthrough > Submissiveness

# Version 0.8.421
- Ynis Farion and Taurn Morna added to the third weekend 
- Thursday
- names of NPCs could be changed in Settings
- inventory improved - scrollable, reverse search
- far more names of colours in the inventory
- exporting saves to file (experimental work in the progress)

# Build 0.8.42
- far more names of colours in the inventory
- Thursday
- added missing scene with dominant creepy nerd (Monday or Tuesday)
- Taurn Morna dungeon (accessible from Tristan)
- new attack for Harlot - Dance

# Build 0.8.41
- Ynis Farion and Tristan (Third Weekend > Game)
- names of NPCs could be changed in Settings
- exporting saves to file (wip)
- hellion tattoos
- inventory improvements - scrollable, reverse search
	<lik>Get Dressed gets disabled after your start messing with clothes, it was easier than fixing it
- outfits, makeup cleared after starting a new game
- fixed tais, hoofs, wings disappearing after reload
- fixed the first advice (when checking for pink hair, value was assigned not compared)

# Version 0.8.39
- Weekend + Monday + Tuesday
- Get Dressed gets disabled after your start messing with clothes, it was easier than fixing it
- outfits, makeup cleared after starting a new game
- fixed tais, hoofs, wings disappearing after reload
- fixed the first advice (when checking for pink hair, value was assigned not compared)	

# Build 0.8.39
- horrible chimera combining new content from 0.8.32 (Weekend + Monday + Tuesday), game improvements from 0.8.356 and essential bugfixes from 0.8.41

# Version 0.8.356/Build 0.8.40
- hint system 
- in the field for searching in the inventory added text <i>🔎 Seach</i> to make its purpose less enigmatic
- puzzle game better implemented, skippable, I was not able to find exactly the bug which was causing it stuck which make me pretty anxious but I reworked all the things which might cause it
- <i>Valsyan Cavern</i> is skippable
- when opened from the <i>Main menu</i>, <i>Back</i> in <i>Saves</i> returns to the Menu
- fixed hair colour at the beginning 
- <i>Random Outfit</i> (renamed to <i>Random Clothes</i>) will not unequip locked items (ie vibrating panties)
- the correct combination is displayed after failing the hacking minigame, I hope it will help people to feel bad about their failures
- on hover explained how <i>Teasing</i> work
- lowered difficulty (tweaked some numbers, ensured player playing on normal or lower difficulty acquires second level weapon in the first (rat) dungeon, beast (cavern) dexterity check lower, spiders are no longer poison-resistant, check to pretend to join the cult lowered, sorcerers healing abilities limited)
- fixed bug which caused horrible deadly loop because accessing inventory restart the randomisation seed, shield or dodging ifreets/maarids spells did not work because it did not restarted his combat phrase

# Version 0.8.353/0.8.363 
- fixed many typos
- fixed some bugs (pink hair not consistently pink, loincloth moved in a layer above greaves, alchemyst in Westgard)

# Version 0.8.35 
- first two weeks completely reworked
- the story mostly stayed the same, order of some events was changed or they were slightly expanded 
- completely new clothing system
- completely new dungeon system

# Build 0.8.34/0.8.36 
- attempt to balance dungeons somehow
- cheating - on the easy difficulty it is checked whether you are running out of hp's and health potions and consequently more are dropped as loot 
- fixed some combat-related issues (poison not working etc. )
- fixed oversight when the fatality of action was calculated when was the action written on screen not when taken (ie. extra hp gained by drinking health potion right before dashing through blades was ignored)
- <i>get dressed</i> choice - puts on the same clothes as yesterday
- magic power is displayed in the inventory, hit points details displayed on mouse hover, fixed going into negative amounts of money
- armor is a separate layer, could be combined with clothes
- pant legs and sleeves are tucked into armor
- fixed some minor issues with assets (broken bikini bottom etc.)
- some messing with price and color of clothes in the game
- changed font
- at the beginning of a dungeon, there is an option to leave 
- there is a chance I finally found the bug which caused losing clothes after reload

# Build 0.8.32 
- some scenes for Tuesday and Wednesday
- it could be displayed who is talking <small>(Settings > NPCS > Show names)</small>
- avatar got nighties so she would not be nude in the morning

# Build 0.8.30 
- Friday + Weekend + Monday
- alternative to boobjob <small>(second advice, Matej)</small>
- new dungeon Westgard Sewers 2 <small>(accessible during the second weekend, dungeons for the third weekend are not finished yet)</small>
- DAD updated, shoes, corsets and gorgets fixed
- potions rebalanced, some efects changed a bit, few new added
- two new, bovinium <small>(actually minotaurium with boobs)</small> and felinum
- fixed many potion and transformation related bugs 
- some more messing with the save system, you can now <i>Save As</i> and change the name of the save,

# Build 0.8.20 
- dungeons improved 
- saving improved
- added new dungeon Westgard Sewers 
- price for skill checks is paid only when their fail 
- <i>Feroucious Attack</i> hits one more enemy but causes the half of the damage, new <i>Double Strike</i> does the full damage to two enemies
- names of potions are revealed if you drink enough of them
- Healt and Energy potions could be drink directly during combat 
- poisoining + antivenom 
- effect of <i>Tease</i> based on charisma, probability of <i>Danger Savvy</i> based on the number of enemies
- improved saving (three autosaves, the latest save is detected, unlimited slots)
- puzzle minigame improved
- fixed css width of the right div
- fixed (Random Apperance) being broken
- fixed potion changing colours when reloaded
- fixed bugged saving of game inventory
- fixed lips transformation being broken
- fixed colision of corset (leather armor) and belt
- fixed buying and equiping itemps with right click not working
- many tipos fixed 

# Build 0.8.11 
- fixed bugged piercings
		
# Build 0.8.10 
- some new content (meeting AI during the weekend, some missing Juraj's scenes, like week of new or rehashed content)
- new armors, weapons rebalanced a bit, some prices changed
- socks and jewellery 
- added sorting in the inventory, items could be equiped while trading by right click
- some good similar games mentioned in the Credits 

# Build 0.8.00 
- user interface reworked
- new version DAD and new reworked assets
- new absolutely awesome inventory system
- new scalable dungeon system
- order of event reworked, some expanded a bit
- character appearance could be adjusted
- minigames accessible from the main menu
- one new minigame (Crystals) and prototype of another

#  0.6.10 
- fixed saving, it should work everywhere except dungeons, but that is okay because I will claim it is an anti-savescumming feature (actually my clothing and saving systems did not work together so I had to completely rework them a bit)<li>fixed countless other issues (following attempts to solve the letter after the first one were broken, broken links, Hellion's tattoo disappeared, etc.)
	
#  0.6.00  
- week of new content<li>including the second dungeon and the second minigame<li>the first dungeon reworked<li>fancier health bars<li>to the starting city added shop with basic clothes customisatin<li>rewritten intro<li>a lousy attempt to improve the main character motivation <li>reworked potions, added Minotaur transformation<li>included experimental version of procedural clothing generator<li>made saving system from scratch, it probably will not properly and you shall not trust it
	
#  0.5.00 
- like day and half of new content<li>changed some colours for better readability, I hope<li>added succubi transformation effects<li>Kewins are now shuffled<li>hacking minigame made very slightly easier, there are 8 attempts instead of 7 and it could be skipped after the first game, not third 
	
