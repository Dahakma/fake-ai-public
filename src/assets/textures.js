import copper from "./textures/copper.png";
import fishnet_1 from "./textures/fishnet_1.png";
import fishnet_2 from "./textures/fishnet_2.png";
import fishnet_3 from "./textures/fishnet_3.png";
import fishnet_4 from "./textures/fishnet_4.png";
import fishnet_5 from "./textures/fishnet_5.png";
import fishnet_6 from "./textures/fishnet_6.png";
import leopard from "./textures/leopard.png";
import snake from "./textures/snake.png";
import zebra from "./textures/zebra.png";
import camo from "./textures/camo.png";
import iron from "./textures/iron.png";
import emo from "./textures/emo.gif";
import bumblebee from "./textures/bumblebee.gif";
import anarchy from "./textures/anarchy.gif";
import cyany from "./textures/cyany.gif";
import forest from "./textures/forest.gif";
import domino from "./textures/domino.gif";
import lgbt from "./textures/lgbt.gif";
import eesti from "./textures/eesti.gif";
import indonesia from "./textures/indonesia.gif";
import jamaica from "./textures/jamaica.gif";
import witch from "./textures/witch.gif";
import africa from "./textures/africa.gif";

export const textures = {
	copper,
	fishnet_1,
	fishnet_2,
	fishnet_3,
	fishnet_4,
	fishnet_5,
	fishnet_6,
	leopard,
	snake,
	zebra,
	camo,
	iron,
	emo,
	bumblebee,
	anarchy,
	cyany,
	forest,
	domino,
	lgbt,
	eesti,
	indonesia,
	jamaica,
	witch,
	africa,
};


