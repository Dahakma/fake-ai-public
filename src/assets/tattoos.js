//autogenerated by scripts/import_assets.js 
import anal from "./tattoos/anal.png";
import anchor from "./tattoos/anchor.png";
import ankh from "./tattoos/ankh.png";
import bad_girl from "./tattoos/bad_girl.png";
import bad_girl2 from "./tattoos/bad_girl2.png";
import bimbo from "./tattoos/bimbo.png";
import biohazard from "./tattoos/biohazard.png";
import bitch from "./tattoos/bitch.png";
import bloom from "./tattoos/bloom.png";
import bloom2 from "./tattoos/bloom2.png";
import blueBand2 from "./tattoos/blueBand2.png";
import breastSide from "./tattoos/breastSide.png";
import breastSide2 from "./tattoos/breastSide2.png";
import breastSide3 from "./tattoos/breastSide3.png";
import breastSideWeb from "./tattoos/breastSideWeb.png";
import butteflySide from "./tattoos/butteflySide.png";
import butterfly from "./tattoos/butterfly.png";
import butt_slutt from "./tattoos/butt_slutt.png";
import carp from "./tattoos/carp.png";
import celticBear from "./tattoos/celticBear.png";
import celticCircle from "./tattoos/celticCircle.png";
import celticHeart from "./tattoos/celticHeart.png";
import chandle1 from "./tattoos/chandle1.png";
import chandle2 from "./tattoos/chandle2.png";
import chandle3 from "./tattoos/chandle3.png";
import chandle4 from "./tattoos/chandle4.png";
import chandle5 from "./tattoos/chandle5.png";
import chandleFlower from "./tattoos/chandleFlower.png";
import chandleFlower2 from "./tattoos/chandleFlower2.png";
import cheap_whore from "./tattoos/cheap_whore.png";
import ch_slut from "./tattoos/ch_slut.png";
import circle from "./tattoos/circle.png";
import circleFlowers from "./tattoos/circleFlowers.png";
import circleFlowers2 from "./tattoos/circleFlowers2.png";
import clover from "./tattoos/clover.png";
import cock from "./tattoos/cock.png";
import daddys from "./tattoos/daddys.png";
import daisy from "./tattoos/daisy.png";
import deers from "./tattoos/deers.png";
import dices from "./tattoos/dices.png";
import dolphinCircle from "./tattoos/dolphinCircle.png";
import dragon2 from "./tattoos/dragon2.png";
import dragon3 from "./tattoos/dragon3.png";
import dragonBlue from "./tattoos/dragonBlue.png";
import dragons from "./tattoos/dragons.png";
import dragon_as2 from "./tattoos/dragon_as2.png";
import dragon_as3 from "./tattoos/dragon_as3.png";
import dump from "./tattoos/dump.png";
import dump2 from "./tattoos/dump2.png";
import eat_me from "./tattoos/eat_me.png";
import egypt4 from "./tattoos/egypt4.png";
import egyptSun from "./tattoos/egyptSun.png";
import flowers2 from "./tattoos/flowers2.png";
import flowers3 from "./tattoos/flowers3.png";
import freeUse from "./tattoos/freeUse.png";
import futhark from "./tattoos/futhark.png";
import girlie from "./tattoos/girlie.png";
import gothicRose from "./tattoos/gothicRose.png";
import guns from "./tattoos/guns.png";
import heartBirds from "./tattoos/heartBirds.png";
import heartButterfly from "./tattoos/heartButterfly.png";
import heartCurve from "./tattoos/heartCurve.png";
import horus from "./tattoos/horus.png";
import inmon from "./tattoos/inmon.png";
import inmon1 from "./tattoos/inmon1.png";
import inmon3 from "./tattoos/inmon3.png";
import inmon5 from "./tattoos/inmon5.png";
import inmon6 from "./tattoos/inmon6.png";
import inmon7 from "./tattoos/inmon7.png";
import isis2 from "./tattoos/isis2.png";
import ivy from "./tattoos/ivy.png";
import lily from "./tattoos/lily.png";
import lock from "./tattoos/lock.png";
import majesticButtefly from "./tattoos/majesticButtefly.png";
import mand from "./tattoos/mand.png";
import mand2 from "./tattoos/mand2.png";
import mermaid from "./tattoos/mermaid.png";
import meta from "./tattoos/meta.png";
import niceFlowerSide from "./tattoos/niceFlowerSide.png";
import niceShapes from "./tattoos/niceShapes.png";
import nordic from "./tattoos/nordic.png";
import nude from "./tattoos/nude.png";
import nympho from "./tattoos/nympho.png";
import owlCurve from "./tattoos/owlCurve.png";
import pentagram from "./tattoos/pentagram.png";
import pentagram2 from "./tattoos/pentagram2.png";
import pentagramNeck from "./tattoos/pentagramNeck.png";
import queenSpades from "./tattoos/queenSpades.png";
import ragrets from "./tattoos/ragrets.png";
import rose from "./tattoos/rose.png";
import roseHeart from "./tattoos/roseHeart.png";
import roseSide from "./tattoos/roseSide.png";
import roseSide2 from "./tattoos/roseSide2.png";
import roseSkull from "./tattoos/roseSkull.png";
import scorpion from "./tattoos/scorpion.png";
import sexy_bitch from "./tattoos/sexy_bitch.png";
import sex_kitten from "./tattoos/sex_kitten.png";
import sillyUnicorn from "./tattoos/sillyUnicorn.png";
import simpleButterfly from "./tattoos/simpleButterfly.png";
import sixtyNine from "./tattoos/sixtyNine.png";
import skull2 from "./tattoos/skull2.png";
import skull3 from "./tattoos/skull3.png";
import sleeve1 from "./tattoos/sleeve1.png";
import sleeveCircles from "./tattoos/sleeveCircles.png";
import sleeveDemo from "./tattoos/sleeveDemo.png";
import sleevePoly from "./tattoos/sleevePoly.png";
import sleeveTigre2 from "./tattoos/sleeveTigre2.png";
import sleeve_dra_01 from "./tattoos/sleeve_dra_01.png";
import sleeve_ire from "./tattoos/sleeve_ire.png";
import sleeve_poly_02 from "./tattoos/sleeve_poly_02.png";
import slut from "./tattoos/slut.png";
import slut2 from "./tattoos/slut2.png";
import snake2 from "./tattoos/snake2.png";
import stars1 from "./tattoos/stars1.png";
import stars2 from "./tattoos/stars2.png";
import stars3 from "./tattoos/stars3.png";
import stupid_slut from "./tattoos/stupid_slut.png";
import sun from "./tattoos/sun.png";
import sun2 from "./tattoos/sun2.png";
import sun3 from "./tattoos/sun3.png";
import thighFlowers1 from "./tattoos/thighFlowers1.png";
import thighStars from "./tattoos/thighStars.png";
import thornedRose from "./tattoos/thornedRose.png";
import tiger from "./tattoos/tiger.png";
import tiger2 from "./tattoos/tiger2.png";
import toy from "./tattoos/toy.png";
import trashy_slut from "./tattoos/trashy_slut.png";
import tree from "./tattoos/tree.png";
import triangle from "./tattoos/triangle.png";
import tribalButterfly from "./tattoos/tribalButterfly.png";
import tribalCurve from "./tattoos/tribalCurve.png";
import tribalRose1 from "./tattoos/tribalRose1.png";
import tribCurve from "./tattoos/tribCurve.png";
import waveDolphin from "./tattoos/waveDolphin.png";
import web1 from "./tattoos/web1.png";
import web2 from "./tattoos/web2.png";
import whore from "./tattoos/whore.png";
import wingedEye from "./tattoos/wingedEye.png";


export const tattoos = {
	anal,
	anchor,
	ankh,
	bad_girl,
	bad_girl2,
	bimbo,
	biohazard,
	bitch,
	bloom,
	bloom2,
	blueBand2,
	breastSide,
	breastSide2,
	breastSide3,
	breastSideWeb,
	butteflySide,
	butterfly,
	butt_slutt,
	carp,
	celticBear,
	celticCircle,
	celticHeart,
	chandle1,
	chandle2,
	chandle3,
	chandle4,
	chandle5,
	chandleFlower,
	chandleFlower2,
	cheap_whore,
	ch_slut,
	circle,
	circleFlowers,
	circleFlowers2,
	clover,
	cock,
	daddys,
	daisy,
	deers,
	dices,
	dolphinCircle,
	dragon2,
	dragon3,
	dragonBlue,
	dragons,
	dragon_as2,
	dragon_as3,
	dump,
	dump2,
	eat_me,
	egypt4,
	egyptSun,
	flowers2,
	flowers3,
	freeUse,
	futhark,
	girlie,
	gothicRose,
	guns,
	heartBirds,
	heartButterfly,
	heartCurve,
	horus,
	inmon,
	inmon1,
	inmon3,
	inmon5,
	inmon6,
	inmon7,
	isis2,
	ivy,
	lily,
	lock,
	majesticButtefly,
	mand,
	mand2,
	mermaid,
	meta,
	niceFlowerSide,
	niceShapes,
	nordic,
	nude,
	nympho,
	owlCurve,
	pentagram,
	pentagram2,
	pentagramNeck,
	queenSpades,
	ragrets,
	rose,
	roseHeart,
	roseSide,
	roseSide2,
	roseSkull,
	scorpion,
	sexy_bitch,
	sex_kitten,
	sillyUnicorn,
	simpleButterfly,
	sixtyNine,
	skull2,
	skull3,
	sleeve1,
	sleeveCircles,
	sleeveDemo,
	sleevePoly,
	sleeveTigre2,
	sleeve_dra_01,
	sleeve_ire,
	sleeve_poly_02,
	slut,
	slut2,
	snake2,
	stars1,
	stars2,
	stars3,
	stupid_slut,
	sun,
	sun2,
	sun3,
	thighFlowers1,
	thighStars,
	thornedRose,
	tiger,
	tiger2,
	toy,
	trashy_slut,
	tree,
	triangle,
	tribalButterfly,
	tribalCurve,
	tribalRose1,
	tribCurve,
	waveDolphin,
	web1,
	web2,
	whore,
	wingedEye,
};

export const tattoos_critical = { //TODO TEMPFIX!! - to make sure at least these tattoos are not broken & are loaded 
	triangle,
	dragonBlue,
	blueBand2,
};



export const tattoos_slutty = [ //tattoos marked as especially slutty (todo - should be here?)
		"freeUse",
		"slut",
		"slut2",
		"dump",
		"dump2",
		"sixtyNine",
		
		"toy",
		"bimbo",
		"bad_girl",
		"cock",
		
		"daddys",
		"anal",
		"bitch",
		"eat_me",
		"sex_kitten",
		"stupid_slut",
		"cheap_whore",
		
		/*
		//TODO - inmons slutty?
		"inmon",
		"inmon1",
		"inmon3",
		"inmon5",
		"inmon6",
		*/
		
		"inmon7",
		
		"bad_girl2",
		"butt_slutt",
		"sexy_bitch",
		"nympho",
		"whore",
		"trashy_slut",
];