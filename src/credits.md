The Game was made by me, [Dahakma](https://dahakma.maweb.eu). You can give me feedback, ask questions or complain on my [Discord](https://discord.gg/mwtVmAs). 

It was done for the **Virtual Worlds Contest** on [TF Games Site](https://tfgames.site/index.php?module=viewgame&id=1886). It was a last-minute idea and that is why the plot really does not make sense and really wish I did something else. It won 3rd place.

The game heavily utilizes the amazing [Dynamic Avatar Drawer](https://tfgames.site/index.php?module=viewgame&id=868). 

I also made the trivia quiz and bimbo-transformation sandbox [**Who Wanna Be a Bimbo?**](https://tfgames.site/index.php?module=viewgame&id=1217)
and the pretty self-descriptive story  [**I Was Enslaved by an Evil Witch and Turned into Her Maid!**](https://tfgames.site/index.php?module=viewgame&id=2001)
Check them out!

I also have a [Patreon](https://www.patreon.com/Dahakma). Pls back me, I am broke. Patrons have access to a neat version with enabled cheats and the newest updates.

The harlot character is inspired by the best lewd game I have ever played **A Dance with Rogues**. It is basically a mod of *Neverwinter Nights* but it has tons of content and an amazing story and great characters and everything. The hellion is inspired by **Darkest Dungeon** and also a bit by the character *Sam [Gardner] the Barbarian*. The virtual game is overall inspired by my favorite RPG **TESIII: Morrowind**.
<!-- https://rule34.xxx/index.php?page=post&s=view&id=2799364 -->
Other main inspirations are the awesome game with great art and story **Good Girl Gone Bad** and the story of blackmailed bitchy elven schoolgirl **Adventures Of Tara**.

The main game loop is inspired by **The Making of a Slut** and the old **Perverted Education** and some systems were inspired by **Degrees of Lewdity**, although I cannot recommend the content of those games universally to everybody.

My current absolutely favorite game, also set in virtual reality, is [**Healslut**](https://dizzy.works). It is definitely worth checking out. My other favorite game, where is the life of the main heroine complicated by slutty AI too, is the sublime [**Tales of the Drunken Cowboy**](https://tfgames.site/index.php?module=viewgame&id=760).
				
				
