import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";

import {next_event, add_tomorrow, add_today, get_schedule} from "Taskmaster/scheduler";
import {wearsPink, sluttiness, makeup, wearRandomClothes, remove, wear, create, wears, showLess, showAll} from "Avatar/index";
import {placetime} from "Loop/text_extra";



export const advice_1 = (index) => {
	switch(index){
		default:
		case 101: 
			set.irl();
			placetime("at home");
			if(head.day === 3){ //mond
				txt(`When I returned from the school, I immediately started the game - getting rid of those creepy nerds and their horrible creation was my main priority. `);
			}else if(head.day <= 5){ //wen
				txt(`Finishing the game ASAP and getting rid of those creepy nerds and their horrible creation was my main priority so I decided to spend the afternoon and evening playing. `);
			}else{
				txt(`If I ever wanted to get rid of those creepy nerds and their horrible creation, I had to stop procrastinating and finish their stupid game. `);
			}
			txt(`I expected my advance will be easy and swift but I immediately ran into troubles. I was unable to deal any damage and was always unceremoniously killed. It was impossible to gain any foothold! `);
			txt(`To get better equipment I needed money. To get the money I need to explore dungeons. To explore dungeons without getting immediately slaughtered I need better equipment. It was a vicious loop.  `);

			if(mile.determined){
				txt(`I was trying again, always failing. I eventually had to admit I was wasting my time and had to change my strategy.  `);
			}else{
				txt(`When I was killed for the third time, I decided to give up. This was going nowhere!  `);
			}
			txt(`There had to be a better way. Maybe I should ask somebody for an advice? Those three nerds knew the game through and through. Maybe I should try to convince one of them to help me.  `);		
			if(mile.a_burned){
				link(`Try @qaa(the one with crush)`, 102);
			}else{
				chapter(`Try @qaa(the one with crush)`, `a_advice_1`);	
			}
			chapter(`Try @qbb (the creepy one)`, `b_advice_1`);		
			chapter(`Try @qcc (the clumsy one)`, `c_advice_1`);	
			break;
		
		case 102: 
			if(head.day === 3){
				txt(`I considered @qaa. He would be my first choice but after the horrible argument we had today, I was not in the mood to speak with him or to even see his stupid pathetic face. `);
			}else{
				txt(`Under normal circumstances, @qaa would be my first choice. But after our recent horrible argument... `);
				txt(`Well, maybe he had enough time to realise how wrong he was? `);
				txt(`I tried to text him but he was ghosting me. Really fucking mature, dumbass! `);
			}
			chapter(`Try @qbb (the creepy one)`, `b_advice_1`);		
			chapter(`Try @qcc (the clumsy one)`, `c_advice_1`);	
			break;
		
		case 200:
			switch(mile.advice_1){
				case 2:
					qbb(`Do whatever you wish, I don't really care! `);
					break;
				default: //empty
					break;
			}
			
			if(SETTING.no_virtual_game){
				chapter(`Start the game.  `, `skip_feyd`);
			}else{
				chapter(`Start the game. `, `feyd`);
				if(softbug) chapter(`#Skip the game. `, `skip_feyd`);
			}
			break;
	}
}















export const advice_2 = (index) => {
	switch(index){
		default:		
		case 101: 
			set.irl();
			placetime("at home");
			txt(`I have to admit, I was a bit overwhelmed by Arazor's grandiose plan. I had no idea how to lead an expedition! There were so many things to organize! And even after managing that, I had no idea in which direction we should sail!`);
			if(mile.blackmail){
				txt(`The bastards were not afraid to outright blackmail me and I needed to finish the game as soon as possible. There was no time to waste on the trial and error approach. I needed a little assistance. `);
			}else{
				txt(`Their tasks were getting more and more annoying and I wanted to finish the game as soon as possible. There was no time to waste on the trial and error approach. I needed a little assistance. `);
			}
			
			txt(`Asked for advice worked once, why it should not work for the second time? `);
			
			if(!mile.a_burned) chapter(`Ask @qaa(the one with crush)`, `a_advice_2`);	
			chapter(`Ask @qbb (the creepy one)`, `b_advice_2`);		
			chapter(`Ask @qcc (the clumsy one)`, `c_advice_2`);	
			break;
	}
}


export const advice_3 = (index) => {
	switch(index){	
		default:	
		case 101: 
			set.irl();
			placetime("at home");
			txt(`TODO: something about help delivering a letter from Westgard governor to the High Queen of Elves living in Amaranthine Forest. `);
			
			chapter(`Ask @qaa(the one with crush)`, `a_advice_3`);	
			chapter(`Ask @qbb (the creepy one)`, `b_advice_3`);		
			chapter(`Ask @qcc (the clumsy one)`, `c_advice_3`);	
			break;
		
		case 102: 
			!mile.advice_3_1 ? chapter(`Ask @qaa(the one with crush)`, `a_advice_3`) : chapter(`Ask @qaa(the one with crush)`, `a_advice_3`, 102);
			!mile.advice_3_2 ? chapter(`Ask @qbb (the creepy one)`, `b_advice_3`) : chapter(`Ask @qbb (the creepy one)`, `b_advice_3`, 102);
			!mile.advice_3_3 ? chapter(`Ask @qcc (the clumsy one)`, `c_advice_3`) : chapter(`Ask @qcc (the clumsy one)`, `c_advice_3`, 102);
			break;		
	}
}



export const advice_4 = (index) => {
	switch(index){
		default:
		case 101: 
			set.irl();
			placetime("at home");
			txt(`TODO`);
			
			if(mile.advice_3 !== 1)	!mile.advice_3_1 ? chapter(`Ask @qaa(the one with crush)`, `a_advice_3`) : chapter(`Ask @qaa(the one with crush)`, `a_advice_3`, 102);
			if(mile.advice_3 !== 2) !mile.advice_3_2 ? chapter(`Ask @qbb (the creepy one)`, `b_advice_3`) : chapter(`Ask @qbb (the creepy one)`, `b_advice_3`, 102);
			if(mile.advice_3 !== 3) !mile.advice_3_3 ? chapter(`Ask @qcc (the clumsy one)`, `c_advice_3`) : chapter(`Ask @qcc (the clumsy one)`, `c_advice_3`, 102);
			break;	

		case 102: 
			if(mile.advice_3 !== 1) !mile.advice_3_1 ? chapter(`Ask @qaa(the one with crush)`, `a_advice_3`) : chapter(`Ask @qaa(the one with crush)`, `a_advice_3`, 102);
			if(mile.advice_3 !== 2) !mile.advice_3_2 ? chapter(`Ask @qbb (the creepy one)`, `b_advice_3`) : chapter(`Ask @qbb (the creepy one)`, `b_advice_3`, 102);
			if(mile.advice_3 !== 3) !mile.advice_3_3 ? chapter(`Ask @qcc (the clumsy one)`, `c_advice_3`) : chapter(`Ask @qcc (the clumsy one)`, `c_advice_3`, 102);
			break;			
	}
	
}







