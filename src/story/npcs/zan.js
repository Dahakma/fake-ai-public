import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";
import {placetime} from "Loop/text_extra";
import {emo, draw, effigy, INVENTORY, sluttiness, makeup, wearRandomClothes, remove, wear, create, wears, showLess, showAll} from "Avatar/index";
import {toHSLA, nameColor, parseHSLA} from "Libraries/color/index";
import {log_sex} from "Loop/text_extra";


export const tom_zan_date = (index)=> {
	placetime("classroom");
	mile.tom_zan = 1;
	txt(`During the break, I chatted with my friends. We were discussing the weekend. I let them do all the talking, my weekend was horrible, mostly consisting of pandering to those stupid nerds and playing a stupid virtual game.`);
	sas(`We missed you!`);
	kat(`Sorry, I couldn't go with you this time! Did anything interesting happen? Did you meet any hot guys?`);
	eva(`No, nothing especially noteworthy.`);
	sas(`@zan and @tom!`);
	eva(`Oh, yeah! How could I forget! @zan and @tom were making out!`);
	kat(`No way! @zan and @tom?`);
	eva(`And not only making out. They left the club together.`);
	kat(`Interesting.`);
	sas(`I think they fit together pretty well.`);
	txt(`Suggested romantic @sas but I have my doubts.`);
	kat(`Yeah, it won't last. @tom can't keep it in his pants and @zan is a skank. I give it two weeks!`);
	eva(`I bet even less!`);
	next();
}

export const tom_zan_break = (index)=> {
	placetime("schoolyard");
	mile.tom_zan = 4;
	txt(`I joined @eva and @sas outside. They were discussing things that happened at @randomFemaleName's party.`);
	eva(`@kat! Where were you?!? You missed a lot!`);
	kat(`Did I? I couldn't go because... studying. What happened?`);
	sas(`@zan and @tom broke up.`);
	kat(`Why?`);
	eva(`@zan cheated on him.`);
	kat(`Of course she did! What a skank!`);
	sas(`She didn't cheat on him. He was cheating on her! She found him talking with @randomFemaleName and by talking I mean making out with his hands under her clothes.`);
	eva(`@zan was drunk or high and - being skank she is - thought the best retribution would be to have sex with the first guy she met. So she disappeared somewhere with @voj.`);
	kat(`And they fucked?`);
	eva(`I haven't seen them but yeah, that's what she told @tom.`);
	kat(`I assume he took it well?`);
	sas(`Hahaha!`);
	eva(`No! There was a lot of screaming. It was absolutely horrible and awkward being forced to watch them argue. You would love it!`);
	kat(`Damn! I wish I could be there!`);
	eva(`And then they broke up. @tom got drunk and wanted to fight @voj!`);
	kat(`Oh no! Did @voj beat him up?`);
	sas(`No. @voj was the only reasonable person there.`);
	eva(`Except for sleeping with @zan.`);
	txt(`Damn! Such an exciting happening and I was sitting home playing a videogame! Those stupid nerds were completely ruining my social life!`);
	next();
}



export const tom_date = (index)=> {
	switch(index){	
		default:
		case 101:
			placetime(`tram stop`);
			mile.tom_zan = 6; 
			mile.tom_date = 3;
				
			if(mile.chair_elected === 1){
				
				emo(`relax`);
				//TODO - rewrite for slutty/prude kate 
				
				txt(`I met with @tom halfway between our places. I went through so much troubles just to get releected. But I made it! And only thing that remained was to tie a few lose ends. `);
				tom(`Hello.`);
				kat(`Hello. Excited about your reward? `);
				tom(`Of course! Where do you want to go? @zan showed me this gallery - I know the galleries sound lame but there are all sorts of weird modern installations and some of them are moving or are nicely illuminated and you can take awesome selfies there...`);
				kat(`Honestly I'm not in mood for galleries or museums. What about going to your place and doing something more physical? I think you deserve it. `);
				
				
			}else{
				emo(`sad`);
				
				txt(`I met with @tom halfway between our places. The date with @tom reminded me ${mile.chair_elected === 2 ? "@eva" : "@ven"} was now the class chairman and it was making me pretty dejected.`);
				tom(`Hello.`);
				kat(`Hello.`);
				tom(`What is wrong?`);
				kat(`So many things.`);
				tom(`Smile up! I'm here just for you! Where do you want to go? @zan showed me this gallery - I know the galleries sound lame but there are all sorts of weird modern installations and some of them are moving or are nicely illuminated and you can take awesome selfies there...`);
				{
					const deal = (()=>{
						if(mile.tom_help === 3) return "So I could give you the promised blowjob? ";
						if(mile.tom_help >= 4) return "So we could skip straight to the promised sex? ";
						return "";
					})();
					kat(`Sorry, I'm not in the mood. Can we just go to your place? ${deal}`);
				}
			}
				//TODO - reply by slutiness 
				/*
				if(mile.tom_help >= 4){
					Hah, you're so eager to get into my bed? 
					
				}
				*/
			txt(`@tom was suprised but quickly nodded:`);
			tom(`Yeah, sounds good, if you want to, I'm definitely not against it! Let's go!`);
			link(`Go.`, 102);
		
			break; 
			
		case 102:
			placetime(`tram stop`);
			emo(`curious`);
			effigy.tom.showLess(0,-1);
			showLess(1,1);
			txt(`We arrived at @tom's place, he lived in a similar tower block as me. His room was disorganized, walls were covered with posters of cars and semi or even fully-nude models and actresses and one disturbingly random poster of Mucha's Art Nouveau painting.`);
			tom(`So what would you like to do? Watch a movie or...`);
			txt(`I leaned closer and kissed him. I did not want to talk, I was in the mood for something more physical. He kissed me and gently caressed my face.`);
			tom(`Whoa, @katalt, I already like you more than @zan! Sometimes it was such a chore to convince @zan to have sex. I'm pretty sure she always wanted it but she just played hard to get to piss me up.`);
			kat(`Well, you're lucky. Today I'm very easy!`);
			tom(`The luckiest man on Earth!`);
			txt(`@tom make out with me, this time his hand slipped under my @upper ${mile.boobs > 1 ? ", he exploited the opportunity to closely examine my enlarged breasts" : "and he squeezed my boob"}. ${!wears.bra && wears.piercedNipples ? "The tip of his index finger was rubbing against my nipple piercing. " : ""}`);
			txt(`Our lips stopped touching and he helped me to strip my top. Then I removed his, I rather enjoyed the sight of his muscular chest. ${mile.a_dating ? "It was a nice change, @qaa should definitely start working out. " : ""}`);
			tom(`What would you like to do? Maybe it would cheer you up if I eat out your pussy? @zan used to love that very much! I kinda enjoyed it too but there was the problem she never shut up when I was down there, blowjobs were far better in that regard.`);
			kat(`Sure, that sounds like fun.`);
			txt(`I lay down on his bed and he pulled down my @lower, exposing my ${wears.panties ? "cute panties" : "@pussy"}. Suddenly he stopped and looked unsure.`);
			kat(`@tom?`);
			tom(`I'm sorry, @kat, but I can't do this.`);
			kat(`What do you mean?`);
			tom(`I have to go.`);
			txt(`He hastily put his shirt back on.`);
			kat(`Go where?`);
			tom(`To talk with @zan.`);
			kat(`What?! Why?`);
			tom(`Because I was stupid!`);
			
			
			link(`Okay... `, 103, ()=> counter.temp = 1);
			link(`You can't be fucking serious! `, 103, ()=> counter.temp = 3);
			link(`I let you to fuck my ass.  `, 103, ()=> counter.temp = 4);
			break;
					
	case 103:
			emo(`help`);
			if(counter.temp === 1){
				txt(`I shrugged with resignation.`);
				tom(`Thank you, @kat, I'm happy you understand! Bye!`);
			}else if(counter.temp === 2){
				tom(`I am!`);
				kat(`You're being stupid right now!`);
				tom(`I'm not! I know exactly what I have to do! I'm sorry, @kat. Bye!`);
			}else{
				if(mile.slut < 16) mile.slut++;
				tom(`Oh...`);
				txt(`He hesitated for a moment but then he shook his head.`);
				tom(`Sorry, @kat. I have to go. Bye!`);
			}
			txt(`And he was gone, leaving me there horny and nearly completely naked. Fuck! How could @tom refuse to fuck me? My self-assurance was hurt pretty badly.`);
			//TODO maturbations
			txt(`I slowly got dressed and went home.`);
			next();
			break;
	}
}

			
export const tom_zan_threeway = (index)=> {
	switch(index){	
		default:
		case 101:
			placetime(`At home`);
			emo("suspect");
			mile.tom_zan_threeway = 1;
			txt(`My phone began furiously ringing. The caller id revealed it was @tom. I was still a bit angry at him and my first instinct was to hang up and ghost him. However, I eventually decided to be an adult and answered the phone:`);
			tom(`@kat! You have to come to @zan's place! Right now! I'll send you the coordinates. `);
			txt(`Without greeting he began hastily explaining.`);
			kat(`Slow down! Why? What do you mean? Are you going to humiliate me and abandon me again, like the last time?`);
			tom(`...no! I'm extremely sorry about that! I know it was unfair to treat you that way. But at the moment I wasn't able to think straight! And I can make it up to you!`);
			kat(`Really?`);
			tom(`Yes! Would you want to have a threesome with me and @zan? Why I'm even asking, of course, you would!`);
			kat(`And @zan actually agrees with it?`);
			tom(`No! The whole thing was her idea!`);
			kat(`I can't believe it!`);
			tom(`Me neither! You have to get here before I wake up!`);
			kat(`Hmmm.`);
			if(mile.slut < 5){
				tom(`Come on! This is once in a lifetime opportunity! Have you ever had a threesome? Me neither! It will be amazing and eye-opening, I promise! Please, @katalt, don't let me down! Pretty please!`);
			}else if(mile.slut > 10){
				tom(`Come on! This is once in a lifetime opportunity! Well, at least for me. I'm sure you get invited to threesomes all the time. Please, @katalt, don't let me down! Pretty please!`);
			}else{
				tom(`Come on! This is once in a lifetime opportunity! I promise it will be an amazing experience! Please, @katalt, don't let me down! Pretty please!`);
			}
			
			link(`Awesome! I'll be there!`, 102);
			link(`Hmmm... fine!`, 102);
			next(`Not interested. `, ()=> {
				mile.tom_zan_threeway = -1;
				mile.slut--;
			}); //why would anybod pick this? 
			break;
	
		case 102:
			placetime(`@zan's place`);
			emo("shy");
			txt(`@zan opened the door and was a bit surprised to see me.`);
			zan(`Hello, @katalt! ...what do you need?`);
			if(mile.slut > 10){
				kat(`I'm here for the threesome!`);
				txt(`I was not beating about the bush.`);
			}else{
				kat(`@tom called me... you know... because of the threesome...`);
			}
			zan(`THREESOME!?! I have no idea what are you talking about!?`);
			kat(`@tom?`);
			txt(`We both turned to @tom who arrive at the door too and casually grabbed @zan by her ass.`);
			tom(`You know, you told me you wanted a threesome with @katalt!`);
			zan(`NO! I did not! Do you ever even listen to what I am saying!`);
			txt(`Fumed @zan.`);
			tom(`Well... sometimes I do... but you were saying...`);
			zan(`You asked me about a threesome and I told you that maybe, at some point in the future, I might not be against it! And when you asked what girl would I choose to be the third, I told you <i>I guess @katalt but I'm not sure. </i>`);
			kat(`I'm very honored to be your first choice.`);
			{
				const sluton = (()=>{
					if(mile.slut <= 3){
						return `Even though I expected you'll say <i>no</i>. You don't seem like the person who would be into such wild sexual adventures. ` 
					//}else if(mile.slut ){}
					}else if(mile.slut >= 12){
						return `I know you're the type of person who doesn't shy away from the new sexual experience and you wouldn't embarrass me by refusing the offer. `;
					}else{
						return ``;
					}
				})();
				zan(`You're welcome. ${sluton} But it was all hypothetical!`);
			}
			tom(`...`);
			kat(`...`);
			zan(`...`);
			kat(`So... should I go home?`);
			txt(`@zan wavered for a moment.`);
			zan(`Well... I guess... when you're already here...`);
			
			link(`Enter. `, 103);
			break;
	
	case 103: 
			emo("focus");
			txt(`@zan did not look especially excited, especially not in comparison with thrilled @tom.`);
			zan(`Would you like something to drink?`);
			kat(`Sure, that would be nice.`);
			txt(`@zan went to get us drinks in the kitchen while @tom led me to @zan's bedroom.`);
			tom(`@kat, you're the greatest person in the history of the universe and I'll be forever in your debt for fulfilling my second most desired dream!`);
			kat(`You're one of the worst people I ever met and I hate you for tricking us into a threeway. And I don't even want to guess what is your most desired dream, but I assume it involves us being twin sisters, you disgusting perv.`);
			tom(`What? No, it's to have a successful career that will make my father proud. But I like the way you think, even though I prefer diversity! Anyway, @zan is my girlfriend and you're our guest: would it be more polite if I fucked you first and then @zan or the other way around? I'm not sure about the established protocol. I've seen a lot of porn - and I mean a *lot* of porn with various combinations of men and women but I never paid attention to such details.`);
			kat(`You should've taken notes.`);
			tom(`Yeah, probably...`);
			zan(`Are you fine with peach juice and vodka?`);
			txt(`@zan opened the ajar door with her foot, carefully carrying three tall glasses with ice and long metal straws. She seemed more bashful than usual.`);
			kat(`Absolutely!`);
			tom(`So we were discussing what would be the proper order. Should I fuck first you or @katalt?`);
			zan(`You're going to fuck her too!?`);
			tom(`Yeah, that's the point of the threesome. You're not okay with it?`);
			zan(`Yeah, I think so, I just assumed that...`);
			txt(`They were discussing. I excused myself and went to the bathroom to freshen up, respectively, pee and give myself a pep talk in the mirror. As soon as I closed the door of her room, I could hear their arguing escalating and getting fiercer and louder. However, when I returned, there was an eerie silence.`);
			link(`Threeway. `, 104);
			break;
			
	case 104: 
			showLess(1,1,1);
			effigy.tom.showLess(1,1,1);
			effigy.zan.showLess(1,1,1);
			emo("relax");
			
			txt(`They probably concluded @zan's bed was too narrow and dangerous for three people so they prepared mattresses on the floor. They were sitting on them, wearing only their underwear, patiently waiting for my return.`);
			zan(`Hello, @kat!`);
			tom(`Come between us!`);
			txt(`Their pretended casualness and forced friendly smiles made them look very creepy. I took my glass and sat down between them. I felt a bit overdressed.`);
			kat(`Did you figure out how to start?`);
			if(mile.sub > 4){
				zan(`Yeah. Why don't you kiss me again, @kat?`);
				txt(`Confidently suggested @zan.`);
				kat(`I'd love to!`);
			}else{
				zan(`Well... we thought we'll go with the flow...`);
				kat(`Why don't you give me a kiss?`);
				txt(`I took the charge when I saw how unsure they were.`);
			}
			txt(`@zan's fingers toyed with my hair and she pulled me closer. We were making out and I could feel @tom's hands from behind squeezing my breasts. When we interrupted our long kiss, @zan helped me to strip my @upper and I turned to kiss @tom. Then @zan and @tom began making out with each other, lovingly and passionately.`);
			kat(`That wasn't a bad start, was it? What should we do next?`);
			txt(`I suggested when I ${!wears.dress ? "pulled down my @lower and" : ""} began getting bored of watching them.`);
			
			if(mile.sub > 8 && mile.slut > 11){ //sub big slut
				zan(`Now you'll suck @tom's cock. I want to see whether it's true what guys say about you.`);
				kat(`Guys talking about me sucking cocks?`);
				tom(`All the time! But highly!`);
			}else if(mile.sub < 1 && mile.slut > 8){ //dom slut
				kat(`I think I would like to suck @tom's cock now. If you won't mind, @zan, of course.`);
				zan(`Of course not! Go ahead! His cock is all yours!`);
			}else if(mile.sub < 2){ //dom
				kat(`Maybe I could give @tom a blowjob?`);
				txt(`I organized things. @zan nodded:`);
				zan(`Sure! That sounds like a good idea!`);
				tom(`That sounds like an amazing idea!`);
			}else{ 
				if(mile.sub > 8){ //sub
					txt(`@zan smiled and ordered me:`);
					zan(`Now I want you to suck @tom's cock!`);
				}else{ //neutral
					zan(`Maybe you could suck @tom's cock?`);
				}
				tom(`I love you, @zan!`);
				zan(`Shut up, you dummy!`);
			}
			txt(`@tom stood up and we kneeled in front of him. @zan pulled down his ${effigy.tom.panties.name} while I took out his erect dick and let my tongue lightly twirl over the tip.`);
			tom(`Oh @kat!`);
			zan(`So what do you think about his pride?`);
			tom(`@zan's fingers tenderly brushed over his hard shaft.`);
			
			link(`Nice! `, 105, ()=> counter.temp = true);
			link(`Nah. `, 105, ()=> counter.temp = false);
			break;
			
	case 105: 
			emo("happy");
			effigy.tom.showLess(0,0,1);
			log_sex("bj", "tom");
			
			if(counter.temp){
				kat(`I kinda like it! Exactly as you described!`);
			}else{
				kat(`Honestly, I'm not impressed so much. Based on your descriptions I imagined something better.`);
			}
			tom(`You were talking about my cock?`);
			zan(`No! Don't favor yourself too much, we were discussing cocks in general.`);
			kat(`I think I've seen bigger.`);
			zan(`Yeah, me too! But still, I think it's cute!`);
			tom(`Come on, girls!`);
			txt(`Whined @tom from above but his dick was at our mercy and we both teased it as we could. At first, we shared it, but then @zan choose to step back and let me do my worst.`);
			txt(`Delighted @tom was grunting and I took his dick deep in my mouth, roughly sucking him, then slowing down and just letting my tongue frolic, making him crazy with lust.`);
			tom(`Fuck yeah! Awesome! Watch it, @zan! This is how you give a blowjob!`);
			zan(`What the fuck is that supposed to mean? You don't like my blowjobs?!`);
			tom(`No... I didn't mean that! ...I... I just wanted @kat to know she's really great at sucking cocks!`);
			txt(`He looked down, into my eyes and kindly caressed my face:`);
			tom(`You are, @kat!`);
			zan(`Is she better than me!?!`);
			txt(`@zan was not willing to let it go. Frightened @tom looked at me, then at @zan, then back at me, then back at @zan. Suddenly he was enlightened:`);
			tom(`Answering that question wouldn't be fair to @kat!`);
			txt(`@zan stood up and @tom took her around her shoulders.`);
			tom(`Come on, babe! You promised you won't be jealous!`);
			zan(`I'm not jealous! I just hate that you're embarrassing me in front of @katalt!`);
			txt(`@tom kissed her and she conciliatorily ruffled his hair.`);
			tom(`Cutiepie, you know how much I love your blowjobs!`);
			zan(`Shame you're not getting any any time soon!`);
			
			link(`Better enjoy your last one! `, 106, ()=> counter.nice = false);
			link(`Don't be so mean to him! `, 106, ()=> counter.nice = true);
			break;
	
	
	case 106: 
			showLess(0,0,1);
			log_sex("oral", "kat", "tom");
			
			{
				const dom = mile.sub < 3;
				if(counter.nice){
					tom(`I think @katalt is a very wise woman and you should do what she's suggesting!`);
					zan(`She's too nice and she would spoil you. But fine, I'll be nicer to you.`);
					tom(`Yay!`);
					if(dom){
						kat(`You should be a good boy and thank me properly.`);
					}else{
						zan(`I think you should be a good boy and reward @kat for being so kind.`);
					}	
				}else{
					txt(`I giggled and lauging @zan patted my head. Only @tom was not amused:`);
					if(mile.tom_zan_kiss > 0){
						tom(`You wanton bitches are again sticking together!`);
					}else{
						tom(`I don't deserve to be mocked by two sluts!`);
					}
					if(dom){
						kat(`I don't think you appreciate my hard work enough! Why don't you show me what you can do with your tongue?`);
					}else{
						zan(`I don't think you appreciate @kat's hard work enough! It's your turn to pleasure her with your tongue!`);
					}
				}
				txt(`${dom ? "I" : "@zan"} ordered him and he nodded. He gingerly laid me down on the matrass${wears.panties &&  !wears. bra ? " and @zan pulled down my @panties" : ""}${wears.panties && wears.bra ? ", @zan pulled down my @panties and then even helped me to strip my bra" : ""}.`);
				
			}
			
			tom(`I dreamed of getting into your panties for so long and it's finally happening!`);
			txt(`@tom bowed down and his hot tongue began exploring my lower regions.`);
			kat(`Ahhhh!`);
			txt(`I reflexively moaned when @tom's tongue circled around my little button.`);
			zan(`You can thank me! I learned him everything he knows!`);
			txt(`Chuckled @zan and sipped from straw. She was kneeling next to me, in her left hand she was holding the glass of peach juice and vodka, the fingers of her right one were casually fiddling with my nipples.`);
			kat(`Thank you!`);
			txt(`Very smug @zan put her drink away and bowed down to kiss me.`);
			zan(`Honestly, watching him going down on you makes me seriously more jealous than watching you sucking his dick!`);
			kat(`Oh no! I hope we're not neglecting you! You're getting your pussy eaten next!`);
			txt(`I bent my arm to tickle @zan between her legs. I could feel how damp she was.`);
			link(`Eat @zan. `, 107);
			break;
			
	case 107:
			effigy.zan.showLess(1,0,1);
			log_sex("oral", "zan");
			
			txt(`Honestly, I would not mind spending the rest of the evening with @tom's head between my thighs. But I could not be selfish.`);
			if(mile.sub > 10){
				txt(`So I obeyed without hesitation when @tom stopped licking, slapped my thing and ordered:`);
				tom(`My tongue is getting tired, why don't you two sluts have fun together?`);
			}else if(mile.sub < -5){
				kat(`Okay, @tom? You're allowed to stop! @zan! Lie down as I was lying!`);
				txt(`I instructed them.`);
				tom(`Good! My tongue was getting tired!`);
			}else{
				kat(`Okay, I love what you're doing, @tom, but I want to show @zan something.`);
				tom(`Good! My tongue was getting tired!`);
				txt(`@zan lay down exactly as I was lying before.`);
			}
			zan(`@kat? I was never eaten by a girl!`);
		//TODO - ALTERNATIVE IF KATE NEVER LESBIAN ORAL
			kat(`You don't have to worry, it will change your world!`);
			txt(`I ensure her when I was moving between her legs.`);
			tom(`Please don't! Make her scream but... you know... don't do anything that would... you know... make her give up on men!`);
			txt(`I chuckled and then bent down and kissed her lower belly. @tom, who already experienced my oral skills, was a bit concerned that I will turn his precious girlfriend into a little lezzie.`);
			txt(`@zan very vocally appreciated what I was doing below her waist. @tom went to get more drinks and when returned, he did not join us at first, just watched, voyeuristically enjoying a real-life porn scene in front of him. But watching was only half of the fun.`);
			kat(`Ouch!`);
			txt(`I whined when he sneaked up behind me and surprisingly spanked my butt.`);
			tom(`Sorry, I couldn't resist!`);
			txt(`I was kneeling, my head down pressed against @zan's pussy and my ass prezented a vulnerable target. I spread my legs a little wider to give @tom better access to my holes and his fingers immediately exploited it and entered me.`);
			tom(`Can I shove my dick inside her?`);
			zan(`Do... do whatever you want!`);
			txt(`Moaned delirious @zan.`);
			tom(`Thanks! I'm doing it for you, babe!`);
			if(mile.sub > 15) txt(`He asked @zan if he can fuck me. Neither of them cared about my opinion like I was just a toy serving to sexually satisfy them both. But I enjoyed being useful and did not protest. `);
			link(`Get fucked. `, 108);
			break;

	case 108:
			effigy.zan.showLess(0,0,1);
			log_sex("sexs", "tom");
			
			txt(`@tom positioned himself behind me, and firmly grabbed my waist. I could feel something hard brushing against my wet entrance and the he firmly thrust in.`);
			zan(`Oh yeah! Fuck that little slut hard! Oh yes! Don't stop, @kat!`);
			txt(`@zan madly laughed, clutching my head with both of her hands, grinding my face against her pussy in the same tempo as was @tom penetrating me from behind. It was kinda awesome to be in the middle, giving and receiving, feeling the energy of them both.`);
			tom(`You're so beautiful, babe!`);
			txt(`@tom had a splendid full view of @zan lying supine on the mattress and moved one of his hands from my hip to reassuringly caress and lovingly squeeze @zan's calf.`);
			zan(`Awww!`);
			txt(`Gasped @zan and self-consciously tried to fix her hair which was disheveled and sticking to her sweat-covered face. When she carelessly swept with her arm, she accidentally overturn one - fortunately almost empty - high glass. I think it was probably mine but I was not sure, I lost track.`);
			tom(`Oh yeah! I'm getting closer!`);
			zan(`Please, don't cum in her!`);
			if(mile.sub > 5){
				txt(`@zan begged and @tom had a stronger will than I expected. He pulled out of me and haphazardly pushed me away, ${mile.sub > 15 ? "completely discarding me" : ""} so he could move to @zan and kiss her.`);
				if(mile.sub < 10){
					tom(`Sorry, @kat!`);
				}
			}else{
				txt(`@zan begged and @tom had a stronger will than I expected.`);
				tom(`Would... would you mind, @kat?`);
				kat(`Not at all.`);
				txt(`He pulled his dick out of me and I moved away so he could kiss @zan.`);
			}
			txt(`They were making out and then frisky @zan toppled @tom, mounted him and began gleefully and recklessly riding his dick like a seasoned cowgirl. They really were a cute couple, I was thinking while sitting on the floor, rubbing my still heated pussy and watching them furiously fucking. I thought at the moment they completely forgot about me until @zan impatiently spurred me:`);
			zan(`Where the hell are you, @kat!`);
			kat(`Right here!`);
			txt(`I hastily joined @zan and descended down on @tom's face, so he could both enjoy @zan's pussy  and eat mine.`);
			zan(`Fuck! This is awesome!`);
			txt(`@zan ecstatically cried. She was tightly embracing me for extra balance and our heads were lightly brushing against each other. She heavily panted next to my ear and nearly deafened me when she climaxed and shrieked like a banshee.`);
			
			link(`Finish. `, 109);
			break;
			
	case 109:
			showAll();
			remove("bra", effigy.zan);
			effigy.zan.showLess(-1, 1);
			
			txt(`Exhausted we collapsed down. I was sandwiched between them, lightly touched and fondled from both sides.`);
			tom(`So... wasn't this a brilliant idea?`);
			zan(`...yeah, I have to admit, it was, babe.`);
			tom(`Thank you, @kat.`);
			zan(`Did you have fun, @kat? I wasn't sure if we were paying you enough attention.`);
			kat(`Don't worry, it was awesome!`);
			txt(`Lying between them was cozy yet I somehow felt I did not belong there.`);
			kat(`I think I'll go.`);
			zan(`You don't have to go!`);
			txt(`@zan told me but she did not try to convince me very hard. She carelessly dressed so she could see me out, still naked @tom remained laying on the mattresses and was checking his phone.`);
			zan(`I think it went better than I expected.`);
			kat(`Yeah! I have time the next week if you're interested.`);
			zan(`I'm not yet sure. But for the next threeway, I'll want to try an extra guy!`);
			kat(`I'm not sure if @tom will be excited.`);
			zan(`I don't care, he owes me one. And @kat?`);
			kat(`Yes?`);
			zan(`Please, stay away from @tom! And any of our possible future encounters discuss straight with me, not him!`);
			kat(`Oh, @zan! You're still jealous?`);
			zan(`Around sexy bitches like you, you have to watch out!`);
			txt(`@zan looked serious but then she faltered, ending our wonderful evening by threatening me did not feel right. So she quickly kissed me on my cheek, smiled and only then opened the door and let me leave.`);
			next();
			break;
	}
}
			
			
			
			
	/*
	
	
	"What's the matter, babe?" he asks, calmly holding his hand on her shouldsr. "Don't you like me?"
Clara is flustered and clearly way out of her comfort zone. "Uhh, yeah I like you bu-"
"Then what's the problem?"
"I uhh... I don't know how?"



	
	The three-way itself should be fairly organic. Move, touch, and maneuver however you like. Take the lead or allow yourself to be led—whatever feels natural.
As for good positions to try, Morse suggests the guy lie on his back and enjoy oral sex from one woman while the other woman straddles his face so he can perform oral on her. Or have one woman lie on her back as the other woman lies on top of her. Then, the guy can enter the woman on top doggy style while the women touch one another. Another option: Arrange yourselves in a circle, and go down on each other.

"There are so many places to put our hands, our genitals, our mouths,” says Morse. “If you have a free hand or tongue, just get in there.”
	Offer your threesome buddies a drink and a snack because it’s polite.
Music helps to set the mood.

I found that having a threesome amplified the “Am I going to get murdered?” factor that comes with any app date. Something about a couple talking about how they have never done this before but are so excited to play with you (yikes) really ups the potential cartoon villain ante.

Woman C: They were two random guys I met and who pr


I had just had a New Year’s party, so I offered them some snacks and a drink but they declined. I went to the bathroom to freshen up (aka pee and give myself a pep talk in the mirror), and when I came back, one of the guys had turned on some weird EDM music and they were in their boxers on my bed.

I think he went down on me while I made out with her. And at some point, she and I took turns going down on each other. I still had some booze coursing through my system and I bit her nipple a bit too hard.

We took turns going down on her and getting her off, then they took turns on me, then she went down on him while I rode his face and then got on top.

wasn’t as cohesive as some threesomes are. He was very much more into me, since we were seeing each other. It felt a little disjointed.

It was way less capital-S sexy than I imagined. Like, two guys, one girl, isn’t that supposed to be a gal’s dream? But honestly, it was just so many dicks. Too many dicks.

not to make it feel like a group chat where someone’s just getting ignored the whole time.


 And penetration doesn’t do much for me—I really only orgasm from oral—so eventually, things were getting rubbed a little raw and I never even orgasmed. When we turned on the lights at the end, I saw I’d bled a little and I was embarrassed, so my drunk ass decided the best way to handle it was to gasp and say, “Did you bleed on my bed?” As if it weren’t the girl who just got pounded by two separate dicks.
 
  opted out of blowing him, but being able to opt out of my least favorite sex act was its own kind of exciting. Delegation is a joy.
  
  I think the nature of threeways is that it’s not going to be everyone all he time. There’s gotta be an element of selflessness (that, really, all sex should have).
  
   People freak out about making sure everyone is having sex the whole time, which is silly! Don’t worry about getting everyone onto the same body at once. It’s fun to be able to watch each other, to encourage each other, to touch each other. Get creative about what “participation” means.
   At one point, I was giving one guy head while the other went down on me. That was fun!
   
   Just have fun with it! Don’t expect the best sex of your life. But expect some good stories.
   
    Lots of beds are too small for three people, so get creative with where you actually want to get down.
	
	
	Who is allowed to kiss each other? Who is allowed to penetrate each other? Often couples will set boundaries about the kind of behavior that is OK for them during a threesome. If you're the couple, it's on you to clearly communicate that to your third.


 As you can imagine, this made the dude think I was a magical being with the power to make miracles happen. This was both good and bad. Good because the night was fun, bad because, afterward, he thought it was something I could make happen all the time.

*/