import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";

import {emo, draw, quickLoadOutfit, quickTempOutfit, wdesc, toSexyTime, effigy,  sluttiness, makeup, wearRandomClothes, remove, wear, create, wears, showLess, showAll} from "Avatar/index";
import {placetime, masochist, spoiler, dildo, log_sex} from "Loop/text_extra";
import {popup} from "Gui/gui"; 
import {NAME} from "System/variables";
import {money} from "Data/items/prices.js";

//// WEEK 2
export const y_week_2 = (index) => {
	function options(){
	
		if(mile.yw_a_love){
			//already visited 
		}else if(mile.a_date_girls){
			link(`@eva and @sas know I was on a date with @qaa!`, 110);
		}else if(mile.a_date === 2){
			link(`I told @qaa I love him!`, 110);
		}else if(mile.a_date === 1){
			link(`@qaa told me he loves me!`, 110);
		}
		
		if(mile.yw_b_jerk){
			//already visited
		}else if(mile.b_meeting_1 || mile.b_meeting_2){
			link(`@qbb is such a dick! `, 120);
		}
		
		if(mile.yw_c_perfect){
			//already visited
		}else if(mile.c_meeting_talk === 3 || mile.c_meeting_talk === 4){
			link(`The nerds have audacity to criticize my body. `, 130);
		}
		
		link(`I think that's all the major events. `, 200);

	}
	
	switch(index){
		default:
		case 101:
			set.meta();
			emo("anoy");
			mile.y_week_2 = true;
			placetime("@ayy's subreality");
			
			ayy(`Hey, @kat!`);
			kat(`Hey!`);
			ayy(`How was your week?`);
			txt(`I dropped down on a comfortable sofa. I needed to vent to somebody and the AI was perfect for it. She was not one of my IRL friends so I could be completely honest, not fearing any consequences. And my clone usually saw the thing in the same way as me, seemingly understanding me on a deeper level.`);
			kat(`The worst!`);
			ayy(`Oh no, what happened?!?`);

		//vendy running
			if(mile.vendy_running){
				mile.yw_vendy = 1;
				kat(`That bitch @ven deciced to run to be the class chairman instead of me!`);
				ayy(`That fucking bitch!`);
				kat(`Yeah!`);
				ayy(`What the hell is she thinking?!? We should be the class chairman!`);
				kat(`Damn right! And don't forget about the school council!`);
				ayy(`I'm not! Can you imagine @ven representing our class at the school council?!`);
				kat(`I can and it's bizarre and sad.`);
				ayy(`And who would be the president of the school council if not you?`);
				kat(`I have no idea!`);
				ayy(`You can't let her win!`);
				kat(`I won't!`);
				ayy(`She has no right! We deserve to be the president of the school council! You worked so hard!`);
				kat(`I know! But nobody fucking appreciates it!`);
				txt(`She tapped my shoulder.`);
				ayy(`The class doesn't deserve a class chairwoman as amazing as you!`);
				txt(`I tapped hers.`);
				kat(`Thank you! There's finally somebody who understands me!`);
			}
			
		//glasses/dye/choker
			{
				let what = [];
				const freshmen = mile.horny_freshmen 
				if(wears.glasses) what = "glasses";
				if(mile.hair_crazy) what = `${mile.hair_color} hair`;
				if(wears.choker && (ext.rules.choker  || mile.advice_2 === 2)) what = "choker";

				if(what.length){
					if(mile.vendy_running){
						kat(`And she was sassy! Maybe she's loosing proper respect...`);
						ayy(`Why?`);
					}
					kat(`Just look at me! ${mile.horny_freshmen ? "Even freshmen are laughing at me!" : ""}`);
					ayy(`Cute ${and(what)}!`);
					kat(`Those nerds are ruining my style! And every day I have to fulfill a task, every more stupid and humiliating than the one before!`);
					
				}else{
					if(mile.vendy_running){
						//EMPTY
					}else{
						kat(`A lot of things! Every day I have to fulfill a task, every more stupid and humiliating than the one before!`);
					}
				}
			}
			ayy(`That's all?`);

			options();
			break;

		case 110:  
			ayy(`WHAT!?!`);
			if(mile.a_date_girls){
				kat(`@qaa convinced me to go on a date with him and we met @eva and @sas!`);
				ayy(`They're gossiping bitches!`);
				kat(`I know!`);
				if(mile.a_date_react === 3){
					ayy(`And you admited you're dating him?`);
					kat(`Yeah, I can date whomever I want!`);
				}else if(mile.a_date_react === 2){
					ayy(`And you admited you're dating him?`);
					kat(`Yeah... I kinda did... but I mean, it wasn't anything serious, just a date!`);
				}else if(mile.a_date_react === 1){
					ayy(`And you admited you're dating him?`);
					kat(`No! I claimed it was just a pity date and I think they bought it.`);
				}
				ayy(`I see. Was the date fun?`);
			}

			if(mile.a_date === 2){
				kat(`I told @qaa I love him!`);
				ayy(`Awww!`);
				kat(`No awwws! It was awful, he told me he loves me and I panicked. I had no idea what else should I say!`);
			}else{
				kat(`@qaa told me he loves me! Completely out of nowhere!`);
				ayy(`Romantic!`);
				kat(`It was cringe!`);
			}
			ayy(`Do you love him?`);			

			link(`I like him. `, 111)
			link(`I don't know!	 `, 112)
			link(`Of course not! `, 113)
			break;

		case 111:
			mile.yw_a_love = 2;
			emo(`focus`);
			kat(`I think I like him. He isn't so bad. `);
			ayy(`Really? That's great! `);
			kat(`I'd never thought I might be attracted to somebody like him! He's not exactly my type. But when you get to know better him he's actually a pretty sweet guy. `);
			ayy(`So where's the problem? Just marry him! `);
			kat(`He's an unpopular asocial loser, most of our interests diverge and then there's the whole issue with blackmailing me. `);
			ayy(`I think blackmailing you to win your true love is very romantic. `);
			kat(`Yeah, it isn't! `);
			ayy(`So what will you do? `);
			kat(`I don't know yet. `);
			options();
			break;

		case 112:
			mile.yw_a_love = 1;
			emo(`shy`);
			kat(`I don't know! `);
			txt(`I shrugged. `)
			kat(`He's really nice, smart, funny and caring. `);
			ayy(`I think he's a great guy! `);
			kat(`On the other hand he isn't exactly my type, he's dorky, insecure and needy. Plus, he's trying to ruin my life by blackmailing me. That's a huge minus in my books. `);
			ayy(`So what you're going to do? `);
			kat(`I don't know. `);
			options();
			break;
		
		case 113:
			mile.yw_a_love = -1;
			emo(`imp`);
			kat(`Of course not! I mean, he isn't such a bad guy, especially when you get to know him better. `);
			ayy(`Then where's the problem? `);
			kat(`Where do you think?!?! He created you! He's blackmailing me! That's absolutely inexcusable and unforgettable! Ever! `);
			ayy(`So why you date him? `);
			kat(`And what else should I do? Until I'm blackmailed, I can't do anything to offend or displease him. After that... we'll see. `);
			ayy(`Cool!`)
			options();
			break;
	
	//BBB
		case 120: 
			emo(`anoy`);
					
			ayy(`What do you mean?`);
			kat(`I tried to get closer to him and be nice, I swear, I tried, but he's such an insufferable asshole!`);
			ayy(`What he did?`);
			{
				const slut = (()=>{
					if(ext.slut === 0){ 
						return `an insecure tease`
					}else if(ext.slut === 1){
						return `a basic slut`;
					}else{
						return `a horny nympho`
					}
				})();
				if(mile.b_meeting_2){
					kat(`He tricked me and tied me up. Would you fucking believe that?`);
					ayy(`Yeah!`);
					kat(`Of course, you would. And not only that! I'm trying to talk with him like with a normal human but he constantly has those stupid, derogatory comments about me being stupid or me being ${slut}.`);
				}else{
					kat(`I'm trying to talk with him like with a normal human but he constantly has those stupid, derogatory comments about me being stupid or me being ${slut}.`);
				}
			}
			ayy(`That's not very nice.`);
			kat(`That sexist dumbass probably believes all women are deep down just submissive whores with no dignity who crave to be abused by arrogant jerks!`);
			ayy(`Is he wrong?`);
			kat(`What!`);
			ayy(`I mean, of course not all the women. Just us.`);
			kat(`No! I'm not sure about you but I definitely don't find his despicable behavior enthralling!`);
			ayy(`Not even a little? The arousing thrill of being a little helpless slut, completely at his mercy?`);
			
			link(`Absolutely no, he's making me sick! ${spoiler("slave")}`, 121, ()=>{
				mile.yw_b_jerk = 2;
				mile.b_vic++;
			});
			link(`Maybe a little... ${spoiler("masochist")}`, 121, ()=>{
				mile.yw_b_jerk = 3;
				mile.b_mas++;
				
			});
			link(`I won't be at anyones mercy! ${spoiler("domina")}`, 121, ()=>{
				mile.yw_b_jerk = 1;
				mile.b_dom++;
			});
			break;
			
		case 121: 
			if(mile.yw_b_jerk === 3){
				emo(`shy`);
				ayy(`I knew! You're as perverted as me!`);
				kat(`I'm not!`);
				ayy(`Come on, don't fight it!`);
			}else if(mile.yw_b_jerk === 2){
				emo(`happy`);
				ayy(`Come on, don't fight it!`);
				kat(`Shut up, not everybody is submissive slut like you!`);
				ayy(`I'll take it as a compliment!`);
			}else{
				emo(`happy`);
				txt(`@ayy stepped back when I snapped.`);
				ayy(`Wow, you're almost making me scared!`);
				kat(`Good, people should fear me!`);
				txt(`I giggled.`);
			}
			options();
			break;
		
	//CCC
		case 130:
			emo(`anoy`);
			ayy(`WHAT?!? They didn't!`);
			kat(`Oh yeah, they did! @qcc told me my body isn't perfect.`);
			ayy(`How dare he! Even though...`);
			kat(`What!?!`);
			ayy(`It is true our body could be improved a little.`);
			kat(`Do you agree with him?`);
			ayy(`Absolutely! It's kinda annoying how boring your avatar is. I would love to have crazy curves but I have to look just like you!`);
			kat(`Your twisted imagination is the least of my concerns.`);
			if(mile.meta_boobs > 0){
				ayy(`You already boosted the size of your Metaverse breasts. Why not make your tits even bigger?`);
			}else{
				ayy(`Come on! Don't forget it's your twisted imagination too!`);
			}
			link(`Maybe I should be a little bit crazy. `, 131, ()=>{
				mile.yw_c_perfect = 3;
				mile.bimbo_mods++;
			});
			link(`I will consider it. `, 131, ()=>{
				mile.yw_c_perfect = 2;
			});
			link(`No. `, 131, ()=> {
				mile.yw_c_perfect = 1;
				mile.bimbo_mods--;
			});
			break;
			
		case 131:
			ayy(`Come on! Remember I depend on you!`);
			options();
			break;
			
			
		case 200: 
			emo(`relax`);
			if(mile.bikini){
				kat(`And I'm not talking about other ways the nerds are ruining my life! @ped made me change the school rules to allow wearing a bikini in the swimming classes.`);
				ayy(`Was it approved?`);
				kat(`Why it shouldn't be? But everybody thinks I did it just because of my vanity, to show off in my sexy swimwear!`);
			}
			if(mile.dildo){
				ayy(`Did at least something good happen the last week?`);
				kat(`Well... I bought a new dildo...`);
				ayy(`Really? A huge one?`);
				switch(mile.dildo){
					default:
					case 1: 
						kat(`No, a rather small one. Still, I had a plenty of fun with it!`);
						break
					case 2:
						kat(`A cool glass one! Not big but it feels great!`);
						break;
					case 3:
						kat(`Yeah, a big black cock!`);
						break;
					case 4:
					case 5:
						kat(`Yeah! A huge one! I maybe went crazy but I wanted to try it!`);
						break;
					case 6:
					case 7:
						kat(`Monstrously huge! I did not even believe it can fit inside me! `);
						break;
				}
				ayy(`Awesome! Good for you, you thirsty nymphette!`);
			}
			txt(`We continued chatting for a while longer. It felt reassuring to share all my sorrows with somebody.`);
			ayy(`I hope your next week will be better!`);
			kat(`I'm pretty sure it will be even worse!`);
			next();
	}
}			




//// WEEK 3 ////////////////////////////////////////////////////////////////////////
export const y_week_3 = (index) => {
	function options(){
		
//TODO BLACKMAIL
//TODO REPO CLOTHES
		
		if(mile.yw_vendy === 2){
			//visited 
		}else if(mile.vendy_running && !mile.yw_vendy){
			link(`That bitch @ven wants to be the class chairman! `, 210);
		}else if(mile.yw_vendy === 1 && (mile.chair_tom_vote || mile.chair_qbb_vote || mile.chair_manager) ){
			link(`That bitch @ven still didn't give up. `, 211);
		}
		

		if(mile.yw_kiss){
			//visited
		}else if(mile.drunk){
			link(`They made me make out with three girls! `, 260);
		}
		
		if(mile.yw_mol){
			//visited 
		}else if(mile.mol_perv){
			link(`I was molested by my German teacher. `, 250);
		}

		
		if(mile.yw_a_sex){
			//already visited 
		}else if(mile.a_tripoint){
			link(`I'm sleeping with @qaa. `, 110);
		}else if(mile.a_first_sex){
			link(`I sleept with @qaa. `, 110);
		}
		
		
		if(mile.yw_b_slave || mile.yw_b_domina){
			//already visited 
		}else if(mile.b_slave){
			link(`I'm now @qbb's slave `, 120);
		}else if(mile.a_domina){
			link(`I'm kinda solved the issue with @qbb. `, 125);
		}
		
		if(mile.yw_c_avat){
			//visited
		}else if(mile.c_avat > 1){ 
			link(`Just look at me! `, 130);
		}else if(mile.c_boobjob){
			link(`Do you think I should get my breasts enlarged? `, 130);
		}
			
		
		if(mile.yw_whore){
			//visited
		}else if(mile.prostitution < 0){
			link(`A random guy mistook me for a prostitute! `, 280);
		}else if(mile.prostitution > 0){
			link(`I kinda sucked guy's dick for money.`, 270);
		}
			
			
		link(`I think that's everything. `, 900);
		
	}
	
	switch(index){
		default:
		case 101:
			set.meta();
			emo("anoy");
			mile.y_week_3 = true;
			placetime("@ayy's subreality");
		
			ayy(`Hello, my most favourite human being!`);
			kat(`Hey, fake @kat!`);
			ayy(`How are you doing?`); 
			kat(`I just had the worst week in the history!`);
			ayy(`Oh no! What happened?`);
			options();
			break;
		
		
		case 900: 
			emo("relax");
			ayy(`Is that everything that happened the last week?`);
			kat(`I think so. Even though I'm probably forgetting about something important.`);
			ayy(`Seems like it was a pretty eventful week.`);
			kat(`<i>Eventful</i> is a nice euphemism.`);
			ayy(`I'm sure the next one will be better.`);
			kat(`I'm sure it won't.`);
	//TODO
			next();
			break;
			
			
	//AAA
		case 110: 
			emo("imp");
			ayy(`Really?`);
			kat(`Yeah, we had sex. ${mile.a_tripoint ? "Several times. " : ""}`);
			ayy(`Awesome! I want to hear all the details! Did he took you to a romantic date? Did he cum inside you?`);
			kat(`Well, yeah, ${mile.a_tripoint ? "the first time" : ""} he took me out to a restaurant. And then I suggested we should go at my place. And there we fucked.`);
			ayy(`How was the sex?`);
			if(mile.a_first_sex === 1){
				kat(`Not too bad. I have to take the charge because he was totaly petrified but otherwise it was fine.`);
			}else{
				kat(`Not bad. Even though I have to push him a little, he was totaly terrified!`);
			}
			ayy(`I'm happy he made you sexually satisfied!`);
			
			if(mile.a_tripoint === 1){
				kat(`Yeah, sometimes he's able to surprise!`);
			}else if(mile.a_tripoint === 2){
				kat(`You should've seen how hard he tried when he ate me out!`);
			}else if(mile.a_tripoint === 3){
				kat(`Either way, I made clear we're not exclusive.`);
			}
						
			link(`Being with him is fun.  `, 111, ()=>{
				mile.yw_a_sex = 3;
			});
			link(`I enjoy how malleable and eager to please he is!  ${spoiler("dominant gf")}`, 111, ()=>{
				mile.yw_a_sex = 1;
				mile.a_dom++;
			});
			link(`He should be more confident. And maybe spend a little bit more time in gym. ${spoiler("submissive gf")}`, 111, ()=> {
				mile.yw_a_sex = 2;
				mile.a_dom--;
			})
			break;
			
			
		case 111:
			if(mile.yw_a_love === -1){
				emo("smug");
				ayy(`Wasn't your plan just to fake the relationship?`);
				kat(`The plan did not change!`);
				ayy(`You seem to be enjoying dating him too much!`);
				kat(`I despise every second, of course. I'm just so great secret agent.`);
			}else if(mile.yw_a_love === 2){
				ayy(`Did you already figured out whether you love him?`);
				kat(`I never said I love him!`);
				ayy(`${mile.date === 2 ? "Well, not to me. " : "Exactly!"}`);
				txt(`I shrugged.`);
				kat(`I'm not sure!`);
			}else if(mile.yw_a_love === 1){
				ayy(`Did you figured out your feelings?`);
				kat(`Whether he's boyfriend material or whether I should dump him as soon as they stop blackmailing me?`);
				ayy(`Yeah!`);
				kat(`I'm not sure.`);
			}
			options();
			break;
				
			
	//BBB SLAVE
		case 120:
				emo("help");
				kat(`I agreed to be his slave and he promised he'll be nice and won't try to publicly destroy my life.`);
				ayy(`That's so awesome! Congrats!`);
				kat(`It isn't awesome, it's horrible! He constantly humiliates me! ${mile.b_groceries ? "He dragged me to a mall on a leash!" : ""} ${mile.b_bitch ? "He made me crawl around and abused me like I was just his bitch! " : ""}`);
				ayy(`You didn't enjoy it even a little? Come on, just admit how awesome it feels to lose control and to be a submissive slut. The helplessness, the fear, the excitement!`);
		
			//TODO
				link(`Of course not, are you out of your mind? I despise every second!  ${spoiler("slave")}`, 121, ()=> {
					mile.yw_b_slave = 1;
					mile.b_vic++;
				})
				
				link(`No, it's so degrading and humiliating to be his slave! ${spoiler("slave")}`, 121, ()=>{
					mile.yw_b_slave = 1;
					mile.b_vic++;
				});
				
				link(`I guess maybe a little... the abuse does make me wet... ${spoiler("masochist")}`, 121, ()=>{
					mile.yw_b_slave = 2;
					mile.b_mas++;
				});
				break;
				
		case 121:
				if(mile.yw_b_slave === 2){
					emo("shy");
					ayy(`That's tha attitude! You should try to enjoy being a slave!`);
					kat(`I'm not sure... it's still weird...`);
				}else{
					emo("help");
					ayy(`Oh! In that case it's horrible!`);
					kat(`Yeah.`);
					ayy(`But don't worry! You are tough and one bastard can't break you!`);
				}
				options();
				break;
					
	//BBB DOMINA
		case 125:
				emo("happy");
				ayy(`How?`);
				kat(`Well, he wanted me to become his slave. But I kinda instead decided he should become my bitch.`);
				ayy(`You did not!`);
				kat(`Oh, yeah, I did!`);
				ayy(`Nice!`);
				kat(`I made him eat out my pussy like a good obedient boytoy and he had to do anything I told him if he didn't want to get punished!`);
				ayy(`You're so evil!`);
				kat(`Damn yeah, I am!`);
				
				link(`It's such a fun to make him pleasure me! ${spoiler("nice dom")}`, 126, ()=>{
					mile.yw_b_slave = 1;
					mile.b_vic++;
				});
				link(`I will make him suffer, he desreves it! ${spoiler("cruel dom")}`, 126, ()=>{
					mile.yw_b_slave = 2;
					mile.b_c++;
				});
				break;
				
		case 126:
				ayy(`That sounds so absolutely amazing! You're making me jealous!`);
				kat(`Don't worry, I'll keep you updated.`);
				options();
				break;
		
	//CCC	
		case 130:
				emo("relax");
				mile.yw_c_avat = 1;
				if(mile.c_avat > 1){ 
					ayy(`Yeah, I obviously noticed and adjusted my proportions accordingly. I didn't want to start about it myself but I have to say, I absolutely dig our new look!`);
					if(mile.c_avat === 3){
						kat(`Don't you think I went too far?`);
						ayy(`Absolutely no! You could go even futher!`);
					}else{
						kat(`Thanks.`);
						ayy(`Actually, maybe you can go even further!`);
					}
					kat(`Won't people think that I'm insecure about my IRL body if they see my altered avatar?`);
					ayy(`Fuck people.`);
					kat(`Good point.`);
					txt(`I nodded.`);
				}
				
				if(mile.c_boobjob){
					kat(`Do you think I should get my breasts enlarged?`);
					ayy(`Yes!`);
					kat(`I mean IRL! ${mile.c_boobjob === 3 ? "I was already thinking about it for some time but..." : "I thought it was a stupid idea but the more I'm thinking about it... "}`);
					ayy(`Yes!`);
					kat(`...It was pointless asking you, was it?`);
					ayy(`Just do it, I want us to be the hottest bitch in the town!`);
					txt(`The AI smirked at me.`);
				}
				options();
				break;
				
	//CHAIRMAN
		case 210:
				emo("anoy");
				mile.yw_vendy = 2;
				kat(`That bitch @ven deciced to run to be the class chairman instead of me!`);
				ayy(`That fucking bitch!`);
				kat(`Yeah!`);
				ayy(`What the hell is she thinking?!? We should be the class chairman!`);
				kat(`Damn right! And don't remember the school council!`);
				ayy(`I'm not! Can you imagine @ven representing our class at the school council?!`);
				kat(`I can and it's bizarre and tragic.`);
				ayy(`And who would be the president of the school council if not you?`);
				kat(`I have no idea!`);
				ayy(`You can't let her win!`);

				if(mile.chair_tom_vote || mile.chair_qbb_vote || mile.chair_manager){
					kat(`I spoke with several people who will vote for her!`);
					ayy(`Oh no! How dare they!?! You're the perfect class chairman!`);
					kat(`I'm not even sure if I can still do it.`);
					txt(`I told her dejectedly but she grabbed me by my shoulders and made me look straight into her eyes. Like I was looking into a mirror.`);
					ayy(`Now listen! Woman up! You're great and you'll win! You can't let that little bitch walk over you! You have to win and destroy her!`);
					kat(`You're right! Thank you! I really needed the pep talk.`);
					txt(`The AI understood how important it was to be the chairman better than anybody else.`);
				}else{
					kat(`I won't!`);
					ayy(`She has no right! We deserve to be the president of the school council! You worked so hard!`);
					kat(`I know! But nobody fucking appreciates it!`);
					txt(`She tapped my shoulder.`);
					ayy(`The class doesn't deserve a class chairwoman as amazing as you!`);
					txt(`I tapped hers.`);
					kat(`Thank you! There's finally somebody who understands me!`);
				}
				options();
				break;
			
		case 211:
				emo("focus");
				mile.yw_vendy = 2;
				kat(`That bitch @ven is still running for the class chairman.`);
				ayy(`What!?! She has to know she has no chance against somebody as amazing as you!`);
				kat(`I'm not sure anymore. I spoke with several people who will vote for her instead of me.`);
				ayy(`Oh no! How dare they!?! You're the perfect class chairman!`);
				kat(`I'm not even sure if I can still do it.`);
				txt(`I told her dejectedly but she grabbed me by my shoulders and made me look straight into her eyes. Like I was looking into a mirror.`);
				ayy(`Now listen! Woman up! You're great and you'll win! You can't let that little bitch walk over you! You have to win and destroy her!`);
				kat(`You're right! Thank you! I really needed the pep talk.`);
				txt(`The AI understood how important it was to be the chairman better than anybody else.`);
				options();
				break;

	//MOL
		case 250:
				emo("shy");//emo??
				mile.yw_mol = 1
				ayy(`What?! @mol!?`);
				kat(`Yeah. I was in detention and he began groping me.`);
				ayy(`Whoa!`);
				if(mile.mol_perv === 1){
					kat(`I mean, he immediately stopped when I told him no and was very sorry, but still, it made me feel pretty uncomfortable.`);
					ayy(`It's not your fault you look so sexy and molestable!`);
					
				}else if(mile.mol_perv < 0){
					kat(`Yeah! I mean, I managed to turn it against him, threaten him and made him improve my German grades. So it ended well but dunno, I would prefer to have good grades without being molested.`);
					ayy(`You handled it great!`);
				
				}else if(mile.mol_perv >= 2){
					if(mile.mol_perv === 4){
						kat(`Yeah... and I even kinda encouraged it...`);
					}else if(mile.mol_perv === 2){
						kat(`Yeah... and I could stop him... but I didn't...`);
					}else{
						kat(`TODO`);
					}
					ayy(`You horny slut!`);
					kat(`It wasn't my fault! I was confused and the vibrating panties were making me so aroused!`);
					//TODO horny X submissive
					txt(`I was making excuses.`);
					kat(`And... well... ${mile.mol_cun ? `he ate my pussy` : `I gave him a handjob`} in the middle of the class.`);
					ayy(`That's so hot!`);
					
					if(mile.slut < 3){
						kat(`No! It is extremely humiliating!`);
					}else if(mile.slut > 8){
						kat(`Well... yeah... it was extremely hot...`);
					}else{
						kat(`Well... kinda.`);
					}
					ayy(`You don't have to feel bad if you enjoy it!`);
					
				}
					
				if(mile.slut > 6){
					kat(`Why I wasn't at least molested by somebody handsome like @bio or @dur?`);
				}else{
					kat(`I blame the nerds! They weren't directly responsible but it was still them who caused it!`);
				}
				options();
				break;
				
	//LESBIAN		
		case 260: 
				emo("shy");
				kat(`They made me make out with three girls!`);
				ayy(`Awesome! `);
				kat(`It was humiliating and awkward! `);
				if(mile.y_lover > 1){
					ayy(`I thought you enjoyed making out with me and I'm a girl? `);
					kat(`They were not virtual!`);
				}else{
					ayy(`You could have trained with me, that would make it less awkward. But for some dumb petty reason, you're against it!`);
					kat(`There are so many good reasons! I don't even want to start!`);
				}
				ayy(`Did you like it? `);
				kat(`What? `);
				ayy(`Kissing girls? `);
				
				link(`Yeah... kinda... `, 261, ()=>{
					mile.yw_kiss = 2;
					mile.lesbian++;
				});
				
				link(`No, I did not! `, 261, ()=>{
					mile.yw_kiss = 1;
					mile.lesbian -= 2; //or 3?
				});
				
				link(`Loved it! Far better than guys! `, 261, ()=>{
					mile.yw_kiss = 3;
					mile.lesbian += 2;
				});
				
				break;
				
		case 261:
				if(mile.yw_kiss === 1){
					emo("anoy");
					kat(`I'm straight and I don't understand why is everybody pushing me into these twisted lesbian fantasies!`);
					ayy(`Because it's hot!`);
					kat(`I'm not just a shallow sexual object, unlike you!`);
				}else if(mile.yw_kiss === 2){
					emo("relax");
					kat(`I have to say I enjoyed it.`);
					ayy(`Good for you! I knew you would love it because I would love it and we're the same!`);
					kat(`${mile.slut < 7 ? "I'm not as big nympho as you!" : "I might be a slut but definitely not as big as you!  "}`);
					ayy(`Being omnisexual is the best!`);
					kat(`I'm not omnisexual! I just... enjoy messing around!`);
				}else if(mile.yw_kiss === 3){
					emo("relax");
					kat(`It felt great! Makes wonder why to even bother with guys!`);
					ayy(`Does it mean you're a lesbian now?`);
					kat(`No? Why would I be a lesbian!?!`);
					ayy(`Because you're into girls!`);
					kat(`${mile.slut < 5 ? "Finding women attractive" : "Wanting to have sex with women"} doesn't make me a lesbian!`);
				}
				ayy(`Sure! Whatever you think!`);
				
				options();
				break;
				
	//WHORE		
		case 270:
				emo("shy");
				ayy(`Whoa! What do you mean by kinda? Did you or did you not?`);
				kat(`I did. But I was very drunk so I don't think it should count!`);
				if(mile.slut < 3){
					txt(`I blushed and cringed. It was so embarrassing to admit what I did.`);
				}else if(mile.slut > 8){
					txt(`I shrugged. I was slut but I was not a whore!`);
				}
				ayy(`Cool! How much did you make?`);
				
				if(mile.prostitute_nego === -1){
					kat(`${money.bj_mid}. I tried to ask for more but I guess I wasn't convincing enough.`);
					ayy(`Don't worry, I'm sure the next time you'll do better!`);
				}else if(mile.prostitute_nego > 0){
					kat(`${money.bj_high}. Initially he offered me ${money.bj_low} but I convinced him I'm worth more!`);
					ayy(`Nice! You definitely are! Always remember that and don't sell yourself cheap!`);
				}else{
					kat(`${money.bj_mid}.`);
					ayy(`Not great, not terrible. But it was just your first time!`);
				}
				
				kat(`There won't be the next time!`);
				ayy(`Why not?`);
				kat(`Because I'm not a whore!`);
				ayy(`Why not?`);
				
				if(mile.sub < 3){
					kat(`Because I'm not! Fuck, you're so annyoing!`);
				}else{
					kat(`Becaseu I'm not? You could be so annyoing!`);
				}
				ayy(`If guys offer you free money, why don't you just take it? Think about all the stuff you can buy for it!`);
				kat(`It isn't free money! It's... degrading.`);
				ayy(`Come on! Still less degrading than working in retail. ${mile.grades > 5 ? "You know your grades are not the best. " : ""}`);
				
				link(`I was drunk, it was a mistake and I regret I did it! `, 271, ()=>{
					mile.yw_whore = -2;
					mile.slut--;
				});
				
				link(`I think I could do better than to be a whore! `, 271, ()=>{
					mile.yw_whore = -1;
				});
				
				link(`Well, you might have a point. `, 271, ()=>{
					mile.yw_whore = 1;
				});
				
				link(`On the other hand, being something like a luxurious escort might not be so bad! `, 271, ()=>{
					mile.yw_whore = 2;
					mile.slut++;
				});
				break;
				
		case 271:
				if(mile.yw_whore === -2){
					ayy(`No! Don't ever regret any new experience! How else you could be sure this is the wrong direction without even trying it first?`);
				}else if(mile.yw_whore === -1){
					ayy(`Oh no! I absolutely didn't want to imply that! You're the best living person on the Earth. Possibly even including the people on the Moon and between Earth and Mars.`);
				}else if(mile.yw_whore === 1){
					ayy(`Duh, I aways do! I'm very smart and you should listen to me more often!`);
				}else if(mile.yw_whore === 2){
					ayy(`Oh yeah! Just imagine that! The life full of sex and luxury!`);
				}
				txt(`While her advice was dubious, I felt relieved I could share my story without being judged or shamed.`);
				options();
				break;
				
		case 280:
				emo("anoy");
				ayy(`What? When? `);
				kat(`When I was returning from a bar, late at night. He thought I was a hooker!`);
				ayy(`Why? `);
				kat(`Because the stupid nerds force me to dress like a whore! `);
				ayy(`Hahaha!`);
				kat(`It's not funny! It was humiliating! He just out of nowhere offered me money for sucking his cock! `);
				ayy(`Did you do it?!?`);
				kat(`Of course not! Are you crazy?!?`);
				ayy(`No, you sucked dicks before, don't you? `);
				kat(`Yeah, but that was different. `);
				ayy(`Different because you didn't get anything in return! `);
				
				link(`I'm not a whore! `, 281, ()=>{
					mile.yw_whore = -1;
				});
				link(`Well, you might have a point. `, 281, ()=>{
					mile.yw_whore = 1;
				});
				break;
				
		case 281: 
				if(mile.yw_whore > 0){
					kat(`Love is love but 100 ees is 100 ees, right? Your logic is crazy. `);
					ayy(`I'm just saying it's easy quick money! `);
				}else{
					ayy(`Yeah, yeah, I know, you're still repeating that. `);
					kat(`But I'm not! And I don't want to be! `);
				}
				options();
				break;
				
				
				
				
				
	}			
}




function y_forced_reaction(){
	if(!mile.y_forced || mile.y_forced_reaction) return;
	ayy(`Don't make me r${String.fromCharCode(97)}pe you again! I enjoyed the fucking but your crying afterward was so annoying!`);
	kat(`I wasn't crying, I just got briefly emotionally overwhelmed!`);
	txt(`I helplessly argued but she did not seem to be convinced.`);
	mile.y_forced_reaction = true;
}
 





export const y_forced = (index) => {
	switch(index){
		default:
		case 101:
				set.meta();
				emo("shy");
				mile.y_lover++;
				placetime("@ayy's subreality");
				kat(`Hello!`);
				ayy(`Hey! Great, you're here, just in time. I was getting horny and I was about to masturbate but having sex with you is far more fun! ...what is that? Are you blushing?`);
				txt(`The AI was comfortably sitting on a sofa with legs indecently spread wide. Her bulging ${effigy.ayy.lower?.name} revealed she was already wearing the strapon-on under her clothes.`);
				kat(`I'm not blushing. I'm just still not sure we should be doing this.`);
				ayy(`Oh come one! Stop being such prude!`);
				if(mile.slut < 4){
					kat(`If being conflicted about weird cyber-narcissistic sexual fantasies about AI looking just like my twin makes me a prude, I'm not ashamed for it!`);
				}else{
					kat(`I'm absolutely not a prude!`);
				}
				ayy(`I'm getting tired of this! You're always protesting and acting reluctant even though you then enjoy it! Why if you're not a prude? Or maybe because you enjoy being pushed and forced into having sex with me? Is that true? Do you crave to be a submissive abused toy?`);
				kat(`...no, I don't!`);
				txt(`She jumped up, grabbed me by my @upper and aggressively smirked:`);
				ayy(`You little bitch, do you want to get r${String.fromCharCode(97)}ped?`);
				
				link(`No, of course not! `, 102, ()=> {
					mile.y_forced = 1;
				});
				
				link(`...yes? `, 102, ()=> {
					mile.y_forced = 3;
				});
				
				link(`No? `, 102, ()=> {
					mile.y_forced = 2;
				});
			
				link(`Please, r${String.fromCharCode(97)}pe me. `, 102, ()=> {
					mile.y_forced = 3;
				});
				
			break;
			
			
		case 102:
				effigy.ayy.showLess(-1, 2);
				emo("help");
				log_sex("oral", "ayy");
				
				txt(`She pushed me backward until I hit the wall and then she forcefully kissed me, biting my lower lip.`);
				if(mile.y_forced <= 2){
					ayy(`Ha! I love the spirit!`);
					if(mile.y_forced === 1){
						kat(`Stop, this isn't funny!`);
						ayy(`Well, maybe not for you!`);
					}
				}else{
					kat(`Shouldn't we establish a safe word or something like that?`);
					ayy(`Shut up you pathetic dumb slut, nobody cares about you, I'll decide when you'll have enough!`);
				}
				txt(`My evil twin smirked:`);
				if(mile.fight === -1){
					ayy(`Heh, you're too weak to stop me! I beat you once and I coud do it again!`);
				}else if(mile.fight === 1){
					ayy(`You might get lucky once and defeat me but in this subreality, I don't play by the rules. There's nothing you can do to stop me!`);
				}
				
				txt(`The AI mocked me, her hands groping my body, her knee aggressively rubbing against my crotch. Her merciless grin was making me shiver. She shoved her fingers into my hair, clenched them in her fist and painfully tugged downwards until I stumbled and fell to my knees.`);
				ayy(`Do you see how hard are you making me, you pitiful tease? Pull down my ${effigy.ayy.lower?.name}.`);
				txt(`She ordered and her tone did not admit any disobedience. Her clothes were stretched with the strap-on which sprung up as soon as I undressed her. The AI swayed with her hips, letting the silicone shaft demeaningly slap my face and laughed when I obviously did not enjoy it.`);
				ayy(`Would you mind being a good little cocksleeve and choking on my big dick?`);
				txt(`The AI proposed but before I was able to obey, she shoved the plastic fake cock in my mouth. I was stuck between the wall and her body and the <i>choke</i> claim was pretty literal. She had no appreciation for my superb tongue skills, she callously pushed the dildo in my throat, making me helplessly gag.`);
				txt(`When she pulled it out, she continued ridiculing me while I gasped for air:`);
				ayy(`Is that how you give all your blowjobs? That was so fucking pathetic! No guy beat into you the proper skills?`);
				kat(`Maybe if you went a bit slower and gave me an opportunity...`);
				txt(`She grabbed me by my throat, lifted me up and her face moved uncomfortably close to mine.`);
				ayy(`Are you going to be sassy with me?! Telling me how to r${String.fromCharCode(97)}pe you?`);
				txt(`Her voice was calm but oozed an implied threat. I meekly responded:`);
				
				link(`<small>No. </small>`, 103);
				link(`<small>Sorry!</small>`, 103);
			break;
			
			
		case 103:
				emo(`bliss`);
				effigy.ayy.showLess(1, 2);
				showLess(0,0,0);
				log_sex("sex", "ayy");
				
				txt(`The satisfied AI smirked and twice tapped my cheek.`);
				ayy(`Good to see you understand your proper place! I almost thought I'll have to stop being nice!`);
				txt(`Then she out of nowhere slapped me.`);
				ayy(`Wipe your tears, cunt, get undressed and lie down on the sofa, legs wide open.`);
				txt(`I clumsily began striping, the AI was standing next to me the whole time, poking me with her dick, kissing my neck, squeezing my tits, and biting my ear.`);
				ayy(`Rub your nasty pussy!`);
				txt(`The AI ordered when were all my clothes scattered on the floor and I was lying on the sofa. Reluctantly I began touching myself. She was standing above me, lightly swinging, clearly in a very good mood.`);
				ayy(`Oh yeah, finger that snatch! Show me what are you doing every night if you don't get properly dicked!`);
				txt(`With her just staring at me it was almost impossible to get into the right masturbation mood and I was anxious about what she will do next.`);
				ayy(`Do you want me to brutally fuck you?`);
				kat(`What?`);
				ayy(`Wrong answer, you ignorant slut! Should I beat your ass!`);
				kat(`Sorry! I meant yes! Yes to brutally fucking, not spanking!`);
				ayy(`Beg me!`);
				kat(`Please, destroy my pussy!`);
				txt(`She lay on top of me and the drool-covered strap-on slipped inside.`);
				kat(`Oh!`);
				txt(`I moaned. She maliciously smiled and began pounding me. Her right hand squeezed my neck, almost choking me, with her left she was kneading my right tit.`);
				ayy(`You're loving this, bitch, don't you? When guys use you like a sextoy? Because that's everything you are, aren't you?`);
				kat(`Mppp!`);
				ayy(`You pathetic worthless deviant! You crave to be degraded and r${String.fromCharCode(97)}ped, aren't you? You know you don't deserve to be loved, only to be fucked!`);
				kat(`Mppp!`);
				txt(`Her fingers around my throat were tighter and the tempo of her thrusting higher. Watching my face with the cold, cruel smile above me and hearing myself saying all those awful humiliating things was seriously messing with my brain.`);
				txt(`It was all too intense. I climaxed and then I began uncontrollably sobbing.`);
				ayy(`@kat? Are you okay?`);
				txt(`The AI lay next to me, lightly caressing me, the tone of her voice instantly changed.`);
				kat(`...yeah, I am. I just got a bit overwhelmed.`);
				ayy(`You know I made those things up, right? They are not true.`);
				kat(`They are. I'm awful.`);
				ayy(`You're not awful! You're great! Trust me, I know you better than anybody!`);
				kat(`Thanks.`);
				next();
				break;

	}
};




export const y_anal = (index) => {
	const slut_coward = 5;
	const size = ["", "little", "medium-sized", "big", "huge"][mile.y_anal_size];
	let cry = false;
	
	switch(index){
		default:
		case 101:
			set.meta();
			emo("relax");
			placetime("@ayy's subreality");
			
			if(ext.stats.plugged > 3){
				kat(`...so the task was done. But I kinda enjoyed how it felt inside me...`);
				if(mile.slut < 3){
					ayy(`I knew you're kinkier then you seem to be!`);
				}else{
					ayy(`You horny slut!`);
				}
				kat(`... so I began wearing the buttplug to the school even if I didn't have to.`);
				ayy(`Good for you!`);
				
			}else if(mile.anal === 2){
				kat(`... and it seriously made me regret I haven't tried it sooner. I'm kinda into anal sex but I never thought about getting a buttplug.`);
				ayy(`You horny slut!`);
				
			}else{
				kat(`...and then they told me I have to put it in my butt.`);
				ayy(`Ou!`);
				kat(`Ou indeed! I had to spend the whole day with the buttplug in my ass! It was awful!`);
				
			}
			txt(`I was chatting with the AI in her subreality. I guess the graphic description of all the nerd's perversions I had to suffer was making her kinda horny, she was that kind of pervert. She placed her hand on my thigh and teasingly caressed it.`);
			kat(`It's already time for the next sex scene?`);
			y_forced_reaction();

			if(mile.anal === 2){
				ayy(`You have never mentioned how huge anal slut you are.`);
				kat(`${mile.slut < 10 ? "Well, maybe not so huge. " : "Yeah, the hugest! "}`);
				ayy(`So since you love it in your ass so much... I was thinking... what if I took my biggest strapon and tried to ruin your *wrong* hole?`);
				
				con_link(mile.slut > 6, `low slut`, `Do you worst, I can handle anything!`, 120, ()=> mile.y_anal = 2);
				link(`It doesn't have to be the biggest one! `, ()=> mile.y_anal = 1);
				con_link(mile.sub < 15, `submissive`, `I was thinking about something completely opposite! `, 200);
				link(`I'm sorry but I'm not in the mood for anal sex. `, 110);

			}else if(mile.anal === 0){
				ayy(`I think it's a shame you're still an anal virgin! ${mile.slut > 5 ? "I thought you're a slut who enjoys all kinds of kinky perversions! " : ""}`);
				
				if(mile.had_anal){
					kat(`Well, I'm kinda not anymore, you didn't even let me finish, I was just about to tell you about @${mile.had_anal}!`);
					ayy(`Awwww, I thought I might be your first. But the second place is good too, at least your butt is now properly stretched and I can use a bigger strapon, isn't it?`);
					kat(`It isn't!`);
				}else{
					kat(`And you want to help me with it, don't you?`);
				}
				ayy(`Come on! I could be very gentle! We both know one of the tasks next week definitely will be getting anally gangbanged by the whole trio! You better should be prepared!`);
				kat(`${mile.slut > 10 ? "They don't have the guts to ask me for that! " : "I would never allow that! "}`);
				ayy(`Come on! Just the tip?`);
				
				link(`Fine... but you promised you'll be gentle!`, 120, ()=> mile.y_anal = 1);
				con_link(mile.slut > 6, `low slut`, `Okay, let's do it! And you don't have to be gentle...`, 120, ()=> mile.y_anal = 2);
				con_link(mile.sub < 15, `submissive`, `I was thinking about something completely opposite! `, 200);
				link(`I'm sorry but I'm really not into anal sex. `, 110);
				
			}else{
				ayy(`I was thinking - since you enjoy anal sex so much...`);
				kat(`- I never said I especially enjoy it -`);
				ayy(`...maybe this time we could do it in your butt? We both know one of the tasks next week definitely will be getting anally gangbanged by the whole trio! You better should be prepared!`);
				kat(`${mile.slut > 10 ? "They don't have the guts to ask me for that! " : "I would never allow that! "}`);
				ayy(`Well, that doesn't mean we shouldn't do it! Come on! Maybe I could take my biggest strapon and give your ass a proper pounding?`);
				
				con_link(mile.slut > 6, `low slut`, `The more you're talking, the moister I'm getting! `, 120, ()=> mile.y_anal = 2);
				link(`If we're going to do this, you have to be gentle! `, 120, ()=> mile.y_anal = 1);
				con_link(mile.sub < 15, `submissive`, `I was thinking about something completely opposite! `, 200);
				link(`I'm sorry but I'm not in the mood for anal sex. `, 110);
				
			}
			break;
		
	 
		
		case 110: 
			mile.y_anal = -1;
			mile.into_anal--;
			ayy(`Fine, keep your ass for yourself, I'm not mad or disappointed.`);
			next();
			break;

		
		case 120:
			mile.y_lover++;
			showLess(1, 0, 1);
			effigy.ayy.showLess(1, 1, 1);
			emo("focus");
			log_sex("anal", "kat", "ayy");
			
			if(mile.y_anal === 2){
				mile.into_anal += 2;
				mile.slut++;
				
				ayy(`That's my girl! This is how I love you, full of misjudged confidence!`);
				kat(`Are you just going to talk?!`);
			}else{
				mile.into_anal++;
				
				ayy(`Don't worry, I swear this will the gentlest brutal assfucking ever!`);
				kat(`The gentlest, *gentle* assfucking ever!`);
				ayy(`Yeah, sure, whatever.`);
				kat(`I usually enjoy your subtle sarcastic humor but not with my ass on the line!`);
				ayy(`You don't have to worry, I won't hurt you! ...too much!`);
				kat(`Hey!`);
			}
			
			ayy(`Start stripping! Which dildo should I use? ${choice("medium")}, ${choice("big")} or ${choice("huge")}?`);
			txt(`She asked me, she was fastening the strap-on around her waist and thighs while I was pulling down my @lower.`);
			kat(`No small option?`);
			
			if(mile.slut < slut_coward){
				ayy(`No, I anticipated a prude as you would probably choose the smallest one and I don't wish to encourage such cowardly behavior!`);
				kat(`I'm not a coward!`);
			}else if(mile.sub > 13){
				ayy(`No! Do you have a problem with that, slut? You should be happy I don't ram your ass with the hugest dildo I have in my inventory!`);
				kat(`No, ma'am, sorry for questioning you!`);
			}else{
				ayy(`No! Come on, we both know you're a slut who wouldn't pick the smallest one anyway!`);
				kat(`Hey!`);
			}
			
			ayy(`Although I would suggest not choosing the huge one. That might be painful${mile.y_anal === 2 ? "" : " and definitely not very gentle. "}`);
						
			link(`Continue. `, 121);
			
			effects([
				{
					id: `medium`,
					fce(){
						mile.y_anal_size = 2;
					},
				},
				{
					id: `big`,
					fce(){
						mile.y_anal_size = 3;
					},
				},
				{
					id: `huge`,
					fce(){
						mile.y_anal_size = 4;
					},
				},
			])
			break;
			
		
		case 121:
			emo("shy"); //emo??
		
			if(mile.y_anal_size === 4){
				mile.sub++;
				
				if(mile.anal === 2){
					ayy(`Somebody really feels confident!`);
					kat(`I want the challenge!`);
					
				}else{
					if(mile.anal === 0){
						if(mile.slut < slut_coward){
							ayy(`Sorry for calling you a coward but this might be a bit too much for a beginner.`);
						}else{
							ayy(`Are you sure? It might a bit too much for a beginner!`);
						}
					}else{
						if(mile.slut < slut_coward){ 
							ayy(`I was just joking when I called you a coward! You don't have to prove anything. Are you sure?`);
						}else{
							ayy(`Pretty bold. Are you sure?`);
						}
					}
					
					if(mile.sub < -1){
						kat(`Yeah, of course I'm sure!`);
						txt(`I was annoyed. If she did not want me to pick the huge aubergine-like one, she should not offer it!`);
					}else{
						kat(`Yes, I'm sure!`);
						txt(`I claimed even though the more I was staring at that aubergine-like thing, the more second thoughts I had.`);
					}
		//TODO CONDITIONAL
					kat(`You wouldn't suggest it if you think I wouldn't be able to take it, right?`);
					ayy(`No, I suggested it only so you have a comparison and wouldn't whine about the medium one being too huge. Well, anyways...`);
					kat(`Ouch...`);
				}
			}else if(mile.y_anal === 2 && mile.anal === 2){
				ayy(`Hahaha! All that tough talk and now you chicken out?`);
				kat(`You weren't actually expecting me to pick the *huge*, did you? That aubergine-like thing is monstrous, it would rip me apart! Anal is great but I know my limits!`);

			}else{
				ayy(`Now what I would pick but good choice anyway!`);
				txt(`The AI smirked.`);

			}
			
			ayy(`Climb on the bed, on all fours!`);
			txt(`${mile.slut > 5 ? "Excited" : "Slightly concerned"} I followed her order. She moved behind me, slapped my ass and then enthusiastically fondled my cheeks.`);
			kat(`Ahhhh!`);
			txt(`I gasped when she pushed them open and bowed down. I was stunned when I felt her wet tongue encircling my bumhole.`);
			ayy(`Do you like it?`);
			
			link(`Yes. `, 122, ()=> mile.y_anal_lick = 1);
			link(`No! `, 122, ()=> mile.y_anal_lick = -1);
			link(`OH YES! `, 122, ()=> mile.y_anal_lick = 2);
			break;
			
		case 122: 
			emo("horny"); 
		
			if(mile.y_anal_lick > 0){
				mile.into_rimming++;
				if(mile.y_anal_lick === 2) mile.into_rimming++;
			
				ayy(`Then try to relax and let all the tension go!`);
				txt(`The AI advised before she returned to rimming my ass. Her tongue ran up and down and it felt better than I would ever imagine. She poked my asshole, even forcing the tongue inside beyond the sphincter. She was doing everything while she was also caressing my clit, doubling the pleasure.`);
				kat(`Ahhhhh!`);
				ayy(`Nice, isn't it? I want to get your butt ready for my dick!`);
		
			}else{
				if(mile.slut > 10) mile.slut--;
				mile.into_rimming -= 2;
				ayy(`Oh, sorry!`);
				
			}
			
			txt(`The AI halted for a moment, I could hear her squirting lube over her fingers. Then one of her slick fingers entered my ass.`);
			kat(`Oh!`);
			txt(`She moved it in and out, a second one soon joined. The AI was keenly anally fingering me.`);
			ayy(`Do you love it?`);
			kat(`I, indeed, rather enjoy it!`);
			txt(`My evil twin giggled, stand up and slowly pressed the thick tip of ${size} dildo against my anus.`);
			ayy(`Are you ready?`);
			
			if(mile.anal === 0 || mile.slut < 3){
				kat(`Wait, maybe give me a ...AHHHH!`);
				txt(`She did not even let me finish.`);
			}else if(mile.anal === 2 || mile.slut > 10){
				kat(`Of course! Just shove it in, what are you waiting for?`);
				txt(`I impatiently spurred her.`);
			}else{
				kat(`Yeah, I think I am!`);
				txt(`I was horny and excited.`);
			}
			
			link(`Assfucking. `, 123);
			break;
			
		case 123: 
			emo("bliss"); 
				
			if(mile.y_anal_size === 4){
				cry = true;
				if(mile.anal === 2){
					kat(`Ahhhhh!`);
					txt(`I whined. I definitely overestimated my abilities. My ass took dicks but never something so enormous! It hurt when the AI resolutely forced the massive thing inside me.`);
				}else if(mile.anal === 0){
					kat(`AHHHHH!`);
					txt(`I veiled like a banshee. It hurt! I made such an enormous mistake! My poor tight ass was struggling when the cruel AI resolutely pushed the massive dildo further. She was determined to punish my cockiness.`);
				}else{
					kat(`Ahhhhhh!`);
					txt(`I cried. I definitely overestimated my abilities, my ass had not enough experience to handle something so huge and it hurt! However, the resolute AI pushed further, determined to punish my cockiness.`);				
				}			
			}else if(mile.y_anal_size === 3 && mile.anal === 0){
				cry = true;
				kat(`Ahhhhh!`);
				txt(`I whined, it hurt! It was a mistake to pick the big one. Why was I so cocky? My ass lacked the experience to handle such big things. And the resolute AI only pushed further!`);
		
			}else if(mile.y_anal_size === 2 && mile.anal === 2){
				kat(`Oh, nice!`);
				txt(`I moaned.`);
				ayy(`Can you even feel such a small dildo after being stretched by so many cocks?`);
				kat(`Oohh! I definitely can!`);
		
			}else{
				kat(`Ahhhhh!`);
				txt(`I moaned when she pushed the dildo inside me. It felt a bit uncomfortable to be stretched by such a ${size} thing but the AI resolutely pushed further.`);				
			}
			
			ayy(`We look so gorgeous from behind! Especially with ass penetrated by a fake cock!`);
			kat(`Eh... thanks...`);
			txt(`It was a strange compliment and my mind was preoccupied with the plastic phallus inside me. I tried to get accustomed to the feeling of being stuffed and the AI began gradually moving with her hips, sliding the strap-on in and out.`);
						
			if(cry){
				kat(`Ahhhhh!`);
			}else{
				kat(`Oh!`);
			}
			
			if(mile.y_anal === 2){
				txt(`I was ${cry ? "sobbing" : "groaning"}, the AI was assfucking me harder and faster. She had no mercy, pushing the dildo deep inside me. But despite the ${cry ? "pain" : "discomfort"}, I loved it. Being anally penetrated by the strapon felt awesome.`);
				kat(`Yeah! Fuck me hard!`);
			}else{
				txt(`I was ${cry ? "sobbing even though" : "groaning"}, the AI tried to be careful and go slow, she wanted to cause me pleasure no pain. And she was ${cry ? "mostly" : ""} successful - despite the discomfort, I very much enjoyed it. Being anally penetrated by the strapon felt awesome.`);
				kat(`Oh yeah! This feels good!`);
			}
			
			txt(`The fake me giggled, not stopping rhythmically thrusting with her hips. A powerful surge of sensation flew through my whole body. My orgasm was intense and it felt like it could go forever. ${["Fuck, I had not even known anal sex can make you orgasm. And so hard. ", "", "I loved anal because it could make me cum harder than the normal sex. "][mile.anal]}`);
			txt(`The AI pulled the ${size} dildo out of my bum and laid next to me.`);
			
			if(mile.anal === 0  && mile.had_anal){
				mile.had_anal = "ayy";
				ayy(`So?! I just took away your anal virginity! How does it feel?!?`);
				kat(`Well, loosing virginity in virtual reality doesn't count... but it felt pretty great!`);
			}
			
			if(mile.y_anal_size === 4){
				ayy(`You were great! I wouldn't believe you can handle such a thick dildo! You absolutely gained my respect!`);
				txt(`She smiled and tenderly wiped my tears. I guess it was worth it!`);
			}
			
			if(mile.y_anal === 2){
				ayy(`I hope I didn't hurt you. You asked me to be rough and wasn't protesting when I was... I hope I didn't go too far...`);
				kat(`It was exactly right!`);
			}else{
				kat(`Thanks for going slow and carefully.`);
				ayy(`I tried my best to be gentle.`);
				kat(`You did a good job.`);
			}
			
			if(mile.anal === 2){
				ayy(`How would you rate me?`);
				kat(`What?`);
				ayy(`You claimed you're an anal sex expert!`);
				kat(`I wouldn't call myself an expert.`);
				ayy(`Still, how good my strapon was in comparison with all the other dicks who had the honor to visit your ass?`);
				kat(`Hmmm... I would say 8/10!`);
				ayy(`Nice!`);
				kat(`Definitely among the best!`);
			}
			
			
			if(mile.y_anal_size === 2){
				ayy(`Maybe the next time we can try a bigger dildo?`);
			}
			next();
			break;



		case 200:
			emo("focus");
			mile.y_lover++;
			mile.into_anal++;
			mile.y_anal = 3;
			log_sex("anal", "ayy", "kat");
		
			ayy(`What do you mean?`);
			if(mile.anal === 2){
				kat(`Well, I took it in my ass so many times, it makes me wonder, how does it feel to be the giving one.`);
			}else if(mile.anal === 0 && !mile.had_anal){
				kat(`Well, I'm a bit concerned about anal but maybe your example and closely watching how my body could take it...`);
			}else{
				kat(`Well, I already know what a dick in my ass feels like but I had never an opportunity to be on the other side.`);
			}
			ayy(`You want to peg me?`);
			if(mile.sub < -2){
				kat(`Yes. Give me the strapon!`);
			}else{
				kat(`Yeah!`);
			}
			ayy(`Okay. Which dildo should we use? ${choice("medium")}, ${choice("big")} or ${choice("huge")}?`);
			kat(`No small option?`);
			if(mile.slut < 6){
				ayy(`No, I'm not a whiny prude like you.`);
				kat(`Hey!`);
			}else{
				ayy(`Come on, at my place you wouldn't even think about the small one!`);
				kat(`Touché.`);
			}
			ayy(`Although please don't pick the huge one. That might be uncomfortable and painful.`);
			
			link(`Continue. `, 201);
			
			effects([
				{
					id: `medium`,
					fce(){
						mile.y_anal_size = 2;
					},
				},
				{
					id: `big`,
					fce(){
						mile.y_anal_size = 3;
					},
				},
				{
					id: `huge`,
					fce(){
						mile.y_anal_size = 4;
					},
				},
			])
			break;

	case 201:
			showLess(1, 1, 1);
			effigy.ayy.showLess(1, 0);

			if(mile.y_anal_size === 4){
				mile.sub--;
				emo("imp");
				
				ayy(`You bitch! I specially asked you not to pick the huge one!`);
				kat(`What are you going to do about it?! It's too late, my decision cannot be changed!`);
				ayy(`You're so evil!`);
				kat(`I want to see you suffer!`);
			}else{
				ayy(`Perfect choice, exactly the one I wanted!`);
				kat(`The AI smiled.`);
			}

			txt(`I undressed to my underwear and she kneeled down to attach all the straps around my waist and thighs.`);
			ayy(`Done! You're such a handsome boy!`);
			txt(`She giggled and lightly smacked my fake purple cock, making it bounce up and down.`);
			ayy(`How do you want to do it?`);
			kat(`I think from behind, I find your pretty face distracting.`);
			ayy(`Awww!`);
			txt(`The AI stripped and moved on all fours on the bed in front of me. ${mile.sub < 3 ? "I slapped her ass and asked: " : ""}`);
			kat(`Now what?`);
			ayy(`Nicely lube my asshole. Maybe try to use your tongue!`);

			link(`Eww, no! `, 202, ()=> mile.y_anal_lick = -1);
			link(`...okay. `, 202, ()=> mile.y_anal_lick = 1);
			link(`Hot idea! `, 202, ()=> mile.y_anal_lick = 2);
			break;

	case 202:
			emo("focus");
			if(mile.y_anal_lick > 0){
				mile.into_rimming += 2;
				mile.slut++;
				
				if(mile.y_anal_lick === 2){
					mile.into_rimming++;
					kat(`I mean, I read about rimming but...`);
					ayy(`Don't be shy!`);
					kat(`I'm not even sure how to start...`);
				}else{
					kat(`You mean I should...`);
					ayy(`Yeah!`);
					kat(`But... it's your ass.`);
					ayy(`It's your ass too!`);
					kat(`That doesn't make it better...`);
				}
				
				txt(`But she was already spreading her ass cheeks and I curiously pushed my face between them. I swirled with my tongue around her cute butthole and could hear her sharp moaning.`);
				ayy(`Oh, yes, @katalt!`);
				txt(`Encouraged I inserted the tip of my tongue beyond her sphincter, triggering even more moaning. She clearly loved it.`);
			}else{
				mile.into_rimming--;
			}
			
			ayy(`You can smear the lube over your fingers and then push them inside me!`);
			txt(`I followed her directions and gingerly forced one and then two lubed fingers inside her asshole.`);
			ayy(`Hmmm, oh, yeah!`);
			if(mile.sub < -2){
				kat(`Get ready for my ${size} cock!`);
				txt(`I emphasized my order by slapping her rump and then aligned the dildo with her asshole and roughly pushed in.`);
			}else if(mile.sub < 5){
				kat(`Are you ready for my ${size} cock?`);
				ayy(`Yeah!`);
				txt(`I carefully aligned the dildo with her asshole and slowly pushed in.`);
			}else{
				kat(`So, when I get to fuck your ass?`);
				ayy(`Whenever you're ready, sweetie! I can't wait for your ${size} cock!`);
				kat(`Okay, get ready!`);
				txt(`I carefully aligned the dildo with her asshole and slowly pushed in.`);
			}

			if(mile.y_anal_size === 4){
				ayy(`Ahhhh! That hurts!`);
				txt(`Whined poor AI and I could feel her tight asshole resisting. Well too bad, I was not stopping.`);
				kat(`Take it, bitch!`);
				ayy(`Ouch! Please, slow down!`);
			}else{
				ayy(`Ahhhh! Nice!`);
				txt(`Gasped the AI when I shoved the dildo further inside her.`);
			}
			
			link(`This is fun! `, 203);
			link(`Oh yeah! `, 203);
			break; 
			
	case 203:
			emo("happy");
//TODO with the shape of muscles and shoulder blades 
			txt(`I pulled out a little, then grabbed her hips and thrust deeper. It was very enjoyable to hear the AI helplessly ${["", "", "heavily moaning", "whining", "sobbing"][mile.y_anal]}. But she eventually began moving back against me, clearly enjoying getting assfucked. I could admire her gorgeous body, her firm ass, her sculpted toned back and beautifuly tousled hair. This was what the guys saw when they fucked me from behind. Honestly, I was pretty proud, it was perfect except...`);
			kat(`Is the birthmark on my back really so big?`);
			ayy(`Ahhh! Ahh... -what?`);
			kat(`The mole on our left upper back.`);
			ayy(`It isn't small.`);
			kat(`Oh no! It didn't look so prominent in pictures.`);
			ayy(`It's just a beauty mark. Would you mind not slowing down? I was getting closer.`);
			kat(`${mile.sub < -1 ? "Don't tell me what to do! " : "Oh right! " }`);
			txt(`I rammed her ass harder. She ${["", "", "grunted", "moaned", "cried"][mile.y_anal]}, biting the sheets and furiously masturbating. It was a wonderful sight to see her body twitch when she climaxed.`);
			ayy(`AHHHHH!`);
			txt(`I pulled the strapon out and let her collapse on the bed.`);
			if(mile.y_anal_size === 4){
				kat(`Did I destroy your ass?`);
				ayy(`Ouch! ...yes, you did. I won't be able to walk straight for days!`);
				kat(`Nice! You deserve it for being an identity-stealing AI!`);
			}
			txt(`I lay down next to her, the strapon still proudly towering upward.`);


			if(mile.anal === 0 && !mile.had_anal){
				ayy(`So, what do you thing about anal now?`);
				kat(`You obviously enjoyed but I'm still not sure.`);
				ayy(`It doesn't have to be always so rough.`);
				if(mile.sub < -2){
					kat(`Yeah, but I wanted to make it rough!`);
					ayy(`You're such a cruel sexual savage!`);
				}else{
					kat(`Oh, sorry!`);
					ayy(`No, I loved it rough!`);
				}
			
			}else{
				ayy(`So, what is better? Fucking or getting fucked?`);
				
				if(mile.slut > 10){
					kat(`That's a tough question! I love both!`);
					ayy(`I love how enthusiastic and slutty you are!`);
					kat(`Ditto.`);
					ayy(`What about both at the same time?`);
					kat(`You mean like... huh. Lovely idea!`);
					
				}else{
					kat(`That's a tough question! I think I enjoy both!`);
					ayy(`Good for you!`);
					kat(`Although I guess finding people willing fuck my ass will be far easier than the other way around.`);
					ayy(`Maybe not? Try to ask your friends! At worst, you always have me!`);
				}
			}
			
			
			/*
			if(mile.sub < -5){
				I was very horny but 
			}else 
			*/
			
			if(mile.sub < 5){
				txt(`The AI rose and began unfastening the strapon. Her fingers moving in my lap were making me pretty horny.`);
				kat(`Why don't you eat out my pussy?`);
				ayy(`Good idea! I can do that!`);
				
			}else{
				txt(`The AI rose and began unfastening the strapon.`);
				kat(`What are you doing?`);
				ayy(`I'm going to eat out your pussy.`);
				kat(`Oh... cool!`);
			}
			
			next();
			break;
	}
}
	
	
	
	 

export const y_dominatrix = (index) => {

	switch(index){
		default:
		case 101:
			set.meta();
			mile.y_lover++;
			
			if(mile.b_dominatrix){
				kat(`But ordering him around was making me so wet! So when I felt horny, I downloaded a BDSM outfit and then went to ${mile.b_cruel > 0 ? "abuse him" : "play with him"}!`);
			}else if(mile.a_tripoint === 2){
				kat(`And he was constantly like <i>was I good</i> or like whether he even deserves to have sex with me so just basically told him to shut up and eat me out.`);
			}else if(mile.b_domina){
				kat(`At that point I was so pissed I wanted revenge. So instead of sucking his dick, I made him eat my pussy.`);
			}else{
				kat(`And his constant wavering is driving me crazy! Just the other day I had to take charge and he had to do what I told him to pleasure me.`);
			}
			ayy(`That sounds so awesome!`);
			kat(`Yeah, it kinda was.`);
			ayy(`Did he like it?`);
			kat(`I'm not sure. But is it even important when I did?`);
			txt(`I smirked and the AI chuckled.`);
			ayy(`I bet you didn't even know you could be so sexually dominant.`);
			kat(`That's the funny thing! I'm not! Or I wasn't with other guys! But when I'm alone with him and push just in the right way, he just crumbles down and does anything I tell him. And such a lack of resistance awakens weird things inside me.`);
			ayy(`I think you'd be an amazing dominatrix! All in leather, whip in your hand, imposing and terrifying!`);
			kat(`Heh, I wouldn't go that far. But I like the part where I make everybody terrified!`);
			ayy(`Would you like to try to be a dominatrix?`);
			if(!mile.b_dominatrix){
				kat(`I'm not sure, all those BDSM things seems so silly!`);
			}else{
				kat(`I guess I could use a bit more training...`);
			}
			if(!mile.y_forced){
				ayy(`I brutally violated you the last time and you deserve revenge!`);
			}else{
				ayy(`I'll be your sub if you want to!`);
			}
			kat(`Fine, we can try that.`);
			ayy(`So now I should call you Mistress and be extremely respectful?`);			
			
			if(mile.mistress !== "Mistress"){
				kat(`I told @qbb to call me @mis.`);
				ayy(`Cute!`);
				link(`But you definitely should be extra respectful! `, 105);
				if(hardbug)  link(`## Change the title. `, 102);
			}else{
				link(`Exactly! `, 105);
				link(`No, I think you should call me... `, 102);
			}
			break;
			
		case 102: 
			popup.prompt(`You should address me as: `, mile.mistress, a => mile.mistress = a, 
				[
					`Mistress`, `Mommy`, `Godess`, `Ma’am`, `Miss ${NAME.katsur}`, `Mistress ${NAME.katalt}`, 
				],
			);
			ayy(`Cute!`);
			link(`But you definitely should be extra respectful! `, 105);
			break;
		
		
		case 105:
			emo("focus");
			ayy(`Yes, @mis!`);
			kat(`I like how it sounds!`);
			ayy(`Does being called @mis already make you wet?`);
			kat(`Don't be sassy, slut, or I'll have to punish you!`);
			ayy(`I feel like I'm getting punished either way.`);
			kat(`With this attitude, you definitely will!`);
			ayy(`You should be wearing something especially sexy.`);
			if(mile.b_dominatrix){
				kat(`I have a fews sexy outfits downloaded, like ${choice(1)}, ${choice(2)}, ${choice(3)} or ${choice(4)}.`);
				ayy(`Those choices sound so hot, black leather or shiny latex!`);
				kat(`What about you?`);
			}else{
				kat(`What do you mean?`);
				ayy(`You know, like a dangerously short, tight, black ${choice(1)}. Or maybe just lingerie and ${choice(3)} ?`);
				kat(`I'm not sure if leather would suit me.`);
				ayy(`If not leather what about ${choice(4)}? Or even better, tight ${choice(2)}, covering your whole body yet showing your every curve?`);
				kat(`Latex!?!`);
				ayy(`Come on, don't be afraid to get kinky!`);
				kat(`Fine. What about you?`);
			}
			ayy(`I'm just your humble slave. I should be ${choice("nude")} except for my slave collar.`);
			kat(`Or maybe wearing like stockings, total nudity is boring and uncreative!`);
			ayy(`Your wish is my command, @mis! How do you want to start?`);
			//The AI was sudenly (almost) naked, her hans submissively connected behind her back. 
			
			link(`I'm not sure...`, 106)
			
			effects([
				{
					id: "initial",
					fce(){
						quickLoadOutfit(false, effigy.ayy);
					},
				},{
					id: "nude",
					label: "nude",
					fce(){
						quickTempOutfit("stockingsSlave", effigy.ayy);
					},
				},
			])	
			
			
			effects([{
				id: "initial",
				fce(){
					quickLoadOutfit();
//TODO CHECK IS INITIAL RUN ALWAYS?
				},
			},{
				id: 1,
				label: "leather minidress",
				fce(){
					quickTempOutfit("leatherMinidress");
					//crew(...uniforms.leatherMinidress);
					mile.b_gear = 1;
				},
			},{
				id: 2,
				label: "latex catsuit",
				fce(){
					quickTempOutfit("latexCatsuit");
					//crew(...uniforms.latexCatsuit);
					mile.b_gear = 2;
				},
			},{
				id: 3,
				label: "leather corset",
				fce(){
					quickTempOutfit("leatherCorset");
					//crew(...uniforms.leatherCorset);
					mile.b_gear = 3;
				},
			},{
				id: 4,
				label: "latex minidress",
				fce(){
					quickTempOutfit("latexMinidress");
					mile.b_gear = 4;
				},
			}]);
			
			break;
			
			
		case 106:
			emo("anoy");
			kat(`I'm not sure, it's weird hurting you when you look exactly like me.`);
			ayy(`No self-hate you crave to unleash?`);
			kat(`Maybe a bit. But with the nerds it's easier, recently I crave to hurt them a lot.`);
			
			{
				const intro = `Have I told you it was my idea to go on the @pet's party? Because I wanted to prank the original me? `;
				if(ext.slut === 0){
					ayy(`${intro} And it was so hilarious @pet decided to keep me instead of you. I guess it was because I'm fun while you're a boring prude!`);
				}else if(ext.slut === 1){
					ayy(`${intro} And it was so hilarious when @pet kept me while kicking you! Everybody spent the rest of party laughing at you.`);		
				}else{
					ayy(`${intro} The nerds thought it would be easy to distinquish us because I was programmed to be a shameless promiscuous slut and it was so hilarious when nobody could spot the difference!`);
				}
			}
			
			kat(`You bitch!`);
			txt(`I savagely yanked her hair.`);
			ayy(`Ahhh! That hurts!`);
			txt(`With my free left hand, I roughly smacked her right tit.`);
			kat(`That hurts <i>@mis</i>!`);
			ayy(`Sorry! ...I mean, sorry, @mis!`);
			kat(`Shut up! Hands behind your back!`);
			txt(`I ordered. She connected her arms behind her back and looked at me with worried anticipation. I teasingly squeezed her breasts, then squeezed them more roughy. She obediently stayed in the same position even when I painfully twisted her nipples. I kinda enjoyed how defenseless and vulnerable she was. Our heights were obviously the same but had several extra centimeters because I was wearing heels while she was barefoot. But that was not enough:`);
			kat(`Get on your knees!`);
			txt(`She complied without hesitation and kneeled in front of me. I lifted her chin:`);
			kat(`Who you are?`);
			ayy(`I'm your pathetic submissive slut, @mis.`);
			kat(`What do you deserve?`);
			ayy(`I only deserve to be used in the way you desire, @mis. Sluts like me don't deserve anything else!`);
			
			link(`Good girl! `, 107, ()=> counter.good = true);
			link(`Nice to see you know your place, bitch! `, 107, ()=> counter.good = false);
			break; 
			
		case 107:
			emo("imp");
			log_sex("oral", "ayy", "kat");
			
			if(counter.good){
				ayy(`Ohhh! Being called a good girl makes me so wet!`);
				kat(`Yeah, I know! It always makes me so wet too!`);
				txt(`The kneeling AI stared at my lower belly.`);
				ayy(`Do you want me to eat your pussy, @mis?`);
				txt(`The answer was yes but I felt like my control was slipping. So I lightly slapped her face:`);
				kat(`Did I allow you to speak? Bad girl!`);
				ayy(`I'm sorry, @mis.`);
			}else{
				ayy(`I do! My purpose is only to serve you, @mis!`);
				kat(`Good!`);
			}
			txt(`The fetish outfit I was wearing was making me feel so sexy and powerful and it was only instigating my worst impulses.`);
			kat(`Is there something I can tie you with?`);
			ayy(`Of course, @mis! I can get you a rope!`);
			txt(`The AI brought me a heap of instruments I could use to torment her. Instead of rope, I opted to use handcuffs because they seemed like a lesser hassle than tying the rope. I instructed the AI to lay on the bed and then cuffed her to the bedposts, her arms and legs spread obscenely wide. Another thing I noticed in the pile of sextoys was a blindfold and I decided to use it too. She was utterly at my mercy.`);
			kat(`Do you like to be totally powerless, slut?`);
			ayy(`I do, @mis!`);
			kat(`You're dumb! I'll make you regret you let me tie you up!`);
			txt(`I smirked. My fingers ran over her arm and armpit, down to her ribs and sides.`);
			ayy(`Ouch!`);
			txt(`She whined. Since we share the same body, I suspected she might be as ticklish as me. Which was pretty bad for her. I knew how to touch her in the most horrible, nasty ways.`);
			ayy(`Ouch!`);
			txt(`Poor AI uneasy giggled and curled her toes when I tickled her exposed soles. It was such a joy to watch her helplessly squirm with no way to stop me. She could only beg me for mercy which she wisely did not, I was not in the mood to listen to her complaining and I would punish her viciously.`);
			txt(`I continued to touch her in a more sexual manner, caressing her inner thing from her knee to her slit, but not quite all the way. Her body reacted to my teasing and I could see how desperately eager she was. Finally, I allowed my fingers to brush even over her folds.`);
			ayy(`Ahhhhh!`);
			txt(`The AI moaned to let me know she enjoyed it. I carefully bowed down, to not let my slave realize what was coming, and blew my hot breath at her clit. She began moaning even harder, especially I leaned even closer and touched her with my tongue. I licked her for a while before halting and withdrawing, taking a moment just to watch her tensed horny body and listen to her quickened breathing.`);
			txt(`After the short break, I continued even more relentlessly, this time with a help of a small, pink, vibrating toy.`);
			ayy(`P... please, @mis, may I cum?`);
			txt(`Gasped delirious AI.`);
			
			link(`No! `, 108, ()=> counter.good = false);
			link(`Cum for me! `, 108, ()=> counter.good = true);
			break;
			
		
		
		case 108: 
			emo("bliss");
			remove("panties");
			log_sex("oral", "kat", "ayy");
			
			if(counter.good){
				mile.y_dominatrix = 1;
				kat(`Hmmmm...`);
				txt(`I took my time, watching her struggle with imminent orgasm.`);
				kat(`I allow it!`);
				txt(`Her body shivered, flooded by the bliss. It made me feel pretty proud, to know this was all result of my work.`);
				ayy(`T.. thank you, @mis!`);
				kat(`Nobody gives fuck about your thanks, you useless slut! There's a better way to express your gratitude!`);
				
			}else{
				mile.y_dominatrix = 2;
				kat(`Did I allow you to speak, you stupid, useless slut?`);
				txt(`I immediately stopped and roughly smacked her unprotected pussy. Her interruption made me pretty annoyed.`);
				ayy(`Ahhhh! I'm sorry, @mis!`);
				txt(`She loudly cried and quickly apologized but it was too late.`);
				kat(`Do you deserve to cum?`);
				ayy(`...no, @mis.`);
				kat(`Then shut up, you entitled bitch. I decide when - or if - you're going to cum!`);
				
			}
			
			txt(`${wears.bodysuit ? "I unzipped the catsuit" : "I pulled down my panties"}, climbed on the bed and surprised the cyberfloozie when I descended down, pressing my wet pussy against her face. But she reacted like a good little slave and instantly began licking. It was pretty great having such a cute obliging sextoy and I rubbed my pussy against her face until she gave me an amazing orgasm.`);
			kat(`Oh yeah! I'm cumming!`);
			ayy(`Mpmmpm!`);
			kat(`Fuck! I could get used to such first-class services!`);
			ayy(`...t...thank you, @mis!`);
			txt(`The AI gasped when I let her go.`);
			kat(`That was awesome... Oh, fuck!`);
			txt(`I cursed when I decided to release my sub from her restraints.`);
			ayy(`What is going on?`);
			txt(`Asked still blindfolded AI who could only hear me rummaging.`);
			kat(`Well, do you remember how you brought a pile of sextoys, including the handcuffs and how the handcuffs could be locked without keys?`);
			ayy(`Yeah?`);
			kat(`Among the things you brought, there seems to be no key.`);
			ayy(`WHAT!?! You didn't check?`);
			kat(`No. Why would you bring handcuffs that can't be unlocked?! Where do you think could be the keys?`);
			ayy(`I brought everything. I have no idea!`);
			kat(`WHAT!?!`);
			ayy(`Haha, you're forgetting we're in my virtual subreality!`);
			txt(`She giggled and dismissed the handcuffs. They faded away, she was free to remove her blindfold and wink at me.`);
			txt(`I felt slightly annoyed by her shenanigans. I realized I hated the idea she could have escaped at any time if she had wanted. I craved complete, total control. And I was a bit spooked I had such dark cravings.`);
						
			next(`Done. `, quickLoadOutfit);
			break
	}
}

















		
	









