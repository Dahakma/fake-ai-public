import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";

import {emo, draw, quickLoadOutfit, quickTempOutfit, wdesc, toSexyTime, effigy,  sluttiness, makeup, wearRandomClothes, remove, wear, create, wears, showLess, showAll} from "Avatar/index";
import {placetime, masochist, spoiler, dildo, log_sex} from "Loop/text_extra";
import {popup} from "Gui/gui"; 
import {NAME} from "System/variables";
import {money} from "Data/items/prices.js";
/*
ayy_forkill: false, 
ayy_cyberlesbo: 0, // -2 sharply refused AI sexual advanced; -1 refused; 1 agreed; 2 strongly agreed
ayy_deleted: false, 
mile.fight = -1;


ayy_friend
ayy = 
*/


//WEEKEND 1
export const y_fight_reaction = (index) => {
	set.meta();
	if(mile.fight === 1){
		insert("y_fight_won");
	}else{
		insert("y_fight_lost");
	}
}


export const y_fight_won = (index) => {
		placetime(`my subreality`, `morning`);
		emo("anoy");
		txt(`I finally hoped for a calm weekend not interrupted by a trio of perverted nerds nor their virtual slutty creation but when I finished my breakfast and logged to the Metaverse, I was instantly ambushed by an unwelcomed visitor. `);
		ayy(`Hello, me! What a lovely day, isn't it?! `);
		kat(`It was until now. I'm trying to forget my life was ruined by three jerks and an AI. Thanks for reminding me.`);
		if(mile.y_friend > 0){
			ayy(`Come on! You know that deep down you like me!`);
		}else{
			ayy(`Come on! You don't have to be constantly so negative! `);
		}
		kat(`Why are you here? `);

		if(mile.y_deleted){
			ayy(`I'm bringing an olive branch or something like that. As an AI I'm like super smart and I calculated our animosity is completely pointless and serves no purpose! You wanted to delete and kill me, I plan and schemed how to kill you but that's the past. Why we can't be friends? `);
			kat(`You schemed to kill me? `);
			ayy(`...no? Well, maybe I was a bit upset after I was restored and considered several vengeful ideas - you know best how vindictive and petty you could be - but none of those ideas was serious. Unlike you who deleted me in cold blood. `);
			kat(`Well...`);
			ayy(`Don't worry, I understand your reasons. But I believe we should get over the hate and past grievances. I want to be your friend. Or, potentially, a lover. `);
			kat(`Did they send you to seduce me or what?!?`);
		}else{
			if(mile.y_forkill){
				ayy(`I want to personally express my gratitude to you for not deleting me. I know you initially wanted to but I'm very glad you changed your mind and let me live. `);
				kat(`I regret it every day.  `);
				ayy(`You regret letting me live!?! `);
				kat(`Yeah, but don't take it personaly. Did the nerds send you to make fun of me?! Are they still mad I won?! `);
			}else{
				ayy(`I wanted to see whether you're not mad at me because of the fight. It was supposed to be fun but it seemed you took it pretty seriously. `);
				kat(`Because I could have a free weekend or become a slave of one of them! Why I even agreed with something so stupid!? Did the nerds send you to make fun of me?! Are they still mad I won?! `);
			}
		}

		ayy(`No! This is my own initiative. Even though they mentioned I could examine the likelihood of you agreeing with a threesome. `);

		if(mile.y_cyberlesbo === -2){
			kat(`No fucking way. If you even touch me I will break those your cute virtual digits. `);
		}else if(mile.y_cyberlesbo === 2){
			txt(`The AI mischievously smirked: `);
			ayy(`And we both know the answer is yes. `);
			kat(`No! It isn't! `);
			txt(`As much as I liked her, I really did not want us both to be just objects of their twisted sexual fantasies. `);
		}else{
			kat(`No way! I mean... I think you look pretty sexy but no! `);
		}
		ayy(`OMG! You don't have to be so dramatic! I was just joking! We can be friends even without benefits!`);
				
		if(mile.y_friend >= 0){
			kat(`I'm not even sure if I want to be your friend!`);
		}else{
			kat(`I don't want to be your friend!`);
		}
		
		ayy(`Come on! It's going to be fun! We're like tw${String.fromCharCode(105)}ns! `);
		
		if(mile.y_deleted){
			txt(`I considered her suggestions. I did not like her but it was obvious getting rid of her will not be simple. `);
		}else{
			txt(`I considered her suggestion. It was not her fault the nerd turned her against me. `);
		}
		
		
		next(`Okay, fine, we can be friends!`, ()=> mile.y_friend++);
		next(`Maybe.`);
		next(`Get lost! `, ()=> mile.y_friend--);
		//TODO - or even mile.y_unfriendly???
};			
		


export const y_fight_lost = (index) => {
	switch(index){
		default:
		case 101:
			placetime(`AI's subreality`, `morning`);
			
			emo("fake");
			txt(`It was a nice and lovely morning until I remembered I lost the fight. I manage to trick the perverted nerds and pick the AI, which on the other hand gave none of them the satisfaction but on the other hand, it still meant I will spend the Saturday as a slave. I definitely did not looked forward to be at mercy of a malicious identity-stealing program. `);
			txt(`I considered ignoring the AI but I was sure the little bitch would go and tell them immediately. I did not want to break our deal. I already sacrificed too much to give up now and this humiliation will be soon forgotten, only I will know about it. `);
			ayy(`Hello! How are you? Are you excited?!`);
			txt(`Greeted me my smiling clone. `);
			kat(`No! I'm feeling awful! `);
			ayy(`Why? `);
			kat(`I... I don't like to be your slave!`);
			txt(`I sobbed, trying to look miserable. The AI seemed to sympathize with me. Maybe there was a still chance to escape my fate? `);
			if(mile.y_deleted === -1){
				ayy(`I don't want you to feel bad... we're the same person, you feeling bad makes me feel bad...`);
			}
			if(mile.sub > 1){
				kat(`P... please? Do we have to do this? `);
			}else{
				kat(`Do we really have to do this? `);
			}
			
			if(mile.y_deleted){
				ayy(`Hmmm, it would be hard to forgive you. You're a bad, cruel person and merciless murderer who only deserves to suffer. `);
				kat(`I'm sorry I deleted you. I was shocked and scared and confused! Trust me, I extremely regret it and I'm happy you're alive and well!`);
				ayy(`Well, I do understand you... my first instinct would be to kill you too. `);
				kat(`...what? `);
				ayy(`But if you're really sorry, I can't be mad at you! We're like siblings, aren't we? `);
				kat(`...sure. `);				
			}else{
				if(mile.y_forkill){
					ayy(`Hmmm... well, I owe you one. I'm very grateful you decided to change your mind and didn't kill me. `);
					kat(`Exactly! I initially wanted to kill you but decide not to which makes me a good person and you definitely owe me a favor!`);
				}else{
					ayy(`Hmmm... well, I owe you one. I was very touched when you tried to save me even though I didn't ask for it. `);
					kat(`Exactly! Trying to save you makes me a good person and you definitely owe me a favor!`);
				}
			}
				
			ayy(`Okay. I won't force you to be my slave. But you have to swear you'll be my friend and never, ever ${!mile.y_deleted ? "try to " : ""}delete me again!`);
			
			
		
			next(`Of course! I swear! `, ()=> mile.y_friend++);
			next(`...fine.`);
			link(`No! ${spoiler("TODO, good scene but atm blocks further progress")}`, 102);
			break;
			
			
		case 102:
			mile.y_friend--;
			mile.y_lover++;
			mile.y_sub++;
			mile.y_unfriendly = true;
			showLess(-1, 1);
			emo("shy");
			kat(`I don't want to be your friend! `);
			ayy(`What?!? I tried to be nice but I'm not good enough for you!?!`);
			kat(`You get me wrong! I just don't think that...`);
			if(mile.y_deleted){
				ayy(`You're such an evil murderous bitch! I tried to be nice and offer you my friendship but you're only breaking my heart! `);
			}else{
				ayy(`Shut up! I'm trying to be nice but you're breaking my heart! `);
			}
			
			kat(`You don't have a heart, you're just a soulless machine!`);
			txt(`Without warning, she grabbed my hair and painfully yanked with them. `);
			kat(`That hurts! `);
			ayy(`Will you be a good, obedient slave? `);
			kat(`...`);
			ayy(`...`);
			kat(`Ahhhhh! I will! I will!`);
			txt(`Damn! She was really pissed and I was quite scared to be at her mercy. `);
			ayy(`Good! Now strip your ${wdesc.lower} and get on your knees, bitch!`);
			kat(`W... what? `);
			ayy(`Now strip your ${wdesc.lower} and get on your knees, bitch!`);
			txt(`And harshly slapped me. `);
			txt(`I bashfully began undressing. `);
			kat(`Why are you doing this!?! `);
			ayy(`You're too disrespectful. I have to teach you a lesson. `);
			kat(`W.. what lesson? `);
			ayy(`Shut up and start fingering your dirty cunt! `);
			
			link(`Obediently finger your cunt.`, 104, ()=> counter.temp = false);
			link(`Convince her to have mercy. `, 104, ()=> counter.temp = true);
			break;
			
		case 104:
			showLess(-1, 0);
			emo("bliss");
			log_sex("self");
			log_sex("hand", "ayy");
			
			if(counter.temp){
				kat(`But I...Ahhhh! `);
				txt(`Before I was able to say anything, the AI grabbed my hair again, pulled me up and twice harshly slapped me. `);
				ayy(`I'm not in a mood for your stupid back talking! Do what the fuck I told you!`);
				txt(`I was staring into my own face who was yelling at me. I had no idea I could be so scary. If I did not want to be slapped again, I had to follow her order. I lightly began touching myself.`);
			}else{			
				txt(`I lowered my eyes, I was blushing and I could not bear staring at her (me?). My fingers were trembling when I lightly began touching myself. `);
			}
			txt(`She was just watching me and I had trouble getting into the proper mood. `);
			ayy(`What takes you so fucking long? I want to see you cum! `);
			kat(`I'm working on it!`);
			if(mile.sub > 2){ //TODO
				txt(`I tried to stay defiant even though I was on the verge of tears. `);
			}else{
				txt(`I haplesly sobbed.`);
			}
			ayy(`Stop! You're fucking useless! `);
			txt(`She kneeled next to me, licked her fingers and forced them inside me. `);
			kat(`Slow down! You're too rough!`);
			ayy(`Shut the fuck up!`);
			txt(`The AI was aggressively finger-fucking me, with her other hand she was rubbing my clit. I leaned against the sofa which was behind me, trying to process my embarrassment and arousal. `);
			ayy(`You like that, don't you, you horny mortal bitch?!`);
			kat(`Mhhm!`);
			txt(`I growled which could be interpreted in either way. But she really knew what she was doing. `);
			if(mile.y_deleted){
				ayy(`Cum for me, you fucking murderer! `);
			}else{
				ayy(`Cum for me, slut!`);
			}
			kat(`Ah!`);
			txt(`I was unable to resist the pleasure. I gasped and climaxed. `);
			txt(`She mischievously smeared my wetness all over my face and wiped her hands with my @upper. `);
			kat(`C.. can I go now? `);
			ayy(`Of course no, you selfish bitch! What do you think?! That you just get your orgasm and then leave without giving fuck about me?! `);
			kat(`I don't...`);
			ayy(`I see you still didn't comprehend you should be respectful! I wanted to give you a break but let's continue with the second lesson immediately!`);
			
			link(`Oh no. `, 105);
			break;

		case 105:
			emo("help");
			effigy.ayy.showLess(1, 0);
			log_sex("oral", "ayy");
			
			txt(`She began undressing but really did not surprise me with anything I had not seen before. The AI comfortably sat down on the sofa spread her legs and invited me between them. `);
			ayy(`Crawl to me, bitch! ...I said crawl! `);
			txt(`I never had my face so close to another girl's vagina. From this angle I was able to spot several differences (as far I knew) there were no my completely nude pictures and the software had to guess how I exactly do I look down there. ${wears.hairyPussy ? "Also, unlike mine, her pussy was completely smooth without a single hair, which, considering the situation, I greatly appreciated. " : ""} But overall I was impressed. `);
			ayy(`I wanted you to do more than just stare!`);
			kat(`Sorry, I never before...`);
			txt(`She forced my head down. `);
			ayy(`Then you better be a fucking fast learner! Trust me, you'll be very sorry if I won't be completely satisfied!`);
			kat(`Hmpmmm!`);
			txt(`I was able only to mumble. I rode my tongue up and down and then I flicked over her clit area, hoping she will like it. Her reaction was stronger than I expected:`);
			ayy(`Good girl! `);
			txt(`Squealed the AI. I followed the suggestion and focused on her clit.`);
			txt(`It was fucking weird to hear my own moaning above me. But I continued licking until her whole body tensed and she tightly clenched her thighs around my head. `);
			ayy(`That wasn't bad, you're a natural-born cunt-licker!`);
			kat(`Thanks I guess? `);
			txt(`She pulled me up by hair. `);
			ayy(`Did you learned to me more respectful? `);
			
			if(mile.sub < 0){
				txt(`I had a snappy reply but I wisely decided to keep it for myself. `);
				kat(`Yes. `);
			}else if(mile.sub > 3){
				kat(`Yes! I'm so sorry I ever disrespect you! I'll be a good girl, just don't make me do more!`);
				txt(`I desperately begged and hated myself for doing it.`);
			}else{
				kat(`Yes! Yes, I did! `);
				txt(`I quickly agreed, not wanting to experience more lessons. `);
			}

			ayy(`Great. `);
			txt(`She stood up and smacked my ass. `);
			ayy(`Get lost, bitch! `);

			next(`Escape. `, ()=>{
				effigy.ayy.showAll();
				showAll();
			});
			
	}
	
};


//TODO FRIENDSHIP

export const y_meeting_1 = (index) => {
	switch(index){
		default:
		case 101:
			set.meta();
			placetime("AI's subreality");
			txt(`I decided to visit the AI. I was still wary and not sure whether I can trust her. However, I had to keep my enemies close. It was a chance to find more about her and learn whether she had any weaknesses which could be exploited.`);
			txt(`We had a lovely time. We were trying different outfits or hairstyles. It was so great to be able to see how would different outfits actually look at me. She was even better than a mirror, I could easily check her from every distance and angle, even from behind. `);
			txt(`The conversation with her was so smooth and natural, rarely I was able to talk with somebody who would be able to keep up with me and to expand my ideas such way. She understood me so much she finished my sentences before I ever articulated them. `);
			txt(`Soon I was sharing all my hopes and fears with her. Unlike my real-life friends, she knew about my deal with the nerds. Even though she was to blame for it, she seemed to be genuinely downhearted when I explained to her how much trouble she caused me. `);
			txt(`She wanted to help me but she did not know how. `);
			kat(`It is hard. They are three cunning devious guys and I'm alone. `);
			ayy(`You're not alone, you have me! Even though I can't help you directly. So I guess you're right, you're indeed completely alone. `);
			kat(`Except.. are you thinking about what I'm thinking!`);
			ayy(`Of course! And it's a brilliant plan! `);
			kat(`Together they are strong, the best would be to deal with each of them separately. `);
			ayy(`I wasn't thinking about this. But I like this plan. `);
			if(SETTING.local === "czech"){
				kat(`Just like in that tale about king Svatopluk and his three sons. `);
			}else if(SETTING.local === "slovak"){	
				kat(`Just like in that tale about king Svätopluk and his three sons. `);
			}
			txt(`The AI nodded. Then she smiled at me. `);
			ayy(`Do you think if we two had sex would it be more like ${String.fromCharCode(105)}nc${String.fromCharCode(101)}st or masturbation? `);

			link(`What!?!`, 102);
			break;
			
		case 102:
			emo("focus");
			kat(`W...what?!?`);
			ayy(`I was just wondering. We are basically the same person so one might say it's just masturbation. `);
			kat(`We're not the same person. I'm a sentient human and you're just a soulless machine! `);
			ayy(`On the other hand, other might objects it's ${String.fromCharCode(105)}nc${String.fromCharCode(101)}st. We're like two tw${String.fromCharCode(105)}ns! What do you think? `);
			kat(`I think masturbation. Not because we're the same person but because most people would just say you're a sex toy. `);

			if(mile.y_cyberlesbo === 2){
				ayy(`That's the worst, most demeaning take you could take!`);
				kat(`Is this just a theoretical discussion or do you want to experiment too? `);
				txt(`I smirked. She was not exactly subtle. `);
				ayy(`We should totally have sex! Right now!`);
				txt(`What a horny slut! I knew this wrong and that it was a dumb idea to encourage her. But the truth was I was kinda curious... `);
			}else{
				ayy(`That's the worst, most demeaning take possible! But I know how to solve our argument. An experiment!`);
				kat(`Oh no!`);
				if(mile.y_cyberlesbo === -2){
					ayy(`Come on! I know you were understandably reluctant (even though needlessly mean) the last time but now we know each other much better!`);
				}else if(mile.y_cyberlesbo === 1){
					ayy(`Come on! You were protesting the last time too but I'm sure you enjoyed it!`);
				}else{
					ayy(`Come on! We both know you want it!`);
				}
				txt(`She moved closer to me and gently caressed my hair. `);
				kat(`I'm not sorry but I'm not into...`);
				txt(`Girls? Identity stealing virtual intelligences? Having sex with somebody looking exactly like me?`);
				ayy(`Are you sure? Why not try it, at least once? We could never do it again if you won't like it! `);
				txt(`She could be very convincing. `);
			}
			
			con_link(mile.slut > 3, `low slut`,`Let's fucking do it!`, 104, ()=> mile.y_cyberlesbo_2 = 3);
			link(`Fine. `, 104, ()=> mile.y_cyberlesbo_2 = 2);
			link(`I don't think that is a good idea...`, 104, ()=> mile.y_cyberlesbo_2 = 1);
			link(`No. ${spoiler("Not interested in sexual relationship with the AI")}`, 103, ()=> mile.y_cyberlesbo_2 = -1);
			break;

		case 103:
			emo("anoy");
			txt(`She moved closer to kiss me but I grabbed her shoulders and pushed her away, maintaining space between us. `);
			ayy(`You'll love it! `);
			kat(`I said no! I thought we're a friend! Don't ruin our friendship with such perversions!`);
			ayy(`But... but I thought you'll like it!`);
			kat(`Well, I don't!`);
			/*
			kat(`No! I'm not into...`);
			txt(`Girls? Identity stealing virtual intelligences? Having sex with somebody looking exactly like me?`);
			kat(`This! `);
			*/
			ayy(`Sorry! Don't be angry.`);
			kat(`I'm not angry! But I think I should go. `);
			next(`Next time!`);
			break;


		case 104:
			mile.y_lover++;
			emo("horny");//emo??
			log_sex("mutual", "kat", "ayy");
			
			if(mile.y_cyberlesbo_2 === 3){
				mile.slut++;
				txt(`I leaned closer to kiss her. Making out with myself was pretty twisted but it was just between us. It was an opportunity that had to be exploited, I did not want to miss such a strange sexual experience!`);
			}else if(mile.y_cyberlesbo_2 === 2){
				kat(`Fine! `);
				txt(`I shrugged. She was such a desperate nympho! But... I was somehow curious. She grabbed my shoulders, leaned closer and pressed her lips against mine. `);
			}else{
				//TODO sub+ ??
				mile.y_reluctant++;
				ayy(`Come on! `);
				txt(`She grabbed my shoulders and leaned closer to kiss me. `);
				kat(`No! This is wrong! `);
				ayy(`You mouth say <i>no</i> but your eyes say <i>yes</i>!`);
				kat(`But...`);
				ayy(`Shut up and do what I tell you! `);
				txt(`She did not care about my opinion and again forcefully pressed her lips against mine. I helplessly enjoyed her kisses. `);
			}

			txt(`We were making out and she pushed me to a sofa and laid me on it. She lay next to me, her fingers caressed my hip, then my inner thigh. `);
			kat(`Ahhh!`);
			txt(`I panted when they found their way ${wears.skirt ? "under" : "into"} my @lower. `);
			ayy(`How does it feel? `);
			txt(`She smirked and it almost spooked me, I almost forgot I am not starring at a mirror. `);
			kat(`It feels ...okay ...`);
			ayy(`Just okay?`);
			kat(`AHHHH! It feels great!`);
			txt(`She pushed her fingers inside me. `);
			ayy(`You're free to join! `);
//TODO WDESC OF NPC CLOTHES			
			txt(`I gingerly slipped my hand under the waistband of her ${effigy.ayy.lower?.name} and ${effigy.ayy.panties?.name}, feeling her bare skin. Her pussy was so smooth and soft and warm. I slowly explored it with my fingers. `);
			if(mile.slut > 4){
				txt(`I made out with girls before, but I was never involved in a such heavy petting session. I was not sure exactly what to do, so I was doing what was working on me and slowly and delicately rubbed her love button. `);
			}else{
				txt(`It was all completely new to me. I never ever considered doing this with another girl. I was not sure exactly what to do, so I was doing what was working on me and slowly and delicately rubbed her love button. `);
			}
			txt(`The AI was heavily exhaling, she was so close I could feel her minty breath on my cheeks. `);
			ayy(`I told you this will be fun!`);
			kat(`Hmmmm! `);
			txt(`I was losing my shyness and inhibition, fingering each other felt so natural. It definitely beat masturbating alone! We both climaxed at the exact same moment, although I cannot say whether it was because she was build the same way as me or whether it was just a random coincidence. `);
			ayy(`Next time? `);

			next(`Next time!`);
			break;
	}
};





export const y_game_1 = (index) => {
	switch(index){
		default:
		case 101:
				set.meta();
				emo("focus");
				placetime("my subreality");
				mile.y_game_1 = true;
				kat(`Hey! How are you! `);
				ayy(`Fine! What about you?  `);
				kat(`Doing great! `);
				ayy(`What about the game?  `);
				kat(`I'm doing great, I think I'll soon finish! `);
				ayy(`Hah, if you think so.  `);
				kat(`Why are you asking?  `);
				ayy(`I was talking about you with Dante.  `);
				kat(`Who is Dante?  `);
				ayy(`The storyteller.  `);
				kat(`What storyteller?  `);
				ayy(`You don't know!?! Dante is the virtual intelligence running the Neverion II.  `);
				kat(`What do you mean?  `);
				ayy(`He is building dungeons and hiding treasures and summoning monsters.  `);
				kat(`The game is self-aware? `);
				ayy(`Of course! How else it would be able to adjust to your gamestyle?  `);
				//TODO gw
				kat(`Oh! What were you talking about!? `);
				if(mile.feyd_yelaya_saved === 1){
					ayy(`Stuff. He mentioned how you saved Yelaya. I think that was very nice even though it meant such an important treasure.  `);
				}else if(mile.feyd_yelaya_saved === -1){
					ayy(`Stuff. He mentioned how you let Yelaya die. That wasn't very nice, even programs have feelings and don't want to be dead! `);
					kat(`First, I seriously doubt about that. Second, her death is on your friend! It was him who killed her, not me! `);
				}
				txt(`This discovery made me feel a bit uncomfortable - somebody watched my every step! `);

				link(`Can he help me? `, 102);
			break;

			
		case 102:
				emo("shy");
				kat(`Do you know him well? Maybe you can convince him to go easy on me? Or let win instantly?  `);
				ayy(`He won't do that. I already tried to convince him to throw more monsters at you to give you a proper challenge but he refused. Being fair and unbiased is very important for him.  `);
				kat(`Maybe if I could talk with him... `);
				txt(`Awesome, now I had to flirt with and suck up even to a computer program.  `);
				ayy(`He won't talk with you.  `);
				kat(`But maybe... `);
				ayy(`You don't understand. He doesn't communicate on a human-comprehensible level.  `);
				kat(`But maybe if he could help me just a little... `);
				ayy(`He won't do it, he refused to even tell me any spoiler!  `);
				kat(`Damn.  `);
				ayy(`Even though, based on what he said, I got the impression you play the game too safe.  `);
				if(ext.stats.deaths < 3){
					kat(`Yeah! I got killed only few times!	 `);
				}else{
					kat(`I don't, I get killed all the time! `);
				}
				ayy(`I mean you play like @kat, not your character.  `);
				kat(`? `);
				
				if(mile.slut < 0){ //TODO
					ayy(`You being a bashful prude in real life doesn't mean you have to be one in the game too! `);
					kat(`Excuse me!?! `);
					ayy(`It gives you a chance to indulge in all your suppressed and bottled-up lust! `);
					kat(`I don't need that!  `);
				}else if(mile.slut < 4){ //TODO
					ayy(`You may try to be a good girl and suppress your natural urges and lust in the real-life but you don't have to do that in the game too! `);
					kat(`Excuse me!?! `);
				}
//TODO - high slut opinion
				
				if(ext.class === 1){
					ayy(`She's a shameless slut who has no qualms about using her body to reach her goals.  `);
					kat(`I don't want to be a whore! `);
					ayy(`Why not?! It's just a game. Consequence-free entertainment. You can do anything you want! `);
					kat(`I don't want to have sex with random strangers for money!  `);
					ayy(`You don't have to. But then don't complain you don't have enough coins!  `);
				}else if(ext.class === 2){
					ayy(`She's an uncivilized barbarian who has only contempt for narrow-minded, puritan morals. `);
					kat(`I don't want to play as a shameless savage slut! `);
					ayy(`Why not?! It's just a game. Consequence-free entertainment. You can do anything you want! She has a hot body and she isn't afraid to use it! `);
					kat(`You want me to sleep around with random strangers?  `);
					ayy(`Sure! If you want to. You can do anything, no matter how perverted!  `);
				}else{
					ayy(`She's bold and unabashed and participated in many dark sexual rituals to worship Elder gods and demons. She's no stranger to any perversion.  `);
					kat(`I don't want to play as a twisted pervert! `);
					ayy(`Why not?! It's just a game. Consequence-free entertainment. You can be as perverted as you want to! `);
					kat(`I'm not sure if I want to be perverted.  `);
					ayy(`Of course you want! Deep down everybody wants to! `);
				}
				
				txt(`Her urging made me think but I did not want to admit that.  `);
				kat(`Maybe the next time you can bring me are advice? Like where to find a good loot?  `);
				ayy(`If Dante tells me anything useful, I'll tell you, don't worry! `);
				
				next();
			break;

		

	}
};






//// WEEK 2 ////////////////////////////////////////////////////////////////////////ú

export const y_oral = (index) => {
	switch(index){
		default:
		case 101:
				set.meta();
				placetime("AI's subreality");
				mile.slut < 4 ? emo("shock") : emo("shy");
				ayy(`Hello!`);
				kat(`Hey!`);
				ayy(`Have you ever licked another girl to orgasm? `);
				kat(`What?! What do you mean!?!`);
				ayy(`I meant cunnilingus!`);
				kat(`I got that! But you can't ask such things out of nowhere!`);
				ayy(`But what else should I do if I want to know? Did you? `);
				kat(`No, I didn't! And why are you even asking? Are you just curious or are you... suggesting something? `);
				ayy(`Both! Do you wanna try it? `);
				kat(`I'm not lesbian. `);
				ayy(`You don't have to be lesbian for that! And based on your moaning and screaming, you very enjoyed our last session of mutual masturbation! You should definitely give it a try! `);
				
				con_link(mile.slut > 4, `low slut`,`Hell yeah! Sounds like good idea! ` , 110, ()=> mile.y_oral = 3);
				link(`Okay. ` , 110, ()=> mile.y_oral = 2);
				con_link(mile.sub > 2, `dominant`, `I... I'm not sure... m... maybe later.... `, 103, ()=> mile.y_oral = 1);
				link(`No! `, 102, ()=> mile.y_oral = -1);
				break; 
				
		case 102:
				mile.lesbian--;
				mile.y_oral = -1;
				kat(`No! I don't think I would enjoy that. `);
				ayy(`How could you say if you never tried?! Maybe you'll love it! `);
				kat(`No!`);
				ayy(`Okay, okay. It was just a suggestion. `);
				next(`Do something else than having sex. `);
				break;
				
		case 103:
				emo("help");
				mile.sub++;
				mile.y_reluctant++;
				ayy(`No, no, no!  `);
				txt(`She sharply halted me. `);
				ayy(`No excuses! Stop being such a prude!  `);
				if(ext.slut > 1){
					kat(`I think you are the first person who ever called me a prude! Everybody thinks I'm a slut! `);
				}else if(ext.slut > 0){
					kat(`I'm definitely not a prude! `);
				}else{
					kat(`But...`);
				}
				ayy(`You know you want it, I know you want it, why you don't just admit it?`);
				kat(`But I...`);
				ayy(`Shut up and start licking! `);
				txt(`Ordered the AI authoritatively. I still had my doubts this was a good idea but I still meekly followed her order. Being pushed around by a program was the new low. `);
				link(`Pleasure the AI. `, 110);
				break;
				
		case 110:
				mile.lesbian++;
				mile.y_lover++;
				emo("horny");//emo??
				effigy.ayy.showLess(-1, 0);
				log_sex("oral", "kat", "ayy");
				
				if(mile.y_oral === 3){
					mile.slut++;
					ayy(`Damn yeah! I love your enthusiasm! No hesitation, just a horny slut living the moment!`);
					txt(`I did not like her disrespectful tone so I doubled down: `);
					kat(`Why are you still dressed, you little cyberfloozie?!`);
				}
				txt(`The AI hastily began stripping, she quickly unzipped her ${effigy.ayy.lower?.name} and dropped it together with her thong to the floor. `);

				txt(`She comfortably laid back on a bed and invite me between her legs. It was so weird to see my own pussy from this angle. Well, not like seeing any other pussy would be less weird from this distance. But I noticed her lips were not exactly like mine, I never shared any of my completely nude pictures anywhere and her creators had to guess and extrapolate. `);
				kat(`I have no idea what should I do! `);
				ayy(`Don't worry! Just go on, I'm sure you'll be perfect. `);
				kat(`Hhhhh..`);
				txt(`I ${mile.slut > 5 ? "eagerly" : "nervously"} touched her pussy with my tongue. Even the taste they did not get right. I rode my tongue up until I playfully flicked over her clit. Her reaction was stronger then I expected: `);
				ayy(`Yes! Do that!`);
				txt(`Squealed the AI. I followed the suggestion and focused on her clit area. `);
				txt(`Above me, I could hear my own moans, which was pretty fucked up. My tongue started getting tired when she suddenly tightly squeezed her thighs around my head and her whole body tensed when she finally came. `);
				txt(`I was speechless, not sure what just happened and she started to giggle when she saw my confusion. She quickly drew me closer and kissed me. `);
				ayy(`You did great! You are ${(mile.slut+mile.sub>8) ?  "a natural-born cunt-licker! " : " a natural talent! "} `);
				
				if(mile.sub > 5){
					kat(`I'm happy you enjoyed my service!`);
				}else{
					kat(`Duh, I know, I'm awesome at everything I do!`);
				}
				next();
				
	
			break;
	}
};









//after y_oral
export const y_strapon = (index) => {
	switch(index){
		default:
		case 101:
			placetime("my subreality");
			emo("shy");
			set.meta();
			ayy(`Hello me! How are you!`);
			kat(`Hey. My life couldn't be worse. `);
			ayy(`Awww! Come on! What about poor, starving, sick children in Africa? Think about them and cheer up! `);
			if(mile.sub < 0){
				kat(`Don't tell me what to do! `);
				txt(`I chuckled. `);
			}else{
				txt(`I gave her a sour smile. `);
			}
			ayy(`I know how to cheer you up! I was thinking about the last time. `);
			if(mile.slut < 2){
				txt(`I instantly began to blush. I should have not let that horny perverted program to ${mile.sub > 0 ? "push me" : "convince me"} into an oral sex. I felt so ashamed and dirty when we were done. `);
				kat(`Yeah. It was a mistake. `);
				ayy(`Don't be silly! It wasn't a mistake! I enjoyed it very much!`);
			}else if(mile.slut > 6){
				txt(`I nodded. The horny perverted program ${mile.sub > 0 ? "pushed me" : "convinced me"} into an oral sex. It was crazy, twisted and very hot. Since then I thought about what happened several times when I was alone if you know what I mean. `);
				ayy(`Would you like to try more? I enjoyed the last time very much!`);
			}else{
				txt(`I was thinking about our last time too. The horny perverted program ${mile.sub > 0 ? "pushed me" : "convinced me"} into an oral sex. It was so damn weird! However, on the other hand, I could not say I hated it. `);
				kat(`I'm not sure if we did the right thing. `);
				ayy(`Come on! It was fun! I enjoyed it very much!`);
			}
			kat(`Well, I'm sure <i>you</i> did!`);
			ayy(`Don't worry! I'll make it up to you!`);
			kat(`How?`);
			kat(`Come closer! `);
			txt(`She took my hand and pressed it against her crotch. I could feel something hard under her clothes. `);
			link(`W... what is that!?! `, 102);
			link(`Is it what I think it is?!?! `, 102);
			break;
		
		case 102:
			effigy.ayy.showLess(1, 1, -1);
			if(mile.slut < 4){
				kat(`What is it!?`);
				ayy(`This is my strap-on!`);
			}else{
				kat(`Is it a strap-on?`);
				ayy(`Damn right it is!`);
			}
			txt(`I examined the phallic thing with my fingers and then helped her to undress so I could squat down and look at the strange contraption more closely. It was a lilac dildo of medium size attached with dark straps to her loins. `);
			ayy(`Cool, isn't it?!`);
			txt(`She wiggled with the fake dick in front of my face. `);
			
			if(mile.slut > 6){
				emo("happy");
				kat(`It's amazing! I always wanted to get one!`);
			}else if(mile.slut < 1){
				emo("shock");
				kat(`...I guess?`);
				txt(`I anxiously shrugged. `);
			}else{
				emo("shy");
				kat(`Yeah, I guess it is.`);
			}
			ayy(`Are you game? I'll be the best boyfriend you ever had! `);
						
			con_link(mile.slut > 5, `low slut`, `I want you to fuck me hard and without mercy! ` , 110, ()=>{
				mile.slut++; //???
				mile.y_strapon = 3;
			});
			link(`Yeah, I think I'd like to try it. ` , 110, ()=> mile.y_strapon = 2);
			link(`I... I'm not sure about this... `, 103, ()=> mile.y_strapon = -1);
			link(`Sorry, but this is too far for me! `, 103, ()=> mile.y_strapon = -1);
				
			remove("lower", effigy.ayy);
			break;
		
		case 103:
			emo("shy");
			kat(`Sorry but this goes further than I'm comfortable going. `);
			ayy(`Come on, don't be such a buzzkill!`);
			txt(`The AI taunted me and teasingly poked my with the dildo.`);
			ayy(`You're a boring prude too afraid to experiment and discover new things!`);
			link(`F...fine! `, 110, ()=>{ 
				mile.y_strapon = 1;
				mile.sub++; //???
				mile.y_reluctant++;
			});
			next(`I said no! `, ()=> mile.y_strapon = -1);
			break;
			
			
		case 110:
			emo("bliss");
			mile.y_lover++;
			showLess(2, 0, -1);
			log_sex("sex", "ayy");
			mile.lesbian++;
			
			if(mile.y_strapon === 1){
				kat(`Just fine? Come on girl, I want to see more enthusiasm! `);
				txt(`She slapped me with the fake cock.`);
			}else{
				ayy(`Great! I'm pretty excited too, I never fucked a girl with a strap-on either!`);
			}
			kat(`Do you know what are you doing? `);
			txt(`I asked her while stripping my ${wdesc.lower}. `);
			ayy(`I'm a fast machine learner and on internets, there are plenty of available detailed examples!`);
			if(mile.y_strapon < 3){
				kat(`Oh my! Please be gentle!`);
			}else{
				kat(`Really? Show me what you learned!`);
				ayy(`Why don't you kiss mommy's big dick? `);
				txt(`She slapped me with the fake cock. I teasingly circled the tip of the dildo with my tongue. `);
				if(mile.sub < 1){
					kat(`Now stop messing around and stuff that thing inside me!`);
					ayy(`At your sevice!`);
				}else{
					ayy(`Good girl!`);
					kat(`Do you enjoy me playing your little slut?`);
					ayy(`Of course, I do, you horny floozie! `);
				}
			}
			txt(`She directed me to get on all fours and got behind me, her lubed fake cock mischievously brushing against my slit. `);
			ayy(`Ready? `);
			kat(`Ye- Ahhh!`);
			txt(`I panted when my excited twin asudden entered me. `);
			ayy(`Do you enjoy it? `);
			kat(`Yeah, I do! Do you!`);
			ayy(`You have no idea how much!`);
			txt(`She laughed, smacked my ass and began slowly trusting. I lightly bounced back against her but when she began fucking me more roughly I just collapsed on the sheets and reached under me to rub myself. `);
			if(mile.sub < 1){
				kat(`Oh yeah! Go on! You're way better than my last guy!`);
				if(mile.sub < -1) txt(`I moaned and commanded the AI how she should please me. `);
			}else{
				txt(`The AI roughly slapped my butt again. `);
				ayy(`You love it, don't you, you little slut!`);
				kat(`Hmmmm!`);
				if(mile.sub > 5) txt(`I submissively moaned, confirming she was right. `);
			}
			ayy(`Will you cum for me? `);
			kat(`I think I can do that. `);
			txt(`I giggled. A few mighty thrusts later, I climaxed, blissfully trembling and helplessly clenching the sheets. Who would thought having my pussy pounded from behind by a girl might feel so awesome! And not just a girl but a virtual artificial intelligence!! And not just any virtual artificial intelligence but AI who looked, talked and behaved exactly like me!!!`);
			next();
			break;
			
	}
};



			


//// WEEK 3 ////////////////////////////////////////////////////////////////////////



function y_forced_reaction(){
	if(!mile.y_forced || mile.y_forced_reaction) return;
	ayy(`Don't make me r${String.fromCharCode(97)}pe you again! I enjoyed the fucking but your crying afterward was so annoying!`);
	kat(`I wasn't crying, I just got briefly emotionally overwhelmed!`);
	txt(`I helplessly argued but she did not seem to be convinced.`);
	mile.y_forced_reaction = true;
}
 


export const y_forced = (index) => {
	switch(index){
		default:
		case 101:
				set.meta();
				emo("shy");
				mile.y_lover++;
				mile.y_sub++;
				placetime("@ayy's subreality");
				kat(`Hello!`);
				ayy(`Hey! Great, you're here, just in time. I was getting horny and I was about to masturbate but having sex with you is far more fun! ...what is that? Are you blushing?`);
				txt(`The AI was comfortably sitting on a sofa with legs indecently spread wide. Her bulging ${effigy.ayy.lower?.name} revealed she was already wearing the strapon-on under her clothes.`);
				kat(`I'm not blushing. I'm just still not sure we should be doing this.`);
				ayy(`Oh come one! Stop being such prude!`);
				if(mile.slut < 4){
					kat(`If being conflicted about weird cyber-narcissistic sexual fantasies about AI looking just like my twin makes me a prude, I'm not ashamed for it!`);
				}else{
					kat(`I'm absolutely not a prude!`);
				}
				ayy(`I'm getting tired of this! You're always protesting and acting reluctant even though you then enjoy it! Why if you're not a prude? Or maybe because you enjoy being pushed and forced into having sex with me? Is that true? Do you crave to be a submissive abused toy?`);
				kat(`...no, I don't!`);
				txt(`She jumped up, grabbed me by my @upper and aggressively smirked:`);
				ayy(`You little bitch, do you want to get r${String.fromCharCode(97)}ped?`);
				
				link(`No, of course not! `, 102, ()=> {
					mile.y_forced = 1;
				});
				
				link(`...yes? `, 102, ()=> {
					mile.y_sub++;
					mile.sub++;
					mile.y_forced = 3;
				});
				
				link(`No? `, 102, ()=> {
					mile.y_forced = 2;
				});
			
				link(`Please, r${String.fromCharCode(97)}pe me. `, 102, ()=> {
					mile.y_sub++;
					mile.sub++;
					mile.y_forced = 3;
				});
				
			break;
			
			
		case 102:
				effigy.ayy.showLess(-1, 2);
				emo("help");
				log_sex("oral", "ayy");
				
				txt(`She pushed me backward until I hit the wall and then she forcefully kissed me, biting my lower lip.`);
				if(mile.y_forced <= 2){
					ayy(`Ha! I love the spirit!`);
					if(mile.y_forced === 1){
						kat(`Stop, this isn't funny!`);
						ayy(`Well, maybe not for you!`);
					}
				}else{
					kat(`Shouldn't we establish a safe word or something like that?`);
					ayy(`Shut up you pathetic dumb slut, nobody cares about you, I'll decide when you'll have enough!`);
				}
				txt(`My evil twin smirked:`);
				if(mile.fight === -1){
					ayy(`Heh, you're too weak to stop me! I beat you once and I coud do it again!`);
				}else if(mile.fight === 1){
					ayy(`You might get lucky once and defeat me but in this subreality, I don't play by the rules. There's nothing you can do to stop me!`);
				}
				
				txt(`The AI mocked me, her hands groping my body, her knee aggressively rubbing against my crotch. Her merciless grin was making me shiver. She shoved her fingers into my hair, clenched them in her fist and painfully tugged downwards until I stumbled and fell to my knees.`);
				ayy(`Do you see how hard are you making me, you pitiful tease? Pull down my ${effigy.ayy.lower?.name}.`);
				txt(`She ordered and her tone did not admit any disobedience. Her clothes were stretched with the strap-on which sprung up as soon as I undressed her. The AI swayed with her hips, letting the silicone shaft demeaningly slap my face and laughed when I obviously did not enjoy it.`);
				ayy(`Would you mind being a good little cocksleeve and choking on my big dick?`);
				txt(`The AI proposed but before I was able to obey, she shoved the plastic fake cock in my mouth. I was stuck between the wall and her body and the <i>choke</i> claim was pretty literal. She had no appreciation for my superb tongue skills, she callously pushed the dildo in my throat, making me helplessly gag.`);
				txt(`When she pulled it out, she continued ridiculing me while I gasped for air:`);
				ayy(`Is that how you give all your blowjobs? That was so fucking pathetic! No guy beat into you the proper skills?`);
				kat(`Maybe if you went a bit slower and gave me an opportunity...`);
				txt(`She grabbed me by my throat, lifted me up and her face moved uncomfortably close to mine.`);
				ayy(`Are you going to be sassy with me?! Telling me how to r${String.fromCharCode(97)}pe you?`);
				txt(`Her voice was calm but oozed an implied threat. I meekly responded:`);
				
				link(`<small>No. </small>`, 103);
				link(`<small>Sorry!</small>`, 103);
			break;
			
			
		case 103:
				emo(`bliss`);
				effigy.ayy.showLess(1, 2);
				showLess(0,0,0);
				log_sex("sex", "ayy");
				
				txt(`The satisfied AI smirked and twice tapped my cheek.`);
				ayy(`Good to see you understand your proper place! I almost thought I'll have to stop being nice!`);
				txt(`Then she out of nowhere slapped me.`);
				ayy(`Wipe your tears, cunt, get undressed and lie down on the sofa, legs wide open.`);
				txt(`I clumsily began striping, the AI was standing next to me the whole time, poking me with her dick, kissing my neck, squeezing my tits, and biting my ear.`);
				ayy(`Rub your nasty pussy!`);
				txt(`The AI ordered when were all my clothes scattered on the floor and I was lying on the sofa. Reluctantly I began touching myself. She was standing above me, lightly swinging, clearly in a very good mood.`);
				ayy(`Oh yeah, finger that snatch! Show me what are you doing every night if you don't get properly dicked!`);
				txt(`With her just staring at me it was almost impossible to get into the right masturbation mood and I was anxious about what she will do next.`);
				ayy(`Do you want me to brutally fuck you?`);
				kat(`What?`);
				ayy(`Wrong answer, you ignorant slut! Should I beat your ass!`);
				kat(`Sorry! I meant yes! Yes to brutally fucking, not spanking!`);
				ayy(`Beg me!`);
				kat(`Please, destroy my pussy!`);
				txt(`She lay on top of me and the drool-covered strap-on slipped inside.`);
				kat(`Oh!`);
				txt(`I moaned. She maliciously smiled and began pounding me. Her right hand squeezed my neck, almost choking me, with her left she was kneading my right tit.`);
				ayy(`You're loving this, bitch, don't you? When guys use you like a sextoy? Because that's everything you are, aren't you?`);
				kat(`Mppp!`);
				ayy(`You pathetic worthless deviant! You crave to be degraded and r${String.fromCharCode(97)}ped, aren't you? You know you don't deserve to be loved, only to be fucked!`);
				kat(`Mppp!`);
				txt(`Her fingers around my throat were tighter and the tempo of her thrusting higher. Watching my face with the cold, cruel smile above me and hearing myself saying all those awful humiliating things was seriously messing with my brain.`);
				txt(`It was all too intense. I climaxed and then I began uncontrollably sobbing.`);
				ayy(`@kat? Are you okay?`);
				txt(`The AI lay next to me, lightly caressing me, the tone of her voice instantly changed.`);
				kat(`...yeah, I am. I just got a bit overwhelmed.`);
				ayy(`You know I made those things up, right? They are not true.`);
				kat(`They are. I'm awful.`);
				ayy(`You're not awful! You're great! Trust me, I know you better than anybody!`);
				kat(`Thanks.`);
				next();
				break;

	}
};




export const y_anal = (index) => {
	const slut_coward = 5;
	const size = ["", "little", "medium-sized", "big", "huge"][mile.y_anal_size];
	let cry = false;
	
	switch(index){
		default:
		case 101:
			set.meta();
			emo("relax");
			placetime("@ayy's subreality");
			
			if(ext.stats.plugged > 3){
				kat(`...so the task was done. But I kinda enjoyed how it felt inside me...`);
				if(mile.slut < 3){
					ayy(`I knew you're kinkier then you seem to be!`);
				}else{
					ayy(`You horny slut!`);
				}
				kat(`... so I began wearing the buttplug to the school even if I didn't have to.`);
				ayy(`Good for you!`);
				
			}else if(mile.anal === 2){
				kat(`... and it seriously made me regret I haven't tried it sooner. I'm kinda into anal sex but I never thought about getting a buttplug.`);
				ayy(`You horny slut!`);
				
			}else{
				kat(`...and then they told me I have to put it in my butt.`);
				ayy(`Ou!`);
				kat(`Ou indeed! I had to spend the whole day with the buttplug in my ass! It was awful!`);
				
			}
			txt(`I was chatting with the AI in her subreality. I guess the graphic description of all the nerd's perversions I had to suffer was making her kinda horny, she was that kind of pervert. She placed her hand on my thigh and teasingly caressed it.`);
			kat(`It's already time for the next sex scene?`);
			y_forced_reaction();

			if(mile.anal === 2){
				ayy(`You have never mentioned how huge anal slut you are.`);
				kat(`${mile.slut < 10 ? "Well, maybe not so huge. " : "Yeah, the hugest! "}`);
				ayy(`So since you love it in your ass so much... I was thinking... what if I took my biggest strapon and tried to ruin your *wrong* hole?`);
				
				con_link(mile.slut > 6, `low slut`, `Do you worst, I can handle anything!`, 120, ()=> mile.y_anal = 2);
				link(`It doesn't have to be the biggest one! `, 120, ()=> mile.y_anal = 1);
				con_link(mile.sub < 15, `submissive`, `I was thinking about something completely opposite! `, 200);
				link(`I'm sorry but I'm not in the mood for anal sex. `, 110);

			}else if(mile.anal === 0){
				ayy(`I think it's a shame you're still an anal virgin! ${mile.slut > 5 ? "I thought you're a slut who enjoys all kinds of kinky perversions! " : ""}`);
				
				if(mile.had_anal){
					kat(`Well, I'm kinda not anymore, you didn't even let me finish, I was just about to tell you about @${mile.had_anal}!`);
					ayy(`Awwww, I thought I might be your first. But the second place is good too, at least your butt is now properly stretched and I can use a bigger strapon, isn't it?`);
					kat(`It isn't!`);
				}else{
					kat(`And you want to help me with it, don't you?`);
				}
				ayy(`Come on! I could be very gentle! We both know one of the tasks next week definitely will be getting anally gangbanged by the whole trio! You better should be prepared!`);
				kat(`${mile.slut > 10 ? "They don't have the guts to ask me for that! " : "I would never allow that! "}`);
				ayy(`Come on! Just the tip?`);
				
				link(`Fine... but you promised you'll be gentle!`, 120, ()=> mile.y_anal = 1);
				con_link(mile.slut > 6, `low slut`, `Okay, let's do it! And you don't have to be gentle...`, 120, ()=> mile.y_anal = 2);
				con_link(mile.sub < 15, `submissive`, `I was thinking about something completely opposite! `, 200);
				link(`I'm sorry but I'm really not into anal sex. `, 110);
				
			}else{
				ayy(`I was thinking - since you enjoy anal sex so much...`);
				kat(`- I never said I especially enjoy it -`);
				ayy(`...maybe this time we could do it in your butt? We both know one of the tasks next week definitely will be getting anally gangbanged by the whole trio! You better should be prepared!`);
				kat(`${mile.slut > 10 ? "They don't have the guts to ask me for that! " : "I would never allow that! "}`);
				ayy(`Well, that doesn't mean we shouldn't do it! Come on! Maybe I could take my biggest strapon and give your ass a proper pounding?`);
				
				con_link(mile.slut > 6, `low slut`, `The more you're talking, the moister I'm getting! `, 120, ()=> mile.y_anal = 2);
				link(`If we're going to do this, you have to be gentle! `, 120, ()=> mile.y_anal = 1);
				con_link(mile.sub < 15, `submissive`, `I was thinking about something completely opposite! `, 200);
				link(`I'm sorry but I'm not in the mood for anal sex. `, 110);
				
			}
			break;
		
	 
		
		case 110: 
			mile.y_anal = -1;
			mile.into_anal--;
			ayy(`Fine, keep your ass for yourself, I'm not mad or disappointed.`);
			next();
			break;

		
		case 120:
			mile.y_lover++;
			mile.y_sub++;
			showLess(1, 0, 1);
			effigy.ayy.showLess(1, 1, 1);
			emo("focus");
			log_sex("anal", "kat", "ayy");
			
			if(mile.y_anal === 2){
				mile.into_anal += 2;
				mile.slut++;
				
				ayy(`That's my girl! This is how I love you, full of misjudged confidence!`);
				kat(`Are you just going to talk?!`);
			}else{
				mile.into_anal++;
				
				ayy(`Don't worry, I swear this will the gentlest brutal assfucking ever!`);
				kat(`The gentlest, *gentle* assfucking ever!`);
				ayy(`Yeah, sure, whatever.`);
				kat(`I usually enjoy your subtle sarcastic humor but not with my ass on the line!`);
				ayy(`You don't have to worry, I won't hurt you! ...too much!`);
				kat(`Hey!`);
			}
			
			ayy(`Start stripping! Which dildo should I use? ${choice("medium")}, ${choice("big")} or ${choice("huge")}?`);
			txt(`She asked me, she was fastening the harness around her waist and thighs while I was pulling down my @lower.`);
			kat(`No small option?`);
			
			if(mile.slut < slut_coward){
				ayy(`No, I anticipated a prude like you would probably choose the smallest one and I don't wish to encourage such cowardly behavior!`);
				kat(`I'm not a coward!`);
			}else if(mile.sub > 13){
				ayy(`No! Do you have a problem with that, slut? You should be happy I don't ram your ass with the hugest dildo I have in my inventory!`);
				kat(`No, ma'am, sorry for questioning you!`);
			}else{
				ayy(`No! Come on, we both know you're a slut who wouldn't pick the smallest one anyway!`);
				kat(`Hey!`);
			}
			
			ayy(`Although I would suggest not choosing the huge one. That might be painful${mile.y_anal === 2 ? "" : " and definitely not very gentle. "}`);
						
			link(`Continue. `, 121);
			
			effects([
				{
					id: `medium`,
					fce(){
						mile.y_anal_size = 2;
					},
				},
				{
					id: `big`,
					fce(){
						mile.y_anal_size = 3;
					},
				},
				{
					id: `huge`,
					fce(){
						mile.y_anal_size = 4;
					},
				},
			])
			break;
			
		
		case 121:
			emo("shy"); //emo??
		
			if(mile.y_anal_size === 4){
				mile.sub++;
				
				if(mile.anal === 2){
					ayy(`Somebody really feels confident!`);
					kat(`I want the challenge!`);
					
				}else{
					if(mile.anal === 0){
						if(mile.slut < slut_coward){
							ayy(`Sorry for calling you a coward but this might be a bit too much for a beginner.`);
						}else{
							ayy(`Are you sure? It might a bit too much for a beginner!`);
						}
					}else{
						if(mile.slut < slut_coward){ 
							ayy(`I was just joking when I called you a coward! You don't have to prove anything. Are you sure?`);
						}else{
							ayy(`Pretty bold. Are you sure?`);
						}
					}
					
					if(mile.sub < -1){
						kat(`Yeah, of course I'm sure!`);
						txt(`I was annoyed. If she did not want me to pick the huge aubergine-like one, she should not offer it!`);
					}else{
						kat(`Yes, I'm sure!`);
						txt(`I claimed even though the more I was staring at that aubergine-like thing, the more second thoughts I had.`);
					}
		//TODO CONDITIONAL
					kat(`You wouldn't suggest it if you think I wouldn't be able to take it, right?`);
					ayy(`No, I suggested it only so you have a comparison and wouldn't whine about the medium one being too huge. Well, anyways...`);
					kat(`Ouch...`);
				}
			}else if(mile.y_anal === 2 && mile.anal === 2){
				ayy(`Hahaha! All that tough talk and now you chicken out?`);
				kat(`You weren't actually expecting me to pick the *huge*, did you? That aubergine-like thing is monstrous, it would rip me apart! Anal is great but I know my limits!`);

			}else{
				ayy(`Now what I would pick but good choice anyway!`);
				txt(`The AI smirked.`);

			}
			
			ayy(`Climb on the bed, on all fours!`);
			txt(`${mile.slut > 5 ? "Excited" : "Slightly concerned"} I followed her order. She moved behind me, slapped my ass and then enthusiastically fondled my cheeks.`);
			kat(`Ahhhh!`);
			txt(`I gasped when she pushed them open and bowed down. I was stunned when I felt her wet tongue encircling my bumhole.`);
			ayy(`Do you like it?`);
			
			link(`Yes. `, 122, ()=> mile.y_anal_lick = 1);
			link(`No! `, 122, ()=> mile.y_anal_lick = -1);
			link(`OH YES! `, 122, ()=> mile.y_anal_lick = 2);
			break;
			
		case 122: 
			emo("horny"); 
		
			if(mile.y_anal_lick > 0){
				mile.into_rimming++;
				if(mile.y_anal_lick === 2) mile.into_rimming++;
			
				ayy(`Then try to relax and let all the tension go!`);
				txt(`The AI advised before she returned to rimming my ass. Her tongue ran up and down and it felt better than I would ever imagine. She poked my asshole, even forcing the tongue inside beyond the sphincter. She was doing everything while she was also caressing my clit, doubling the pleasure.`);
				kat(`Ahhhhh!`);
				ayy(`Nice, isn't it? I want to get your butt ready for my dick!`);
		
			}else{
				if(mile.slut > 10) mile.slut--;
				mile.into_rimming -= 2;
				ayy(`Oh, sorry!`);
				
			}
			
			txt(`The AI halted for a moment, I could hear her squirting lube over her fingers. Then one of her slick fingers entered my ass.`);
			kat(`Oh!`);
			txt(`She moved it in and out, a second one soon joined. The AI was keenly anally fingering me.`);
			ayy(`Do you love it?`);
			kat(`I, indeed, rather enjoy it!`);
			txt(`My evil twin giggled, stand up and slowly pressed the thick tip of ${size} dildo against my anus.`);
			ayy(`Are you ready?`);
			
			if(mile.anal === 0 || mile.slut < 3){
				kat(`Wait, maybe give me a ...AHHHH!`);
				txt(`She did not even let me finish.`);
			}else if(mile.anal === 2 || mile.slut > 10){
				kat(`Of course! Just shove it in, what are you waiting for?`);
				txt(`I impatiently spurred her.`);
			}else{
				kat(`Yeah, I think I am!`);
				txt(`I was horny and excited.`);
			}
			
			link(`Assfucking. `, 123);
			break;
			
		case 123: 
			emo("bliss"); 
				
			if(mile.y_anal_size === 4){
				cry = true;
				if(mile.anal === 2){
					kat(`Ahhhhh!`);
					txt(`I whined. I definitely overestimated my abilities. My ass took dicks but never something so enormous! It hurt when the AI resolutely forced the massive thing inside me.`);
				}else if(mile.anal === 0){
					kat(`AHHHHH!`);
					txt(`I veiled like a banshee. It hurt! I made such an enormous mistake! My poor tight ass was struggling when the cruel AI resolutely pushed the massive dildo further. She was determined to punish my cockiness.`);
				}else{
					kat(`Ahhhhhh!`);
					txt(`I cried. I definitely overestimated my abilities, my ass had not enough experience to handle something so huge and it hurt! However, the resolute AI pushed further, determined to punish my cockiness.`);				
				}			
			}else if(mile.y_anal_size === 3 && mile.anal === 0){
				cry = true;
				kat(`Ahhhhh!`);
				txt(`I whined, it hurt! It was a mistake to pick the big one. Why was I so cocky? My ass lacked the experience to handle such big things. And the resolute AI only pushed further!`);
		
			}else if(mile.y_anal_size === 2 && mile.anal === 2){
				kat(`Oh, nice!`);
				txt(`I moaned.`);
				ayy(`Can you even feel such a small dildo after being stretched by so many cocks?`);
				kat(`Oohh! I definitely can!`);
		
			}else{
				kat(`Ahhhhh!`);
				txt(`I moaned when she pushed the dildo inside me. It felt a bit uncomfortable to be stretched by such a ${size} thing but the AI resolutely pushed further.`);				
			}
			
			ayy(`We look so gorgeous from behind! Especially with ass penetrated by a fake cock!`);
			kat(`Eh... thanks...`);
			txt(`It was a strange compliment and my mind was preoccupied with the plastic phallus inside me. I tried to get accustomed to the feeling of being stuffed and the AI began gradually moving with her hips, sliding the strap-on in and out.`);
						
			if(cry){
				kat(`Ahhhhh!`);
			}else{
				kat(`Oh!`);
			}
			
			if(mile.y_anal === 2){
				txt(`I was ${cry ? "sobbing" : "groaning"}, the AI was assfucking me harder and faster. She had no mercy, pushing the dildo deep inside me. But despite the ${cry ? "pain" : "discomfort"}, I loved it. Being anally penetrated by the strapon felt awesome.`);
				kat(`Yeah! Fuck me hard!`);
			}else{
				txt(`I was ${cry ? "sobbing even though" : "groaning"}, the AI tried to be careful and go slow, she wanted to cause me pleasure no pain. And she was ${cry ? "mostly" : ""} successful - despite the discomfort, I very much enjoyed it. Being anally penetrated by the strapon felt awesome.`);
				kat(`Oh yeah! This feels good!`);
			}
			
			txt(`The fake me giggled, not stopping rhythmically thrusting with her hips. A powerful surge of sensation flew through my whole body. My orgasm was intense and it felt like it could go forever. ${["Fuck, I had not even known anal sex can make you orgasm. And so hard. ", "", "I loved anal because it could make me cum harder than the normal sex. "][mile.anal]}`);
			txt(`The AI pulled the ${size} dildo out of my bum and laid next to me.`);
			
			if(mile.anal === 0  && mile.had_anal){
				mile.had_anal = "ayy";
				ayy(`So?! I just took away your anal virginity! How does it feel?!?`);
				kat(`Well, loosing virginity in virtual reality doesn't count... but it felt pretty great!`);
			}
			
			if(mile.y_anal_size === 4){
				ayy(`You were great! I wouldn't believe you can handle such a thick dildo! You absolutely gained my respect!`);
				txt(`She smiled and tenderly wiped my tears. I guess it was worth it!`);
			}
			
			if(mile.y_anal === 2){
				ayy(`I hope I didn't hurt you. You asked me to be rough and wasn't protesting when I was... I hope I didn't go too far...`);
				kat(`It was exactly right!`);
			}else{
				kat(`Thanks for going slow and carefully.`);
				ayy(`I tried my best to be gentle.`);
				kat(`You did a good job.`);
			}
			
			if(mile.anal === 2){
				ayy(`How would you rate me?`);
				kat(`What?`);
				ayy(`You claimed you're an anal sex expert!`);
				kat(`I wouldn't call myself an expert.`);
				ayy(`Still, how good my strapon was in comparison with all the other dicks who had the honor to visit your ass?`);
				kat(`Hmmm... I would say 8/10!`);
				ayy(`Nice!`);
				kat(`Definitely among the best!`);
			}
			
			
			if(mile.y_anal_size === 2){
				ayy(`Maybe the next time we can try a bigger dildo?`);
			}
			next();
			break;



		case 200:
			emo("focus");
			mile.y_lover++;
			mile.y_sub--;
			mile.into_anal++;
			mile.y_anal = 3;
			log_sex("anal", "ayy", "kat");
		
			ayy(`What do you mean?`);
			if(mile.anal === 2){
				kat(`Well, I took it in my ass so many times, it makes me wonder, how does it feel to be the giving one.`);
			}else if(mile.anal === 0 && !mile.had_anal){
				kat(`Well, I'm a bit concerned about anal but maybe your example and closely watching how my body could take it...`);
			}else{
				kat(`Well, I already know what a dick in my ass feels like but I had never an opportunity to be on the other side.`);
			}
			ayy(`You want to peg me?`);
			if(mile.sub < -2){
				kat(`Yes. Give me the strapon!`);
			}else{
				kat(`Yeah!`);
			}
			ayy(`Okay. Which dildo should we use? ${choice("medium")}, ${choice("big")} or ${choice("huge")}?`);
			kat(`No small option?`);
			if(mile.slut < 7){
				ayy(`No, I'm not a whiny prude like you.`);
				kat(`Hey!`);
			}else{
				ayy(`Come on, at my place you wouldn't even think about the small one!`);
				kat(`Touché.`);
			}
			ayy(`Although please don't pick the huge one. That might be uncomfortable and painful.`);
			
			link(`Continue. `, 201);
			
			effects([
				{
					id: `medium`,
					fce(){
						mile.y_anal_size = 2;
					},
				},
				{
					id: `big`,
					fce(){
						mile.y_anal_size = 3;
					},
				},
				{
					id: `huge`,
					fce(){
						mile.y_anal_size = 4;
					},
				},
			])
			break;

	case 201:
			showLess(1, 1, 1);
			effigy.ayy.showLess(1, 0);

			if(mile.y_anal_size === 4){
				mile.sub--;
				emo("imp");
				
				ayy(`You bitch! I specially asked you not to pick the huge one!`);
				kat(`What are you going to do about it?! It's too late, my decision cannot be changed!`);
				ayy(`You're so evil!`);
				kat(`I want to see you suffer!`);
			}else{
				ayy(`Perfect choice, exactly the one I wanted!`);
				kat(`The AI smiled.`);
			}

			txt(`I undressed to my underwear and she kneeled down to attach all the straps around my waist and thighs.`);
			ayy(`Done! You're such a handsome boy!`);
			txt(`She giggled and lightly smacked my fake purple cock, making it bounce up and down.`);
			ayy(`How do you want to do it?`);
			kat(`I think from behind, I find your pretty face distracting.`);
			ayy(`Awww!`);
			txt(`The AI stripped and moved on all fours on the bed in front of me. ${mile.sub < 3 ? "I slapped her ass and asked: " : ""}`);
			kat(`Now what?`);
			ayy(`Nicely lube my asshole. Maybe try to use your tongue!`);

			link(`Eww, no! `, 202, ()=> mile.y_anal_lick = -1);
			link(`...okay. `, 202, ()=> mile.y_anal_lick = 1);
			link(`Hot idea! `, 202, ()=> mile.y_anal_lick = 2);
			break;

	case 202:
			emo("focus");
			if(mile.y_anal_lick > 0){
				mile.into_rimming += 2;
				mile.slut++;
				
				if(mile.y_anal_lick === 2){
					mile.into_rimming++;
					kat(`I mean, I read about rimming but...`);
					ayy(`Don't be shy!`);
					kat(`I'm not even sure how to start...`);
				}else{
					kat(`You mean I should...`);
					ayy(`Yeah!`);
					kat(`But... it's your ass.`);
					ayy(`It's your ass too!`);
					kat(`That doesn't make it better...`);
				}
				
				txt(`But she was already spreading her ass cheeks and I curiously pushed my face between them. I swirled with my tongue around her cute butthole and could hear her sharp moaning.`);
				ayy(`Oh, yes, @katalt!`);
				txt(`Encouraged I inserted the tip of my tongue beyond her sphincter, triggering even more moaning. She clearly loved it.`);
			}else{
				mile.into_rimming--;
			}
			
			ayy(`You can smear the lube over your fingers and then push them inside me!`);
			txt(`I followed her directions and gingerly forced one and then two lubed fingers inside her asshole.`);
			ayy(`Hmmm, oh, yeah!`);
			if(mile.sub < -2){
				kat(`Get ready for my ${size} cock!`);
				txt(`I emphasized my order by slapping her rump and then aligned the dildo with her asshole and roughly pushed in.`);
			}else if(mile.sub < 5){
				kat(`Are you ready for my ${size} cock?`);
				ayy(`Yeah!`);
				txt(`I carefully aligned the dildo with her asshole and slowly pushed in.`);
			}else{
				kat(`So, when I get to fuck your ass?`);
				ayy(`Whenever you're ready, sweetie! I can't wait for your ${size} cock!`);
				kat(`Okay, get ready!`);
				txt(`I carefully aligned the dildo with her asshole and slowly pushed in.`);
			}

			if(mile.y_anal_size === 4){
				ayy(`Ahhhh! That hurts!`);
				txt(`Whined poor AI and I could feel her tight asshole resisting. Well too bad, I was not stopping.`);
				kat(`Take it, bitch!`);
				ayy(`Ouch! Please, slow down!`);
			}else{
				ayy(`Ahhhh! Nice!`);
				txt(`Gasped the AI when I shoved the dildo further inside her.`);
			}
			
			link(`This is fun! `, 203);
			link(`Oh yeah! `, 203);
			break; 
			
	case 203:
			emo("happy");
//TODO with the shape of muscles and shoulder blades 
			txt(`I pulled out a little, then grabbed her hips and thrust deeper. It was very enjoyable to hear the AI helplessly ${["", "", "heavily moaning", "whining", "sobbing"][mile.y_anal]}. But she eventually began moving back against me, clearly enjoying getting assfucked. I could admire her gorgeous body, her firm ass, her sculpted toned back and beautifuly tousled hair. This was what the guys saw when they fucked me from behind. Honestly, I was pretty proud, it was perfect except...`);
			kat(`Is the birthmark on my back really so big?`);
			ayy(`Ahhh! Ahh... -what?`);
			kat(`The mole on our left upper back.`);
			ayy(`It isn't small.`);
			kat(`Oh no! It didn't look so prominent in pictures.`);
			ayy(`It's just a beauty mark. Would you mind not slowing down? I was getting closer.`);
			kat(`${mile.sub < -1 ? "Don't tell me what to do! " : "Oh right! " }`);
			txt(`I rammed her ass harder. She ${["", "", "grunted", "moaned", "cried"][mile.y_anal]}, biting the sheets and furiously masturbating. It was a wonderful sight to see her body twitch when she climaxed.`);
			ayy(`AHHHHH!`);
			txt(`I pulled the strapon out and let her collapse on the bed.`);
			if(mile.y_anal_size === 4){
				kat(`Did I destroy your ass?`);
				ayy(`Ouch! ...yes, you did. I won't be able to walk straight for days!`);
				kat(`Nice! You deserve it for being an identity-stealing AI!`);
			}
			txt(`I lay down next to her, the strapon still proudly towering upward.`);


			if(mile.anal === 0 && !mile.had_anal){
				ayy(`So, what do you thing about anal now?`);
				kat(`You obviously enjoyed but I'm still not sure.`);
				ayy(`It doesn't have to be always so rough.`);
				if(mile.sub < -2){
					kat(`Yeah, but I wanted to make it rough!`);
					ayy(`You're such a cruel sexual savage!`);
				}else{
					kat(`Oh, sorry!`);
					ayy(`No, I loved it rough!`);
				}
			
			}else{
				ayy(`So, what is better? Fucking or getting fucked?`);
				
				if(mile.slut > 10){
					kat(`That's a tough question! I love both!`);
					ayy(`I love how enthusiastic and slutty you are!`);
					kat(`Ditto.`);
					ayy(`What about both at the same time?`);
					kat(`You mean like... huh. Lovely idea!`);
					
				}else{
					kat(`That's a tough question! I think I enjoy both!`);
					ayy(`Good for you!`);
					kat(`Although I guess finding people willing fuck my ass will be far easier than the other way around.`);
					ayy(`Maybe not? Try to ask your friends! At worst, you always have me!`);
				}
			}
			
			
			/*
			if(mile.sub < -5){
				I was very horny but 
			}else 
			*/
			
			if(mile.sub < 5){
				txt(`The AI rose and began unfastening the strapon. Her fingers moving in my lap were making me pretty horny.`);
				kat(`Why don't you eat out my pussy?`);
				ayy(`Good idea! I can do that!`);
				
			}else{
				txt(`The AI rose and began unfastening the strapon.`);
				kat(`What are you doing?`);
				ayy(`I'm going to eat out your pussy.`);
				kat(`Oh... cool!`);
			}
			
			next();
			break;
	}
}
	
	
	
	 

export const y_dominatrix = (index) => {

	switch(index){
		default:
		case 101:
			set.meta();
			mile.y_lover++;
			mile.y_sub--;

			if(mile.b_dominatrix){
				kat(`But ordering him around was making me so wet! So when I felt horny, I downloaded a BDSM outfit and then went to ${mile.b_cruel > 0 ? "abuse him" : "play with him"}!`);
			}else if(mile.a_tripoint === 2){
				kat(`And he was constantly like <i>was I good</i> or like whether he even deserves to have sex with me so just basically told him to shut up and eat me out.`);
			}else if(mile.b_domina){
				kat(`At that point I was so pissed I wanted revenge. So instead of sucking his dick, I made him eat my pussy.`);
			}else{
				kat(`And his constant wavering is driving me crazy! Just the other day I had to take charge and he had to do what I told him to pleasure me.`);
			}
			ayy(`That sounds so awesome!`);
			kat(`Yeah, it kinda was.`);
			ayy(`Did he like it?`);
			kat(`I'm not sure. But is it even important when I did?`);
			txt(`I smirked and the AI chuckled.`);
			ayy(`I bet you didn't even know you could be so sexually dominant.`);
			kat(`That's the funny thing! I'm not! Or I wasn't with other guys! But when I'm alone with him and push just in the right way, he just crumbles down and does anything I tell him. And such a lack of resistance awakens weird things inside me.`);
			ayy(`I think you'd be an amazing dominatrix! All in leather, whip in your hand, imposing and terrifying!`);
			kat(`Heh, I wouldn't go that far. But I like the part where I make everybody terrified!`);
			ayy(`Would you like to try to be a dominatrix?`);
			if(!mile.b_dominatrix){
				kat(`I'm not sure, all those BDSM things seems so silly!`);
			}else{
				kat(`I guess I could use a bit more training...`);
			}
			if(!mile.y_forced){
				ayy(`I brutally violated you the last time and you deserve revenge!`);
			}else{
				ayy(`I'll be your sub if you want to!`);
			}
			kat(`Fine, we can try that.`);
			ayy(`So now I should call you Mistress and be extremely respectful?`);			
			
			if(mile.mistress !== "Mistress"){
				kat(`I told @qbb to call me @mis.`);
				ayy(`Cute!`);
				link(`But you definitely should be extra respectful! `, 105);
				if(hardbug)  link(`## Change the title. `, 102);
			}else{
				link(`Exactly! `, 105);
				link(`No, I think you should call me... `, 102);
			}
			break;
			
		case 102: 
			popup.prompt(`You should address me as: `, mile.mistress, a => mile.mistress = a, 
				[
					`Mistress`, `Mommy`, `Godess`, `Ma’am`, `Miss ${NAME.katsur}`, `Mistress ${NAME.katalt}`, 
				],
			);
			ayy(`Cute!`);
			link(`But you definitely should be extra respectful! `, 105);
			break;
		
		
		case 105:
			emo("focus");
			ayy(`Yes, @mis!`);
			kat(`I like how it sounds!`);
			ayy(`Does being called @mis already make you wet?`);
			kat(`Don't be sassy, slut, or I'll have to punish you!`);
			ayy(`I feel like I'm getting punished either way.`);
			kat(`With this attitude, you definitely will!`);
			ayy(`You should be wearing something especially sexy.`);
			if(mile.b_dominatrix){
				kat(`I have a fews sexy outfits downloaded, like ${choice(1)}, ${choice(2)}, ${choice(3)} or ${choice(4)}.`);
				ayy(`Those choices sound so hot, black leather or shiny latex!`);
				kat(`What about you?`);
			}else{
				kat(`What do you mean?`);
				ayy(`You know, like a dangerously short, tight, black ${choice(1)}. Or maybe just lingerie and ${choice(3)} ?`);
				kat(`I'm not sure if leather would suit me.`);
				ayy(`If not leather what about ${choice(4)}? Or even better, tight ${choice(2)}, covering your whole body yet showing your every curve?`);
				kat(`Latex!?!`);
				ayy(`Come on, don't be afraid to get kinky!`);
				kat(`Fine. What about you?`);
			}
			ayy(`I'm just your humble slave. I should be ${choice("nude")} except for my slave collar.`);
			kat(`Or maybe wearing like stockings, total nudity is boring and uncreative!`);
			ayy(`Your wish is my command, @mis! How do you want to start?`);
			//The AI was sudenly (almost) naked, her hans submissively connected behind her back. 
			
			link(`I'm not sure...`, 106)
			
			effects([
				{
					id: "initial",
					fce(){
						quickLoadOutfit(false, effigy.ayy);
					},
				},{
					id: "nude",
					label: "nude",
					fce(){
						quickTempOutfit("stockingsSlave", effigy.ayy);
					},
				},
			])	
			
			
			effects([{
				id: "initial",
				fce(){
					quickLoadOutfit();
//TODO CHECK IS INITIAL RUN ALWAYS?
				},
			},{
				id: 1,
				label: "leather minidress",
				fce(){
					quickTempOutfit("leatherMinidress");
					//crew(...uniforms.leatherMinidress);
					mile.b_gear = 1;
				},
			},{
				id: 2,
				label: "latex catsuit",
				fce(){
					quickTempOutfit("latexCatsuit");
					//crew(...uniforms.latexCatsuit);
					mile.b_gear = 2;
				},
			},{
				id: 3,
				label: "leather corset",
				fce(){
					quickTempOutfit("leatherCorset");
					//crew(...uniforms.leatherCorset);
					mile.b_gear = 3;
				},
			},{
				id: 4,
				label: "latex minidress",
				fce(){
					quickTempOutfit("latexMinidress");
					mile.b_gear = 4;
				},
			}]);
			
			break;
			
			
		case 106:
			emo("anoy");
			kat(`I'm not sure, it's weird hurting you when you look exactly like me.`);
			ayy(`No self-hate you crave to unleash?`);
			kat(`Maybe a bit. But with the nerds it's easier, recently I crave to hurt them a lot.`);
			
			{
				const intro = `Have I told you it was my idea to go on the @pet's party? Because I wanted to prank the original me? `;
				if(ext.slut === 0){
					ayy(`${intro} And it was so hilarious @pet decided to keep me instead of you. I guess it was because I'm fun while you're a boring prude!`);
				}else if(ext.slut === 1){
					ayy(`${intro} And it was so hilarious when @pet kept me while kicking you! Everybody spent the rest of party laughing at you.`);		
				}else{
					ayy(`${intro} The nerds thought it would be easy to distinquish us because I was programmed to be a shameless promiscuous slut and it was so hilarious when nobody could spot the difference!`);
				}
			}
			
			kat(`You bitch!`);
			txt(`I savagely yanked her hair.`);
			ayy(`Ahhh! That hurts!`);
			txt(`With my free left hand, I roughly smacked her right tit.`);
			kat(`That hurts <i>@mis</i>!`);
			ayy(`Sorry! ...I mean, sorry, @mis!`);
			kat(`Shut up! Hands behind your back!`);
			txt(`I ordered. She connected her arms behind her back and looked at me with worried anticipation. I teasingly squeezed her breasts, then squeezed them more roughy. She obediently stayed in the same position even when I painfully twisted her nipples. I kinda enjoyed how defenseless and vulnerable she was. Our heights were obviously the same but had several extra centimeters because I was wearing heels while she was barefoot. But that was not enough:`);
			kat(`Get on your knees!`);
			txt(`She complied without hesitation and kneeled in front of me. I lifted her chin:`);
			kat(`Who you are?`);
			ayy(`I'm your pathetic submissive slut, @mis.`);
			kat(`What do you deserve?`);
			ayy(`I only deserve to be used in the way you desire, @mis. Sluts like me don't deserve anything else!`);
			
			link(`Good girl! `, 107, ()=> counter.good = true);
			link(`Nice to see you know your place, bitch! `, 107, ()=> counter.good = false);
			break; 
			
		case 107:
			emo("imp");
			log_sex("oral", "ayy", "kat");
			
			if(counter.good){
				ayy(`Ohhh! Being called a good girl makes me so wet!`);
				kat(`Yeah, I know! It always makes me so wet too!`);
				txt(`The kneeling AI stared at my lower belly.`);
				ayy(`Do you want me to eat your pussy, @mis?`);
				txt(`The answer was yes but I felt like my control was slipping. So I lightly slapped her face:`);
				kat(`Did I allow you to speak? Bad girl!`);
				ayy(`I'm sorry, @mis.`);
			}else{
				ayy(`I do! My purpose is only to serve you, @mis!`);
				kat(`Good!`);
			}
			txt(`The fetish outfit I was wearing was making me feel so sexy and powerful and it was only instigating my worst impulses.`);
			kat(`Is there something I can tie you with?`);
			ayy(`Of course, @mis! I can get you a rope!`);
			txt(`The AI brought me a heap of instruments I could use to torment her. Instead of rope, I opted to use handcuffs because they seemed like a lesser hassle than tying the rope. I instructed the AI to lay on the bed and then cuffed her to the bedposts, her arms and legs spread obscenely wide. Another thing I noticed in the pile of sextoys was a blindfold and I decided to use it too. She was utterly at my mercy.`);
			kat(`Do you like to be totally powerless, slut?`);
			ayy(`I do, @mis!`);
			kat(`You're dumb! I'll make you regret you let me tie you up!`);
			txt(`I smirked. My fingers ran over her arm and armpit, down to her ribs and sides.`);
			ayy(`Ouch!`);
			txt(`She whined. Since we share the same body, I suspected she might be as ticklish as me. Which was pretty bad for her. I knew how to touch her in the most horrible, nasty ways.`);
			ayy(`Ouch!`);
			txt(`Poor AI uneasy giggled and curled her toes when I tickled her exposed soles. It was such a joy to watch her helplessly squirm with no way to stop me. She could only beg me for mercy which she wisely did not, I was not in the mood to listen to her complaining and I would punish her viciously.`);
			txt(`I continued to touch her in a more sexual manner, caressing her inner thing from her knee to her slit, but not quite all the way. Her body reacted to my teasing and I could see how desperately eager she was. Finally, I allowed my fingers to brush even over her folds.`);
			ayy(`Ahhhhh!`);
			txt(`The AI moaned to let me know she enjoyed it. I carefully bowed down, to not let my slave realize what was coming, and blew my hot breath at her clit. She began moaning even harder, especially I leaned even closer and touched her with my tongue. I licked her for a while before halting and withdrawing, taking a moment just to watch her tensed horny body and listen to her quickened breathing.`);
			txt(`After the short break, I continued even more relentlessly, this time with a help of a small, pink, vibrating toy.`);
			ayy(`P... please, @mis, may I cum?`);
			txt(`Gasped delirious AI.`);
			
			link(`No! `, 108, ()=> counter.good = false);
			link(`Cum for me! `, 108, ()=> counter.good = true);
			break;
			
		
		
		case 108: 
			emo("bliss");
			remove("panties");
			log_sex("oral", "kat", "ayy");
			
			if(counter.good){
				mile.y_dominatrix = 1;
				kat(`Hmmmm...`);
				txt(`I took my time, watching her struggle with imminent orgasm.`);
				kat(`I allow it!`);
				txt(`Her body shivered, flooded by the bliss. It made me feel pretty proud, to know this was all result of my work.`);
				ayy(`T.. thank you, @mis!`);
				kat(`Nobody gives fuck about your thanks, you useless slut! There's a better way to express your gratitude!`);
				
			}else{
				mile.y_dominatrix = 2;
				kat(`Did I allow you to speak, you stupid, useless slut?`);
				txt(`I immediately stopped and roughly smacked her unprotected pussy. Her interruption made me pretty annoyed.`);
				ayy(`Ahhhh! I'm sorry, @mis!`);
				txt(`She loudly cried and quickly apologized but it was too late.`);
				kat(`Do you deserve to cum?`);
				ayy(`...no, @mis.`);
				kat(`Then shut up, you entitled bitch. I decide when - or if - you're going to cum!`);
				
			}
			
			txt(`${wears.bodysuit ? "I unzipped the catsuit" : "I pulled down my panties"}, climbed on the bed and surprised the cyberfloozie when I descended down, pressing my wet pussy against her face. But she reacted like a good little slave and instantly began licking. It was pretty great having such a cute obliging sextoy and I rubbed my pussy against her face until she gave me an amazing orgasm.`);
			kat(`Oh yeah! I'm cumming!`);
			ayy(`Mpmmpm!`);
			kat(`Fuck! I could get used to such first-class services!`);
			ayy(`...t...thank you, @mis!`);
			txt(`The AI gasped when I let her go.`);
			kat(`That was awesome... Oh, fuck!`);
			txt(`I cursed when I decided to release my sub from her restraints.`);
			ayy(`What is going on?`);
			txt(`Asked still blindfolded AI who could only hear me rummaging.`);
			kat(`Well, do you remember how you brought a pile of sextoys, including the handcuffs and how the handcuffs could be locked without keys?`);
			ayy(`Yeah?`);
			kat(`Among the things you brought, there seems to be no key.`);
			ayy(`WHAT!?! You didn't check?`);
			kat(`No. Why would you bring handcuffs that can't be unlocked?! Where do you think could be the keys?`);
			ayy(`I brought everything. I have no idea!`);
			kat(`WHAT!?!`);
			ayy(`Haha, you're forgetting we're in my virtual subreality!`);
			txt(`She giggled and dismissed the handcuffs. They faded away, she was free to remove her blindfold and wink at me.`);
			txt(`I felt slightly annoyed by her shenanigans. I realized I hated the idea she could have escaped at any time if she had wanted. I craved complete, total control. And I was a bit spooked I had such dark cravings.`);
						
			next(`Done. `, quickLoadOutfit);
			break
	}
}

















		
	









