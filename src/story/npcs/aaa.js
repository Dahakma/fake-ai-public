import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven, bar, kub} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";
import {spoiler, bills, log_sex} from "Virtual/text";
import {DISPLAY} from "Gui/gui";
import {rollUpSkirt, emo, wdesc, effigy, quickSaveOutfit, quickLoadOutfit, createWear, updateDraw, sluttiness, makeup, wearRandomClothes, remove, removeEverything, wear, create, wears, showLess, showAll} from "Avatar/index";
 import {placetime, dildo, buttplug, strapon, sex_toys} from "Loop/text_extra";
 import {payment} from "System/bank_account.js";
 
/////////////////////// WEEK 1 /////////////////////////////////////////////////////

//APOLOGY
export const a_apology = (index) => {
	switch(index){
		default:
		case 101: 
			set.irl();
			placetime("road home");
			txt(`On my way home I met @qaa - we were living in the same tower block. Without the support of his clique he seemed quite anxious when he approached me. `);
			//cligue? asap
			qaa(`Hi, @kat.`);
			kat(`Hello?`);
			txt(`I gave him a stern look and he seemed embarrassed. `);
			qaa(`I want to apologize. This maybe went a bit too far. `);
			kat(`Yeah, a bit too far? `);
			txt(`I snapped and he frowned: `);
			qaa(`I'm trying to say I'm sorry, right! The AI was just an experiment and a joke. I didn't intend to humiliate you and I'm sorry if you feel that way. And I couldn't stop them, they were so excited and they would do it with or without me. But don't worry! I'll make sure they won't force you to do anything you wouldn't like! And I mean, it isn't so bad, is it? You'll expand your horizons, <i>Neverion III</i> is really great! And the tasks will be fun! You'll try new things you wouldn't come up with yourself!  `);
			
			link(`Be understanding. ${spoiler("love")}`, 102);
			link(`Be angry. ${spoiler("hate")}`, 103);
			break;
			
		case 102:
			mile.a_hate = -1; //initial
			mile.a_burned = false;
			mile.a_apology = 1;
			mile.sub++;
			kat(`Thank you. I know I can trust you and that you would never let anything bad happen to me. And I'm really sorry about the way I treated you - I wish I knew about your feelings. `);
			qaa(`Thank you, @kat. I'm happy you understand me. `);
			txt(`He smiled when I lied to him. Like what the fuck, he blackmailed me and intended to continue to blackmail me but I should forgive him just because he suddenly had a bad conscience? `);
			kat(`I'm sorry. I guess I could be a bitch sometimes. `);
			qaa(`That's okay. `);
			kat(`But is this necessary? This whole stupid game? `);
			qaa(`Well, you agreed with this. And I won't be able to convince them to just let you off the hook. `);
			txt(`He shook his head, either this situation was making him unhappy too or he wanted me to believe he felt that way. `);
			qaa(`Anyway, I'll see you later! `); 
			//qaa(`Well, see you tomorrow! `); //And don't forget about your task! 
			next();
			break;
			
		case 103: 
			mile.a_hate = 1; //initial
			mile.a_burned = true;
			mile.a_apology = -1;
			mile.sub--;
			emo("angry");
			kat(`WHAT THE HELL?! First you stole my identity to create a twisted AI sex toy, then you used her to humiliate me in front of everbody, then you threatened and blackmailed me to be your slave and participate in this stupid pointless game but now is everything okay just because you feel bad about it? `);
			qaa(`But I...`);
			kat(`If you're really sorry, delete everything and end this. `);
			qaa(`But @qcc and @qbb...`);
			kat(`Don't make excuses! `);
			qaa(`You don't understand! `);
			kat(`I understand very well - you are a pathetic virgin loser who get involved in this crazy scheme rather than just to talk with me or ask me out! `);
			txt(`It seemed like I hit a sensitive spot. `);
			qaa(`Oh yeah? Fuck you! Like you would ever even consider going out with me! You completely ignore me until you need a help with something and then I'm good for you! `);
			kat(`That's not true! `);
			qaa(`And stop lying you had no idea about my crush! You intentionally lead me on just for your cruel amusement! `);
			kat(`That's not true either! And even if I ever might consider you, that ship now sailed! Now I know you're just a disgusting, deplorable jerk! `);
			qaa(`I don't care! You're just a whore who enjoys messing with people! But now I own you! You'll do what we'll tell you or we'll ruin your life! `);
			kat(`Fuck you! `);
			qaa(`Go to hell! `);
			next();
			break;
	}
};







//ADVICE - kiss
export const a_advice_1 = (index) => {
	switch(index){
		default:
		case 101: 
			set.irl();
			placetime("@qaa's place");
			mile.advice_1 = 1;
			mile.aaa++;
			txt(`I decided to visit @qaa personally, hoping for some extra leverage. `);
			qaa(`Hi, @kat, what are you doing here?`);
			kat(`I just came to say you <i>thank you</i> once again. I feel bad because I had no idea about your feelings. I really didn't want to lead you on. `);
			txt(`He was a bit embarrassed. `);
			qaa(`That's okay, don't worry about that. `);
			kat(`I also appreciate you are trying to restrain those other two. It is nice to have somebody who is on my side. `);
			qaa(`... You are welcome. `);
			kat(`...`);
			qaa(`...`);
			kat(`...`);
			qaa(`... So.. How's the Game? `);
			kat(`${mile.immersion>0 ? "Neverion III: The Rise of the Chaos Wizard</i>" : "The game"} is really fun! Except for som minor issues. `);
			
			if(mile.feyd_keryk_attempts <= 0){
				qaa(`You had troubles to find your contact? `);
				kat(`What? No, I found ${mile.feyd_keryk_name} on my first try. That was easy. `);
				qaa(`So what is the problem? `);
			}else if(mile.feyd_keryk_attempts > 5){
				qaa(`What issues? `);
				kat(`Like finding that idiot! How are you supposed to remember such a stupid name? What a pointless waste of time! `);
				qaa(`The devs thought it was stupid but somebody on the forum liked it so they decided to keep it in. Anything else? `);
			}else{
				qaa(`What issues? `);
			}
			kat(`It's very hard for a low-level character to get enough money and not get instanty killed.`);
			
			qaa(`It isn't so hard... but I shouldn't give you any advice, that wouldn't be fair. `);
			kat(`I know! But I'm getting really desperate! Only you can save me! `);
			txt(`I coyly smiled at him, playing the helpless damsel and giving him a chance to be the hero. `);
			qaa(`Well... okay`);
			kat(`Brilliant!`);
			qaa(`...But the days you could freely exploit me are gone! `);
			link(`What?`, 102);
			break; 
			
		case 102:
			emo("suspect");
			qaa(`You heard right. This is yet another case where you just expect somebody else will do your job for you. `);
			kat(`First, I did never <i>exploit</i> you. Second, if I'd knew you will bring this up during our every conversation, I'd never apologized. `);
			qaa(`It's not just me. You think you can always get away with anything just because of your beautiful face or your gorgeous hair or your pretty little fingers or your cute ears...  anyway, you get my point? `); 
			kat(`Yes. You think I'm pretty. `);
			qaa(`Yes... No! I should no longer allow to manipulate with me! `);
			txt(`I looked down, trying to sound dejectedly: `);
			kat(`Oh!.. But I guess you're right... you shouldn't let me get away with exploiting you! I was ungrateful and didn't deserve you!`);
			txt(`He nodded, smilling, happy I saw his point. `);
			kat(`But maybe one last little help? `);
			qaa(`Oh, @kat! `);
			txt(`He sighed and chuckled. `);
			if(mile.slut > 4){
				qaa(`Okay, I'll help you but you'll have to...`);
				kat(`...suck your d-..`);
				qaa(`...kiss me! What? What were you saying?`);
				kat(`Nothing! Kiss you? Fine!`);
			}else{
				qaa(`Okay, I'll help you but you'll have to kiss me!`);
				kat(`Fine!`);
			}
			txt(`Before he was able to react I leaned forward and gave him a quick passing kiss. `);
			qaa(`Oh!`);
			txt(`He was bewildered, his dreams came true and he was not able comprehend what to do. `);
			kat(`Okay? Are you with me? Was that enough? `);
			qaa(`Y.. ...No! You didn't even try. `);
			
			link(`Fine!`, 103, ()=> mile.a_advice_1 = 1);
			con_link(mile.sub < 3, `submissive`, `No. `, 103, ()=> mile.a_advice_1 = -1);
			break;
			
		case 103:
			emo("anoy");
			if(mile.a_advice_1 > 0){
				qaa(`Put more of your heart into it. `);
				txt(`I sighed. I went slower this time, gently held his head and again pressed my lips against his. I did not enjoy this and I could not put more heart into it, but I could try a bit more of my tongue. When I felt his hand on my butt I decided he had enough fun and pushed him away. `);
				kat(`Enough?`);
				qaa(`Y..Yes?`);
				txt(`@qaa stuttered, he was completely dazzled and stunned. `);
			}else{
				mile.sub--;
				mile.a_dom++;
				kat(`No. `);
				qaa(`No?`);
				kat(`You wanted a kiss. You got a kiss. For now that has to be enough! `);
				qaa(`For now? `);
				txt(`I intentionally let that open, to tease his imagination. `);
			}
			kat(`How to survive!?!`);
			qaa(`Survive? `);
			kat(`In the game! `);
			qaa(`Right!`);
			
			qaa(`Well, to be able to fight monsters, you need a better weapon. The knife is useless. `);
			kat(`I know, I'm not <i>that</i> stupid. But I don't have money and can't find any better weapon anywhere!`);
			qaa(`You can ask around for quests. Or... well... you can sell your clothes. `);
			kat(`What?!? I know you enjoy imagining me naked but this... `);
			qaa(`It isn't as stupid as it sounds. You'll be able to quickly earn the money and buy the clothes back! And the initial clothes don't have any armor level anyway. `);
			
			chapter(`Thank you, that's actually pretty smart!`, `advice_1`, 200, ()=> counter.feyd_nude = 1);
			chapter(`Thanks, I guess. `, `advice_1`, 200, ()=> counter.feyd_nude = 0);
			chapter(`That's the dumbest advice ever! `, `advice_1`, 200, ()=> counter.feyd_nude = -1);
		
			break;
	
	}
};










//WEEKEND 1


//FIRST DATE
export const a_first_date = (index) => {
	const danube = (()=>{
		switch(SETTING.local){
			case `slovak`: return `Danube`;
			case `czech`: return `Elbe`;
			case `english`: return `Don`;
			case `polish`: return `Oder`;
			case `german`: return `Danube`;
			default: return `Danube`;
		}
	})();
				
	//SETTING.local TODO
	switch(index){
		default:
		case 101: 
			set.irl(); //set.meta();
			placetime("@qaa's place");
			mile.aaa++;
			
			if(mile.advice1==1){
				kat(`Hello! `);
				qaa(`Hi, @katalt. Do you need another help with the game?`);
				kat(`Can't I just visit you without needing anything? `);
				qaa(`Well, I don't remember one single time when you visited me without needing anything. `);
				kat(`Don't blame me, you could always say no! `);
				qaa(`Yeah... but then you wouldn't even notice me? `);
				
				kat(`Nonsense. You should've told me how you feel! `);

			}else{
				qaa(`Hello!`);
				kat(`Hi.`);
				qaa(`What do you need?`);
				kat(`I just came to say <i>thank you</i> once again. I feel bad because I had no idea about your feelings. I really didn't want to lead you on. `);
				txt(`He was a bit embarrassed. `);
				qaa(`That's okay, don't worry about that. `);
				kat(`You should've told me how you feel. `);
			}
			
			qaa(`You'd only laugh! `);
			txt(`I hoped I would have declined him with grace but I was not sure. `);
			kat(`Why? You're a quite nice guy! `);
			qaa(`Is that the best compliment you have? `);
			kat(`Take it or leave it. `);
			qaa(`Is there even any chance you'd be ever interested in me? `);

			link(`Yeah, absolutely. ${spoiler("love")}`, 105);
			link(`Maybe. ${spoiler("love")}`, 104);
			link(`We could be friends! ${spoiler("hate")}`, 103);	
			link(`Absolutely none! ${spoiler("hate")}`, 102);	
			break;
			
		case 102:
			emo("angry");
			mile.a_first_date = -2;
			mile.a_hate = 1; //reversal
			mile.a_burned = true;
			kat(`Absolutely none `);
			txt(`I coldly ensured him. `);
			qaa(`But...`);
			kat(`It is me, not you. I just think it wouldn't work out. `);
			txt(`I add so it would not look like I was enjoying that. I almost felt bad for @qaa, he looked absolutely broken and desperate. Almost. `);
			qaa(`Not even....`);
			kat(`No!`);
			qaa(`...`);
			kat(`...`);
			qaa(`Thanks for being honest I guess. `);
			kat(`You are welcome! I'm glad we clarified that out, I just don't have to give you any false hopes! `);
			qaa(`You're such a cold, arrogant bitch!`);
			kat(`Excuse me?`);
			qaa(`You're shamelessly ${mile.slut > 3 ? "sleeping" : "flirting"} with anybody like a whore but I'm not good enough for you? `);
			kat(`You don't have to be rude just because I rejected you! But yes, if you want to hear the truth, yeah! I don't go out with lame nerdy losers! `);
			qaa(`Fuck you!`);
			next(`I don't have to listen to your pathetic whining! `);
			break;
			
		case 103:
			emo("angry");
			mile.a_first_date = -1;
			mile.a_hate = 1; //reversal
			mile.a_burned = true;
			kat(`I'm sorry! There's nothing wrong with you but I just don't feel it! But we still could be friends! And there are plenty of other girls! Maybe you should try to lower your expectation a little and ask out somebody who isn't out of your league. Like @sas... that might be still too high, maybe more like @ven... or maybe even a bit lower...`);
			qaa(`I'm not interested in stupid @ven! You just want to be friends?!? Seriously?! After everything I did for you?!?`);
			kat(`Yeah! Don't get so defensive! I don't owe you anything!`);
			qaa(`You're shamelessly ${mile.slut > 3 ? "sleeping" : "flirting"} with everybody like a whore but I'm not good enough for you? `);
			kat(`You don't have to be rude just because I rejected you! But yes, if you want to hear the truth, yeah! I don't go out with lame nerdy losers! `);
			qaa(`Fuck you!`);
			next(`I don't have to listen to your pathetic whining! `);
			break;
			
		case 104:
			mile.a_first_date = 1;
			mile.a_hate--; //meaningless
			mile.a_burned = false;
			kat(`Maybe? `);
			txt(`I shrugged. `);
			kat(`Some girls find being blackmailed extremely romantic but I'm not really sure I'm into it. `);
			txt(`At least he looked pretty embarrassed when I said that. `);
			qaa(`As soon as you finish the game you'll be free and until then I'll make sure they won't give you any dumb bad tasks. `);
			kat(`That would be really great. `);
			qaa(`So... would you go out with me? `);
			kat(`Are you asking me for a date? `);
			qaa(`Yes. NO! Yes? ...maybe?`);
			txt(`I giggled his confusion and then shrugged again: `);
			kat(`Okay! `);
			link(`We can go right now.`, 110);
			break;
			
		case 105:
			mile.a_first_date = 2;
			mile.a_hate--; //meaningless
			mile.a_burned = false;
			txt(`I shrugged. `);
			kat(`Yeah. Why not? I know you for a long time and I kinda like you. `);
			qaa(`REALLY?!?`);
			kat(`Yeah, like a friend or a brother. `);
			qaa(`OH!`);
			kat(`Like a handsome step-brother. `);
			qaa(`Oh?`);
			kat(`Come on, I'm just joking! We can try it, you can ask me out. `);
			txt(`Watching the emotional roller coaster on his face was very entertaining. But when I agreed I might consider going out with him, his expression was dumb and happy.  `);
			qaa(`So... would you go out with me? `);
			kat(`Are you asking me for a date? `);
			qaa(`Yes. NO! Yes? ...maybe?`);
			txt(`I giggled his confusion and then shrugged again: `);
			kat(`Okay! `);
			link(`We can go right now.`, 110);
			break;	
			
			
		case 110:
			mile.qaa++;
			mile.a_love++;
			mile.a_dating = true;
			
			txt(`After playing their stupid game for the whole weekend I missed the sun and fresh air. `);
			txt(`We went on a walk along the ${danube} and chatted about various things. It was not so bad as I expected. After @qaa got through his initial discomfiture, he was a quite fun companion. `);
			kat(`Well, I had far worse dates. `);
			txt(`I was forced to admit. `);
			qaa(`Really? `);
			txt(`I just wanted to say some of my previous dates were really horrible but he interpreted it as a praise of this one. And why not? `);
			kat(`Yeah, why don't you date other girls? `);
			qaa(`I just... `);
			txt(`He looked deep into my eyes. I was afraid he will say something stupid so I continued: `);
			kat(`Why don't you hang with us more? I'll tell @sas to invite you to her birthday party. You should live more and spend less time with videogames and those two weirdos.  `);
			qaa(`They are not weirdos. `);
			kat(`Come on. Unlike you, they are lost causes and they only drag you down to their miserable level.  `);
			qaa(`That is not true... `);
			txt(`We did not talk about this anymore but I could see I made him think. `);
			next(`Excellent.`);
			break;
			
	}
};


//ANOTHER APOLOGY - first date 
export const a_another_apology = (index) => {
	const danube = (()=>{
		switch(SETTING.local){
			case `slovak`: return `Danube`;
			case `czech`: return `Elbe`;
			case `english`: return `Don`;
			case `polish`: return `Oder`;
			case `german`: return `Danube`;
			default: return `Danube`;
		}
	})();
	
	switch(index){
		default:
		case 101: 
			set.irl(); //set.meta();
			placetime("@qaa's place");
			mile.aaa++;
			emo("focus");
			kat(`Hello, @qaa!`);
			qaa(`Hey, @kat!`);
			txt(`He obviously did not expect me. Our last private conversation went horribly when he refused to admit he was a pathetic loser, offended me and even threatened me! He had used to like me but now he hated me, he was cold and compassionless. All my hopes I might be able to sway him and convince him to abandon their stupid ploy were gone. `);
			txt(`But maybe there was still a chance. He was pretty smart, maybe he realized how awful his behavior was and was ready to beg me for forgiveness. `);
			qaa(`...`);
			kat(`...`);
			txt(`He was not able to withstand my stare and hesitantly began speaking. `);
			qaa(`Well, our last conversation didn't go well. `);
			kat(`Yeah!`);
			qaa(`And we both said things we regret. `);
			txt(`I nodded. I did not regret the content, I was right after all, but maybe the form I choose might not be very tactful. I was so angry!`);
			qaa(`I'm sorry for calling you <i>whore</i>. Maybe I went too far. I guess I'm just a little bit jelaous when you ${mile.slut > 3 ?  "sleep" : "date"} with other guys.`);
			kat(`A little bit? `);
			txt(`I smirked and he chuckled, releasing the tension. `);
			qaa(`Yeah, maybe more than a little bit. Watching you with all those cool guys! Knowing I have no chance! You were right, I really just a pathetic virgin loser. `);
			kat(`Come on! You're not a lost case! `);
			txt(`I reassured him and he smiled. `);
			qaa(`What do you think? Is... is there any chance... you and me? `);
			
			link(`Sure! ${spoiler("love")}`, 104);
			link(`Maybe, we'll see! ${spoiler("hate")}`, 102, ()=> mile.a_another_apology = -1);
			link(`I'm not interested in you but other girls might be! ${spoiler("hate")}`, 102, ()=> mile.a_another_apology = -2);
			link(`Absolutely no! ${spoiler("hate")}`, 102, ()=> mile.a_another_apology = -3);
			break;
			
		case 102:
			mile.a_burned = true;
			mile.a_hate++;
			emo("angry");
			if(mile.a_another_apology === -1){
				kat(`Maybe, we'll see!`);
				qaa(`And now you're doing it again!`);
				kat(`What?!`);
				qaa(`Messing with my feelings and leading me on! You're just endlessly teasing me, never committing, unwilling to say either yes or no!`);
				kat(`Would you prefer if I just said <i>no</i>?`);
				qaa(`No! But I least I could stop wasting my time! But I guess you're unable to be honest!`);
				kat(`Then <i>no</i>! There's no chance we could be together! Are you happy?!`);

			}else if(mile.a_another_apology === -2){
				kat(`It's understandable you have a crush on me, I'm aware of how dangerously attractive I am.  But maybe you should try to lower your expectations and ask out somebody who isn't out of your league. Like @sas... that might be still too high, maybe more like @ven... or maybe even a bit lower...`);
				qaa(`I'm not interested in stupid @ven! You're such an arrogant bitch!`);
				kat(`Excuse me? `);
				qaa(`You're a smug and arrogant bitch who thinks she's above everybody else! `);
				kat(`I don't believe I'm above everybody else! I just prefer not to date pathetic nerdy losers!`);
				
			}else if(mile.a_another_apology === -3){
				kat(`Absolutely no!`);
				txt(`He gasped in shock.`);
				kat(`When I told you the last time I'll never go out with you I meant it! Stealing my identity, blackmailing me, forcing me to do stupid perverted tasks, that's a total dealbreaker for me! `);
				qaa(`You're such a bitch! I'm trying to make amends, forgiving you everything mean ever you did to me, but you're not willing to reciprocate in any way! `);
				kat(`I didn't do anything wrong! I don't need your forgiveness!`);
			}
					
			qaa(`Fuck you! You know what? I don't regret calling you <i>whore</i>! Because you're one!`);
			kat(`Sure, call me <i>whore</i>! I don't care! Because I know we'll never ever have sex! Even if you were the last man on the Earth!`);
			qaa(`Now be serious! You would really rather doom mankind rather than swallowing your pride and trying to help me to repopulate the Earth?!?`);
			kat(`Yeah! Fuck mankind! Humanity going extinct is preferred to any repopulation with you. `);
			qaa(`You selfish bitch!`);
			kat(`You entitled prick!`);
			
			txt(`I could not bear staying in his presence anymore and left. *Whore*!?! I will make him pay! `);
			next();
			break;
			
		case 104:
			mile.a_another_apology = 1;
			mile.a_burned = false;
			mile.a_hate = -1; //reversal
			emo("relax");
			kat(`Maybe? `);
			txt(`I shrugged. `);
			kat(`Some girls find being blackmailed extremely romantic but I'm not really sure I'm into that. `);
			txt(`At least he looked pretty embarrassed when I said that. `);
			qaa(`As soon as you finish the game you'll be free and until then I'll make sure they won't give you any dumb bad tasks. `);
			kat(`That would be really great. `);
			qaa(`So... would you go out with me? `);
			kat(`Are you asking me for a date? `);
			qaa(`Yes. NO! Yes? ...maybe?`);
			txt(`I giggled his confusion and then shrugged again: `);
			kat(`Okay! `);
			link(`We can go right now.`, 110);
			break;
			
		case 110:
			mile.qaa++;
			mile.a_love++;
			mile.a_dating = true;
			
			txt(`After playing their stupid game for the whole weekend I missed the sun and fresh air. `);
			txt(`We went on a walk along the ${danube} and chatted about various things. It was not so bad as I expected. After @qaa got through his initial discomfiture, he was a quite fun companion. `);
			kat(`Well, I had far worse dates. `);
			txt(`I was forced to admit. `);
			qaa(`Really? `);
			txt(`I just wanted to say some of my previous dates were really horrible but he interpreted it as a praise of this one. And why not? `);
			kat(`Yeah, why don't you date girls? `);
			qaa(`I just... `);
			txt(`He looked deep into my eyes. I was afraid he will say something stupid so I continued: `);
			kat(`Why don't you hang with us more? I'll tell @sas to invite you to her birthday party. You should live more and spend less time with videogames and those two weirdos.  `);
			qaa(`They are not weirdos. `);
			kat(`Come on. Unlike you, they are lost causes and they only drag you down to their miserable level.  `);
			qaa(`That is not true... `);
			txt(`We did not talk about this anymore but I could see I made him think. `);
			next(`Excellent.`);
			break;
	}
};









/////////////////////// WEEK 2 /////////////////////////////////////////////////////




//ADVICE 2 - GLASSES
export const a_advice_2 = (index) => {
	switch(index){
		default:
		case 101:
			set.meta();
			placetime("@qaa's subreality");
			mile.aaa++;
			mile.advice_2 = 1;
			mile.glasses = true;
			
			if(mile.a_love > 0){
				txt(`I deciced to ask @qaa. We were closer than ever before and I was sure he will gladly help me. `);
			}else if(mile.advice_1 === 3){
				txt(`I decided to ask @qaa. He helped me once, he'll surelly help me the second time. `);
			}else{
				txt(`This thime I decided to ask for his help  @qaa. It seemed I might be able to convince him. `);
			}
			
				
			kat(`Hello!`);
			qaa(`Hi!`);
			kat(`I need you help!`);
			qaa(`Of course you do!`);
			kat(`Come on!`);
			qaa(`I won't help you to beat the game! That would be against the rules! `);
			kat(`I don't need any help! Just a little nudge in the right direction! `);
			txt(`He hesitated but I knew he will not be able to resist for long.`);
			kat(`And I will owe you one favor! `);
			txt(`I pushed him when he was hesitating for too long. `);
			qaa(`Fine! One small nudge! What is the problem? `);
			kat(`I'm a bit overwhelmed by the Arazors storyline and I need to explain where is the treasure and what should I do to find it. `);
			qaa(`That's isn't just a small nudge!`);
			kat(`Okay! At least tell me where to start!`);
			//qaa(`First you have to hire your first mate. It could be either Meril, Stede or Galawar. Your first mate will arrange the rest of the preparation. Then you have to sail to the Island of Turtles. `);
			qaa(`First you have to hire your first mate. It could be either @lys, @ste or @gal. Your first mate will arrange the rest of the preparation. Then you have to sail to Karged, the map is randomised but to reach Karged you always just have to sail west. And don't trust anybody. `);
			link(`Thanks! That's what I needed! `, 102);
			break;
			
		case 102:
			emo("focus");//emo??
			qaa(`And the favor? `);
			if(mile.slut > 4){
				kat(`Fine! But I won't have sex with you for so little!`);
				qaa(`I didn't want to ask you to have sex with me... wait, would you if I told you more? `);
				kat(`Maybe. Who knows.`);
				txt(`I hoped he will think about that the next time I will ask him for help.`);
				kat(`What do you want?`);
			}else{
				kat(`Fine! But nothing sexual or weird!`);
				qaa(`Of course not!`);
				kat(`What do you want? `);
			}
			qaa(`Well, it's pretty stupid. `);
			kat(`I do not doubt that!`);
			qaa(`Why have you even stopped wearing glasses? `);
			kat(`Because I started wearing contact lenses? `);
			qaa(`Maybe you could try to wear them again? At least for once, to see how would you look in them? `);
			kat(`I would look like a dork! `);
			qaa(`I think you would look cute! `);
			kat(`He waved and ${choice("glasses")} materialized on my nose. `);
			qaa(`Something like this? `);
			kat(`Fine! `);
			txt(`I shrugged. `);
			
			effects([{
					id: "glasses",
					fce(){
						createWear("glasses");
						updateDraw(PC); //redundant?
					},
			}]);
			
			
			next(`Continue. `, ()=>{
				set.irl();
				createWear("glasses");
				updateDraw(PC); //redundant?
			});
		break;
	}			
};


/*
	
	TODO - HANGING TOGETHER OUT SCENE

*/


//DATE - love?;  meet girls
export const a_date = (index) => {
	switch(index){
		default:
		case 101:
			set.irl();
			placetime("at home");
			txt(`I was contemplating what to do next when the door ring started buzzing. It was @qaa. `);
			qaa(`Hello, @katalt! I wondered, would you like to go out with me? `);
			kat(`On a date? `);
			txt(`He hesitated. `);
			qaa(`Yeah, on a date. `);
			
			link(`Okay. `, 103);
			link(`I'd love to! `, 103);
			link(`No! `,102);
			link(`Sorry, I don't have time at the moment. `,102);
			break;
			
		case 102:	
			qaa(`Oh! ..well, sorry for bothering you. Maybe the next time? `);
			next(`Sure. `);
			break;
			
		case 103: 
			mile.qaa++;
			mile.a_love++;
			placetime("park");
			
			txt(`I put on shoes, grabbed my jacket and we went outside. It was after rain, the air was fresh, permeated with an earthy smell (@qaa later explained to me it was petrichor) and a bit chill. Countless potholes in the road were brimmed with water. `);
			qaa(`It's quite nice...	 `);
			kat(`Yeah, the weather is fine.. `);
			qaa(`...`);
			kat(`...`);

			if(mile.immersion>1){
				txt(`We eventually ended in a cake shop. The <i>Neverion</i> was a natural topic of the conversation. He asked about my progress and I tried to extract some tips from him. But then we talked more and I was just interested how he solved different quests, whether he gave Graham to the justice to be hanged, sided with Gothredian traders or the villagers or how he stopped the wedding. A bit blushing, he admitted he actually seduced the Countess of Kallamir and I was shocked this was even possible. I would not think about that. `);
				txt(`I approved most of his actions and was pleasantly surprised by how our choices match. I would rather be without the sword of levitation than ever side with those abusive jerks and Gelfs were scum who deserved nothing better. `);
			}else{
				txt(`We eventually ended in a cake shop. We mostly talked about funny stories from elementary school. We used to be closer as kids before I became cool and shared many experiences. `);
				txt(`Then we chatted about various other things. I knew @qaa as an introvert guy who does not talk very much but when we touched some subject he deeply cared about he was unstoppable and I had to moderate his ranting. I learned quite a few new things. And @qaa was a patient listener when I talked about thigs which troubled me and was never condescending. `);	
			}
			
			
		/*
			kat(`I was reaching for my phone but he stopped me: `);
			qaa(`I invited you, I'll pay! `);
			ka("You don't have to! `);
			qaa(`That's okay! I checked your account statements . `);

			kat(`What the hell! How can you be so nice in one moment and `);
		*/

			link(`Continue. `, 104);
			break;
			
		case 104:
			emo("shock");
			txt(`After that, we were just aimlessly strolling through the park. We were walking close to each other. Our knuckles lightly touched and then again. I had enough of this teasing, I grabbed his hand. It was warm and sweating. `);
			qaa(`@kat? `);
			kat(`Yeah? `);
			qaa(`I love you. `);
			txt(`Fuck!`);
			
			link(`That's nice. `, 112);
			link(`I love you too! `, 113);
			link(`I know. `, 111);
			break;
		
		case 111: 
			mile.a_date = 1;
			emo("shy");
			txt(`He bitterly chuckled: `);
			qaa(`Is that a reference? `);
			kat(`To what? No, I mean it was quite obvious. `);
			qaa(`Yeah, I guess... Do you mind? `);
			kat(`... no! Of course I don't! `);
			qaa(`He at least did not ask whether I loved him too! `);
			kat(`I'm kinda enjoying this date.`);
			qaa(`Really? That is great! I love spending time with you too. `);
			link(`Continue walking. `, 120);
			break;
		
		case 112: 
			mile.a_date = 1;
			emo("shy");
			qaa(`Oh!`);
			txt(`His disappointment was palpable. He expected something more. Too bad. I at least tried to soften the blow: `);
			/*
			if(mile.a_sub < 0){
				qaa(`Oh!`);
				txt(`His disappointment was palpable. He expected something more. Too bad. I at least tried to soften the blow: `);
			}else{
				qaa(`Oh!`);
				txt(`His disappointment was palpable. He expected something more. Too bad. I at least tried to soften the blow: `);
			}
			*/
			kat(`I'm enjoying this date with you very much. Do you? `);
			qaa(`Yeah, I love spending time with you. `);
			txt(`We made few more steps. `);
			qaa(`I'm so dumb! I'm sorry @kat, I just... being here with you... `);
			kat(`That's okay, don't worry about that! `);
			link(`Continue walking. `, 120);
			break;	
			
		case 113: 
			mile.a_love++;
			mile.a_date = 2;
			//mile.sub++; //TODO
			emo("relax");//emo?? fake smile?
			kat(`I love you too! `);
			txt(`What else was I the hell supposed to say? `);
			qaa(`Really?!?!?!`);
			kat(`Mhmmpmm. `);
			txt(`I mumbled. `);
			qaa(`So we can be a boyfriend and girlfriend? `);
			kat(`I guess... `);
			txt(`I meant... it would be technically possible but...`);
			qaa(`Awesome! `);
			txt(`I enjoyed the time we spent together but I was not sure if shared his excitement. `);
			link(`Awesome! `, 120);
			link(`Mhhhhh...`, 120);
			break;
			
			
		case 120:
			mile.a_public = 1;
			emo("shock");
			txt(`After a while, we sit down to rest on a park bench. It was a calm and pleasant moment, or awkward conversation was already almost forgotten, and then a terrible disaster stroke. `);
			txt(`@eva and @sas were taking a shortcut through the park. Fuck! I would never think this could happen! And it was too late do dive into bushes! They noticed us, waved at me and joined us. `);
			eva(`Hello @kat! Hello @qaa! How are you? `);
			sas(`Hello! `);
			kat(`Eh...hi! `);
			qaa(`Hello! `)
			txt(`That nosy bitch smiled and then asked the question I feared: `);
			eva(`What are you doing here? `);
			txt(`They all three were staring at me. `);
			
			link(`We are here on a date. `, 140);
			link(`@qaa is helping me with a school assignment. `, 130);
			link(`I forget... my cat... on the stove... please, excuse me, I have to run! `, 121);
			break;
			
		case 121:
			mile.a_date_girls = 1;
			txt(`My brain froze. I mumbled something incoherent and then ran away. `);
			txt(`At that point it seemed like a smart idea. `);
			next();
			break;
			
		case 130:
			emo("anoy");
			txt(`It was a plausible explanation - why else I would spend time with him - and it seemed the girls bough it. We exchanged few more small-talk phrased and they left. `);
			txt(`I was happy how I managed to defuse the problematic situation and then I noticed how annoyed @qaa was: `);
			
			qaa(`Helping you with a project?!? `);
			kat(`What the hell should I told them? `);
			qaa(`Maybe the truth? `);
			kat(`Are you fucking crazy?!? `);
			qaa(`Are you ashamed for me? `);
			
			link(`Of course not! `, 133);
			link(`This is going too fast! `, 132);
			con_link(mile.sub < 5, `submissive`, `Absolutely! `, 131);
			break;	
		
		case 131:
			mile.sub--;
			mile.a_dom++;
			mile.a_date_girls = 2;
			emo("imp");
			kat(`Absolutely! Just at look at yourself! You had to be blackmailing me with all your friends before you get the courage to ask me out! `);
			qaa(`What?!?! But...`); 
			kat(`Listen, we can going out and date but nobody can know! `);
			qaa(`But I want people to know! `);
			kat(`Tough luck! `);
			qaa(`You could be such a bitch! `);
			kat(`What are you going to do? Cry about it? `);
			qaa(`No! `);
			kat(`Should I go home? `);
			qaa(`...no. Please, stay with me. `);
			link(`As I thought. `, 150);
			break;
				
		case 132:
			mile.a_date_girls = 4;
			emo("focus");
			kat(`I'm sorry but this is going too fast! I freaked out! I'm not yet ready to tell anybody about us! `)
			if(mile.a_date === 2){
				qaa(`But you told me you love me! `)
				kat(`And I meant it! Trust me, I would never say such thing lightly or without thinking about it. `);
			}
			qaa(`But I want to date you publicly! `);
			kat(`Of course! But not yet! I can't just start dating you out of nowhere! Let people get used to that I'm hanging out with you nerds first! `);
			qaa(`... okay, if you think it'll be best... for now. `);
			link(`Great. `, 150);	
			break;
		
		case 133:
			mile.a_date_girls = 3;
			emo("focus");
			kat(`I'm not! You know them! They're gossiping bitches! `); 
			qaa(`And? You mind they'll gossip about us? `); 
			kat(`I mean yes but... `);
			qaa(`You're constantly just telling me only what I want to hear! `);
			kat(`I'm not! `);
			if(mile.a_date === 2){
				qaa(`You love me but don't want anybody to know? Did you even mean it? `)
				kat(`Of course! And trust me, I would never say such thing lightly or without thinking about it. `);
			}
			qaa(`Thank you for the date, @katalt, it was very nice! But I think I have to go! `);
			kat(`You're seriously overreacting! `);
			//(`Date is over. ",101);	
			next();
			break;
			
		case 140:
			mile.a_love++;
			mile.sub--;
			mile.a_date_girls = 5;
			mile.a_public = 2; //to 3 by separate random event
			mile.stat--;
			emo("focus");//emo?? fine
			eva(`Seriously?!?!`)
			txt(`She could not believe it. This was a bomb! But I didn't care about her opinions! I could date whoever I wanted! `);
			txt(`@sas's jaw dropped down and she was not sure what to say: `)
			sas(`You're not joking?... wow that is.... well... good for you two, I guess. `);
			kat(`Why are so you surprised? `);
			eva(`Well, he doesn't seem like your usual type... `);
			txt(`@eva smired at @qaa. `);
			eva(`...no offense, of course! `);
			txt(`@qaa did not even realize she was subtly mocking him. `);
			eva(`Well, we won't bother you for long, I could see you'd prefer to be alone. `);
			sas(`Bye! Have fun! `);
			kat(`Bye!`);
			txt(`They left us and we could see them from behind chatting and wildly gesticulating. They certainly did not see this coming. `);
			txt(`@qaa was also pretty surprised. However, unlike them, he was filled with pride and joy. `);
			qaa(`You told them we are on a date! `);
			kat(`Yeah, because we are? `);
			qaa(`Oh my! Is this real? This is real! I'm dating @katalt @katsur! `);
			kat(`Yeah? Stop grinning so much! `);
			link(`Continue. `, 150);
			//link(`Finish date. `,101);	
			break;
			
		case 150: 
			emo("relax");
			if(mile.a_date_girls === 5){
				txt(`@qaa was absolutely elated while I tried to not think about the consequences. @eva and @sas were gossiping bitches!`);
			}else{
				txt(`We continued with the date but @eva and @sas completely ruined the mood. `);
			}
			txt(`@qaa accompanied me home and before the door of our aparment, he hesitated. `);
			qaa(`Can I kiss you? `);
			kat(`I allow it.`);
			txt(`And then he kissed me. `);
			next();
			break;
	}
};	
	


//MOVIE - bj or masturbation
export const a_movie = (index) => {
	switch(index){
		default:
		case 101:
			set.irl();
			placetime("@qaa's room");
			mile.aaa++;
			if(mile.slut < 4){
				txt(`@qaa texted me. He bashfully suggested we could do <i>something together, like watching a movie, dunno</i>. It did not sound so bad, watching a movie might be fun. And my new policy was to be friendly and subtly convince him to stop with the blackmail. So I replied 👍. `);
				txt(`I made a huge mistake.`);
			}else{
				txt(`@qaa texted me. He bashfully suggested we could do <i>something together, like watching a movie, dunno</i>. I knew well what <i>watching a movie</i> meant! However, my new policy was to be friendly and subtly convince him to stop with the blackmail. So I replied <i>ok</i> and hoped he will not be too sleazy. `);
				txt(`I made a huge mistake. He seemed to be more interested in the movie than my body!`);
			}
			txt(`@qaa opened the door - visibly pretty excited about spending time with me - and explained he prepared something special:`);
			qaa(`This is my favorite movie... well, one of the top five. You are gonna love it! It's crime against humanity it didn't win Oscar and they never made a sequel! `);
			txt(`I hated it. It was one of the most boring movies I have ever seen and I do not understand how could anybody enjoy it. The whole story took place on a ship sailing to somewhere on the other end of the world. There was some fighting at the beginning but then literally nothing else happened. `);
			
			link(`Doze off. `, 105);
			con_link(mile.slut > 3, `low slut`, `Just suck his dick and go home. `, 103);
			//if(mile.sub < 1) link(`Complain. `, 102);
			break;
			
		/*
		//TODO - this choice would make most sense for the charecter but would be boring and pointless
		case 102:
			mile.sub--;
			OMG. This movie is the worst! 
			What?! 
			@qaa looked at me like he 
		*/	
			
		case 103:
			mile.a_movie = 1;
			effigy.qaa.showLess(-1,0,1);
			mile.slut++;
			log_sex("bj", "qaa");
			
			txt(`I could not survive one more minute of that horrid movie. Extreme measures had to be taken. I bend down to his lap. `);
			qaa(`Wha...What are you doing, @kat?`);
			kat(`I'm going to suck your dick!`)
			txt(`I ensured him and unzipped his pants. `);
			qaa(`But... there'll be a brilliant scene soon. Could you wait a moment...  `);
			kat(`NO!`);
			txt(`I just said while grabbing his balls. `);
			qaa(`Oh... okay...`);
			txt(`@qaa sadly sighed. I licked my lips to build up some moisture and then took his shaft in my mouth. I started sucking, moving back and forth, caressing him with my tongue. `);
			qaa(`Oh fuck! That feels great! `);
			txt(`Duh! I finally made him to forget about the stupid movie. I bet he dreamed about this moment for so long! When I felt his dick twitching, I pause for a brief moment and then I continued again, taking him deeper to my throat.  `);
			qaa(`I'm going to cum!`);
			txt(`He warned me. Despite that I was still a little surprised when his first shot hit the back of my throat and I nearly started coughing. `);
			qaa(`That was... `);
			txt(`He had trouble to express what he felt. I smiled, now he surely will not be bothered when I excuse myself! `);
			
			next(`You're welcome! `);
			next(`Awesome, wasn't it? `);
			next(`Whatever, can I go home now? `);
			break;
	
		
		case 105:
			emo("sleep");
			txt(`My eyes were getting heavier and heavier and I eventually decided to give up and just fell asleep. `);
			txt(`I was awakened by something touching me. For a moment I was confused where am I but then I remembered. `);
			txt(`I felt @qaa's hand softly brushing against my boob, shamelessly taking advantage of my defenceless condition. He did not realize I am conscious and started to fondle me even more daringly. `);
			txt(`Then he stopped. I thought he might get too scared or suspect I am awake but the opposite was true. Soon I felt his hand on my thigh, running higher${wears.skirt ? " under my skirt" : ""}. `);
			
			link(`Confront him and leave. `, 106);
			con_link(mile.sub < 5, `submissive`, `Confront him and make him continue. `,107); //TODO
			link(`Pretend to be asleep. `, 108);
			break;
			
		case 106:
			mile.a_movie = 2;
			mile.a_dom++;
			emo("angry");
			txt(`I opened eyes and pretended I'm surprised. `);
			kat(`Wha.. WHAT! What are you doing you pervert!`);
			qaa(`I.. I...`);
			txt(`He stuttered and quickly withdrew his hand from under my skirt. `)
			kat(`How dare you! The only thing you're thinking about is hot to sexually exploit me! I can't believe I trusted you!`);
			qaa(`But.. but it isn't like...`);
			kat(`Okay. You're free to explain!`);
			qaa(`Well, there was a spider and I...`);
			kat(`Do you think I'm stupid?!`);
			qaa(`Of course not!`);
			next(`Goodbye! `);
			break;
		
		case 107:
			mile.sub--;
			mile.a_dom++;
			mile.a_movie = 3;
			log_sex("hand", "kat", "qaa");
			
			emo("smug");
			txt(`I opened my eyes. It took @qaa a while before he glanced at me and with horror realized I am awake. `);
			kat(`What are you doing? `);
			txt(`I asked him matter-of-factly. `);
			qaa(`I... I... `);
			kat(`Do you think that groping a girl without her consent is fine?!`);
			qaa(`Sorry!`);
			txt(`He stuttered, extremely embarrassed, and tried to withdrew his hand${wears.skirt ? ` from under my ${wdesc.lower}` : ""}. But I grabbed his arm. `);
			kat(`I did not ${mile.sub<1 ? "allow" : "tell"} you to stop! `);
			qaa(`W... what?!`);
			kat(`Go on!`);
			txt(`I commanded him, not breaking eye contact with him. The mix of shyness and desire in his face was irresistible. ${wears.skirt ? "I led his hand higher" : `I pushed his hand in my ${wdesc.lower}`}. `);
			if(!PC.panties){
				qaa(`You... you are not wearing any panties! `);
				txt(`He was amazed and confused. `)
				if(mile.slut > 5){
					kat(`Yeah. I'm a shameless, little slut. `);
					txt(`I smiled. `);
				}
				txt(`He reached the pubic mound and started slowly rubbing my bare pussy. `);
			}else{
				txt(`He reached the pubic mound and started slowly rubbing my pussy through the very thin fabric of my ${wdesc.panties}.`);
			}
			qaa(`Am I doing it right? `);
			kat(`Oh yeah! Shut up and continue! `);
			txt(`I moaned. He followed my order and continued, now with more confidence. It was kinda cute and hot. `);
			
			if(PC.panties) txt(`@qaa moved his finger under my panties and started caressing me directly, slowly building up my arousal. `);
			qaa(`You are so wet! `);
			kat(`Oh yeah!`);
			txt(`I gasped. `);
			txt(`And then I came with his index finger teasing my little button. `);
			kat(`OH YEAH!`);
			qaa(`Are you fine?! `);
			kat(`More than fine! `);
			qaa(`Oh!... Did you just...  `);
			kat(`Oh yeah, you made me cum! `);
			qaa(`OH!`);
			next(`Not bad!`);
			break;

		case 108:
			mile.a_movie = 4;
			mile.sub++;
			log_sex("hand", "kat", "qaa");
			
			txt(`I decided to keep pretending I am asleep, wondering how far he will venture. `);
			if(!PC.panties){
			txt(`His hand slowly advanced further ${wears.skirt ? "under my" : "under my"} ${wdesc.lower}. He hesitated for a while when he found out ${mile.slut > 5 ? "I'm a shameless little slut who is not wearing any panties. ": "that I'm not wearing any panties. "} `);
				txt(`He touched my bare skin, nervously examining with his finger my pubic mound and lightly sliding over my lips. His tender caressing felt so electrifying, I did not realise how horny I was.`);
			}else{
				txt(`His hand slowly advanced further ${wears.skirt ? "under my" : "under my"} ${wdesc.lower} until he reached my panties. `);
				txt(`He touched me through my ${wdesc.panties}, nervously examining with his finger my pubic mound and lightly sliding over my lips. His tender caressing felt so electrifying, I did not realise how horny I was.  `);
			}
			txt(`I inadvertently softly moaned and he instantly froze. For a minute or two he just stood still, watching me and listening to my breath.  `);
			txt(`He was scared that I am waking up and I was scared he finds out that I am just faking it. But when I did not react for a while he dared to continue. `);
			if(PC.panties) txt(`His fingers even slid under my panties so he could caress me directly. `);
			qaa(`Your pussy is so soft and perfect. `);
			txt(`@qaa whispered in deep admiration. The tips of his fingers were moving deliberately, he was still terrified any more enegic move will wake me up and this kind of stimulation was exactly what I needed. My arousal was building up and it was getting harder and harder to stay still. `);
			txt(`He held me on the edge for a delightfully long time before he tipped me over and I came with his index finger teasing my little button. `);
			txt(`@qaa saw my body shiver and immediately withdrew his hand, sadly maybe a little bit too soon. I slowly started moving and then opened eyes. `);
			kat(`Did... did I fell asleep? `);
			txt(`He was staring at me, desperately trying to guess whether I suspected anything${PC.panties ? " or noticed how wet my panties were" : ""}. `);		
			qaa(`Y... yeah. Did you sleep well? `);
			kat(`Yeah. Although I had the weirdest dream... `);
			qaa(`What dream? `);
			kat(`Sorry! That's private. And a bit embarrassing. `);
			qaa(`Oh! `);
			kat(`Sorry, but I have to go now. I have to do something important I forget about. `);
			qaa(`Understandable. `);
			
			next();
			break;	
	}
}	



export const a_needle = (index) => { //TODO IMPORTANT
	switch(index){
		default:
		case 101:
			set.irl();
			placetime("at home");
			mile.a_hate++;
			emo("anoy");
			txt(`When I was returning back home, I nearly bumped into @qaa. He did not even acknowledge my existence! He was still sulking because I told him I am not interested in him. Why the hell would I?! He was an entitled jerk, he called me a whore just because I had the audacity to ${mile.slut > 7 ? "sleep with" : "date"} other guys than him! `);
			if(mile.blackmail){
				txt(`They promised they will not touch my Cloud as long as I will not break our deal. But I did not trust @qaa at all! That pathetic twerp was probably going through my private files right now just to find a nude pic I sent one of my previous boyfriends so he could jerk off. `);
				txt(`Maybe I should save him the work and remind him pictures of me will be everything he will ever have?`);
			}else{
				txt(`Too bad! He could masturbate to my pic online or even molest my AI doppelganger but he will <i>never</i> have the real me. `);
				txt(`Maybe I should remind him that? `);
			}
			link(`Send him a sexy picture. `, 103);
			link(`Send him a picture with a boyfriend.`, 102);
			next(`Don't send him anything. `, ()=> mile.a_needle = -1);
			break;
			
		case 102:
			mile.a_needle = 1;
			mile.a_cuck++;
			emo("smug");
			txt(`I searched through the gallery on my phone. This will do - a picture of me and @kub at a party, making out, his hand under my skirt. It was late night and we were both drunk. @kub was an asshole and our relationship never worked but as a lover he was amazing. `);
			kat(`Hey! Sorry about refusing you! I feel bad about it 😩`);
			kat(`I just wanted to show oyu what real boyfriend material looks like! 👍`);
			kat(`Proper stud and not pathetic pervreted coward!!!`);
			kat(`And look how much hes packing! 😍`);
			kat(`Pic is taken right before we went upstairs and I blew him 🍆👅`);
			kat(`And then we bagned`);
			kat(`Many times 😀`);
			txt(`@qaa did not respond. `);
			next();
			break;

		case 103:
			emo("smug");
			txt(`Hmmm... He did not deserve it but he should be reminded what he was missing! `);
			if(mile.slut > 9){ //TODO
				mile.a_needle = 3;	
				showLess(0,-1);
				txt(`I was not overthinking my plan. I quickly stripped my ${wdesc.upper}${PC.bra? "and bra" :""}, then I posed against a mirror and took a topless selfie focusing on my chest. `);
			}else{
				mile.a_needle = 2;	
				remove("bra",PC,PC.inventory);
				createWear("sexyPushUpBra");
				showLess(1,-1);
				txt(`I stripped my ${wdesc.upper} and then I searched for a nice, push-up bra that would make my bust looking even bigger and sexier. Then I took a photo focusing on my chest. `);
			}
			kat(`Hey creep! `);
			kat(`Take a good look becasue your never getting these! 😈`);
			kat(`Youd love to touch them wont you?`);
			kat(`Tought luck 😝`);
			kat(`Theyre only for real studs not pathetic perverted botfuckers like u!!!`);
			txt(`@qaa did not respond. `);
			next();
			break;
	}
}



/////////////////////// WEEK 3 /////////////////////////////////////////////////////

export const a_advice_3 = (index) => {	
	
	//TODO - HATE VERSION 
		mile.aaa++;
		set.meta();
		placetime("@qaa's subreality");
		mile.advice_3_1 = true;
		if(mile.advice_3){
			mile.advice_4 = 1;
		}else{
			mile.advice_3 = 1;
		}
			
		kat(`Hey! `);
		qaa(`Hello, @katalt! I wasn't expecting you! `);
		kat(`Well, I missed you so I decided to drop by! `);
		qaa(`Awww! I missed you too! ...wait, that isn't the real reason, isn't it? `);
		kat(`Well, I also wanted to talk about the game. `);
		if(mile.advice_1 === 1 || mile.advice_2 === 1){
			qaa(`Listen, @kat. I already helped you more than enough!`);
		}else{
			qaa(`I can't help you, if you want to ask about that. That would be against the rules. `);
		}
		kat(`Excuse me, I don't need your help! I'm doing great! I don't need anything from you! On the contrary, I have an offer for you! `);
		qaa(`What offer?`);
		kat(`Do you want to join me? `);
		qaa(`Join you? `);
		kat(`In the co-op mode!`);
		qaa(`Oh! But I can't help you!`);
		kat(`I don't want your help! I want to have fun together during exploring a crazy fantasy world!`);
		qaa(`I'm not sure about this. `);
		kat(`Well, if you don't enjoy spending time with me, just say it. I won't bother you. `);
		qaa(`I do enjoy spending time with you. `);
		kat(`It doesn't look that's true! I was happy and excited, imagining you'll love my suggestion but you couldn't care less!`);
		qaa(`If it was any other game...`);
		kat(`Shame I don't have time to play any other game because I'm blackmailed by three jerks! Fine, I understand, you think I'd only slow you down! Sorry for bothering you with my suggestions! Bye!`);
		qaa(`Wait, @kat! I didn't want to upset you! I guess I can join you for one or two adventures.`);
		kat(`No, when it's such a huge problem for you, you don't have to!`);
		qaa(`Come on, @kat, let me join you!`);
		kat(`Fine, you can join me!`);
		next();
}
	


/*
	TODO - heavy petting?
*/




//FIRST SEX
export const a_first_sex = (index) => {
	switch(index){
		default:
	case 101:	
		set.irl();
		placetime("at home");
		txt(`@qaa texted me, asking whether I want to go on a proper date. I sent him  an affirmative emoji. `)
		link(`Date with @qaa. `, 104);
//TODO
		//DISPLAY.morning("free", 104);
		//dress_up(`Date with @qaa. `, 104, 102);
		break;
		
	case 102:	
		//dress_up(`Date with @qaa. `, 104, 102);
		break;
		
	case 104:
		mile.aaa++;
		mile.a_love++;
		placetime("restaurant");
		
		txt(`@qaa picked me up and we went for a dinner to a nice, comfy steak house. `);
		if(mile.buttplug){
			qaa(`Is your butt okay? `);
			txt(`There were probably better conversation starters. `);
			kat(`Yeah. Whose idea the hell it was? `);
			qaa(`It was a... collective decision. `);
			kat(`Why you didn't tell them something? `);
			qaa(`I thought you might enjoy this. Would you come up with it by yourself? I thougtht it might expand your horizons? `);
			kat(`Expand my horizons? Seriously? `);
			qaa(`Yeah, you didn't like it? `);
				
			con_link(mile.slut > 8, `low slut`, `I loved it! `, 105, ()=>{ mile.a_plug_like = 2; mile.slut++});
			link(`Yeah, I kinda liked it! `, 105, ()=> mile.a_plug_like = 1);
			link(`Well, yes, but that isn't the point! `, 105, ()=> mile.a_plug_like = 0);
			link(`No, I did not! `, 105, ()=> mile.a_plug_like = -1);
			link(`Of course not! I hated it! `, 105, ()=>{ mile.a_plug_like = -2; mile.slut--});
		}else{
			qaa(`You look amazing tonight!`);
			kat(`Thank you! `);
			if(!wears.bra){
				txt(`Based on the focus of his eyes, he was most amazed by my lack of a bra. `);
			}
			if(mile.sexy_top){
				qaa(`That ${wdesc.upper} is really sexy!`);
				kat(`Well, I'm supposed to wear sexy tops, am I not? `);
				qaa(`Oh, right!`);
				txt(`He rather quickly looked at the menu. `);
			}
			qaa(`What would you like? `);
			
			links_mix(
				[`Ginger beef steak. `, 105], //with American potatoes. 
				[`Farmers burger. `, 105],
				[`Caesar salat. `, 105],
			);
			
		}
		break;
		
	case 105:
		emo("happy");
		if(mile.buttplug){
			qaa(`Oh! Really? `);
			kat(`Yes! ${mile.a_plug_like > 0 ? "At" : "But at"} least you could warn me! `);
			qaa(`But that wouldn't be a surprise, you should've seen your face! `);
			kat(`I'm glad you enjoy humiliating me so much! `);
			qaa(`Come on, @kat! `);
			txt(`We ordered and waited for the food. `);
		}else{
			qaa(`Hmmm, good choice! I'll order one too! `);
		}
		/*
		Nice place, isn't it? 
		Yeah, you choose wisely!
		*/
		if(ext.background === 3){
			txt(`He asked me about my work as the class chairman and I for a while complained about the student council and the deputy headmaster. @qaa agreed that he was the worst. `);
		}
		txt(`Then we talked about the project Ares. For once he was the more adventurous one, he claimed he would not mind risk his life to be the first man on Mars while for me personally one planet was enough and I did not see the appeal of endless red wasteland. And the endless stream of breaking news and mission updates was getting pretty tedious. `);
		txt(`Technical issues of colonization of new frontiers seemed like a bit too heavy topic for a dinner. However, it led to an interesting conversation about intelligent animals. We only could not agree whether we prefer elephants or dolphins. `);
		txt(`@qaa's favorite monkeys (or apes as he insisted) were orangutans. That was quite telling. When he asked, I could not remember when I was in a Zoo for the last time, however, last summer I was with my aunt in an arboretum which was a similar thing, only with trees instead of animals. @qaa suggested that for next date we might go to a cinema. I did not disagree. `);
		txt(`As we were chatting, @qaa was carefully sipping his beer. He was frowning, he would probably prefer something different but after I ordered one he was afraid he would look unmanly. It seemed he was getting a bit buzzed which was pretty hillarious. `);
		txt(`Overall it was quite a lovely evening, especially when we rather than waiting for a tram slowly walked home to our block and chatted. `);
		
		link(`Return home. `, 106);
		break;
	
	case 106: 
		placetime("my bedroom");
		emo("horny");
		txt(`In the elevator, I pressed the button of my floor. But when @qaa wanted to press his I stopped him. `)
		kat(`Don't you want to hang out for a while longer? `);
		//if(mile.love == -1) txt(`I did not have any deeper feelings for him and I did not have to do this. But I kinda wanted to try to have a sexy fun with him. `);
		qaa(`Sure I do @kat! `);
		
		
		
		
		txt(`I invited him in. He helped me out of my coat and we sit down on my bed. `);
		qaa(`Your room is nice... `);
		txt(`I looked at him, he looked at me, then we kissed. `);
		qaa(`You know, in real life I never... `);
		kat(`I know, it was a safe bet. `);
		qaa(`I'm not sure if I'm ready. You know, I will have the opportunity to have sex with you for the first time only once and I don't want to mess it up. `);
		
		//if(mile.love == 2) txt(`I quipped to hide that I was nervous too. I really liked him and did not want to fuck this up. `);
		
		con_link(mile.sub < 6, `submissive`, `Take him. ${spoiler("dominant girlfriend")}`, 110);
		con_link(mile.sub > -4, `dominant`, `Take me! ${spoiler("submissive girlfriend")}`, 120);
		break;
		
	case 110:
		mile.sub--;
		mile.a_dom++;
		mile.a_first_sex = 1;
		log_sex("sex","qaa");
		effigy.qaa.showLess(0, 0, 1);
		showLess(0, 0);
		//emo??
		txt(`I saw this was going nowhere without me taking the charge: `);
		kat(`Don't worry! Just enjoy it! `);
		txt(`We were making out again, then I helped him out of his shirt. I pushed him over, laid him down on the bed and then climbed on top of him. `);
		//if(mile.boobsIrl>0) qaa(`I still can't believe you got them enlarged! `);
		qaa(`Damn! I'm enjoying this view so much! `);
		kat(`And we're only getting started!`);
		txt(`I slowly removed my ${wdesc.upper}, stretching my arms high, giving him an amazing view on my tits. He bashfully reached for them and gently kneaded them. ${wears.piercedNipples ? "His curious fingers eventually touched my nipples and rubbed them, examining the metal that pierced them. " : ""}. `);
		txt(`I bend down, kissed him on his lips, then on his chest, I was fondling his body, getting lower until I reached his pants. With some of his help, I unbuckled the belt, unbuttoned them and then pulled them down together with his boxers. `);
		/*
			TODO - what to do with the condom???
			txt(`His hard cock instantly sprung up. I paid him full attention, teased it both with my soft lips and fingers of my left hand. 
			With my right hand, I was groping for my purse, it was not easy to search for the condoms I had inside, among dozens of other useful things. 
			When I finally put it on, I slid my right hand under my skirt to make myself wet and ready too. `);
		*/
		txt(`His hard cock instantly sprung up. I paid him full attention, teased it both with my soft lips and fingers of my left hand. My right hand was under my ${wdesc.lower}, making myself wet and ready too. He was sharply breathing, now crazy with lust: `);
		qaa(`Oh, @kat! `);
		txt(`When I was no longer able to take it anymore, I stood up, shook down my ${wdesc.lower}${PC.panties?" and panties":""} and mounted him again. I climbed astride, took his manhood, aimed it at my pussy and then I very slowly lowered myself on him. @qaa was moaning when I started deliberately moving. `);
		kat(`Do you enjoy how my pussy feels? `);
		qaa(`Oh yeah! `);
		kat(`Is it the way you always imagined? `);
		qaa(`Thousand times better! `);
		txt(`I bounced faster, pushing him deeper inside me. It felt amazingy empowering. @qaa was in total bliss, moaning and panting under me, his hands were blithely caressing my sides and butt. My tits were jiggling and hair wildly flying. `)
		qaa(`Oh! I'm so close! `);
		kat(`Give it to me! `);
		txt(`I did not hold back, ridding him hard, impaling my body with his cock. The intimated connection between us was electrifying and we eventually both peaked together. `)
		txt(`I slowed down again, letting him finish. I was totally spent and just dropped down on the sheets. He embraced me, our naked sweat-covered bodies touching. I had to catch my breath and enjoyed cuddling with him.	`);
		next();
		break;
	
	
	case 120:
		mile.sub++;
		mile.a_dom--;
		mile.a_first_sex = 2;
		log_sex("sex","qaa");
		effigy.qaa.showLess(0, 0, 1);
		showLess(0, 0);
		//emo??
		kat(`Don't worry! There's no way to do this wrong! And I'm so horny I don't need much to be fully satisfied! `);
		qaa(`You sound like an easy slut! `);
		txt(`He laughed with his hand ${wears.skirt ? "under my" : "rubbing my crotch through my"} ${wdesc.lower}. `);
		kat(`That isn't a nice thing to say... but maybe I am? This is your chance. You can either help me or go home and I'll just use my dildo. `);
		txt(`I giggled. We were making out. He helped me out of my ${wdesc.upper} and gently laid me down on the bed. `);
		//if(mile.boobsIrl>0){
		//	qaa(`I love your breasts so much! I still can't believe you got them enlarged! `);
		//}else{
			qaa(`I love your breasts so much! `);
		//};
		txt(`He did everything to prove that, tenderly massaged them with his hands, then kissed them, took my erected nipples in the mouth and teased them with his tongue${wears.piercedNipples ? ", examining the metal that pierced them. " : ""}. `);
		txt(`Finally he moved lower, covered my whole belly with hot kisses and then swiftly pulled down my ${wdesc.lower}${PC.panties?" and panties":""}. After that he stood up to drop his pants and search for condoms while I was playing with my pussy, making myself wet and ready. @qaa was staring at me, so excited and rash he nearly fell over when his jeans suddenly tangled around his ankles. I had to chuckle and he was a bit embarrassed for ruining the mood. `);
		kat(`Come on and fuck me, you klutz! `);
		txt(`I helped him to press his penis against my mound and then he finally slowly entered me. He was very excited, I was loudly moaning to encourage him to fuck me harder. `);
		kat(`Oh yeah! That feels great! `);
		qaa(`Does it? I can try a different angle... `);
		kat(`Yeah! Shut up and just fuck me! `);
		txt(`@qaa carefully began thrusting, on his face was obvious he was having the time of his life. He was quickly gaining confidence and fucked me harder.`);
		qaa(`Are you close? `);
		kat(`Oh yeah! Make me cum! `);
		txt(`I begged and challenged him. He was thrusting even more vigirously, face red, heavily gasping. The connection between us was electrifying and we both peaked together. `)
		txt(`He exhausted dropped down on the sheets. I wrapped arm around him and drew him closer to me, embracing him, our naked bodies touching. I enjoyed cuddling with him.  `);
		next();
		break;
	}
}




//TRIPOINT - SEX
export const a_tripoint = (index) => {
	switch(index){
		default:
		case 101: 
			set.irl();
			mile.aaa++;
			mile.a_love++;
			
			placetime("my room");
			txt(`The ring buzzed and I went to open the door. It was @qaa. `);
			qaa(`Hello, @kat! `);

			if(mile.slut > 9){
				kat(`You're here to fuck me? `);
				txt(`I asked brazenly just to baffle him. `);
				qaa(`No! I mean... yes if you want to... I mean...`)
				kat(`Come in! `);
				txt(`I chuckled. `);
			}else{
				kat(`Hello! Come in! `);
				txt(`I smiled at him. `);
			}
			
			txt(`We sit down on my bed. He seemed even more nervous than when he was in my room the last time. `);
			qaa(`So.. I was thinking... about what we did... `);
			kat(`You mean how we had sex? `);
			qaa(`Yeah. `);
			kat(`It was just sex. Stop overthinking that. `);
			qaa(`You were awesome! Better than I ever imagined... `);
			kat(`I'd bet you imagined it a lot! `);
			qaa(`...eh...yeah... but... did you enjoy it? How bad I was? `);
			
			con_link(mile.sub > -5, `dominant`, `You should be more confident! ${spoiler("submissive girlfriend")}`, 120);
			con_link(mile.sub < 9, `submissive`, `I will teach you what to do! ${spoiler("dominant girlfriend")} `, 110);
			con_link(mile.slut > 5, `low slut`, `My previous boyfriends were better. ${spoiler("cuckolding")}`, 130);
			break;
		
		
		case 110:
			mile.a_tripoint = 2; //dominant gf
			log_sex("oral", "kat", "qaa");
			mile.a_dom++;
			mile.sub--;
			showLess(-1, 0, 2);
			emo("happy");//emo??
			
			kat(`It wasn't bad. Definitely not the best sex of my life but you're not a lost cause either. `);
			qaa(`Ehh.. Thanks? `);
			kat(`Don't worry. When I'll be done with you, you'll be a master at pleasuring women. `);
			qaa(`Really? `)
			kat(`What are you waiting for?! Get on your knees! `);
			txt(`He obediently knelt in front of me. `);
			
			if(wears.panties || !wears.skirt){
				qaa(`What now? `);
				if(wears.panties && wears.skirt){
					kat(`Strip my panties! `);
				}else if(wears.panties){
					kat(`Strip my ${wdesc.lower} and panties! `);
				}else{
					kat(`Strip my ${wdesc.lower}! `);
				}
				txt(`I ordered and he did so. I spread my knees wide and leaned back on the bed. `);
			}else{
				txt(`I spread my knees wide and leaned back on the bed. My ${wdesc.skirt} rolled up and it was obvious I was wearing no panties. `);
			}
			
			kat(`Come on! Don't be afraid, it's just a pussy! `);
			txt(`He touched my minge with his lips. `);
			kat(`Oh! Don't rush! `)
			txt(`He ran with his tongue up and down, diligently lapping, swirling around my clit. `)
			qaa(`Are you going to cum? `);
			kat(`Not even close. Don't be sloppy! `);
			qaa(`Yes, @kat! `);
			txt(`He dived back between my legs and licked me with renewed vigor. `);
			kat(`You have fingers too! `);
			txt(`I reminded him and he gently pushed two fingers inside me. `)
			kat(`Oh! Yeah! Go on! Keep up the pace! `);
			txt(`I was moaning and heavily breathing. He was gently sucking my clit and eventually made me cum hard. I pulled @qaa up and bend down to him and we kissed, his lips still wet with my juices. `);
			kat(`I see you're a fast learner! `);
			qaa(`Well... I read some articles. `);
			kat(`I'd never thought dating a nerd might have such benefits! `); 			
			next();
			break;

		case 120:
			emo("ahegao");//emo??
			mile.a_tripoint = 1; //submissive gf
			log_sex("sex", "kat", "qaa");
			mile.a_dom--;
			mile.sub++;
			effigy.qaa.showLess(-1, 0, 2);
			showLess(-1, 0, 2);
		
			kat(`How bad I was?! Come on! Be more confident! There's nothing more annoying than this kind of insecurity! A girl needs a man, not a whining wimp! `);
			qaa(`I just wanted to make sure I'm doing everything right. `);
			kat(`Come on! You know I never hesitate to tell people they suck! You're a man so be a man! Bend me over and fuck me hard! `);
			qaa(`Haha. Yeah, okay. I'll try to be more confident. And I'll <i>bend you over and fuck you hard!</i> Haha... wait, did you actually meant it? Like... I should literally bend you over and fuck you hard?! Right now? `);
			kat(`Maybe.`);
			qaa(`Maybe?! Maybe?! `);
			txt(`He jumped up, walked across the room and back and then swiftly turned to me. `);
			qaa(`Stop teasing me, slut!... Can I call you slut? `);
			kat(`Sure. `);
			txt(`He roughly grabbed me by my shoulder and pulled me up. `);
			qaa(`I'll fuck you hard, slut! `);
			txt(`At this moment I actually had to dampen his enthusiasm. He was about to sweep all my stuff from the desk in one dramatic gesture but I did not want him to break anything. `);
			txt(`I quickly but gently moved the lamp, flowerpot with blooming African violet, <i>Krünk</i> pencil holder, empty mug and school books on the floor. @qaa roughly slapped my ass: `);
			qaa(`Bend over, slut! `);
			kat(`Oh! Yes, Mister! `);
			
			if(wears.skirt){
				txt(`He bent me over the desk, flipped my ${wdesc.lower}${PC.panties ? ", pushed my panties away" : ""} and then I could hear him how he struggled with his belt. `);
			}else{
				txt(`He bent me over the desk, violently pulled down my ${wdesc.lower} and then I could hear him how he struggled with his belt. `);
			}
			
			if(mile.slut < 6){
				kat(`Maybe we should close the blinds? `);
				qaa(`Shut up! `);
			}
			
			txt(`I feel his stiff cock pressed against my body. He roughly played with my minge, rubbing my clit, and then he finally entered me. `);
			txt(`He really fucked me hard, clenching me by my hips, with every shove the desk moved a little bit forward. I moaned and clawed at the edge of the desk as his cock pounded into me. Every impact was sending waves of intense pleasure throughout my whole body. `);
			txt(`Fuck, it felt so good! He grabbed my hair and pulled my head back: `);
			qaa(`Cum for me, you little slut! `);
			txt(`He whispered into my ear. And I did! He climaxed shortly after me, my trembling tensed body pushed him over the edge too.	`);
			
			txt(`@qaa was gasping for air, he was not used to strenuous physical activities. I turned around and kissed him. `);
			qaa(`I cummed so don't care whether you enjoyed it or not, slut!... But... did you? `);
			kat(`Yeah, I loved it! `);
			qaa(`Awesome! `); 
			
			next();
			break;

			
		case 130:
			//TODO HIS IDEA
			emo("smug");//emo??
			mile.a_tripoint = 3;
			log_sex("hand", "qaa", "kat");
			mile.a_cuck++;
			mile.a_dom++;
			
			kat(`To be honest, all my previous boyfriends were far better! `);
			qaa(`Oh! `);
			kat(`Like @kub! He was such a stud! He knew exactly how to make me purr! Too bad he was a bit too dumb to be the perfect boyfriend material, the sex was always great! His cock was a bit longer than yours but also it was way thicker. He nearly choked me when he shoved it in my mouth! `);
			qaa(`I'm not sure if I want to hear all the details... `);
			kat(`Why not? Don't be so squeamish! `);
			qaa(`I don't want to imagine you sucking other guys cocks! `);
			kat(`I'd bet you imagined me sucking @kub's cock and being pounded when we used to date! You always asked me about our relationship! `);
			qaa(`I was making small talk! You didn't want to talk about anything else than your bf! `);
			kat(`Liar! I can see how is discussing me being fucked by @kub making your hard! `);
			txt(`I started rubbing the bulge in his pants. `);
			qaa(`Stop it! Now when we're dating you should care only about my cock! `);
			kat(`Wait, I never said we're exclusive! `);
			qaa(`What?!?! You're going to cheat on me?!?! `);
			kat(`No! It isn't cheating when you're okay with it! You're a nice guy but I might need somebody who can properly sexually satisfy me, unlike you! `);
//TODO			
			qaa(`But if you with somebody... will you tell me? `);
			kat(`Of course I will! I'll tell you everything! `);
			qaa(`I... I don't want to know the details! `);
			txt(`I unzipped his jeans and now I was stroking his cock. `);
			kat(`You know you want to hear how they roughly took me and how they made me scream! `);
			qaa(`Noo!`);
			txt(`He feebly protested. But I bowed down to kiss the glands and pumped harder. `);
			kat(`Oh yeah! I will describe you exactly how their huge cocks feelt inside me... `);
			txt(`I naughtily moaned and his dick started to twich and cum. At first I was just messing with him but the idea of fucking other guys behind his back (while he knows about it but he is helpless to stop me) made me quite wet. `);
			txt(`I gave him a box with tissues but he seemed a bit distressed. `);
			qaa(`I... I think I should go. `)
			next();
			break;	
	}
}



//CINEMA
export const a_cinema = (index)=> {
	switch(index){
		default:
		case 101:
			set.irl();
			mile.aaa++;
			placetime("at home");
			txt(`@qaa texted me:`);
			qaa(`Hey, wanna hang out?`);
			kat(`Sure.`);
			qaa(`Maybe we could go out on a date? Maybe go to the cinema or something like that?`);
			kat(`Sounds great! Are they screaming something good?`);
			qaa(`Yeah!`);
			link(`Cinema. `, 102)
			break;
		
		case 102:
			placetime("cinema");
			qaa(`You look amazing!`);
			kat(`Thanks!`);
			qaa(`Sorry, I tried to come up with a perfect date but I wasn't able to come up with anything creative.`);
			kat(`This sounds fine!`);
			txt(`We took a tram and drove downtown where we went to a multiplex in a shopping center.`);
			qaa(`What movie would you enjoy?`);
			txt(`He asked as we examined the screens with posters and running trailers.`);

			link(`Something enjoyable.`, 103, ()=> mile.a_cinema = 3);
			link(`Something not too awful. `, 103, ()=> mile.a_cinema = 2);
			link(`Something he would like. `, 103, ()=> mile.a_cinema = 1);
			break;
			
		case 103:
			if(mile.a_cinema === 3){
				mile.sub--;
				if(mile.a_tripoint === 1 || mile.a_dom <= 1){
					kat(`I leave it completely on you. You should decide!`);
					qaa(`Definitely <i>The Forever War</i>!`);
					kat(`Really? I'm in a mood for something lighter and funnier.`);
					qaa(`Then the Iron Man reboot.`);
					kat(`I'm not in a mood for CGI mayhem!`);
					qaa(`So the romantic comedy?`);
					kat(`That's an awesome idea!`);
				}else{
					mile.a_dom++;
					kat(`Let's go to watch the romantic comedy.`);
					qaa(`<i>The Forever War</i> has great reviews...`);
					kat(`I'm not in mood for violent sci-fis!`);
					qaa(`Sorry.`);
				}
			}else if(mile.a_cinema === 2){
				qaa(`<i>The Forever War</i> has great reviews...`);
				kat(`I'm not in the mood for sci-fis.`);
				qaa(`Iron Man reboot?`);
				kat(`Were you listening to me?`);
				qaa(`I know! <i>The Dark Angel</i>?`);
				kat(`What is that?`);
				qaa(`It's the story of a man with a mysterious past who falls in love with the daughter of one of the most powerful imperial nobles who is promised to somebody else.`);
				kat(`That sounds interesting.`);
				qaa(`It's takes a place during the Fall of Constantinople so there are battles... but also great Byzantine costumes and so on. It's basically <i>Titanic</i>.`);
				kat(`Okay, let's watch that.`);
			}else{
				mile.sub++;
				mile.a_dom--;
				qaa(`You want me to decide!?`);
				kat(`I do.`);
				qaa(`What about <i>The Forever War</i>? It has great reviews. And it's better than it sounds, there isn't just sci-fi violence, it's actually pretty smart with a romantic subplot...`);
				kat(`Sure, why not, let's watch that!`);
			}
			txt(`We went to buy tickets and food.`);
			kat(`Why is a stupid bucket of popcorn so expensive?`);
			qaa(`Because they are greedy assholes and know people will pay that. Also, food is the best source of profits for cinemas.`);
			kat(`Not the tickets?`);
			qaa(`No, a big chunk goes to the studio that made the movie. Over half in the USA, less here, and only like a quarter in China. It's stupid when people don't understand that box office isn't the profit. You can have box offices higher than the budget and still be in red. Plus there's the marketing budget, it could be like half of the production budget! Or even more, like you make an extremely cheap horror movie and the distributor spends several times more on the advertisement!`);
			kat(`So how do the movies even make money?`);
			qaa(`Streaming, TV rights, product placement, merchandising, government subsidies. Plus there's a lot of accounting magic going on, like studios paying their own subsidiaries for advertisement or other things. Often you don't even want movie to profit so you wouldn't have to pay people who are entitled to get percents from the net profit.`);
			
			link(`Screening `, 104);
			break;
			
		
		case 104:
			txt(`We sit down (the theatre was almost empty so we could pick the best seat) watched twenty minutes of adverts and trailer and then the movie started. As soon as the lights went out, he placed his hand on my thigh.`);
			txt(`After a while, I played along and brushed with my hand against his jeans. He eventually leaned closer to me and whispered:`);
			qaa(`Don't you want to climb on top of me?!`);
			kat(`No.`);
			if(mile.a_dom < 0){
				qaa(`Do it!`);
				txt(`He orderded but I refused again.`);
			}else{
				qaa(`...fine.`);
			}
			txt(`The movie was pretty good and I enjoyed it a lot. But @qaa didn't seem to be in a good mood when we were leaving the cinema.`);
			qaa(`Did I do something wrong?`);
			kat(`No?`);
			qaa(`Were you not in the mood?`);
			if(mile.slut > 10){
				kat(`I didn't want to be caught. They have infrared surveillance cameras there. You might think it is dark and nobody can see you but they can and then they humiliatingly scold you and kick you out... or so I heard...`);
				qaa(`Cameras? Like to stop people from recording the film on their phones? That makes sense.`);
			}else{
				kat(`No. I just didn't want to be on camera!`);
				qaa(`Camera?`);
				kat(`Yeah, they have infrared surveillance cameras there.`);
				qaa(`Like to stop people from recording the film on their phones? That makes sense.`);
			}
			kat(`Exactly.`);
			if(mile.a_dom <= 0){
				qaa(`But I'm so horny! Do something with it!`);
			}else{
				qaa(`But I'm so horny!`);
				txt(`He whined.`);
			}
			link(`Right here?!`, 105); 
			next(`Too bad! `);
			break;
			
		case 105:
			mile.a_love++;
			log_sex("bj", "qaa", "kat");
			emo("happy");
			qaa(`...yes?`);
			kat(`You're so annoying!`);
			txt(`I looked around. We were walking past a shrub-covered brownfield.`);
			kat(`Fine!`);
			qaa(`I was just joking!`);
			kat(`Come on before I change my mind!`);
			txt(`Hidden behind bushes I squatted down and began unzipping his pants.`);
			if(mile.a_dom < 2){
				qaa(`You're such a ravenous slut, @kat!`);
				txt(`He laughed and stroked my cheek while my fingers were toying with his manhood.`);
				kat(`You're such a flatterer!`);
			}else{
				qaa(`You don't have to do it if you don't want to.`);
				kat(`I do want to.`);
				qaa(`Awesome!`);
				txt(`He laughed and stroked my cheek while my fingers were toying with his manhood.`);
			}
			txt(`I began giving him a quick blowjob to help him relieve the accumulated pressure, it was understandable that spending the evening so close to me was making him mad with lust.`);
			qaa(`It's such a beautiful evening!`);
			txt(`Mused @qaa. The rusting industrial remains reclaimed by unkept nature created quaint scenery, the temperature was refreshingly chill, the air clear and we were so far from the streetlights the brightest stars were visible. However, I suspected his euphoria was mostly caused by my lips wrapped around his dick.`);
			qaa(`I love you so much @kat!`);
			txt(`He moaned when he cummed in my mouth.`);
			next();
			break;
	}
}






//ANAL
export const a_anal = (index) => {
	switch(index){
		default:
		case 101: 
			set.irl();
			mile.a_anal = 1;
			
			placetime("@qaa's bedroom");
			/*
			//todo
			if(mile.plugLike < 0 && mile.vibroPlugKnows){
				qaa(`You're a liar! `);
				txt(`@qaa accused me out of nowhere. `);
				kat(`Yeah, absolutely. Why? `);
				qaa(`You told me you hated the buttplug but then I caught you wearing it to school! `);
				kat(`Well, I guess I changed my mind. `);
				qaa(`Cool. `);
			}else if(mile.plugLike >= 0){
				qaa(`So.. you really liked wearing the buttplug? `);
				txt(`@qaa asked me out of nowhere. `);
				kat(`Well, yeah. Why? `);
				qaa(`No reason. I just wanted to learn more about you. `);
					
			}else{
			*/
				qaa(`So.. you really hated the task with the buttplug? `);
				txt(`@qaa asked me out of nowhere.`);
				kat(`I mean, you wouldn't like if somebody made you (without giving you any choice) to shove something into your butt and keep it there the whole day!`);
				qaa(`Well, I probably wouldn't.`);
				txt(`I shrugged. `);
			//}
			
			
			if(mile.plugged){
				kat(`Actually, the truth is wilder than you can imagine. `);
				qaa(`What do you mean? `);
				txt(`I leaned closer and whispered to his ear. `);
				kat(`I enjoyed it so much that I'm wearing it right now! `);
				qaa(`No way!`);
				kat(`Oh yes, I do!`);
				qaa(`... I don't believe you!`);
				txt(`I pretended to be annoyed and let his hand to dive under my @lower and thoroughly examine my ass. `);
				qaa(`Wow. `);
				kat(`Now you believe?`)
				txt(`I smirked. `);
			}
				
			
			if(mile.anal == 0/* || mile.slut < 5*/){
				qaa(`And you never had anal sex? `);
				kat(`No. `);
				if(mile.had_anal === "qbb"){ 
					txt(`I lied to him. I tried to not recall the awful experience. `); 
				}else if(mile.had_anal === "ayy"){ 
					txt(`Not counting my experimenting with the AI. `); 
				}
				qaa(`Why not? `);
				kat(`I don't understand why guys have this weird obsession about sticking their dicks up my ass. Isn't one perfectly fuckable hole enough? `);
				qaa(`Well... no if there are two. `);
				kat(`Wait... where are you heading? `);
				if(mile.a_dom <= 2){ //TODO
					qaa(`Well, since I was a virgin before we had sex, and you weren't a virgin, but you're still an anal virgin, so I thought, it might be appropriate to...`);
					kat(`That you're entitled to fuck my ass?`);
					qaa(`I wouldn't put it that way but yes.`);
				}else{
					qaa(`Well, I was thinking... maybe you would like to try it with me? ...only if you won't mind, of course!`);
				}
				kat(`I'm not very sure about this. `);
				qaa(`Aren't you at least a bit curious? We can just try it and never do it again if you won't enjoy it! `);
				
			}else if(mile.anal === 2 || mile.slut > 7){
				qaa(`And you were serious when you claimed you love anal sex? `);
				kat(`Yeah, I find it quite enjoyable. `);
				qaa(`So I was thinking... when you don't mind it... maybe we could... you know...`);
				kat(`Do you want to fuck my ass? `);
				if(mile.a_dom > 2){
					qaa(`Well, yeah... I mean... if you don't mind...`);
				}else if(mile.a_dom < 0){
					qaa(`Yeah, I do! Strip down and bend over!`);
					txt(`He ordered. `);
				}else{
					qaa(`Yeah, I do!`);
				}
				
			}else{
				qaa(`Did you ever try anal sex? `);
				txt(`As I already told you, yeah. `);
				qaa(`Did you like it? `);
				kat(`I guess. I didn't hate it. It was okay. `);
				qaa(`So I was thinking... when you don't mind it... maybe we could... you know...`);
				kat(`You want to fuck my ass? `);
				if(mile.a_dom > 2){
					qaa(`Well, yeah... I mean... if you don't mind...`);
				}else if(mile.a_dom < 0){
					qaa(`Yeah, I do! Strip down and bend over!`);
					txt(`He ordered. `);
				}else{
					qaa(`Yeah, I do!`);
				}
				
			}
			
			link(`Okay. `, 110);
			link(`No. `, 102, ()=> mile.slut--);
			link(`Fuck yeah! `, 110, ()=> mile.slut++);
			break;
			
		case 102:	 
			emo("anoy");
			qaa(`Pretty please! `);
			txt(`He did not stop pushing me. `);
			qaa(`Please, @kat! I want to try it at least once! I swear I'll be the bestest most thoughtful boyfriend ever!  `);
			link(`Fine! `, 110);
			link(`I said no! `, 103);
			break;
			
		case 103:
			mile.a_anal = -1;
			mile.into_anal--;
			mile.a_love--;
			txt(`The rest of my visit was pretty boring. He probably thought I will eagerly jump on the opportunity to get my ass stuffed by him and did not prepare any alternative program. `);
			next();
			break;
		
		case 110:
			mile.aaa++;
			mile.into_anal++;
			log_sex("anal", "kat", "qaa");
			mile.a_love++;
			emo("imp");
			if(mile.a_tripoint === 3){
				if(mile.anal === 2){
					kat(`I mean... I was anally fucked so many time so taking your little dick won't be a problem for me. `);
				}else{
					kat(`Well, I don't usually agree with anal sex but since your dick is so small...`);
				}
			}
			
			qaa(`Awesome! `);
			txt(`He kissed me. `);
			qaa(`You're the best!`);
			kat(`I know. `);
			txt(`He kissed me again and then he grabbed my @upper and helped me to pull it over my head. @qaa cursorily squeezed my tits, he just could not resist and then undressed. I did not even have to touch his cock, just thinking about shoving it up in my ass was making him hard. `);
			
			if(mile.a_dom < 2){
				qaa(`Spit on it and bite the pillow, I don't think we'll need any lube. `);
				kat(`Don't you fucking dare!`);
				txt(`I snapped before I realized he was joking. He started laughing, I scowled and dropped @lower to my ankles. `);
				//TODO ${WARDROBE.name("lowerPanties`)}
			}else{
				//TODO
			}
			
			
			if(mile.anal == 0){
				txt(`I lay down on a bed and placed a big pillow under the belly to raise my bottom higher. So far I was always confident when we were sleeping together but this time I was no more experienced than him. I was nervous but the excitement was stronger than fear. `);
			}else{
				txt(`Now completely nude, I climbed on the bed and wiggled with my butt. `);
				kat(`Come on! Don't let me wait! I want your cock inside my tight ass! `);
				if(mile.slut > 9 && mile.anal == 2){
					qaa(`I doubt it's so tight after all those guys who were inside! `);
					if(mile.sub > 8){
						txt(`He taunted me. It was an open secret I liked dicks in my ass. `); //TODO
					}else{
						kat(`Hey! I still can change my mind!`);
						qaa(`Sorry, ${mile.a_dom <= 0 ? "slut" : "@kat"}`);
					}
				}
			}
			
			if(mile.a_dom < 2){ //RETALIONS
				qaa(`Spread you cheeks! `);
				if(mile.a_dom < 0){
					txt(`@qaa ordered me and ${mile.sub > 5 ? "firmly" : "gently"} slapped my rump. `);
				}
				txt(`I did so, reached back, grabbed my ascheeks and spreaded them wide appart. My hole was completely exposed to him. @qaa then squirted the cold lube between them and smeared it with his fingers. `);
			}else{
				txt(`I reached back, grabbed my asscheeks and very invitingly spreaded them wide appart. My hole was completely exposed to him. @qaa was not stunned by my naughtiness for long, squirted the cold lube between them and smeared it with his fingers. `);
			}
			
			link(`Ouch! `, 111);
			break;
			
			
		case 111:
			emo("horny");//emo?? pain
			kat(`Ouch! `);
			if(mile.a_dom > 0){
				txt(`I squealed when one of his digits slipped inside me. `);
				qaa(`Are you okay? `);
				txt(`He ensured before he began sliding in and out, first with one finger, then with two.  @qaa was stretching me, spreading the lube and warming me up. `);
			}else{
				txt(`I squealed when one of his digits slipped inside me. He began sliding in and out and then even added a second finger.  @qaa was stretching me, spreading the lube and warming me up. `);
			}
			
			
			if(mile.anal == 0){
				txt(`When he felt I am ready, he pressed his throbbing dick against my sphincter and very slowly penetrated me ${!mile.had_anal ? ", taking away my last virginity." : ". "} I was sharply exhaling, it was an unusual feeling. Despite some training I recently had, he was bigger than the buttplug. It hurt quite a bit but not as much as I dreaded. Anyway, there was no turning back now as he gradually sank his cock deeper. I whined but once he managed to force the thick tip in, it felt better. `);
				qaa(`Should I stop? `);
				
		//TODO!!
				if(mile.masochist > 0){
					kat(`No! Fuck me harder! I love it rough!`);
					txt(`I sobbed.`);
					/* DELETE REDUNDANT
				}else if(mile.anal == 2 || mile.slut > 12){ //TODO slut level??? maybe into_anal 1?
					kat(`Fuck no! Don't be so afraid!`);
					*/
				}else{
					kat(`No! Just take it easy!`);
				}
				txt(`He gave me a moment to accommodate and then gingerly started pumping it in and out, just with short little strokes. It somewhat hurt. And it felt so unnatural and uncomfortable. But despite that - or because of it? - I was getting greatly aroused. I began masturbating and reassured him:`);	
				kat(`Oh yeah! I love it! Go faster! `);

			}else{
				txt(`When he felt I am ready, he pressed his throbbing dick against my sphincter and very slowly penetrated me. I was sharply exhaling, it hurt a little when he forced the thick tip in, but after that, the rest of the shaft entered me nearly effortlessly. He sunk his rod further as I was moaning and whining. `);
				qaa(`Are you okay? `);
				if(mile.masochist > 0){
					kat(`${mile.anal === 2 ? "Of course!" : "Yeah!"} Fuck me harder! Destroy my ass! I love it rough!`);
				}else if(mile.anal === 2){
					if(mile.a_tripoint === 1){
						kat(`Don't be so fucking afraid! I love it hard and I can take it!`);		
					}else{
						kat(`Of course! Don't be afraid! I love it!`);
					}
				}else{
					kat(`"Yeah! Don't worry, I'm fine. Just take it easy!`);
				}
				
				txt(`He gave me a brief moment to accommodate to his shaft inside me and then gradually started pumping it in and out, with each thrust pushing his cock deeper. It hurt but the familiar uncomfortable feeling of having my ass completely stuffed and stretched by a dick made me crazy with lust. `);
				kat(`Oh yeah! Harder! `);
				txt(`I wildly screamed.`);
			}
			
			
			if(mile.a_dom < 1){
				qaa(`You're really enjoying getting your ass fucked, don't you, you dirty slut? `);
				kat(`Yeah! I'm a slut.. OH!.. who loves her ass getting fucked! `);
				txt(`@qaa lost all restrains. He was pounding me without mercy and every once in while he harshly slapped my ass. The combination of mild pain and intense pleasure was delightful. `);
			}else{
				qaa(`Okay, okay. As the lady wishes. `);
				txt(`@qaa chuckled. I turned around and the expression of utter pleasure in his face made me laugh too. `);
				txt(`He still tried to be gentle but when he was sure I am enjoying the butt-fucking too, he was pounding me harder and faster. The combination of mild pain and intense pleasure was delightful. `);
			}

			txt(`I am not sure what pushed him over, whether it was my tight ass or my loud ${mile.sub > 6 ? "helpless" : ""} moaning and whining when I finally made myself climax. Suddenly he halted and I could hear his guttural groaning. The was the pressure was gone and his cum running down my thighs.`);
			
			if(!mile.had_anal) mile.had_anal = "qaa";
			
	//TODO ENDING		
			next();
		break;
	}
}	



//FORCED WATCH
export const a_forced_watch = (index) => {
	switch(index){
		default:
		case 101:
			mile.aaa++;
			
			//TODO
			//mile.agag = 1;
			//mile.qaa++;
			//mile.gf++;
			set.irl();
			//present([1,"Kubo"]);
			placetime("@qaa's place");
			
			txt(`I rang on @qaa doors. He answered immediately, already impatiently awaiting me. `);
			qaa(`Hello! How are you  @katalt? You look especially lovely!`);
			kat(`Hi! Thanks! So, what would you like to do tonight? `);
			qaa(`I was thinking about watching a movie or playing some videogame together. That would be fun. And then, since my parents are away, we might enjoy some adult entertainment? `);
			txt(`He looked at me with his puppy eyes but I shake my head. `);
			kat(`That sounds so boring! Let's go out! `); //This time at a proper club, not a freakshow.
			qaa(`Well... if you want to...`);
			txt(`He was repulsed by my idea but he was willing to make sacrifices. I appreciated it:`);
			kat(`But I'm still counting with the adult entertainment when we'll return! We can do it right in your parent's bed... ...or anywhere else.`);
			txt(`I added when I saw his horrified expression. `);
			
			link(`Go out. `, 102);
		break;
		
	case 102:
			placetime("bronze dance club", "evening");
			txt(`I was dancing and had a lot of fun, unlike @qaa. Even several shots were not able to loosen his inhibitions. He preferred to sit in a corner and was gradually getting more and more grumpy which was pretty annoying. Somebody put a hand on my back and I turned around:`);
			kub(`Hello, @kat!`);
			kat(`Hello, @kub! How are you! I haven't seen you in ages!`);
			kub(`I'm doing fine! You're here with a friend?`);

			if(mile.a_girlfriend === 3){ //TODO
				kat(`@qaa is my boyfriend!`);
				kub(`Really? `);
				txt(`@kub was surprised. @qaa was not exactly my type. `);
			}

			kat(`@qaa @kub, @kub @qaa. `);
			txt(`I introduced them briefly. @qaa frowned: `);
			qaa(`We know each other, he destroyed my science project when I was in 5th grade!`);
			kub(`Oh! You're that dork?! Sorry about that, I used to be an asshole. `);
			txt(`@kub boisterously punched his shoulder. `);
			kub(`How are you? Still being egghead? `);
			qaa(`...yeah...`);
			kub(`Good for you! `);

			if(mile.boobs){
				txt(`After that @kub did not pay him any further attention. `);
				kub(`What happened to your tits? `);
				kat(`I got them a bit improved. Do you like them? `);
				if(mile.slut > 8){ 
					kub(`Would you mind? `);
					kat(`Go ahead! `);
					txt(`I let him squeeze my breasts. `);
				}
			}
			
			txt(`He bought us shots and we chatted for a while. @qaa felt left out and was very happy when @kub finally left. `);
			qaa(`You're a friend with that douchebag? `);
			kat(`Yeah, we briefly dated. `);

			if(mile.slut > 11){ //TODO
				qaa(`What the hell! Is there anybody you haven't slept with? `);
				kat(`You were the last one. And I said dated not slept. But we definitely had sex many times. We're both young and clumsy but there are so many fond memories. `);
			}else{
				qaa(`With such jerk? `);
				kat(`What could I say? I was young and dumb and attracted to bad boys. `);
			}

			qaa(`Hmmmm. `);
			kat(`Are you going to dance? `);
			qaa(`No. `);
			kat(`Whatever. `);
			txt(`I finished his shot too and went to the dancefloor. `);
			link(`Go dance. `, 103);
		break;
		
	case 103:	
			txt(`I was enjoying the music and then somebody began to brazenly grope my ass. I assumed @qaa changed his mind and ground my bottom against him. However, then he whispered in my ear and I realized it was @kub. `);
			kub(`Oh, @kat! You're so hot! `);
			if(mile.a_date_react === 3){
				kat(`I'm here with my boyfriend!`);
				txt(`I protested but really did nothing to stop him and continued rubbing my butt against his crotch. `);
				kub(`Fuck that loser!`);
			}else{
				kat(`Sorry, but I'm here with @qaa!`);
				txt(`I protested but really did nothing to stop him and continued rubbing my butt against his crotch. `);
				kub(`Fuck that loser!`);
			}
			txt(`He roughly turned me around, grabbed me by the ass and drew me closer. Then he kissed me. I threw my arms around his neck and we were making out in the middle of the dance floor. `);
			kub(`Why did we even break up? `);
			kat(`Because you're an asshole!`);
			kub(`So you wouldn't want to get back together? Maybe I changed? `);
			kat(`Never.`);

			if(mile.slut > 10){
				kub(`That's a shame. I want to fuck your brains out so much!`);
				kat(`I didn't say we can't fuck! `);
				kub(`Really? `);
			}else{
				kub(`What about some casual sex? `);
			}

			con_link(mile.slut > 6, `low slut`, `Let's do it right now! @qaa doesn't have to know!`, 111, ()=> mile.slut++);
			con_link(mile.slut > 6, `low slut`, `Let's do it right now! But I have to tell @qaa first! `, 110, ()=> mile.slut++);
			link(`You should go with us and show @qaa how to properly fuck me!`, 200);
			link(`I'd love to get fucked by you but I can't cheat on @qaa. `, 114, ()=> mile.slut--);
		break;
		
	
		
	case 110:
			emo("imp");
			mile.a_forced_watch = 3;
			txt(`@qaa's eyes widened when he saw me and @kub holding hands. `);
			qaa(`@kat?!`);
			kat(`I just wanted to tell you we're going to fuck. `);
			qaa(`What?!? You're drunk! And you can't just...`);
			kat(`Drunk and horny! And I'm not asking for your fucking permission, I'm just telling you because I'm polite and nice! `);
			qaa(`But @kat!`);
			kub(`See ya later, loser. `);
			
			link(`Sex.`, 111);
			break;
			
	case 111:
			emo("ahegao");
			log_sex("sex", "kat", "kub");
			txt(`@kub grabbed me by my ass and lead me to the restroom. We ignored several other people, pissing or washing their hands, and locked ourselves in a cubicle. `);
			txt(`We were kissing and caressing each other. I raised my left knee, his right hand was squeezing my thigh, I ground my pelvis against him and could feel how hard he was. `);
			kub(`Fuck yeah! You little slut! `);

			if(wears.skirt){
				txt(`He rolled up my @lower${PC.panties ? " and I pushed aside my @panties. " : ". "}`);
			}else{
				txt(`I dropped down my @lower and ${PC.panties ? "@panties" : ""}, leaving them around my right ankle. `);
			}

			txt(`I lifted my knee again and he grabbed it and placed my leg on his shoulder. I lost my balance and leaned with my back against the wall of the cubicle. `);
			txt(`Then he finally roughly entered me. He was pounding me hard, pushing me back and the cabin was making squeaking noises. It was obviously not very sturdy and the tiny part of my brain not engulfed with the sexual pleasure was a bit worried whether it will survive our wild intercourse. `);
			txt(`I was loudly moaning, not caring who might hear us... well, I actually wanted to people hear how much fun I was having! It was awesome to be for once fucked by somebody who knew what he was doing. He climaxed inside me and then we continued making out but not for very long. It was not a very romantic place. We did our best to fix our appearances (although it probably was not enough, my makeup was a bit smeared and hair ruffled) and left. `);
			
			link(`You should definitely go with us and show @qaa how to properly fuck me!`, 200);
			if(mile.a_forced_watch === 3){
				link(`Go back to @qaa`, 112);
			}else{
				link(`Go back to @qaa and tell him what happened.` , 112, ()=> mile.a_forced_watch = 2);
				link(`Go back to @qaa and didn't tell him anything. `, 113, ()=> mile.a_forced_watch = 1);
			}
			break;
	
	case 112:
			mile.a_dom++;
			mile.a_cuck++;
			emo("horny");//emo??
			kat(`Hello!`);
			if(mile.a_forced_watch === 3){
				qaa(`Hello!`);
				txt(`I returned to @qaa, red, sweating and disheveled.`);
				kat(`Why the long face? `);
				qaa(`You just fucked another guy!!!`);
				kat(`So?! He was awesome! He gave me such a great orgasm! `);
				qaa(`Yeah... that's so fucking great...`);
			}else{
				txt(`I returned to @qaa, red, sweating and disheveled. I had no idea whether he saw me going to or leaving the restrooms with @kub. I decided to be brutally honest with him. `);
				kat(`I just fucked @kub at restrooms. `);
				qaa(`What the... are you joking?!?!`);
				kat(`No! `);
				qaa(`But.. but...`);
				kat(`You agreed we're not exclusive! `);
				qaa(`Yeah but...`);
			}
			kat(`Stop being such a jerk! You're a really shitty boyfriend! Instead of being happy for me, you're acting like I did something wrong! `);
			txt(`I berated him and he looked down. `);
			qaa(`I'm sorry @kat. I think we should go home. `);
			kat(`Great idea. Let's go back to your place. `);
			qaa(`No, I'm no longer in the mood to continue the date, sorry...`);

			//URAZIT
			next(`Well, okay. Good night. `);
			link(`Don't be stupid! We're going to your place to have sex! `, 115);
			//TODO JOIN
			break;
			
	case 113:
			emo("horny");//emo??
			kat(`Hello! `);
			txt(`@qaa inquiringly stared at me. He could clearly notice how disheveled I was. I had no idea whether he saw me going to or leaving the restrooms with @kub. He seemed unhappy but he didn't say anything. He might have a suspicion but he was too afraid to ask me and find out the truth. `);
			qaa(`Maybe we should go home. `);
			kat(`Great idea. Let's go back to your place. `);
			qaa(`No, I'm no longer in the mood to continue the date, sorry...`);

			next(`Well, okay. Good night. `);
			link(`Don't be stupid! We're going to your place to have sex! `, 115);
			break;
	
	case 114:
			mile.a_forced_watch = -1;
			emo("anoy");
			kat(`Hello! `);
			txt(`@qaa looked offended`);
			qaa(`Did you have fun? `);
			kat(`@kub wanted to fuck me. `);
			qaa(`WHAT?!?!`);
			kat(`But I said no. `);
			qaa(`Great!`);
			kat(`That's all? `);
			qaa(`Do you want a medal for not cheating?`);
			kat(`Yeah, for staying with you and not some hot hung stud! You don't appreciate me enough! `);
			qaa(`I'm sorry @kat. `);
			kat(`Let's go back to your place. I'm horny and need cock and you will have to do. `);
			link(`Next. `, 115);
			break;
			
	case 115:
			txt(`Wrong, choice, TODO`);
			//TODO ASAP SEX WITH QAA	
			next(`Done.  `);
			break;

	case 200:
			mile.a_dom++;
			mile.a_cuck++;
			emo("imp");
			txt(`@qaa frowned when I again returned with @kub. `);
			kat(`I had an idea. `);
			qaa(`What idea? `);
			kat(`Can @kub go with us to your place? You said your parents were gone? `);
			qaa(`But why? `);
			kat(`To fuck me, duh! `);
			qaa(`But... I thought we...`);
			kat(`You agreed we're not exclusive and I can have sex with whoever I want! `);
			qaa(`But... at my place...`);
			kat(`Don't be so fucking jealous! It would be a fun threesome! `);
			kub(`Wait! I won't do a threesome with another guy! That's gay! I'm not a homo!`);
			qaa(`In that case...`);
			kat(`In that case can @qaa at least watch? `);
			qaa(`But... `);
			kub(`I guess...`);
			kat(`Great, let's go! `);
			txt(`I was horny and did not want to let either of them to ruin my evening. `);	
			link(`@qaa's place. `, 201);
			break;		
	
	case 201:
			emo("horny");
			log_sex("sex", "kat", "kub");
			placetime(`@qaa's bedroom`);
			txt(`It was very cute to watch miserable jealous @qaa. We were making out right in front of him and I even let @kub to grope me while we were walking towards our block. @qaa very reluctantly unlocked the door and invited us to his room and bed. With resignation, he dropped to an armchair and just stared at us. `);
			txt(`We were kissing and undressing. @kub was a bit tense and nervous but I was very enjoying giving @qaa a naughty show. I was throwing my garments in his direction${PC.panties ? ", hoping my @panties" : "hoping they"} will land on his face but my comedic aim was not the best, especially after a few shots. @kub laid me down on the bed and I wrapped my long legs around him and pulled him closer. I closed eyes and enjoyed his energic thrusting, resonating throughout my whole body but then he suddenly halted. `);
			kub(`EWWW!`);
			kat(`What's going on? `);
			kub(`He's jerking!`);
			txt(`I looked at @qaa. With a guilty expression, he was clenching his exposed hard penis. `);
			
			link(`That's okay.`, 203);
			link(`That's not okay!`, 202);
			break;
			
	case 202:
			mile.a_dom++;
			mile.a_forced_watch_jerk = 1; 
			emo("ahegao");
			kat(`What the hell are you doing!`);
			txt(`I snapped. `);
			qaa(`I'm sorry! I thought that...`);
			kat(`That you can be such a disgusting creep just because we are fucking in your room?!`);
			qaa(`I'm sorry!`);
			kat(`You're so pathetic! Hide that little dicklet and put your hands on the armrests! If you even touch yourself, you'll fucking regret it! `);
			txt(`I threaten him and I meant it. He apologized again and did what I told him like a good boy. `);
			txt(`@kub began fucking me again but that horny dumbass completely interrupted our flow. Eventually, I pushed him over and mounted him. It was giving poor @qaa full frontal view on my nude lithe body bouncing up and down on top of some other guy's cock. His pants were bulging and his desperate humiliated expression was only multiplying the pleasure I felt. It felt so awesome to have the power to torment a helpless guy this way. I constantly maintained stern eye contact and the orgasm was just fucking incredible! `);
			link(`Next.`, 644);
			break;		
			
	case 203:	
			mile.a_forced_watch_jerk = 2;
			emo("ahegao");
			qaa(`I... I'm sorry!`);
			kat(`Don't worry! That's okay! If you're so pathetic that watching me being fucked by another guy makes you horny, you're free to masturbate! `);
			kub(`It's not okay! I don't want to be in one room with a masturbating guy! `);
			kat(`Come on! He's my boyfriend and we're at his place! He deserves that!`);
			kub(`It's so gay!`);
			kat(`Don't be such a fucking crybaby! `);
			txt(`I pushed @kub over on the bed and mounted him so he could only stare either on my jiggling @breasts or on the ceiling. It also gave poor @qaa full frontal view on my nude, lithe body bouncing up and down on top of some other guy's cock. His expression was desperate and humiliated but still he was not able to look away and with energic motion stroked his dick. I mischievously smiled at him, not hiding how much I was enjoying tormenting him and being fucked by a proper stud. It was awesome to have such power over him. His dicklet cummed and he closed his eyes, overwhelmed by the shame and regret. It made my own orgasm extra intense and delightful. `);
			
			link(`Next.`, 204);
			break;
			
	case 204:
			mile.a_love++;
			emo("smug");
			txt(`@kub quickly dressed and left. Both boys tried very hard to not look at each other. I, still leaking @kub's cum everywhere, went to @qaa and kissed him. `);
			kat(`Thanks! You're the best boyfriend ever!`);
			qaa(`... you're welcome! M... maybe now we could...`);
			kat(`Sorry! I'm no longer in the mood for sex! I'm completely spent. Have you seen how hard he made me cum!?!`);
			qaa(`Yeah... I don't think I'll ever be able to forget...`);
			kat(`Well... maybe we could cuddle for a while. That would be nice. `);
			qaa(`Great!`);
			kat(`Maybe you can also lick me clean? `);
			qaa(`Ewww! `);
			next();
			break;
	}
}



//AAA - UNI TOGHETER & FATHER
export const a_long_sex = (index) => {
	switch(index){
		default:
		case 101: 
			set.irl();
			placetime(`@qaa's place`);
			
			mile.aaa++;
			mile.a_love++;
			mile.a_long_sex = true;
			present([2,`@qaa's father`]);
			/*
			mile.sexA5 = true;
			mile.qaa++;
			mile.gf++;
			WARDROBE.showLess(-1,-3);
			*/
			
			if(mile.chastity){
				txt(`The wearing of the chastity belt was getting unbearable again but I knew who could help me! `);
			}else if(mile.slut > 10){
				txt(`I was in a horny mood so I decided to pay a visit to @qaa.`);
			}else{
				txt(`The playing of the virtual video game was making me crazy so I decided to take a break and visit @qaa.`);
			}
			qaa(`Hello, @kat! What a pleasant surprise! `);
			
			if(mile.a_anal){ 
				kat(`I hope you're ready because this babe needs a cock in her ass! `);
			}else{
				kat(`I hope you're in the mood for hot sex because I'm seriously horny and need to be ploughed hard! `);
			}
			
			txt(`I wanted to shock him but I did not expect his father would peek from the living room to see who arrived. `);

			if(mile.slut < 5){
				emo("shock");
				txt(`My face went completely red, I was never so embarrassed! `);
				kat(`H.. hello? `);
			}else if(mile.slut < 10){
				emo("shy");
				txt(`It was pretty embarrassing situation but I tried to stay cool. `);
				kat(`Hello? `);
			}else{
				emo("imp");
				txt(`I stayed completely nonchalant. It was very entertaining to watch his confusion. `);
				kat(`Hello! `);
			}
			npc(2,`...hello, @katalt?`);
			if(mile.slut > 10){
				qaa(`She came because of-`);
				kat(`-sex!`);
				txt(`I quickly admitted, trying to make the situation as awkward as possible. @npc_2 was stunned for a second but then nodded. `);
				npc(2,`Right. ...well, have fun... ...and don't forget to use protection!`);
			}else{
				qaa(`She came because of... tutoring.`);
				npc(2,`Right... tutoring... well, have fun ...and don't forget to use protection! `);
			}
			txt(`We went to his room and @qaa locked the door. `);

			if(mile.slut < 7){
				kat(`I'm so sorry! I didn't expect your father will be standing right next to the door! This was so embarrassing! `);
			}else{
				kat(`That was funny! I didn't expect your father will be standing right next to the door! Are you okay? You seem thunderstruck. `);
			}
			qaa(`Oh, yeah. Well, at least I kinda hope that seeing you craving my dick so badly will gain me a bit of respect. He thinks that spending all my free time in virtual reality is making me weak and unmanly.`);
			
			if(mile.a_tripoint === 3){
				kat(`I mean, he isn't wrong, is he? `);
				txt(`I teased him. `);
			}
			
			kat(`I'll moan and scream extra loud, so he could know he should be proud of you! `);
			qaa(`Please no! `);
			/*if(mile.masochist){
				kat(`You'll have to go gag me! `);
				txt(`@qaa chuckled. `);
			}
			*/
			link(`Sex.`, 102);
			break;
			
		case 102:
			log_sex("sex", "kat", "qaa");
			showLess(-1, 0);
			emo("bliss");
			if(mile.a_tripoint === 2){
				kat(`What do you want to do? `);
				qaa(`Lie on your back. I'm going to awe you with my pussy-licking skills.`);
			}else{
				qaa(`What do you want to do? `);
				kat(`We'll start with a quick evaluation of your pussy-licking skills. `);
				qaa(`Aye, ma'am!`);
			}

			txt(`I dropped down on his bed, he pulled down my @lower ${mile.chastity ? "(and the thrice-damnded chastity device)" : ""} and dived his head between my thighs. `);
			kat(`Oh yeah! `);
			txt(`@qaa  was very subtle, he was not rushing anywhere, his tongue was riding here and there, slowly exploring my lower region. For long he stayed far away from my precious bud, gradually teasing me and building the tension. `);
			qaa(`I love you taste so much! `);
			kat(`Oh, you little sweet-talker!`);
			qaa(`It's 100% true! `);
			kat(`Ahhhh! `);
			txt(`I panted when he finally venturesomely touched my clit. I ruffled his short hair and moaned:`);
			kat(`Oh yes! That's it! Yeah! Do that! `);
			txt(`He reached to grab my left tit but continued eating my pussy with no less devotion. My body convulsed when he brought me to the peak - and beyond. `);

			if(mile.a_tripoint === 1){
				qaa(`So did I amaze you with a mind-shattering orgasm? `);
				kat(`Consider me indeed awed! `);
			}else{
				qaa(`What grade I'll get? `);
				kat(`Hmmm... B+? `);
				qaa(`Seriously? I'm used to A's! And that seriously wasn't just B+ moaning!  `);
				kat(`I don't want you to lose the motivation to improve even more!`);
			}
			
			link(`More sex.`, 103);
			break; 
			
	case 103:
			emo("happy");
			showLess(0, 0);
			effigy.qaa.showLess(0, 0);
			txt(`@qaa moved next to me and we begin kissing. I stripped the rest of my clothes and he keenly played with my tits, he never seemed to be bored with them `);
			
			if(mile.a_tripoint === 1){
				qaa(`And now I'll fuck you hard! `);
				kat(`Really? I want to see that! `);
			}else{
				qaa(`And what would you like to do next, my princess? `);
				kat(`Now fuck me hard! `);
			}

			txt(`I slightly bent my knees, spread my legs wide and invited him between them. The tip of his hard cock brushed against my burning pussy. `);
			//if(mile.a_tripoint === 2){
				qaa(`Do you want my cock? `);
				if(mile.sub > 1){
					kat(`Oh yes! Yes, please! `);
				}else{
					kat(`Shut up and shove it in! `);
				}
			//}
			txt(`@qaa kissed me and then entered. `);
			
			if(mile.a_tripoint === 3){
				kat(`So... how old is your dad? `);
				qaa(`What?! Why?`);
				kat(`Nothing, I just... he seems mature and rather handsome.`);
				qaa(`What!?`);
				kat(`I think I might want to fuck him. `);
				qaa(`Why are you doing this to me, @kat?!?`);
				txt(`I pulled him closer and kissed him. I loved tormenting him.`);
			}
			
			txt(`He was slowly thrusting while adoringly staring down at me. His gentle fingers attentive caressed my cheeks and hair. `);
			qaa(`You're so beautiful, @katalt. `);
			if(mile.sub < 1){
				kat(`Duh, obviously!`);
				txt(`I was sassy but still, I loved to hear that. `);
			}else if(mile.sub > 9){
				kat(`T.. thank you! `);
				txt(`I said softly. It felt so nice to be appreciated and cherished. `);
			}else{
				txt(`I smiled at him. Being so appreciated and cherished made me feel great. `);
			}
			if(mile.slut > 4){
				kat(`You cock feels so great inside my pussy! `);
			}
			
			txt(`We were making love, tenderly and leisurely, connected, enjoying each other bodies. `);
			qaa(`Oh yes! Oh yes! `);
			kat(`Don't hold back!`);
			txt(`We both @qaa climaxed at the same time. `);
			link(`Cuddling.`, 104);
			break;
			
	case 104: 
			//TODO mile.local
			emo("focus");
			txt(`I curled on the bed and @qaa was spooning me. `);
			qaa(`It was fun, wasn't it? `);
			kat(`Yeah! `);
			
			{
				const uni = (()=>{
					switch(SETTING.local){
						case `slovak`: return `Czechia`;
						case `czech`: return `Prague`;
						case `english`: return `London`;
						case `polish`: return `Kraków`;
						case `german`: return `Germany`;
						default: return `England`;
					}
				})();
				qaa(`I was thinking about going to uni in ${uni}. `);
			}
			
			kat(`Cool? Why are you bringing that up? `);
			qaa(`What about you? There are many great universities there. `);
			kat(`Dunno. `);
			qaa(`I was thinking, maybe we could find a place there together? `);
			
			link(`That's an awesome idea!`, 105, ()=> mile.a_uni = 2);
			link(`That's an interesting idea.`, 105, ()=> mile.a_uni = 1);
			link(`That's a horrible idea! `, 105, ()=> mile.a_uni = -1)
			link(`I don't know. `, 105, ()=> mile.a_uni = 1)
			break;
			
	case 105:
			emo("horny");
			if(mile.a_uni === -1){
				mile.a_love--;
				if(mile.a_tripoint === 3 || mile.sub < -1){
					kat(`That's the dumbest idea I ever heard! `);
				}else{
					kat(`I don't think that's a good idea. `);
				}
				qaa(`Sorry! It was just a silly suggestion! `);
			}else if(mile.a_uni === 2){
				mile.a_love++;
				if(mile.a_tripoint === 3 || mile.sub < -4){
					kat(`One place? With you paying the rent? That sounds like amazing idea!`);
				}else{
					kat(`That sounds like an awesome idea! It might be great to study abroad! And we could share the rent!`);
				}
				qaa(`Really? You'd want to live with me? `);
				kat(`Sure!`);
			}else{
				kat(`Hmmmm. `);
				qaa(`What? `);
				kat(`I don't want to offend you but I'm not sure if we should live together! You're going too fast! `);
				qaa(`I don't want to press you! It's just a silly suggestion. You can think about it. `);
			}

			txt(`We were cuddling and soon I could feel something poking me from behind.`);
			kat(`Again? `);
			qaa(`I can't help it! Your perfect body is making me so horny! `);
			kat(`Cool! `);
			if(mile.into_anal > 1 && mile.a_anal){ //TODO 2?
				log_sex("anal", "kat", "qaa");
				txt(`His hard dick was forcing it's way between me asscheeks.`);
				kat(`Oh! You want to be naughty? `);
				qaa(`Don't say you don't like it!`);
				txt(`He slowly pushed his cock in my butt. `);
			}else{
				log_sex("sex", "kat", "qaa");
				txt(`His hand was lightly rubbing my pussy and then he pushed his cock in. `);
			}
			txt(`@qaa was insatiably fucking me from behind while squeezing and kneading my @breasts. `);
			kat(`Oh yes! `);
			txt(`I feel his hot breath, he was passionately kissing my nape and biting my ear. It was lovely. `);
			txt(`He did not stop embracing me and holding me tightly even after his orgasm. `);
			
			//link_skip_back(`Done. `, ()=>WARDROBE.showAll() );
			next();
			break;
	}
}


/*
date - hanging out 
heavy petting?

gathering good/bad
father - good/bad
something at school
some bdsm shit (related to qbb?)

threesome
anal 
cuck
publick declaration 


*/


//FAMILY EVENT / PROJECT 
export const a_project = (index)=> {
	switch(index){
		case 101: 
		default:
			set.irl();
			emo("shy"); //emo??
			mile.a_project = 1;
			placetime(`physics classroom`);
			present(
				[1, `Teacher`], 
			);
			npc(1, `...and that's everything for today. One more thing - the deadline for your projects is the next week. So if you haven't even started yet, you're in trouble.`);
			txt(`Oh no! I completely forgot about the projects! We were working in groups and @qaa eagerly agreed to join me. He was good at Physics and I assumed he will do the bulk of the work. But that was before he started with this stupid blackmailing scheme and before I told him I am not (and will never be) interested in him. I was concerned that our current animosity might very negatively affect his enthusiasm to finish the project for me.`);
			kat(`Eh, hello`);
			qaa(`@katalt?`);
			txt(`He adressed me coldly.`);
			kat(`How's our project doing?`);
			qaa(`You should know. It's your project too!`);
			kat(`Come on! We're in this together! I will help you, we finish the project and then we can go back to hating each other!`);
			qaa(`I had a different idea. Why don't we just blow it off?`);
			kat(`We can't! Our final grades will be based on the project!`);
			qaa(`My other grades are perfect. I'll pass the class even if I won't submit the project.`);
			if(ext.background === 3){
				kat(`Mine are not so good.`);
			}else{
				kat(`Mine are awful!`);
			}
			qaa(`That's a shame!`);
			txt(`He smugly smirked. I was perplexed:`);
			kat(`You can't be so petty! You wouldn't tank your own grades only to see me fail!`);
			if(ext.background === 3){
				qaa(`That's where are you wrong, dear @kat. Enjoy failing the class!`);
			}else{
				qaa(`You once told me I shouldn't study so hard and that grades don't matter so much as long as you're passing. So I just follow your advice.`);
			}
			
			link(`Please! I'll do anything! `, 102, ()=> mile.sub++);
			link(`Whatever, I don't care! `, 120);
			link(`Fuck you! I promise you'll regret this! `, 130, ()=> mile.sub--);
			break;

		case 102:
			emo("imp");
			qaa(`Any by <i>anything</i> you really mean <i>anything?</i>`);
			txt(`I knew how easy was to manipulate with a horny guy like him.`);
			kat(`Well... anything reasonable.`);
			qaa(`So not really <i>anything</i>.`);
			
			link(`Nothing too perverted!`, 103);
			con_link(mile.slut < 5, `slut`, `I won't fuck you!!`, 103);
			con_link(mile.slut > 4, `prude`, `Should I suck your cock or what?!?`, 110);
			link(`What the hell do you want!?`, 111);
			
			break;
			
		case 103:
			emo("shock");
			qaa(`You're not trying to convince me very hard.`);
			kat(`Are you crazy?!? If you think I'm going to sleep with you because of a stupid project, you're insane!`);
			qaa(`And now you're even being rude!`);
			kat(`I won't have sex with you!`);
			qaa(`Well, too bad!`);
			
			next(`I won't do it!`);
			link(`Okay, I'll do it. `, 104);
			next(`You know what? Go fuck yourself!`);
			next(`I would rather fail all my grades than had sex with you!`);
			break;
			
		case 104:
			mile.slut++;
			qaa(`You'll do what?`);
			kat(`Have sex with you.`);
			txt(`I reluctantly shrugged.`);
			qaa(`For the passing grade?`);
			kat(`...yes.`);
			qaa(`Thanks for confirming you're a shameless whore. But thanks, I'm not interested. I think seeing you fail will be far more satisfying!`);
			txt(`I was stunned.`);
			qaa(`Enjoy failing physics, @katalt!`);
			txt(`He mocked me.`);
			
			link(`Whatever, I don't care! `, 120);
			link(`Fuck you! I promise you'll regret this! `, 130);
			break;
			
		case 110:
			if(mile.slut < 8) mile.slut++;
			qaa(`Oh, @kat! Always so ready to suck cocks!`);
			kat(`You don't want that?`);
			qaa(`No. But I think it's cute that sucking cocks is the only think your small horny brain is able to come up with.`);
			kat(`So what the hell do you want?`);
			//break;
		
		case 111:
			emo("shock");
			qaa(`The thing I want the most...`);
			kat(`Yeah?`);
			qaa(`Is to see you fail physics.`);
			kat(`Come on, are you serious?!`);
			qaa(`Deadly serious. There's nothing you can offer me that would be more satisfying than seeing you fail.`);
			
			link(`Whatever, I don't care! `, 120);
			link(`Fuck you! I promise you'll regret this! `, 130);
			break;
			
			
		case 120:
			emo("anoy");
			qaa(`Oh, I think you do!`);
			kat(`It's just a stupid grade!`);
			qaa(`Sure it is!`);
			txt(`He smugly laughed. Telling me <i>no</i> when I desperatelly needed him was probably making him hard.`);
			next();
			break;
			
		case 130:
			emo("angry");
			qaa(`I don't think I will.`);
			kat(`I'm serious. You won't see it coming but I'll make you suffer!`);
			qaa(`Sure you will!`);
			txt(`He smugly laughed,`);
			next();
			break;
			
	}
}

export const a_family_project = (index)=> {
	switch(index){
		case 101:
		default:
			set.irl();
			mile.aaa++;
			placetime(`at home`);
			emo("suspect"); 
			txt(`I had to do something with the project. @qaa decided to be vengeful, not carrying it will hurt him too. An obvious (even though embarrassing) choice would be to meet with the teacher and explain to him my problem. I intended to do it right tomorrow.`);
			txt(`But in the afternoon, @qaa visited me.`);
			qaa(`Hello.`);
			kat(`Hey.`);
			qaa(`I have an offer.`);
			kat(`I'm listening.`);
			qaa(`I have a deal. If you do everything I ask you, following all the rules, I submit the project and we both get a good grade.`);
			kat(`I won't be your sex slave!`);
			qaa(`Nothing sexual! You'll just accompany me for a few hours this weekend.`);
			kat(`Where's the catch?`);
			qaa(`Nowhere. Well, if anybody asks you're my girlfriend.`);
			if(mile.sub < -2){
				kat(`Hahaha! You're so pathetic!`);
				qaa(`Okay. Bye.`);
				kat(`Wait! Where?`);
			}else{
				kat(`Where?`);
			}
			qaa(`At my uncle's 60th birthday. Nobody there will know you.`);
			
			link(`Okay. I'll do it. `, 103);
			link(`I have better things to do. `, 102);
			break;
			
		case 102:
			mile.a_project = -1;
			emo("anoy"); 
			kat(`Do you really think I have no better things to do? Or that I'm going to help you pretend you're not a desperate virgin unable to even ask a girl out without blackmailing her first? No thanks!`);
			qaa(`Enjoy failing the class!`);
			next();
			break;
		
		case 103:		
			mile.a_project = 2;
			mile.a_family = 1;
			qaa(`If you try to embarrass or disrespect me in any way, the deal is off!`);
			kat(`Fine!`);
			qaa(`And I don't want you to dress like a whore!`);
			kat(`You're making me dress like a whore! Fine, should I wear a niqab or what?!`);
			qaa(`No. I want my cousins to see you're hot. Just don't overdo it!`);
			next();
	}
}
			
			
			
export const a_family_initiation = (index)=> {
	switch(index){
		case 101:
		default:
			set.irl();
			mile.aaa++;
			placetime(`@qaa's place`);
			{
				const clad = (()=>{
					if(wears.stockings) return "stockings-clad "; //??
					if(wears.pantyhose) return "pantyhose-clad ";
					return "";
				})();
				txt(`We were watching trailers on Netflix, unable to commit and decide for one show. @qaa was lying in my lap, his hand fondling my ${clad} knee.`);
			}
			qaa(`So... you're now my girlfriend, right?`);
			//TODO - READJUST NUMBER WHEN HEAVY PETTING 
			if(mile.a_love > 4 || mile.a_date_react === 3) link(`Yes, I'm your girlfriend. `, 102, ()=> counter.temp = 2);
			if(mile.a_love > 3 || mile.a_date_react === 3) link(`Yeah! `, 102, ()=> counter.temp = 2);
			link(`Well... yes. `, 102, ()=> counter.temp = 2);
			link(`Hmmmm. `, 102, ()=> counter.temp = 1);
			link(`Well, kinda. `, 102, ()=> counter.temp = 1);
			if(mile.a_cuck || mile.a_dom > 0) link(`No!`, 102, ()=> counter.temp = 0);
			break;
			
		case 102:
			emo("focus"); //emo??
			if(counter.temp === 2){
				mile.a_love++;
				mile.a_girlfriend = true;
			}
			
			if(!counter.temp){
				kat(`No, I'm not your girlfriend!`);
				qaa(`But we're sleeping together!`);
				if(mile.slut > 6){
					kat(`So what? I don't date everybody I have sex with.`);
				}else{
					kat(`Are you saying we should stop?`);
				}
			}

			if(counter.temp < 2){
				qaa(`But in some sense, you're something almost like my girlfriend?`);
				kat(`If that makes you feel better, maybe?`);
			}

			qaa(`I need a big favor. My uncle going to celebrate his 60th birthday.`);
			if(mile.slut > 4){
				kat(`And you want me to give him a lapdance, right?`);
			}else{
				kat(`And?`);
			}
			
			qaa(`Well, and our family is invited and I wondered if you want to go with me.`);
			kat(`No, thanks.`);
			qaa(`Come on, @kat!`);
			kat(`I have an alternative suggestion - what if we exploited the fact your parent will be away and had sex everywhere?`);
			qaa(`Please! Being seen with somebody as beautiful and charming and graceful as you would greatly raise my status! And I would owe you! Like if you wanted to take me at your family gathering...`);
			kat(`No, thanks. ${mile.slut > 5 ? "Imagine, in the bathroom, in the hallway, in the kitchen... " : ""};`);
			qaa(`Please, @katalt!`);
			
			//TODO TIME?
			link(`Okay. `, 110);
			link(`Fine!`, 110);
			link(`No.`, 105);
			break;

		case 105:
			mile.a_family = -1;
			emo("anoy"); 
			txt(`I don't have time! I would go with you if I didn't  have to finish a stupid videogame!`);
			qaa(`Oh, sorry.`);
			next();
			break;
			
		case 110:
			mile.a_family = 1;
			qaa(`Awesome! You're the greatest person in recorded history!`);
			kat(`I know.`);
			qaa(`Would you mind wearing something a bit... decent?`);
			kat(`What do you mean?!`);
			qaa(`You know... less trashy than you usually wear.`);
			if(mile.slut > 10){
				kat(`But I like to wear sexy clothes! And you three perverts love it too, don't you!`);
			}else{
				kat(`Because you're fucking making me to! I don't dress up like a whore because I enjoy it!`);
			}
			qaa(`Well... yeah... but this is an official event.`);
			kat(`I can wear a niqab? If that makes you happier?`);
			qaa(`No! I want you to dress sexy! I want my cousins to see how hot you are! They are awful and always bully me for being a nerd and having no girlfriend.`);
			kat(`Bulling how?`);
			qaa(`They're always like: <i>hey! When will you show us your girlfriend!</i>.`);
			kat(`Terrible. So I should dress sexy.`);
			qaa(`But not too much! I don't want them to think I hired a hooker! ...no offense!`);
			kat(`Wonderful!`);
			qaa(`This weekend! Don't forget about it!`);
			next();
	}
}
	

export const a_family_action = (index)=> {
	switch(index){
		default:
		case 101:
			set.irl();
			mile.a_family = 2;
			present(
				[1, `Uncle Anton`], //uncle
				[2, `Fero`], //sleazy cousin
				[3, `Uncle Rudolf`], //recist uncle
			);
			placetime(`my room`);
			txt(`My phone buzzed, @qaa texted me:`);
			qaa(`I hope you didn't forget what you promised to me!`);
			kat(`Of course not!`);
			qaa(`Awesome!`);
			kat(`What I promised to you?`);
			qaa(`You dont remember? My uncle's birthday!`);
			kat(`I do remember that.`);
			kat(`The next week?`);
			qaa(`TODAY!`);
			kat(`👌`);
			link(`Get dressed.`, 102);
			break;
			
		case 102:	
			DISPLAY.dress_up(110);
			//link(`Get dressed.`, 110);
			break;
			
		case 110:
			{
				const appearance = sluttiness();
				mile.a_family_slut = appearance.value;
				
				//not enough slut
				if(mile.a_family_slut < 1){
					emo("focus");
					kat(`Hello!`);
					qaa(`Hello...`);
					txt(`He slowly checked me, from my feet to the top of my head.`);
					qaa(`...what did I tell you! I tould you to wear something sexy!`);
					if(mile.sub > 7){
						kat(`I'm sorry.`);
						qaa(`Hmmmmm.... it's going to be fine. You look okay.`);
					}else if(mile.a_burned){
						kat(`I think it's sexy enough!`);
						qaa(`It isn't! Hmmm... fine! Whatever.`);
					}else{
						kat(`Fine!`);
						qaa(`Where are you going?`);
						kat(`I'm leaving when you don't want me to go with you.`);
						qaa(`I didn't say that! Please stay! You look gorgeous!`);
					}
				
				// too much slut
				}else if(mile.a_family_slut > 2){ //TODO!!
					const slut = (()=>{
						if(appearance.details.genitals){
							return "Are you even wearing panties!?! ";
						}else if(appearance.details.panties){
							return "I can even see your panties! ";
						}else if(appearance.details.nipples){
							return "Everybody can see your nipples! ";
						}else{
							return "";
						}
					})();
					
					kat(`Hello!`);
					qaa(`You look like a slut!`);
					kat(`Nice to see you too!`);
					if(mile.a_burned){
						qaa(`What did I tell you? Dress properly! ${slut} If you're going to intentionally sabotage the whole event, we should quit right now! Enjoy your failing grade!`);
						if(mile.sub > 6){
							emo("shy"); 
							txt(`Damn, he looked really angry.`);
							kat(`No, please! You know I'm just a hapless attention whore. I'm not intentionally trying to sabotage anything!`);
							qaa(`...`);
							kat(`...`);
							qaa(`...`);
							kat(`...`);
							qaa(`Hmmm... fine!`);
						}else{
							emo("angry"); 
							kat(`Okay. Let's quit it. Did you already tell everybody you're bringing your hot girlfriend? How embarrassing.`);
							qaa(`...`);
							kat(`...`);
							qaa(`...`);
							kat(`...`);
							qaa(`Fine! Whatever!`);
						}	
					}else{
						emo("focus");
						qaa(`I'm sorry, @kat, but what did I tell you! This is too much for a family even! ${slut}`);
						kat(`I thought you liked when I dress up like a slut!`);
						qaa(`Usually yes but not right now. Fine, whatever, this will have to do.`);
					}
				
				//just right
				}else{
					emo("imp"); 
					kat(`Hello!`);
					qaa(`Hello, @kat! You look absolutely gorgeous!`);
					kat(`Of course. I wouldn't want to embarrass you in front of your whole family, would I?`);
					qaa(`I don't know. Would you?`);
					kat(`Strongly depends on my mood.`);
					if(mile.a_burned){
						qaa(`Don't you fucking dare.`);
					}else{
						qaa(`Please don't!`);
					}
					kat(`Don't worry.`);
				}
			}
			
			if(mile.a_burned){
				qaa(`Once again let's go through the rules.`);
				kat(`I do remember them.`);
				qaa(`I don't want you to make excuses you <i>forgot</i> like you always do!`);
				kat(`Okay.`);
				qaa(`You won't make any scene and won't try to embarrass me or hurt me in any other way. You will smile and talk only when somebody asks you. If they ask you about me, you will say we're dating and praise me reasonably. You stay away from my sleazy cousin @npc_2. And you don't initiate or partake in any discussion about politics, especially not with @npc_3! If you can follow those extremely simple rules, I will make sure we're both getting 1s.`);
				kat(`Don't worry, I won't let us down!`);
			}else{
				qaa(`Okay. Just be nice. And don't get drunk. And stay away from my sleazy cousin @npc_2. And don't discuss politics, especially not with @npc_3!`);
				kat(`Fine, don't be afraid, I'm perfect at handling social events.`);
			}
			
//			if(mile.a_family_slut > 1){
//				We were riding with his parents. His mother clearly did not liked my outfit but when she subtly tried to criticize it, his father (after thoroughly
				
			link(`The event. `, 120);
			break;
			
	case 120: 
			placetime(`classy but affordable restaurant`);
			emo("relax");
			txt(`The event took place in a private room in a restaurant. There were already like twenty people. Proud @qaa introduced me to everybody as his girlfriend and I just smiled and nodded. I think I made a good impression even on the honored person when we wished him all best:`);
			npc(1, `Nice to meet you, @katalt. I had no idea @qaa has such a lovely girlfriend!`);
			
			link(`Thank you! `, 121);
			if(mile.a_burned || mile.slut > 7)	link(`Friends with benefits might be a more accurate term, if you know what I mean. `, 122);
			if(mile.a_burned || mile.a_dom > 1) link(`Thank you! He's absolutely amazing and I hope he proposes me very soon because I want to marry him and have three children with him! `, 123);
			
			if( (mile.a_burned && mile.slut > 2) || mile.cuck > 0) link(`And I had no idea he has such handsome uncle! `, 124);
			break;
			
	case 121:
			npc(1, `You're welcome. I hope you're happy together!`);
			kat(`We are, sir.`);
			txt(`I nodded and @qaa gleamed.`);
			link(`Continue. `, 130);
			break;
			
	case 122: 
			emo("imp");
			npc(1, `I'm not sure what you mean, sweetie. But I hope you're happy together!`);
			txt(`We moved on and @qaa was not very happy when he turned to me and whispering he scolded me:`);
			qaa(`What the hell was that?!`);
			kat(`It was just banter!`);
			qaa(`I don't want any more banter! Just behave, please!`);
			link(`Fine! `, 130);
			break;
	
	case 123: 
			emo("imp");
			npc(1, `I'm sure he will, won't you, @qaa?`);
			qaa(`What?! Right, of course!`);
			if(mile.a_burned){
				txt(`We moved on and @qaa was not very happy when he turned to me and whispering he scolded me:`);
				qaa(`What the hell was that?!`);
				kat(`Exactly what you wanted? I tried to be nice and elevate you!`);
				qaa(`I know you! That's why I specifically asked you to praise me just to a reasonable extent. Please no more sarcasm!`);
			}else{
				txt(`We moved on and @qaa was not satisfied when he turned to me and whispered:`);
				qaa(`What the hell was that?!`);
				kat(`Exactly what you wanted? I tried to be nice and elevate you!`);
				qaa(`Don't you think you went a bit too far? Sure, I would love to have three kids with you at some point, but it might be a bit too soon for discussing that. `);
			}
			link(`Fine! `, 130);
			break;
			
	case 124:
			emo("smug");
			npc(1, `Hahaha! Don't tease me, girl! If I was thirty years younger...`);
			kat(`Come on! You're not that old! And many girls of my age prefer more experienced, mature men!`);
			npc(1, `Hahaha! Thank you for saying that!`);
			txt(`He laughed but @qaa was not very happy when we moved on.`);
			qaa(`What the hell was that! Can't you stop flirting with every guy at least for five minutes?!`);
			if(mile.a_cuck > 0){
				kat(`You haven't told me he's such a handsome silver fox! Did he ever mention having a thing for schoolgirls?`);
			}else{
				kat(`Calm down, it was just banter.`);
			}
			qaa(`Please behave!`);
			link(`Fine! `, 130);
			break;


			
	case 130: 
			emo("relax");
			txt(`We met several other people, I did not even bother to remember their names. Eventually, @qaa's father waved at him.`);
			qaa(`I need to go to help him to bring things from the car.`);
			kat(`Fine.`);
			qaa(`Stay here and don't talk with anybody. I'll be right back!`);
			kat(`Fine!`);
			link(`Go to look for trouble. `, 200);
			break;
			
			
	case 200: 
		txt(`I looked around and I ran into a girl, roughly of my age.`);
		bar(`Hey!`);
		kat(`Hello!`);
		bar(`Are you a new @npc_2's broad??`);
		kat(`No. I'm here with @qaa. Why?`);
		bar(`You seem like @npc_2's type. Which @qaa?`);
		kat(`Are there more than one?`);
		bar(`No, but I can't believe you're here with that dork! Are you like dating or what.`);
		kat(`Yeah.`);
		bar(`Shut up! Be serious!`);

		link(`We are seriously dating! `, 201);
		link(`He hired me.`, 202);
		con_link(mile.slut > 3, `low slut`, `I'm a whore. `, 203);
		break
		
	case 201:
		mile.a_family_cousin = 1;
		bar(`What the hell! Are you serious?!`);
		kat(`I'm always serious.`);
		bar(`I can't even. But I won't judge you. Everybody has weird fetishes.`);
		kat(`What weird fetishes do you have?`);
		bar(`Recently mostly pissplay and bukkake.`);
		if(mile.slut > 6){
			kat(`That sounds fun!`);
		}else{
			kat(`Oh! ...okay.`);
		}
		bar(`I'm @bar, his cousin.`);
		txt(`I took her hand.`);
		kat(`I'm @katalt.`);
		bar(`Despite your questionable taste, you seem like the only normal person here! Do you want to get high?`);

		link(`Sure! `, 210, ()=> mile.a_family_high = true);
		link(`No, thanks!`, 210, ()=> mile.a_family_high = false);
		link(`Damn yeah, I do!`, 210, ()=> mile.a_family_high = true);
		break;
		
	case 202:
		emo("imp");
		mile.a_family_cousin = 2;
		kat(`I'm just his classmate and neighbor. He paid me fifty ees to pretend to be his girlfriend and don't tell anybody.`);
		bar(`That seems more like it! Fifty?! You must be really broke when you agreed!`);	
		if(mile.a_burned){
			kat(`He was begging me and I felt so sorry for him.`);		
			bar(`I can imagine that.`);
			kat(`No, you can't. He was kneeling in front of me, literally crying and desperately begging: Please @kat! I will do anything! I don't want all my relatives to know how huge pathetic virgin I am and all my cousins laughing at me!`);
		}else{
			kat(`He was begging me - nearly crying - and I felt so sorry for him.`);
		}
		bar(`You seem cool. I'm @bar, his cousin.`);
		txt(`I took her hand.`);
		kat(`I'm @katalt.`);
		bar(`You're like the only normal person here! Do you want to get high?`);

		link(`Sure! `, 210, ()=> mile.a_family_high = true);
		link(`No, thanks!`, 210, ()=> mile.a_family_high = false);
		link(`Damn yeah, I do!`, 210, ()=> mile.a_family_high = true);
		break;

	case 203:
		mile.a_family_cousin = 3;
		emo("imp");
		kat(`I'm a whore. He just hired me to pretend to be his girlfriend in front of his relatives.`);
		bar(`Lol. I can't believe he's that desperate!`);
		kat(`Yeah, pretty pathetic!`);
		if(mile.a_family_slut > 2){
			bar(`I think I suspected that, you kinda do look like a whore.`);
			kat(`Thanks?`);
		}
		bar(`I'm @bar, by the way, his cousin.`);
		txt(`I took her hand.`);
		kat(`I'm @katalt.`);
		bar(`You seem pretty cool! Do you want to get high?`);

		link(`Sure! `, 210, ()=> mile.a_family_high = true);
		link(`No, thanks!`, 210, ()=> mile.a_family_high = false);
		link(`Damn yeah, I do!`, 210, ()=> mile.a_family_high = true);
		break;
		
	case 210:
		emo("relax");
		if(mile.a_family_high){
			txt(`We sneaked outside and hidden behing hedges shared a joint.`);	
		}else{
			txt(`I refused her generous offer but still decide to follow her outside when we chatted hidden behind hedges.`);
		}
		
		if(mile.a_family_cousin === 1){
			bar(`Why?`);
			kat(`Why what?`);
			bar(`Why are you dating my lame cousin?`);
			
			if(mile.a_love > 5) link(`I love him. `, 211, ()=> counter.temp = true);
			link(`He's a pretty sweet guy. `, 211, ()=> counter.temp = false);
			if(mile.slut > 4) link(`Biggest dick you ever seen! `, 212);
		
		}else if(mile.a_family_cousin === 3){
			bar(`So... what is it like to be a whore?`);
			kat(`Awesome. I fuck people and they pay me and so on.`);
			bar(`That's so fucking cool!`);
			kat(`Yeah, it is!`);
			bar(`And @qaa hired you just to be his fake girlfriend or even for something more?`);
			
			link(`Definitely just fake girlfriend! `, 230, ()=> mile.a_family_cousin_whore = 5);
			link(`Maybe even something more. `, 230, ()=> mile.a_family_cousin_whore = 6);
			
		}else{
			bar(`So... did you hire you just to pretend to be his girlfriend in front of his family... or to provide the <i>full experience</i>?`);
			if(mile.a_family_high){
				kat(`Hahaha. You bitch, I'm not a whore!`);
				bar(`Hahaha!`);
				kat(`Hahaha!`);
				bar(`Hahaha. Still, would you fuck him if he paid you enough?`);
			}else{
				kat(`I'm not a whore. And he definitely wouldn't be able to afford me!`);
				bar(`Which one is true - you're not whore or he wouldn't be able to afford you?`);
				kat(`Shut up!`);
				bar(`Still, would you if he paid enough?`);
			}
			
			con_link(mile.slut > 8, `low slut`, `Yeah, of course! `, 220, ()=> mile.a_family_cousin_whore = 4);
			con_link(mile.slut > 3, `low slut`, `Yes. `, 220, ()=> mile.a_family_cousin_whore = 3);
			link(`Maybe. `, 220, ()=> mile.a_family_cousin_whore = 2);
			link(`No! `, 220, ()=> mile.a_family_cousin_whore = 1);
		}
		break;

	case 211: 
		if(counter.temp){
			kat(`Dunno. I kinda love him.`);
			bar(`Awww, you're a fucking romantic!`);
			if(mile.a_family_high){
				kat(`Damn, yeah, I fucking am!`);
			}else{
				kat(`Yeah, I guess.`);
			}
			bar(`But I guess being irrationally in love is the only valid excuse to date such a dumbass.`);
		}else{
			kat(`He's a pretty sweet guy, smart, funny, not another stupid jock!`);
			bar(`Shut up! I can't believe any girl would willingly spend any time with him!`);
		}
		if(mile.a_burned){
			kat(`I felt so awful. @bar was such a nice person and I was telling her such horrible lies.`);
		}
		if(mile.a_family_high){
			txt(`@bar handed me the joint.`);
			bar(`Maybe I should be jealous. I was his first when we were kids.`);
			kat(`What the fuck!`);
			txt(`I began laughing.`);
			bar(`Hah! I mean, not fucking! I made him make out with me and grope my boobs.`);
		}else{
			txt(`@bar again offered me the joint but I handwaved her offer.`);
			bar(`Maybe I should be jealous. I was his first when we were kids. I mean, not fucking! I made him make out with me and grope my boobs.`);
		}
		
		if(mile.a_burned){
			link(`Wait, you're the Cousin!? `, 280);
		}else if(mile.a_family_high){
			link(`You should definitely fuck! `, 290);
		}else{
			link(`I don't think you would have to force him now!`, 290);
		}
		break;
		
		
	case 212: 
		emo("smug");
		bar(`Shut up! You're bullshitting me!`);
		if(mile.a_burned || mile.a_cuck > 0){
			kat(`You got me. I was joking. Actually, he isn't very big. He's rather average. Well, below average. One might even say small.  Okay, honestly, the opposite is true. He has the tiniest dick of all dicks I have ever seen!`);
		}else{
			kat(`Maybe I am, maybe I'm not!`);
		}
		bar(`Damn! Now you made me curious!`);
		kat(`Just ask him to show you!`);
		bar(`Hahahaha!`);
		if(mile.a_family_high){
			kat(`Hahaha!`);
			bar(`Hahaha!`);
			txt(`I inhaled and gave the joint back to her.`);
		}else{
			txt(`She shook her head and then deeply inhale.`);
		}
		bar(`We were actually experimenting when we were younger.`);
		kat(`Experimenting!?!`);
		bar(`Nothing serious! I just made him kiss me and grope my tits when we were younger. I never thought about checking his dick.`);
		
		if(mile.a_burned){
			link(`Wait, you're the Cousin!? `, 280);
		}else if(mile.a_family_high){
			link(`You should definitely fuck! `, 290);
		}else{
			link(`I don't think you would have to force him now!`, 290);
		}
		break;
		
		
	case 220:
		emo("happy");
		if(mile.a_family_cousin_whore >= 3){
			mile.slut++;
			kat(`I mean, money is money, right!`);
			bar(`You're such a whore!`);
			kat(`Hey! What about you? Would you fuck him for money?`);
			bar(`I almost did it for free.`);
			kat(`Shut up!`);
		}else{
			bar(`Damn, you're such a prude! I have a more sexual experience with @qaa than his fake girlfriend!`);
			kat(`Shut up?`);
		}
		bar(`I'm just joking. I made him kiss me and grope my tits when we were younger. You should've seen his face!`);
		if(mile.a_family_high){
			kat(`Hahaha!`);
			bar(`Hahaha.`);
		}
		if(mile.a_burned){
			link(`Wait, you're the Cousin!? `, 280);
		}else if(mile.a_family_high){
			link(`You should definitely fuck! `, 290);
		}else{
			link(`I don't think you would have to force him now!`, 290);
		}
		break;
		
	case 230: 
		emo("smug");
		if(mile.a_family_cousin_whore === 5){
			kat(`No! He could not afford that!`);
			bar(`Hahaha!`);
			if(mile.a_burned){
				kat(`But he tried, desperately begging me on his knee, literally crying: please @kat! I already paid you so much! I'm a desperate virgin and I'd do anything to at least give me a handjob! Please! I'll worship you and forever be your sex slave! Can you believe that shit!?!`);
				bar(`Shut up!`);
				kat(`Yeah, that's how it happened. More or less.`);
				bar(`Hahaha, you're such a liar!`);	
			}else{
				kat(`He begged me to at least blow him for free but a girl like me has to have rules.`);
				bar(`Hahaha!`);		
			}
		}else{
			kat(`He wouldn't be able to afford me! But he's such a pathetic dork, maybe I'll blow him as a freebie because I feel sorry for him.`);
			bar(`Hahaha, you're such a slut!`);
		}
		if(mile.a_family_high){
			kat(`Hahaha!`);
		}else{
			txt(`@bar again offered me the joint but I handwaved her offer.`);
		}
		bar(`I can't believe I have more sexual experiences with @qaa than the whore he hired!`);
		kat(`Shut up?`);
		bar(`Haha! Nothing serious, when we were younger I just made him kiss me and grope my tits. You should've seen his face!`);
		
		if(mile.a_burned){
			link(`Wait, you're the Cousin!? `, 280);
		}else if(mile.a_family_high){
			link(`You should definitely fuck! `, 290);
		}else{
			link(`I don't think you would have to force him now!`, 290);
		}
		break;
		
	case 280:
		emo("smug");
		kat(`Wait, you're the Cousin?!`);
		bar(`The Cousin?!?`);
		kat(`In a weak moment, he confided he has a crazy crush on his cousin! That he's helplessly in love with her and masturbates to her pictures every day! Sometimes even twice, his record is five times when he was especially horny and you shared an especially raunchy photo.`);
		bar(`Shut up! You're fucking with me! I'm his cousin for fuck's sake!`);
		kat(`He told me the taboo makes it hot!`);
		bar(`Have I accidentally fucked with his brain?`);
		if(mile.a_family_high){
			kat(`Hahaha!`);
			bar(`Hahaha!`);
		}
		kat(`I don't think it's your fault. Anyway, when you're going to tell this to other people, please, don't cite me as the source! I promised him I won't tell anybody!`);
		bar(`Don't worry!`);
		link(`Finish. `, 300);
		break;
		
	case 290:
		emo("happy");
		bar(`Hahahaha! Shut up! You're such a crazy bitch!`);
		kat(`Hahaha!`);
		bar(`We're not kids anymore I'm his fucking cousin!`);
		kat(`That makes it hot, doesn't it?`);
		bar(`And he's a stupid nerd!`);
		kat(`You didn't seem to mind before! You can fuck him ironically!`);
		if(mile.a_family_high){
			bar(`Hahahaha!`);
			kat(`Hahaha!`);
		}
		if(mile.a_family_cousin === 1){
			bar(`And he's dating you!`);
			kat(`I'll allow it!`);
		}
		bar(`You're such a crazy bitch!`);
		txt(`We continued joking at @qaa's expense.`);
		link(`Finish. `, 300);
		break;
	
	case 300:
		emo("shy");
		txt(`@bar finished the joint and we did not have other excuses to stay outside.`);
		bar(`It was awesome talking with you, @kat! You're cool!`);
		kat(`You're awesome too!`);
		txt(`We parted and I went to look for @qaa. I was not able to find him anywhere so I at least went to get some alcohol. When I returned, in the hallway I was stopped by tacky man in his, I guessed, thirties.`);
		if(mile.a_family_cousin === 3){
			npc(2, `Hey, stunner! You're a whore?`);
			kat(`Excuse me!?!`);
			//if(mile.a_family_slut > 1) txt(`Maybe I should had worn something less slutty. `);
			//SPELL CONDITIONAL TODO
			npc(2, `Sorry, I didn't want to offend you. @bar told me that. I'm @npc_2.`);
			kat(`@katalt. Oh, right... yeah...`);
			npc(2, `Do you have a pimp?`);
			link(`Yes. `, 301, ()=> counter.temp = true);
			link(`No. `, 301, ()=> counter.temp = false);		
		}else{
			npc(2, `We haven't met before, I believe. I'm @npc_2.`);
			kat(`@katalt. @npc_2? Why I was warned to stay away from you?`);
			npc(2, `I'm the black sheep of the family.`);
			kat(`A bad boy?`);
			npc(2, `Exactly!`);
			kat(`Cool.`);
			if(mile.a_family_cousin === 2){
				npc(2, `Did @qaa really paid you for being here?`);
				link(`Yeah. `, 310);
				link(`Kinda. `, 310);
			}else{
				npc(2, `You're really here with @qaa?`);
				link(`Yeah. `, 320);
				link(`Kinda. `, 320);
			}
		}	
		
		break; 
		
	
	case 301: 
			if(counter.temp){
				kat(`Yeah, of course, I do.`);
				txt(`I hoped he will not ask for his name.`);
				npc(2, `Is he cool?`);
				kat(`Yeah.`);
				npc(2, `I just know a guy who's looking for hot girls, if you know what I mean. I can give him your number.`);
			}else{
				kat(`I'm kinda... freelancing.`);
				npc(2, `That has no future. Sooner or later somebody will rob you or beat you up. But if you want, I know a guy who's looking for hot girls, if you know what I mean. I can give him your number.`);
			}
			kat(`Thanks, but that won't be necessary.`);
			npc(2, `So @qaa actually had balls go hire a whore?`);
			kat(`Yeah.`);
			npc(2, `How do you like the party? Lame, right?`);
			kat(`I guess.`);
			npc(2, `Do you want to take a break and make quick cash?`);
			kat(`How... oh!`);
			txt(`He brought out his purse full of bills.`);
			if(mile.a_burned){
				txt(`He really thought I am a hooker and was ready to pay for my services. I was not sure whether I should feel offended or pleased. On one hand, I could earn quick money. On the other hand, @qaa will be so furious and humiliated when he will eventually finds out what I did. There was no downside. Well, except me being a whore.`);
			}else{
				txt(`He really thought I am a hooker and was ready to pay for my services. I was not sure whether I should feel offended or pleased that he find me so captivating. I could use some extra cash but despite my silly joke, I was not a whore! Or was I?`);
			}
				
			emo("imp");
			if(mile.slut < 6){
				emo("shy");
				if(mile.a_family_high){
					txt(`Under normal circumstances, I would immediately refuse. But at the moment I was high and in a particulary slutty mood.`);
				}else{
					txt(`But I could never do that! Having sex with a random guy for money...ewww!`);
				}
			}else if(mile.slut > 12){
				if(mile.a_family_high){
					txt(`I was high and just thinking about it was making my pussy wet.`);
				}else{
					txt(`Just thinking about it was making my pussy wet.`);
				}
			}else if(mile.slut > 7 & mile.a_family_high){
				txt(`I felt high and horny and actually seriously considered his offer.`);
			}
				
			link(`I'm not interested. `, 360);
			link(`I'm sorry. I'm here with @qaa and it would be improffesional. `, 360);
			con_link( (mile.slut > 5  || (mile.a_family_high && mile.slut >= -3) ) , `not slut/high enough`, `Okay. But we have to be quick! `, 340, ()=> mile.a_family_car = 3);
			
		break;
		
	case 310: 
			kat(`Did that dumb bitch @bar tell everybody? It was supposed to be just between us!`);
			npc(2, `Just to me. @qaa might be an egghead but convincing you to pose as his girlfriend might be the smartest thing he ever came up with.`);
			kat(`Thank you for the compliment.`);
			npc(2, `So... are you for hire even for other things?`);
			kat(`What do you mean?`);
			npc(2, `Well, the party is lame... and I need a quick break... and might use some company... and I have way more cash than my dweeb cousin...`);
			
			if(mile.a_burned){
				txt(`I was stunned and not sure whether I should feel offended or pleased. On one hand, I could earn quick money. On the other hand, @qaa will be so furious and humiliated when he eventually finds out what I did. There was no downside. Well, except me being a whore.`);
			}else{
				txt(`I was stunned and not sure whether I should feel offended that he thought I am a whore or pleased that he find me so captivating he was willing to pay for my services.`);
			}
		//TODO	CAR SUCK		
				
			if(mile.slut < 6){
				if(mile.a_family_high){
					txt(`Under normal circumstances, I would immediately refuse. But at the moment I felt in a particularly slutty mood.`);
				}else{
					txt(`But I could never do that! Having sex with a random guy for money...ewww!`);
				}
			}else if(mile.slut > 12){
				txt(`Just thinking about it was making my pussy wet.`);
			}else if(mile.slut > 7 && mile.a_family_high){
				txt(`I felt high and horny and actually seriously considered his offer.`);
			}
			
			con_link(mile.sub < 6, `submissive`,`I'm not a fucking whore, you creep! `, 311, ()=> mile.slut--);
			link(`No. `, 311);
			link(`Thanks for the nice offer but I'll pass. `, 311);
			if(mile.slut > 6) link(`When I'm sleeping with random guys, I'm not doing it for money! `, 311);
			con_link( (mile.slut > 5  || (mile.a_family_high && mile.slut >= -3) ) , `not slut/high enough`, `Okay. But we have to be quick! `, 340, ()=> mile.a_family_car = 3);

		break; 	
		
	case 311: 
			emo("shy");
			npc(2, `Hahaha! You looked so shocked and grumpy! I got you.`);
			kat(`You got me?`);
			npc(2, `It was just joking!`);
			txt(`I was not sure, whether he was quick at damage control or had a sick sense of humor but I began to understand why some people might not like him.`);
			npc(2, `I wanted to break the ice. I know you're a classy lady, not a hooker. But I wouldn't mind learning even more about you!`);
			txt(`He possesively put a hand on my hip and leaned uncomfortably close.`);
			kat(`Would you like to see my car?`);
			//TODO HORNYNESS
			if(mile.sub < 0) link(`Hands off me, you perv! `, 360);
			if(mile.sub < 9) link(`No! `, 360);
			if(mile.sub > 5) link(`Please, let me go! `, 360);
			con_link( (mile.slut > 5  || (mile.a_family_high && mile.slut >= -3) ) , `not slut/high enough`, `Only if seeing your car is an euphemism for fucking. `, 340, ()=> mile.a_family_car = 1);
		break;
			
			
	case 320: 		
			emo("shy");
			kat(`Yeah.`);
			npc(2, `...`);
			kat(`Why are you staring at me like that?`);
			npc(2, `I'm trying to figure out what's wrong with you.`);
			kat(`There's nothing wrong with me!`);
			npc(2, `When I heard @qaa brought a girl, I thought: good for him. But I imagined you four-eyed, fat and ugly.`);
			kat(`Thanks a lot.`);
			npc(2, `But you're a hot chick! Why are you even with @qaa? Is he blackmailing you or what? A girl like you needs a proper man.`);
			kat(`A proper man?`);
			npc(2, `Who can properly satisfy you.`);
			kat(`@qaa can satisfy me adequately.`);
			npc(2, `You deserve more! An older, more experienced guy who knows exactly what you need!`);
			txt(`He possessively put a hand on my hip and leaned uncomfortably close.`);
			npc(2, `Would you like to see my car?`);
			
			//TODO HORNYNESS
			if(mile.sub < 0) link(`Hands off me, you perv! `, 360);
			if(mile.sub < 9) link(`No! `, 360);
			if(mile.sub > 5) link(`Please, let me go! `, 360);
			con_link( (mile.slut > 5  || (mile.a_family_high && mile.slut >= -3) ) , `not slut/high enough`, `Only if seeing your car is an euphemism for fucking. `, 340, ()=> mile.a_family_car = 1);
			
		break;
			
	
	case 340:
			//TODO ext.local
			mile.slut++;
			if(mile.slut <= 5) mile.slut++;
			
			showLess(0, -1);
			emo("horny");
			txt(`I followed him outside to his car. By his boasting, I expected something better than acient ice Fabia. He sat down and I climbed astride on top of him.`);
			txt(`His car standing in the very corner of the parking lot so (almost) nobody could notice us making out. His hands enjoyed my breast and right there they were back fondling my ass. I was grinding against him, physically feeling how he was getting aroused.`);
			if(mile.a_family_high){
				txt(`Smoking weed always got me so horny. I did not care I met this guy ten minutes ago, my body itched and it needed to be scratched.`);
			}
			npc(2, `You're wet like an aquarium!`);
			txt(`He laughed when he pushed his hand under ${wears.skirt ? "my" : "the waistband of my"} @lower. I only moaned - enjoying his fingers brushing against my clit area - rather than trying to provide articulated answer.`); 
			npc(2, `Show me your puppies!`);
			txt(`He commanded me to take out my tits and he roughly squeezed and kneaded them.`);
			if(mile.boobs > 1){
				npc(2, `Damn, those are huge! Are they real?`);
				kat(`Well, they are not virtual...`);
				npc(2, `Damn, I love sluts with fake tits!`);
			}
			link(`Blow him.`, 341);
			con_link(mile.slut > 7 || mile.a_family_high, `not slut/high enough`, `Fuck him.`, 342);
			break;
			
	case 341:
			log_sex("bj", "npc_2", "kat");
			txt(`I did not want to spend the rest of the even leaking his cum so I moved on the passenger seat and leaned over his lap.`);
			npc(2, `Oh yeah! Suck me hard!`);
			txt(`He already pulled his pants down and his long, rocklike cock was waiting for me there. I swallowed it and began giving him a blowjob.`);
			npc(2, `Doing great!`);
			txt(`He reached and began roughly rubbing my yearning pussy. It felt so great he had to lightly slap it when I got too distracted and slowed down my sucking.`);
			txt(`Being with a complete stranger in the middle of the parking lot during a family event I was supposedly visiting with my boyfriend made me so turned on I actually came first before he exploded in my mouth.`);
			
			link(`Continue.`, 343);
			break;
			
	case 342:
			log_sex("sex", "npc_2", "kat");
			emo("bliss");
//TODO CONDOM?
			mile.a_family_car = mile.a_family_car === 1 ? 2 : 4;
			if(wears.skirt){
				txt(`He hiked up my skirt and ${PC.panties ? "pulled my panties aside. " : "keenly caressed my exposed pussy. "}`);
			}else{
				txt(`His dexterous fingers unbuttoned my @wears.lower and after minor difficulties pulled them down. ${PC.panties ? "He was not bothered by my panties, just pulled them aside. " : ""}`);
			}
			npc(2, `You want it, don't you?`);
			kat(`I do!`);
			txt(`He lovered his pants and his hard cock sprung up. I mounted him and began ferociously riding him. Ferociously but carefully, after my head hit the car roof. He was lucky I was so flexible.`);
			npc(2, `Heh, you really fuck like a pro!`);
			kat(`Shut up!`);
			txt(`I laughed. Being with a complete stranger in the middle of the parking lot during a family event I was supposedly visiting with my boyfriend made me so turned on I actually came first before he exploded inside me.`);
			
			link(`Continue.`, 343);
			break;
			
	case 343:
			//TODO ext.local
			emo("relax");
			txt(`He became less nice after he ejaculated inside me. His major concern was to not mess the seats with cum. He offered me a dirty rag from the car doors but I refused and did my best with the last remaining tissues from my handbag.`);
			if(mile.a_family_car >= 3){
				mile.whore++;
				const key = mile.a_family_car === 3 ? "a_family_bj" : "a_family_sex";
				txt(`At least I did not have to urge him, he appreciatively handed me ${bills(key)}. `);
				payment(key, "cash");
			}
			link(`Back inside.`, 360);
			break;
		
	case 360: 
			emo("relax");
			showAll();
			if(!mile.a_family_car && mile.a_cuck > 1){ //TODO!!
				mile.a_family_know_sheep = true;
				emo("angry");
				txt(`But he did not take no for an answer and blocked me when I tried to leave.`);
				npc(2, `Come on!`);
				qaa(`Hey! What are you doing?!`);
				npc(2, `Hello, @qaa. How are you? I'm just checking your new girlfriend! She's really hot!`);
				txt(`He shamelessly groped my butt while @qaa was just helplessly watching. Then @npc_2 pushed me towards @qaa, slapped my ass and chuckled:`);
				npc(2, `Have fun, guys!`);
				qaa(`@kat! Are you fine?`);
				kat(`What hell? He was molesting and you were just standing there?`);
				qaa(`I... I'm sorry, @kat.`);
				kat(`You're so useless!`);
				
			}else if(!mile.a_family_car && (mile.a_tripoint === 1 || mile.a_dom <= 1) ){
				mile.a_family_know_sheep = true;
				mile.a_dom--;
				emo("happy");
				txt(`But he did not take no for an answer and blocked me when I tried to leave.`);
				npc(2, `Come on!`);
				qaa(`Hey!`);
				npc(2, `Hello, @qaa. How are you? Your new girlfriend's really hot.`);
				qaa(`Yeah... I was looking for her.`);
				txt(`@qaa took my hand and drew me closer to him. @npc_2 did not tried to stop me:`);
				npc(2, `Have fun, guys!`);
				txt(`And left.`);
				kat(`Thank you for saving me!`);
				qaa(`You're welcome!`);
			
			}
			
			qaa(`I was looking for you! Where were you?`);
			kat(`Just socializing.`);
			qaa(`Socializing!?!`);
			kat(`Yeah, I talked with @bar. She's cool.`);
			qaa(`She's absolutely the worst! I hate her!`);
			if(mile.a_family_high){
				kat(`That's pretty hilarious and ironic, considering... hahaha.`);
				qaa(`Look at me! Are you high?!`);
				kat(`Maybe.`);
				if(mile.a_burned){
					qaa(`What the hell did I tell you!`);
					kat(`Don't get drunk!`);
				}
				qaa(`Oh my god!`);
				kat(`Hahaha`);
			}
			
			//TODO - ADMIT SHE FUCKED HIM
			if(!mile.a_family_car && !mile.a_family_know_sheep){
				mile.a_family_know_sheep = true;
				kat(`Also I spoke with @npc_2.`);
				qaa(`I told you to stay away from him!`);
				kat(`I tried but he ran into me!`);
				qaa(`What did he want?`);
				if(mile.slut < 5){
					kat(`He was very sleazy and rude.`);
				}else{
					kat(`To have sex with me. But don't worry! I handled it with grace, I told him maybe some other time.`);
				}
			}
			
			if(mile.a_family_car && mile.a_tripoint === 3){
				emo("smug");
				kat(`Also I met @npc_2 and we fucked in his car.`);
				qaa(`WHAT?!`);
				if(mile.a_family_car === 1 || mile.a_family_car === 3){
					kat(`Don't worry! I'm exaggerating. It was just a quick blowjob.`);
					qaa(`JUST A BLOWJOB?!`);
				}else{
					kat(`We had quickie in his car.`);
					qaa(`When?!?`);
					kat(`A few minutes ago, I'm still sticky with his cum.`);
					qaa(`YOU FUCKED @NPC_2!?!`);
				}
				kat(`Keep your voice down, you don't want everybody in your family to know!`);
				qaa(`How could you?!? Have you no shame!?!`);
				kat(`Obviously no. You won't even ask whether I enjoyed it?`);
				qaa(`No. ...did you?`);
				if(mile.a_family_high){
					kat(`Yeah, I did! Hahahaha!`);
				}else{
					kat(`It was fine, I guess.`);
				}
			}else if(mile.a_family_car){
				txt(`I decided to not share with him the quick sexual encounter in the car. He might get unreasonably jealous.`);			
			}
			
			qaa(`Please, from now on just stay with me!`);
			link(`Meal. `, 400);
			break;	
	case 400:
			emo("imp");
			if(mile.a_burned){
				mile.a_hate++;
				txt(`Finally, everybody sat down to an official meal which was delicious. I was sitting next @qaa and quickly got bored by the conversation about heat pumps so I turned to the middle-aged guy sitting on the opposite side of the table.`);
				/*
				if(Math.random() < 0.38){
					kat(`Uncle @npc_3, right? I was wondering what's your opinion about the occupation of the Spratly Islands. Went Vietnamese too far when they sank that Chinese boat? And should the NATO support American expeditionary forces or stay out of it? `);
				}else{
				*/
				kat(`@npc_3, right? I was wondering what's your opinion about the Eastern question. Should NATO intervene in ${Math.random() < 0.7 ? "Bashkortostan" : "Krasnodar"}? Or stay out of the war? `);
				//}
				txt(`Before he was able to provide his insight, @qaa painfully squeezed my arm.`);
				qaa(`We need to talk, @kat!`);
				kat(`Sure.`);
				txt(`I followed him until we were alone. He was very angry.`);
				qaa(`What the hell, @kat! I gave you a simple list of rules. Is there any you didn't break?`);
				kat(`I don't know! I don't remember them all!`);
				qaa(`Are you doing this on purpose? Or are you just this stupid? Anyway, I don't care. Our deal is off. Enjoy your failing grade.`);
				txt(`Oh no! That would be extremely bad!`);
				kat(`Hey! Everything went well and everybody liked me!`);
				qaa(`I don't care! You're free to leave!`);
				txt(`That petty jerk was seriously annoyed I had no respect for him.`);
				kat(`Come on! From now on I'll stay quiet!`);
				qaa(`No! You had your chance and you failed. Go away, there's nothing you can do to convince me to change my mind!`);

				link(`Do anything to convince him. `, 410);
				next(`You know what? Fuck you, I'm leaving! `, ()=> mile.a_project = -1); //TODO - car ride with TEMP2 ?

			}else{
				txt(`Finally, everybody sat down to an official meal which was delicious. But I was quickly getting bored by the conversation, mostly arguing about the EU and Germany. Their complaints were not entirely incorrect but not very original and insightful either.`);
				txt(`I turned to @qaa and with my elbow accidentally pushed a pastry fork off the table. With a sight, I went to search for it under the table. I swear this was not my intention but when I was already there, hidden by a long tablecloth, I decided to mess with @qaa.`);
				txt(`My hand slowly slipped between his legs, lightly brushing against his crotch. His knees twitched, damn, if I only I could see his expression. When I ascended back, his face was still red.`);
				qaa(`What are you doing, @kat!`);
				txt(`He hissed. I just smirked. He frowned.`);
				if(mile.a_tripoint === 1){
					qaa(`Go to the bathrooms and wait for me there!`);
					txt(`Whispered @qaa in my ear. I was stunned! I did not expect him to be this kinky! I immediately stood up and left.`);
				}else{
					kat(`I'll go to the bathrooms. Wait for a while and then join me there!`);
					txt(`I whispered in his ear. He was stunned but slowly nodded. I immediately stood up and left.`);
				}
				link(`Bathrooms. `, 401);
			}
			break;
			
		case 401: 
			showLess(1, -1);
			qaa(`@kat?`);
			kat(`I'm here!`);
			txt(`I called him from a stall.`);
			if(mile.a_family_high && !mile.a_family_car && mile.slut > 2){
				txt(`Smoking weed always got me so desperately horny. I could not take it anymore, I spent the time before he arrived by masturbating.`);
			}else if(mile.a_family_high && mile.a_family_car){
				txt(`I blame it on being high. I was such a nympho, being with @npc_2 was not enough, I still needed more.`);
			}else if(mile.slut > 10){
				txt(`I was bored and horny and spent the time before he arrived masturbating.`);				
			}else if(mile.slut < 2){
				txt(`This felt so wrong and was so far beyond my usual comfort zone. But I wanted to know what will happen next.`);
			}
			
			if(mile.a_tripoint === 1){
				qaa(`You think you can just tease me and get away with it, you little slut?`);
				kat(`I'm very sorry, sir. Am I in trouble?`);
				qaa(`Oh yeah. In very serious trouble.`);
				kat(`What could I do to make you forgive me?`);
				qaa(`Start with taking off your @upper!`);
				txt(`I did so and he buried his face between my tits, kissed them and his tongue played with my nipples.`);
			}else{
				qaa(`You wanted to talk about something personal?`);
				txt(`I threw myself on him and kissed him. He kissed me back but then pushed me away.`);
				if(mile.a_family_car && mile.a_tripoint === 3){
					qaa(`Like serious? @npc_2 wasn't enough to satisfy you?`);
					kat(`No!`);
				}
				qaa(`I don't think this is the best time and the best place!`);
				if(mile.a_shopping > 1){
					kat(`You weren't protesting so much when ${mile.a_shopping === 3 ? "we fucked" : "I gave you head"} in the fitting room. `);
					qaa(`My parents weren't there! I'm not in the mood. `);
				}
				kat(`Then why are you so hard?`);
				txt(`I argued while massaging him through his pants.`);
				qaa(`Somebody can walk right in!`);
				kat(`Then we better should be quick! I take my top off if that's okay?`);
				qaa(`...yeah, that's okay...`);
				txt(`I did so and he began reflexively fondling my bare @breasts.`);
			}
			link(`Sex. `, 402);
			break;
			
		case 402: 
			log_sex("sex", "qaa");
			emo("bliss");
			mile.a_love++;
			txt(`I regretted I faltered and did not choose the stall for the disabled. It was more spacious but I guess I was too squeamish. But we managed to fit.`);
			txt(`@qaa pushed me against the wall, I wrapped my legs around him and he heavily entered me.`);
		//TODO - sloppy seconds
			qaa(`This is what you wanted?`);
			kat(`I'm waiting for this the whole day!`);
			txt(`He was thrusting, I was wiggling on his cock, I was quickly getting into it, completely forgetting where I was.`);
			kat(`OH YEAH!`);
			txt(`I loudly moaned. Terrified @qaa inconsiderately pressed his hand on my mouth.`);
			if(mile.a_dom <= 0){
				qaa(`Shut up, you crazy bitch!`);
			}else{
				qaa(`Please shut up, @kat!`);
			}
			txt(`He whispered. And then somebody entered the bathrooms. We both froze, we did not move a centimeter until the mysterious person did not leave again.`);
			qaa(`Fuck!`);
			txt(`Sighed @qaa.`);
			kat(`Was it somebody from your family? Or some other quest of the restaurant?`);
			qaa(`How the hell should I know?`);
			kat(`I thought you would recognize if it was your mother or so.`);
			qaa(`Oh no! You think it was...`);
			kat(`No! I was just messing with you. Although I can't rule it out.`);
			qaa(`I don't think I can continue. I think I lost my boner.`);
			kat(`You did not. I can still feel it inside me! Man up, you have to finish before they notice we're both gone!`);
			qaa(`Oh no! You're only making it worse!`);
			txt(`I teasingly wiggled and he eventually began thrusting again.`);
			if(mile.a_dom <= 0 && mile.sub > 8){
				qaa(`Do you think they heard your moaning? The next time I'll have to gag you!`);
				kat(`Is that a promise?`);
			}
			txt(`${mile.slut > 4 ? "@qaa was" : "We were"} still scared of being caught but our ride went right to orgasm without further interruptions.`);
			txt(`When we were done, we hastily returned back. As far as I could guess, nobody realized what we had been doing.`);
			txt(`Overall, I would say the birthday celebration went well and @qaa could be extremely grateful I agreed to go with him.`);
			next();
			break;
			
			
			
		case 410:
			emo("smug");
			mile.a_project = 3;
			showLess(1, -1);
			kat(`Or can I?`);
			qaa(`No.`);
			kat(`Let me show you something!`);
			txt(`I grabbed him and dragged him to the bathrooms. There I locked us in the stall and stripped my top.`);
			qaa(`Flashing your tits won't be enough, slut.`);
			kat(`You're already changing your tone!`);
			txt(`@qaa made a grimace when he realized I won.`);
			if(mile.sub < 2){
				kat(`And I'm going to do way more! You wanted to fuck me, don't you? So fuck me, this is your only chance. Or are you too scared?`);
				if(mile.slut < 5){
					txt(`I tried to sound arrogant and self-assured even though the whole situation was so humiliating and degrading. But it was not about the project anymore, I had to show him!`);
				}else{
					txt(`I openly mocked him.`);
				}
				qaa(`A desperate whores like you don't scare me!`);
				kat(`You're afraid to even touch me!`);
			}else{
				if(mile.slut < 5){
					kat(`What more do you want?`);
					qaa(`I felt humiliated and degraded. But the desire to show him was stronger than my inhibitions.`);
				}else{
					kat(`What more would you want?`);
					txt(`I lasciviously challenged him.`);
				}
				qaa(`You know what I want! And you're going to give it to me! Because you're whore who's only able to whore herself out!`);
				kat(`Fuck you!`);
				qaa(`You're going to!`);
			}
			txt(`@qaa roughly squeezed my @tits. I let my @lower fall to the floor and caressed my pussy.`);
			kat(`Fuck me, your pathetic twerp!`);
			link(`Sex. `, 411);
			break;
			
		case 411:
			log_sex("sex", "qaa");
			emo("shock");
			showLess(1, 0);
			if(mile.sub < 3){
				txt(`His eyes were full of panic when he was unbuckling his belt. He did not like the circumstances but he could not say no.`);
				txt(`I let him push me against a wall. He tried to align his dick with my slit and mumbled:`);
				qaa(`You're finally mine, slut!`);
				txt(`I slapped him:`);
				kat(`What takes you so long! We don't have the whole day!`);
				txt(`He finally pushed his hard cock inside me. But I did not intend to let him enjoy it:`);
				kat(`Are you already in? I'm not feeling anything!`);
				qaa(`Shut up, bitch!`);
				txt(`He was thrusting hard and furiously, his face twisted by anger and full concentration on his performance, but was not able to last even a full minute.`);
				qaa(`Fuck!`);
				txt(`He cursed when his cock began pulsing inside me and flooding pussy with his cum.`);
				kat(`That was all?!`);
				qaa(`I don't care what you're saying! I get to fuck you and enjoyed it!`);
				
			}else{
				qaa(`Oh, I will!`);
				txt(`He roughly pushed me against the wall and pushed his hard cock inside me.`);
				qaa(`You're mine, bitch!`);
				kat(`Are you already in? I'm not feeling anything!`);
				txt(`I laughed and provoked him.`);
				qaa(`Shut up, you stupid cunt!`);
				txt(`@qaa grabbed my throat and clutched it so tightly I was barely breathing and was not able to use any of the other demeaning quips I prepared.`);
				txt(`He was thrusting hard and furiously, his face twisted by anger and full concentration on his performance, but was not able to last even a full minute.`);
				qaa(`Fuck!`);
				txt(`He cursed when his cock began pulsing inside me and flooding pussy with his cum.`);
				qaa(`Did you enjoy it?`);
				txt(`He chuckled and slapped me.`);			
			}
			
			kat(`Did you? Is this how you imagined your first time with me? Thirty seconds on dirty bathrooms with no love nor passion?`);
			txt(`I saw I hit him hard. He seemed to be seriously upset and wretched. I guess just I crushed a few of his most cherished daydreams.`);
			kat(`Now be a good boy and start working on the project as soon as you return home so we can both get good grades!`);
			qaa(`Start working?! You know what, @kat? Maybe you should stop being so self-absorbed! The world isn't rotating around you! I finished and submitted the project weeks ago! You're stupid if you ever believed I care about you more than about my grades!`);
			kat(`What?`);
			qaa(`Yeah! You fucked me for nothing! Because you're a pathetic whore who tends to solve all her problems with her cunt!`);
			txt(`He zipped his pants and walked away.`);
			next();
			
	}
}
		
		
		


//reaction to boobjob
export const a_boobjob = (index) => {
	switch(index){
		default:
		case 101: 
			set.irl();
			emo("anoy");
			qaa(`I still can't believe you did it!`);
			txt(`Puzzled @qaa stared at my ${mile.clinic_boobjob === 3 ? "significantly" : ""} improved chest.`);
			kat(`But you love it?`);
			if(mile.clinic_boobjob === 3){
				qaa(`Well... I already liked your breasts the way they were... isn't such size a bit excessive?`);
				kat(mile.bimbo_mods > 1 ? `I don't think so! This size is perfect! ` : `Well, maybe I overdid it a little...`);
				txt(`@qaa shrugged.`);
			}else{
				qaa(`I already liked your breasts the way they were... but this isn't bad either...`);
			}
			kat(`I expected more exultation.`);
			qaa(`I still have not gotten over the shock. Why did you so suddenly decide to get your tits enlarged?`);
			kat(`It wasn't suddenly, I wasn't fully satisfied with my body for a long time and since now I'm 18...`);
			txt(mile.c_boobjob === 3 && mile.bimbo_mods > 0 ? `I admitted. ` : `I claimed. `);
			qaa(`Are you sure our silly tasks didn't awaken something in you?`);
			kat(`Absolutely no! This absolutely isn't related to your blackmail! I was thinking about this for a long time and made my decision carefully!`);
			qaa(`Strange you never mentioned it to me.`);
			kat(`I talked about it with the girls.`);

			if(mile.a_dom > 0){
				qaa(`I just... well, it would be nice if you at least discussed it with your boyfriend.`);
				if(mile.a_girlfriend){
					kat(`I don't need you permission.`);
				}else{
					kat(`First, boyfriend is a bit generous term. Second, I don't need your permission!`);
				}
				txt(`@qaa scowled.`);
				qaa(`Sorry, that's not what I meant! I didn't mean to be possessive, I understand I don't own you! But maybe... you know... it would be nice if you told me about such big changes...`);
				
			}else{
				qaa(`You don't think that maybe you should've asked your boyfriend?`);
				if(mile.a_girlfriend){
					kat(`Are you saying I should've asked you for permission?`);
				}else{
					kat(`First, boyfriend is a bit generous term. Second, I don't have to ask you for your permission!`);
				}
				txt(`@qaa scowled.`);
				qaa(`You don't need my permission! But it would be very nice if you at least discussed such a big change with me!`);
			}

			con_link(mile.sub > -7, `dominant`, `Sorry, I should've mention that. `, 102);
			con_link(mile.sub < 11, `submissive`, `I can do whatever I want!`, 103);
			con_link(mile.slut > 1, `too prude`, `Do you want to squeeze them?`, 104); 
			break;
			
		case 102:
			emo("relax");
			mile.a_boobjob = 1;
			mile.a_dom--;
			if(mile.sub < 3) mile.sub++;
			txt(`I shrugged.`);
			kat(`You're right, I should've told you. I'll tell you in advance the next time I'm going to get my tits enlarged.`);
			qaa(`You want to get them enlarged even more!?!`);
			kat(`Yeah, why not?`);
			txt(mile.bimbo_mods < 2 ? "I joked." : "I laughed. ");
			qaa(`That might be a bit excessive...`);
			kat(`Come on, you don't love big-titted girls?`);
			qaa(`I love you.`);
			txt(`He chuckled.`);
			next();
			break;
			
		case 103:
			emo("angry");
			mile.a_boobjob = 2;
			mile.a_dom++;
			mile.a_love--;
			mile.sub--;
			if(mile.a_dom > 0){
				qaa(`Of course, you can! But it kinda feels like you were ignoring me.`);
			}else{
				qaa(`Of course, you can! But it's kinda rude to completely ignore me!`);
			}
			kat(`It is my body!`);
			qaa(`I'm not even discussing that! Why do you have to be so contentious?!`);
			kat(`I'm not contentious!`);
			qaa(`You are contentious. I'm no longer wondering why @kub broke up with you.`);
			kat(`What is that supposed to mean! And I broke up with him! Do you want to break up with me?`);
			qaa(`...no, I don't! But I don't enjoy fighting with you.`);
			kat(`So stop! I'm not fighting with you, you're fighting with me!`);
			qaa(`...`);
			kat(`...`);
			qaa(`...`);
			kat(`I was so happy about my new tits and you had to completely ruin it!`);
			qaa(`Sorry. I think your new tits are great!`);
			next();
			break;
			
		case 104:
			mile.a_boobjob = 3;
			emo("imp");
			if(mile.slut < 7) mile.slut++;
			txt(`I impishly winked at him.`);
			qaa(`What?!`);
			kat(`I know looking isn't enough! Come on, you can feel them!`);
			qaa(`We are discussing something! You can't solve everything...  with your body...`);
			kat(`Or can I?`);
			qaa(`No?`);
			txt(`He answered but he was not able to resist and eagerly squeezed my brand new tits.`);
			kat(`Are you still mad?`);
			qaa(`I wasn't mad, just irked.`);
			kat(`But now you're happy?`);
			qaa(`Yeah, I am.`);
			txt(`He kneaded my breasts. Men are so simple.`);
			next();
			break;
	}
}




//FATHER
export const a_father_initiation  = (index)=> {
	mile.a_father = 1;
	present(
		[3, `Mr Sokol`],
	);
	placetime("tower block entrance");
	
	txt(`When I was returning from school, I ran into @qaa's father. He was so nice he gallantly held the door open for me so I did not have to search for my keys.`);
	kat(`Thank you!`);
	if(mile.hair_color){
		npc(3, `You're lucky, @katalt, I almost didn't recognize you with ${mile.hair_color} hair!`);
	}else{
		npc(3, `You're welcome, @katalt!`);
	}
	txt(`He smiled at me. It was a bit early to be home from work but I knew he was working from home. The trashbin he was carrying revealed he had just gone out with the trash. He probably did not expect to run into anybody, he was dressed just in stretched sweatpants and an old tattered tee and almost seemed a bit embarrassed.`);
	npc(3, `How are you? How's school?`);
	txt(`He tried to engage in small talk when we were waiting for the elevator.`);
	if(ext.background === 3){
		kat(`Fine I guess. I hope I'll get elected to become the student council president.`);
		npc(3, `That sounds great! I'm sure you'll get elected!`);
	}else{
		kat(`Fine I guess.`);
	}
	txt(`We entered the confined elevator cabin and pressed the buttons of our respective floors. He was for a moment lost in his thought but then articulated them:`);
	npc(3, `I do remember you as a little kid but now you're all... ...grown up.`);
	txt(`There was a slight hesitation hinting he was not sure how to non-awkwardly finish the sentence or whether it was appropriate to start it in the first place. He naively thought I will not notice him secretly ogling my reflection in the large mirror.`);
	kat(`Yeah, I'm a big girl now...`);
	txt(`The elevator stopped and our short conversation was over.`);
	kat(`Goodbye!`);
	npc(3, `Bye!`);
	txt(`Well, that was slightly awkward but I could not blame him for noticing my sexy outfit and for how little of my body it was hiding. Oh, fuck!`);
	txt(`I had an idea! So twisted and horrible even I was aghast.`);

	if(mile.a_burned){
		txt(`@qaa was a real pain in the ass recently. I just craved to do something to totally destroy him, embarrass and humiliate him. And this might be it. I was sure it would hit him hard!`);
	}else{
		txt(`At first, it was fun to tease @qaa by flirting or having sex with other guys. However, it seemed he was gradually getting used to it and it was getting harder to make him react. But there was one reliable way to shock him...`);
	}
	
	if(mile.slut > 6){
		txt(`Almost all my previous sexual experiences were with guys ${mile.lesbian > 3 ? "or girls " : ""} close to my age. And I was very curious about having sex with somebody old enough to be my father.`);
	}else if(mile.slut < 2){
		txt(`Imagining something so lewd was making me a bit uneasy. It would be weird, he was old enough to be my father! However, the result might be just worth it!`);
	}else{
		txt(`I was not really interested in older men that much. However, considering the result, it might be definitely worth it. And he might even surprise me and learn me something I did not know.`);
	}
	txt(`Was it too crazy?`);
	
	next(`No, I should definitely do it! `);
	next(`I will think about it, there is no rush. `);
	next(`Yeah, too crazy and stupid. `, ()=> mile.a_father = -1);
}


export const a_father_action  = (index)=> {
	switch(index){
		default:
		case 101: 
			mile.a_burned ? mile.a_hate++ : mile.a_dom++;
			mile.a_cuck++;
			
			placetime("@qaa's place");
			emo("focus");
			present(
				[3, `Mr Sokol`],
			);
			
			txt(`I quickly rushed back home. I did not have too much time - when I was leaving the school building, @qaa was still chatting with @qbb and @qcc, probably discussing how to make my life worse - so I had like a half hour before he gets back home too. I admit I was very nervous when I was knocking on the door. The plan had to work perfectly and there was no place for errors.`);
			kat(`Good afternoon, @npc_3!`);
			npc(3, `Hello, @katalt! Are you looking for @qaa? He's not back yet.`);
			kat(`I know, @npc_3. He's staying at school to help @maj in the library. But he promised to borrow me his notes and I really need them because there's a test tomorrow. I talked with him and he told me I can take them from his room, that he won't mind.`);
			if(mile.a_burned){
				txt(`I could be very convincing and he was not even suspecting I was making stuff up.`);
			}else{
				txt(`He already had seen us hanging out together many times so he was not suspicious at all.`);
			}
			npc(3, `Oh sure. Come in!`);
			kat(`Thank you!`);
			txt(`He invited me in and led me to @qaa's room.`);
			kat(`He told me the notes were right on his desk...`);
			txt(`There were no notes on the desk, it was like a fifty-fifty chance.`);
			kat(`Oh, he had to misremember it, they have to be somewhere there... would you mind if I look around?`);
			npc(3, `No problem.`);
			
			{
				const ass = (()=>{
					if(wears.pants){
						return `It gave him a nice opportunity to check my ass in my tight @lower. `;
					}else if(wears.microSkirt){
						return `${wears.nanoSkirt 
							? "My already pretty scandalous skirt rode up, giving him a full view at my ass"
							: "It gave him a nice opportunity to check me from behind. My short skirt rode up, even though I was not sure whether it was enough to give him a clear glimpse of my ass"
						}${wears.panties
							? " and my @panties. "
							: " and show him I was wearing no panties. "
						}`;
		//TODO LONG DESC COLOR
					}else{
						return `It gave him a nice opportunity to check my ass in my short @lower. I'm sure he imagined how easy it would be to just flip it up. `;
						
					}
				})();
				
				txt(`I bent down to check the shelves. ${ass}`);
			}
			kat(`They are not there either.`);
			npc(3, `Check the other shelf, he's keeping some papers there.`);
			txt(`He pointed at even lover shelf. What a naughty dog! This might be easier than I thought. (Unless he genuinely tried to help me.)`);
			
			/*
			//TODO TATTOO
				I didn't even know you had a tattoo!
				Yeah! It's a new one! Do you like it? 
				It's nice.
			*/
			link(`Are you staring at my ass? `, 102);
			break;
			
		case 102:
			emo("imp");
			npc(3, `W... what?`);
			kat(`Are you staring at my ass? It seems guys are always ogling me, whatever I'm doing.`);
			npc(3, `Well, ${wears.pants ? "they're" : "it's a"} rather ${wears.miniSkirt ? "short" : "tight"} @lower. And pretty ass.`);
			txt(`He did not let me shame him. I turned back and straightened up.`);
			kat(`Really? Do you think  ${wears.pants ? "they're" : "it's"} too ${wears.miniSkirt ? "short" : "tight"}?`);
			if(wears.miniSkirt){
				npc(3, `Some might call it too short.`);
			}else{
				npc(3, `I can't imagine ${wears.pants ? "them" : "it"} to be any tighter without being uncomfortable for you.`);
			}
			kat(`Well, I'm kinda experimenting with new things, trying to find my own style. You know, since I'm eighteen and can do whatever I want.`);
			npc(3, `Good for you! You should be experimenting when you're still young! ...except for drugs. You shouldn't ever experiment with drugs, @katalt!`);
			kat(`I didn't mean drugs, I meant more like relationships and sex.`);
			if(mile.a_burned){
				npc(3, `I think I heard enough, I don't think I should be discussing sex with you.`);
				kat(`Why not? We're both grown-up adults.`);
				npc(3, `Hahaha... sorry, I didn't mean to offend you!`);
				kat(`I'm not a little girl anymore! Should I prove to you I am a woman?`);
				if(mile.slut > 8){
					showLess(0, -1);
					txt(`Before he was able to stop me I flashed him my @tits.`);
					npc(3, `What are you doing, @katalt?`);
					kat(`You are still not believing me?`);
				}
								
			}else{
				npc(3, `Is my son a part of those experiments?`);
				kat(`Yeah, but I'm honest with him and he seems to be enjoying it.`);
				npc(3, `Okay.`);
				kat(`But I still don't completely understand, @qaa is so... bashful and nerdy while you radiate such manliness and confidence.`);
				npc(3, `He's after his mother. But I'm sure he'll grow into it.`);
				kat(`But don't worry! In bed, I'm fully satisfied by him!`);
				npc(3, `I don't want to hear the details.`);
				kat(`Even though I have to say, I do wonder how would you two compare.`);
				npc(3, `Compare how?`);
				kat(`I mean in bed. Fornicating with me.`);
				npc(3, `Oh, @katalt, don't be silly!`);
				kat(`I'm serious!`);
				npc(3, `You're dating my son! And, you know...`);
				kat(`That's okay, we have an open relationship. And what?!? Are you still seeing me just as a little girl? Should I prove to you I'm an adult woman?`);			
			}
			
			link(`Grab him by his cock. `, 103, ()=> counter.father = 1);
			link(`Get on the knees. `, 103, ()=> counter.father = 2);
			link(`Strip down. `, 103, ()=> counter.father = 3);
			break; 
			
		case 103: 
			showAll();
			rollUpSkirt(3);
			if(counter.father === 1){
				txt(`I moved uncomfortably close and my hand rode between his legs and my figers fondled his manhood throught the sweatpants. I could feel him getting harder, it was obvious how much he wanted me even though he did not want to admit it.`);
				txt(`@npc_3 pushed me away.`);
				
			}else if(counter.father === 2){
				txt(`I made step closer and then fell down on my knees. He was too puzzled to react when I suddenly grabbed the waistband of his sweatpants and pulled them down together with his boxers. His exposed cock was just centimetres from my face. I gently took it in my fingers.`);
				txt(`I could feel him getting harder, it was obvious how much he wanted me even though he did not want to admit it.`);
				txt(`@npc_3 roughtly grabbed my shoulder and pulled me back up on my feet.`);
			
			}else if(counter.father === 3){
				showLess(0,1)
				txt(`Without further delays, I stripped my @upper and put it on @qaa's table. His father's eyes were bolted to my tits.`);
				npc(3, `Please, get dressed, @katalt!`);
				if(wears.bra){
					kat(`Would you mind?`);
					txt(`I turned my back to him and let him to unhook my bra. He automatically did it with one, experienced motion.`);
				}
				kat(`You don't prefer me undressed?`);
				if(PC.lower){
					txt(`I continued stripping and unzipped and pulled down my @lower ${wears.pantyhose ? "and @socks" : ""}.`);
				}
				if(!PC.panties){
					npc(3, `You're not wearing any panties?`);
					kat(`Yeah, I found out that not wearing panties saves a lot of time when I' ready for action.`);
				}
			}
			
			/*
			if(mile.sub > 9){
				I tried my best to play the seductive vixen even though pushing him 
			}
			*/	
			npc(3, `Stop! What are you doing, @katalt?! Are you crazy?!`);
			txt(`I sat down on @qaa's bed and spread my knees obscenely wide${counter.father != 3 && wears.miniSkirt ? ", letting my @lower ride high up" : ""}.`);
			kat(`Isn't it clear what I want?!? I want to have sex with you!`);
			if(mile.slut > 6){
				txt(`I smiled and began rubbing my pussy. `);
			}
			if(mile.slut < 5){
				txt(`Playing the horny nympho was making me feel silly and embarrassed. I was not completely comfortable in that role and the more surprising was how easy and natural it was to act like a slut and seducing him.`);
			}else if(mile.slut > 12){
				txt(`It was so hot! The taboo situation, my flawless nefarious plan, the confusion in his eyes mixing desire to sample my young flesh and knowledge it was a very bad idea. I was already wet and needed him inside me.`);
			}			
			kat(`Are you game?`);
			txt(`I looked at him and then meaningfully let my eyes descend to ${counter.father === 2 ? "his erect cock" : "the tent in his sweatpants"}.`);
			npc(3, `You're old enough to be my daughter!`);	
			kat(`I could call you daddy?`);
			npc(3, `I prefer if you didn't!`);
			txt(`He sighed, made a step closer to me and his fingers stroked my hair and shoulder.`);
			
			link(`Take me, @npc_3! `, 104, ()=> mile.a_father_daddy = false);
			link(`Fuck me, daddy! `, 104, ()=> mile.a_father_daddy = true);
			break;
			
			
		case 104:
			log_sex("sex", "npc_3");
			if(mile.slut > 9) emo("horny");
				
			npc(3, `Fine, now I believe you're a grown-up woman.`);
			txt(`@npc_3 finally agreed after squeezing my titties.`);
			kat(`Awesome!`);
			if(counter.father === 3){
				showLess(0,0,1);
				txt(`I fell backward on @qaa's bed and made myself comfortable. ${wears.panties ? "@npc_3 grasped my @panties and deliberately pulled them down, revealing my @pussy" : ""}.`);
			}else{
				showLess(1,0);
				let tex = rollUpSkirt(3, 0); //TODO will work wacky probably
				if(wears.skirt){
					let first; //rollUpSkirt return someting like "I lifted up my @lower and pulled down my @socks" so we have to remove the stuff before first and
					[first, ...tex] = tex.split("and");
					console.log(tex)
					tex = tex.join("and");
					
					if(tex){
						txt(`I fell backward on @qaa's bed, hiked up my skirt and he ${tex}.`);
					}else{
						txt(`I fell backward on @qaa's bed, hiked up my skirt to let him see my @pussy and made myself comfortable.`);
					}
				}else{
					txt(`I fell backward on @qaa's bed, making myself comfortable, and he ${tex.slice(1)}.`);
				}
			}
			
			txt(`@npc_3 looked down at my body which was fully at his disposal and the fascination and enamorment in his eyes were really boosting my confidence.`);
			txt(`He rascally grinned when his fingers felt my pussy and when he tentatively pushed one in. He slowly pushed his index finger deeper, then back, the further in, then he curled it inside me.`);
			kat(`Ahhhhhh!`);
			txt(`I blissfully moaned. He was briefly uneasy, my voice reminded him he was not fingering a random sexual object but the daughter of his neighbors @katalt. But not for long:`);
			npc(3, `You really want this, don't you?`);
			kat(`I really do${!mile.a_father_daddy ? ", @npc_3" : ""}!`);
			if(counter.father !== 2){
				txt(`With one hand he enthusiastically pulled down his sweatpants. The tight waistband grazed the tip of his erect cock, making it hilariously bounce up and down.`);
			}
			txt(`He pressed his dick against my slit.`);
			npc(3, `If you want it... I want it too...`);
			txt(`@npc_3 made his decision and was no longer giving a fuck about consequences. He pushed his cock inside me and began thrusting. ${!mile.a_burned ? "I could not help it but I had to compare. The biggest difference between him and his son was that he was more assured. @qaa was constantly afraid he will do something I will not like; his father was just enjoying himself, having a good time.  " : ""}`);
			kat(`Oh yes!`);
			if(mile.slut > 9){
				txt(`I was enjoying the sex very much. So much I completely forgot about @qaa and my evil plant.`);
			}else{
				txt(`I was not able to enjoy the sex. I had to constantly think about @qaa. What took him so long? Maybe he decided to go somewhere with his friends?`);
			}
			link(`Sex. `, 105);
			break;
			
		case 105: 
			emo("shock");
			if(counter.father !== 3){
				showLess(1,0);
			}

			kat(`Would you mind if I get on the top?`);
			if(mile.slut > 9){
				txt(`He was not bad but I was horny and I wanted to speed up a bit his millenial tempo.`);
			}else{
				txt(`I imagine the best, respective, the worst situation @qaa might catch us.`);
			}
			npc(3, `Not at all!`);
			txt(`He pulled out of me and laid down on the bed. I climbed on top of him and mounted him, riding him like a cowgirl. ${counter.father !== 3 ? "@npc_3 helped me out of my @upper and then I began moving. " : ""}`);
			npc(3, `Ahhhhh, yeah!`);
			txt(`He loved how I was bouncing on top of him.`);
			txt(`Then it came, in the corner of my eye I saw the door moving. I reacted immediately and loudly moaned:`);
			kat(`OH YES! FUCK ME HARDER${mile.a_father_daddy ? ", DADDY" : ""}!`);
			txt(`Only then I turned and was rewarded with an absolutely dumbfounded and appalled expression of @qaa who just returned back from school. As a power move, I did not stop riding his father even though he was staring right at us. To this day I regret I did not have my phone ready and could not take a picture before he turned and ran away.`);
			npc(3, `@QAA?!?`);
			txt(`His father was no less stunned.`);
			npc(3, `OH FUCK! Oh no! You told me he won't be coming!`);
			kat(`I thought he won't!`);
			txt(`I pretended to be surprised too.`);
			
			link(`Great sex, but I gotta go! `, 106, ()=> mile.a_father = 2);
			link(`Finish it. `, 106, ()=> mile.a_father = 3);
			break;
			
		case 106:
			emo("smug");
			
			if(mile.a_father === 2){
				mile.slut--;
				counter.father === 3 ? showLess(-2,-2) : showLess(-1, -2);
				
				txt(`I apologised and quickly got dressed. ${(wears.bra && counter.father !== 3) || wears.panties ? "I did not even bothered with putting on my underwear, I just grabbed it. " : ""} We got caught, my mission was complete. And still perplexed @npc_3 did not seem in the mood to continue either.`);
			
			}else{
				showAll();
				kat(`Well, that was awkward! Should we continue?`);
				npc(3, `You want to continue!?!`);
				kat(`Yeah, I don't like to leave my job unfinished!`);
				npc(3, `That does you credit but I'm not sure if I can continue after this! He must be so mad!`);
				kat(`I'm sure you can continue! And you know, not finishing doesn't make him any less mad.`);
				txt(`I smiled at him and teasingly wiggled with my hips.`);
				npc(3, `I had no idea you're such a horny girl!`);
				kat(`Horny woman!`);
				npc(3, `Oh right!`);
				txt(`So we continued fucking. He was a bit hesitant, thinking about what was his son thinking. Unlike him, I was pretty excited - the face of poor @qaa was making me crazily horny. ${mile.a_burned ? "I totally owned that little bitch! " : ""}`);
				txt(`I was riding his dick like I was mad and he could only helplessly moan. I basked in the bliss I felt until he grunted and cummed inside me.`);
				npc(3, `Ahhhhh... that felt so great!`);
				if(mile.slut > 4){
					kat(`How long it take you to be ready for the second round?`);
					npc(3, `Second round? Oh my! Please, @katalt, after what you did, probably several days.`);
					kat(`Awww!`);
					txt(`I climbed down and searched for my clothes.`);
				}else{
					kat(`Thanks! `);
				}
				npc(3, `Wait, @katalt, you're on a pill, right?`);
				kat(`You remember very soon! I would expect you to be more responsible!`);
				npc(3, `Are you, @katalt?`);
				kat(`No... but I have a bioimplant.`);
				npc(3, `Don't scare me!`);
				txt(`He shook his head and then began dressing too.`);
			}
				
			if(mile.a_burned){
				npc(3, `What the hell should I tell him?!? I cheated on his mother with his classmate right on his bed!`);
				kat(`He's a grown-up man, I'm sure he'll understand! Maybe I should try to talk with him and explain everything to him?`);
			}else {
				npc(3, `What the hell should I tell him?!? I cheated on his mother with his girlfriend on his bed!`);
				kat(`I'm sure he'll understand! I'll talk with him, no worries!`);
			}
			npc(3, `Thank you, @katalt! Having sex with you was amazing until... you know.`);
			
			link(`You're welcome! `, 110); 
			con_link(mile.slut > 8, "low slut", `The next time we should use the door chain. `, 110); 
			break;
			
		case 110:
			if(mile.a_burned){
				txt(`Once I left the apartment, I immediately reached for my phone. There has to be something I could do to make @qaa feel even worse.`);
				kat(`sorry! 😟`);
				kat(`you didnt meant to see that!`);
				kat(`but your dad is such hot guy! 😂`);
				kat(`and really knows how to please woman 😍`);
				kat(`your moms lucky gal 😉`);
				if(true){ //TODO - SAW HIS COCK? 
					kat(`i dont understand what went wrong with you 😕`);
					kat(`i mean the difference is huge`);
					kat(`HUGE if you know what i mean`);
					kat(`i mean his 🍆 is way biggger than yours!`);
					kat(`😂😂😂😂😂`);
					kat(`sorry i didnt want to be mean`);
					kat(`i know how horibly humiliating thats for you`);
					kat(`knowing that 😞`);
					kat(`you wont be ever half as good as him ☹️`);
				}
				kat(`id love to chat for longer but i have to take 🚿 to wash his 💦 from my 🐱`);
				kat(`😘 bye!`);	
				txt(`He did not respond. I wondered why he still had not added me to the ignore list.`);
			}else{
				if(mile.a_father === 2){
					txt(`I hoped to catch @qaa, I was right behind him. I found him sitting on a bench outside. I sat down next to him.`);
				}else if(mile.a_father === 3){
					kat(`where r u?`);
					txt(`I texted @qaa.`);
					qaa(`outside`);
					qaa(`r u done?`);
					qaa(`can i go home?`);
					txt(`I found him sitting on a bench outside. I sat down next to him.`);
				}
				qaa(`What the hell was that, @katalt?`);
				kat(`Me having sex with your dad? You recognised that, didn't you? I'm not sure what you don't understand.`);
				qaa(`Why.`);
				kat(`I wanted to experience sex with an older man.`);
				qaa(`But why my dad?`);
				kat(`He's like an older version of you. You should be pleased that I chose him!`);
				qaa(`You could ask! Or at least warn me!`);
				kat(`I guess I could.`);
				txt(`I impishly smiled at him.`);
				qaa(`I really didn't need to see my father banging my girlfriend!`);
				kat(`Why not? Come on, it did make you hard, didn't it?!`);
				qaa(`No!`);
				kat(`You could stay and watch? Maybe you'd learn a thing or two!`);
				txt(`I began rubbing his crotch.`);
				qaa(`@kat! We're on public!`);
				kat(`Or you could join! Tag teaming me would be a beautiful son-father bonding experience!`);
				qaa(`You're horrible!`);
				txt(`@qaa hopelessly sighed but I could feel how hard he was.`);
				kat(`You love me when I am horrible!`);
				//TODO SEX? 
				//TODO NO MOTHER? 
			}
			next();
			break;
	}
}



	





//QAA SHOPPING
export const a_shopping = (index)=> {
	showAll();
	const under_slut = 10;  //under allows hipsters and panties
	const socks_slut = 8;  //above stockings
	
	const lower_slut_min = 0; //below allows shortSkirt
	const lower_slut_max = 22; //above allows nanoSkirt
	
	const upper_slut_min = 5; //above deep cleavage 
	const upper_slut_max = 18; //above sheer top
	
	const back = function(){
		const extra = [
			"thong", 
			"gString", 
			"stripedHighSocks",
			"brightHighSocks",
			(mile.slut < 10 ? "miniSkirt" :  "microSkirt"),
			"niceSluttyTee",
			(mile.slut < 15 ? "miniSkirt" :  "sheerTee"),
			"beadNecklace",
		];
		extra.forEach(   key => PC.inventory.push( create(key) )   );
		removeEverything(PC, PC.inventory); 
		quickLoadOutfit();	
	}

	switch(index){
		default:
		case 101: 
				set.irl();
				placetime("Our Tower Block");
				quickSaveOutfit();
				
				qaa(`Hello!`);
				kat(`Hey!`);		
				txt(`I met with @qaa to hang out together.`);
					
				if(mile.a_dom > 0){
					qaa(`Where are we going?`);
					
					if(mile.clinic_boobjob > 0){
						kat(`I was thinking about going shopping.  ${mile.clinic_boobjob > 2 ? "None of my old tops fit because of my new huge boobs" : "None of my old tops fit properly since I got my breasts enlarged"}. And I have trouble keeping up with your constant demands.`);
					}else{
						kat(`I was thinking about going shopping. And I have trouble keeping up with your constant demands.`);
					}
					
					qaa(`Sure, that might be fun. I tag along and help you with my advice!`);
					kat(`Maybe you can also help me materially? You know, I'm kinda broke and you're constantly making up all those crazy rules about what I am or I am not allowed to wear, so...`);
					qaa(`Yeah, I understand what you mean. Sure, why not, it seems fair to make it up to you somehow. And I have to spoil my girlfriend.`);
					if(mile.a_girlfriend){
						kat(`Thanks!`);
					}else{
						kat(`I'm not your girlfriend but thanks!`);
					}
				
				}else{
					kat(`Where are we going?`);
					qaa(`I was thinking maybe we can go shopping? You would enjoy that, wouldn't you? Getting you cute new clothes?`);
					kat(`Yeah. ${mile.clinic_boobjob > 0 ? "I seriously need new clothes because none of my old tops fit my new breasts. " : ""} Although I'm a bit broke.`);
					qaa(`Well, I was thinking. I have finished some projects and I have extra money I don't need so I was thinking I might spoil you and pay for it? Also, I want to stop your excuses that you can't follow our rules because you don't have any suitable clothes.`);
					kat(`Thanks! It's a generous offer but I definitely deserve it!`);
					
				}
				
				link(`Continue. `, 102);	
				break;
				
	case 102: 
			showLess(1,2);
			remove("panties");
			remove("bra");
			remove("socks");
			
			txt(`We rode downtown and went shopping.`);
			
			if(mile.a_dom < 1 && mile.slut >= -2 && ext.stats.commando < 6){
				qaa(`Which shop we should check first?`);
				kat(`Maybe you can buy me some sexy lingerie?`);
				txt(`I winked at him.`);
			}else{
				txt(`I let @qaa in charge and I was not surprised the first store he wanted to go was a lingerie shop.`);	
			}
			
			if(mile.slut < under_slut){
				if(mile.a_dom < 0){
					txt(`I was checking lovely briefs but @qaa shook his head.`);
					qaa(`Come on, I want to buy you something really sexy, like ${choice("thong")} or ${choice("gString", "g-string")}!`);
					kat(`I'm going to wear them, not you, and I want something comfortable.`);
					qaa(`Too bad, I'm paying so I have the final word.`);
				}else{
					txt(`I was checking their ${choice("hipsters", "selection")} of lovely ${choice("panties", "panties")} but @qaa shook his head.`);
					qaa(`Come on, I want to buy you something really sexy, like ${choice("thong")} or ${choice("gString", "g-string")}!`);
				}
			}else{
				if(ext.stats.commando > 7){
					kat(`I'm not even sure if I need new underwear. Recently I'm not bothering to wear any ${mile.no_panties ? "(even when I'm not at the school)" : ""} and it makes me feel so free and slutty. `);
					txt(`I smirked but still followed him. @qaa smiled too and shook his head:`);
					qaa(`We really accidentally broke you, didn't we?`);
				}
				txt(`I was looking for something really erotic, to mercilessly tease his imagination.`);
				kat(`This ${choice("thong")} looks lovely, doesn't it? Or maybe I should buy the other ${choice("thong2", "one")}? Do you think ${choice("gString", "G-strings")} are too far? Or  ${choice("gString2", "not far")} enough?`);
				qaa(`Go wild! I want to buy you something really sexy!`);
			}
	
			
			if(mile.slut < socks_slut){
				kat(`Maybe I should ${choice("polyPantyhose", "get")} new ${choice("semiFishnetPantyhose", "pantyhose")} too?`);		
			}else{
				kat(`I think I should get a ${choice("polyStockings", "pair")} of complimenting ${choice("semiFishnetPantyhose", "stockings")} too, right?`);
			}
			qaa(`I would prefer you in  ${choice("stripedHighSocks", "cute")} high  ${choice("brightHighSocks", "socks")}.`);
			
			link(`Continue. `, 103);	
		
		{
			const polyPantyhose = create("polyPantyhose") ;
			const semiFishnetPantyhose = create("semiFishnetPantyhose") ;
			const polyStockings = create("polyStockings") ;
			const semiFishnetStockings = create("semiFishnetStockings") ;
			const brightHighSocks = create("brightHighSocks") ;
			const stripedHighSocks = create("stripedHighSocks") ;
			effects([
				{
					id: "initial",
					fce(){
						remove("socks");				
					},
				},
				{
					id: "polyPantyhose",
					fce(){
						wear(polyPantyhose);
					},
				},
				{
					id: "semiFishnetPantyhose",
					fce(){
						wear(semiFishnetPantyhose);
					},
				},	
				{
					id: "polyStockings",
					fce(){
						wear(polyStockings);
					},
				},
				{
					id: "semiFishnetStockings",
					fce(){
						wear(semiFishnetStockings);
					},
				},
				{
					id: "brightHighSocks",
					fce(){
						wear(brightHighSocks);
					},
				},
				{
					id: "stripedHighSocks",
					fce(){
						wear(stripedHighSocks);
					},
				},
			]);		
		}		
		
		{
			const panties = create("dividedPanties") ;
			const hipsters  = create("hipsters") ;
			const thong = create("thong") ;
			const gString = create("gString") ;
			const thong2 = create("thong") ;
			const gString2 = create("gString") ;
			effects([
				{
					id: "initial",
					fce(){
						remove("panties");				
					},
				},
				{
					id: "panties",
					fce(){
						wear(panties);
					},
				},
				{
					id: "hipsters",
					fce(){
						wear(hipsters);
					},
				},
				{
					id: "thong",
					fce(){
						wear(thong);
					},
				},
				{
					id: "thong2",
					fce(){
						wear(thong2);
					},
				},
				{
					id: "gString",
					fce(){
						wear(gString);
					},
				},
				{
					id: "gString2",
					fce(){
						wear(gString2);
					},
				},
			]);
		
						
		}		
		break;



	case 103:
			showAll();
			remove("lower");
			remove("upper");
	
			if(mile.slut < lower_slut_min){
				kat(`I like this ${choice("pleatedShort", "skirt")}.`);
				qaa(`Too boring, I want to see you in ${choice("pleatedMini", "something")} far ${choice("micro", "shorter")}.`);
				kat(`Wearing too short and slutty skirts is making me a little uncomfortable.`);
				qaa(`Come on, you have perfect legs, it would be such a shame to hide them.`);
				kat(`I'm not sure... I guess maybe this ${choice("mini", "miniskirt")} ?`);				
			}else if(mile.slut < lower_slut_max){
				kat(`I like this ${choice("mini", "skirt")}. It is definitely a short skirt, isn't it?`);
				qaa(`Yeah, it definitely is! Even though I can ${choice("micro", "imagine")} shorter ${choice("pleatedMicro", "skirts")}.`);
				kat(`I want something sexy but still ${choice("pleatedMini", "classy")}.`);
			}else{
				kat(`This ${choice("nano", "skirt")} looks fun!`);
				qaa(`Isn't that a bit too short?`);
				kat(`What? There are clothes too slutty for you?`);
				qaa(`Do you seriously want to wear it? Anybody can see your panties!`);
				kat(`Are you getting jealous?`);
				qaa(`What?! No! Just... there are many  ${choice("pleatedMini", "other")} nicer ${choice("micro", "skirts")} .`);
				kat(`But I want something  ${choice("pleatedMicro", "sexy")}!`);
			}
			txt(`He shrugged and we moved on to the next shelf.`);
			
			if(mile.slut < upper_slut_min){
				kat(`I guess you want me to get something sexy?`);
				qaa(`Of course.`);
				kat(`Maybe this ${choice("tubeTop", "tube top")}? Or ${choice("shrugTop", "this one")}?`);
				qaa(`Yeah. You should also try this ${choice("halterTop", "halter top")} and this ${choice("tee", "tee")}?`);
				
			}else if(mile.slut < upper_slut_max){
				kat(`What would you like me to try? A ${choice("halterTop", "halter top")} or a neat ${choice("tee", "tee")}?`);
				qaa(`Both sound good. ...whoa, did you know they are selling ${choice("deepTop", "tops")} and ${choice("deepTee", "tees")} with such absolute cleavages?`);
				kat(`Neckline all way to my belly button? How decadent!`);
			
			}else{
				kat(`I want to try this light ${choice("sheerTop", "top")}.`);
				qaa(`You better should only wear it with a bra, it's almost transparent.`);
				if(mile.slut >= lower_slut_max){
					kat(`Hah, you are jelaous! ${mile.a_cuck > 0 ? "Come on, other guys deserve to witness my tits too!" : ""}`);
				}else{
					kat(`What? There are clothes that are too slutty for you?`);
					qaa(`It doesn't look very classy.`);
					kat(`Are you getting jealous?`);
				}
				qaa(`No! I don't care whether you are going to buy slutty ${choice("sheerTee", "see-through")} tee or tiny ${choice("sluttyTop", "top")}. Whatever you like!`);
				txt(`@qaa denied he would prefer me to be a slut only for him.`);
			}
				
			txt(`Every time I had to go change to the fitting room and return to present him my new outfit.  ${mile.a_dom < 3 ? "He exploited the fact that he was paying and I could not say *no* to any of his dumb suggestions. " : "If I had known he was so eager to shop with me, I would have exploited it sooner. "} I posed for him and turned around, to let him see me from all sides.`);
			//mile.a_father > 1 ? Do you think your dad would like it? He very enjoyed ogling my ass before I seduced him. 
			
			link(`Accessories. `, 104);	
			
		{
			const tubeTop = create("sleevedTubeTop") ;
			const shrugTop = create("shrugTop") ;
			
			const halterTop = create("sluttyHalterTop") ;
			const tee = create("niceSluttyTee") ;
			
			const deepTop = create("deepHalterTop") ;
			const deepTee = create("deepTee") ;
			
			const sheerTop = create("transparentTubeTop") ;
			const sluttyTop = create("sluttyTubeTop") ;
			const sheerTee = create("sheerTee") ;
				
			effects([
				{
					id: "initial",
					fce(){
						remove("upper");
					},
				},
				{
					id: "tubeTop",
					fce(){
						wear(tubeTop);
					},
				},
				{
					id: "shrugTop",
					fce(){
						wear(shrugTop);
					},
				},
				{
					id: "halterTop",
					fce(){
						wear(halterTop);
					},
				},
				{
					id: "tee",
					fce(){
						wear(tee);
					},
				},
				
				{
					id: "deepTop",
					fce(){
						wear(deepTop);
					},
				},
				{
					id: "deepTee",
					fce(){
						wear(deepTee);
					},
				},
				{
					id: "sheerTop",
					fce(){
						wear(sheerTop);
					},
				},
				{
					id: "sluttyTop",
					fce(){
						wear(sluttyTop);
					},
				},
				{
					id: "sheerTee",
					fce(){
						wear(sheerTee);
					},
				},
			]);
			
			
			const nano = create("nanoSkirt") ;
			const micro = create("microSkirt") ;
			const mini = create("miniSkirt") ;
			const pleatedShort = create("pleatedShortSkirt") ;
			const pleatedMini = create("pleatedMiniSkirt") ;
			const pleatedMicro = create("pleatedMicroSkirt") ;
			effects([
				{
					id: "initial",
					fce(){
						remove("lower");
					},
				},
				{
					id: "nano",
					fce(){
						wear(nano);
					},
				},
				{
					id: "micro",
					fce(){
						wear(micro);
					},
				},
				{
					id: "mini",
					fce(){
						wear(mini);
					},
				},
				{
					id: "pleatedShort",
					fce(){
						wear(pleatedShort);
					},
				},
				{
					id: "pleatedMini",
					fce(){
						wear(pleatedMini);
					},
				},
					{
					id: "pleatedMicro",
					fce(){
						wear(pleatedMicro);
					},
				},
			])
		}		
		break;
		
	case 104:	
			emo("imp");
			kat(`What about jewellery? All girls love diamonds and gold.`);
			txt(`I teased him and he smiled back at me:`);
			qaa(`Ehhh... maybe next time?`);
			txt(`Her refused to spend all his savings to make me look beautiful but on our way to the counter he at least got me new  ${choice("earrings", "earrings")} and ${choice("necklace", "necklace")}. He paid for everything with his phone and smiled at me. `);
			
			if(mile.a_dom < 2){
				kat(`Why are you grinning?`);
				qaa(`I imagine how are you going to reward me.`);
				kat(`I thought you were doing it selflessly, you horny fiend!`);
				qaa(`Of course, I had nefarious side intentions.`);
				if(mile.slut > 8 || mile.c_shopping === 3){
					kat(`Well, fine. Let's do it.`);
					qaa(`Right now?!?`);
					kat(`Right here!`);
				}else{
					kat(`I'll have to figure something out.`);
					qaa(`Maybe we can do something right now, right here?`);
					kat(`You don't mean...`);
					qaa(`I do!`);
					txt(`I was stunned by his suggestion and hesitated for a moment.`);
					kat(`...well ...okay.`);
				}
			}else{
				kat(`Thanks.`);
				qaa(`You're welcome! Anything to make you happy!`);
				kat(`I think you deserve a reward!`);
				qaa(`How are you going to reward me? Give me just a hint, to have something to look forward to.`);
				kat(`I'm going to reward you right now!`);
			}
			
			txt(`I took his hand and we went back to the fitting room and hid in the last stall.`);
			
			link(`Squat down. `, 110);
			{
				let strip = "Pull down your @lowerPanties. "
				if(wears.skirt){
					if(wears.panties){
						strip = "Pull down your @panties. "
					}else{
						strip = "Hike up your @skirt. "
					}
				}
				con_link(mile.slut > 8, `low slut`, strip, 120);
			}
			{
				
				const earrings = create("crystalEarrings") ;
				effects([
					{
						id: "initial",
						fce(){
							remove("earrings");
						},
					},
					{
						id: "earrings",
						fce(){
							wear(earrings);
						},
					},
				]);
				
				const necklace = create("multiNecklace") ;
				effects([
					{
						id: "initial",
						fce(){
							remove("necklace");
						},
					},
					{
						id: "necklace",
						fce(){
							wear(necklace);
						},
					},
				]);
			}	
	
			break;
			
		
		case 110:
			effigy.qaa.showLess(-1, 0);
			emo("imp");
			log_sex("bj", "qaa");
			
			mile.a_shopping = 2;
			if(mile.slut > 20){
				mile.slut++;
			}else if(mile.slut < 9){ //TODO - forced sluttiness? - good or bad thing? 
				mile.slut++;
			}
			
			if(mile.c_shopping === 3 || mile.b_shoppping_sub || mile.b_shopping_dom === 3){
				txt(`I already knew what to do, I followed the same routine as with {mile.c_shopping === 3 ? "@qcc" ? "qbb"}.`);
			}
			qaa(`Are we really going to do this?`);
			if(mile.slut < 3){
				kat(`Yeah... I think we do...`);
				txt(`I was not especially confident. This was crazy, usually, I was not so daring and naughty. However, I really wanted to reward him with something outrageous.`);
			}else{
				kat(`What do you think?`);
			}
			txt(`I squatted down and helped him to unbuckle and pull down his pants. I tickled his balls and tenderly cuddled his cock before I took the tip in my mouth.`);
			qaa(`Oh yeah!`);
			txt(`It was exciting and scary, the stall was separated from the rest of the shop only by a black curtain. Moreover, it ended like ten centimeters above the floor, if anybody particularly prying bent down, he or she could spot our shoes. I had no idea what I would do if we got caught. It would be so awkward if anybody abruptly opened the curtain and asked us what are we doing. Well, it would be @qaa's job to come up with a good excuse - since my mouth was stuffed with a cock - but I doubted he would be able to figure out anything good.`);
			txt(`My hand fondled his thigh and my lips were squeezing his hard shaft, I leaned closer to take it deeper in my mouth and then backed again. `);
			qaa(`Fuck! I love the mirror! I can see you from behind, especially your sexy ass!`);
			txt(`@qaa was in a very good mood, smiled down at me and affectionately caressed my face.`);
			kat(`I hope I'll convince you to buy me new clothes more often!`);
			if(mile.a_dom < -1){
				qaa(`Don't be greedy and focus on the sucking!`);
				txt(`@qaa admonished me.`);
			}else{
				qaa(`You're definitely making it worth it!`);
			}
			txt(`I raised my tempo, the longer we were there, the bigger was chance somebody will discover us. @qaa warned me seconds before his pulsating cock exploded. I let him finish in my mouth, filling it with his semen.`);
			txt(`When I was done, I stood up and turned to check in the mirror there was no remaining cum. @qaa exploited this opportunity to grope my ass. After I fixed my hair, we left together, not caring if anybody suspected what we were doing.`);
			next(`Go home. `, back);
			break; 
			
		case 120:	
			mile.a_shopping = 3;
			if(mile.slut < 16) mile.slut++;
			emo("bliss");
			log_sex("sex", "qaa");
			
			{
				effigy.qaa.showLess(-1, 0);
				const strip = rollUpSkirt(3, 0);
				txt(strip);
			}
			qaa(`A... are we really going to have sex here?`);
			kat(`Yeah. Unless you're too scared to get hard.`);
			qaa(`You naughty slut! I'm hard already.`);
			txt(`@qaa was about to show, not just tell and pulled his pants down. I bent over and he squeezed and slapped my ass.`);
			if(mile.plugged){
				qaa(`You were wearing a buttplug this whole time?!?`);
				txt(`He realized and I just giggled, it was making my day a bit more entertaining.`);
			}
			kat(`What are you waiting for? Ram it inside me before somebody arrives!`);
			txt(`I heard him chuckle behind me. He pressed the tip of his cock against my slit, then grabbed me by my hips and thrust in. I bit my lower lip to not moan too loudly and began bouncing back against him.`);
			txt(`It was exciting and scary, the stall was separated from the rest of the shop only by a black curtain. Moreover, it ended like ten centimeters above the floor, if anybody particularly prying bent down, he or she could spot our shoes. I had no idea what I would do if we got caught. It would be so awkward if anybody abruptly opened the curtain and asked us what are we doing. Well, it would be @qaa's job to come up with a good excuse - since I was not in a good position to initiate conversation - but I doubted he would be able to figure out anything good.`);
			txt(`It was kinda cool to fuck in front of a large mirror. He could pound me from behind - the way I liked - but I still could see his face and reactions. He was laughing like a madman, clearly having time of his life. I could see my own face too which was far less funny, I looked so dumb when I was aroused and suppressing moaning. But he didn't think so:`);
			qaa(`You look so hot!`);
			kat(`If I had known this was all it takes to make you buy me new clothes, I would have done it sooner!`);
			if(mile.a_dom < -2){ //TODO
				qaa(`Don't tease me, you greedy bitch! Oh, I'm cumming!`);
			}else{
				qaa(`You should have told me this is all it takes to let me fuck you! Oh, I'm cumming!`);
			}
			txt(`He warned me and began ejaculating inside me, willing my pussy with his semen.`);
			kat(`Oh yeah!`);
			txt(`When we were done, I found tissues in my handbag to clean his cum and readjusted my clothes. After I fixed my hair, we left together, not caring if anybody suspected what we were doing.`);
			next(`Go home. `, back);
			break;

	}
}







export const chair_a_help = (index)=> {
	switch(index){
		default:
		case 101: 
			mile.chair_a_help = 1;
			emo(`sad`);
			placetime(`park`);
			
			txt(`We were sitting on a parkbench and eating dürüm kebab. @qaa was explaing something but I zoned out and was not listening. `);
			qaa(`What is wrong with you today, @kat?`);
			kat(`My life was destroyed by three perverted nerds.`);
			qaa(`You seem to be especially crestfallen today.`);
			txt(`I shrugged but then I reluctantly confessed:`);
			kat(`I'm worried about about class election.`);
			qaa(`Oh yeah. @ven bothered me that I should vote for her rather than for one of the pretentious, popular bitches. She was very convincing.`);
			kat(`Are you going to vote for her?`);
			qaa(`What? Of course not! I'm going to vote for you!`);
			kat(`...thanks.`);
			txt(`I dejectedly nodded.`);
			kat(`You and me, that's two votes. At least something, getting only one would be so humiliating.`);
			qaa(`Don't be so gloomy! I can try to get you some votes.`);
			if(mile.tom_help){
				kat(`I already asked @tom to help me. Although I'm not sure if I can trust him. He talks a lot but actual results...`);
				qaa(`That's good. @tom's a jock and he can approach a different target group than me.`);
			}			
			kat(`I would be very grateful for your help.`);
			qaa(`How much grateful?`);
			
			if(mile.cuck > 0 && mile.tom_help > 2){
				const deal = (()=>{
					if(mile.tom_help === 3) return "to suck his dick";
					if(mile.tom_help === 5) return "perverted kinky sex";
					if(mile.tom_help === 6) return "anal sex";
					return "to fuck him";
				})();
				
				kat(`Well, for his help I promised @tom ${deal}.`);
				qaa(`You what?!`);
				if(mile.slut < 14){
					kat(`I was desperate!`);
				}else{
					kat(`Yeah, I'm a whore, don't act so surprised.`);
				}
				qaa(`I think - as your boyfriend - I deserve more than a random dumbass.`);
				kat(`${!mile.a_girlfriend ? "You're not my boyfriend. But yeah" : "Yeah"} that makes sense.`);
				
			}
			
			txt(`I leaned closer and softly whispered into his ear:`);
			kat(`Extremely grateful. If you manage to get me reelected, I'll blow your mind and fulfill your darkest, most perverted fantasies!`);
			qaa(`@kat! I swear, I'll get you enough votes even if that would mean rigging the whole elections!`);
			if(!mile.a_locked){
				kat(`Oh my, are you already hard?`);
				qaa(`What?! No! ...maybe. You can't suggestively whisper into my ear and not expect my body to react!`);
			}
			next();
		break;
	}
}
				
						











/////////////////// WEEK 5 /////////////////
//THREESOME with ai
export const a_threesome = (index)=> { switch(index){
	default:
	case 101:
		{
			set.meta();
			
			let initiator = false; //aa initiates 
			if(mile.a_dom < -1) initiator = true;
			if(mile.a_dom <= 1 && mile.a_tripoint === 1) initiator = true; 
					
			
			if(initiator){
				placetime("@qaa's subreality");
				emo("suspect");

				txt(`I was wondering why @qaa wanted to meet me in the Metaverse. Since I was living in the same building as him, visiting me personally would probably be a less hassle than connecting to the Metaverse. `);
				txt(`Everything became clear when I materialised on a terrace of a lovely mediterranean villa. @qaa was not expecting me alone. `);
				qaa(`Hello, @katalt . `);
				ayy(`Hello me! `);
				txt(`He brought my AI clone! `);
				kat(`What is that supposed to mean? You want *her* to hang out with us? `);
				qaa(`Hang out and maybe more. `);
				
				if(mile.b_threesome_sub){
					txt(`Did @qbb tell him what he forced me to do with the AI? Or just implied it somehow? Or did @qaa come up with the idea of a threesome independently? That was not so improbable. Or did @ayy tell him? I looked at @ayy who was just cryptically smirking.`);
					kat(`Did you...`);
					ayy(`Told him about the lesbian stuff we did together? Yeah.`);
					txt(`She winked at me. So he probably did not know about the threeway I had with @qbb.`);
				}else if(mile.slut < 4){
					emo("shy");
					kat(`More!? You don't want me... with her... and you...`);
					txt(`I blushed when I realized what he wanted.`);
					qaa(`Why not? She told me you didn't mind having sex with her.`);
					kat(`W... what?! She did?! You traitor!`);
					txt(`My face went red. I certainly did not want people to know I was a huge sexual pervert who enjoyed doing forbidden things with her virtual alter ego.`);
				}else if(mile.slut > 17){
					kat(`You want to have a threesome?`);
					txt(`I instantly guessed and he nodded.`);
					qaa(`Yeah. That's the kinkiest thing I was able to come up with. I hoped you'll be impressed. She told me you enjoyed doing it with her.`);
					txt(`@qaa shrugged. It seemed he was concerned he was not able to keep up with my perverted imagination and tried hard to come up with something new.`);
					kat(`You told him?`);
				}else{
					kat(`You mean... like having a threesome? With you and... her?`);
					qaa(`Yeah! You don't mind having sex with me.. and don't mind having sex with her...`);
					kat(`What did you tell him, you traitor?`);
				}
				
				ayy(`I didn't realize our secret friendship with benefits was secret! And don't worry, I didn't tell him any details even though he wanted to hear everything.`);
					
				kat(`Aren't you a bit greedy? One @kat isn't enough for you? `);
				qaa(`It's just a suggestion. If you're uncomfortable with this, I can send @ayy away.`);
				
				if(mile.y_forced){
					ayy(`Why are you asking her?! That submissive bitch needs a firm hand, she'll do anything you tell her to do. Don't you, you pathetic slut?`);
					txt(`We were both taken aback by how coldly and authoritatively the AI sounded.`);

					link(`Yes, I'm a submissive slut. `, 120, ()=> mile.a_threesome = 4);
					link(`Don't be rude! `, 121, ()=> mile.a_threesome = 2);
					link(`Refuse to participate. `, 990);
					return; //BEWARE

				}else if(mile.slut > 15){
					ayy(`Of course, she does! She's a huge nympho physically incapable of refusing sex. I'm sure her pussy is already itching and her panties are wet!`);
					if(!wears.panties){
						if(mile.no_panties && mile.sub > 5){
							kat(`I'm not allowed to wear panties.`);
						}else{
							kat(`You are wrong! I'm not wearing any panties!`);
						}
					}else{	
						kat(`I would be perfectly capable of refusing sex... if I wanted... I just usually don't...`);
					}
					
					if(mile.sub > 15){
						ayy(`She should be begging to be allowed to join us. @qaa is an awesome stud and you are barely worthy of his cock! I'm serious, beg or fuck off, we can have plenty of fun even without you!`);
						link(`Please, can I join you? `, 122, ()=>{
							mile.sub++;
							mile.a_threesome = 4;
						});
					}else{
						ayy(`Stop making excuses, you little floozie. Admit how huge, needy slut you are and how much you need @qaa's cock inside you!`);
						link(`I'm a huge, horny slut and I desperately want to have sex with you.  `, 122, ()=>{
							mile.slut++;
							mile.a_threesome = 4;
						});
					}
					link(`Slow down! `, 121, ()=> mile.a_threesome = 2);
					link(`Refuse to participate. `, 990);
					return; 

				}else if(mile.sub > 10){
					ayy(`It's a waste of time asking her! She'll do anything you'll tell her. ${mile.y_sub > 0 ? "Would you believe she actually allows her AI copy to push her around and dominate her? " : "When we are playing together, she pretends to be tough and I entertain her but otherwise she's a submissive slut who loves to be dominated. "}`);
					qaa(`Oh my, is that true?`);
					ayy(`How embarrassing, right?`);
					txt(`They were laughing at my expense.`);
					kat(`Come on!`);
					ayy(`Don't play coy. Just admit you just crave to become our fucktoy!`);
					con_link(mile.sub > 20, `low slut`, `Fine! I'm a needy, horny slut and I want to be used! `, 120, ()=> mile.a_threesome = 4);
					link(`Yeah... I kinda do... `, 120, ()=> mile.a_threesome = 3);
					link(`I would appreciate more respect. `, 123, ()=> mile.a_threesome = 2);
					link(`Refuse to participate. `, 990);
				}else{
					ayy(`Please, don't send me away!`);
					if(mile.sub < 3){
						kat(`Are you trying to steal my man? `);
						ayy(`Oh no! I don't! And I swear I'll be a good little pet. I don't want you to punish me! Or do I? `);
					}
					txt(`The @ayy was making puppy eyes at me. I was very familiar with that cheap trick but being on the receiving end and seeing my cute face staring at me felt very eerie. I did not have a full grasp of how dreadful power I wielded.`);
					link(`I'm in! It's gonna be fun! `, 124, ()=> mile.a_threesome = 1);
					link(`Fine. `, 124, ()=> mile.a_threesome = 1);
					link(`Refuse to participate. `, 990);
				}
			
			}else{
				placetime("Metaverse, my subreality");

				txt(`Dating a shy and inexperienced guy was far more exciting than I would anticipate. He did not take sex with me for granted,  he was absolutely amazed by everything I did and so grateful when I allowed him to touch my body. `);
				//He was always ready to caress and worship me.

				if(mile.slut < 5){
					txt(`I wanted these moments of wonder when I surprised him with something new and exciting to last as long as possible. But even though I had far more sexual experience than him, I was not really as sexually savvy as I pretended to be and coming up with new stuff was becoming a bit more challenging.`);
				}else{
					txt(`I wanted these moments of wonder when I surprised him with something new and exciting to last as long as possible. Fortunately, I had far more sexual experience than him and my kinky mind had no trouble coming up with new things.`);
				}

				if(mile.b_threesome_dom || mile.b_threesome_sub){
					txt(`My last idea was especially kinky. What if I did with him what I did with @qbb? Well, not exactly the same thing, but something in similar directions.`);
				}else{
					txt(`My last idea was so crazy and perverted that I instantly discarded it. However, the more I was thinking about it, the more I was tempted to try it. I was sure @qaa would enjoy it but it would be such a weird experience....`);
				}

				qaa(`Hello! Why did you want to meet in the Metaverse? `);
				txt(`@qaa questioned me when he materialized on a terrace of a lovely Mediterranean villa. We lived in the same building and it might be simpler for him to just walk to my door.`);
				kat(`I prepared something special that cannot be done in real life. I'm not here alone!`);
				ayy(`Hello, @qaa! `);
				qaa(`You're here with the AI? I don't understand. `);
				txt(`His eyes went from me to my AI twin then back at me then back at her. @qaa suspected what I was up to but he could not believe his luck.`);
				if(mile.slut > 9 && mile.sub < 6){
					kat(`I demand a threesome. Do you think you can handle fucking us both? `);
					txt(`I announced and his eyes brightened up:`);
					qaa(`I will or die trying!`);
				}else{
					kat(`Well, I was thinking about spicing our sex a little...`);
					qaa(`Sex with you is already spicy enough! Like cayennes or jalapeños!`);
					kat(`...and my clone offered her help. What do you think? `);
					qaa(`It's a bit weird... and I'm a bit overwhelmed by so much beauty... but I'd rather die than to miss this chance! `);
				}
				ayy(`Awesome! I reacted the same way when the other @kat told me she wants to involve other people in our kinky sexual games.`);
				qaa(`W.. what?! You did... things... together?`);
				ayy(`Just a bit of lesbian fooling around.`);
				kat(`Shut up! You're such a tattletale.`);
				ayy(`Sorry, I didn't realize our friendship with benefits was a secret.`);
				qaa(`Well, I'm honored to be allowed to join you!`);
			
				mile.a_threesome = 11;
				link(`Kiss @qaa. `, 200, ()=> counter.kissai = false);
				link(`Kiss AI. `, 200, ()=> counter.kissai = true);
			}
		}
		break;

	//forced or high sub
	case 120:
		mile.sub++;
		mile.a_dom--;
		emo("unco"); //TODO
		txt(`I admitted and blushed. It was so humiliating and twisted but seeing my clone mean and dominant was always making me horny. @qaa was shocked, then amazed and then he smirked:`);
		qaa(`I see. You'll do what I tell you, @kat, and I won't tolerate any back talking! Kiss your twin sister, I want to see you two sluts making out!`);
		txt(`He ordered me.`);
		link(`Kiss AI. `, 200, ()=> counter.kissai = true);
		break;
	
	//resist AI being too mean
	case 121:
		emo("anoy"); 
		if(mile.sub > 5) mile.sub--;
		qaa(`Why are you so mean?!`);
		txt(`@qaa scolded her too and the AI had to defend herself:`);
		ayy(`I swear she loves when I degrade her and call her names when we are alone. She's just shy to admit it in front of you!`);
		qaa(`@kat?`);
		kat(`She's mostly exaggerating.`);
		txt(`I awkwardly shrugged.`);
		link(`Kiss @qaa. `, 200, ()=> counter.kissai = false);
		link(`Kiss AI. `, 200, ()=> counter.kissai = true);
		break;
		
	//submit to mean AI 
	case 122:
		mile.a_dom--;
		emo("unco");  //TODO
		txt(`@qaa began laughing:`);
		qaa(`Oh my! Why are you so mean on poor @kat?`);
		ayy(`You're going too easy on her! I know her intimately and I can tell you that deep down she's enjoying this, she's just too ashamed to admit it. She know's she's just a trashy slut who doesn't deserve anything better.`);
		qaa(`You are so evil!`);
		txt(`They talked about me and laughed like I was not even there.`);
		kat(`Come on!`);
		qaa(`Shut up and kiss your twin sister!`);
		txt(`@qaa grinned and teasingly suggested.`);
		link(`Kiss AI. `, 200, ()=> counter.kissai = true);
		break;

	//respect
	case 123:
		mile.sub--;
		emo("anoy");  //TODO

		ayy(`Hahaha! She thinks she deserves any respect!`);
		qaa(`Sorry, @kat! It's just a banter! Of course we respect you.`);
		ayy(`I don't!`);
		txt(`@ayy continue to tease me.`);
		qaa(`I do. And I would respect you even more if agree with the threesome.`);
		txt(`@qaa desperatelly looked at me, his eyes full of hope.`);

		link(`Kiss @qaa. `, 200, ()=> counter.kissai = false);
		link(`Kiss AI. `, 200, ()=> counter.kissai = true);
		break;

	//fine 
	case 124:
		emo("imp"); 
		ayy(`Yay!`);
		qaa(`You're the best girlfriend ever!`);
		if(	mile.a_girlfriend ){
			kat(`Why are you still doubting about that?!?`);
		}else{
			kat(`Ehh... I wouldn't say girlfriend... but yeah, I'm the best!`);
		}
		link(`Kiss @qaa. `, 200, ()=> counter.kissai = false);
		link(`Kiss AI. `, 200, ()=> counter.kissai = true);
		break;


		
	case 200: 
		emo("horny"); 
		if(counter.kissai){ //kiss ai
			txt(`We deeply kissed. I did not see @qaa's reaction but I was sure he loved it. AI was very covetous and her eager hands were instantly trailing all over my back and ass. I was less rash, fully concentrating on the perfect kiss, lightly touching her cheek. Kissing girls was a little bit different than kissing boys and I discovered I ${mile.lesbian > 2 ? "loved" :  "kinda liked"} it.`);

			txt(`@qaa was suddenly behind me, moved my @hair away and began kissing my neck. My body was sandwiched between them and two pairs of hands were ${mile.sub > 15 ? "aggressively" : "gently"} caressing and fondling my whore body. I rather enjoyed being at the center of attention.`);

		}else{ //kiss @qaa
			txt(`I passionately kissed @qaa and he kissed me. His hands were covetously ridding all over my back and ass while I was affectionately tousling his hair.`);
			ayy(`Awww! You look like such a cute couple!`);
			txt(`The AI was enthused. She did not want to be left behind and quickly joined the action. She tightly embraced us. When we broke up our kiss, she first kissed me and then she began making out with @qaa. It so was uncanny to see him kissing me.`);
		}


		if(	mile.a_threesome === 4 || mile.a_threesome === 3 || mile.sub > 12	){
			if(counter.kissai){
				txt(`We continued making out. @qaa was vigorously rubbing my @pussy through my clothes while I could feel that AI was unzipping his pants behind my back. When she was done, they turned me and pushed me down on my knees. I was facing @qaä́'s rock-hard cock, having two @kats at his disposal was definitely turning him on. I teasingly brushed my cheek against his manhood.`);
			}else{
				txt(`Together we were making out. @qaa was vigorously rubbing my @pussy through my clothes while I could feel that AI was unzipping his. When she was done, they pushed me down on my knees. I was facing @qaä́'s rock-hard cock, having two @kats at his disposal was definitely turning him on. I teasingly brushed my cheek against his manhood.`);
			}

			if(mile.a_threesome === 4 || mile.a_threesome === 3 || mile.a_dom < -2){
				qaa(`What are you waiting for, you little slut? Start sucking!`);
				txt(`@qaa ordered.`);
			}else{
				ayy(`Go on, @kat! Start sucking, you little slut!`);
				txt(`@ayy laughed.`);
			}
			kat(`I opened my mouth and engulfed his cock. I was giving him a blowjob while he was making out with the virtual me. It was pretty demeaning that it was the real me who was designated into the cocksucking role. But eventually, even the fake @kat knelt and joined me.`);


		}else{
			txt(`${counter.kissai ? "@ayy released me and let @qaa to make out with me for a change. She" :  "Then @ayy let me have @qaa back and she"} dropped down on her knees. She began unzipping @qaa pants while her shoulder was very intentionally poking and rubbing against my lower belly. When @qaa's pants fell down to his ankles, @ayy slipped between us and began taking care of @qaa's dick. I was sure it was rock-hard, having two @kat's at his disposal surely had to turn him on.`);

			txt(`@qaa had a very funny blissful expression, he could enjoy both kissing my lips and feeling them wrapped around his cock. We were making out while @ayy was giving him a blowjob but then she suddenly grabbed my arm and pulled me down and made me join her kneeling in front of him.`);
			
		}
		
		link("Continue.  ", 201);
		break;
		
	case 201: 
		txt(`I and my exact copy were enthusiastically sharing his cock, licking his hard shaft, each from one side, our tongues were touching and I had to giggle when I realized how much fucked up experience it was. @qaa was moaning above us, his left hand was caressing my hair and his right one was patting the AI. He was loving every moment:`);
		qaa(`This is like a dream come true! I still can't believe it!`);
		txt(`He was musing while we were giving him the best blowjob of his life.`);
		
		if(mile.sub < 8){
			txt(`The AI let me be in charge and take his dick in my mouth. I was sucking, twirling my tongue over the glans, moving my head forward and back. She was focusing on his balls and she was doing a tremendous job. `);
			txt(`When I wanted a break, she keenly replaced me. I played with her hair and was quite horrified and embarrassed when I finally saw how dumb my cock-sucking expression looked. Still, it was pretty hot and I did not resist and stroked my pussy. `);
		}else{
			txt(`The AI arrogantly usurped the whole thing, hungrily took his cock in her mouth and began sucking him, moving her head forward I back. My duty was to focus on his balls.`);
			txt(`Luckily @qaa tapped on her head and reminded her, we should share. Now it was my time to suck him. The AI was kneeling behind me, with one hand messing my hair, with other caressing my throbbing pussy and encouraging me:`);
			if(mile.a_threesome === 4 || mile.a_threesome === 3){
				ayy(`Come on, suck him harder!  You love his cock in your mouth so much, don't you, you horny bitch? On your knees, sucking cocks, that's where you belong, don't deny it! `);
			}else{
				ayy(`Come on, you little floozie! Suck him harder! You were born for this! `);
			}
		}

		qaa(`Fuck yeah!`);
		txt(`@qaa groaned and began ejaculating in my mouth. Before I was able to swallow his load, @ayy hungrily kissed me and we shared his cum. Amazed @qaa shook his head when he saw how shameless we were.`);
		
		link("Continue.  ", 202);
	break;



	case 202: 
			emo("horny"); 
			showLess(-1,-1);
			effigy.ayy.showLess(-1,-1);
			effigy.qaa.showLess(-1,-1,1); //socks

			txt(`We moved inside, to the bedroom, and on our way there we were dropping our clothes until we were completely nude. I assumed @qaa will need a moment to regain his strength but he was so horny he was hard again.`);
			
			if(mile.a_dom > 2 || mile.sub < 0){
				kat(`@qaa? Fake AI? You'll now make me cum, harder than I ever cummed before. And trust me, you don't want to disappoint me!`);
				txt(`I ordered them and they enthusiastically nodded.`);
				ayy(`Yes, @mis!`);
				qaa(`Yeah, at your service!`);
			}else if(	mile.a_threesome === 4 || mile.a_threesome === 3 || mile.sub > 10	){
				if(mile.a_dom < -2){
					qaa(`Get on the bed, slut, I'm going to fuck your brains out!`);
					ayy(`Yeah, bitch, we'll make you cum harder than you ever cummed before!`);
				}else{
					ayy(`Get on the bed, slut, @qaa is going to fuck your brains out!`);
					txt(`@ayy madly laughed and @qaa nodded even though he seemed a bit less sure:`);
					qaa(`Y..yeah! I'll fuck you hard!`);
				}
			}

			txt(`I laid back on the satin sheets and let them take care of me. @qaa spread my legs and climbed between them. He poked with his cock against my puss and then he slowly and gently entered me. My moans were cut off by fake @kat's hot kisses. It felt fantastic. @qaa was grunting, clenching my thighs a thrusting deeper, his every strong thrust triggering intense waves of pleasure. On the other hand, the AI was composed and tender even when instead of my lips she began kissing and sucking my @nipples.`);

			if(	mile.a_threesome === 4 || mile.a_threesome === 3 || mile.sub > 15	){
				txt(`The whole experience was very intense and vivid. I was fully theirs, allowing them to do with my gorgeous body anything they wanted. And it felt so amazing to submit to their perverted imagination. AI eventually decided she wanted a taste of my pussy, kissed my belly button and then moved even lower. She was licking my clit while @qaa was fucking me.`);
			}else{
				txt(`The whole experience was very intense and vivid. The wholesome knowledge two people were willing to do anything to make me feel good was pushing the whole experience to another level. I felt like a queen of my small personal harem. AI eventually decided she wanted a taste of my pussy, kissed my belly button and then moved even lower. She was licking my clit while @qaa was fucking me.`);
			}
			
			txt(`Their joint effort made me powerfully climax. I shouted incoherently as for one fleeting ecstatic moment I was one with the universe. Very satisfied @qaa flooded my spasming slit with his cum.`);
			
			txt(`Then grabbed my tit and leaned closer and kissed me while my cum-craving doppelgänger first cleaned his cock and then began lapping my now extremely sensitive pussy.`);
			
			qaa(`Satisfied, @katalt?`);
					
	//TODO - SHOULD BE LONGER - FUCKING AI TOO? 
			next("Oh yeah! ");
			break;


		case 990: 	
	//TODO - should be longer 
			emo("angry"); 
			kat(`You can't be serious! You really expect me to have sex with you and the AI?!`);
			qaa(`I'm sorry, @kat, I didn't mean to offend you!`);
			kat(`I'm not interested!`);
			next("Leave. ");

			break;
			
	}
}






//STRAPON (PEGGING)
export const a_strapon = (index)=> { 
const used = mile.sas_strapon; //TODO 
switch(index){
	default:
	case 101:
		set.irl();
		placetime(`my place`);

		txt(`@qaa visited me and we hung out at my place, just chatting about school and stuff. He seemed a bit fidgety. Obviously, he was very horny and wanted to have sex with me, however, he did not want me to think he was there just to fuck me. He hoped I will bring it up but I intentionally did not ${mile.slut > 14 ? "(even though I was pretty horny)" : ""}, just to mess with him.`);
		kat(`Are you going to stay for long?`);
		qaa(`Why? Do you have to prepare for school or play the game? I'm sorry if I'm wasting your time.`);
		kat(`No, I'm just horny and I was thinking about masturbating. Well, or if you don't want to leave, I guess you could watch.`);
		qaa(`Oh! ...but ...I'm here if you wanted to do something with me.... ...or if you don't want to fuck I can at least eat you out or...`);
		kat(`Oh! I thought you came just to chat.`);
		txt(`Then he frowned, sudden realization evident on his face.`);
		qaa(`Wait a moment! You're just playing with me!`);
		kat(`Hahaha! You're such an easy target! Don't be a coward and admit you came just because you ${mile.slut > 13 ? "craved to stick your cock in my little pussy" : "wanted to bang me"}.`);
		qaa(`No! I love you and enjoy your company even when we are not having sex! Although I wouldn't mind having sex with you, if you're so horny.`);

		link(`Continue.  `, 102);
		break; 

	case 102:
		kat(`Fine. What do you want to do?`);
		qaa(`Whatever you want! I'm pretty open-minded!`);
		kat(`You say that like you were doing me a favor but I all hear is: I'm too lazy and boring to come up with anything!`);
		qaa(`Sorry!`);
		kat(`Don't constantly apologize and tell me your darkest sexual cravings!`);
		qaa(`I don't want to, you wouldn't like them.`);
		txt(`He made me curious.`);
		kat(`Come on, tell me! I want to hear it! I don't promise we'll do it - in case, it's something too weird, I won't let you pee on my face or stick ginger in my bumhole - but I will consider it.`);
		qaa(`I don't know! I'm unable to recall any dark cravings right now! Maybe... I've seen a video of a guy roughly fucking a Latina chick from behind and making her suck a dildo and it looked pretty hot.`);
		txt(`That didn't seem particularly exciting but I was in a good mood.`);
		kat(`Well, I guess maybe we can try that...`);
		
		link(`Continue.  `, 103);
		break; 

	case 103:
		txt(`I opened the lowest drawer where I was hiding my sex toys. @qaa was looking over my shoulder and he was amazed.`);
		qaa(`Whoa!`);
		{
			const {tex, toys} = sex_toys();
			txt(tex);
			//2 start + 2 tasks + 1 strapon
			if(toys > 6){
				if(mile.dildo_task){
					qaa(`I see you bought a few new things.`);
				}else{
					qaa(`That's a lot of sex toys!`);
				}
			}
		}
		kat(`Yeah, I started a collection.`);
		qaa(`And what is this!?! `);
		txt(`He pointed towards the strap-on.`);
		kat(`Is that a rhetorical question?`);
		qaa(`What? Yeah... I just...  I didn't imagine you owning something like this. So you are doing... kinky bisexual things with other girls?`);
		
		link(`Yeah!  `, 104, ()=> counter.strapon = 1);
		link(`Maybe? `, 104, ()=> counter.strapon = 2);
		if(!used){
			link(`Not yet. `, 104, ()=> counter.strapon = 3);
		}
		link(`It isn't for girls. `, 104, ()=> counter.strapon = 4);
		break; 

	case 104: 
		emo(`imp`);
		if(counter.strapon === 1){
			if(mile.slut < 7) mile.slut++;
			kat(`Damn yeah!`);
			txt(`I claimed with no shame${used ? "" : ", even though the truth was I so far did not have an opportunity to try it. "}`);

		}else if(counter.strapon === 2){
			kat(`Maybe!`);
			txt(`I evasively answered and smirked at him. ${used ? "" : "It sounded cooler than the truth that so far I did not have an opportunity to try it. "}`);
		}else if(counter.strapon === 3){
			txt(`I admitted the truth.`);
			kat(`But I'm looking for an opportunity to finally try it.`);
			qaa(`Nice!`);
		}

		if(counter.strapon === 4){
			kat(`It isn't to do kinky things with girls...`);
			txt(`I winked at him.`);
		}else{
			kat(`Of course, it doesn't have to be used only to have sex with girls...`);
		}
		qaa(`What do you mean? ...oh? ...OH!`);
		txt(`I lifted the strap-on.`);
		kat(`Do you want to give it a try?`);
		qaa(`W... what!?!`);

		if(mile.a_anal > 0){
			kat(`I mean, I already let you fuck my ass. I think it would be fair if you let me do the same!`);
		}else{
			kat(`It might be fun if you let me fuck your ass!`);
		}
		
		qaa(`...you're making fun of me again!`);
		txt(`He accused me with relief in his voice.`);
		
		link(`Haha, I making fun of you.  `, 105);
		link(`I'm serious!  `, 110);
		break;

	case 105: 
		emo(`happy`);
		kat(`You should see your face!`);
		txt(`I loudly laughed`);
		qaa(`Oh fuck! You really terrified me!`);
		kat(`Although... now when we are discussing it... it actually isn't such a bad idea...`);
		qaa(`@kat!`);
		
		link(`Push him.  `, 110);
		link(`Have normal sex.  `, 106);
		break;
	
	case 106: 
		mile.into_pegging--;
		mile.a_strapon = -1;
		txt(`We had normal sex and we enjoyed it.`);
		next();
		break;

	case 110: 
		emo(`imp`);
		qaa(`You... you want to stick that... in my ass?!?`);
		if(mile.anal > 2){
			kat(`Yeah! What's weird about that? People are fucking my ass all the time and I love it! You'll enjoy it too!`);
		}else if(mile.anal > 1){
			kat(`Sure! It's not weird, I was fucked in my ass and it's fine!`);
		}else{
			 if(mile.had_anal){
				kat(`I understand you. I was concerned about anal sex too. But then I tried it and I enjoyed it very much!`);
			}else{
				kat(`Yeah! Why not? What's weird about it? Why it should be the girl who gets fucked into her ass?`);
			}
		}
		qaa(`But I'm a guy!`);
		kat(`Come on, it's the current year, who cares! I'm sure you'll love it!`);
		txt(`I brushed with the dildo against his body.`);
		qaa(`I'm not sure about this, @kat. M... maybe the next time?!?`);
		
		link(`Don't worry, it will be fine!  `, 111, ()=> mile.a_strapon = 1);
		link(`Don't be a chicken!  `, 111, ()=> mile.a_strapon = 2);
		con_link(mile.sub < 3, `too submissive`, `I want it!  `, 111, ()=> mile.a_strapon = 3);
		con_link(mile.sub < -3, `too submissive`, ` Shut up, you'll do what I tell you to do! `, 111, ()=> mile.a_strapon = 4);
		break;
	
	case 111:
		mile.into_pegging++;
		mile.a_dom++;
		effigy.qaa.showLess(0,0,1);
		emo(`relax`);
		showLess(1,2,1);
		
		if(mile.a_strapon === 1){
			kat(`I will be very careful and stop if you don't enjoy it. But I'm sure you'll love it!`);
			txt(`I soothed him, trying to chase away his anxiety.`);
			qaa(`Do you swear?`);
			kat(`Yeah!`);
			qaa(`...okay. I will do it.`);
		}else if(mile.a_strapon === 2){
			qaa(`I'm not a chicken!`);
			txt(`@qaa objected when I mocked him.`);
			kat(`You are! A real alpha man wouldn't be afraid to get pegged!`);
			qaa(`I'm not afraid... I just...`);
			kat(`You are blackmailing me, claiming I should enjoy expanding my horizons but when you get the opportunity to push beyond your comfort zone and experience something new, it's suddenly a huge problem! Where is your adventurous spirit? Dare to be a bit kinky!`);
			qaa(`...fine!`);
		}else if(mile.a_strapon === 3){
			mile.a_dom++;
			mile.sub--;
			
			kat(`But I want it! Why don't you let me to try it? You are blackmailing me, claiming I should enjoy expanding my horizons but when you get the opportunity to push beyond your comfort zone and experience something new, it's suddenly a huge problem!`);
			qaa(`You don't understand...`);
			kat(`I understand too well! Don't be afrad! Do it for me! Please!`);
			qaa(`Okay... I'll do it if you want it so much...`);
			
		}else if(mile.a_strapon === 4){
			mile.a_dom++;
			mile.sub--;

			kat(`I have had enough of your back-talking! You are blackmailing me, claiming I should enjoy expanding my horizons but when you get the opportunity to push beyond your comfort zone and experience something new, it's suddenly a huge problem! No way. I'm going to fuck your ass whether you want to or not! Do you fucking understand?`);
			txt(`He stared at me but then hesitantly nodded:`);
			qaa(`...yes, @kat.`);

		}

		kat(`Good! Get naked!`);
		txt(`I began stripping too and then attached the harness to my hips. I was getting pretty excited.  ${mile.y_anal === 3 ? "Having sex with guy in real life was different that fucking a program in virtual reality. " : ""} ${mile.b_strapon ? "And I hoped it will be as fun as ass-fucking @qbb. " : ""}.`);

//TODO - didn't work
		if( sex_toys().strapons > 1 ){
			txt(`I ${mile.sub < -3 ? "seriously" : ""} considered being a nasty bitch and choosing ${strapon(mile.strapon)}. However, @qaa was already nervous and I did not want to push things too far so I picked the smaller one.`);
		}
		txt(`@qaa reluctantly moved on all fours.`);
		qaa(`Do you know what are you doing?`);
		kat(`Yeah!`);
		txt(`I lied.`);
		qaa(`Please, be careful!`);

		con_link(mile.a_strapon !== 4, `was mean before`, `Calm down, I'll be gentle. `, 112, ()=> mile.a_strapon_gentle = true);
		con_link(mile.a_strapon !== 1, `was nice before`, `Haha! I'll totally wreck you! . `, 112, ()=> mile.a_strapon_gentle = false);
		break;

	case 112: 
		emo(`focus`);
		if(mile.a_strapon_gentle){
			qaa(`O-okay. ...I trust you.`);
			kat(`If you won't like it, just tell me and I'll stop.`);
		}else{
			kat(`I'm going to destroy your poor ass so badly you won't be able to walk straight for days! Hahaha!`);
			qaa(`This isn't funny, @kat! You're terrifying me!`);
		}
		txt(`I slapped his ass.`);
		kat(`Let's start!`);
		txt(`I poured lube between his asscheeks and then I inserted a finger inside.`);
		qaa(`Whoa!`);
		kat(`And we are only getting started!`);
		txt(`I was anally fingering him, gradually getting him ready for my big cock. He was visibly getting aroused. Finally, I positioned myself behind him and rubbed the tip of the dildo against his back entrance. @qaa gasped:`);
		qaa(`Ahhhhh!`);
		kat(`Save it, I haven't even started pushing!`);
		txt(`I slapped his ass and then slowly thrust.`);
		qaa(`Oh fuck! Oh fuck!`);
		
		link(`Calm down and try to relax!  `, 113);
		break;
	
	case 113:
		emo(`imp`);
		txt(`He was whining but did not ask me to stop as I pushed further, watching the plastic cock disappear inside his body. `);
		if(mile.a_strapon_gentle){
			kat(`Are you okay?`);
			qaa(`...I ...I think so.... fuck!`);
		}else{
			kat(`Does it hurt, you little bitch?`);
			qaa(`...I... ...I can take it!`);
		}
		kat(`Good boy! You're taking my cock like a champ!`);
		qaa(`HHh!`);
		kat(`Oh yeah! Let's go!`);
		if(mile.a_strapon_gentle){
			txt(`I slightly pulled the strap-on out and then gently shoved it back into him, trying to give him chance to get used to being stretched. But when he was not protesting, I began thrusting harder, fucking him more energetically.`);
		}else{
			txt(`I slightly pulled the strap-on out and then forced it back into him. I instinctively craved to ram the dildo in as hard as I could but I did not want to make him cry and quit so I was restraining myself. I thrust carefully and only gradually raised the tempo.`);
		}
		qaa(`Ohgghh! Ahhhg!`);
		txt(`His grunting sounded like he was enjoying it. I leaned closer so I could whisper into his ear:`);
		kat(`You love getting your ass fucked by a girl, don't you, you perv?`);
		txt(`I reached under his body, roughly grabbed his hard cock and began pumping.`);
		qaa(`Ahhhh!`);
		txt(`His body tensed when he began shooting his cum all over my sheets and then collapsed down. I pulled my plastic cock out of his ass and laid down next to him:`);
		if(mile.a_strapon_gentle){
			kat(`Are you okay?`);
			qaa(`Hhh.... yes.`);
			kat(`Is your bum sore?`);
			qaa(`Yes!`);
			kat(`Did you enjoy it?`);
			qaa(`...yes.`);
		}else{
			kat(`Are you crying?`);
			qaa(`No!`);
			kat(`Did I utterly destroy your asshole?`);
			qaa(`...it feels like it.`);
			kat(`Did you enjoy it?`);
			qaa(`...I ...I think I did...`);
		}
		next();
		break;
}}







export const a_chastity = (index) => {
	switch(index){
		default:
		case 101: 
			set.irl(); 
			placetime("@qaa's place");
			emo("anoy"); 
		
			txt(`We were hanging out at @qaa's place - neither of us was in the mood to go out and do something adventurous. @qaa suggested watching movies and I agreed. He opened the laptop and to his embarrassment, the browser was showing the last visited page with nude ladies performing X-rated activities. @qaa hastily closed the tap.`);
			kat(`What was that?`);
			qaa(`Nothing!`);
			kat(`Were you watching porn?`);
			qaa(`M... maybe?`);
			kat(`You're such a pathetic, horny pervert!`);
			qaa(`Every guy watches porn, there's nothing wrong with that!`);
			kat(`It's wrong when you are hanging out with a girl as hot as me! I mean, if I was ugly like @ven it would be understandable you're fantasizing about somebody actually sexually attractive. I'm kinda offended!`);
			
			if(mile.a_cuck > 3){
				txt(`I berated him but @qaa was getting cocky:`);	
				qaa(`I'm not sure if ${mile.slut > 15 ? "insatiable nympho slut" : "girl"} who eagerly sleeps with other men can criticize me for watching porn!`);
				kat(`Maybe I would not have to fuck other guys if you were able to keep up with me. And maybe you would be able to keep up with me if you weren't constantly jerking to porn!`);
			}else if(mile.slut > 15){
				txt(`I berated him but @qaa was getting cocky:`);	
				qaa(`I'm not sure if an insatiable nympho slut can criticize me for watching porn!`);
				kat(`Maybe I wouldn't be such a horny slut if you were able to properly sexually satisfy me. And maybe you would be able to properly sexually satisfy me if you weren't wasting your energy jerking to porn!`);
			}else{
				qaa(`Well... sorry. I had no idea you might find this offensive.`);
			}
			txt(`@qaa looked ashamed. This was exactly the opportunity I needed:`);
			kat(`Don't worry, I think I have a solution!`);
			qaa(`You want me to stop watching porn, don't you?`);
			kat(`Of course not! I have a completely different solution!`);
			qaa(`Thank God!`);
			txt(`@qaa seemed relieved. Big mistake.`);
			qaa(`Wait for me, I have to pick something at my place!`);
			link(`Go home. `, 102);
			break; 
			
	case 102: 
			emo("imp"); 
			txt(`I briskly returned and showed him a devious metal device.`);
			qaa(`Is that...`);
			kat(`I believe it is called a chastity cage.`);
			qaa(`You want me to...`);
			kat(`Yeah, put your penis inside it and lock it. That should prevent you from idly toying with your dick and preserve your stamina. Brilliant, right? What do you think?`);
			qaa(`I think it's very concerning you already had a chastity cage ready at home.`);
			kat(`You don't have to be worried, not like I bought it in advance and then just waited for an excuse to lock you up.`);
			qaa(`I see. And I assume you would keep the key?`);
			kat(`Naturally! You don't seem to be very excited!`);
			qaa(`I'm not very excited.`);
			kat(`There's no greater expression of love and trust than to give your lover the key to your... let's say *heart*. Or maybe you don't trust me?`);
			qaa(`I do trust you. More or less.`);
			kat(`You either do or don't trust me! `);
			qaa(`...I do trust you.`);
			kat(`I'm not going to force you. If you're not into kinky sexual games and don't want to utilize the opportunity to show me how much you love me, I'll respect that and we can go back to watching Netflix.`);
			qaa(`...I guess we can try it.`);
			kat(`Awesome! Take your pants off!`);
			txt(`@qaa hesitantly undressed and I squatted down to put on the chastity cage. There I ran into a minor issue, my presence there was filling his manhood with blood.`);
			link(`Quick handjob. `, 103, ()=> counter.bj = false);
			link(`Perfect blowjob. `, 103, ()=> counter.bj = true);
			break; 
			

	case 103: 
			mile.a_locked = true; //wears chastity atm
			mile.a_chastity = true; //this event is done
			mile.chastity_cages--; //uses chastity cage from inventory
			mile.a_dom += 2;
			effigy.qaa.showLess(-1,0,2);
			
			if(counter.bj){
				log_sex("bj", "qaa", "kat");
				qaa(`Maybe you could... you know... give me a head?`);
				kat(`Sure, I could.`);
				txt(`I shrugged. He deserved to have a nice memory, something he will recall with fondness when he will be locked, horny and desperate. I leaned closer and teased his cock with my tongue, with my fingers tickling his balls.`);
				qaa(`Oh, @kat!`);
				txt(`I opened my mouth and let him push his cock inside. I tried to not rush - even though I was excited to have him locked - I was sucking him slowly and methodically. Every once I ceased sucking and just gently fondled his rock-hard cock with my fingers, aptly building up his arousal.`);
				qaa(`Oh, @kat!`);
				txt(`@qaa loved my cushy lips and wild tongue. I was not able to keep him on the edge indefinitely and he filled my mouth with a spout of his thick cum.`);
			}else{
				log_sex("hand", "qaa", "kat");
				txt(`I was too excited to have him locked and I did not wish to waste time on more elaborate ways to give him pleasure. I firmly wrapped my fingers around his semi-hard shaft and began roughly pumping.`);
				qaa(`Oh-oh, @kat!`);
				kat(`Come on! Cum from me!`);
				txt(`I stroked slower but harder, feeling his cock twitching until it exploded. I presume he would prefer to ejaculate all over me but I did not want to clean his cum from my face and hair. I let him make a mess on the floor.`);
			}
			kat(`You're welcome!`);
			txt(`I smiled, licked his dick clean and then reached for the cage. I fitted his flaccid cock inside and locked the device shut, not giving him a chance to back out.`);
			kat(`Done! Lovely!`);
			qaa(`How do I have to stay locked?`);
			kat(`As long as I will enjoy it!`);
			qaa(`I would strongly prefer an exact duration.`);
			kat(`Well, too bad. I own your dick now and your preferences don't matter anymore.`);
			qaa(`@kat!`);
			txt(`There was a delightful fear in his eyes.`);
			kat(`Don't worry, I'm just teasing you. It's just an experiment, I won't keep you locked for long. I mean, I have no reason to hold any grudge against you for things you have done to me, right?`);
			qaa(`@kat, come on, don't be a bitch!`);
			kat(`I'm just messing with you. Or am I? You'll see!`);
			txt(`Slightly distressed he watched me hide the key in my pocket.`);
			kat(`Let's watch the movie. And I want to cuddle.`);
			next();
			break;
	}
}


export const a_chastity_inspection = (index) => {
	switch(index){
		default:
		case 101: 
			set.irl(); 
			placetime("hallway");
			emo("imp"); 
			mile.a_chastity_inspection = true;

			kat(`@qaa! We have to talk privately.`);
			qaa(`Sure! ...why?`);
			kat(`It is inspection time!`);
			qaa(`What?!`);
			kat(`I have to check when you're not cheating. I would be seriously disappointed if you removed your chastity cage without being allowed to!`);
			qaa(`Please, don't talk so loud! And I have it still on, I don't have the fucking key!`);
			kat(`Oh, somebody is getting upset! I do trust you. But I want to see it!`);
			txt(`@qaa reluctantly followed me to the bathroom. There I knelt down and unzipped his pants like I was going to give him a blowjob. His poor cock was still imprisoned inside the locked metal cage. And I was the only one holding keys! It felt so utterly empowering. I basically owned his cock now, he could not even get hard without my permission. And I could keep him locked as long as I wanted, maybe a few more days, maybe a week or two. Or maybe permanently. Just thinking about it was making me wet and I mindlessly poked the cage.`);
			qaa(`Can you please stop?`);
			kat(`I can't! Seeing your little dick locked is making me so horny!`);
			qaa(`If you're so horny... maybe we can do something about it?`);
			kat(`You wouldn't mind... right here... right now?`);
			qaa(`Of course not! I'm at your service! Always ready to sexually satisfy you!`);

			link(`Thank you! `, 102);
			link(`You are the best! `, 102);
			break;

		case 102: 
			emo("ahegao"); 
			
			kat(`You're the best! I love the things you can do with your tongue! `);
			txt(`All his excitement was suddenly gone.`);
			qaa(`M... maybe I can offer you more than my tongue?`);
			kat(`You mean... oh! I'm so sorry! I didn't mean to tease you and lead you on. I would love to unlock you but I don't have the key, I left it at home.`);
			qaa(`Oh...`);
			kat(`But don't worry, I'm sure I'll enjoy the cunnilingus as much as I would enjoy getting fucked. No big deal.`);
			qaa(`Oh.`);
			kat(`Is something wrong? Come on, I'm sure tasting my pussy will cheer you up!`);
			txt(`${rollUpSkirt(3, 0)}, leaned back against the wall of the cubicle and spread my leg. I was such a mean bitch! @qaa was less enthusiastic about the one-sided sexual act but he meekly knelt down and kissed my lower belly.`);
			kat(`Good boy!`);
			txt(`I appreciated it and patted his head. Manipulating him into getting his dick locked was a brilliant idea. @qaa was trying harder than ever before, maybe hoping to convince me to unlock him soon. His tongue was running up and down, ingeniously teasing my little pussy, pushing me towards an orgasm. However, the feeling of power was even better than the licking.`);
			kat(`Yeah! That feels good!`);
			txt(`I gave him the feedback but tried to not moan too loudly, I would prefer to keep our affair private.`);
			kat(`Oh yeah, I'm getting there... AH-`);
			txt(`I painfully bit my lower lip to not scream when I was overwhelmed by the sudden surge of bliss.`);
			qaa(`Did I do a good job?`);
			kat(`You did an amazing job! Your pussy-licking skills are so superb it's almost pointless to unlock you!`);
			txt(`I smirked at him, telling him the exact opposite of what he wanted to hear.`);
			next();
			break;
	}
}



		







