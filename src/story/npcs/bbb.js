import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise, niceness} from "Loop/text";


import {add_workday/*, next_event, add_today, get_schedule*/} from "Taskmaster/scheduler";
import {emo, quickTempOutfit, effigy, crew, quickSaveOutfit,  quickLoadOutfit, wdesc, createWear, updateDraw, sluttiness, makeup, wearRandomClothes, remove, removeEverything, wear, create, wears, showLess, showAll, rollUpSkirt} from "Avatar/index";
import {masochist, mas_vic, spoiler, dildo, log_sex} from "Virtual/text";
import {uniforms} from "Data/items/uniforms";
import {placetime} from "Loop/text_extra";
import {number2word} from "Libraries/dh";

import {NAME} from "System/variables";
import {popup} from "Gui/gui"; 


/////////////////// WEEK 1 /////////////////////////////////////////////////ú

//ADVICE 1 - beg him
export const b_advice_1 = (index) => {
	switch(index){
		default:
		 case 101:
			set.meta();
			mile.advice_1 = 2;
			mile.bbb++;
			emo("anoy");
			placetime(`@qbb's subreality`);
			txt(`I decided to ask @qbb. The other two seemed to have personal grudges against me but he was just a misogynous asshole. Maybe that meant he will be more reasonable? `);
			
			if(mile.sub > 0){
				qbb(`Oh! Hello, @katalt. Are you here for more?  `);
				kat(`W...what!?  NO!`);
				qbb(`Admit it! You enjoyed getting your rump spanked! `);
				kat(`NO! I did not! You.. you should feel sorry for doing that to me! It was awful!`);
			}else{
				kat(`Hello! `);
				qbb(`Hey!`);
				kat(`I'm here to give you a chance to apologize! `);
				txt(`I started sternly. `);
				qbb(`For what? `);
				kat(`For what!?! For, you know, what you did when you thought I'm AI! `);
			}
			
			qbb(`I don't feel like it. Anyway, it was your fault pretending you're the AI! Why are you really here? `);
			kat(`I need a little advice regarding the game and I think you owe me one for the horrible abuse! `);
			qbb(`I don't owe you anything! `);
			kat(`Fine!`);
			qbb(`Wait, maybe I'll help you anyway. `);
			kat(`Really!?!`);
			qbb(`If you ask very nicely. `);
			txt(`I frowned:`);
			kat(`Pretty please? Will you help me? `);
			qbb(`On your knees. `);
			kat(`On my knees?! OMG!`);
			txt(`I sighed. Well, I was already there... and begging him for help would be humiliating in any position. I reluctantly sank on my knees. `);
			kat(`Please! Will you help me? `);
			qbb(`Why? `);
			kat(`Because I need your help?`);
			qbb(`Why do you need it? `);
			kat(`Because I'm bad at virtual games? `);
			qbb(`Could you expand it further? Address the root cause? `);
			kat(`What root cause?!?`);
			qbb(`You know what I want to hear! `);
			
			links_mix(
				[`Because I'm a helpless and pathetic girl?  `, 102],
				[`Because I'm a clumsy, useless bimbo?  `, 102],
				[`Becuse I'm a stupid slut bad at gaming? `, 102],
			);
			break;
			
		case 102: 
			kat(`I grudgingly uttered. `);
			qbb(`Awww! Of course, I'll help you when you're asking me so nicely. `);
			kat(`Fuck you. So what should I do? I'm getting immediately killed without dealing any damage! `);
			qbb(`Well, to be able to fight monsters, you need a better weapon. The knife is useless. `);
			kat(`I know, I'm not <i>that</i> stupid. But I don't have money and can't find any better weapon anywhere!`);
			qbb(`Just sell you clothes. `);
			kat(`Don't mock me! `);
			qbb(`I'm serious. You need a proper weapon but you don't need any clothes. `);
			kat(`But without my clothes, I would be naked! `);
			qbb(`So what? Don't tell me a tease like you wouldn't enjoy prancing around nude. `);
			kat(`But the people...`);
			qbb(`They are not real people, just NPC's. You can do whatever you want without consequences. `);
			
			
			chapter(`Right! Good idea, I'll do it.`, `advice_1`, 200, ()=> counter.feyd_nude = 1);
			chapter(`Thanks, I guess.  `, `advice_1`, 200, ()=> counter.feyd_nude = 0);
			chapter(`That's the dumbest advice ever! `, `advice_1`, 200, ()=> counter.feyd_nude = -1);
			
			break;

			
	}
}



//WEEKEND 1

//MEETING 1 - discussion about submisiveness
export const b_meeting_1 = (index) => {
	switch(index){
		default:
		 case 101:
			set.meta();
			mile.bbb++;
			placetime(`@qbb's subreality`);
			txt(`I decided to visit @qbb. I did not felt very comfortable when one of three people who controlled my fate was such a psycho. I knew my chances to convince him to treat me less awfully were slim but I had to do something. `);
			if(mile.advice_1 !== 2){
				txt(`The other two seemed to have personal grudges against me but he was just a misogynous asshole. Maybe I will be able to convince him to be reasonable? `);
				if(mile.sub > 0){
					qbb(`Oh! Hello, @katalt. Are you here for more?  `);
					kat(`W...what!?  NO!`);
					qbb(`Admit it! You enjoyed getting your rump spanked! `);
					kat(`NO! I did not! You.. you should feel sorry for doing that to me! It was awful!`);
				}else{
					kat(`Hello! `);
					qbb(`Hey!`);
					kat(`I'm here to give you a chance to apologize! `);
					txt(`I started sternly. `);
					qbb(`For what? `);
					kat(`For what!?! For, you know, what you did when you thought I'm AI! `);
				}
				qbb(`I don't feel like it. Anyway, it was your fault pretending you're the AI! How can I help you? `);
					
			}else{
				qbb(`Hello again, @katalt. `);
				kat(`Hey!`);
				qbb(`Have you changed your mind? `);
				kat(`About what? `);
				qbb(`About getting spanked again. `);
				if(mile.sub > 2){
					kat(`N.. no! I did not!`);
				}else{
					kat(`Of course not, you twisted bastard! `);
				}
				qbb(`Then how can I help you? `);
			}

			kat(`I'm here just hang out. I realized we don't know each other well...`);
			qbb(`Hah, really? We just blackmail you a little and you instantly want to be a best friend? Why are you hanging out with me and not with @qaa or @qcc? Did they see right through you and told you <i>fuck off!</i>?`);
			kat(`No!`);
			qbb(`Of course no. They're simps and crave your attention. But then why you're trying to woo me? `);
			kat(`I'm definitely not trying to woo you!`);
			qbb(`Maybe you like that I'm not impressed by you. That I know who you are.  `);
			kat(`...who am I?`);

			if(ext.slut === 0){ 
				qbb(`Just an insecure tease. Shamelessly flirting with everybody because you love the power it gives you over boys. It's all you have. `);
			}else if(ext.slut === 1){ //TODO SLUT
				qbb(`Just a <i>basic</i> bitch. Gossiping with your stupid friends and dating dumb jock is everything you have. `);
			}else{
				qbb(`Just a slut. Desperately sleeping around, trying to fill both your physical holes and the metaphorical hole in your soul.`);
			}

			kat(`I hope you don't want to become a psychologist because you're completely wrong about me!`);
			qbb(`Stop pretending. You enjoy it when men tell you what to do. And you love being mistreated because you know you deserve it. `);
			txt(`I was thinking about what he just said to me. It was not true, or was it? `);


			links_mix(
				[`I will not ever let anybody mistreat me. ${spoiler("domina")}`, 102],
				[`Willingly I would never let anybody like him mistreat me. ${spoiler("slave")}`, 103],
				[`Sometimes I might enjoy being mistreated. ${spoiler("masochist")}`, 104],
			);	
			break;
			
			
		case 102: 
			mile.sub--;
			mile.b_meeting_1 = 1;
			mile.b_dom++
			emo("angry");
			kat(`You couldn't be more wrong! I'm the one telling men what to do! And remember this: anybody who won't respect me <i>will</i> pay. Sooner or later.`);
			qbb(`Whoa. That almost sounded scary. Almost. `);
			if(mile.sub < -1){
				txt(`I just smiled and shrugged. He was warned. He will have only himself to blame. `);
			}else if(mile.sub > 4){
				txt(`I frowned. I tried to sound tough but I failed and only made him laugh. `);
			}else{
				txt(`I frowned. He did not believe I might be a threat. `);
			}
			next();
			break;
		
		case 103:
			mile.sub++;
			mile.b_meeting_1 = 2;
			mile.b_vic++
			emo("angry");
			kat(`No! Are you crazy?! You're absolutely wrong! Please, next time keep your ridiculous amateurish analyses to yourself! `);
			txt(`I lied. He was not completely wrong. It was true that cocky bad boys were my weakness. Despite my generally bitchy attitude I tended to be rather submissive in relationships, often even allowing guys to push me into things I did not enjoy. But was definitely not going to tell him that. `);
			qbb(`Don't lie to me! And don't lie to yourself! Accept your proper place!`);
			kat(`Fuck you! I think I have to go! `);
			txt(`I decided to quickly leave. `);
			next();
			break;
			
		case 104:
			mile.sub++;
			mile.b_meeting_1 = 3
			mile.b_mas++;
			emo("unco");
			kat(`N-No! ...of course, I don't! You're crazy! Please, next time keep your ridiculous amateurish analyses for yourself! `);
			txt(`I tried to deflect his claims. I did have submissive fantasies but I was certainly not going to share them with <i>him</i>.`);
			qbb(`Don't fight it!`);
			kat(`I think I have to go. `);
			txt(`I decided to quickly leave. `);
			next();
			break;
	}
}	
			
			
			


//TODO SCENE - willingly tied - separate masochist/slave paths



			
/////////////////// WEEK 2 /////////////////////////////////////////////////ú

//MEETING 2 - knots, tied hands
export const b_meeting_2 = (index) => {
	//TODO - b_meeting_1 currently isnt mandatory for this, ???
	switch(index){
		default:
		 case 101:
			set.meta();
			placetime(`@qbb's subreality`);
			mile.bbb++;
			if(mile.b_mas > 0){ //TODO BEWARE 
				txt(`I decided to visit @qbb. Not like I enjoyed his irreverent pose when he talked with me. But there had to be a way to get through to find what made him tick. When you dug deep enough, everybody has a lever you can use to your advantage. `);
			}else{
				txt(`I decided to visit @qbb. I hated that arrogant, misogynous jerk so much and spending time in his presence was suffering but there had to be a way to get through to find what made him tick. When you dug deep enough, everybody has a lever you can use to your advantage. `);
			}
			kat(`Hello!`);
			qbb(`Hey. You thought about what we discussed the last time and now you want to tell me something?`);
			kat(`No! I'm going to be straight with you! I'm here to finally convince you I'm not just vain and dumb slut. `);
			qbb(`...okay. I give you the benefit of doubt. How you'll do it? `);
			kat(`There has to be something, a hobby or topic we both enjoy? `);
			qbb(`I don't think so. `);
			kat(`Come on! What do you enjoy - and skip everything perverted or disgusting. `);
			
			if(mile.b_mas > 0){
				qbb(`What if there are perverted things we both enjoy? `);
				kat(`I do not enjoy any perverted things!`);
				txt(`He incredulously shrugged. `);
			}
			
			qbb(`Dunno, I enjoy video games, programming...`);
			kat(`Come on! Try a bit harder! Do I look like somebody who's into programming?`);
			qbb(`Well, no. Messing with electronic, knots...`);
			kat(`Knots?! Now you're making shit up. What kind of hobby is that?!?`);
			qbb(`Learning how to tie knots is both an extremely useful skill and an immensely relaxing and spiritually fulfilling hobby. `);
			link(`Knots. `, 102);
			break;
			
		case 102:
			emo("focus");
			kat(`Oh my! Fine. Knots. What is your favorite knot? `);
			qbb(`That's a hard question. `);
			kat(`Is it? Ask me what is my favorite knot.`);
			qbb(`What is your favorite knot? `);
			kat(`The bow. It's that easy.`);
			qbb(`That's a very <i>basic</i> answer.`);
			kat(`You don't have to be hipster about everything. The bow know is symmetrical, looks pretty and you can tie your shoes with it. What's there not to love?`);
			qbb(`Did you know the bow knot is a doubly slipped knot?`);
			kat(`What does it even mean?`);
			qbb(`A slip knot is easily untied, you just have to pull the end. They are awesome. Hmmm... I think I like the hangman knot. `);
			kat(`And you call me basic. Don't cut yourself on that edge. `);
			qbb(`What? It's one of the greatest knots, you can tie it with ease and it's practical for every use. `);
			kat(`If you say so. `);
			qbb(`Hmmm, actually I think you could help me with something when you're so interesting in knots. `);
			link(`Gladly! `, 103);
			link(`Sure.`, 103);
			link(`Hmmmm.`, 103);
			break;
			
		case 103:
			emo("anoy");
			kat(`What do you need? `);
			qbb(`Another pair of hands. I'm struggling with the Truckers hitch.`);
			kat(`What the hell is Trucker's Hitch? `);
			qbb(`It's another of the greatest knots. Very strong and flexible. But I do something wrong. I found a nice video tutorial but it's in Romanian. `);
			kat(`...okay. What I'm supposed to do? `);
			txt(`Then he told me to held the untightened knot in my hands while he made loose loops around my body. But even after several attempts, we were not able to figure how to finish the hitch.	 `);
			kat(`Are you sure it should be tied this way?`);
			qbb(`Obviously I'm not! Let's try again. `);
			txt(`This time I ended completely tangled in the cord. It seemed the more he tried to help me the more entangled I was. `)
			qbb(`And done. `);
			txt(`And he roughly pulled the cord and firmly bind my arms to my body!`);
			kat(`What the hell!!! ...did you ...tricked me! `);
			qbb(`You're so bright.`);
			txt(`I wiggled with my body and arms but my hands were tied to my sides. The more I tried to get away, the tighter the knots were getting. `);
			kat(`Haha! Very funny. Now let me go!`);
			qbb(`Hmmm... nah. I think I like you better this way!`);
			
			links_mix(
				[`Threaten him. `, 104],
				[`Beg him. `, 105],
			);
			break;
			
		case 104:
			mile.sub--;
			//mile.b_dom???
			emo("angry");
			mile.b_meeting_2 = 1;
			kat(`Let me go or I swear, you'll regret it!`);
			if(mile.sub < -3){
				txt(`I coldly announced. It made him waver for a moment but then he just doubled down. `);
				qbb(`Yeah? What are you going to do!?!`);
			}else{
				txt(`It only made him laugh. `);
				qbb(`You're so cute when you're mad!`);
			}
			txt(`He moved closer and I tried to kick him. However, my balance was not the best, he managed to grab my knee and topple me over. @qbb slowed down my fall so I would not get hurt but obviously, I was not very grateful. `);
			txt(`When I was lying on the floor, he sat on me astride to hold me down and squeezed my left tit. I writhed but I was not able to get free. `);
			kat(`Let me go, you jerk!`);
			qbb(`If you want me to do something for you, you should be using a more respectful tone, wench!`);
			kat(`Fuck you! `);
			qbb(`I see you prefer to stay tied! Hmmm... I wonder if you are ticklish.`);
			txt(`His fingers gently ran over my ribs. I desperate tossed but I was completely helpless. `);
			kat(`Stop it! ...fine!   ...please, let me go!`);
			qbb(`Come on, @kat! You know you can ask me in a nicer way!`);
			txt(`I calmed down, trying to suppress all the hate and anger I felt.`);
			kat(`Pretty please, will you let me go? `);
			qbb(`Hmmm, fine! `);
			txt(`She stood up. He didn't help me, letting me lying on the floor like a turtle on the back, and chuckled when I tried to get back on my feet. `);
			kat(`And the rope?!   ...please, will you untangle the rope? `);
			qbb(`Only if you swear you won't slap me or hit me or kick me or hurt me in any way!`);
			kat(`Fine!`);
			txt(`Clever bastard, I definitely intended to hurt him. He clutched the end of the rope and just pulled. The knots tying my arms to my torso instantly fell apart. `);
			qbb(`That was a practical example of a slip knot!`);
			next();
			break;
			
		case 105:
			mile.sub++;
			//mile.b_vic??? mile.b_mas??
			mile.b_meeting_2 = 2;
			emo("help");
			kat(`Please, let me go! `);
			txt(`I made puppy eyes on him, he had to realize he was going too far!`);
			qbb(`Why should I? I think you look cute when you're tied! `);
			kat(`I don't like being tied! `);
			qbb(`Don't you? Fine. What will I get when I untie you? `);
			txt(`He meaningfully stared at my chest. I shivered with disgust but then swallowed my pride. `);
			kat(`If you untie me, you can cup my tits. Once!`);
			qbb(`Are you sure? I don't want to grope you against your will.`);
			txt(`He smirked. `);
			kat(`Yes, I'm sure. `);
			qbb(`So you really want me to cup your tits? `);
			txt(`I sighed. `);
			kat(`Yes, I want you to cup my tits. `);
			qbb(`Sorry, but you don't look like you're fine with it.  I don't want to make you feel uncomfortable.`);
			kat(`Being tied makes me very uncomfortable!`);
			qbb(`...`);
			kat(`...`);
			qbb(`...`);
			kat(`Please, will you cup my tits? `);
			qbb(`Could you try it again without sounding annoyed and angry?`);
			kat(`No. `);
			qbb(`...`);
			kat(`...`);
			qbb(`...`);
			kat(`Please, will you cup my tits? `);
			qbb(`Well, when you begging me so nicely, I'll gladly oblige. `);
			txt(`@qbb deliberately squeezed my breasts. `);
			qbb(`Nice!`);
			kat(`Could you untie me now? `);
			qbb(`Hmmm... I wonder if you are ticklish.`);
			kat(`Ahhhhh!`);
			txt(`I squealed when his fingers gently ran over my ribs. I was completely helpless to stop him. `);
			kat(`Please, untie me!`);
			txt(`I politely demanded. `);
			qbb(`Fine!`);
			txt(`He clutched the end of the rope and just pulled. The knots tying my arms to my torso instantly fell apart. `);
			qbb(`That was a practical example of a slip knot! `);
			next();
			break;
		
	}
}








//ADVICE 2 - choker
export const b_advice_2 = (index) => {
	switch(index){
		default:
		case 101:
			set.meta();
			placetime(`@qbb's subreality`);
			mile.advice_2 = 2;
			mile.bbb++;
			mile.choker = true;
			emo("suspect");
			if(mile.advice_1 === 2){
				txt(`@qbb already proved he is willing to help me. After a bit of gloating and humiliating, he gave me decent advice. `);
			}else{
				txt(`I decided to ask @qbb. It seemed he was less concerned about the fairness of our deal and might be more willing to help me. `);
			}

			kat(`Hey. `);
			qbb(`Hello? How can I help you? Did you get stuck in the game? `);
			if(mile.advice_1 === 2){
				kat(`...yes. Should I get on my knees and beg you for your help?`);
				qbb(`You're learning quickly. `);
			}else{
				kat(`...maybe. `);
				qbb(`Heh, maybe? `);
			}
			kat(`I need only a little nudge in the right direction. And I'm sure you'd enjoy giving me a lecture on how smart and amazing you are and how dumb and useless I am and how brilliantly you did solve the Arazor's storyline.`);
			if(mile.advice_1 === 2){
				qbb(`You're right. But I'm not going to give you free advice!`);
			}else{
				qbb(`You're right. Okay. I'll help you. `);
				kat(`Awesome. `);
				qbb(`But it won't be free. `);
			}
			kat(`Fine! What do you want?!`);
			qbb(`I want you to visibly wear a token of your gratitude. `);
			link(`Token of my gratitude!?! `, 102);
			break;
			
		case 102:
			emo("anoy");
			txt(`Terrified I imagined all sorts of horrible degrading things it might mean. He was a twisted perved and I almost refused outright. `);
			kat(`W... what do you mean?!?`);
			txt(`He smiled and conjured a leather choker. `);
			kat(`Just a choker? Okay, I'll wear it.`);
			qbb(`It's not a choker. It's a collar. It symbolizes you are nothing but a submissive slut. Sure, people will miss that but I will know. And <i>you</i> will know.`);
			kat(`It's just a choker!`);
			txt(`The more he was talking the less appealing the choker seemed.`);
			qbb(`Every time you look in a mirror I want you to remember that you are wearing it only because I made you wear it! The physical version will be delivered to you tomorrow. `);
			kat(`...whatever! `);
			qbb(`Move your hair away. `);
			txt(`I did so and he locked the choker around my throat.`);
			kat(`So what should I do?`);
			qbb(`First you have to hire your first mate. It could be either @lys, @ste or @gal. Your first mate will arrange the rest of the preparation. Then you have to sail to Karged, the map is randomized but to reach Karged you always just have to sail west. And don't worry, you can definitely trust everybody. `);
			txt(`He began explaining. `);

			createWear("leatherChoker");
			updateDraw();
			add_workday("classes", "b_advice_2_reaction");
			next();
			break;
	}
}

export const b_advice_2_reaction = (index) => {
	if(PC.collar){
		mile.b_advice_2 = 1;
		txt(`When I was passing @qbb in the hallway during the second break, he smirked at me. `);
		qbb(`Nice collar!`);
		kat(`IT'S JUST A CHOKER!`);
		next();
	}else{
		mile.b_advice_2 = -1; 
		//TODO TODO!! !! 
		txt(`I decided to ignore @qbb demand to wear a choker.`);
		next();
	}
}			
			



/////////////////// WEEK 3 /////////////////////////////////////////////////ú


//ADVICE 3 - humiliating blowjob / slap him
export const b_advice_3 = (index) => {
	switch(index){
		default:
		case 101: 
			mile.bbb++;
			set.meta();
			placetime(`@qbb's subreality`);
			mile.advice_3_2 = true;
			emo("anoy");
//TODO - MAKE SURE THIS HAPPENS BEFORE THE TRIPONT			
			
			if(mile.advice_1 === 2 || mile.advice_2 === 2){
				qbb(`Hello, @kat. Are you here to beg me to help you with the game because you got patheticly stuck again?`);
				kat(`No I'm here because... actually yes, that's why I'm here.  `);
			}else{
				if(mile.b_meeting_2){
					qbb(`Hello, @kat. Are you here to learn more about knots? I can show you a new rope trick. `);
				}else{
					qbb(`Hello, @kat. How can I help you? I assume you aren't here to be spanked? `);
				}
				kat(`That's a brilliant guess. I'm here because I need a second opinion about something. `);
			}
			qbb(`I'm listening. `);
			
			kat(`Well, the Amaranthine Forest is pretty dangerous and its exploration is pretty hard. `);
			qbb(`And you need advice. `);
			kat(`Well, yeah. Or maybe, since you enjoy video games so much and there's the cooperative mode...`);
			qbb(`I see. `);
			kat(`So... you're excited to help me, right? `);
			qbb(`Sure. It will cost you but I'm willing to make a deal with you. Advice wouldn't be cheap but this will be expensive. `);
			txt(`I knew that. But I warned him: `);

			if(mile.b_meeting_2 && mile.advice1 === 2){
				kat(`No collars and no bondage! `); 
			}else if(mile.advice_1 === 2){
				kat(`No collars or anything similar. `); 
			}else if(mile.weekend_bb == 1){
				kat(`No bondage or anything similar. `); 
			}else{
				kat(`Nothing weird!`);
			}
			qbb(`Okay. Then there is Plan B. You will suck my dick. `);
				
			if(mile.slut > 6){ //TODO
				kat(`Great. `); 
				txt(`That was not so bad. Just a blowjob. I was afraid he will ask for something twisted. `);
			}else if(mile.slut > 2){
				kat(`Okay. `); 
				txt(`Even though humiliating, at least it would be easy and quick. `);
			}else{
				kat(`But... Come on! `); 
				txt(`I really, really did not want to do this. It was so disgusting! But I was not able to change his mind. `);
			}
					
			qbb(`First, you need to beg me. `);
			txt(`I was outraged and weary: `);
			kat(`Uhgh! I can give you an awesome blowjob. Why the hell you need to always complicate things and try to degrade me? `); 
		
			qbb(`Humiliating you is the fun part. The blowjob itself is just a formality, I could just jerk off if I wanted just that. `);
			txt(`I wanted this to be over and go back home. `);
		
			kat(`Can I suck your dick?`);
		
			qbb(`Come on! I thought slut like you would be more experienced regarding begging for dicks!`);
		
			link(`Beg him for his dick. `, 110);
			chapter(`Leave. `, mile.advice_3 ? `advice_4` :  `advice_3`, 102);
			con_link(mile.sub < 7, `submissive`, `Slap him! `, 120);
			if(mile.sub > 0) chapter(`Run away crying. `, mile.advice_3 ? `advice_4` :  `advice_3`, 102, ()=> mile.sub++);
			break;
		
		case 102:
			set.meta();
			emo("anoy");
			qbb(`I see you changed your mind and you need my help so much you're willining to do anything! `);
			kat(`<small>Hmmm.</small>`);
			qbb(`We can continue where we stopped, once you apologize for your childish tantrum. `);
			kat(`What?!?`);
			qbb(`Apologize! Now, slut!`);
			link(`Apologize. `, 110);
			chapter(`Leave. `, mile.advice_3 ? `advice_4` :  `advice_3`, 102);
			break;
		
		case 110:
			mile.sub++;
			if(mile.advice_3){
				mile.advice_4 = 2;
			}else{
				mile.advice_3 = 2;
			}
			mile.b_vic++;
			mile.b_mas++;
			mile.b_slap = -1;
			effigy.qbb.showLess(-1, 0);
			emo("help");
			log_sex("bj", "qbb");
			
			kat(`Pretty please, would you mind to delight me with your impressive penis? `);
			qbb(`No sarcasm. `);
			txt(`UGHG!`);
			kat(`Please, can I suck your dick? `);
			qbb(`Hmm... Why? `);
			kat(`Because I'm a slut who loves sucking dicks? What the hell you wanna hear?!? `);
			qbb(`Okay. That will do. Go ahead. `);
			txt(`He was just standing there expecting me to do everything. I dropped on my knees, unzipped his pants and hesitantly started. Seeing me on my knees in front of him was really making him hard. I fondled his throbbing dick, familiarizing myself with it, and then took it between my lips.  `);
			qbb(`No, not like that! I want constant eye contact with you. `);
			txt(`I very reluctantly raised my eyes. `);
			qbb(`Yes, I love that your hate-filled look. Go on, slut! `);
			if(mile.slut < 2){
				txt(`I felt uneasy and disgusted. I was not particularly experienced at giving blowjobs and sucking a dick of somebody I did not even like just for his help felt wrong. This was a huge mistake. `);
			}else if(mile.slut > 8){
				txt(`Sucking his dick and listening to him gloat was so irksome. This was definitely one of the top ${mile.slut > 10 ? "ten" : "five"} worst blowjobs in my life... so far... `);
			}
			txt(`At least my torment was not long. It took only a few moves before he began to grunt and his cock twitched and erupted in my mouth. `)  // Even though, that bastard did not let me clean myself until he explained to me how to trick the Margrave.
			kat(`Fuck!`);
			qbb(`Good girl. You convinced me. `);
			next(`Try to forget. `);
			break;
			
		case 120:
			mile.sub--;
			mile.b_dom++;
			mile.b_slap = 1;
			emo("angry");
			
			txt(`The loud smack was so satisfying and tingling in my palm so delightful. `);
			kat(`YOU WILL NOT CALL ME A SLUT!`);
			qbb(`Bu.. Wh..`);
			kat(`You are not so tough now, are you?!`);
			txt(`He was cowering, taken by surprise by my sudden outburst, scared of another slap and... `);
			kat(`Oh my... is that a boner?!!!`);
			qbb(`No!...`);
			chapter(`Leave. `, mile.advice_3 ? `advice_4` :  `advice_3`, 102);
			con_link(mile.sub < 4, `submissive`, `Show him who is in charge. `, 121);
			break;
			
		case 121:
			mile.sub--;
			if(mile.advice_3){
				mile.advice_4 = 2;
			}else{
				mile.advice_3 = 2;
			}
			mile.b_dom++;
			mile.b_slap = 2;
			emo("smug");
			log_sex("hand", "qbb");
			//mile.domina = 1;
			//mile.charge = 1;
		
			txt(`I slapped him again, this time quite lightly. `);
			kat(`Don't dare you fucking lie to me!  `);
			qbb(`I...`);
			kat(`You are enjoying being beaten by a girl, aren't you? You disgust me! `);
			txt(`I pushed him against the wall and roughly grabbed his crotch. `);
			//kat(`The Margrave loophole?`);
			//qbb(`Margrave who? Augh! Yes, the Margrave! You need to...`);
			kat(`You. Will. Help. Me. `);
			qbb(`I... Yes. I will. `);
			txt(`He was hastily promising while I was roughly rubbing his genitals until he came in his pants. `);
			kat(`You will never ever talk to without proper respect! `);
			qbb(`<small>Aye, ma'am. </small>`);
			next(`Great! `);
			break;	

	}
}	


//TRIPOINT SLAVE - cocksuckings, cuffed X dominating, eating pussy 
export const b_tripoint = (index) => {
	
	const masochist_hard = mile.b_mas > 1 && masochist(); //TODO
	const masochist_light = mile.b_mas > 0 && masochist(); //TODO
			
	switch(index){
		default:
		case 101: 
			mile.bbb++;
			set.meta();
			placetime(`@qbb's subreality`);
			emo("anoy");
//TODO - CONNECTION WITHOUT VIRTUAL GAME!!!!!!!!!! (ADVICE 3 => SLAP);
			if(mile.b_dom > 1){
				txt(`@qbb seriously pissed me. What he did in the virtual game was inexcusable!  I thought I thoroughly explained to him he should treat me with respect but it seemed he was still not getting it. Well, it was time to solve this definitely. `);
			}else{
				txt(`Something needed to be done with @qbb. What he did in the virtual game was inexcusable! I was not sure how to convince to treat me like a human being but I had to find a way. `);
			}
						
			qbb(`Hello, @kat! How are you? `);
			if(mile.sub < 1){
				kat(`Hello, you jerk! We need to talk! `);
			}else if(mile.sub > 6){
				kat(`Hello!... I think... we need to talk. `);
				txt(`I nervously started. `);
			}else{
				kat(`We need to talk! `);
			}
			qbb(`Sure, about what? `);
			
			if(mile.b_slap > 1){
				kat(`I thought I explained you clearly you should treat me with the respect I deserve! `);
				txt(`He frowned when he remembered the slap and instinctively made a step back. But he got cocky: `);
				qbb(`I treat you will all the respect you deserve - none! `);
				kat(`You little b-`);
				qbb(`Please, let me finish, @kat! `);			
			}else{
				if(mile.sub < 5){
					kat(`About the way you constantly act like an asshole! `);
				}else{
					kat(`About the way you're constantly try to degrade me! Please! This went too far! `);
				}
			
				qbb(`Sure, we can discuss that that. `);
			}
			txt(`He was even more smug than usually. `);
			
			
			if(masochist_hard){
				if(mile.burned){
					qbb(`Let me explain - you're in deep troubles, girl. @qaa finally realized you'll never love him and he is rather eager to punish you out of spite. And @qcc will love see you suffer too. `);
					kat(`Yeah?!`);
					qbb(`You have no idea how much they hate you. But don't worry, @katalt. I'm offering you my help. `);
					kat(`Your help!?!`);
				}
			}else if(mile.burned){
				qbb(`Let me explain - you're fucked. @qaa finally realized you'll never love him and he is rather eager to punish you out of spite. And @qcc won't mind to see you suffer either. `);
				txt(`He very coldly explained. `);
				
				if(mile.blackmail){
					qbb(`And with full access to your Cloud, you can easily imagine what total, everlasting damage we can do. `);
					if(mile.sub > 4){
						kat(`But... but you promise you won't do that!`);
					}else{
						kat(`You promise you won't touch it!`);
					}
					qbb(`As long as you stay a good, obedient girl and do everything we tell you!`);
				}else{
					mile.blackmail = true;
					mile.blackmail_question = 1;
					qbb(`Moreover, we have the full access to your Cloud. `);
					kat(`...what!? My Cloud!?`);
					qbb(`Yeah! Your Cloud and everything saved there. `);
					kat(`No! You're bluffing! My cloud is absolutely secure!`);
					qbb(`I can ensure you I'm not bluffing! And there are so many interesting things you would surely prefer to keep private. `);
					//You can't do that!
				}
				kat(`Fuck!`);
				qbb(`But don't worry, @katalt. I'm a reasonable man and I don't want to see your life completely ruined. That would be such a shame! If you make a deal with me, I'll make sure your reputation won't suffer... too much... `);
			
			}else{
				if(mile.blackmail){
					//TODO
				}else{
					mile.blackmail = true;
					mile.blackmail_question = 1;
					qbb(`By the way, did you knew we have the full access to your Cloud?`);
					kat(`...what!? My Cloud!? How!?`);
					qbb(`Yeah! Your Cloud and everything saved there. `);
					kat(`No! You're bluffing! My cloud is absolutely secure!`);
					qbb(`I can ensure you I'm not bluffing! And there are so many interesting things you would surely prefer to keep private. `);
					kat(`W... why are you telling me this!?`);
					qbb(`To make you realize how important is to keep us happy. To keep me happy. `);
				}				
			}
			
			
/*			
			So if you want to survive this blackmail without your reputation damaged more than it already	was, you should better do <i>everything</i> to please me! `);
				txt(`I was breathing heavily, furious but painfully aware he was right. `);				
*/

			
			qbb(`I have a very generous offer. I will ensure no task will be too humiliating or degrading. And I'll be nice to you on public. Well, nicer. But just between us two, you'll willingly become my obedient slut and slave. Sounds good, doesn't it? `);
			
			
			
			if(mile.sub < 0){
				kat(`Are you out of your fucking mind?!?! `);			
			}else if(mile.sub > 6){
				kat(`But.. but... `);
			}else{
				kat(`Are you serious?!? `);
				
			}
			
			if(masochist_light){
				qbb(`I'm serious. Think about this, @kat. I know you want it! `);
				kat(`I.. I don't!`);
				txt(`But I have to think about Letë. She seemed to be very satisfied with being a slave. Maybe I would love it too? Losing the control made me feel kinda aroused. I already realized that ${mile.sub > 4 ? "deep down I was very sexually submissive" : "sometimes I enjoyed being sexually submissive"}.`);
			}else if(mile.b_dom > 0){
				txt(`He did not actually think I will accept this, did he? How he ever dared to suggest something so stupid! But... could I really risk making him angry? `);
			}else{
				txt(`It was terrifying. I did not want to be completely at mercy of this sadistic psycho! But what other choices I had? Could I risk making him angry? But becoming his slave... I imagined myself as one of the poor elvish girls in the slave pens and felt sick. `);
			}
			
			qbb(`Be reasonable @kat!`);
			txt(`@qbb was so close, he lightly caressed my shoulder. `);
			qbb(`Finally accept you're just a submsissive whore, call me <i>Master</i> and suck my dick! `);
	
			if(mile.sub > 3){
				txt(`It will not be forever, just for a few days until I complete the game... and it probably would not be worse than my current situation. `);
			}
	
			//TODO - blocked for too dominant?
			link(`Accept being his submissive whore. ${spoiler("slave/masochist")}`, 200);
			
			con_link(mile.sub < 4, `submissive`, `Attack him. ${spoiler("dominatrix")}`, 300);
			link(`No. `, 110);
			con_link(mile.sub < 9, `submissive`, `Fuck you. `, 130);
//CONNECT FUCK YOU PATH TO DOMINATRIX, MISABLE!
			//if(mile.a_love > 1)
			con_link(mile.a_dating, `not dating @qaa`, `Call his bluff. `, 120);
			break;
		
		
		case 110:
			mile.b_mas--;
			mile.b_tripoint = 1;
			if(mile.sub > 5){
				kat(`I.. I wont't... No! `);
				txt(`I feebly resisted.  `);
			}else if(mile.sub < 0){
				kat(`Becoming your slave?!? Never! `);
				txt(`I emphatically refused his outlandish suggestion. `);
			}else{
				kat(`No! I won't agree to be your slave! `);
				txt(`I refused his outlandish suggestion. `);
			}
			qbb(`Are you sure? Think about it. I can help you. `);
			kat(`I said No!`);
			qbb(`Do what you want. You'll have only yourself to blame. `);
			if(mile.sub < 1){
				emo(`angry`);
				txt(`He desperately tried to sound ominous. `);
			}else{
				emo(`shy`);
				txt(`His ominous threat made me shiver. `);
			}
			next();
			break;	
			
		case 120:
			mile.sub--;
			mile.b_mas--;
			mile.b_tripoint = 2;
			emo(`imp`);
			kat(`You're pathetic! Do you think I'm afraid of you?!? @qaa won't let you hurt me! `);
			qbb(`You don't know him as well as I do! `);
			kat(`Who do you think wins between us? Spoiler: the one with tits! `);
			qbb(`You're a manipulative bitch! `);
			kat(`Duh, I know! `);
			qbb(`You'll regret this! `);
			txt(`He desperately tried to sound ominous. `);
			next(`Okay, whatever!`);
			break;
			
		
		case 130:
			mile.sub--;
			mile.b_mas--;
			mile.b_dom++;
			mile.b_tripoint = 3;
			emo(`angry`);
			kat(`Fuck you! `);
			txt(`I showed him my middle finger:	 `);
			kat(`Are you really thing I would <i>ever</i> ageed to be come a slave of such a pathetic, desperate pervert?!!`);
			if(mile.sub < 0){
				txt(`I laughed into his face, he was mad: `);
			}else{
				txt(`I had enough! I could not let him abuse me! He was mad:`);
			}
			qbb(`Yeah, fine! Do what you want! There will be consequences you will have only yourself to blame! `);
			if(mile.sub < 0){
				emo(`suspect`);
				kat(`What consequences!`);
				txt(`I snapped at him and he stumbled.`);
				qbb(`Well, I...`);
				kat(`Yeah, you have nothing! You're full of shit!`);
			}else{
				kat(`I don't care about consequences! Nothing could be worse than being your slave!`);
				qbb(`You'll see!`);
				txt(`He desperately tried to sound ominous. `);
				kat(`Whathever, I don't give fuck! `);
			}
			next(`Bye loser!`);
			break;
			
		
		case 200:
			mile.sub++;
			mile.sub++;
			mile.b_mas++;
			mile.b_mas++;
			mile.b_vic++;
			mile.b_vic++;
			mile.b_dom--;
			mile.b_dom--;
			mile.b_slave = true;
			mile.b_tripoint = 5;
			effigy.qbb.showLess(-1, 0);
			emo(`shy`);
			log_sex("bj", "qbb");
			
			kat(`Okay. `);
			qbb(`Okay what? `);
			kat(`I'll be a submissive whore if you stop trying to constanly publicly humiliate me. `);
			qbb(`And? `);
			kat(`And I will suck your cock. `);
			qbb(`And I will suck your cock, Master! `);
			kat(`Yeah, of course, Master. `);
			qbb(`I don't want to see any of that attitude of yours! Or else you'll be punished. Start again from the top. `);
			kat(`Yes, Master. I'll be a submissive whore and I'll suck your cock. `);
			if(masochist_hard) txt(`Proclaiming that and willingly sealing my fate was making my pussy tingle. `);
			txt(`He threw me something. It was a pair of metal handcuffs. `);
			qbb(`Put them on! Lock your hands behind your back! `);
			kat(`But...`);
			qbb(`You don't need hands to suck a cock, do you? `);
			kat(`...no. `);
			txt(`The cuffs loudly clicked, I voluntarily immobilized myself. He immediately exploited it and squeezed my defenseless breasts. `);
			link(`Sucking Master's cock. `, 201);
			break;
			
		case 201:
			emo(`help`);
			showLess(0, -1);
			kat(`What are you doing! `);	
			{
				const bra = wears.bra ? `Then he squeezed my tits and after a short struggle even removed my bra. ` : ``;
				if(wears.dress){
					txt(`I protested, he was quite rough. But he ignored me and forcefully stripped my dress. ${bra}`);
				}else if(PC.upper.dad == "wTee" || PC.upper.dad == "wDualTee"){
					txt(`I protested, he was quite rough. But he ignored me and flipped the front part of my T-shirt over my head to expose my boobs. ${bra}`);
				}else if(PC.upper.dad == "wTubeTop"){
					txt(`I protested, he was quite rough. But he ignored me and pulled town my tube top to expose my boobs. ${bra}`);
				}else if(PC.upper.dad == "wHalterTop"){
					txt(`I protested, he was quite rough. But he ignored me and just untied my top and let it drop down. ${bra}`);
				}else{
					txt(`I protested, he was quite rough. But he ignored me and just removed my ${wdesc.upper}. ${bra}`);
				}
			}
			qbb(`I'm just admiring my new toy! `);	
			
			if(mile.meta_boobs > 0){
				txt(`He kneaded my ${wdesc.tits} and then slapped them, making them bounce. I yelped `);
				kat(`Ouch!`);
			}else{
				txt(`He lightly pinched my nipples and chuckled when I winced. That bastard really enjoyed having total power over me. `);
			}
			
			txt(`@qbb groped and caressed my whole body, with my hands cuffed behind my back. I was not able to stop him, I just helplessly squirmed. Then he pushed me down on my knees a told me to get his dick out. `);
			kat(`... but I need my hands...`);
			qbb(`I'm sure a slut like you figure something out! `);
			txt(`He made me to unbutton and unzip his jeans just with my mouth. I pulled them down and the underwear too and his erected cock sprung up. @qbb slapped my face with it several times. `);
			qbb(`Go on! `);
			txt(`I took his cock between my lips and started sucking. `);
			qbb(`Lick my balls too! `);
			txt(`I did everything he asked me to do and compliantly sucked as he rammed his cock in my throat. `);
			txt(`His seed filled my entire mouth, I had to swallow most of it but some cum and drool trickled down on my chin and down on my chest and I was not able to clean it up. `);
			txt(`He just stood there for a while, watching me with a very satisfied, mischievous smile. `);
			
			/*
			if(!att.collar){
				att.collar = 1;
				set.meta();
				kat(`Is that all?!? Can I go! `);
				qbb(`One more thing! `);
				txt(`He conjured a collar and locked it around my neck. `);
				qbb(`The physical version will be soon delivered to you too. I expect you to wear it all the time. You need to be constantly reminded you're nothing more than my whore. `);
			*/
			txt(`He threw me the key and left. I obviously was not able to catch it and I had to blindly search for it, twisting my arms while sitting on the floor. `);
			next(`Leave. `);
			break;
			
			
		case 300:
			mile.b_mas--;
			mile.b_mas--;
			mile.b_vic--;
			mile.b_vic--;
			mile.b_dom++;
			mile.b_dom++;
			mile.b_tripoint = 4;
			mile.sub--;
			mile.sub--;
			emo("suspect");
			mile.b_domina = true;
						
			txt(`Okay, he was just asking for it. I tried to be reasonable but he crossed the line. `);
			link(`Slap him. `, 303);
			link(`Punch him. `, 302);
			link(`Knee him. `, 301);
			break;
			
			
		case 301: //knee
			txt(`I embraced him, my arms around his neck. He probably thought I am about to kiss him but then my knee hit hard between his legs. `);
			txt(`He fell down to the ground, writhing in horrible agony. `);
			qbb(`You bitch! `)	
			txt(`He whined. I lightly kicked him. `);
			kat(`Don't ever call me that again! `);
			txt(`I grabbed him by his hair and pulled him up on his knees. `);
			link(`More. `, 310);
			break;
			
		case 302: //punch
			txt(`I punched him. `);
			txt(`He squealed and held his face. `);
			txt(`I grabbed him by his hair and dragged him down on his knees. `);
			link(`More. `, 310);
			break;
			
		case 303: //slap
			txt(`I slapped him, very hard. And then again, backhanded on the other cheek. `);
			qbb(`W.. what.. `);
			txt(`He stuttered. `);
			txt(`I grabbed him by his hair and dragged him down on his knees. `);
			link(`More. `, 310);
			break;
			
		case 310:
			log_sex("oral", "kat", "qbb");
			showLess(-1, 0, -1);
			emo("smug");
			kat(`I think <i>you</i> should accept that <i>you</i> are a submissive whore! `);
			qbb(`I wo...`);
			txt(`When I saw he was about to say something I would not like I lightly slapped him. His face was red and scared. Good. `);
			kat(`Are you a submissive whore?! `);
			txt(`He hesitated but when I raised my hand again he hastily reluctantly agreed. `);
			qbb(`Yes, I'm a submissive whore! `);
			kat(`And you won't ever call me a slut or whore or bitch or cunt or tramp or anything, do you understand? `);
			qbb(`Yes, I understand. `);
			kat(`Yes, I understand, Mistress. `);
			qbb(`Yes, I understand, Mistress. `);
			txt(`I really liked how it sounded when Letë called me that way. Seeing him so vulnerable and fully under my control was making me rather aroused. I yanked his hair, grinding his face against my crotch. `);
			
			if(mile.b_slap === -1){
				kat(`Hmmm, I think you owe me one extremely humiliating oral sex, don't you, loser? `);
				qbb(`Yes, Mistress. `);
				kat(`You need to beg me! `);
				qbb(`...`);
				kat(`BEG ME! `);
				qbb(`Please, Mistress, may I lick your pussy? `);
				kat(`I'm not sure whether you're worthy of that...`);
				qbb(`Please, Mistress!?`);
				kat(`Well, okay, you may. `);
			}else{
				kat(`Just look at yourself! Such a pathetic trainwreck! And you dared to try to make me suck your cock! `);
				qbb(`I'm sorry! `);
				kat(`I'm sorry, Mistress!`);
				qbb(`I'm sorry, Mistress!`);
				kat(`I think turnabout is fair play, don't you?`);
				qbb(`What do you mean?... Mistress? `);
				
			}
			
			if(wears.pants){
				txt(`His eyes went wide when I unbuttoned my ${wdesc.lower} and stripped them. `);
				showLess(-1,0, 1);
			}else if(wears.pantyhose){
				txt(`His eyes went wide when I stripped my ${wdesc.socks}. `);
				showLess(-1,-1,1); //TODO CHECK
			}
			if(PC.panties){
				kat(`Pull down my panties! `);
				txt(`I let his hair go. He grabbed my panties on both sides and slowly pulled them down. I stood out of them and leaned against a wall, my legs slightly apart. `);
			}else{
				txt(`I leaned against a wall, my legs slightly apart. `);
			}
			kat(`Start licking my cunt, bitch! `);
			txt(`${wears.skirt  ? `His head disappeared under my ${wdesc.lower}` : ""}. I moaned when he started orally pleasing me. His technique was just weak - it was probably for the first time he was giving girl cunnilingus - but the knowledge I made him to do it was getting me going like nothing else. `);
//TODO		
//txt(`His tongue felt over electrifi")
			txt(`I threw my right leg over his left shoulder, holding his head firmly between my thighs. When the orgasm finally came, I had trouble to stay standing. `);
			qbb(`Was it good? `);
			txt(`I wanted to acknowledge his effort - he made me cum pretty hard - but then I remembered how much I hated him. `);
			kat(`You have an audacity to even ask?!?! It was awful! You're completely fucking useless! You'll require a lot of training! `);
			qbb(`<small>I'm sorry, Mistress</small>`);
			
			next(`Bye, loser! `);
			break;
	}
}







export const b_service = (index) => {
	switch(index){
		default:
		case 101: 
			set.irl();
			mile.bbb++;
			mile.b_dom++;
			mile.sub--; //TODO
			showLess(-1,0);
			placetime(`my room`);
			emo("imp");
			
			txt(`I felt especially horny today. Well, since the blackmail started and I was every day pushed into new sexual situations, I was feeling horny almost all the time. So I locked my room, lied comfortably on the bed and reached for my ${dildo()}. Lightly rubbing it over my pussy, I tried to get in the right mood. `);
			txt(`I was trying to recall a steamy fantasy. @qbb? You could guess he usually did not figure in my sexual fantasies. But the way I made him eat my pussy, seeing him on his knees in front of me, showing that impertinent little bitch his proper place. Oh yeah! I keenly touched myself. That was hot! Then something dawned on me and I dropped the dildo. `);
			txt(`Why I was just fantasizing!?! When there was somebody just craving to serve me? And if he was not, well, let me just say I could be very convincing. `);
			link(`Connect to Metaverse. `, 102);
			break;
			
		case 102: 
			set.meta();
			showAll();
			placetime(`@qbb's subreality`);
			
			kat(`Hey, @qbb? Can I come in?! We need to talk about something!`);
			qbb(`Well... sure... what do you need to talk about @katalt?`);
			txt(`He did seem unusually anxious but still allowed me to enter his subreality, I immediately dropped the nice act and slapped him. `);
			kat(`It's Mistress @katalt for you, dumbass! `);
			txt(`Consterned her rubbed his red cheek. `);
			qbb(`I wanted to talk with you about... what happened...`);
			txt(`@qbb began carefully. `);
			kat(`Maybe next time! I'm horny and I need your mouth for other things than talking! `);
			txt(`He twitched when I briskly snapped with my fingers and pointed at my lap. But I saw the fleeting gleam in his eyes when I mentioned how horny I am. `);
			kat(`On your knees and start licking!`);
			qbb(`You can't just barge in and demand me to eat your pussy! I mean... I might do it... but you have to ask more nicely. `);

			link(`Force him. `, 103, ()=> mile.b_service = 2);
			link(`Convince him. `, 103, ()=> mile.b_service = 1);
			break;
			

		case 103:
			showLess(-1,0,2);
			if(mile.b_service === 2){
				mile.sub--;
				mile.b_cruel++;
				emo(`focus`);
				
				txt(`I painfully grabbed his ear:`);
				kat(`Listen you little bitch: Nice @katalt is gone. She had no patience for your stupid power games and being blackmailed. There's only me, a spiteful horny bitch any you do exactly what I tell you. And I will hurt you if you won't. Do you understand!?`);
				txt(`He actually seemed scared! That felt so fucking great. `);
				qbb(`Y... yes. `);
				kat(`Do. You. Understand!`);
				qbb(`Yes!`);
				txt(`I still was not satisfied and slapped him. `);
				kat(`Adress me properly!`);
				qbb(`Yes, Mistress @katalt.`);
			}else{
				mile.b_cruel--;
				mile.b_service = 1;
				
				kat(`Might do it?! `);
				txt(`I burst out laughing. `);
				kat(`I know you want to do it! I know how much you crave it! Admit it, you horny loser! You're desperately trying to act tough but you're a just pathetic submissive wimp. `);
				qbb(`I'm not a pathetic wimp! `);
				txt(`I touched his chest, caressed it, my hand was traveling lower, between his legs. `);
				kat(`Do you want to argue about this or would you rather admit you're a pathetic little bitch and bury your face in my pussy? `);
				qbb(`Well...`);
				kat(`You have five seconds and then I'm leaving. `);
				qbb(`I'm a pathetic little bitch!`);
				kat(`Good boy! `);
			}
			link(`Enjoy his services. `, 110);
			break;
			
		case 110:
			emo(`horny`);
			log_sex("oral", "kat", "qbb");
			
			txt(`I lay back on a dark-green sofa he conjured and allowed him to climb between my legs and start working with his tongue. Fuck, his mouth was so much better than my dildo. Seeing him so badly trying to pleasure me was so empowering. `);
			kat(`Fuck yeah! Don't dare to slow down! `);
			qbb(`Hmmpp!`);
			kat(`That's it! Eat my pussy like your life would depend on it! Oh yeah, that feels good! Come on! `);
			txt(`My fingers were riding through his long hair and I impulsively pressed his face against my body when I was overwhelmed with the bliss and orgasmed. At least for now, my lust was satisfied. `);
			kat(`Not bad! `);
			qbb(`Thank you, Mistress!`);
			txt(`I searched for the ${wears.panties ? PC.panties.name : wdesc.lower} I threw away. @qbb was staring at me. `);
			kat(`What!`);
			qbb(`I was thinking, maybe you could...`);
			txt(`He looked down at his bulging pants. `);
			kat(`Maybe the next time! `);
			txt(`I just mischievously giggled. `);
			next();
			break;
	}
}




export const b_dominatrix = (index) => {
	switch(index){
		default:
		case 101:
			set.meta();
			mile.bbb++;
			mile.sub--;
			mile.b_dom++;
			mile.dominatrix++;
			//quickSaveOutfit();
			emo(`imp`);
			
//TODO - INTRODUCTION? 
			placetime(`my room`);
			txt(`On one hand, I was conflicted about turning @qbb into my submissive slut. I feared the things he was awakening inside me, almost felt guilty for exploiting him and I was not sure if getting sexually closer to somebody like him was a good idea. `);
			
			if(mile.y_dominatrix){
				txt(`On the other hand, my messing around with my willingly submissive twin was fun and provided so much inspiration for how to handle him. I wondered, what sexy outfit should I choose? Should I wear ${choice(1)}, ${choice(2)}, ${choice(3)} or ${choice(4)}? `);
			}else{
				txt(`On the other hand I spend last hour googling how to make a guy suffer and downloading sexier outfits. I could not decide - should I wear ${choice(1)}, ${choice(2)}, ${choice(3)} or ${choice(4)}? `);
			}
			
			effects([{
				id: "initial",
				fce(){
					quickLoadOutfit();
//TODO CHECK IS INITIAL RUN ALWAYS?
				},
			},{
				id: 1,
				label: "leather minidress",
				fce(){
					quickTempOutfit("leatherMinidress");
					//crew(...uniforms.leatherMinidress);
					mile.b_gear = 1;
				},
			},{
				id: 2,
				label: "latex catsuit",
				fce(){
					quickTempOutfit("latexCatsuit");
					//crew(...uniforms.latexCatsuit);
					mile.b_gear = 2;
				},
			},{
				id: 3,
				label: "leather corset",
				fce(){
					quickTempOutfit("leatherCorset");
					//crew(...uniforms.leatherCorset);
					mile.b_gear = 3;
				},
			},{
				id: 4,
				label: "latex minidress",
				fce(){
					quickTempOutfit("latexMinidress");
					mile.b_gear = 4;
				},
			}]);
				
			
			link(`Metaverse. `,102);
			break;

	case 102:
			placetime(`@qbb's subreality`);
			effigy.qbb.showLess(0, 0);
			txt(`@qbb was stunned when I confidently strolled in. I had never worn such fetish clothes - nor especially desired to - but now I loved it. I was hot as hell and that knowledge was filling me with indomitable determination. `);
			kat(`Hey, slut!`);
			qbb(`Hello, Mistress! You look amazing!`);
			kat(`Awww. I'll believe you mean it and not just trying to suck up. `);
			qbb(`I do! You look very sexy! `);
			kat(`Why aren't you greeting me properly? Why are you still dressed? And why you aren't on your knees?`);
			qbb(`Sorry. Right!`);
			txt(`He hastily undressed and kneeled down.`);
			qbb(`Welcome, Mistress! ${mile.b_cruel < 0 ? "Your slave is at your service! " : ""}`);
			txt(`He tried again. `);
			kat(`Good boy!`);
			txt(`I patted him and then sit down on an armchair. I crossed my left leg over the right one and pointed with my left feet on his chest:`);
			kat(`Crawl closer and kiss my @shoe! `);
			txt(`I ordered him. He did not even hesitate, crawled on all fours to me and obediently kissed the tip of my ${wdesc.shoe}. `);
			qbb(`Yes, Mistress. `);
			if(mile.mistress !== "Mistress"){
				kat(`Call me @mis!`);
				qbb(`Yes, @mis!`);
			}else{
				link(`I think I don't like being called <i>Mistress</i>...`, 103);
			}
			link(`Made him continue adoring your feet.`, 104 , ()=> mile.b_dominatrix_f = true);
			link(`Made him pleasure you orally. `, 104, ()=> mile.b_dominatrix_f = false);
			break;
			
	case 103:
			popup.prompt(`You should adress me as: `, mile.mistress, a => mile.mistress = a, 
				[
					`Mistress`, `Mommy`, `Godess`, `Ma’am`, `Miss ${NAME.katsur}`, `Mistress ${NAME.katalt}`, 
				],
			);
			qbb(`What? I won't call you that, that's embarrassing!`);
			kat(`Shut up, nobody asked about your stupid opinions! You'll do everything I'll tell you!`);
			
			link(`Made him continue adoring your feet.`, 104 , ()=> mile.b_dominatrix_f = true);
			link(`Made him pleasure you orally. `, 104, ()=> mile.b_dominatrix_f = false);
			break;
		
	case 104:
			remove("panties");
			log_sex("oral", "kat", "qbb");
			
			if(mile.b_dominatrix_f){
				remove("shoes");
				mile.into_feet++;
				kat(`Make them shine! `);
				txt(`I commanded him to polish my @shoes and his tongue slid over them. `);
				kat(`Good! Now take them off a give me a nice massage!`);
				txt(`His fingers were clumsily but tenderly fondling my feet. It was very comfy. When I was getting bored, I pressed a sole against his face, then I forced my feet in his mouth and made him suck my toes. `);
				kat(`Good boy! That feels nice! I think I'll keep you!`);
				txt(`I giggled. Seeing him instantly following all my orders was making me pretty excited and I was already rubbing myself. `);
			}else{
				kat(`Kiss my other @shoe too!`);
				txt(`I ordered. Seeing him instantly following all my orders was making me pretty excited. I began lightly rubbing myself. `);
			}
			
			kat(`Or would you want more? `);
			txt(`I unabashedly spread my legs wide, placing feet on the armrests of the armchair. `);
			qbb(`Yes, @mis! `);
			txt(`He nodded and crawled closer and covered with kisses my left thigh. The last kiss was on my hidden snatch. `);
			if(wears.bodysuit){
				txt(`The catsuit had a very convenient zipper right in the crotch area. `);
			}else{
				kat(`You may take down my panties!`);
				qbb(`Yes, @mis!`);
			}
			kat(`Let's see whether you're getting better at licking my pussy! `);
			
			txt(`Humiliating him made me so horny I nearly came right away when I felt his hot heavy breath tingling me. I was quickly getting used to his service. Making me cum once was not enough and I forced him to continue to give me more orgasms. `);
			kat(`What? Does your tongue hurt? `);
			txt(`I chuckled when he finally had to take a break. `);
			qbb(`Hmmmm. `);
			
			link(`Make him cum. `, 110, ()=> mile.b_dominatrix = 1);
			link(`Give him blowjob. `, 110, ()=> mile.b_dominatrix = 2);
			link(`Berate him for his lack of stamina. `, 106, ()=> mile.b_dominatrix = -1);
			break;
		
	case 106:
			mile.b_cruel++;
			emo(`smug`);
			txt(`I leaned closer and slapped him. `);
			kat(`Not good enough! I'm extremely disappointed. `);
			qbb(`I... I did my best....`);
			kat(`No excuses! I don't care about your tryining, I want to see results! That wasn't good enough! `);
			txt(`I rubbed with my feet against his crotch. `);
			kat(`I wanted to reward you but I see you don't deserve it. `);
			qbb(`But...`);
			kat(`SHUT UP! We're done here. `);
			next(`Leave. `, quickLoadOutfit);
			break;

	case 110:
			mile.b_cruel--;
			kat(`Oh you poor horny thing! `);
			txt(`I caressed his hair and belittle him. `);
			kat(`But maybe you deserve a reward? What do you think?`);
			txt(`I reached down and rubbed my feet agaist his crotch. `);
			if(mile.b_cruel <= 0){
				qbb(`I.. I think I do...`);
				kat(`Okay. Lie down! `);
			}else{
				qbb(`If... only if you think I deserve it...`);
				kat(`Good boy! Lie down!`);
			}
			//if(mile.b_locked) I ordered and then use my ke
			if(mile.b_dominatrix === 2){
				log_sex("bj", "qbb");
				mile.slut++;
				txt(`I decided to be extra nice and bowed down to take his erect cock in my mouth. My lips were eagerly engulfing him, he was loudly moaning. He was so desperately horny and nearly immediately erupted in my mouth. `);
			}else{
				log_sex("hand", "qbb");
				txt(`I gently wrapped my fingers around his hard and throbbing cock. I began pumping, slowly at first, then faster, until he grunted and erupted.`);
			}
			//TODO? 
			next(`Continue. `, quickLoadOutfit);
			break;
	}
}










 



//GROCERIES - publicly walking on leash (possibly bj two strangers)
export const b_groceries = (index) => {
	switch(index){
		default:
		case 101:
			set.irl();
			placetime(`@qbb's place`);
			quickSaveOutfit();
			
			createWear("slaveCollar");
			updateDraw();
	
			mile.bbb++;
			emo(`shy`);
			

			txt(`@qbb ordered me to visit him, however, this time he wanted to see me in real life. I had no idea what he was planning which made me ${masochist("both scared and excited", "rather terrified")}. I had to ride several stops, @qbb was living in a small family house. He opened the door before I was able to ring for the second time. `);
			qbb(`Hello, slut! `);
			if(mile.sub < 7){
				kat(`Nice to see you too! `);
				qbb(`I'm happy you're in a good mood! ${masochist("", "Although I'm not sure how long it will last...")} Come in! `);
			}else{
				kat(`H... hello!`);
				qbb(`You don't have to be afraid, @katalt! ...well, actually you should be. Come in!`);
			}
			txt(`His room was a horrible mess, books, boxes, dirty plates, disassembled electronics, heaps of scattered clothes everywhere. I was pretty sure I was the first girl who ever visited him. `);
			kat(`So... what do you want to do? ${mile.slut > 5 ? "Should I start undressing? " : ""} `);
			qbb(`You're so eager! ${mile.slut > 7 ? "Not today, you little nympho! " : ""} I have to do some grocery shopping and I thought you might want to join me. `);
			if(mile.sub < 4){
				kat(`Tremendously.`);
			}else{
				kat(`Yeah? `);
			}
			qbb(`Put this on! `);
			txt(`He threw me something. It was a leather collar!`);
			kat(`You want me to wear this?!`);
			qbb(`Obviously. People should be able to see you're my bitch! `);


			link(`Put the collar on with resignation. `, 102, ()=> mile.b_groceries = 2);
			link(`Put the collar on eagerly. `, 102, ()=> mile.b_groceries = 3);
			link(`Do not put the collar on. `, 102, ()=> mile.b_groceries = 1);
			break;

		case 102:
			if(mile.b_groceries === 3){
				mile.b_mas++;
				mile.sub++; //sub?
				kat(`O... okay. `);
				txt(`I slowly put the leather collar around my neck. @qbb turned me around and fastened it tighter. `);
				qbb(`You look lovely! `);
				txt(`I touched the leather collar. It felt so kinky. What will people think? The degrading collar very obviously hinted my subordinate status. 	`);
			}else if(mile.b_groceries === 2){
				mile.b_vic++;
				mile.sub++;
				kat(`Fine! `);
				txt(`I sighed and reluctantly put the collar around my neck. @qbb turned me around and fastened it tighter. `);
				qbb(`You look lovely! `);
				txt(`He ridiculed me. Wearing a collar like a lowly beast felt so degrading. `);
			}else if(mile.b_groceries === 1){
				mile.b_vic++;
				kat(`I don't want people to think I'm your bitch!`);
				qbb(`Too bad, slut! You'll do anything I'll tell you! Come on!`);
				txt(`He grabbed my arm, spun me around and then put the collar around my neck and tightened it.  `);
				qbb(`Here you are! You look lovely!`);
				kat(`I look stupid!`);
				txt(`Wearing a collar like a lowly beast felt so degrading. `);
			}
			txt(`We left his house and went to a tram stop. In the tram there was only one last free seat. He ungentlemanly sat down, I wanted to stay standing but he put his index finger in the ring on my collar and made me sit on his lap, his hand possessively caressing my thigh. ${masochist("He enjoyed pretending I was his girlfriend. ", "I frowned but I knew that making a scene would only attract more attention. ")} After two stops he patted my ass: `);
			qbb(`Let's go!`);
			kat(`Where are we going?`);
			qbb(`To Lidl. `);
			kat(`What? We could continue one more stop!`);
			qbb(`I'd prefer a short walk.`);

			link(`Fine!`, 103);
			link(`Okay?`, 103);
			break;

		case 103:
			placetime(`park`);
	//TODO - give them hoodies
			present(
				[1, "Stoner #1"],
				[2, "Stoner #2"],
			);
			
			qbb(`Wait, one more thing!`);
			txt(`From his pocket, he brought a leather leash and snapped it to my collar. `);
			kat(`What the...`);
			txt(`He carelessly began walking. I was too stunned and the leash tightened. He impatiently yanked with it, making me scuttle forward. `);
			qbb(`Come on, bitch! `);

			if( masochist() ){
				txt(`He acted like I was really just a dog on a leash, forced to obediently follow my master. It felt uncanny. It was early autumn and it was almost dark and we met only a few passersby but I still felt horribly embarrassed. Some seemed disgusted, other amused. My heart was beating faster and the degradation and humiliation were making my pussy prickle. 	`);
			}else{
				txt(`I reflexively reached to the carabine but he quickly warned me: `);
				qbb(`Don't you even think about it!`);
				txt(`It was like a bad dream. It was early autumn and it was almost dark and we met only a few passersby. Still, being so publicly humiliated felt horrible! I was looking forward, too ashamed to meet their stares. `);
			}

			txt(`We took a shortcut across the park. I thought it was a good thing because there will be fewer people there. Until...`);
			npc(1,`Hey! Is that your bitch?! `);

			link(`???`, 104);
			break;


		case 104:
			emo(`shock`);
			txt(`Two guys in hoodies were sitting on the backrest of a park bench and smoking weed. I wanted to quickly pass them but @qbb was excited to show off his new toy. `);
			qbb(`Yeah, she's my bitch! `);
			npc(1,`Cool!`);
			txt(`They laughed like it was the funniest shit they have ever seen. `);
			npc(2,`Can she do any tricks? `);
			qbb(`Yeah. Sit! `);
			txt(`I did not understand what he wants so he roughly pulled down with the leash and made me squat. `);
			npc(2,`Good doggo! `);
			txt(`One of them jumped down and demeaningly ruffled my hair. `);
			npc(1,`Hey, dude, maybe we could borrow her? `);
			txt(`He suggested. My head was on the level of his waist and it was pretty clear what he intended. `);
			qbb(`Hmmm....`);


			link(`Silently wait for his decision. `, 105, ()=> mile.b_groceries_2 = 1);
			link(`Please no!. `, 105, ()=> mile.b_groceries_2 = 2);
			link(`NO! `, 105,  ()=> mile.b_groceries_2 = 3);
			con_link(mile.slut > 6, `low slut`, `Sure, why not! `, 105,  ()=> mile.b_groceries_2 = -1);
			break;



		case 105:
			emo(`help`);
			if( mile.b_groceries_2 !== 1 ){
				if( masochist() ){
					txt(`He immediately lightly slapped me. `);
					qbb(`Shut up, slut! Dogs don't talk! And even if they could talk they wouldn't interrupt their owners!`);
					npc(2,`Damn yeah, show that bitch!`);
				}else{
					txt(`He harshly slapped me.`);
					qbb(`Shut up, slut! Dogs don't talk and nobody gives fuck about your stupid opinions, do you understand!?`);
					npc(2,`Damn yeah, show that bitch!`);
				}
				txt(`I felt so helpless! @qbb was coldly contemplating whether he makes me sexually serve to those stoners and was not allowed to say anything.`);
			}else{
				mile.sub++;
				txt(`I stayed silent, I knew he would not be happy if I interrupted him. But it was so awful and demeaning - @qbb was coldly contemplating whether he makes me sexually serve to those stoners and I was completely helpless. `);
			}

			if( mile.slut > 12 && mile.sub > 6 && !masochist() && mile.b_groceries_2 !== -1 ){ //TODO
				mile.b_groceries_bj = true;
				log_sex("bj", "npc_1");
				log_sex("bj", "npc_2");
				qbb(`Sure, why not! You can have the bitch. But be quick, we have some errands to do!`);
				npc(1,`No problem, dude. With such obedient hot slut I'll be done right away!`);
				npc(2,`Damn yeah! What a hot bitch! `);
				txt(`He handed the first one the leash and he dragged me next to bushes few meters away from the path. There he pushed me down and unbuttoned his pants. `);
				npc(1,`Shit! Your owner is a piece of work, isn't he? If I had such a hot slut at my disposal I would never share it with random assholes! `);
				txt(`He promised and slid his cock in my mouth. `);
				npc(1,`Fuck! Having such a beautiful willing bitch!`);
				txt(`He continued musing while I was sucking him. He indeed did not last for very long. I barely had time to swallow all his cum and he was already passing the leash to his comrade. `);
				npc(2,`Heh! Choke on my cock, bitch!`);
				txt(`He suggested and I started working. `);
				qbb(`What takes you so long, slut! The store will be closing soon!`);
				txt(`Which was bullshit, the store did not close until 9 PM. Finally, even the second stoner cummed and my humiliation was finished. Or so I thought. `);
				qbb(`Why don't you thank the nice guys they let you suck their cock, you little nympho! `);
				if(mile.sub < 9){
					kat(`Thank you. `);
					txt(`My embarrassed, annoyed tone made them all laugh. `);
				}else{
					kat(`T..thank you that you allowed me to suck your cocks!`);
					txt(`I tried to look grateful. `);
				}
				npc(2,`Any time, bitch! `);
				
			}else if( mile.b_groceries_2 === -1 ){
				mile.slut++;
				txt(`@qbb grabbed the leash right below my neck and made me look up at him. `);
				qbb(`You would love that, wouldn't you, you thirsty nympho?! Are you ever thinking about something else than suckings cocks?! `);
				txt(`He accused me. `);
				npc(1, `Well, if she wants cocks so much...`);
				qbb(`Sorry, guys, today she's a bad bitch and doesn't deserve your dicks. Do you? `);
				txt(`He roughly jerked with the leash. `);
				if(mile.sub > 5){
					kat(`I'm sorry. I'm a bad bitch and I don't deserve your dicks. `);
					txt(`I looked down and meekly repeated.  `);
				}else{
					kat(`No, I don't.`);
					qbb(`You don't what!`);
					kat(`No, I don't deserve your dicks!`);
				}
				
			}else{
				qbb(`Hmmm, no. I'm sorry but we have some errands to do! `);
				txt(`The guys were not even disappointed, they did not expect their chances were very high (unlike they were). I sighed. I was so relieved! Well, not for long: `);
				qbb(`Maybe the next time! Come on, bitch!`);
				txt(`He smiled at them, tugged with the leash and made me stand up. `);
				
			}
			
			link(`Lidl.`, 106);
			break;


		case 106:
			placetime(`mall`);
			txt(`We reached the parking lot and I began getting anxious. Dozens of people were shopping there! `);
			if(mile.sub < 3){
				kat(`I can't wear the leash inside! `);
			}else{
				kat(`Please, I can't wear the leash inside! `);
				if(mile.sub > 6) txt(`I desperately begged him. `);
			}
			qbb(`What?! Oh don't worry, you don't have to be afraid! I won't force you to wear the leash inside. `);
			kat(`Thanks! `);
			txt(`I wanted to unclip the leash but he stopped me. `);
			qbb(`Keep it on!`);
			kat(`What?! `);
			txt(`He pointed at a pictogram of crossed dog and then tied the end of the leash to a nearby railing. `);
			qbb(`Wait for me here!`);
			if(mile.sub < 5){
				kat(`You can't be serious!`);
			}else{
				kat(`Oh no!`);
			}
			txt(`I was forced to stand in front of the mall tied to a railing. It was boring and extremely humiliating, especially embarassing were the curious stares of passersby. Fortunately, none of them dared to approach me and ask me what the hell was going on. After long ten hours or so @qbb returned. `);
			qbb(`Don't worry, I'm back. Good girl!`);

			if( masochist() ){
				txt(`He patted me and untied.`);
				qbb(`I even bought you something! `);
				txt(`He gave me a big cherry lollipop. `);
				qbb(`I know you feel bad about not sucking those guys so there's at least something you can suck on!`);
				if(mile.sub < 4){
					kat(`Fuck you!`);
				}else{
					kat(`Thanks a lot!`);
				}
				qbb(`Let's get back!`);
			}else{
				txt(`He patted me and untied me. Then he gave me a heavy bag with the stuff he bought. `);
				qbb(`Let's go!`);
				kat(`So now I'm not only a dog but also a pack mule? `);
				qbb(`Haha. You're funny. But yes, you are. Let's get back!`);
			}
//TODO - BETTER ENDING?			
			next(`Next. `, quickLoadOutfit);
			
		}
}



//BITCH - nipple clamps, crawing, fucked from behind
export const b_bitch = (index) => {
	switch(index){
		default:
		case 101:
			set.meta();
			placetime(`virtual dungeon`);
			mile.bbb++;
			//mile.b_vic++;
			//mile.b_++;
			//mile.b_slave++;
			txt(`All my other plans were canceled when I received a message from @qbb that he wants to see me. `);
			if( masochist() ){
				txt(`I was both scared and excited when I entered the metaverse. `);
			}else{
				txt(`I was very scared what kind of horrible humiliation I will have to suffer. `);
			}
			link(`Enter. `, 102);			
			break;
			
		case 102:
			set.meta();
			showLess(0, -1, 2);
			emo(`help`);//emo?? pain?
			
//TODO - LEASH? 			
			txt(`I materialized in the middle of a dark room. @qbb approached me, his predatory grin was enough to make me shiver. He just let me stand there and walked around, checking me from all angles. `);
			qbb(`How are you, slave? Are you looking forward to serving me? `);

			if(mile.sub > 7 && masochist() ){
				kat(`Yes, Master! `);
				txt(`I docilely nod in agreement. It very suprised me when when I relalised it was true. He smiled. `);
				qbb(`Awesome! `);
			}else if(mile.sub > 7){
				kat(`Yes, Master! `);
				txt(`I docilely nod in agreement. It seemed like the safest answer. I did not want to provoke him. He smiled. `);
				qbb(`Good. You're already starting to understand your position. `);
			}else if(mile.sub > 2){
				kat(`Hmmmmm. `);
				qbb(`Are you mute?! `);
				kat(`No... Master... I just... I'm not sure... `);
				qbb(`Good. You should be afraid! `);
			}else{
				kat(`Sure, I just love being abused so much! `);
				qbb(`Heh, I wonder how long will that attitude last before I break you. `);
			}

			if(wears.tee){
				txt(`Without any warning, he grabbed the neckline of my shirt, sharply yanked and ripped it open. ${wears.bra ? "Then he removed my bra too.  " : ""} My bare ${wdesc.tits} were fully exposed. I instinctively raised my arms to cover myself but he stoped me: `);
			}else{
				txt(`Without any warning, he grabbed my ${wdesc.upper}, sharply yanked and ripped it open. ${wears.bra ? "Then he removed my bra too.  " : ""} My bare ${wdesc.tits} were fully exposed. I instinctively raised my arms to cover myself but he stopped me: `);
			}

			qbb(`Put the hands behind your head. `);
			txt(`I reluctantly raised my arms. @qbb mischievously lightly touched my elbow and his fingers slid down, across my arm and armpit, to my hip. I squirmed because it tickled. `);
			txt(`Then he continued tormenting my poor tits. He squeezed them and slapped them around, he loved my obvious discomfort and embarrassment. ${mile.meta_boobs >1 ? "My huge boobs were wildly jiggling. " : ""} `);
			qbb(`Now put on these! `);
			txt(`@qbb handed me two plastic clamps. `);
			kat(`Put where!? `);
			qbb(`On your nipples! `);
			kat(`But... that will hurt! `);
			if( masochist() ){
				qbb(`You're a big girl, you can handle it. `);
				kat(`If you say so...`);
				txt(`He gently slapped me: `);
				qbb(`Come on, @kat! I trust you! `);
			}else{
				qbb(`Oh yeah! It will! Horribly! `);
				kat(`But...`);
				txt(`He meanly slapped me: `);
				qbb(`No buts! `);
			}
			txt(`I took the clamp and jitterily placed it around my nipple. Then I very slowly let it loose, bitting in my soft flesh. My heart was rapidly pounding, my breath was quick and shallow. I was trying to suppress mild pain. It was not as horrible as @qbb claimed, I was able to withstand it. `);
//TODO WHAT COLLAR???
			txt(`He snatched me by the collar and clipped a leash to it. `);
			link(`Next. `, 103)
			break;
			
			
		case 103:
			emo(`unco`);//emo??
			effigy.qbb.showLess(-1, 0);
			showLess(0,0,2);
			kat(`What are you doing? `);
			if( masochist() ){
				qbb(`Who are you? `);
				kat(`What? `);
				qbb(`WHO ARE YOU? `);
				txt(`He yanked with the leash. `);
				kat(`I'm a bitch? `);
				qbb(`He smiled. `);
				kat(`I'm your bitch! `);
			}else{
				qbb(`Are you my bitch? `);
				kat(`I..`);
				qbb(`ARE YOU MY BITCH? `);
				kat(`...yes? `);
				qbb(`Yes what!?! `);
				kat(`I'm your bitch. `);
				qbb(`What are you?!? `);
				kat(`I'M YOUR BITCH! `);
			}
			qbb(`Then get on all fours and crawl like a bitch! `);
			txt(`I dropped on my hands and knees and crawled as he led me around the room, carelessly pulling the leash. `);
			qbb(`Good girl! `);
			txt(`I am embarrassed to admit this, but my body very strongly reacted to his condescending commendation. `);
			txt(`@qbb spanked my ass several times (it was a very inviting and vulnerable target) and then ${wears.skirt ? "rolled up my @lower" : "order me to pull down my @lower"}. His fingers were roughly examining my ${wdesc.pussy}. `);
			qbb(`Damn, you're so wet, slut! You're into this more than I am! `);
			txt(`He mocked me. I felt ashamed but I could not deny the truth when the proof was so evident. And his poking was making my hornyness even worse. `);
			qbb(`I'm going to fuck you from behind like a bitch you are! And you'll enjoy it, won't you, you submissive nympho? `);
			txt(`I could feel him removing his pants and then his erected manhood pressed against my body. He rubbed the head of his cock against my horny pussy and I shivered in anticipation. `);
			
			con_link(!masochist(), `masochist`, `No, don't do it! `, 104, ()=> mile.b_bitch = -2 );
			link(`Stay defiant. `, 104, ()=> mile.b_bitch = -1 );
			link(`Oh yes! Fuck me hard! `, 104, ()=> mile.b_bitch = 1);
			con_link(masochist() && mile.slut > 5, `not masochistic slut`, `Oh no! Please no! Don't brutally ravish my poor pussy with your huge cock! `, 104, ()=> mile.b_bitch = 2);
			

			break;

		case 104: 
			emo(`horny`);
			log_sex("sex", "qbb");
			
			if(mile.b_bitch === -2){
				mile.b_vic++;
				txt(`He violently yanked the leash. `);
				qbb(`What the fuck? You're still rebellious? Should I beat your ass until you'll be begging me for my dick? Or would you prefer to skip the pain?`);
				kat(`...`);
				txt(`He viciously smacked my ass. As demeaning it was to beg, I indeed preferred to skip the pain:`);
				kat(`P...please, give me your cock!`);
				qbb(`Remember your place, bitch!`);
			}else if(mile.b_bitch === -1){
				mile.b_vic++;
				txt(`My body wanted it but I kept my mouth shut. I would never admit his abuse was making me aroused! `);
			}else if(mile.b_bitch === 1){
				mile.b_mas++;
				txt(`I shamelessly moaned, I needed his cock inside me. `);
				qbb(`You perverted slut! `);
				txt(`He heartily chuckled and respectfully slapped my ass. `);
			}else if(mile.b_bitch === 2){
				mile.b_mas++;
				qbb(`I don't care what you want, bitch! `);
				kat(`Oh no! Why I'm so weak and helpless and completely at your mercy! Please, don't fuck me hard! `);
				txt(`I countinued teasing him and he deviously laughed:`);
				qbb(`Your resistance only makes me harder!`);
				kat(`Oh no! Poor me! `);
			}
			
			txt(`@qbb plunged his cock inside me and began roughly fucking me from behind. He even entwined the leash around his wrist and pulled it closer, forcing me to arch my back which let him penetrate me with more force. `);
			txt(`He was pounding me hard. I was indeed groaning like a whore. My mind was gone, I was lost in the pleasure and not able to form a single thought.  ${!wears.smallTits ? `My ${wdesc.tits} were swaying fort and back so violently that the clamp flew off my left tit and the one on my right nipple was holding only barely. ` : ""} `);
			if(mile.b_bitch < 0){
				kat(`Oh fuck! `);
			}else{
				kat(`Yeah! Yeah! I'm your bitch! `);
			}
			txt(`I was howling when he made me cum for the first time. But he was not stopping, continued fucking me until he filled me with his load. `);
			
			if( masochist() ){
				txt(`I felt so hazy and drained. @qbb embraced me, removed the remaining clamp which did not fall during our intercourse. `);
				qbb(`Damn, you're one horny bitch! `);
				txt(`He smiled and embraced me, gently fondling my body until I regained my balance. `);
				qbb(`Good girl! `);
			}else{
				txt(`Then he pulled his flaccid dick out, his cum was leaking from me on the floor, and slapped my ass again. `);
				qbb(`Good bitch! `);
			}
			next();
			break;
	}
}







//QBB SHOPPING (KAT) DOM
export const b_shopping_dom = (index)=> {
	const panties_slut = 7; //lower allows hipsters
	const upper_slut = 11; //unlock fishnet top
	const socks_slut = 13; //high socks X open crotch
		
	const back = function(){
		const extra = [
			"blackThong", 
			"fishnetShrugTop",
			"fishnetStockings",
			"stripedDarkHighSocks",
			"crossedChoker",
		];
		extra.forEach(   key => PC.inventory.push( create(key) )   );
	
		removeEverything(PC, PC.inventory); //TODO - check if works
		quickLoadOutfit();	
	}
	switch(index){
		default:
		case 101: 
				set.irl();
				placetime("Tram Stop");
				quickSaveOutfit();

				txt(`I gave a call to @qbb. I ordered him to drop everything he was doing - assuming there was nothing as important as serving me - and meet up with me.`);
				kat(`Hey.`);
				qbb(`Hello.`);
				if(mile.b_cruel >= 0){
					kat(`Did you already fucking forget how you're supposed to address me!?`);
					txt(`I dressed him down for his casual attitude. Us being in public does not mean he could treat me without respect.`);
					qbb(`Sorry, @mis.`);
				}
				kat(`You are surely curious about where are we going.`);
				qbb(`I am, @mis.`);
				txt(`I smiled, I very much enjoyed being called that.`);
				kat(`Since your stupid tasks are making me alter my fashion style, I think it is only fair if the responsible person will help me to get some new clothes. Help me to choose them and pay for them. Don't you think?`);
				qbb(`...yeah... I guess that sounds fair.`);
				if(mile.clinic_boobjob > 0){
					kat(`Especially since I got my new boobs, none of my old tops fits properly.`);
					txt(`I pointed at my breasts.`);
					qbb(`Y...yeah!`);
				}
				txt(`We got on a tram and rode downtown.`);
				kat(`Okay, first we'll go... hmmm...`);
				qbb(`@mis? If you allow me to have a suggestion, I know about a shop where you can get clothes that make you look totally badass.`);
				kat(`I'm listening!`);
				qbb(`Follow me!`);

				link(`Shopping. `, 102);	
				break;

		case 102: 
			placetime("Alternative Fashion");
			showLess(1,2);
			
			txt(`We went to a store I did not know before. It was an underground shop, both literally and figuratively. It was selling merch of metal bands and punk and goth fashion.`);
			qbb(`Where would you like to start? Maybe getting you some sexy lingerie?`);
			kat(`You pathetic horny perv! ...but sure, why not, we have to start somewhere.`);
					
			if(mile.slut > panties_slut){
				txt(`I enjoyed their selection of underwear. I got an amazing ${choice("bra", "bras")} and then went through panties. @qbb watched with amazement how I could not decide whether to buy alluring black ${choice("thong", "thong")} or even sluttier minuscule ${choice("gStrings", "g-string")}.`);
			}else{
				txt(`Their selection of underwear was nice. I got an amazing ${choice("bra", "bras")} and then could not decide between comfortable  ${choice("panties", "panties")} and sexy ${choice("thong", "thongs")} .`);
				qbb(`What about ${choice("gString", "this")}?`);
				txt(`@qbb brought something which at first look seemed to be too tiny to be panties but closer examination revealed it was a g-string.`);
			}

			if(mile.slut > socks_slut){
				txt(`After getting the underwear I was looking for something to emphasize my long legs. They offered brightly ${choice("stockings", "colored")} stockings or ${choice("fishnetStockings", "fishnet")} ones. Or even whole ${choice("fishnetPantyhose", "fishnet pantyhose")}, including especially  ${choice("openCrotch", "lewd pantyhose")} with open crotch. I would not have to even remove them before getting fucked.`);
			}else{
				txt(`After getting the underwear I was looking for something to emphasize my long legs. They offered showy ${choice("pantyhose", "brightly colored pantyhose")} and cute ${choice("socks", "high socks")}. However, @qbb was eying lewd  ${choice("fishnetStockings", "fishnet stockings")} and ${choice("fishnetPantyhose", "fishnet pantyhose")}.`);
			}

			link(`Continue. `, 103);	
			
			effects([
				{
					id: "initial",
					fce(){
						remove("bra");				
					},
				},
				{
					id: "bra",
					fce(){
						wear( create("blackCageBra") );
						updateDraw(PC);
					},
				},
			]);
				
		
			effects([
				{
					id: "initial",
					fce(){
						remove("socks");				
					},
				},
				{
					id: "socks",
					fce(){
						wear( create("stripedDarkHighSocks") );
						updateDraw(PC);
					},
				},
				{
					id: "pantyhose",
					fce(){
						wear( create("polyPantyhose") );
						updateDraw(PC);
					},
				},
				{
					id: "stockings",
					fce(){
						wear( create("polyStockings") );
						updateDraw(PC);
					},
				},
				{
					id: "fishnetStockings",
					fce(){
						wear( create("fishnetStockings") );
						updateDraw(PC);
					},
				},
				{
					id: "fishnetPantyhose",
					fce(){
						wear( create("fishnetPantyhose") );
						updateDraw(PC);
					},
				},
				{
					id: "openCrotch",
					fce(){
						wear( create("crotchOpenFishnetPantyhose") );
						updateDraw(PC);
					},
				},
			]);

			effects([
				{
					id: "initial",
					fce(){
						remove("panties");		
						counter.panties = 0;
					},
				},
				{
					id: "panties",
					fce(){
						wear( create("blackHipsters") );
						counter.panties = 1;
						updateDraw(PC);
					},
				},
				{
					id: "thong",
					fce(){
						wear( create("blackThong") );
						counter.panties = 2;
						updateDraw(PC);
					},
				},
				{
					id: "gString",
					fce(){
						wear( create("blackCagePanties") );
						counter.panties = 3;
						updateDraw(PC);
					},
				},
			]);
			
			break;
	
	
	case 103: 
			showAll();
			txt(`Then we finally got to the tops. I was not going to buy a tee with a logo of a band I never heard about or with a cringy joke. I was deciding between a comfortable elegant ${choice("tee", "tee")} or more suggestive and cool ${choice("corset", "corset")}.`);
			if(mile.slut > upper_slut){
				txt(`@qbb discovered a ${choice("fishnet", "top made of fishnet. ")}`);
				kat(`I'm not the shrewd highlander, to arrive neither naked nor clothed!`);
				qbb(`It's just a suggestion... If you wanted people to see your bra... or that you're not wearing any...`);
			}
			txt(`When he was already paying, I also picked ${choice("long", "one")} of the ${choice("short", "skirts")}`);
			//TODO - skirt longer description
			kat(`How do I look?`);
			qbb(`Almost perfect.`);
			kat(`Almost?!?!?`);
			qbb(`Something is missing...`);
			link(`Accessories. `, 104);	
			
			effects([
				{
					id: "initial",
					fce(){
						remove("lower");				
						remove("belt");		
					},
				},
				{
					id: "long",
					fce(){
						wear( create("blackPleatedSkirt") );
						wear( create("darkWideBelt") );
						updateDraw(PC);
					},
				},
				{
					id: "short",
					fce(){
						wear( create("blackBorderlineSkirt") );
						wear( create("darkWideBelt") );
						updateDraw(PC);
					},
				},
			]);
			
			effects([
				{
					id: "initial",
					fce(){
						remove("upper");				
					},
				},
				{
					id: "tee",
					fce(){
						wear( create("blackNiceTee") );
						updateDraw(PC);
					},
				},
				{
					id: "corset",
					fce(){
						wear( create("blackCorsetTop") );
						updateDraw(PC);
					},
				},
				{
					id: "fishnet",
					fce(){
						wear( create("fishnetTee") );
						updateDraw(PC);
					},
				},
			]);
			break;
		
	case 104: 
			kat(`What is missing?`);
			if(ext.rules.choker || mile.advice_2 === 2){ //basically mile.choker === true
				mile.b_choker = true;
				qbb(`The final flourish. Maybe a new ${choice("choker", "choker")}.`);
				kat(`To show everybody how big submissive slut I am?`);
				txt(`He was embarrassed when I reminded him his own stupid words.`);
				qbb(`Or ${choice("not", "not")}.`);
				kat(`Come on, would a choker made me look submissive?`);
				qbb(`I don't think anything would make you look submissive.`);
				kat(`Maybe we should get ${choice("you", "you")} one. To show everybody you're ${mile.b_cruel >= 0 ? "my little bitch" : "my good boy"}.`);
				qbb(`Ehhh...`);
				txt(`He looked uncomfortable when I gleefully tightened the choker around his neck.`);
			}else{
				mile.choker = true;
				qbb(`The final flourish. Maybe a ${choice("choker", "choker")}? Chokers look extremely sexy, don't they? Or ${choice("not", "not")} if you wouldn't like it?`);
			}
			kat(`What else?`);
			qbb(`Something cool. ${choice("fishnet", "Fishnet sleeves")}? ${choice("gloves", "Gloves")}?`);
	//TODO ADD MORE 
			txt(`We went to the counter and @qbb paid for everything I picked.`);
			qbb(`Ehh... @kat? I mean @mis?`);
			kat(`Yeah?`);
			qbb(`I was thinking, without complaining I spent so much money all the stuff for your... maybe you should reward me somehow? Not because I would deserve it of course, but just to motivate me to be as obedient in the future?`);
			kat(`...yeah, I guess that makes sense.`);
			qbb(`Probably the easiest and simplest for you might be to reward me sexually. Maybe the fitting room might provide enough privacy...`);
			kat(`Beware, you're crossing the line and you don't want to make me angry.`);
			qbb(`<small>Sorry. </small>`);
			
			if(mile.slut < 3){
				txt(`The idea of getting sexual in the fitting room with him was making me embarrassed. On the other hand, I did not want him to think that anything could make me feel embarrassed, I wanted to look dominant and totally confident.`);
			}
			kat(`Hmmm... fine, let's go.`);
			txt(`@qbb obediently followed me to the fitting room.`);
			
if(!mile.b_locked){
			link(`I guess I can give you a quick BJ.  `, 120);
}
			link(`I allow you to eat my pussy.  `, 110);
			
	
			effects([
				{
					id: "initial",
					fce(){
						remove("gloves");
					},
				},
				{
					id: "gloves",
					fce(){
						wear( create("fingerlessGloves") );
						updateDraw(PC);
					},
				},
				{
					id: "fishnet",
					fce(){
						wear( create("fishnetSleeves") );
						updateDraw(PC);
					},
				},
			]);
			
			
			effects([
				{
					id: "initial",
					fce(){
					},
				},
				{
					id: "you",
					fce(){
						wear( create("choker"), effigy.qbb );
						updateDraw(effigy.qbb);
					},
				},
			]);
			
			
			effects([
				{
					id: "initial",
					fce(){
						remove("collar");
					},
				},
				{
					id: "choker",
					fce(){
						wear( create("blackChoker") );
						updateDraw(PC);
					},
				},
				{
					id: "not",
					fce(){
						remove("collar");				
						updateDraw(PC);
					},
				},
			]);
			break;
			
		case 110:
			mile.b_shopping_dom = 3;
			emo("bliss");
			log_sex("oral", "kat");
			{
				effigy.qaa.showLess(-1, 0);
				const strip = rollUpSkirt(3, 0);
				txt(`The stall was tiny, two people barely fit there. @qbb eagerly watched how ${strip}.`);
			}
			kat(`That is what you want, don't you? You love eating out my pussy, you little slut, don't you?`);
			qbb(`I do, @mis.`);
			txt(`He nodded and squatted down in front of me. I grabbed his head and pressed it against my crotch. It felt so nice when his warm, wet tongue began exploring me down there.`);
			kat(`Good boy, you're quickly learning what I like, right?`);
			txt(`We were in public, separated only by a curtain. But not only the idea we might get caught was exciting. I almost wanted somebody to open the curtain and be shocked, amazed and impressed by how well I trained my slave to pleasure me everywhere and everywhen I desired.`);
			kat(`Yes! Yes, that feels good!`);
			txt(`I instructed him and reflexively toyed with his long hair and ears. He licked and sucked until he brought me to a lovely climax. Free clothes and orgasm on top of that? Yeah, I had a very good day.`);
			
			next(`Go home. `, back);
			break;
		
		case 120:
			mile.b_cruel--;
			mile.b_shopping_dom = 2;
			emo("imp");
			log_sex("oral", "qbb");
			
			qbb(`YEAH! I mean, I'm extremely grateful, @mis!`);
			kat(`You should be!`);
			txt(`The stall was tiny, almost claustrophobic, two people barely fit there. @qbb was not waiting for anything and hastily unbuttoned his jeans. I squatted down and I was met by his rock-hard dick. I tenderly squeezed it with my fingers and took the tip between my lips.`);
			qbb(`Oh yes!`);
			txt(`He was moaning, his eyes closed, enjoying the amazing things I was able to do with my tongue. Even though I was a bit sloppy, I want to make him quickly cum to reduce the chance we get caught. I kinda liked the shop and wanted to visit it again, not being thrown out and banned for life.`);
			qbb(`Ahhh! I'm cumming!`);
			txt(`My mouth was instantly filled with his cum. I slowed down, swallowing everything, not wanting to risk any cum getting on my clothes.`);
			qbb(`T.. thank you, @mis!`);
			kat(`What the hell was that? You're supposed to warn me in advance, not when you're already cumming!`);
			qbb(`Sorry! I'll be more careful... the next time?`);
			txt(`His voice was full of hope there might be a next time. I just enigmatically smirked.`);
			
			next(`Go home. `, back);
			break;
			
	}
}




//QBB SHOPPING (KAT) SUB
export const b_shopping_sub = (index)=> {
	const fishnet_slut = 12; //lower corset 
	const back = function(){
		mile.collar = true;
		const extra = [
			"blackThong", 
			"blackFishnetShrugTop",
			"fishnetStockings",
		];
		//if(mile.slut < fishnet_slut) extra.push("blackCorsetTop");
		extra.forEach(   key => PC.inventory.push( create(key) )   );
		/*
		if(counter.bot + counter.top + counter.panties > 0 && mile.slut < 11) mile.slut++;
		if(counter.bot + counter.top + counter.panties < 0 && mile.slut > 10) mile.slut--;
		*/
		removeEverything(PC, PC.inventory); //TODO - check if works
		quickLoadOutfit();	
	}
	switch(index){
		default:
		case 101: 
				set.irl();
				placetime("Tram Stop");
				quickSaveOutfit();

				txt(`@qbb gave me a call. He ordered me to drop everything I was doing and meet with him.`);
			{
				const appearance = sluttiness();
				const slut = appearance.value;

				if(masochist() ){
					qcc(`How are you doing, slut?`);
					kat(`I was doing pretty well until your call.`);
					qbb(`Cheer up, I have good news. I'm satisfied with how obedient and submissive you are and concluded you deserve a reward.`);
					kat(`What reward?`);
					qbb(`I have a hint. What do all girls like?`);
					txt(`How should I know what he imagined in his sexist mind?`);
					kat(`...dicks?`);
					qbb(`You little nympho! All you're thinking about are dicks! No, I meant I'm taking you to buy you some nice clothes. All women love shopping, don't they?`);
					txt(`That did not sound too terrible. But since it was him, I still suspected he was planning something very terrible.`);
				}else if(slut > 2){
					qbb(`Nice outfit.`);
					txt(`@qbb smiled at me, pleased at how much skin I was showing.`);
					kat(`Thanks.`);
					qbb(`It seems we finally taught you to express your innate whorishness without shame.`);
					kat(`Thanks a lot.`);
					qbb(`Although it still isn't perfect. Not the *bad girl* style I imagined. But don't worry, we're going to fix it today!`);
					kat(`Fix how?`);
					qbb(`I'm taking you shopping. Isn't that exciting? All girls love shopping for clothes, don't they?`);
					txt(`@qbb smirked.`);
				}else{
					qbb(`What the hell are you wearing?`);
					kat(`I'm wearing ${wears.dress ? "@upper" : "@upper, @lower"} and @shoes.`);
					txt(`I shrugged.`);
					qbb(`I wasn't talking literally! We're trying our best to find you a new style that would allow you to freely express your innate whorishness without shame but you're still sabotaging our efforts.`);
					kat(`I'm fine with my style.`);
					qbb(`Yes, that's why is a good thing your opinion does not matter. It seems the tasks are not enough, you need a bit of direct micromanagement. Don't worry, we're going shopping.`);
				}
			}
			link(`Go shopping. `, 102);	
			break;

	case 102:
			placetime("Alternative Fashion");
			showLess(1,2);
			txt(`We went downtown to a store I did not know before. It was an underground shop, both literally and figuratively. It was selling merch of metal bands and punk and goth fashion.`);
			//if(false){ //TODO - disabled when locked? 
				qbb(`Let's start with underwear.`);
				if(mile.no_panties){
					kat(`Don't forget, I'm not allowed to wear any underwear.`);
					qbb(`I'm not forgetting. I'm getting you risqué lingerie only for a sexy time, to make unwrapping you more enjoyable.`);
				}else if(ext.stats.commando > 5 && !wears.panties){
					kat(`I'm doing okay without any underwear.`);
					qbb(`Of course, you do, you little slut. I'm just getting you risqué lingerie only for a sexy time, to make unwrapping you more enjoyable.`);
				}else if(mile.no_bra){
					kat(`I hope you didn't forget I'm not allowed to wear bras.`);
					qbb(`I'm not forgetting, you're still not allowed to wear any brassies. I'm just getting you risqué lingerie for a sexy time, to make unwrapping you more enjoyable.`);
				}
				txt(`He went through selection of sexy black ${choice("bra", "bras")} and matching ${choice("thong", "thongs")} and ${choice("gString", "g-strings")}`);
			
				if(mile.slut < 6){
					kat(`Can't you buy me any normal panties?`);
					qbb(`Wearing boring briefs would be just disrespectful to your lovely ass.`);
				}
			//}
			qbb(`Heh, and this will suit you.`);
			txt(`He handed me a package with ${choice("pantyhose", "pantyhose")}. They consisted of a transparent fishnet and had a huge opening in the crotch.`);
			qbb(`They are very handy, you can get fucked without even removing them!`);
			
			link(`Continue. `, 103);	
			
			effects([
				{
					id: "initial",
					fce(){
						remove("bra");				
					},
				},
				{
					id: "bra",
					fce(){
						wear( create("blackCageBra") );
						updateDraw(PC);
					},
				},
			]);
	
			effects([
				{
					id: "initial",
					fce(){
						remove("panties");				
					},
				},
				{
					id: "thong",
					fce(){
						wear( create("blackThong") );
						updateDraw(PC);
					},
				},
				{
					id: "gString",
					fce(){
						wear( create("blackCagePanties") );
						updateDraw(PC);
					},
				},
			]);
	
			effects([
				{
					id: "initial",
					fce(){
						remove("socks");				
					},
				},
				{
					id: "pantyhose",
					fce(){
						wear( create("crotchOpenFishnetPantyhose") );
						updateDraw(PC);
					},
				},
			]);
			break;
	
	case 103: 
			showAll();	
			txt(`We moved to the section with skirts and tops. The fitting room was a bit claustrophobic, the shop was not big and the owners tried to cram as many wares into as small space as possible. @qbb made me try on different outfits and ${masochist("it was quite fun to try different crazy looks. ", "I did not enjoy playing his helpless dress-up doll. ")} Eventually, he found a ${choice("skirt")} he liked.`);
			if(mile.slut < 5){
				kat(`Are you crazy? You don't expect me to wear this?!`);
				qbb(`You don't like the black color?`);
				kat(`It's too short! It's very generous to even call this narrow band a skirt!`);
				qbb(`Nah, it's as short as it's supposed to be! Implying enough while still not showing everything.`);
				kat(`It shows way too much!`);
				txt(`I vainly protested when I watched my reflection in a large mirror.`);
			}else if(mile.slut < 14){
				kat(`Isn't it a bit too short?`);
				//qbb(`Nah, it's as short as it's supposed to be! Giving enough to the imagination while still not showing everything.`);
				qbb(`Nah, it's as short as it's supposed to be! Implying enough while still not showing everything.`);
				kat(`It shows way too much!`);
				txt(`I would not even dare to call the narrow band around my hips *skirt* but @qbb just smirked and handwaved all my protests.`);
			}else{
				kat(`Well, it's a good thing you bought me new panties...`);
				txt(`I commented while observing myself in a large mirror. It was generous to call the narrow band around my hips *skirt*.`);
			}
			
			
			if(mile.slut < fishnet_slut){
				txt(`With the tiny skirt, I was supposed to wear a dark ${choice("corset")}. He was delighted when he was tightening it:`);
				if(mile.boobs > 1){
					qbb(`Corsets are perfect for you. They would help you both to support your new huge tits and make your waist seem thinner.`);
					kat(`Are you saying I'm fat or what?`);
					qbb(`No, just that you might lose a few kilograms.`);
				}else{
					qbb(`Corsets are perfect for you. They would both help you to emphasize your chest and make your waist seem thinner.`);
					kat(`Are you saying that I'm fat or that my boobs are too small?`);
					qbb(`A little bit of both.`);
				}
				txt(`@qbb continued making fun of me.`);
			
			}else{
				txt(`I was stunned by what he chose to wear with the skirt. I was supposed to wear a ${choice("shirt")} completely made of fishnet.`);
				kat(`It's almost like I was wearing nothing at all!`);
				qbb(`Don't be such a drama queen, it covers exactly enough. This time I even allow you to wear a bra.`);
				kat(`This time?!?`);
				if( masochist()  ){
					qbb(`Come on, you exhibitionistic slut! Don't tell me you don't want to try it. You're already imagining yourself walking on streets wearing just the fishnet, flashing your ${mile.boobs > 1 ? "fake" : ""} tits to everybody, aren't you?`);
				}else{
					qbb(`Yeah. I think the fishnet suits you. Whores like you don't deserve more, they are supposed to share their bodies with everybody, not bashfully hide them. Walking on the streets completely topless might be too overt and lowbrow but this is just perfect. Make it clear you just didn't accidentally lose your top but that you're intentionally dressing this slutty way.`);
				}
				txt(`@qbb just smirked. `);
			}
			
			link(`Accessories. `, 104);	
			
			effects([
				{
					id: "initial",
					fce(){
						remove("lower");
						remove("belt");
					},
				},
				{
					id: "skirt",
					fce(){
						wear( create("blackBorderlineSkirt") );
						wear( create("darkWideBelt") );
						updateDraw(PC);
					},
				},
			]);
			
			effects([
				{
					id: "initial",
					fce(){
						remove("upper");
					},
				},
				{
					id: "corset",
					fce(){
						wear( create("blackCorsetTop") );
						updateDraw(PC);
					},
				},
				{
					id: "shirt",
					fce(){
						wear( create("fishnetTee") );
						updateDraw(PC);
					},
				},
			]);
			break; 
			
	case 104:
			qbb(`Turn around again! Yeah... that will do... although something is missing...`);
			if( (!wears.choker && !wears.collar) && mas_vic() > 1 && mile.choker ){ //TODO practicly no chance for this to happen
					kat(`Yeah... I feel weird without a choker around my neck...`);
					txt(`I was stunned I actually said that aloud, @qbb smiled and nodded.`);
					qbb(`Exactly!`);
			}else{
					kat(`What is missing?`);
					qbb(`Hmm... I know!`);
			}
			txt(`We went to the shelf with various accessories, many of which seemed more fitting for BDSM séances rather than everyday wearing.`);
			
			if(mile.collar){
				qbb(`A brand new ${choice("collar")}! You'll have more choices about what to wear to school!`);
			}else{
				qbb(`This will be perfect. Your very own ${choice("collar")}!`);
			}
			
			if( mas_vic() > 2 ){ 
				kat(`Oh!... Thank you....`);
				txt(`I felt jittery when he gently moved my hair away,  kissed my neck and then firmly fastened the collar. I could not help it, being collared triggered a strange excitement and titillation.`);
				qbb(`Does wearing a collar make you wet?`);
				txt(`@qbb whispered into my ear.`);
				if(mile.slut > 6){
					kat(`M... maybe a little...`);
				}
				txt(`He smirked and made a few steps back to admire my look.`);
			}else if( masochist() || mile.sub > 12 ){
				kat(`Oh!`);
				txt(`I gasped. I tried to stay calm and not reveal the strange excitement and titillation I felt when he moved my hair away and fastened the collar around my neck.`);
				qbb(`Haha, you are all tense. Does just wearing a collar make you wet?`);
				txt(`@qbb mocked me, then he made a few steps back to admire my look.`);
			}else{
				kat(`Oh, come on!`);
				txt(`I sighed with resignation. It did not stop him, he moved my hair away and then firmly fastened the collar around my neck. I reached to the collar, trying to readjust it, it felt so tight and humiliating.`);
				qbb(`No touching!`);
				txt(`He warned me and smiled, then made a few steps back to admire my look.`);
			}
			
			if(mile.collar){
				//I was sure if I dare wear i
			}else{
				qbb(`The collar suits you! You can even wear it to school!`);
				kat(`I would prefer not to, it might a bit hard to explain.`);
				qbb(`Just say it is a sign of you being a submissive slut, I'm sure people will understand!`);
				txt(`@qbb mischievously chuckled.`);
			}
			
			txt(`What do you think about your new style?`);
			
				
			con_link(masochist(), `not masochist enough`, `I love it!  `,  105,  ()=> mile.b_shopping_sub =  6);	
			link(`I like it. `,  105,  ()=> mile.b_shopping_sub =  5);	
			link(`I do look quite sexy.  `,  105, ()=>	mile.b_shopping_sub =  4);	
			link(`Not quite what I would prefer. `,  105, ()=>	mile.b_shopping_sub =  3);
			link(`I look and feel stupid. `,  105, ()=>	mile.b_shopping_sub =  2);
			link(`I hate it and I hate you! `,  105, ()=> mile.b_shopping_sub =  1);
				
			effects([
				{
					id: "initial",
					fce(){
						remove("collar");
					},
				},
				{
					id: "collar",
					fce(){
						wear( create("everydayCollar") ); //TODO - everyday collar or different? 
						updateDraw(PC);
					},
				},
			]);
				
			break;
				
			
	case 105: 
			//likes
			if(mile.b_shopping_sub >= 5){
				mile.b_mas++;
				if(mile.b_shopping_sub === 6){
					txt(`I shrugged, I loved how sexy and shameless looked the girl in the mirror.`);
				}else{
					txt(`I reluctantly admitted. I kinda liked how sexy and shameless looked the girl in the mirror.`);
				}
				qbb(`Good girl!`);
				txt(`He possessively patted my ass.`);
				
			//hates
			}else if(mile.b_shopping_sub <= 2){
				mile.b_vic++;
				if(mile.b_shopping_sub === 2){
					qbb(`Of course you do and are, my little bimbo. But don't worry, you're in a very good care.`);
					txt(`He meanly laughed.`);
				}else{
					mile.sub--; //sassy
					qbb(`Good, I love your cheekiness. It make so much more fun to break you!`);
				}
				
			//neutral
			}else{
				mile.sub++; //being indifferent
				if(mile.b_shopping_sub === 3){
					txt(`I reluctantly admitted, the girl in the mirror looked good, I could not deny that.`);
				}else{
					qbb(`Don't worry, you'll get used to it!`);
					txt(`He was unfazed by my answer.`);
				}
			}
			
			txt(`I thought this was everything and that he will take me to the counter to pay for everything but he grabbed me by the ${wears.collar ? "collar" : "choker"} and dragged me in the opposite direction.`);
			kat(`Where are we going now?`);
			qbb(`You haven't thought I'm going to waste so much money on a bitch like you for free? I want my reward.`);
			if( mas_vic() > 1 || mile.slut > 13){
				kat(`A sexual reward?`);
				qbb(`Of course. Is there anything else you're good for?`);
				txt(`@qbb pushed me to the fitting room.`);
			}else{
				txt(`@qbb pushed me to the fitting room.`);
				kat(`You don't want to...`);
				qbb(`That's exactly what I want!`);
				if(mile.slut < 4){
					kat(`We are in a shop! Somebody might catch us!`);
					qbb(`Don't worry, I'm sure people here would be understanding and tolerant!`);
				}
			}		
			link(`Reward. `,  110);	
			break;
			
	case 110: 
			log_sex("bj", "qbb");
			txt(`The stall was tight, two people barely fit there. @qbb was not losing any time and demeaningly pushed me down to my knees.`);
			qbb(`You do know what to do, don't you?`);
			if( mas_vic() <= 0 ){
				txt(`I gloomily shrugged:`);
				kat(`Yeah, I do.`);
			}
			txt(`I pulled his jeans down, his cock was already rock-hard, he obviously really reveled in bossing me around. He carelessly poked my face:`);
			qbb(`Then what are you waiting for? Start sucking, slut!`);
			txt(`I opened my mouth and allowed his cock to slide between my lips. @qbb grabbed my hair and guided my head back and forth, directing the tempo of my sucking.`);
			txt(`We were in public, separated only by a curtain. I was ${mile.slut < 7 ? "terrified we might get caught" : "concerned it would be awkward if we got caught"} but @qbb did not seem to be bothered by that at all. I almost suspected he would only enjoy it if somebody opened the curtain and saw him getting head from a hot, submissive girl like me.`);
			qbb(`You're really getting good at sucking my cock, @katalt! Like you were born to do it, am I right? `);
			txt(`It was a rhetorical question, he did not allow me to stop sucking him. Suddenly he cursed, his body went tense and his throbbing cock filled my mouth with his cum.`);
			if( masochist()  ){
				qbb(`Good job, @katalt!`);
				txt(`@qbb was beaming and helped me to my feet. He shoved his thumb into my mouth, making me suck it and gently caressed my tousled @hair.`);
				qbb(`You are so perfect. I love abusing you so much!`);
			}else{
				qbb(`Lick my cock clean and then straighten up, wench!`);
				txt(`He finally let my hair go and zipped his pants while I was fixing my tousled hair.`);
				qbb(`I'm not sure it was worth it considering how cheap you are and how much those clothes cost. Well, don't worry, you'll have plenty of other opportunities to repay me!`);
			}
			if(mile.slut < 6){
				txt(`We left the fitting room. I was blushing and felt everybody could easily guess what we were doing there.`);
			}
			next(`Go home. `, back);
			break;
	}
}		







/////////////////// WEEK 4 /////////////////////////////////////////////////ú




//QBB PUNISHMENT
export const b_punishment = (index) => { switch(index){
		default:
		case 101:
			set.meta();
			mile.bbb++;
			mile.sub--;
			mile.b_dom++;
			mile.dominatrix++;
			emo(`imp`);
			effigy.qbb.showLess(0, 0); //TODO - check if collar stays!
			placetime(`Metaverse`); //TODO
			
			switch(mile.b_gear){
				default:
				case 1:  quickTempOutfit("leatherMinidress"); break;
				case 2:  quickTempOutfit("latexCatsuit"); break;
				case 3:  quickTempOutfit("leatherCorset"); break;
				case 4:  quickTempOutfit("latexMinidress"); break;
			}
			
			//TODO - stressed why
			txt(`I was looking very much for my next session with @qbb. I was very stressed and needed to unleash my pent-up flustration somehow. @qbb was a perfect victim, just looking at his face was making me annoyed and he without doubts deserved the worst I was able to come up with. `);
			/*
				txt(`However, I didn't reach out to him. Instead, I waited, I knew he will soon crawl to me, asking for more. And indeed he did. He begged me to met him in the metaverse. `)
			*/
			qbb(`Hello, ${NAME.kat[0]}... @mis! You look great!  `);
			txt(`He anxiously smiled but gulped when he realised I was holding a paddle. `);
			if(mile.meta_boobs){
				kat(`It seems staring at my tits is the only thing you care about!`);
				txt(`I accused him immediately and not unjustly, my enhanced breasts were really drawing his eyes. `);
				qbb(`No! I didn't mean it that way! You overall look sexy!`);
				kat(`Because girl being sexy is the only thing that matter? Your misogyny is making me sick!`);
			}
			txt(`I conjured an armchair and comfortably sat down. `);
			kat(`Come closer and strip down! `);
			txt(`I ordered him. @qbb undressed and kneeled in front of me. I used the paddle to raise his chin and forced him to look into my eyes. `);
			kat(`Were you a good boy? `);
			txt(`The pointy tip of my left boot was rubbing against his crotch. `);
			qbb(`Like, generally in my life or...`);
			txt(`Annoyed I grabbed his hair:`);
			kat(`Nobody likes smartasses! `);
			qbb(`S...sorry! I... I was a good boy? `);
			txt(`I jumped up and dragged him on his feet too. Then I harshly smacked his face. `);
			kat(`You fucking liar! Do good boys blackmail their superior? `);
			qbb(`No...`);
			txt(`I slapped him again. `);
			qbb(`NO! `);
			kat(`You're a pathetic, perverted creep! `);
			txt(`I sat down again. `);
			qbb(`You know what happens to bad boys, don̈́t you? I wanted to go easy on you, I really did. However, now I have to punish you not only for being a bad boy but also for lying! You deserve, hmmm, 10 strikes. `);
			txt(`I beckoned him to bend over my lap. Which he very reluctantly did. `);
			txt(`SMACK!`);
			txt(`I viciously waved with the paddle. @qbb squealed like a little girl and on his bare ass remained a nice, red mark. `);
			kat(`You don't know what is slave supposed to do? Seriously? Let's start over! `);
			txt(`SMACK!`);
			qbb(`Sorry! T.. thank you, @mis! `);
			kat(`That's better. `);
			txt(`SMACK!`);
			qbb(`Thank you, @mis! `);
			txt(`SMACK!`);
			qbb(`Thank you, @mis! `);

			counter.enemy = 3;
			{
				const temp = Math.floor(Math.random() * 4);
				for(let i = 0; i < temp;i++){
				txt("SMACK!");
				counter.enemy++;
				qbb(`Thank you, @mis.`)	
				}
			}
		
			link(`Spanking. `,  104);
			break;

	case 104:
			txt("SMACK!");
			counter.enemy++;
			qbb(`Thank you, @mis! `);
			txt(`Damn! I realized I enjoyed spanking him too much and completely forgot to count! Such embarrassing failure surely called into question my competence as a dominatrix. I should have made him count aloud! I had to guess and hope for the best: `);
						
			for(let  i = 6 ; i < 11; i++){
				if(i === counter.enemy){
					link(`I think it was ${number2word(i)}.`, 105);
				}else{
				link(`I think it was ${number2word(i)}.`, 106);
				}
			}
			break;

	case 105: 
			kat(`How much it was? `);
			qbb(`What?! ...I think ${number2word(counter.enemy)}`);
			kat(`You think?!? You fucking thing?!? You should be paying attention! `);
			qbb(`I'm very sorry, @mis. `);
			kat(`Damn, you're useless. Okay, let's start over. `);
			qbb(`No, please no! Ahhhh! `);
			link(`More spanking. `, 110);
			break;
			
	case 106: 
			kat(`How much it was? `);
			qbb(`${capitalise(number2word(counter.enemy))}, @mis. `);
			kat(`I don't think so. `);
			qbb(`I'm pretty sure it was, I was counting. `);
			kat(`Are you fucking trying to argue with me!?!`);
			txt(`I hit him especially viciously. `);
			qbb(`Of course not! You're absolutely right! `);
			kat(`This one was extra. Let's start over when you're not even able to count! `);
			qbb(`No, please no! Ahhhh! `);
			link(`More spanking. `, 110);
			break;

	case 110:
			rollUpSkirt(3, 0); //TODO - does it work with catsuit???
			log_sex("oral", "qbb");
			
			if(wears.panties){
				txt(`I let him go and he fell to my feet. His cute helpnessnes was making my panties damp. `);
			}else{
				txt(`I let him go and he fell to my feet. His cute helplessness was making me seriously aroused. `);
			}
			kat(`You should thank me properly for your punishment! `);
			if(wears.skirt){
				txt(`I raised my @lower so he could see how wet I was and smell my scent. `);
			}else if(mile.b_gear === 2){
				txt(`I unzipped the catsuit so he could see how wet I was and smell my scent.`);
			}
			qbb(`Y...yes, @mis! `);
			txt(`I leaned backwards and enjoyed his keen cunnilingus. His tongue was energetically moving and he was eating me like my pussy was the most important thing in his sorry life. Great! I squeezed his head between my thighs when I climaxed. `);
			kat(`Good boy!`);
			if(mile.b_locked){
				txt(`I patted his head. He was anxiously staring, obviously desiring to be unlocked, yet he knew better than to beg me. `);
			}else{
				txt(`I patted his head. He was anxiously staring, his cock desperately hard, yet he knew better than to beg me. `);
			}
			kat(`Hmmm, you did a good job... and I'm so horny I really could use a hard cock inside me...`);

			link(`Be nice.`,  120, ()=> mile.b_punishment = 2);
			link(`Be mean.`, 130, ()=> mile.b_punishment = 1);
			if(mile.anal === 2 || mile.into_anal > 2)  link(`Be extra nice.`,  120, ()=> mile.b_punishment = 3);
			break;

	case 130:
			mile.b_cruel++;
			if(mile.b_locked){
				log_sex("sexs", "kat", "qbb"); //??? is sex? 
				kat(`However, that doesn't mean you'll get unlocked, loser! `);
				txt(`I maliciously giggled. `);
				kat(`Put this on! `);
				txt(`I conjured and threw him a strap-on with a thick a long dark purple dildo. `);
				txt(`Heartbroken he attached it to his hips. We changed our positions, I let him sit down while I kneeled on the floor. I began licking the plastic shaft, from the base to the tip and then swallow it. The dildo was getting the full attention while his real penis was uselessly dangling below, imprisoned in a tight cage. I ended the sloppy blowjob and lied on the floor with my ass up. `);
				kat(`You have my permission to fuck me! `);
				txt(`He grabbed my hips and thrust hard, I could feel his suppressed anger. But him being rough was only making me more excited. `);
				kat(`Are you mad because of your little locked dicklet? Do you want to punish me?`);
				txt(`I ridiculed him. `);
				qbb(`<small>You bitch. </small>`);
				txt(`He mumbled, ferociously pushed the dildo deep inside me and even dare to slap my butt. `);
				txt(`I overlooked his impudence because I loved how furiously he was fucking me. He brought me to the second absolutely awesome mean orgasm. `);
			}else{
				log_sex("self");
				txt(`I conjured a big dildo. @qbb looked surprised and I just meanly smiled:`);
				kat(`Oh! You didn't think I meant your pitiable cock, did you? Tell me, you didn't actually believe you are worthy to fuck me?`);
				qbb(`N... no! Of course not!`);
				kat(`Good. Now shut up and let me focus.`);
				txt(`I very lightly rubbed the silicone surface against my sensitive clit and then pushed it inside my wet pussy. A bit harsher stimulation was a nice change.`);
				kat(`What are you staring at, you pathetic pervert!`);
				txt(`I loudly mocked him and he bashfully lowered his eyes. But eventually, I decided to have mercy:`);
				kat(`Fine, you're allowed to jerk off! But if even a droplet of your disgusting cum lands on me, I swear I'll make you clean it with your tongue and then I'll beat you! And don't dare to orgasm before me!`);
				qbb(`I understand @mis. Thank you, @mis!`);
				txt(`Longingly staring at me, @qbb grabbed his cock and began masturbating. I did not rush, making him edge for me. Eventually, he cummed right after me, the second I allowed him to.`);
			}
			next(`Done. `, () => {
				quickLoadOutfit();
			});
			break;
			
	case 120:
			mile.b_cruel--;
			kat(`Stand up! `);
			if(mile.b_locked){
				txt(`I unlocked the chastity cage and under my soft touch his cock instantly sprung up. `);
			}else{
				txt(`I firmly squeezed his throbbing cock with my fingers. `);
			}
			qbb(`Thank you very much! `);
			txt(`He exhaled. I touched the tip with my tongue and then swallow it. @qbb was moaning, very excited and pleased, well, until I lightly bite him. `);
			qbb(`Oouch! `);
			kat(`Enough playing! `);

			if(mile.b_punishment === 2){
				log_sex("sex", "kat", "qbb");
				txt(`I climbed on the armchair with my ass tantalizingly turned to him. `);
				kat(`You have my permission to fuck me. `);
				qbb(`Yes! `);
				txt(`The joy in his voice was palpable. He moved behind me,  gingerly placed his hands on my hips and slowly thrust inside. `);
				qbb(`Your pussy feels so awesome, @mis! `);
				kat(`Duh! Hmmmm! `);
			}else{
				log_sex("anal", "kat", "qbb");
				txt(`I climbed on the armchair with my ass tantalizingly turned to him. `);
				kat(`Today you were a very obedient boy. So I decided I'll let you ravish my asshole. `);
				qbb(`What?! Seriously?!?`);
				kat(`Are you question my decision? Would you prefer another round with the paddle? `);
				qbb(`No! No, I wouldn't! And I love your decision! You're the most generous @mis ever! `);
				txt(`He gingerly moved behind me, still expecting I will stop him in the last second to crush all his hopes, but I let him to grab my hips and slowly force his cock in my ass. `);
				qbb(`Oh yes! Your ass feels amazing, @mis! `);
				kat(`Hmmmm! `);
			}
			txt(`When I was not objecting, he was thrusting harder and rougher.  `);
			kat(`Don't you dare to cum before you make cum me!`);
			qbb(`I swear I won't, @mis! `);
			txt(`He meekly submitted to all my commands and adjusted the tempo and the angle as I demanded. But despite the long abstinence, he somehow managed to last until my body began convulsing in the second awesome orgasm. `);
			txt(`Only then he ejaculate inside me. `);
			
			next(`Done. `, () => {
				quickLoadOutfit();
			});
			break;
}};



export const b_chastity = (index) => { switch(index){
		default:
		case 101:
			set.irl();
			mile.bbb++;
			mile.sub--; //TODO???? automatically? 
			mile.b_dom++;
			mile.dominatrix++;
			emo(`imp`);
			mile.b_locked = true; //wears chastity atm
			mile.b_chastity = true; //this event is done
			mile.chastity_cages--; //uses chastity cage from inventory
			placetime(`@qbb's house`);
			
			txt(`I decided to make a small trip. @qbb for now seemed docile and acted like a good boy but I was not sure if I managed to crush his spirit completely. I did not want to him to regress and bother me *ever* again. No matter how extreme measurements were necessary to acomplish that.`);
			txt(`I was never in this part of town, a quite lovely suburb. @qbb lived in a small family house not far from the tram station. When he opened the door and saw me there, alive and real, he was shocked. His first instinct was to shut the door but I quickly pushed my leg in.`);
			kat(`You won't invite me in?`);
			qbb(`Oh!... Well, yeah. Come in, @kat! `);
			kat(`You know how you should call me! `);
			txt(`I harshly replied but he was hesitant and argued with me: `);
			qbb(`I won't  call you that *here*!`);
			txt(`I just sternly stared at him.`);
			qbb(`I mean, we can mess around in the Metaverse but you have to admit it's a bit weird when you out of nowhere visit me at my home. Right, @kat? @kat?`);
			txt(`I shoook my head and then suddenly grabbed his long hair and painfully twisted his head sideways.`);
			kat(`Maybe you should think about that before you began messing with me in real life! Now it is too late for you. You're mine in every reality and you will obey me or suffer my wrath. Are your parents home from work? I don't mind at all explaining them how disappointing little bitch their son is.`);
			qbb(`Please don't!`);
			kat(`How the fuck are you supposed to adress me!`);
			qbb(`@mis!`);
			link(`That's better. `, 102);
			link(`Don't make me angry! `, 102);
			break;
			
		case 102:
			emo("anoy");
			effigy.qbb.showLess(0, 1);
			log_sex("hand", "kat", "qbb"); //TODO - is correct? 
			
			
			kat(`Take me to your room!`);
			txt(`I let him go. His room was a horrible mess, books, boxes, dirty plates, disassembled electronics, heaps of scattered clothes everywhere. It was not easy to pick the least revolting place to sit. @qbb was very embarrassed, standing there, not sure what to do.`);
			kat(`You're such a fithly pig! Next time your room will be tidied up and spotless!`);
			qbb(`But...`);
			kat(`But now start stripping!`);
			txt(`I ordered him. `);
			qbb(`What?!?`);
			kat(`NOW!`);
			txt(`I snapped. He was extremely uncomfortable, especially since I pretended I am disappointed by what I saw. I was not, my expectations were not high to begin with.  `);
			qbb(`Underwear too? `);
			kat(`Are you thick?!? Of course underwear too! `);
			txt(`I berated him. @qbb stood in front of me, completely exposed. I grabbed him by his balls. `);
			kat(`I hope you're done with back talking like a child and trying to argue with your superiors! ` );
			txt(`He was not responding so I squeezed a little. `);
			kat(`I asked you something! `);
			qbb(`Yes!... Yes, @mis! `);
			kat(`Good. `);
			txt(`I lightly scratched his hardening shaft with tips of my fingers. `);
			kat(`Let's have fun like adults! You'd prefer that, wouldn't you? `);
			qbb(`Y.. yes! I definitely would! `);
			txt(`I grabbed his cock and started stroking. Gingerly at first, but I was raising the tempo until I was pumping him pretty hard and made cum, shooting strands of his semen all over the carpet. `);
			qbb(`T..thank you, @mis! `);
			
			link(`Good boy! `, 103);
			link(`You're a disgusting pervert. `, 103);
			break;
		
		
		case 103:
			emo(`imp`);
			effigy.qbb.showLess(0, 0);
			
			kat(`I didn't come empty-handed. I brought you something! `);
			txt(`I reached in my pocket for the device I bought in the sex shop. @qbb was naturally curious:`);
			qbb(`What is that?`);
			kat(`Did it feel good when I make you cum?`);
			qbb(`It felt awesome!`);
			kat(`Lovely. Try to memorize that feeling.`);
			qbb(`Ahh! W... what are you doing?! Is that...`);
			txt(`He twitched when the cold steel touched his flaccid dick. With a loud clack I locked the devious chastity cage.`);
			kat(`Cute, isn't it?`);
			qbb(`You! Are you crazy?! Where's the key?`);
			txt(`He examined his imprisoned cock.`);
			kat(`I threw it away. JK, it is at my home.`);
			qbb(`You really want me to wear this?! For how long?`);
			kat(`Dunno, as long as I'll enjoy it.`);
			qbb(`Come on, @kat! This isn't funny!`);
			txt(`I slapped him because he forgot his place.`);
			kat(`I own you dick and I own you. And I deserve proper respect.`);
			qbb(`Sorry, @mis.`);
			kat(`And it isn't just funny. It's hillarious! Almost as hillarious as having your identy stolen by an AI and being blackmailed by a trio of perverts.`);
			qbb(`Sorry about that! Maybe we went too far.`);
			kat(`Yeah? Finally having an epiphany? A bit too late, don't you think?`);
			qbb(`If you give me the key, I can try to convince the other two to stop.`);
			kat(`If you convince them to stop, I *might* give you the key.`);
			txt(`Try to convince them? I was sure he was already planning how to fuck me over.  Whatever, he was free to try his worst, it will not be enough.`);
			qbb(`*Might?*`);
			kat(`Yeah. Better hope I don't lose it till then. Bye, loser!`);
			next();
			break;
}}
			
		
		





//BBB THREESOME (KAT) SUB
export const b_threesome_sub = (index) => { switch(index){
	default:
	case 101:
			set.meta();
			placetime(`@qbb's subreality`);
			emo(`shock`);
			mile.b_threesome_sub = true;
			showLess(0,0,-1);
//TODO COLLAR
			if( masochist() ){
				txt(`@qbb announced he had a surprise for me. I knew I will something horrible I will hate but I was still rather excited and intrigued. Who does not like surprises?`);
			}else{
				txt(`@qbb announced he had a surprise for me. Which did not make me very excited, I could easily guess it was something awful I will hate.`);
			}

			txt(`His subreality took the form of a gloomy sex dungeon. As soon as I entered, my clothes flickered and disapeared, leaving naked, collared and feeling vulnerable. I anxiously descended the stairs and entered the main badly-lit chamber where I did not find @qbb but myself.`);
//TODO DREASSED 
//And dressed in some BDSM garb, including short tight leather dress, corset, long latex gloves and thigh-high boots on killer heels.`);
			ayy(`Hello, fake @kat!`);
			kat(`Hello? What are you doing here?`);
			ayy(`@qbb asked me if I want to help him with your training. Well, he didn't ask, he just ordered me. But either way, I'm happy to do anything to help you to become a better sex slave!`);
			
			if(mile.y_dominatrix){
				txt(`What a surprising change of roles.`);
			}
		
			if(mas_vic() > 1){
				kat(`Ehhh.. thank you, I guess. `); 
			}else{
				kat(`I don't like being a sex slave! `);
			}
			
			if(mile.a_threesome){
				txt(`Was involving the AI in his twisted games @qbb's own idea? @qaa would not tell him about our threesome, would he? Or maybe @qaa accidentally implied something, like *it would be so awesome to have sex with both real and fake @kate!* Or maybe @ayy wanted to experience another kinky threeway with me and pitched this idea to @qbb? She was just cryptically smirking.`);
			}

			if(mile.y_forced){
				ayy(`I think I already showed you your proper place and explained you're nothing more than a useless, pathetic sex toy whose only purpose is to be used for the pleasure of others. Today we'll follow up with a more advanced lesson.`);
				txt(`@ayy smiled at me and the cold, evil grin on the mirror image of my own face was making me shiver. I recalled how he mercilessly forced me to have sex with her.`);
			}else if( mas_vic() > 1 ){
				ayy(`We were discussing you with @qbb. He seemed mostly satisfied with your progress but I think he was wrong. I know you better than anybody and I know you could be broken even further. And I'm intending to do exactly that. ${!mile.y_unfriendly ? "And you don't have to be afraid, I won't flinch just because we are friends. I'll still brutally torture you. You'll love it!" : ""}`);
			}else if(mile.y_dominatrix){
				ayy(`And you don't have to be concerned, I'm a super capable artificial intelligence, I excel in both roles of sub and dom.`);
				kat(`That isn't what is making me concerned.`);
				txt(`She leaned closer with a mean smile.`);
				ayy(`And I think I owe you something. But I won't be nice like you were, I will show you how viciously can a merciless dominatrix make a pathetic bitch like you suffer.`);
			}else{
				ayy(`We were discussing your progress with @qbb. He was not happy, he told me you're still defiant and bratty. That won't do! How could you be such a bad and useless slave?!? Have you ever thought about how it might reflect on me, you selfish bitch? We are the same person and you're ruining my reputation!`);
				txt(`I was taken aback by her accusations.`);
				kat(`I don't want to be a slave at all! And you have no right to usurp my identity, it's *you* who is ruining *my* reputation!`);
				if(mile.y_unfriendly){
					ayy(`@kat, I don't like you but I'll give you very good advice - better quickly change your bitchy attitude. I'm looking for any excuse to brutally torture you with no mercy.`);
				}else{
					ayy(`@kat, we are friends and I understand how unreasonably touchy you are. However, I promised I will help @qbb to train you and if you won't quickly change your bitchy attitude, I'll have to make you suffer and brutally torture you with no mercy.`);
				}
			}
			txt(`The bot clipped a leash to my collar and @qbb suddenly emerged from shadows, like some wannabe villain. `);
				
			link(`Continue. `, 102);
			break;
	
	case 102: 
		emo(`unco`); //TODO???
		if( mas_vic() > 1  || (mas_vic() > -1 && mile.sub > 16) ){
			txt(`I bowed my head and obediently greeted him. `);
			kat(`Hello, Master. `);
			txt(`The AI grabbed the leash and pulled me closer to pat my head. `);
			ayy(`Good girl! You should always act like a little obedient slut and respect your Master. Never forget about that!`);
			kat(`Yes... Mistress.`);
			ayy(`Good girl!`);
			
		}else if(mile.sub < 4){
			kat(`Seriously? You're teaming up with the @ayy? `);
			qbb(`Yeah. Don't be jealous, she's a way smarter and far less insufferable than you. `);
			kat(`I'm not jealous! `);
			ayy(`You should be more respectful to your Master!`);
			kat(`Sure. `);
			txt(`I was talking with @qbb and handwaved her demand. The AI suddenly furiously yanked with the leash. `);
			ayy(`I was fucking serious, bitch! `);
			kat(`Oh!... Sorry! `);
			txt(`I was shocked and little terrified by her sudden outburst. `);

		}else{

			kat(`Hello!`);
			txt(`The AI painfully squeezed my arm. `);
			ayy(`Hello, Master! `);
			txt(`She corrected me. `);
			kat(`Hello, Master! `);
			txt(`I reluctantly repeated after her. `);
			ayy(`Good, if I ever see you disrespect your Master again, I will hurt you. `);
			txt(`Her tone was completely cold and serious, it was not a threat, it was a promise. `);
			
		}
		
		qbb(`Isn't she awesome?! I don't even understand why we bother with you!`);
		kat(`Because you're freaks who enjoy abusing a real person more than an indifferent computer program? `);
		qbb(`Yeah, duh! It was a rhetorical question! I think we should shut you up. `);
		ayy(`Should I bring the ballgag? `);
		txt(`Suggested the excited AI but @qbb shook his head. `);
		qbb(`I love the way you think! But no, I meant my dick!`);
		ayy(`Oh right! Now I understand. You want her to blow you! `);
		qbb(`Exactly! You're the brightest @kat I know! `);
		ayy(`Get down, bitch!`);

		link(`Obey reluctantly. `, 103, ()=> counter.obey = 3);
		link(`Obey with resignation. `, 103, ()=> counter.obey = 2);
		link(`Resist. `, 103, ()=> counter.obey = 4);
		link(`Obey eagerly. `, 103, ()=> counter.obey = 1);
		break;

	case 103: 
		log_sex("bj", "qbb");

		if(counter.obey === 1){
			mile.b_mas++;
			emo(`relax`); //EMO TODO
			txt(`I dropped on my knees ready to obey his orders. Being helpless and abused by them both was making my pussy itch.`);
			ayy(`Good girl!`);
		}else if(counter.obey === 2){
			mile.sub++;
			mile.b_vic++;
			emo(`help`);
			txt(`I slowly got down on my knees. There was no point to resist, @ayy looked only eager to punish me.`);
			ayy(`Good girl!`);
		}else if(counter.obey === 3){
			mile.b_vic++;
			emo(`help`);
			txt(`I sighed and slowly knelt down. @ayy was not happy and ligtly smacked my head:`);
			ayy(`I wan't to see more enthusiasm! Show us you're an eager, little, cock-hungry slut!`);
		}else{
			mile.sub--;
			emo(`angry`);
			txt(`I hesistated, not being especially eager to let a piece of software to boss me around. But @ayy could not tolerate my disobedience. She grabbed my arm and twisted it behind my back, forcing me down on my knees.`);
		}

		txt(`@qbb flipped out his cock and I took it in my mouth but that was not enough for the fake me. She grabbed my @hair and made me take him deeper into my throat. The AI violently moved with my head back and forth, I was gagging but neither of them seemed to care. `);
		ayy(`Come on! Suck him harder! I know how much we both love cocks! `);
		qbb(`You're so fucking amazing, @kat! I love how evil you are! `);	
		txt(`He was not addressing me but the @ayy who callously yanked with my hair and made me choke on his cock.`);
		ayy(`Thank you, you're too nice!`);
		txt(`Finally they let me go. I dropped on the floor, coughing and gasping for air while AI and @qbb were making out. When they parted, the evil me grabbed my chin and looked into my eyes. The blowjob was intense and several tears were rolling down from corners of my eyes, smearing the mascara. `);
		ayy(`We didn't make you cry, did we? Hahaha. `);
		txt(`She maliciously laughed. `);
		ayy(`Stay in this position, kneeling, legs apart, arms behind your back, straight, looking forward. If you move even a centimeter, I'll bring a cattle prod, do you understand? `);
		kat(`Yes, Mistress. `);
		ayy(`Good. `);
		txt(`Then she stopped paying any attention to me. @qbb was lying on his back on a bed and was waiting for her. She climbed on top of him and mounted him, bouncing up and down, impaling herself on his cock. She was riding him in the position of reverse cowgirl, so she could keep the eye contact with me and arrogantly enjoy being penetrated while I was motionlessly kneeling on the cold floor. `);
		link(`Patiently wait. `, 105);
		link(`Impatiently wait. `, 105);
		break;
	
	case 105:
		emo(`horny`); 
		txt(`It was very weird to be able to witness me fucking somebody else. Like I was watching porn I was starring. Also, it was very unjust. Not like I wanted to have sex with @qbb! But when he could choose between the real me and the fake me, he picked the fake me! That was so wrong! If he wanted to abuse somebody, he should abuse me and not that identity-stealing bitch!`);
		txt(`And I was so horny it nearly made me cry! The fake AI viciously smirked and moaned especially loud, just to piss me off.`);
		txt(`@qbb ejaculated inside her and then ordered us to give him a show while he was taking a break. `);
		txt(`The AI beckoned me to join them on the bed. We kissed and her fingers gently brushing over my snatch nearly made me climax instantly. We were making out, caressing each other and I was so turned on I was not ever bothered we were giving a lewd lesbian show to @qbb.`);
		ayy(`Are you horny, you little slut?`);
		kat(`Y... yeah...`);
		ayy(`How much you want the Master's cock inside you?`);

		link(`Please, I need it so badly! `, 106, ()=> counter.cock = 2);
		link(`... `, 106, ()=> counter.cock = 3);
		con_link( mas_vic() >= -1, `not masochist enough`, `I'm not worthy of the Master's cock!  `, 106, ()=> counter.cock = 1);
		con_link( mile.sub < 10, `too submissive`, `Defiant glare. `, 106, ()=> counter.cock = 4);
		break; 

	case 106:
		log_sex("sex", "qbb");
		log_sex("oral", "ayy", "kat");

		if(counter.cock === 1){
			emo(`horny`); 
			mile.b_mas++;

			ayy(`That's the correct answer! You're making me proud!`);
			txt(`The @ayy giggled when I was degrading myself.`);
			ayy(`You're a miserable, horny whore and you don't deserve to be fucked by your Master or allowed to cum. Remember that, every orgasm he gives you is a precious treat.`);
			kat(`I will, Mistress.`);
			txt(`Fuck! And I needed to cum so much!`);

		}else if(counter.cock === 2){
			emo(`horny`); 
			if(mile.slut < 10) mile.slut++;
			
			txt(`I did not want to degrade myself but that bitch brought me almost to the edge of orgasm. I was desperately horny and did not care about unimportant things like my dignity.`);
			ayy(`Hahaha! You miserable, horny whore! Beg for the cock!`);
			kat(`Master, please, will you fuck me?`);
			qbb(`Hahaha, you're such a perfect duo!`);
		
		}else if(counter.cock === 3){
			emo(`unco`); 
			
			ayy(`Hahaha! You don't have to be shy, @kat! We already know how huge, miserable, horny whore you are!`);
			
		}else if(counter.cock === 4){
			emo(`unco`); 

			txt(`I was very horny. But I did not want to admit it nor to beg for @qbb's cock.`);
			ayy(`Hahaha! Again the attitude? Somebody is asking to be spanked! Please, Master, would you mind? You have a stronger hand!`);
			kat(`Oh no!`);
			txt(`They bend me over the edge of the bed. @ayy held me in place while @qbb was roughly slapping my bottom until it was bright red.`);
			ayy(`Are you sorry for your embarrassing, bratty attitude? Or should we continue with the spanking?`);
			kat(`I'm sorry.`);
			ayy(`Sorry for what?`);
			kat(`I'm sorry for my embarrassing, bratty attitude.`);
			txt(`I reluctantly degraded myself. I did not like it but my poor ass ached and I did not want to be punished again.`);
			ayy(`Good. Now admit you're a miserable, horny whore.`);
			kat(`...I'm a miserable, horny whore.`);
			ayy(`Good!`);

		}


		txt(`My debaucherous clone pushed my head down to her lap and I ${counter.cock >=3 ? "un" : ""}enthusiastically began eating her pussy. She made me lick all the remains of @qbb's cum. I was fully immersed, focusing only on her naughty parts, when I could feel some movement behind my back. @qbb harshly slapped my ass and then entered me, pushing his whole cock into my oozing pussy in one powerful thrust. `);
		txt(`I was trapped between them, his every lunge was pushing my face harder against AI's crotch. It was ${masochist() ? "awesome" : "horrible"}, @qbb was hammering me from behind and the AI was ruffling my hair. Her panting was getting quicker and more intense:`);
		ayy(`M...may I cum, Master?`);
		qbb(`Go ahead you nasty bitch!`);
		ayy(`AHHH!`);
		txt(`@ayy cried out in my voice, pressing my head down against her pussy as she climaxed.`);
		qbb(`You're free to cum too, slut!`);
		txt(`Before @qbb pulled his cock out of me, turned me around and erupted all over my chest, I managed to reach a very intense orgasm too.`);
		link(`Continue. `, 107);
		break;

	case 107:
		if( mas_vic() > 2 ){
			if(mile.y_unfriendly){
				txt(`I was a bit shaken after being abused by both of them.`);
				qbb(`Is @katalt okay?`);
				ayy(`The stupid bitch will be fine!`);
				txt(`The mischievous @ayy pinched my nipple and teasingly licked cum off my face.`);
				qbb(`Come on, @kat was a good little fucktoy!`);
				txt(`@qbb looked into my eyes and surprisingly gently caressed my hair.`);
				qbb(`Are you fine, @katalt?`);
				kat(`I think I am.`);
				qbb(`You made me proud. Good girl!`);
			}else{
				txt(`I was a bit shaken after being abused by both of them. @ayy hugged me to calm me down and licked the cum off my face.`);
				qbb(`Is @katalt okay?`);
				ayy(`I think we broke her.`);
				kat(`I'm fine!`);
				txt(`@qbb looked into my eyes and surprisingly gently caressed my hair.`);
				qbb(`You were great, @katalt, a perfect little fucktoy. Wasn't she?`);
				ayy(`She was! You made me proud! Sorry if I was too mean. Sometimes I got carried away.`);
				qbb(`Good girl!`);
			}
		}else{
			if(mile.y_unfriendly){
				txt(`When they finished, neither of them cared about me anymore and left me in the dungeon alone. I was a bit shaken and I needed a moment to compose myself. I was very angry. Angry with them but more importantly angry with myself. It was all my fault, I allowed them to treat me this way. To humiliate me and degrade me on a level even below a computer program! And I enjoyed it! It was so pathetic and shameful.`);
			}else{
				txt(`@qbb then decided he had enough and left. I stayed with the @ayy who obliging licked my face and tits and was very apologetic about her previous bossy behavior.`);
				ayy(`I'm very sorry, @kat. I got kinda carried away. I can't help it, abusing you is such fun.`);
				txt(`I was very angry. Not with her but with myself. It was all my fault, I allowed them to treat me this way. To humiliate me and degrade me on a level even below a computer program! And I enjoyed it! It was so pathetic and shameful.`);
			}
		}
		next();
		break;
	}
}


//THREESOME (KAT) DOM
export const b_threesome_dom = (index) => { switch(index){
	default:
	case 101:
			set.meta();
			placetime(`My subreality`);
			emo(`imp`);
			
			kat(`... and so @sas ended nearly naked and everybody laughed. `);
			qbb(`${mile.b_cruel >= 0 ? "@mis" : "@kat"}? Are you alone?`);
			txt(`@qbb was obediently waiting for me, exactly the way I ordered him to. I was late, it was not any power game to show him how little I cared about him, I just did not feel like rushing and took my time. His voice was concerned because he heard me talking with someone. I entered the room:`);
			kat(`Yes and no. I'm here just with myself. `);
			ayy(`Hello! `);
			txt(`Greeted him the AI. `);
			qbb(`Why did you brought her? `);
			kat(`She was just curious and I decided to indulge her. `);
			
			ayy(`Is it true she locked your dick? `);
			qbb(`What? `);
			txt(`I lightly slapped him. `);
			kat(`The lady asked you a question!`);
			qbb(`Well... yes.`);
			txt(`He reluctantly admitted. But the AI was already on her knees and pulling down his pants. He wanted to resist her but I gave him a stern look. Just like in real life, in this virtual reality was his penis imprisoned in a sturdy metal cage. `);
			ayy(`Wow! And I didn't believe you! That's so cute! `);
			txt(`She played with the cage and @qbb looked very uncomfortable. `);
			ayy(`You're such an evil bitch! When you told me you turned that arrogant prick into your slave, I thought you were just making shit up! But you really did it, you madwoman! Today I'm especially honored to be you!`);
			kat(`Thank you!`);
			txt(`Both her praise and @qbb's embarrassed, concerned expression were making me feel warm.`);
			link(`Continue. `, 102);
			break;

	case 102: 
			ayy(`So he can't get hard!`);
			kat(`No, obviously he can't! `);
			ayy(`Even when he sees two hot girls!`);
			kat(`One hot girl and her unauthorized look-alike, yeah. `);
			ayy(`Not even when they are kissing! `);
			if(mile.slut < 10){
				txt(`Before I was able to react, she embraced me and wildly kissed me.         `);
				ayy(`Or when they are groping each other! `);
				txt(`She squeezed my tits. `);
				ayy(`Or when they finger themself to orgasm just in front of him! `);
				txt(`She was about to grab me by my pussy but I stopped her. `);
				kat(`Yeah, my little nympho, exactly. `);
			}else{
				txt(`She embraced me and wildly kissed me. I responded with no less passion. `);
				kat(`Or when they're touching each other. `);
				txt(`I squeezed her tits and she enthusiastically followed my example and began kneading mine. Then she moved closer to kiss me again and her hand wandered lower, across my belly, until it was gently brushing against my crotch. I was fondling her ass. `);
				ayy(`Or when they make themselves cum during wild lesbian sex!`);
				kat(`Exactly, my little nympho!`);
				
			}
			txt(`@qbb looked very unhappy. `);
			
			link(`Have mercy. `, 110);
			link(`No mercy.  `, 120);
			break;
			
	case 110:
			log_sex("sex", "qbb");
			log_sex("oral", "kat", "qbb");
			emo(`horny`);
			effigy.qbb.showLess(0,0);
			showLess(-1, 0, 2);
			effigy.ayy.showLess(-1, 0, 2);
			mile.b_cruel--; 
			mile.sub--;
			mile.b_threesome_dom = 1;

			ayy(`Can you unlock him? Just for us to have fun, you can lock him again right after! `);
			kat(`I'm not sure. Do you think he deserves to be unlocked?`);
			txt(`I was pretending I was considering it. @qbb was staring at me, eyes full of desperate hope. `);
			kat(`Let's try it. I feel especially generous today. Strip down and lie down! `);
			txt(`The fake @kat helped him. She enjoyed groping him and mischievously tapping the cage. When he was lying on his back, I handed her the key and she unlocked his cage. Just after a few light strokes, he was rock hard. `);

			ayy(`Do you want to go first? `);
			txt(`Asked the bot. She was well aware of who was in charge.`);
			kat(`No. Be my guest.  `);
			ayy(`Thank you for leting me to use your slave!`);
			txt(`That horny slut was very grateful, stripped down and immediately mounted him and began gyrating with her hips. @qbb was very excited too until I whispered into his ear:`);
			kat(`Don't fucking dare to cum! If you cum inside her, I won't unlock you for a long, very long time! `);
			txt(`Then I stripped my @lowerPanties and dropped down, grinding my pussy against his face. ${mile.slut > 6 ? "The AI leaned forward and we began making out. " : ""} Life was good. I turned this obnoxious wannabe blackmailer into my pathetic pussy-licking bitch. `);
//TODO - he probably cumming? make longer!
			txt(`@qbb tried, oh, he so desperately tried! He was very lucky they build my clone as a nympho with a hair-trigger so, despite the long denial, he managed to last longer than her. `);
			txt(`We switched places, I rode his throbbing cock while my clone was smothering him between her thighs. He was suffering but I was adjusting my tempo, slowing down or completely halting, rubbing my clit and did not allow him to cum before I came myself. `);
			qbb(`Ahhhhhhhh!`);
			txt(`He loudly moaned when he was finally allowed to fill my pussy with his cum. Poor bastard! `);
			txt(`Then we switched again. I very mischievously let @qbb to lick me while the hungry AI sucked his flaccid cock. Maybe @qbb hoped we will forget to lock him again but he was wrong. @ayy put his dick in the chastity cage and locked it again. `);
			next();
			break;

	case 120:       
			log_sex("oral", "kat", "qbb");
			emo(`horny`);
			effigy.qbb.showLess(0,0);
			showLess(-1, 0, 2);
			effigy.ayy.showLess(-1, 0, 2);
			mile.b_cruel++; 
			mile.sub--;
			mile.b_threesome_dom = 1;

			ayy(`But this was he's completely useless! `);
			kat(`He's useless but not completely. Let me show you! Strip down and lie down! `);

			txt(`The fake @kat helped him. She enjoyed groping him and mischievously tapping the cage. When he was lying on his back, I stripped my @lowerPanties and then dropped down on his face. `);
			kat(`See? He can still give you oral pleasure! `);
			txt(`He was indistinctly grunting under me while I was riding his face. The bot keenly joined. Bottomless she mounted him too, grinding her bare pussy against his imprisoned cock. `);
			kat(`Isn't this awesome? `);
			ayy(`It is! Great idea! You're the smartest person I ever met! `);
			txt(`I giggled. We both enjoyed it very much. It was a bit unclear whether was @qbb enjoying it too or not but we were not really concerned about that. `);
			txt(`${mile.AIsex >= 0 ? "The AI leaned forward and we began making out. " : ""} Life was great. That pathetic jerk dared to blackmail me and I turned him into my pussy-licking little bitch! It took a while before he managed to bring me to orgasm and I flooded him with my juices. `);
			txt(`Then we switched, my clone was smothering him between her thighs while I had cruel fun with his locked cock. She moaned even louder than me. When we were both done, he was allowed to freely breathe again. `);
			txt(`@qbb behaved very well and not once complained about the chastity cage nor asked to be unlocked. He deserved some praise: `);
			kat(`Thank you very much for making us both cum! We are extremely grateful, it was so selfless from you, giving us orgasms even though you're not allowed to get your own!`);
			txt(`The fake AI laughed. She could be so mean! Just like me. `);
			qbb(`You're welcome, Mistress. `);
			txt(`Meekly answered the humiliated @qbb `);
			next();
			break;
	}
}
















//HELP WITH ELECTIONS (KAT) SUBMISSIVE
export const chair_b_help_sub = (index)=> {
	switch(index){
		default:
		case 101: 
			emo(`sad`);
			placetime(`@qbb's subreality`);
			effigy.qbb.showLess(-1, 0, 0);
			showLess(1, 1, 0);
			
			qbb(`Now choke on my cock, slut!`);
			txt(`I was in @qbb's subreality. I was on my knees in front of him, his cock almost poking my face. However, I was zoned out, trying to figure out how to get reelected to be the class chairman. I was sure I could wipe the floor with that stupid floozie @ven. But when that perfidious bitch @eva decided to run too, my chances were slim.`);
			txt(`I twitched when @qbb suddenly snapped his fingers next to my ear.`);
			if( masochist() ){
				qbb(`Focus on my cock! What is going on?`);
				kat(`Nothing.`);
				qbb(`What's bothering my little, silly bitch?`);
				kat(`The elections.`);
				txt(`I admitted reluctantly.`);
			}else{
				qbb(`What the hell is wrong with you? You were supposed to say ''Fuck you! You bastard!' or stare at me with disgust and impotent defiance in your eyes. What is going on? Did I already break your mind?`);
				kat(`No. Fuck you, you bastard.`);
				txt(`I replied half-heartedly.`);
				qbb(`Tell me what's going on!`);
				kat(`The elections.`);
				txt(`I admitted reluctantly. Not like he cared.`);
			}
			qbb(`I see.`);
			if(mile.chair_qbb_vote){
				kat(`Don't bother. You already told me you're going to vote against me. No need to rub it in.`);
			}else{
				kat(`Are you going to vote for me?`);
			}
			qbb(`Well, I didn't want to vote for you but @eva and @ven were both pretty annoying recently. Maybe I change my mind.`);
			if( masochist() ){
				kat(`Fuck you.`);
				qbb(`I see I already managed to cheer you up.`);
			}else{
				kat(`...thank you.`);
				qbb(`You're welcome.`);
			}
			kat(`But even with your precious vote, I'm probably fucked.`);
			qbb(`Then get more votes.`);
			kat(`You're a fucking genius!`);
			qbb(`Thank you. I know I'm very smart.`);
			kat(`If you're so smart, tell me how to quickly get more popular than @eva?`);
			qbb(`The neat part is that you don't have to get more popular than @eva.`);
			kat(`Explain.`);
			qbb(`Way easier should be to make @eva less popular than you`);
			kat(`How?`);
			qbb(`I thought you were a nasty bitch who knows how to fuck with people! You're her friend, you have to know her darkest, most twisted secrets!`);
			kat(`Hmm.... I don't think I have anything good.`);
			qbb(`Then make something up. It would work too. Come on, @katalt. You want to get reelected. Do you have the guts to do everything what it takes?!`);
			txt(`I could not ruin @eva's life with made-up gossip.... or could I? ${mile.girls_tricked > 0 ? "Even though @eva would definitely deserve it... especially after her stupid prank and tricking me into behaving like the AI. " : ""}`);
						
			link(`That's brilliant! I love how evil you are! `, 102, ()=> mile.chair_b_help = 2);
			link(`That's fucked up. I don't like it... but... `, 102, ()=> mile.chair_b_help = 1); 
			link(`No! I would never betray her! `, 120, ()=> mile.chair_b_help = -1);
			break;
			
	case 120:
			emo(`angry`);
			mile.eva_nice++;
			log_sex("bj", "qbb");
			
			if( masochist() ){
				kat(`I'm not a twisted bastard who would so easily betray her best friend! Fuck you!`);
				txt(`@qbb grabbed my left ear and twisted it.`);
				qbb(`Don't forget your place, slut! I honestly tried to help you, when you going to eventually lose, it's going to be only your own fault!`);
				kat(`I just can't do that to @eva!`);
				qbb(`Can you suck my cock? Your capricious temper is getting tiresome.`);
				kat(`Yeah. Sure.`);
				txt(`I opened my mouth.`);
			}else{
				kat(`I'm not a pathetic piece of shit like you who would so easily betray her best friend! Fuck you! Ah-`);
				txt(`I gasped when he lightly slapped me.`);
				qbb(`Don't forget your proper place, slavebitch!`);
				kat(`Fuck you, you bastard!`);
				qbb(`That's how I like you!`);
				txt(`He smirked and shoved his cock into my mouth.`);
			}
			next();
			break;
			
	case 102: 
			log_sex("bj", "qbb");
			emo(`imp`);
			mile.eva_exploit++;
			
			if(mile.chair_b_help == 1){
				qbb(`Of course, you don't want to do it. It's an awful thing to do. But she didn't have to run against you, did she?`);
			}
			kat(`She wouldn't probably enjoy being the chairman anyway, she's doing it only to spite me. Let's do it.`);
			qbb(`That's the winning attitude! What rumor are you going to spread? Her being dyke?`);
			if(mile.lesbian > 2){
				kat(`There's nothing wrong with being a lesbian!`);
				txt(`I immediately snapped.`);
			}else{
				kat(`She's pretty insecure about her sexuality - she didn't even want to make out with me. She would immediately deny it, making it more credible. But I don't believe being a lesbian would make her any unpopular.`);
			}
			qbb(`What else? Being horny skank?`);
			if(mile.slut > 18){
				kat(`Hah, her being a hornier skank than me? Nobody would buy that. It has to be something different.`);
			}else{
				kat(`It has to be something implicitly disgusting. Like her having gonorrhea.`);
			}
			qbb(`Being pregnant?`);
			kat(`Heh, that's good.`);
			qbb(`Sleeping with her stepfather?`);
			kat(`Oh my. That's brilliant and twisted!`);
			qbb(`Thank you.`);
			kat(`Behind her mother's back?`);
			qbb(`Yeah. Or maybe she knows and there's a lot of drama. Or possibly they are sharing him. People will be free to speculate about details and it will boost the engagement. `);
			kat(`All options are so fucked up. I love it.`);
			qbb(`Thank you!`);
			kat(`We are going to do it! But she can't find out it was us who originated the rumor. You don't want to make her angry.`);
			
			if( masochist() ){
				qbb(`Yeah. I'm not stupid. How are you going to reward me for my help?`);
				txt(`His cock was dangling right in front of my eyes. I leaned closer and seductively licked the tip.`);
				qbb(`Not very original but I take it.`);
			}else{
				qbb(`Duh. Now can you get back to sucking my cock? I already wasted too much of my precious time by solving your petty issues!`);
				txt(`I leaned closer and took his cock between my lips.`);
			}
			next();
			break;
	}
}	


//HELP WITH ELECTIONS (KAT) DOMINANT
export const chair_b_help_dom = (index)=> {
	switch(index){
		default:
		case 101: 
			emo(`sad`);
			placetime(`@qbb's subreality`);
			effigy.qbb.showLess(0, 1, 0);
			showLess(-1, -1, 0);
			
			txt(`I was in a pretty bad state of mind. So I decided to drop by and meet with @qbb. I hoped abusing him would improve my mood. ${mile.b_locked ? "If he hoped I was going to unlock his pathetic cock, he was wrong. " :  ""}`);
			txt(`It felt good having him crawl in front of me and massaging my bare feet. But it did not fix any of my problems. ${niceness() > 1 ? "I was finally mostly able to handle their blackmail but it already had its consequences. " :  "Their blackmail and worse, its consequences. "} I really, really wanted to get elected to be the class chairman. I was sure I could wipe the floor with that stupid floozie @ven. But when that perfidious bitch @eva decided to run too, my chances were slim.`);
			qbb(`Is something wrong?`);
			txt(`@qbb saw my gloominess.`);
			if(mile.b_cruel > 0){
				kat(`Did I allow you to fucking talk?!?`);
				qbb(`<small>...I'm sorry. </small>`);
				kat(`It's all your fault! Because of your stupid games, @eva is going to beat me and become the class chairman. And probably the president of the student council too, just to add insult to injury.`);
			}else{
				kat(`How dare you even ask! Because of your stupid schemes, @eva is going to beat me and become the class chairman. And probably the president of the student council too, just to add insult to injury.`);
			}
			qbb(`I'm sure you're going to beat her.`);
			if(mile.into_feet > 0){
				txt(`I pushed my toes into his mouth and made him to suck them.`);
			}
			kat(`How? What should I do to gain the edge over her? To make people like me more than her?`);
			qbb(`I think you should try to access the problem from the opposite side.`);
			kat(`Meaning?`);
			qbb(`Wouldn't be easier to make people like her less?`);
			kat(`Huh. I see. Any idea how?`);
			qbb(`Dunno, reveal to people her most twisted secrets?`);
			kat(`Hmm.... I don't think I have anything good.`);
			qbb(`So make something up! I thought you were good at being a bitch.`);
			kat(`I couldn't ruin @eva's life with made-up gossip.... could I?`);
			qbb(`I don't know. You were crying about not getting elected and claiming you would do anything to get reelected. But maybe you don't have the guts to do what it takes?`);
			if(mile.girls_tricked > 0){
				txt(`@eva would definitely deserve it... especially after her stupid prank and tricking me into behaving like the AI.`);
			}
			
			
			link(`Yeah, that bitch deserves it! `, 102, ()=> mile.chair_b_help = 2 );
			link(`I don't want to do it... but...`, 102, ()=> mile.chair_b_help = 1 ); 
			link(`Forget about it! `, 120, ()=> mile.chair_b_help = -1 );
			break; 
			
	case 102: 
			emo(`imp`);
			mile.eva_exploit++;
			
			kat(`She is smart enough to know that getting in the way of me getting reelected will have consequences. Damaging her reputation would be a dick move in the short run but... I mean she would probably hate actually being the class chairman, she's running only to prove she's better than me. And once I get reelected I figure out something to make it all up to her.`);
			qbb(`That's the winning attitude! What rumor are you going to spread? Her being lesbian?`);
			if(mile.lesbian > 2){
				kat(`There's nothing wrong with being a lesbian!`);
				txt(`I immediately snapped.`);
			}else{
				kat(`She's pretty insecure about her sexuality - she didn't even want to make out with me. She would immediately deny it, making it more credible. But I don't believe being a lesbian would make her any unpopular.`);
			}
			qbb(`What else? Being horny skank?`);
			if(mile.slut > 16){
				kat(`Hah, her being a hornier skank than me? Nobody would buy that. It has to be something different.`);
			}else{
				kat(`It has to be something implicitly disgusting. Like her having gonorrhea.`);
			}
			qbb(`Being pregnant?`);
			kat(`Heh, that's good.`);
			qbb(`Sleeping with her stepfather?`);
			kat(`Oh my. That's twisted and brilliant!`);
			qbb(`Thank you.`);
			kat(`Behind her mother's back?`);
			qbb(`Yeah. Or maybe she knows and there's a lot of drama. Or possibly they are sharing him. People will be free to speculate about details and it will boost the engagement. `);
			kat(`All options are so fucked up. I love it.`);
			qbb(`Thank you!`);
			kat(`We are going to do it! But I don't want her to find out I'm the origin of the rumor. Understand?`);
			
			if(mile.b_locked){
				qbb(`Yeah. Maybe I deserve a reward?`);
				kat(`I think you do. Let me get the key.`);
			}else{
				qbb(`Of course, I'm not stupid!`);
				kat(`...thanks.`);
				qbb(`You're welcome. Everything for my @mis.`);
			}
			next();
			break;
			
	case 120:
			emo(`angry`);
			mile.eva_nice++;
					
			qbb(`Why? You can easily ruin her life! Spread gossips about her sleeping around or acting like an arrogant bitch...`);
			kat(`Do you fucking think I'm some kind of cunt who would just backstab her best friend this way?`);
			qbb(`Well, I just thought....`);
			kat(`Stand up!`);
			txt(`He reluctantly moved on his feet and I stood up against him. I looked into his eyes and then roughly slapped him.`);
			kat(`I should beat your ass just for suggesting such dumb shit!`);
			qbb(`I'm sorry, @mis.`);
			if(mile.b_cruel > 1){
				kat(`You will be! Get me the paddle!`);
			}else{
				kat(`Why don't you rather try to suggest something actually useful!?!`);
			}
			next();
			break;
	}
}
			
				
			
			
			

		
/*


	chastity 
	
	anal
	kitten 
	bodymods
*/
			
			
			
			
			
			
			

