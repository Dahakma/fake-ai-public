import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";
import {decapitalise} from "Libraries/dh"; //TODO!!!!
import {placetime} from "Loop/text_extra";
import {add_before_workday, next_event, add_tomorrow, add_workday, add_today, get_schedule} from "Taskmaster/scheduler";
import {emo, INVENTORY, createWear, recreate, wdesc, effigy, wearsPink, sluttiness, makeup, wearRandomClothes, remove, removeEverything, wear, create, wears, showLess, showAll, quickSaveOutfit, quickLoadOutfit, updateDraw} from "Avatar/index";
import {masochist, spoiler, dildo, log_sex} from "Virtual/text";
import {groups} from "Data/items/prices";
import {payment} from "System/bank_account.js";
import {NAME} from "System/variables";

///////////////////////  WEEK 1  ////////////////////////////////////////////////////////////////////

//ADVICE 1 - pink clothes
export const c_advice_1 = (index) => {
	switch(index){
		default:
		case 101:
			set.meta();
			placetime("@qcc's subreality");
			mile.advice_1 = 3;
			mile.ccc++;
			
			txt(`I assumed @qcc might be easy to convice. I decided to meet him in the Metaverse. He seemed a bit nervous to face me alone. Good. `);
			qcc(`H-Hi, @katalt. I'm sorry about those things, I had no idea it was really you and not the AI. `);
			kat(`Don't worry about that. I mean, I hate being blackmailed and stuff, but I'm really glad you made me play <i>${mile.immersion>0 ? "Neverion II: The Rise of the Chaos Wizard" : "that game"}</i>. It is really awesome!`);
			txt(`His eyes brighten up.`);
			qcc(`Really!? You seriously enjoy the game? `);
			kat(`Of course, I would never lie to you! `);
			qcc(`Oh... right... `);
			kat(`Come on! But I do kinda enjoy it. Apart for some minor issues. `);
			if(mile.feyd_keryk_attempts <= 0){
				qcc(`You had troubles to find your contact? `);
				kat(`What? No, I found ${mile.feyd_keryk_name} on my first try. That was easy. `);
				qcc(`So what is the problem? `);
			}else if(mile.feyd_keryk_attempts > 5){
				qcc(`What issues? `);
				kat(`Like finding that idiot! How are you supposed to remember such a stupid name? What a pointless waste of time! `);
				qcc(`The devs thought it was stupid but somebody on the forum liked it so they decided to keep it in. Anything else? `);
			}else{
				qcc(`What issues? `);
			}
			kat(`It's very hard for a low-level character to get enough money and not get instanty killed.`);
			//Well, it is a bit unbalanced. Some numbers are pretty arbitrary and it almost looks like somebody just added random values without having any idea what is she doing. It is really hard for a low-level character to get enough money...`);
			qcc(`What?! It's pretty simple... wait, are you trying to trick me?`);

			link(`Yes.`,102);		
			link(`No.`,103);
			break;
			
		case 102:
			emo("smug");
			kat(`Yes. You explained to me that I'm an evil manipulative bitch so I'm just trying to live up to your expectation. `);
			qcc(`I can't help you! `);
			kat(`You can and you will. And I can make it worth it for you. `);
			txt(`For a moment he struggled - he really did not want to betray his friends - but I knew he will break.`);
			qcc(`O... okay. `);
			link(`So what should I do? `, 104);
			break;
			
		case 103:
			emo("fake");
			kat(`No. I'm just really desperate! The Game is so hard and I.. I'm so awful.. and... and...`);
			txt(`I can be a quite good actress. It seemed I was about to burst into tears which clearly made him very uncomfortable.`);
			qcc(`It really isn't so hard... but I can't help you...`);
			kat(`Please! I don't know what to do! You're my only hope! `);
			txt(`Watching his inner struggle was almost entertaining. `);
			qcc(`O... okay. `);
			link(`So what should I do? `, 104);
			break;	
			
		case 104:
			emo("relax");
			qcc(`To be able to fight monsters, you need a better weapon. The knife is useless. `);
			kat(`I know, I'm not <i>that</i> stupid. But I don't have money and can't find any better weapon anywhere!`);
			qcc(`You can ask around for quests. In the worst case, you can sell your clothes. `);
			kat(`What?!? `);
			qcc(`They are useless, they don't provide any extra defense and in the Neverion cold environment don't cause you any damage. `);
			kat(`But without my clothes, I would be naked!`);
			qcc(`So what? `);
			kat(`So what?!?`);
			qcc(`You're hot, you don't have to be ashamed of your body. And everybody around you is just NPC anyway unless you play in cooperative mode. You don't have to be shy, they are just programs. `);
			
			link(`Right! Good idea, I'll do it.`, 105, ()=> counter.feyd_nude = 1);
			link(`Thanks, I guess. `, 105, ()=> counter.feyd_nude = 0);
			link(`That's the dumbest advice ever! `, 105, ()=> counter.feyd_nude = -1);
			break;
			
		case 105:
			emo("shy");//emo??
			txt(`He might be right but I still didn't like the idea. `);
			qcc(`Wait, could you do something for me? `);
			kat(`Ahg, fine, what do you want? `);
			qcc(`Could you wear something pink to school? `);
			kat(`Pink clothes? `);
			qcc(`Yeah! I think you look cute and very pretty in pink. `);
			txt(`I considered it. Since there was a chance I might need another piece of advice, I decided to agree. `);
			//kat(`Sure! I'd love to! Anything for you, you helped me a lot! `);
			//add_before_workday("afternoon2", "c_advice_1_shop");
//TODO - SHOULD BE BEFORE WORKDAY
			add_today("afternoon2", "c_advice_1_shop"); //TODO UNTESTED BUT SHOUDL WORK
			add_workday("classes", "c_advice_1_reaction");
			chapter(`Sure! `,`advice_1`, 200);
			//TODO ATTACH TASK 
	}	
};			
			
export const c_advice_1_shop = (index) => {
	switch(index){
		default:
		case 101:
				set.meta();
				link(`Shop for pink clothes. `, 102);
				next();
			break;
		case 102: 
			INVENTORY.shop("bimbo", next_event); //TODO - SHOULD WORK BUT TEST ASAP
		//inventoryShop("bimbo");
			
			break;
	}	
};

export const c_advice_1_reaction = (index) => {
	const pink = wearsPink(); //not efficient but whatever
	const outer = pink.filter( a => a !== "bra" && a !== "panties" );
	const inner = pink.filter( a => a === "bra" || a === "panties" );


 
	//and(inner.map( a => PC[a])))
	/*
	console.log(pink)
	console.log(pink.map( a => PC[a]))
	console.log(outer)
	console.warn(inner)
	console.log(inner.map( a => PC[a]))
	*/
	
	switch(index){
		default:
		case 101:
			set.irl();
			placetime("hallway");
			mile.c_advice_1 = outer[0];
			mile.c_advice_1_under = !!inner.length;
			emo("imp");
			
			if(pink.length < 1){
				mile.ccc--;
				txt(`Damn, @qcc asked me to wear something pink and I completely forgot! Well, too bad.  `);
				next();
			}else{
				txt(`I seeked @qcc during a break. `);
				qcc(`Oh, @kat!`);
				if(outer.length){
					kat(`Do I look cute and pretty? Wearing ${and(pink.map( a => PC[a].name))}?`);
					if(inner.length){
						qcc(`Y... Yeah, you look awesome! W... wait, did you say ${and(inner.map( a => PC[a].name))}?`);
						kat(`Yeah? `);
						qcc(`Oh! Cool! `);
						kat(`Do you want to see?`);
						qcc(`W... what? O.. of course! `);
						link(`Hah, too bad! `,102);
						con_link(mile.slut > 3, `low slut`, `Flash your underwear. `,103);
					}else{
						qcc(`Y... Yeah, you look awesome! `);
						kat(`So you'll help me with the game as you promised!`);
						qcc(`W... what!? I don't remember promising that! `);
						kat(`I do! Are you calling me a liar? `);
						qcc(`N.. no!`);
						next(`I thought so! `);
					}
				}else{
					kat(`So? `);
					qcc(`What? `);
					kat(`I'm wearing the pink clothes as you demanded? `);
					qcc(`What pink clothes? `);
					kat(`${capitalise(and(inner.map( a => PC[a].name)))}`);
					qcc(`Oh! Cool! `);
					kat(`Do you want to see?`);
					qcc(`W... what? O.. of course! `);
					link(`Haha, too bad! `,102);
					con_link(mile.slut > 3, `low slut`, `Flash your underwear. `,103);
				}
			}
			break;
			
		case 102:
				mile.sub--;
				emo("smug");
				qcc(`Why are you teasing me like this!?!`);
				kat(`Just to see the disappointment in your face! `);
				qcc(`You're so mean! `);
				kat(`I'm extremely nice, I did exactly what you wanted me to do! And you'll help me with the game, won't you?`);
				qcc(`W... what!? I don't remember promising that! `);
				kat(`I do! Are you calling me a liar? `);
				qcc(`N.. no!`);
				next(`I thought so! `);
				break;
				
		case 103: 
				mile.slut++;
				emo("smug");
				showLess(inner.includes(`bra`) ? 1 : -1, inner.includes(`panties`) ? 1 : -1, 3);
				kat(`I looked around, it seemed we were alone. I readjusted my clothes, giving him a quick glance at my ${and(inner.map( a => PC[a].slot))}.`);
				qcc(`W..wow! `);
				txt(`He blushed. `);
				kat(`So now you'll help me with the game as you promised!`);
				qcc(`W... what!? I don't remember promising that! `);
				kat(`I do! Are you calling me a liar? `);
				qcc(`N.. no!`);
				next(`I thought so! `, showAll);
				break;
			
	}	
};			
			
		

//WEEKEND 1 
			
//MEETING 1 - pretend interest X grope boots
export const c_meeting_1 = (index) => {
	let job = "";
	if(SETTING.local === "czech") job += "or the Castle guards ";
	if(SETTING.local !== "english") job += "or cosmonauts ";
			
	switch(index){
		default:
		case 101:
			set.meta();
			placetime("@qcc's subreality");
			mile.ccc++;
			
			txt(`I decided to visit @qcc. He seemed like the weak link and I hoped I would be able to convince him to abandon their awful vile scheme or at least persuade him to give me less ridiculous tasks. `);
			kat(`Hi! `);
			if(mile.advice_1 === 3){
				qcc(`Hello? Are you here to ask more about the Game? Because I can't tell you anything. `);
				kat(`Don't worry, I'm doing great. `);
			}else{
				qcc(`Hello? Why are you here? Are you here to mock me? `);
				kat(`Of course not!`);
			}
			
			qcc(`So why are you here?`);
			kat(`Can't I just drop by for a friendly visit? `);
			qcc(`I don't think we are friends, we have never been very close... are you here to gather intel for your evil plan for your horrible revenge against us? `);
			kat(`Don't be ridiculous. Besides, I'm not really in a vengeful mood. `);
			qcc(`That's exactly what would somebody in vengeful mood say. `);
			kat(`Okay, I'll be totally honest with you. My visit has only one single intention - socialize with you and convince you I'm not so bad because the way I'm totally helpless against your stupid tasks which makes me pretty scared. I picked you because you seem like the nicest one. @qbb is psycho who is getting hard from humiliating me and commanding me around and @qaa is obsessed with me, he would lock me in his cellar if he could get away with it. You seem like a normal, nice person. It is not your fault they dragged you into this. `);
			qcc(`Well... thanks... `);
			kat(`And I feel there is an unsolved animosity between us... `);
			qcc(`Because I told you that! `);
			kat(`... and I think we should solve it so we could move on. `);
			qcc(`How do you want to simply solve all those years of bullying? `);
			kat(`I never bullied you! `);
			qcc(`You did!`);
			kat(`Name one example!`);
			qcc(`I... well... you laughed when I ripped my pants. `);
			kat(`First, that was the fifth grade. Second, everybody laughed. `);
			qcc(`But you started it!`);
			kat(`Third, you have to admit it was objectively a pretty hilarious moment. `);
			qcc(`You are really not helping your case. `);
			
			link(`Pretend interest. ${spoiler("friend/feminization")} `, 102);
			con_link(mile.slut > 1, `low slut`, `Use boobs. ${spoiler("bimbo")} `, 103);
			break;
	
		case 102:
			mile.c_friend++;
			mile.c_interest = true;
			
			
			
			kat(`So... you like playing video games? `);
			qcc(`Yeah?`);
			if(mile.immersion > 0){
				kat(`So... is <i>Neverion II: The Rise of Chaos Wizard</i> your favorite game? `);
			}else{
				kat(`So... is <i>Neverion: The Rise of... </i> some warlock	 your favorite game? `);
			}
			qcc(`Well, not really. Some people might disagree but I personally prefer the original <i>Neverion</i>. It builds great perfect lore with tons of little interesting things and the sequel only leeched on it without much expanding it further. `);
			kat(`What little things? `);
			qcc(`Like that female characters can't be orcs, only half-orcs because orcs are male-only species which breeds with other races. `);
			kat(`Ew, I've seen orcs and it must be hard for them to find an interested woman to date. `);
			qcc(`Ehh.. yes. Also, the sequel is a bit dumbed down for the more casual audience. `);
			kat(`Like me? `);
			qcc(`Yeah! ...Sorry! I really didn't mean to be rude and offend you! `);
			kat(`Why should I be offended by that? `);
			qcc(`I'm sure you'll be great at the game once you'll get over the learning cliff. `);
			kat(`I think the game is pretty hard! And you have no idea how misogynous the game is! `);
			qcc(`Excuse me, I have a pretty good idea, I played the game with all characters. `);
			txt(`The idea that somebody might replay the game not only once but multiple times was horrifying. `);
			kat(`With all characters!?!!`);
			qcc(`I didn't finish with every character. But I wanted to experience different gameplay. `);
			kat(`So you played with female characters too? `);
			qcc(`Of course! `);
			kat(`So you know horrible and sexist people are! `);
			qcc(`Yeah, but Beletriad is a very traditional and patriarchal society. I think the sexism is pretty awesome! It's realistic, unlike other bland modern games <i>Neverion</i> isn't trying to white-wash history. And it provides an additional challenge. `);
			txt(`He noticed my infuriated glare and hastily backtracked. `);
			qcc(`Of course, I don't think sexism is awesome in general! Sexism is bad, feminism is good, right? Of course, women could be almost as good at men at everything! It's great when women work like soldiers or police officers ${job}or so!`);
			kat(`...okay, I'm pleased you're so ardent feminist and supporter of women in uniforms. `);
			qcc(`Well... I am... You're welcome! `);
			
			if(mile.immersion > 1){ //TODO
				mile.c_friend++;
				txt(`I assumed it would be the best to change the topic and we spend a lot of time in a very interesting discussion about the realm of Beletriad. He was pleasantly surprised by my knowledge and ready to explain all my questions like why king Rawik just did not use his famed retinue and army to conquer Gothred or how important is the elf slave trade for the economy of the western duchies. `);
			}else if(mile.immersion === 1){
				mile.c_friend++;
				txt(`I assumed it would be the best to change the topic and gain more info about the Beletriad. I would never thought I would ever care about the fate or the history of fictional places like the scheming Free City of Gothred or proud and ancient grand duchy of Sintre. `);
			}else if(mile.immersion < -1){
				txt(`I assumed it would be the best to change the topic and we continued discussing <i>Neverion</i>. Or more like he was talking and I was pretending that I am listening. I had no idea what he was talking about nor did I care. `);
			}else{
				txt(`I assumed it would be the best to change the topic and we continued discussing <i>Neverion</i>. He did most of the talking. I did not care about the deeper lore of the Game very much and most of the time I was not entirely sure what is he talking about. `);
			}
			next();
			break;

		case 103:
			mile.slut++;
			mile.c_bimbo++;
			mile.c_grope = true;
			emo("imp");
			kat(`What if I show you my boobs? `);
			qcc(`W... what?! `);
			txt(`He was puzzled by my sudden suggestion. `);
			kat(`You know, my boobs. I thought you like them! `);
			qcc(`Well, I do, they are great. `);
			kat(`Thanks!`);
			qcc(`But you expect me to forgive you everything just for showing me your breasts? `);
			kat(`What is better, being consumed by pointless hate and pathetically reveling in your grudge or trying to move on, and do something more constructive, possibly involving my breasts? `);
			txt(`I was talking fast and he had trouble keeping up with my arguments, especially since most of his mental capacity was preoccupied with musing about my bosom. `);
			qcc(`Well... maybe... `);
			kat(`Thank you, that's a wise and magnanimous decision! `);
			txt(`I greased him more. `);
			qcc(`However, I have already seen the boobs of your exact AI clone.  `);
			kat(`What if I let you touch them? I mean, they are technically virtual, because I'm virtual and your hands are virtual too and they are absolutely indistinguishable  from the breasts of my AI clone, but still?`);
			if(mile.slut < 4){
				txt(`Trust me, I do not usually let people grope my tits. I was not especially excited about this solution but it seemed simple and quick. `);
			}else{
				//TODO
			}
			qcc(`Okay! `);
			kat(`He gingerly reached forward to touch my chest. I pulled my arms back, letting him anxiously squeeze both my tits. `);
			qcc(`Wow!`);
			kat(`Is it fun?`);
			qcc(`Yeah!`);
			txt(`What a simple man. `);
			qcc(`C... could you undress? `);
			if(mile.slut > 5){
				showLess(0, -1);
				kat(`Sure. `);
				txt(`I crossed my arms and stripped my ${PC.upper.name}.`);
				if(PC.bra){
					txt(`He grabbed my tits again and then reached behind my back and struggled with my bra. His face was bright red but eventually managed to unclasp it. `);
				}
			}else{			
				kat(`No! Of course not! `);
				txt(`I snapped. He was getting too cocky. `);
				qcc(`S..sorry! `);
			}
			txt(`Mesmerized @qcc continued lightly kneading my breasts. After a while, it was starting to get too weird so I decided to stop him: `);
			kat(`That might be enough. `);
			qcc(`O... okay. `);
			kat(`Are we friends now? `);
			qcc(`Y... yes, @kat.`);
			next(`Great! `, showAll);
			
			break;		
	}	
};					
			

///////////////////  WEEK 2   //////////////////////////////////////////////////

//if not mile.c_grope	
export const c_meeting_talk = (index) => {
	switch(index){
		default:
		case 101:
			set.meta();
			placetime("@qcc's subreality");
			mile.ccc++;
			if(mile.c_meeting_bigtits){
				txt(`I decided to enter the Metaverse and visit @qcc. The manipulative way he altered the size of the breasts of my avatar made me extremely annoyed (even though I got used to the bigger size pretty quickly) but I believed our relationship was quickly warming up. I could use his advice to beat the game and hoped he will not vote for any too stupid tasks. Maybe he will even feel so sorry for me so much he convinces the other two to stop with the blackmail altogether? `);
				kat(`Hello! It's me!`);
				qcc(`Hi, @katalt. `);
				kat(`Don't look so eager! I'm not going to let you grope my tits again, nor do anything shady with them! `);
				qcc(`I see. Then what do you want me to do? `);
				kat(`Just to chat and hang out with my new friend! Maybe we can talk about virtual games?`);
			}else{
				txt(`I decided to enter the Metaverse and visit @qcc. We spend my previous visit discussing the game${mile.immersion < 0 ? ", which was pretty annoying but" : " and it was"} pretty beneficial. On one hand, by encouraging him to talk about what he liked and by ${mile.immersion < 1 ? "pretending the interest " : "engaging in the conversation"} I made him slowly change his opinion of me. I still could not understand how could be anybody so petty and dislike me. On the other hand, I was able to extract from his enthusiastic rambling several very useful information about ${mile.immersion < 0 ? "the game" : "Beletriad"}.`);
				kat(`Hello! It's me!`);
				if(mile.advice_2 === 3){
					qcc(`Hi! Your hair looks gorgeous. `);
					kat(`I know! But thanks anyway. `);
				}else{
					qcc(`Hi, @katalt. `);
				}
				txt(`He was still wary but not as hostile as before. `);
			}
			
			qcc(`You're here to manipulate me to tell you even more about Neverion? `);
			kat(`Why so cynical? Can't I just enjoy spending my valuable time with nerdy losers? `);
			txt(`He did not find my quip as funny as I did. `);
			kat(`Come on! Don't be so sour! If it convinces you we can discuss anything else! `);
			qcc(`Okay.`);
			kat(`What do you want to talk about?`);
			qcc(`...`);
			kat(`...`);
			txt(`I smiled at him. He nervously smiled at me. `);
			qcc(`...`);
			kat(`...`);
			txt(`I could see the panic building in his eyes but did not do anything to help him. `);
			qcc(`...`);
			kat(`...`);
			qcc(`...`);
			kat(`So? `);
			qcc(`I have no idea what you're supposed to talk about with a girl! `);
			kat(`That's easy, about the stuff you like or interesting things you do. `);
			qcc(`I don't do anything interesting and I think everything I like would bother you!`);
			kat(`You're probably right. `);

			links_mix(
				[`How did it feel to be a girl? ${spoiler("feminization")}`, 102],
				[`Do you think I'm hot? ${spoiler("bimbo")}`, 103],
				//VICKY4 TODO ${spoiler("friend")
			);
			break;
			
		case 102:
			mile.c_tg++;
			mile.c_meeting_talk = 2;
//TODO - DID HE?????
			qcc(`W... what?!?! `);
			kat(`You told me you tried to play the game as a female character. `);
			qcc(`Well, yeah, I did. But there is nothing wrong with it. `);
			kat(`Or is it?`);
			qcc(`...no?!`);
			kat(`Or is it?`);
			qcc(`No!`);
			kat(`Hah. Sorry. I just wanted to ask how it felt, whether being a girl is different than being a guy.  `);
			txt(`Of course, my main goal was to subtly turn our conversation back to the game without him realizing it.`); 
			qcc(`Well, I guess it was pretty different. Not sure about real life but in the game definitely. Girls are so... softer if you know what I mean? `);
			kat(`No, I have no idea what do you mean. `);
			qcc(`Have you ever wanted to try to be a guy? `);
			kat(`Not really. I mean sometimes I wish to be as naturally respected or as casually careless as a guy but physically? Hmmmm. `);
			qcc(`Well, I agree that in your case it would probably be a downgrade. Your physical form is already amazing. `);
			kat(`That was the clumsiest compliment I received but thank you. Still, I just can't imagine you as a girl. `);
			txt(`He cringed but I pushed him further. `);
			kat(`What was your favorite character? `);
			qcc(`Hmmm, hard to say. Probably the Fallen Priestess for gameplay but the Harlot for roleplaying. `);
			txt(`Somehow I was not surprised he enjoyed roleplaying a whore. `);

			if(ext.class === 1){
				kat(`But gameplay-wise the Harlot wasn't so bad either, was she? She could be pretty good too, doesn't she? `);
				qcc(`Well, yeah, she has few really cool perks like...`);
			}else if(ext.class === 2){
				kat(`What about the Hellion? Did you enjoy playing as a savage barbarian? `);
				qcc(`Of course! She's fun! `);
				kat(`What was your favorite skill? `);
				qcc(`She has a few really great skills like....`);
			}else if(ext.class === 3){
				kat(`Do you think the Fallen Priestess is the best character?! `);
				qcc(`I didn't say t...`);
				kat(`Me too! What do you think is her best combination of spells? `);
				qcc(`Well, it depends on the circumstances. For example...`);
			}

			next(`That went great! `);
			break;
			
		case 103: 
			mile.c_bimbo++;
			emo("imp");
			qcc(`Yes. `);
			kat(`This is supposed to be conversation. I need more than <i>yes</i> or <i>no</i> answer. `);
			qcc(`Why are you even asking me about that!?! You obviously are! `);
			kat(`Thank you. I just wanted an honest opinion from somebody who isn't just a sucker trying to get into my panties. `); //of somebody who didn't like me and has no reason to lie to me.
			txt(`I just enjoyed hearing his praise and hoped this theme of the conversation will make him uncomfortable and insecure.`);
			qcc(`Well... `);
			txt(`He beckoned me to stand up and walked around me, fully focusing on his task. `);
			qcc(`I think you're objectively hot, even though you're not perfect.`);
			kat(`Whoa, not so fast! You don't have to be so unfairly critical!`);
			if(mile.c_meta_boobs){
				qcc(`I think a bit improving the size of your breast was a good call, don't you think? `);
				kat(`No, I'm still very mad at you. `);
				qcc(`I think maybe you could consider getting the same improvement irl? `);
				kat(`I don't believe that's necessary!`);
				qcc(`Well... it's your choice. `);
				kat(`He dismissively shrugged. `);
				qcc(`Maybe you could also...`);
			}else{
				switch(mile.meta_upgrade){
					case 1:
						qcc(`I definitely think that boosting the size of your breasts in the Metaverse was a good call. You would definitely look better with bigger breasts!`);
						txt(`I frowned and crossed my arms in front of my chest. `);
						break;
						
					case 2:
						qcc(`I think that reducing your waist in the Metaverse was definitely the right call!`);
						kat(`What!?! I knew you never talked with a girl before but you can't say that!`);
						break;
					
					case 3:
						txt(`He again walked around me, when he was behind me he noted:`);
						qcc(`I think that boosting your ass in the Metaverse was definitely a good call, you look better this way!`);
						kat(`Hey!`);
						break;
					
					case 4:
						txt(`Did you do something with your lips? `);
						kat(`Maybe? `);
						qcc(`They look great! I wish your irl lips looked the same way!`);
						break;
						
					default: //EMPTY
						break;
				}
			}
			txt(`I did not enjoy this anymore but he continued on and on until he shrugged:`);
			qcc(`There are so many minor things but luckily most of them are fixable. `);
			link(`You're right.`, 104);
			link(`You're wrong!`, 105);
			break;
			
		case 104: 
			mile.sub++; //TODO
			mile.c_meeting_talk = 3;
			emo("cry");
			kat(`You really think so? `);
			txt(`His evaluation was analytical and merciless. It made me pretty anxious about my body. I was not used to people criticizing my look!`);
			qcc(`Don't worry, I still think you're the hottest girl in the school! `);
			next(`Well, that was eye-opening. `);
			break;
			
		case 105: 
			mile.sub--; //TODO
			mile.c_meeting_talk = 4;
			emo("angry");
			kat(`You're not objective! You're just enjoying criticizing and being mean on purpose!`);
			txt(`Retrospectively, I have to admit I took his evaluation badly. I was not used to people criticizing my look!`);
			qcc(`Sorry, @kat, I didn't want to sound mean. I just wanted to provide my honest opinion. You told me you like honesty.`); 
			kat(`You're not honest! You're still mad at me and this is just your pathetic petty revenge! `);
			qcc(`I swear it isn't! I think you overall look great. There are only a few minor things. And it's just my subjective opinion, nothing that should bother you too much.`);	
			kat(`You're wrong and dumb and your opinions are stupid! Goodbye! `);
			next(`How dared he!  `);
			break;
	
	}
};		
			 
		
//if mile.c_grope	
export const c_meeting_bigtits = (index) => {
	//TODO - ACCESS FROM c_meeting_talk
	switch(index){
		default:
		case 101:
				set.meta();
				placetime("@qcc's subreality");
				mile.ccc++;
				mile.c_bimbo++;
				emo("shy");//emo??
				//mile.c_meeting_bigtits = true;
				txt(`I decided to pay @qcc a visit. Just to reassure he will support me. `);
				kat(`Hey! `);
				qcc(`Hello, @katalt. `);
				kat(`Why so coldly? I thought we're friends! `);
				txt(`I was taken aback by his tone. `);
				qcc(`You tricked me! `);
				kat(`Tricked how? Don't be ridiculous! `);
				qcc(`By your sexiness! Just you letting me touch your tits once can't completely change our relationship! `);
				kat(`But you enjoyed it!`);
				qcc(`Well, yeah I did. But I'm not as stupid as you think!`);
				kat(`I never thought you're stupid. Naive? Sure! Insecure? Definitely! Socially useless? Without doubt! Desperately horny? Absolutely! But I never questioned your intelligence. `);
				qcc(`Thank you for reminding me why I don't like you. `);
				kat(`Come on! What if I let you touch my boobs twice? `);
				qcc(`Okay. `);
				kat(`I knew that! You're just trying to manipulate me to grope me again! `);
				qcc(`Well, maybe. `);
				if(mile.immersion > 1){
					kat(`You're worse than Gothredian patricians! `);
					txt(`He just smirked. `);
				}
				kat(`Fine! But this is the last time and you swear you make sure you won't give me any stupid and unreasonable tasks, right? `);
				qcc(`Okay. Just... could you make them bigger? `);
				kat(`Aren't you a bit greedy!?!`);
				if(mile.meta_boobs > 0){
					qcc(`I would swear your breasts are a bit smaller IRL. You wish to have bigger tits, don't you?`);
				}else{
					qcc(`You never imagined having larger breasts?`);
				}
				
				link(`Having bigger tits might be fun... `, 102);
				link(`Well, maybe.... `, 102);
				link(`Of course not! `, 102);
			break;
			
		case 102:
				emo("anoy");
				qcc(`Let's just try it, okay? `);
				kat(`I don't know...`);
			
				if(mile.blackmail){
					txt(`Then I noticed my chest is growing. He had to use data from my Cloud to manipulate my Metaverse avatar settings without my approval. `);
				}else{
					txt(`Then I noticed my chest is growing. I did not understand how he could manipulate my Metaverse avatar settings without my approval. `);
				}
				
				if(mile.slut > 4){
					kat(`${choice("small", "Stop it!")} / ${choice("big", "Uhhhg, okay.")} / ${choice("bigger", "Make them even bigger!")}`);
				}else{
					kat(`${choice("small", "Stop it!")} / ${choice("big", "Uhhhg, okay.")} `);
				}
				qcc(`May I?`);
				kat(`Hmmm.`);
				txt(`I let him to touch my now far larger breasts, weight them in his hands, even squeeze a little. `);
				kat(`Okay, enough. Now change them back.  `);
				qcc(`I won't. `);
				kat(`What do you mean?`); 
				qcc(`I won't change them back. I like them more this way. `);
				txt(`He announced smugly.`);
				kat(`Haha, funny. You have to change them back. `);
				qcc(`I don't have to do anything. `);
				//kat(`What the hell? `);
				//qcc(`I think you look better this way! `);
				
				if(mile.sub < 2){
					kat(`You tricked me! I  trusted you, tried to be nice and this is how you repay me?! `);
					qcc(`Please, don't be mad! I think you'll love having bigger breasts! Just try them for a few days, if you won't like them then I'll change you back, I swear. `);
					kat(`Why should I like them? Do you know how annoying it is to have such huge boobs? `);
				}else{
					kat(`What?!? Please, change them back! I can't stay this way!`); 
					qcc(`I think I deserve far larger compensation for all the years you were purposely mean to me! But don't worry, I change them back. Eventually. But maybe you'll find out you actually prefer to have larger breasts? `);
					kat(`I really don't think so. Do you know how annoying it is to have such huge boobs? `);
				}
				qcc(`It feels pretty good... or so I heard! `);
				txt(`I tried to convince him but vainly. And he somehow blocked the access to the settings of my avatar. I didn't even know that was possible! It seemed I will have to stay this way, at least for several days before he changes his mind. `);
				
				next(`Leave. `, ()=> {
					mile.bimbo_mods += mile.c_meta_boobs > 1 ? +1 : -1;
					mile.meta_boobs += mile.c_meta_boobs;
					if(mile.c_meta_boobs === 3) mile.slut++;
				});
				
				effects([{
						id: "small",
						fce(){
							mile.c_meeting_bigtits = 1;
							mile.c_meta_boobs = 1;
						},
						relative: {
							breastSize:16,
							breastPerkiness: 4,
						},
					},{
						id: "big",
						fce(){
							mile.c_meeting_bigtits = 2;
							mile.c_meta_boobs = 2;
						},
						relative: {
							breastSize:26,
							breastPerkiness: 9,
						},
					},{
						id: "bigger",
						fce(){
							mile.c_meeting_bigtits = 3;
							mile.c_meta_boobs = 3;
						},
						relative: {
							breastSize: 35,
							breastPerkiness: 12,
						},
				}]);
			break;	
	}
};


//ADVICE 2
export const c_advice_2 = (index) => {
	switch(index){
		default:
		case 101:
			set.meta();
			placetime("@qcc's subreality");
			mile.ccc++;
			mile.advice_2 = 3;
			mile.c_bimbo++;
			
			if(mile.ccc > 2){
				txt(`I deciced to ask @qcc. It seemed I almost convinced him to be on my side. `);
			}else if(mile.advice_1 === 3){
				txt(`Again, I decided to ask @qcc. He helped me once, he will surelly help me the second time. `);
			}else{
				txt(`This thime I decided to ask for his help  @qcc. It seemed I might be able to convince him. `);
			}
			
			
			if(mile.c_friend > 0){
				kat(`Hello, my good friend!`);
				qcc(`Hi, @kat?`);
			}else{
				kat(`Hello!`);
				qcc(`Hi, @kat!`);
			}
			txt(`@qcc greeted me.`); 
			if(mile.ccc > 1){
				qcc(`Are you here to socialize again? `);
			}else{
				qcc(`What do you need? `);
			}
			kat(`I just need to talk with somebody about the game! There's so much to process and I obviously can't talk about the game with @eva or @sas because I don't want them to think I'm a pathetic person who wastes her time with videogames. `);
			qcc(`Oh. Okay. Well, at least you enjoy the game. What do you like? `);
			kat(`It's so big and so open! And there so many ways to solve the quest! Like the Arazors storyline. What do you think is the best and fastest way to reach the treasure? `);
			qcc(`You won't trick me into helping you to beat the game! `);
			
			if(mile.c_meeting_bigtits){
				kat(`What?!? <i>You</i> complain about tricking people? `);
				txt(`I did not have to point at my bosom, it was the only thing he stared at since the beginning of our conversation anyway. `);
			}else if(mile.c_bimbo > 0 || mile.c_friend > 0){
				kat(`Come on! I was nice to you so now you should be nice to me! `);
				qcc(`I suspected you being nice was just a cold calculation! `);
			}
			
			kat(`Please! I'm completely lost! You don't have to tell me everything, I can find the treasure myself, just give a minor advice on how to start! `);
			qcc(`Fine! `);
			kat(`Awesome! So where is the treasure?! `);
			qcc(`First you have to hire your first mate. It could be either @lys, @ste or @gal. Your first mate will arrange the rest of the preparation. Then you have to sail to Karged, the map is randomised but to reach Karged you always just have to sail west. And don't trust anybody. `);
			
			
	
			link(`Thanks! That's what I needed! `, 102);
			break;
			
		case 102:
			emo("suspect");//emo??
			qcc(`Wait! And my reward? `);
			kat(`What would you like? `);
			qcc(`It might seem weird... `);
			kat(`No worries, just another weird experience to my collection. `);
			qcc(`... but I always wondered how would you look like with pink hair. `);
			//txt(`Like ${choice("this","l_pink")} or ${choice("this","d_pink")}? My new hairstyle rendered nearly instantly. `);
			kat(`Like ${choice("light")}, ${choice("bright")} or ${choice("dark")}? My new hairstyle rendered nearly instantly. `);
			qcc(`Yes, but IRL.`);
			kat(`You mean I should actually dye my hair in the real life?`);
			qcc(`Yes. `);
			txt(`Hmmm... And why not? In the worst case, I can easily (well, not as easy as in the Metaverse) change my color back. `);
		
			if( !mile.qcc_extend_hair && !mile.shop_haircut && PC.getDim("hairLength") < 60){
				qcc(`Also, you would look far better with longer hair. I think it should be a good idea to get your hair extended. `);
				mile.qcc_extend_hair = true;
			}


			effects([{
					id: "light",
					absolute: {
						hairHue: 330,
						hairSaturation: 80,
						hairLightness: 75,
					},
				},{
					id: "dark",
					absolute: {
						hairHue: 330,
						hairSaturation: 80,
						hairLightness: 50,
					},
				},{
					id: "bright",
					absolute: {
						hairHue: 330,
						hairSaturation: 100,
						hairLightness: 60,
					},
				/*
				},{
					
					
					//
					id: "l_pink",
					body: {
						hairHue: 330 -(PC.basedim.hairHue+PC.Mods.hairHue),
						hairSaturation: 98 -(PC.basedim.hairSaturation+PC.Mods.hairSaturation),
						hairLightness: 50 -(PC.basedim.hairLightness+PC.Mods.hairLightness),
					}
				},{
					id: "d_pink",
					body: {
						hairHue: 330 -(PC.basedim.hairHue+PC.Mods.hairHue),
						hairSaturation: 60 -(PC.basedim.hairSaturation+PC.Mods.hairSaturation),
						hairLightness: 70 -(PC.basedim.hairLightness+PC.Mods.hairLightness),
					}
					*/
			}]);
			
			add_tomorrow("afternoon2", "dye_action");
			
			next(`Hmmm... fine, but it's a stupid idea. `, ()=> mile.bimbo_mods--);
			next(`Okay! `);
			next(`Awesome, I wanted to try a new look.  `, ()=> mile.bimbo_mods++);
		break;
	}			
};


///////////////////  WEEK 3  //////////////////////////////////////////////////


export const c_advice_3 = (index) => {
	const price_initial = Math.floor(groups.panties * 2);
	const price_haggled = Math.floor(groups.panties * 3);
	
	switch(index){
		default:
		case 101:
//TODO - BLOCK IF PREVIOUS TASK FAILED FULFIL
			set.meta();
			placetime("@qcc's subreality");
			mile.ccc++;
			mile.advice_3_3 = true;
			
			if(mile.ccc > 3){
				kat(`Hello! `);
				qcc(`Hey, @kat!`);
				txt(`@qcc smiled at me. I think he began to like me, which was pretty much inevitable considering my lovable personality. `);
				qcc(`Are you here to hang out?`);
			}else{
				txt(`@qcc still looked at me with suspicion.`);
				qcc(`What do you need?`);
			}

			if(mile.ccc > 3 /*redundancy*/ && mile.c_friend > 1){ //TODO
				kat(`Yeah! Let's hang out! `);
				qcc(`Cool. `);
				kat(`I have a crazy idea - did you know there's a co-op mode in <i>Neverion</i>? We can hang out there, exploring the wilderness, slaying dragons and so on! `);
				qcc(`Of course, I know that. Also, I know what you're trying to do. I'm not going to finish the game for you!`);
			}	

			if(mile.advice_1 === 3 || mile.advice_2 === 3){
				kat(`Well, honestly, I'm kinda lost and would need a small nudge in the right direction. `);
				qcc(`You want another piece of advice? `);
				kat(`Well, maybe a bit more than and advice. The Amaranthine Forest is so dangerous and I am unable to find the right companion, so I thought since there's the co-op mode and you love playing the game so much...`);
				qcc(`I'm not going to finish the game for you!`);
			}else{
				kat(`I just wanted to ask something about <i>Neverion</i>`);
				qcc(`Yeah? `);
				kat(`Well, I'm kinda stuck. The Amaranthine Forest is so dangerous and I am unable to find the right companion. `);
				qcc(`Okay? What do you want?`);
				kat(`Well, I thought, since there's the co-op mode and you love playing the game so much, maybe we could go on a quest together? That sounds like fun, does it not?`);
				qcc(`I'm not going to finish the game for you!`);
			}

			kat(`I don't want you to finish the game for me! At least not the whole game... `);
			qcc(`Sorry, but I can't do it. `);
			kat(`I can bribe you! Ask for anything!`);
			txt(`His eyes brightened. `);
			qcc(`Do you mean absolutely anything?`);
			kat(`...no. Nothing weird or sexual! `);
			txt(`My initial offer was maybe too generous.`);
			qcc(`Okay.`);
			kat(`So? `);
			qcc(`I mean... well... `);
			kat(`?`);
			qcc(`..ehm... `)
			kat(`Just say it! You have nothing to afraid! I just probably tell you you're disgusting and I'd never ever do that! `);
			qcc(`I want your panties. `);
			kat(`WHAT?!?! My panties?!?`);
			qcc(`It's not for anything weird, just a fetish I have! `);
			kat(`I can't just give you my panties!`);
			qcc(`Right... I could pay for them...`);
			kat(`That isn't the issue! ...how much? `);
			qcc(`Dunno, ${price_initial}?`);
					
			con_link(mile.sub < 10, `too submissive`, `I want at least ${price_haggled}!`, 104, ()=> counter.haggle = true);
			link(`Fine. `, 104, ()=> counter.haggle = false);
			link(`No! `, 103);
			break;

		case 102:
			set.meta();
			qcc(`Hey! Did you change your mind about the panties? `);
			link(`Yes. `, 104);
			chapter(`No. `, mile.advice_3 ? `advice_4` :  `advice_3`, 102);			
			break;
			
		case 103:			
			kat(`I'm not going to sell my panties to  a disgusting pervert! No offence! `);
			qcc(`Understandable. Sorry, it was dumb idea. `);
			chapter(`Leave. `, mile.advice_3 ? `advice_4` :  `advice_3`, 102);
			break;

		case 104: 	
			if(counter.haggle){
				qcc(`Fine!`);
				payment(price_haggled, "qcc");
			}else{
				payment(price_initial, "qcc");
			}
			if(mile.advice_3){
				mile.advice_4 = 3;
			}else{
				mile.advice_3 = 3;
			}
			//mile.a_tg
			txt(`I shrugged. I could be worse than that. `);
			kat(`I promise I'll bring you one of my panties. `);
			qcc(`And I'll help you with your quest! `);
			next();
			
			add_workday("classes", "c_advice_3_reaction");
			break;
	}
}


export const c_advice_3_reaction = (index) => {
	switch(index){
		default:
		case 101:
			set.irl();
			placetime("locker room");
			mile.c_panties_on = false;
			emo("anoy");//emo??
			txt(`When I was alone, nervous @qcc approached. He was not sure whether I will honor our deal or whether I was just teasing him. `);
			qcc(`Do you have... you know? `); 
			kat(`Panties for you? Yeah! `);
			
			link(`Give him panties you brought. `, 102);
			if(mile.slut > 1 && wears.panties && !PC.panties.locked) link(`Pull down ${decapitalise(PC.panties.name)}. `, 103); 
			break;
		
		case 102:
			mile.c_panties_worn = false;
			txt(`With a sigh, I reached into my bag and handed him a pair of my panties. `);
			kat(`Are those fine? `);
			qcc(`Of course they are, @kat! Thank you!!! `);
			txt(`He was staring at them with amazement, gingerly touching the delicate fabric with his fingers. `);
			qcc(`If you'll need any help with anything else... I'm sure we came up with some deal! `);
			
			next();
			con_link(mile.sub < 5, `submissive`, `Make him put them on. `, 105, ()=> createWear("hipsters", undefined, undefined, effigy.qcc) );
			
			break;
			
		case 103:
			emo("imp");
			{
				mile.c_panties_worn = true;
			
				const item = recreate(PC.panties);
				remove(`panties`);
				
				if(wears.skirt){
					txt(`I moved my hands under my skirt and pulled down the panties I was wearing and then handed them to absolutely stunned @qcc. He was so shocked he was not even able to take them. `);
				}else{
					qcc(`Why are we going to a bathroom? `);
					if(mile.slut > 8){
						txt(`I did not respond and he was pretty shocked and blushing when I right in front of him started stripping my ${wdesc.lower} and then pulled down my panties. `);
					}else{
						txt(`I did not respond and disappeared in the booth for a moment so I could strip my ${wdesc.lower} and then pulled down my panties. @qcc was stuned when I returned and he realised what I just did.`);
					}
				}
				kat(`Are those fine? `);
				qcc(`O... of course they are, @kat! Thank you. `);
				txt(`He quickly grabbed them. `);
				qcc(`If you'll need any help with anything else... I'm sure we came up with some deal! `);
				
				next();
				con_link(mile.sub < 5, `submissive`, `Make him put them on. `, 105, ()=> createWear("hipsters", undefined, undefined, effigy.qcc) );
			}
			break;
			
		case 105:
			mile.c_panties_on = true;
			mile.c_tg++
			mile.sub--;
			emo("smug");
			effigy.qcc.showLess(-1, 1, 2);
			txt(`He was about to put them into his bag but then I stopped him: `);
			kat(`Put them on! `)
			qcc(`What?!?! `);
			kat(`Put them on! I want to see you in them! `);
			qcc(`But...`);
			kat(`Do it! `)
			txt(`I harshly ordered. He reluctantly stripped down his pants and underwear, his pride hidden under his long shirt, and pulled on my panties. `);
			qcc(`I.. is that okay? `);
			kat(`Lift up your shirt! `);
			qcc(`But... `);
			txt(`I did it myself. Of course, they were stretched by his huge boner. Dressing in my underwear made him obviously very aroused. `);
			kat(`You're such a pathetic perverted loser! `);
			qcc(`I'm sorry, @kat! `);
			next(`That's fine. `);
			next(`You should be! `);
			break;
		
	}
}




export const c_avatar = (index) => {
	switch(index){
		default:
		case 101:
			set.meta();
			placetime("@qcc's subreality");
			mile.ccc++;

			if(mile.c_meeting_talk === 3 || mile.c_meeting_talk === 4){
				kat(`Hi!`);
				qcc(`Hello.`);
				txt(`@qcc seemed strangely anxious.`);
				kat(`What is going on?`);
				qcc(`I was thinking about what I told you... when you asked about your body... maybe I was too unfairly critical... sorry about that...`);

				if(mile.c_meeting_talk === 4){
					kat(`You don't have to apologize. I just kinda take badly being criticized, especially by somebody like you...`);
					txt(`He stared at me and I hastily changed the direction:`);
					kat(`... you know, somebody with a good taste like you.`);
					qcc(`Sure.`);
					kat(`I'm serious.`);
				}else if(mile.c_meeting_talk === 3){
					kat(`You don't have to apologize. I did ask you for honest feedback and you gave it to me. I'm very grateful to you. Even though you made me feel a bit insecure.`);
					qcc(`WHAT? Come, you don't have to feel insecure! Those were just a few minor dumb suggestions!`);
					kat(`I was a lot of suggestions!`);
				}

				qcc(`Do you trust me?`);
				if(mile.c_meeting_bigtits){
					kat(`You mean after you tricked me and forced me to keep these ${["", "boosted breasts", "huge tits", "ridiculous melons"][mile.c_meta_boobs]}?`);
					qcc(`Oh, yeah. You know what? Follow me and I'll return you full control over your avatar.`);
					kat(`What do you want to do?`);
				}else{
					kat(`...I'm inclined to say <i>no</i>.`);
				}
				
			}else{
				kat(`Hi.`);
				qcc(`Hello.`);
				kat(`We need to talk about my tits.`);
				qcc(`WHAT?! ...oh ...I ...of course! ...gladly!`);
				kat(`I want full control over my avatar back!`);
				qcc(`Oh, you mean that.`);
				kat(`Yeah!`);
				qcc(`So, you don't enjoy having bigger tits.`);
				kat(`I don't enjoy not having full control over my avatar!`);
				qcc(`You know, follow me and I'll give you the control back.`);
				kat(`What do you want to do?`);
			}

			qcc(`I just want to see how would you look with several minor improvements.`);
			kat(`I guess. What kind of improvements?`);
			qcc(`Improvements that will make your avatar look hotter. Why are you so conservative? You can go crazy with the look of your avatar!`);

			if(!mile.c_meeting_talk === 3){
				txt(`I told him the same thing I told you on the first day in the <i>Intro</i>:`);
				kat(`I'm not one of those people who feel insecure about their appearance because they are ugly so that's why my avatar looks basically like the real me.`);
				qcc(`Let's just try it for once!`);	
			}

			counter.reluctant = false;
			link(`Okay, might be fun. `, 102, ()=> mile.bimbo_mods++);
			link(`...fine.`, 102);
			link(`But just experimenting, nothing permanent! `, 102, ()=>{
				counter.reluctant = true;
				mile.bimbo_mods++;
			});
			break;


		case 102:
			emo("suspect");//emo??
			if(counter.reluctant){
				txt(`Nothing virtual is permanent! But don't worry, I won't force you into anything you woun't like!`);		
			}else{
				txt(`@qcc summoned a mirror.`);
			}
			if(mile.meta_upgrade !== 4){
				qcc(`For example, your lips might be a little ${choice("lips_1", "plumpier")}.`);
				kat(`${choice("lips_0", "Nah,")} my lips are fine / Maybe ${choice("lips_2", "more")} than little?`);
				qcc(`Whatever you wish!`);
				kat(`What else?`);
			}
			if(mile.meta_upgrade !== 2){
				qcc(`What about your ${choice("waist_1", "waist")}?`);
				kat(`What is wrong with my waist? Are you saying I'm fat!?!`);
				qcc(`No! Of course not! I didn't want to offend you. But since your avatar doesn't have to care about diets, you're not bound by the same rules as irl.`);
				kat(`I'm very ${choice("waist_0", "offended")} / You're right... I could push it a bit ${choice("waist_2", "further")} than irl.`);
			}
			if(mile.meta_upgrade !== 3){
				qcc(`Then there's your butt.`);
				kat(`My butt?`);
				qcc(`It could be ${choice("butt_1", "thicker")}. You know, like has Ymaneesha.`);
				if(mile.girls_music_video === 1){
					kat(`She has a great ass, doesn't she?`);
				}else{
					kat(`She is a needy slut who got her ass enlarged.`);
				}
				qcc(`Yeah! I don't usually listen to her songs, but I saw her last music video.`);
				kat(`I prefer my ass ${choice("butt_0", "the way it is")} / You want me to have as ${choice("butt_2", "big")} ass as Ymaneesha?`);
				qcc(`Either way, I think you have a lovely ass.`);
				txt(`For a moment he looked hesitant, considering whether he should emphasize his compliment by smacking my bottom but he faltered.`);
			}

			link(`Continue. `, 103);

			//lips
			effects([
				{
					id: "lips_0",
					fce(){
						mile.c_avat_lips = 0;
					},
				},
				{
					id: "lips_1",
					fce(){
						mile.c_avat_lips = 1;
					},
					relative: {
						lipTopSize: 20,
						lipBotSize: 30,
						lipWidth: 25,
					},
				},
				{
					id: "lips_2",
					fce(){
						mile.c_avat_lips = 2;
					},
					relative: {
						lipTopSize: 30,
						lipBotSize: 50,
						lipWidth: 40,
					},
				},
			]);

			//waist
			effects([
				{
					id: "waist_0",
					fce(){
						mile.c_avat_waist = 0;
					},
				},
				{
					id: "waist_1",
					fce(){
						mile.c_avat_waist = 1;
					},
					relative: {
						waistWidth: -8,
					},
				},
				{
					id: "waist_2",
					fce(){
						mile.c_avat_waist = 2;
					},
					relative: {
						waistWidth: -16,
					},
				},
			]);

			//butt
			effects([
				{
					id: "butt_0",
					fce(){
						mile.c_avat_butt = 0;
					},
				},
				{
					id: "butt_1",
					fce(){
						mile.c_avat_butt = 1;
					},
					relative: {
						buttFullness: 12,
						legFullness: 2,
						legFem: 3,
					},
				},
				{
					id: "butt_2",
					fce(){
						mile.c_avat_butt = 2;
					},
					relative: {
						buttFullness: 22,
						legFullness: 5,
						legFem: 6,
					},
				},
			]);
			break;

	case 103:
			emo("neutral");//emo??
			if( PC.getDim("hairLength") < 60 ){
				kat(`Anything else?`);
				qcc(`Of course! Hmmm... maybe ${choice("hair_1", "longer")} hair would make you look more feminine?`);
				kat(`I prefer ${choice("hair_0", "short")} hair / I was thinking about letting my hair grow  ${choice("hair_2", "long")}, I was getting tired of my short hairstyle.`);

				if( !mile.qcc_extend_hair && !mile.shop_haircut){
					qcc(`You look good with long hair. Even in real life. `);
					kat(`Letting my hair grow in real life isn't so easy, it would take a while. `);
					qcc(`Why wait? We have the technology. You can get them extended. `);
					mile.qcc_extend_hair = true;
				}


			}else if( PC.getDim("hairLength") < 80 ){
				kat(`Anything else?`);
				qcc(`Of course! Hmmm... maybe ${choice("hair_2", "longer")} hair would make you look more feminine?`);
				kat(`I prefer ${choice("hair_0", "shorter")} hair. / Looks cute.`);
			}

			if(mile.meta_boobs <= 1){
				txt(`I thought that was everything but @qcc longingly stared at my chest.`);
				kat(`Just say it. You want my tits ${choice("tits_1", "bigger")}?`);
				qcc(`Well... yeah... ...you deserve the best tits there are!`);
				kat(`Fine, they could be ${choice("tits_2", "huge")}. / ${choice("tits_0", "No thanks")}.`);
				
			}else if(mile.meta_boobs <= 3){
				txt(`I thought that was everything but then I noticed him longingly staring at my chest.`);
				kat(`No! Don't even think about it!`);
				qcc(`Come on, @kat!`);
				kat(`My tits are already enormous!`);
				qcc(`They are nice but definitely not enormous. They could definitely be ${choice("tits_1", "bigger")}!`);
				kat(`Fine! / ${choice("tits_0", "No!")} Are you out of your mind!`);
			}

			link(`Continue. `, 104);

			effects([
				{
					id: "hair_0",
					fce(){
						mile.c_avat_hair = 0;
					},
				},
				{
					id: "hair_1",
					fce(){
						mile.c_avat_hair = 1;
					},
					absolute: {
						hairLength: 80,
					},
				},
				{
					id: "hair_2",
					fce(){
						mile.c_avat_hair = 2;
					},
					absolute: {
						hairLength: 110,
					},
				},
			]);


			//tits
			effects([
				{
					id: "tits_0",
					fce(){
						mile.c_avat_tits = 0;
					},
				},
				{
					id: "tits_1",
					fce(){
						mile.c_avat_tits = 1;
					},
					relative: {
						breastSize: 15,
						breastPerkiness: 4,
					},
				},{
					id: "tits_2",
					fce(){
						mile.c_avat_tits = 2;
					},
					relative: {
						breastSize: 25,
						breastPerkiness: 9,
					},
				},
			]);
			break;


	case 104: 
			counter.c_avat = ["tits","waist","butt","hair","tits"].reduce( (acc, key) => acc + mile[`c_avat_${key}`], 0 );
	
			if(counter.c_avat <= 2){
				mile.bimbo_mods--;
				mile.c_avat = 1;
				
				qcc(`You're no fun! I thought you would enjoy altering your avatar. `);
				kat(`Well, I did not. I prefer my ordinary proportions.`);
				txt(`I shrugged. I did not like his suggestions. `);
				
			}else if(counter.c_avat >= 6){
				mile.bimbo_mods++;
				mile.c_avat = 3;
				
				kat(`So? Do I look better? `);
				qcc(`You look amazing. You're really into body modifications, aren't you? `);
				kat(`Well, I wanted to try something crazy with the dimensions of my avatar. `);
				txt(`I watched my reflection in the mirror. My avatar looked and - more importantly - felt different. But I kinda enjoyed the new me. `);
				
			}else{
				mile.c_avat = 2;
				
				qcc(`That wasn't so bad, was it? `);
				kat(`I guess it wasn't.`);
				qcc(`Now you look so much better! `);
				kat(`Hmmmm....`);
				txt(`I watched my reflection in the mirror. Maybe he was right. My avatar looked so much better with several minor improvements.  `);
			
			}
				
		
			mile.meta_boobs += mile.c_avat_tits;
			mile.c_meta_boobs += mile.c_avat_tits;
			//txt(counter.c_avat);
			
			if(mile.c_meeting_bigtits){
				kat(`You also promised to give me back full control over the dimensions of my avatar? `);
				qcc(`You're right... okay, you have full control back. `);
				kat(`Thanks. `);
				qcc(`So now you can keep your avatar's breasts enlarged just because you want to, not because I tricked you. `);
				next(`Exactly. `);
				next(`We'll see.`); 
				link(`No, I'm immediatelly going back to my original size! `, 110);
				
			}else{
				next(`Leave. `);
			}
			break;
			
		case 110:
			set.irl();
			mile.slut--;
			mile.meta_boobs = 0;
			mile.bimbo_mods--;
			
			ext.meta.breastSize = 0;
			ext.meta.breastPerkiness = 0; //clumsy af
			set.meta();
			qcc(`What a shame! `);
			next(`Leave. `);

	}	
};			
	
	
	

export const c_boobjob = (index) => {
	switch(index){
		default:
		case 101:
			set.meta();
			mile.ccc++;
			placetime("@qcc's subreality");
			emo("shock");//emo??
			
			if(mile.c_friend > 0){
				txt(`I decided to hang out with @qcc. I wanted to know his opinion about the latest episode of <i>Narrenturm</i> and was surprised he had not seen it. He explained he read the book and after getting burned so many times by atrocious adaptations he did not even bother to watch it. That was a next-level snobbery, I did not understand why he just cannot simply enjoy things. He could at least watch it for battle scenes and boobs, there were plenty of both. Speaking of boobs, his constant staring at mine was a bit unnerving.`);
				kat(`You can stare at my chest as much as you want, I don't mind.`);
				txt(`It was fun to making him embarrassed and stumbling:`);
				qcc(`S... sorry, @kat! I was just thinking about...`);
				kat(`About my tits?`);
			}else{
				txt(`I decided to meet @qcc in the Metaverse`);
				kat(`Hey!`);
				qcc(`Hi, @kat. It's always lovely to see you.`);
				kat(`Especially since you so horribly mauled my avatar to fit more your twisted fetishes, right?`);
				qcc(`You weren't protesting too much.`);
				txt(`He smirked.`);
				
			}
			
			qcc(`Please, don't get offended but have you ever thought about getting a boobjob? `);			
			if(mile.meta_boobs > 0){
				if(mile.meta_boobs >= 3){
					kat(`I think they're already too huge. `);
				}else{
					kat(`Boosting them a few more sizes? `);
				}
				qcc(`I meant in real life. `);
			}
			
			kat(`What?!?`);
			txt(`That came up completely out of nowhere! `);
			
			
			if(mile.meta_boobs > 1){
				qcc(`Imagine looking as hot as your virtual avatar! `);
			}else{
				kat(`What's wrong with my tits?!? Aren't they good enough?!? `);
				qcc(`Well, they could be bigger and better...`);
			}
			
			if(mile.bimbo_mods > 0){
				kat(`Do you really think so? I'm not sure it's a good idea. `);
			}else{
				kat(`That's a horrible idea! I'm fully satisfied with my breast size!`);
			}
			
			qcc(`You don't have to be afraid to change your body.`);
			kat(`I'm not afraid of anything! And don't try to manipulate me!`);
			qcc(`I don't want to push you into anything! I just thought you might consider it, especially since it's so easy.`);
			
			link(`Easy?!? `, 102);
		break;
	
	case 102:
			emo("shy");//emo??
			kat(`Getting breast implants definitely isn't simple! They would cut my boobs open to insert the implants! That has to be bad, even with regen.`);
			qcc(`Have you ever watched that bimbo game show where people get bimbotized?`);
			kat(`Yeah, a few times? Why?`);
			qcc(`They're using a sarcophagus that somehow stimulates natural body growth.`);
			kat(`I'm not going to compete on that game show and I don't think they would let me use the sarcophagus for free!`);
			qcc(`Of course not! That was just an example, I believe modern cosmetic clinics use similar machines.`);
			kat(`And it has to cost a fortune!`);
			qcc(`Well, it probably isn't too expensive... and in the long run, it would be a good investment... and you can find a summer job...`);
			kat(`In a topless bar, right? To not let them go to waste.`);
			qcc(`...yeah, why not.`);
			
			next(`It might be nice to have bigger breasts. `, ()=>{ 
				mile.bimbo_mods++;
				mile.c_boobjob = 3;
			});
			next(`I will think about it. `,()=>{ 
				mile.c_boobjob = 2;
			});
			next(`I'm fine with my breast size. `,()=>{ 
				mile.bimbo_mods--;
				mile.c_boobjob = 1;
			});
		break;
	
	}
}
	
	
	

					
//QCC SHOPPING 
export const c_shopping = (index)=> { //TODO - ASK IF ENJOYS PINK? 
	const fren = mile.sub < 0 || mile.c_friend > 3; //acting friendly and considerate X acting domineeringly 
	const top_slut_min = 3; //nice tee
	const top_slut_max = 12; //slutty tube top 
	const bot_slut_min = 5; //microSkirt
	const bot_slut_max = 14; //nanoSkirt
	const under_slut_min = 4; //hipsters
	const under_slut_max = 13; //crotchless
	
	const back = function(){
		const extra = [
			"bimboBotOpenTop", 
			"bimboTee", 
			"bimboTubeTop",
			"bimboBorderlineSkirt",
			"bimboThong",
			"bimboPantyhose",
			"bimboStockings",
		];
		extra.forEach(   key => PC.inventory.push( create(key) )   );
				
		if(counter.bot + counter.top + counter.panties > 0 && mile.slut < 11) mile.slut++;
		if(counter.bot + counter.top + counter.panties < 0 && mile.slut > 10) mile.slut--;
		removeEverything(PC, PC.inventory); //TODO - check if works
		quickLoadOutfit();	
	}
	
	switch(index){
		default:
		case 101: 
			mile.c_shopping = 1;
			set.irl();
			mile.ccc++;
			quickSaveOutfit();
			emo('relax');
			placetime("@qcc's place");
			
			kat(`Huh, you're still not tired of them.`);
			txt(`Embarrassed @qcc immediately raised his eyes. He had trouble focusing on anything else than my enlarged breasts and his constant staring was making me pretty ${mile.slut < 6 ? "uncomfortable" : "amused"}.`);
			qcc(`Sorry, @katalt. I just... they look so lovely. I wish I had such amazing tits, it has to be such fun to touch them and squeeze them.`);
			kat(`You're such a pervert.`);
			qcc(`And you're even more popular among the guys than before! If you could hear what are they saying about your new breasts and what would they like to do with them...`);
			kat(`I think I prefer not to hear that.`);
			qcc(`And it was a shrewd investment. The lives of big-titted girls are definitely easier.`);
			kat(`Having ${mile.boobs > 1 ? "huge tits" : "bigger breasts"} doesn't have only benefits, there are many downsides.`);
			qcc(`Like what?`);
			kat(`They are heavier, I'm still unable to get used to their weight. ${mile.boobs > 1 ? "And they are clumsy and too bouncy. " : ""}`);
			qcc(`Oh, I haven't thought about that.`);
			if(mile.girls_boobjob > 0){
			kat(`And I made plenty of girls jealous and angry. I know they are talking about my tits behind my back, how I'm just an attention-whoring bimbo.`);
			qcc(`Those bitches!`);
			}else{
			kat(`And @eva told me I'm looking like an attention-whoring bimbo!`);
			qcc(`What a bitch!`);
			}
			kat(`And I didn't realize that none of my clothes will fit me. Those tees and tops I'm still able to put on are so stretched I am looking like a slut even when I'm not trying to!`);
			qcc(`In that case, you should buy new clothes. Just yesterday I was walking around a shop window and there was this gorgeous minidress that would look so adorable on you!`);
			kat(`What a brilliant idea! I would love to buy a new wardrobe but after paying for the breast enlargement I have no money left. And don't even dare to suggest I should become an escort or anything stupid like that!`);
			qcc(`Oh. ...well... maybe I could help you. I have money and maybe I could buy you a new top or two...`);
			kat(`...where is the catch?`);
			qcc(`What catch?`);
			kat(`There's always a catch when I'm dealing with you! Will I have to be your slave until I repay everything or what?`);
			qcc(`Of course not! It would be a gift! Although...`);
			kat(`What?`);

			if(fren){ //TODO
				qcc(`I want to see what clothes you would like but since it's me who is paying, I have the final decision. That sounds reasonable, doesn't it?`);
			}else{
				qcc(`I'm paying so I'm in charge. You'll do what I tell you and we'll buy what I decide! That sounds reasonable, doesn't it?`);
			}
			txt(`I nodded. What was the worst that could happen? Me getting clothes so horrible I will never wear them for free?`);
			link(`Shopping. `, 102);
			break;
	
	case 102:
			if(false){
				//alredy shopped together 
			}else{
				txt(`We went downtown and he seemed more enthusiastic about buying female clothes than I would expect.`);
			}
			qcc(`${mile.girls_boobjob > 0 ? "Girls say" : "@eva said"} you look like an attention-seeking bimbo? Let's make ${mile.girls_boobjob > 0 ? "them" : "her"} even angrier by getting you some cute bimbo outfits!`);
			if(mile.sub < 4){
				kat(`I like the way you think!`);
				txt(`I smirked.`);
			}else{
				kat(`I would prefer to make them less angry, not more!`);
				txt(`I shrugged.`);
			}
			txt(`We went to find a nice top. @qcc selected several and then send me into the fitting room. I put them on, one after another, and then modeled for him.`);
			
			
			if(mile.slut < top_slut_min){
					kat(`Isn't this ${choice("top", "top")} a bit too much?`);
					txt(`It showed more skin than it covered.`);
					if(fren){
						qcc(`No, it is exactly right! But if you want, you can try ${choice("tee", "something else")}.`);
					}else{
						qcc(`Too bad! I like it!`);
					}
			}else if(mile.slut > top_slut_max){
					kat(`Do you think this ${choice("top", "top")} is sexy enough?`);
					qcc(`Absolutely! But if you prefer, we can try to find something ${choice("slut_top", "sluttier")}`);
			}else{
					kat(`Do I look good in this ${choice("top", "top")}?`);
					txt(`The top showed more skin than it covered.`);
					if(fren){
						qcc(`Yeah, you look amazing! Or we can try something else. Would you prefer something ${choice("tee", "subtler")}? Or something really ${choice("slut_top", "slutty")}?`);
					}else{
						qcc(`Yeah! Although you might look even better in something a bit ${choice("slut_top", "sluttier")}!`);
					}
			}			
			
			txt(`After we bought the tops I needed, we searched for skirts. @qcc really enjoyed me posing for him, like a real-life dress-up doll.`);		
			
			if(mile.slut < bot_slut_min){
					kat(`This ${choice("skirt", "skirt")} is too short!`);
					txt(`I complained when he made me turn around.`);
					if(fren){
						qcc(`You can have the ${choice("longer", "other one")}. But I think it is perfect!`);
					}else{
						qcc(`Stop with that bashful whining, you can either have this on or even ${choice("shorter", "shorter")}.`);
					}
			}else if(mile.slut > bot_slut_max){
					kat(`This ${choice("skirt", "skirt")} is great. Or do you think I should go even shorter?`);
					txt(`I teasingly turned around to let him see it from all angles.`);
					qcc(`I would support that! Although with anything even ${choice("shorter", "shorter")} you will be flashing people your panties.`);
					//TODO NO PANTIES RULE
					if(!wears.panties){
						kat(`Ouch. I'm kinda not wearing any.`);
						qcc(`You what!?!`);
					}else if(wears.nicePanties){
						kat(`Don't worry, I'm wearing nice-looking @panties.`);			
					}
					
			}else{
					kat(`Is the ${choice("skirt", "skirt")} too short?`);
					txt(`${fren ? "I turned around to let him see it" : "He made me turn around to check it"} from all angles.`);
					 
					if(fren){
						qcc(`No! I think you can go even ${choice("shorter", "shorter")}! Although it is true that in anything shorter you would be flashing people your panties. But if you don't feel comfortable we can pick a ${choice("longer", "longer one")}.`);						
					}else{
						qcc(`No. And I want to see you try on even ${choice("shorter", "shorter")} one. I hope you have nice panties! `);
						if(!wears.panties){
							kat(`Ouch. I'm kinda not wearing any.`);
							qcc(`You what!?!`);
						}else if(wears.nicePanties){
							kat(`Of course I do!`);
						}
					}
			}

			link(`Lingerie section. `, 103);		
						
	{		
			
			const top = create("bimboTriangleTop");
			const slut_top = create("bimboSluttyTubeTop"); //TODO
			const tee = create("bimboNiceTee");
			
			//top
			effects([
				{
					id: "initial",
					fce(){
						remove("upper");				
					},
				},
				{
					id: "top",
					fce(){
						wear(top);
						updateDraw(PC);
						counter.top = 0;
					},
				},
				{
					id: "slut_top",
					fce(){
						wear(slut_top);
						updateDraw(PC); 
						counter.top = 1;
					},
				},
				{
					id: "tee",
					fce(){
						wear(tee);
						updateDraw(PC);
						counter.top = -1;
					},
				},
			]);
		
		
			const skirt = create("bimboBorderlineSkirt");
			const shorter = create("bimboNanoSkirt"); //TODO
			const longer = create("bimboMicroSkirt");
			
			//top
			effects([
				{
					id: "initial",
					fce(){
						remove("lower");	
						updateDraw(PC); 
					},
				},
				{
					id: "skirt",
					fce(){
						wear(skirt);
						updateDraw(PC); 
						counter.bot = 0;
					},
				},
				{
					id: "shorter",
					fce(){
						wear(shorter);
						updateDraw(PC); 
						counter.bot = 1;
					},
				},
				{
					id: "longer",
					fce(){
						wear(longer);
						updateDraw(PC); 
						counter.bot = -1;
					},
				},
			]);
	}					
			break;			
						
	case 103:
			showLess(1,1);
			txt(`I thought that was everything but he disappeared in the lingerie section and I was forced to follow him there. He keenly examined the female underwear.`);
			qcc(`What do you think, @katalt? They would look so cute on you!`);
			txt(`@qcc yelled at me across the whole room and raised a lacy pink  ${choice("thong", "thong")} .`);
			
			if(mile.slut < under_slut_min){
				txt(`I blushed and rushed to him, people in the store did not have to know what kind of underwear I was wearing.`);
				qcc(`And maybe a few g-strings?`);
				if(fren){
				kat(`And maybe some normal ${choice("panties", "panties")} ?`);
					qcc(`Oh, are ${choice("gString", "g-strings")}  too uncomfortable to wear? How do they feel? Is the string digging between your cheeks too distracting?`);
				}else{
					kat(`And maybe some normal panties?`);
					qcc(`Come on, ${choice("gString", "g-strings")} are normal panties for hot bimbos like you.`);
					txt(`He chuckled.`);
				}
			}else if(mile.slut > under_slut_max){
				kat(`Sure!`);
				txt(`I nodded and walked to him.`);
				qcc(`Maybe a few ${choice("gString", "g-strings")} ?`);
				if(ext.stats.commando > 6){ //TODO COMMANDO
					kat(`Well, I don't need so many panties, I'm usually fine without them.`);
					qcc(`Come on, everybody knows that a girl in sexy lingerie is hotter than a nude one! What about this?`);
				}else{
					kat(`Sounds great, as long as you're paying.`);
					qcc(`What about this?`);
				}
				txt(`He discovered a pair of ${choice("crotchless", "panties")}  with a hole in the crotch area.`);
				kat(`They don't seem like something for everyday wear.`);
				qcc(`I'm sure they will be useful for special occasions.`);
		
			}else{
				txt(`I rushed to him, people in the store did not have to know what kind ofunderwear I was wearing.`);
				kat(`Yeah, they look naughty.`);
				qcc(`What else? What about a few ${choice("gString", "g-strings")} ?`);
				txt(`He raised a garment reminding several strings tied together rather than a piece of clothing.`);
				if(fren){
					kat(`Sure, why not?`);
				}else{
					kat(`Sure, I gues.`);
					qcc(`What else?`);
					kat(`What about some normal ${choice("panties", "panties")}?`);
					qcc(`Come on, g-strings are normal panties for hot bimbos like you.`);
					txt(`He chuckled.`);
				}
			}	
			
			if(!mile.no_bra){
				kat(`And I need new ${choice("bra1", "bras")} bras!`);
				qcc(`Do you?`);
				kat(`Yeah, my new titties are pretty firm but I would still feel better with a bit more support.`);
				qcc(`Which ${choice("bra2", "bras")} would you like?`);			
			}
				
				
			link(`More. `, 104);		
		
				
		if(!mile.no_bra){	
			const bra1 = create("bimboCageBra");
			const bra2 = create("bimboPushUpBra"); //TODO
			
			//top
			effects([
				{
					id: "initial",
					fce(){
						remove("bra");	
						updateDraw(PC); 
					},
				},
				{
					id: "bra1",
					fce(){
						wear(bra1);
						updateDraw(PC); 
					},
				},
				{
					id: "bra2",
					fce(){
						wear(bra2);
						updateDraw(PC); 
					},
				},
			]);
		}	
		{	
			const gString = create("bimboCagePanties");
			const thong = create("bimboThong");
			const panties = create("bimboHipsters"); //TODO
			const crotchless = create("bimboCrotchlessThong");
			
			//top
			effects([
				{
					id: "initial",
					fce(){
						remove("panties");	
					//	updateDraw(PC); 
					},
				},
				{
					id: "gString",
					fce(){
						wear(gString);
					//	updateDraw(PC); 
						counter.panties = 0;
					},
				},
				{
					id: "crotchless",
					fce(){
						wear(crotchless);
						//updateDraw(PC); 
						counter.panties = 1;
					},
				},
				{
					id: "thong",
					fce(){
						wear(thong);
						//updateDraw(PC); 
						counter.panties = 0;
					},
				},
				{
					id: "panties",
					fce(){
						wear(panties);
					//	updateDraw(PC); 
						counter.bot = -1;
					},
				},
			]);
		}		
				
		
				
		break;
				
			//TODO BRA
	case 104:	
			showAll();
			txt(`At least I did not have to pose for him just in my underwear, even though I was sure his imagination was running wild. We grabbed snazzy ${choice("pantyhose")} and cliché hoop ${choice("earrings")}.`);
			
			
			 if(mile.collar){
				if(!fren && mile.sub > 12 && mile.slut > 6){
					qcc(`A submissive slut like you definitely needs a cute pink ${choice("collar")}!`);
				}else{
					qcc(`What about a cute new ${choice("collar")}?`);
				}
				kat(`Sure, why not.`);
			}else if(mile.choker){
				if(!fren && mile.sub > 12 && mile.slut > 6){
					qcc(`A submissive slut like you definitely needs a cute ${choice("choker")}!`);
				}else{
					qcc(`What about a cute new ${choice("choker")}?`);
				}
				kat(`Sure, why not.`);
			}

			txt(`Finally @qcc was satisfied and went to pay. He bought me more things than I was expecting.`);
			kat(`How much did you pay?`);
			qcc(`Don't worry, I have enough money.`);
			kat(`Honestly, I'm not very used to guys buying sexy clothes and lingerie for me.`);
			qcc(`Better get used to it, look at yourself, sugar daddies will fight over you!`);
			
			next(`Thanks! `, back);
			link(`Kiss him. `, 110);
			con_link(mile.slut > 0, `low slut`, `Complimentary blowjob.`, 111);
				
				
			{	
				const pantyhose = create("bimboOpenPantyhose");
				const earrings = create("bimboLoops"); //TODO
				const collar = create("bimboCollar"); //TODO
				const choker = create("bimboChoker"); //TODO
				
				effects([
					{
						id: "initial",
						fce(){
							remove("collar");	
						},
					},
					{
						id: "collar",
						fce(){
							wear(collar);
							updateDraw(PC); 
						},
					},
					{
						id: "choker",
						fce(){
							wear(choker);
							updateDraw(PC); 
						},
					},
				]);
				
				effects([
					{
						id: "initial",
						fce(){
							remove("socks");	
						},
					},
					{
						id: "pantyhose",
						fce(){
							wear(pantyhose);
							updateDraw(PC); 
						},
					},
				]);
				
				effects([
					{
						id: "initial",
						fce(){
							remove("earrings");	
						},
					},
					{
						id: "earrings",
						fce(){
							wear(earrings);
							updateDraw(PC); 
						},
					},
				]);
			}	
		break;
			
		
	case 110: 
			mile.c_friend++;
			mile.c_shopping = 2;
			emo('imp');
				
			txt(`I leaned closer and kissed him on his cheek.`);
			kat(`Thank you!`);
			txt(`@qcc blushed and seemed to be confounded:`);
			qcc(`You're welcome? ...I mean... you're a sexy girl... and you look even sexier in sexy clothes... it would be such a shame if you couldn't afford them... and I have enough money I don't need right now for anything useful anyway...`);
			next(`Thanks! `, back);
			break; 
				
	case 111:
				mile.c_bimbo++;
				mile.slut++;
				mile.c_shopping = 3;
				effigy.qcc.showLess(-1, 0);
				emo('imp');
				log_sex("bj", "qcc");
				
				txt(`I took his hand and dragged him back to the store.`);
				qcc(`Did you forget something?`);
				kat(`I have to properly reward you!`);
				qcc(`Reward me how?`);
				kat(`The only way a slutty bimbo like me knows, duh!`);
				if(mile.a_shopping === 2 || mile.b_shoppping_sub || mile.b_shopping_dom === 3){
					txt(`I followed the routine. It was a different shop and a different fitting room than the one I visited with ${mile.a_shopping === 2 ? "@qaa" : "@qbb"} but I knew what to do.`);
				}
				txt(`Nobody was in the fitting room at the moment ${mile.slut > 14 ? "(even thought that would not stop me)" : ""} and I pulled him into the last stall.`);
				qcc(`${NAME.kat[0]}... @kat? What are you doing?`);
				txt(`He stumbled, watching me squatting down and grabbing the button of his jeans.`);
				kat(`${mile.slut > 9 ? "I'm going to suck your dick. " : "Well, I think you deserve a reward, don't you? "}`);
				qcc(`Oh, @kat... you don't have to do this...`);
				if(fren){ 
					txt(`@qcc helplessly stumbled.`);
				}else{
					txt(`@qcc helplessly stumbled. It was a nice revenge after he tried to act tough and bossy the whole time we were shopping.`);
				}
				txt(`His cock was quite big and already hard; @qcc loudly groaned when I touched it with my warm tongue.`);
				qcc(`Oh my!`);
				kat(`Tone your voice down! ${mile.slut > 10 ? "...or don't if you want other shoppers to hear you are having a good time! " : ""}`);
				qcc(`Oh fuck!`);
				txt(`I giggled and took the tip of his thick dick in my mouth while stroking the rest of the length with my fingers. I struggled and gagged when I tried to shove it deeper into my throat and engulf it completely.`);
				txt(`@qcc gently pushed my hair away so he could see my face, he was watching my reflection in the larger mirror on the wall.`);
				qcc(`Oh yes! I'm clo..ose!`);
				txt(`His fingers reflexively squeezed my shoulder, I pumped with his pulsating cock and let his cum land on my stuck out tongue (and some on my upper lip and nose).`);
				txt(`When we left the stall, we met two women who probably guessed what we were doing. Blushing @qcc was hiding behind me, I ${mile.slut < 7 ? "anxiously again wiped my mouth to make sure there were no remains of cum. " : "mischievously emphasised wiping my mouth, like I was making sure there was no cum left. "}`);
			next(`Go home. `, back);
			break;
		}
}
			
			
			
			
			
			
			
			
			
			
			
			
			
			

export const chair_c_help = (index)=> {
	switch(index){
		default:
		case 101: 
			emo(`sad`);
			placetime(`@qcc's place`);
			
			if(mile.c_friend > 2){
				txt(`We were playing videogames at @qcc's place. But my heart really was not in it and @qcc noticed.`);
			}else{
				qcc(`... I think transparent materials are really cool. You are technically dressed but still showing everything...`);
				txt(`I was hanging out at @qcc's place. But I was not listening to his lewd fashion rambling, I zoned out and thought about @eva and @ven.`);
			}
			qcc(`....what is wrong with you today?`);
			kat(`My life was destroyed by three perverted nerds.`);
			qcc(`You seem even more sullen than usual.`);
			txt(`I shrugged but then I reluctantly confessed:`);
			kat(`I'm worried about about class election.`);
			qcc(`Oh yeah. @ven bothered me that I should vote for her rather than for one of the pretentious, popular bitches. She was very convincing.`);
			kat(`Are you going to vote for her?`);
			qcc(`What? Of course not! I'm going to vote for you!`);
			kat(`...thanks.`);
			txt(`I dejectedly nodded.`);
			kat(`You and me, that's two votes. At least something, getting only one would be so humiliating.`);
			qcc(`Don't be so gloomy! I can try to get you some votes.`);
			if(mile.tom_help){
				kat(`I already asked @tom to help me. Although I'm not sure if I can trust him. He talks a lot but actual results...`);
				qcc(`That's good. @tom's a jock and he can approach a different target group than me.`);
			}
			
			if(mile.tom_help > 2){
				const deal = (()=>{
					if(mile.tom_help === 3) return "to suck his dick";
					if(mile.tom_help === 5) return "perverted kinky sex";
					if(mile.tom_help === 6) return "anal sex";
					return "to fuck him";
				})();
				
				const deal2 = (()=>{
					switch(mile.tom_help){
						default:
						case 3: return `I will give you a blowjob`;
						case 4: return `I will fuck you`;
						case 5: return `I will have kinky sex with you`;
						case 6: return `I will have anal sex with you`;
					}
				})();
				
				kat(`Thanks! What do you want for your help?`);
				qcc(`For my help?`);

				if(mile.c_friend > 2){ //TODO MORE
					mile.chair_c_help = 20; 
					kat(`Yeah, I promised @tom ${deal}.`);
					qcc(`What?!?`);
					txt(`I could witness his brief mental struggle. It seemed he would not mind the same reward.`);
					qcc(`I don't want anything, @kat! You're my friend and I'm going to help you because I want you to be happy.`);
					kat(`Oh...`);
					txt(`I was moved and not sure what to say.`);
				}else{
					mile.chair_c_help = mile.tom_help; 
					kat(`Yeah, I promised @tom ${deal}.`);
					if(mile.slut < 15){
						qcc(`You would really do that just for votes?`);
						kat(`You shouldn't be surprised, you're constantly telling me how huge shameless slut I am.`);
					}else{
						qcc(`Whoa.`);
						kat(`Yeah, I'm a whore.`);
					}
					qcc(`I wouldn't ask you for that.`);
					//kat(`That's fine. ${deal2}, it's going to be fair. But only if I get reelected! I hope that makes you properly motivated!`);
					kat(`That's fine. I can reward you sexually too. But only if I get reelected! I hope that makes you properly motivated!`);
				}

			}else{
				mile.chair_c_help = 10; 
				kat(`Thank you.`);
				qcc(`And what is going to be my reward for my help?`);
				kat(`You don't have to worry, you'll get a royal reward. But only if I get reelected! I hope that makes you properly motivated!`);
			}
			next();
		break;
	}
}
