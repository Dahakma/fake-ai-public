import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {ada, txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";
import {placetime} from "Loop/text_extra";
import {rollUpSkirt, checkQuality, emo, draw, drawZoomed, drawUnzoomed, effigy, INVENTORY, sluttiness, makeup, wearRandomClothes, remove, wear, create, wears, showLess, showAll, quickSaveOutfit,  quickTempOutfit, quickLoadOutfit} from "Avatar/index";
import {remove_today, add_today} from "Taskmaster/scheduler";
import {toHSLA, nameColor, parseHSLA} from "Libraries/color/index";
import {spoiler, bills, log_sex} from "Virtual/text";
import {money} from "Data/items/prices.js";
import {masochist, buttplug} from "Virtual/text";
/*
stuff related to cosmetic surgeries in story/other/surgery.js
stuff related to tatto in story/tasks/tattoo.js
video_sas in story/tasks/week_4.js
*/

export const girls_weekend_confront = (index)=> {
//TODO
	placetime("classroom");
	txt(`The rest of the Monday was not especially great. @mol (trying to prove once again he is the worst teacher at our school) decided that the best way to start the first Monday class is a surprise test. I spectacularly failed. I blamed the nerds - I had no time to study because of them. But the truth was I would have probably procrastinated anyway. ${wears.miniSkirt ? "Plus the way @mol inconspicuously ogled my bare legs during the whole time we were writing really did not help me to focus on the German grammar. " : ""} `);
	txt(`During the break I confronted @eva and @sas about the weekend incident. `);
	kat(`Thank you, guys! You helped me a lot!`);
	sas(`We wanted @kat! But...`);
	eva(`It seemed you can handle it. I mean, how hard it could be to convince @pet to remove the bot? `);
	sas(`I'm really sorry, I had no idea @pet would let it go so far! `);
	eva(`I can't even imagine how horrible and humiliating it had to be finding those three bastards recreated virtual you for their own perverted pleasure! `);
	txt(`It seemed she imagined that a lot. I just smiled and handwaved it. `);
	kat(`Lol, not really. I didn't even think about it this way. I mean, putting my face on some doll is something more humiliating for them than for me! `);
	sas(`But what if they take the bot on another party? `);
	kat(`They won't! I'll make sure of it! `);
	txt(`I sharply retorted. `); /* even though I was not so sure. */
	next();
}


export const girls_music_video = (index)=> {
	switch(index){
		default:
		case 101:
				placetime("schoolyard");
				emo("focus");
				txt(`During the break I was chatting with ${mile.sas > mile.eva ? "@sas and @eva" : "@eva and @sas"}. We were sitting on desks in the half-empty classroom, snacking and discussing the latest gossip.`);
				eva(`Have you seen the new Ymaneesha's video?`);
				kat(`No. I saw it in my feed but I haven't time to watch it yet. Is it good? I saw there's some kind of drama around it?`);
				sas(`Yeah, people are saying it was more porn than a music video.`);
				kat(`Was it!?!`);
				sas(`Well... I'm not sure...`);
				eva(`You've never seen porn before and have no comparison?`);
				txt(`@sas blushed when @eva teased her.`);
				sas(`I did! But... I'm not sure how to compare those things. I mean... there was no penetration...`);
				eva(`Most of the time she's nude or in lingerie, twerks and crawls over guys.`);
				sas(`And girls.`);
				eva(`Yeah. And girls. The camera really doesn't show anything but you can see her pierced nipples several times and in one shot you can even allegedly spot her pussy.`);
				kat(`She tries to get over her child star image pretty hard, isn't she? But the drama seems overblown.`);
				sas(`Yeah, it isn't half as bad as the stuff she's doing on stage.`);
				eva(`What a slut.`);
					
				link(`Slander Ymaneesha. `, 103);
				link(`Defend Ymaneesha. `, 102);
				break;
				
		case 102: 
				mile.slut++;
				mile.girls_music_video = 1;
				emo("anoy");
				kat(`So what? Being slut isn't a crime.`);
				txt(`I believed they were unfairly harsh on my favorite singer.`);
				eva(`You're defending her?`);
				kat(`She knows what she's doing. Many people wouldn't even know about her new music video without the clickbait articles.`);
				eva(`I'm not saying it won't get her the attention. I'm just saying it's utterly pathetic if you have no other way to get the attention than to flesh your tits or act like a total whore.`);
				kat(`Blame media and horny guys, not Ymaneesha for exploiting them.`);
				sas(`Sex sells.`);
				txt(`Shrugged @sas swayed to my side.`);
				eva(`Would you do the same if you were her?`);
				if(mile.slut > 4){ //probably first week whne slutiness is low
					kat(`Absolutely I would. For Ymaneesha's fame and wealth I would gladly act a bit indecently and stare in scandalous music videos.`);
					eva(`That's just being a whore with a few extra steps!`);
					kat(`Don't be so dramatic!`);
				}else if(mile.slut > 1){
					kat(`Maybe? I wouldn't be nude in my music videos but if you want to be a famous singer, you have to be a bit sexy.`);
					sas(`Or a lot sexy.`);
				}else{
					kat(`No! I mean... I personally wouldn't but I don't judge her.`);
					txt(`The very idea of millions of horny men objectifying me and rewinding the video to see all the juicy details was horrifying. But if Ymaneesha had more courage and less shame than me, good for her.`);
				}
				eva(`I'm just saying a girl should have her dignity. Men don't respect whores. Strive for something more than to be known from a music video where you accidentally show your pussy!`);
				kat(`I doubt it was an accident.`);
				sas(`You mean that she did it on purpose!?!`);
				txt(`Our argument was interrupted by the bell.`);

				next(`Classes. `);
				break;
				
		case 103: 
				mile.slut--;
				mile.girls_music_video = -1;
				emo("smug");
				kat(`Yeah, exactly! She's desperate! The last album was total garbage -`);
				sas(`Except for <i>Dark Obsession</i>.`);
				txt(`Feebly protested @sas. A few years ago she used to be the biggest Ymaneesha's fan.`);
				kat(`- yeah, of course, <i>Dark Obsession</i> is great. The last album was mostly grabage except for <i>Dark Obsession</i> -`);
				sas(`And for <i>Take Me Down</i>.`);
				kat(`- the last album was total grabage with sole exception of <i>Dark Obsession</i>. And now she desperatelly panders for media attention with her sexual shenanigans!`);
				eva(`You're utterly pathetic if you have to flesh your tits or act like a total whore to attract men's attention!`);
				kat(`Yeah, I would never do that! You have to have your dignity. Men don't respect whores.`);
				sas(`Eww, can you imagine millions of horny guys watching a video of you accidentally exposing your pussy?`);
				kat(`I bet it wasn't an accident but that she did it on purpose.`);
				eva(`Nowadays is everything just about sex! Where is the art?`);
				txt(`@eva complained and we concurred. We were very cultured.`);

				next(`Classes. `);
				break;
	}
}



export const girls_movie = (index) => {
	switch(index){
		default:
		case 101:
			set.irl();
			emo("happy");
			placetime("@eva's home");
			txt(`For once I decided to ignore the ${mile.immersion < 1 ? "stupid" : ""} game and instead spend the evening with my friends. We went to @eva's place so we could exploit her stepfather's home cinema to watch movies. <i>Rulers of Fear</i> was an action vampire flick with pretty good action scenes - even though maybe a bit too violent and graphic for my liking - and a rather sweet romantic side plot. @eva liked scary movies and vampires, however, she wanted to watch the movie mostly for Thomas Dutch who played the main role. She had a total crush on him and I understood her. He was cute and well-built and had a wagonload of charisma.`);
			eva(`I wish I could spend eternity with him!`);
			txt(`@eva daydreamed during the ending credits.`);
			kat(`You would soon get bored!`);
			if(mile.slut > 7){
				eva(`Not every one of us is a slut unable to commit to one guy!`);
				txt(`@eva attacked me because I was ruining her fantasy.`);
			}else{
				eva(`I would not!`);
				txt(`@eva scowled because I was ruining her fantasy.`);
			}
			sas(`You would stay with him even if that would mean becoming an undead monster?`);
			eva(`Eternally young, unearthly beautiful and superhumanly strong and fast undead monster? Damn yeah! The medieval scenes were shot in Hungary and Romania. I wish he was shooting something here, I would love to meet him personally!`);

			link(`I'm sure he would fall for you!`, 102);
			link(`He wouldn't even notice you!`, 103);
			break;
	
		case 102: 
			emo("imp");
			mile.girls_movie = 1;
			mile.eva_nice++;
			eva(`Yeah, there are no doubts about that. We would date for three months and then he would propose to me under a waterfall. We would get married in Fiji and you two would be invited.`);
			sas(`Thanks! Maybe you could introduce me to Timothée Chalamet?`);
			eva(`Of course, I'm sure my Tommy knows him!`);
			kat(`Isn't he married?`);
			eva(`I'm sure he wouldn't mind cheating with @sas.`);
			sas(`I don't want to be his mistress, I want him whole for myself.`);
			eva(`I'm sure he would leave his wife for @sas!`);
			sas(`Awesome!`);
			kat(`It isn't awesome. You should feel bad for ruining his marriage. Don't they even have kids?`);
			eva(`Probably several. He'll miss them very much. But he deserves it for leaving his wife for a young, gold-digging whore.`);
			sas(`Hey!`);

			link(`Don't worry, I'm sure he would be allowed to see his kids on weekends. `, 110);
			link(`You're the worst, @sas. You should feel ashamed for being such a homewrecker. `, 110, ()=> mile.sas_exploit++);
			break;

		
		case 103: 
			emo("imp");
			mile.girls_movie = 2;
			mile.eva_exploit++;
			kat(`He's sleeping with the hottest Holywood actresses, do you really think he would be interested in you?`);
			txt(`I mercilessly tore apart her fantasy.`);
			eva(`Sure! Why not?! Maybe he's looking for somebody who isn't a vapid Holywood bimbo?`);
			kat(`Come on, you know he wouldn't be interested in crazy fangirls. And even if he did, he's probably a huge sexual pervert who would just exploit you and degrade you in the worst ways possible and then just threw you out in the morning without even learning your name.`);
			eva(`Shut up! He's not a pervert! He's kind and sweet and we would gently make love for hours and he would never forget about me!`);
			kat(`If you want to naively believe that.`);
			eva(`I want and I will!`);
			sas(`I think it's lovely to have such nice fantasies even if they might be unrealistic and never happen.`);
			kat(`Yeah? What is your unrealistic sexual fantasy?`);
			txt(`@sas's eyes anxiously evaded mine.`);
			sas(`I don't have any unrealistic sexual fantasies.`);

			link(`Fine, keep your twisted secrets. `, 110, ()=> counter.secret = false)
			link(`You'll tell us now! `, 110, ()=> counter.secret = true)
			break;


		case 110:
			if(counter.secret){
				mile.sas_exploit++;
				kat(`Come on! You'll tell us!`);
				sas(`I don't have any unrealistic sexual fantasies.`);
				txt(`I and @eva tried everything, convince her or threaten her, but @sas was like a partisan.`);
			}
			sas(`Can't we talk about something else, please?`);
			kat(`Fine. About what?`);
			sas(`I was thinking about getting a frog...`);
			kat(`Don't you already have a cat?`);
			sas(`That's the issue. I'm not sure if cats and frogs can live together?`);
			eva(`Don't you already have too many frogs?`);
			sas(`But I don't have any real, living one.`);
			kat(`Seriously, @sas, you have already too many frogs. Any guy who will enter your bedroom to have sex with you will think you're a crazy frog lady.`);
			eva(`Good luck there are not too many of them.`);
			txt(`Giggled mischievous @eva and @sas frowned:`);
			sas(`There are plenty of guys willing to enter my bedroom to have sex with me! We chatted with @dav and he asked me for a date!`);
			eva(`Good for you. I have a date too.`);
			kat(`Really?`);
			eva(`I didn't want to tell you to jinx it but I'm going out with @ada. `);
			kat(`With @ada!?! You lucky bitch!`);
			txt(`I was flirting with @ada but not too seriously, he seemed to be annoyingly resistant to my charm and it was not my style to chase guys. But somehow him asking out @eva made me immediately, irrationally want him. `);
			sas(`That sounds awesome! `);
			txt(`We spend the rest of the evening enviously discussing @eva's love life.`);
			/*
			kat(`Really? Your new project is going on well?`);
			eva(`Beyond expectations!`);
			
			*/
			next();
			break;
	}
}







//GIRLS SHOPPING
//contition - no_bra
export const girls_shopping = (index) => {
	switch(index){
		default:
		case 101:
			emo("relax");
			set.meta();
			
			if(!mile.girls_shopping){
				counter.temp = 0;
				set.meta();
				placetime("virtual shopping mall");
				mile.girls_shopping = true;
				mile.sas++;
				mile.eva++;
				txt(`I decided to for once spend time with my friends. I invited @eva and @sas and we went shopping to a virtual mall which offered cathedral-like hall filled with endless shelves of various garments we could try on in privacy and eventually ordered to be delivered. `);
			}
			
			if(counter.temp === 2){
				//draw(effigy.sas);
				present(["sas",["cageBra","gString","socks"]]);
				effigy.sas.draw();
				txt(`We were checking various sexy lingerie, having fun, looking for something nice which would suit us but was not ridiculously expensive. `);
				txt(`@sas tried on an especialy erotic pair and exitedly looked at me. `);
				sas(`What do you think? Too much? `);
				txt(`She anxiously asked for my opinion. `);
				links_mix(
					[`You look hot! `, 121],
					[`You look horrible! `, 122],
				);
				counter.temp = 0;
				return;
			}else if(counter.temp === 3){
				//draw(effigy.eva);
				present(["eva",["jacket","tee","jeans","pushUpBra","socks"]]);
				effigy.eva.draw();
				txt(`@eva finished picking an outfit. She turned to me and @sas, expecting our praise. `);
				eva(`How do I look? `);
				links_mix(
					[`Absolutely awesome! `, 131],
					[`Absolutely awful! `, 132],
				);
				counter.temp = 0;
				return;
			}else{
			//TODO
				present(["sas","female_generic"],["eva","female_generic"]);
				//present.original();
			}
			
			link(`Lingerie. `, 101, ()=>{}, ()=>{
				INVENTORY.shop("vr_shop_lingerie", ()=> {
					if(!mile.sas_girls_shopping){
						counter.temp = 2;
						main(101);
					}
				});
			});
			link(`Tops. `, 101, ()=>{}, ()=>{
				INVENTORY.shop("vr_shop_upper", ()=> {
					if(!mile.eva_girls_shopping){
						counter.temp = 3;
						main(101);
					}
				});
			});
			link(`Bottoms. `, 101, ()=>{}, ()=>{
				INVENTORY.shop("vr_shop_lower");
			});
			/*
			link(`Accessory. `, 101, ()=>{}, ()=>{
				INVENTORY.shop(["accessory"], undefined, 2);
			});
			*/
			next(`Leave. `);
			break;
		
		case 121:
			mile.sas_sassy++;
			mile.sas_girls_shopping = 1;
			emo("happy");
			sas(`Really? `);
			txt(`@sas gleamed, pleased I approved her choice.`);
			kat(`Damn yeah! If I was into girls, I wouldn't be able to hold back! You shouldn't be afraid to be a little bit sexier! `);
			sas(`You think so? `);
			kat(`Of course! You're pretty! You don't have to be bashful, you should be more confident!`);
			sas(`Thank you, @kat!`);
			link(`You're welcome! `, 101);
			break; 
			
		case 122:
			mile.sas_exploit++;
			mile.sas_girls_shopping = 2;
			emo("smug");
			sas(`Oh! Really? `);
			txt(`Disappointed @sas sighed when I rejected her choice. `);
			kat(`Yeah! You look absolutely atrocious! `);
			sas(`I don't know what I was thinking.`); 
			kat(`Don't worry, we can fix that! `);
			txt(`I helped her to pick a pair which I liked more.`);
			kat(`Now you look decent! What would you do without me? `);
			sas(`Thank you, @kat! `);
			link(`You're welcome! `, 101);
			break;
			
		case 131:
			mile.eva_nice++;
			mile.eva_girls_shopping = 1;
			sas(`You look gorgeous! `);
			txt(`@sas quickly joined to my praise. `);
			eva(`Thanks, girls! I <i>do</i> look gorgeous! At least one of us has a good taste!`);
			txt(`Smirked @eva, delighted watching her image in the large mirror. `);
			kat(`Hey! I praised you one time any you're already getting cocky? `);
			eva(`Damn yeah! `);
			txt(`Giggled @eva. `);
			link(`Continue shopping `, 101);
			break;
		
		case 132:
			mile.stat++;
			mile.eva_exploit++;
			mile.eva_girls_shopping = 2;
			mile.eva_no_bra = true;
			mile.sas_no_bra = true;
			emo("focus");
			eva(`What!?!`);
			txt(`Gasped offended @eva. `);
			sas(`Yeah, it doesn't feel right.`);
			txt(`@sas supported me. `);
			eva(`What don't you like?!? `);
			kat(`Hmmm... dunno, it's a bit boring... and prudish...`);
			eva(`Prudish?!?`);
			kat(`Yeah, you should maybe try a little bit sexier top. `);
			eva(`I can try that...`);
			kat(`Maybe lose the bra. `);
			eva(`What? `);
			kat(`Try it! It's way more comfortable to go without a bra! Or are you too shy? `);
			eva(`I'm not shy! 	`);
			txt(`Glorious. I managed to convince @eva to stop wearing bras too. Now I was not the only girl who stood out, now I was the trendsetter. `);

			link(`Continue shopping `, 101);
			break;
	}
};



//REACTION TO QAA DATE
export const a_date_react = (index) => {
	switch(index){
		default:
		case 101:
			placetime("canteen");
			txt(`During lunch, the girls immediately shifted the conversation to me and @qaa.`);
			eva(`So?! How was your date? `);
			kat(`Fine I guess. `);
			eva(`Come on, @katalt, don't be so mysterious! We wanted to hear more about you and your new suitor!`);
			sas(`Yeah, we're curious. Is @qaa your new boyfriend?!`);
			
			link(`Yes. `, 113);
			link(`No! `, 111);
			link(`Maybe. `, 112);
			break;
			
		case 111:
			mile.stat++;
			mile.a_date_react = 1;
			mile.a_public = 2;
			emo("imp");
			kat(`OMG! He isn't my boyfriend! I took him on one date out of pity!`);
			eva(`I had no idea you're so self-sacrificing. `);
			kat(`I'm sure there are many other things you know nothing about. I just felt obliged after he helped me so much with school stuff. So when he asked I said yes. That's it. `);
			sas(`I think what you did was very nice and selfless. `);
			kat(`Thanks. But you don't have to call me hero, the real heroes are hunk firemen saving kittens from trees. `);
			next();
			break;
				
		case 112:
			mile.sub++;
			mile.stat--;
			mile.a_date_react = 2;
			mile.a_public = 3;
			kat(`It was just one date! He asked me out and agreed because we used to be friends.`);
			txt(`I fumbled my excuse. `);
			eva(`And? Will there be more dates? `);
			kat(`Probably no. I don't know. But what would be wrong with that?!? `);
			eva(`Of course nothing! You seem like a cute pair! `);
			txt(`That bitch @eva was mocking me and @qaa! `);
			sas(`Isn't he a bit dorky for you? He's way below your league! `);
			if(mile.sub < 5){
				emo("angry");
				kat(`So what? I'm not marrying him! `);
				txt(`I flared up and @sas quckly backed out: `);
				sas(`Sorry, @kat! `);
				eva(`Calm down! We're not trying to offend you, we're just curious! `);
			}else{
				emo("shy");
				kat(`Well... I guess... `);
				txt(`I shrugged. `);
				sas(`But of course, if you love him, follow your heart! `);
				txt(`Advised romantic @sas.`);
			}
			kat(`But maybe you could keept quiet about this? At least until I figure it out?  `);
			eva(`Don't worry, @kat, we're not scandalmongers! `);
			txt(`Smiled @eva.`);
			next();
			break;
			
		case 113:
			mile.sub--;
			mile.stat--;
			mile.a_date_react = 3;
			mile.a_public = 3;
			mile.a_girlfriend = true;
			emo("focus");
			
			kat(`Yes. `);
			txt(`I claimed. This had to be adressed head on. `);
			sas(`Oh! `);
			txt(`Gasped bewildered @sas. `);
			eva(`How did it happen? `);
			kat(`Well, he asked me out and I agreed and we had fun. Is the idea of me dating him bothering you? `);
			eva(`What?! Of course not. You seem like a cute pair!`);
			txt(`What a bitch! `)
			sas(`He's a bit below your league, don't you think? I think you would deserve somebody better...`);
			kat(`He isn't! @qaa is a great guy! Stop underestimating him! `);
			txt(`I flared up and @sas quckly backed out: `);
			sas(`Sorry, @kat! `);
			eva(`Calm down! We're not trying to offend you, we're just curious! `);
			next();
			break;
			
	}
};



//PIERCING
export const girls_piercings = (index)=> {
	switch(index){
		default:
		case 101:
			emo("anoy");
			placetime("classroom");
			if(wears.visiblePiercedNipples){
				eva(`I see you want everybody to know about your brand new piercings!`);
				txt(`@eva snickered, staring at my chest. The thin fabric of my @upper was not able to hide the outline of my nipples nor my new nipple piercings. ${ (mile.slut < 5 && ext.rules.no_bra) ? "Fucking nerds. I did not realize they were so obvious when I was wearing no bra. " : ""}`);
				if(mile.sub > 8){
					kat(`Oh! Well... yeah...`);
					eva(`I admire your resolve to try new things but I believe you went too far this time. They look a bit too trashy. ${wears.heavilyPiercedFace ? "Although they are still not as bad at those new face piercings of yours. " : ""}`);
				}else{
					kat(`Yeah. Pretty cool, aren't they?`);
					eva(`Eh, nah. A bit too trashy for my taste. ${wears.heavilyPiercedFace ? "Although they are still not as bad at those new face piercings of yours. " : ""}`);
				}	
			}else{
				eva(`<i>Interesting new piercings! </i>`);
				txt(`@eva snickered, staring at my face.`);
				if(mile.sub > 8){
					kat(`Oh! Well... yeah...`);
					eva(`I admire your resolve to try new things but I believe you went too far this time. They look a bit too trashy.`);
				}else{
					kat(`Yeah. Pretty cool, aren't they?`);
					eva(`Eh, nah. A bit too trashy for my taste.`);
				}
			}

			if(mile.sub > 10){
				kat(`I don't think they're trashy...`);
				txt(`I feebly tried to resist @eva's merciless critique.`);
			}else if(mile.sub > 5){
				kat(`I don't think they're trashy!`);
				txt(`I frowned and reisted to @eva's mean critigue.`);
			}else if(mile.sub < 0){
				kat(`Nan, they're not trashy!`);
				txt(`I confidently handwaved @eva's critique.`);
			}else{
				kat(`They're definitely not trashly!`);
				txt(`I frowned when @eva meanly critiqued me.`);
			}

			eva(`I think they are. What do you think? @sas?`);
			sas(`Me?`);
			txt(`@sas was puzzled for a moment, not sure whether to agree with me or @eva.`);
			sas(`Well, I think piercing could be stylish. I kinda wanted to get a piercing but then I decided not to because I don't like getting pierced.`);
			eva(`I don't say all piercings are trashy, I have one too, just @kat's one.`);
			kat(`Thanks.`);
			sas(`What does constitute trashy?`);
			kat(`That's a good question!`);
			eva(`Well, moderation. ${wears.visiblePiercedNipples ? "I guess there's nothing wrong with nipple bars, but you don't have to display them to everybody. Or like o" : "O"}ne or two face piercing looks good but more ${wears.heavilyPiercedFace ? "- like @kat is wearing -" : "than that"} looks trashy. Well, except for snake bites, those look trashy every single time.`);
			sas(`In that case, you might be right. @kat's are definitely trashy.`);
			eva(`See, @kat? Even @sas concur.`);

			link(`I guess you're right. `, 102);
			link(`You're stupid and wrong! `, 110);
			link(`But @sas is afraid of needles! `, 120);
			break;
			
	case 102:
			mile.sub++;
			mile.stat--;
			emo("help");
			remove_today("girls_piercing_action");
			
			kat(`I guess you're right. I do look a bit trashy.`);
			txt(`I was forced to agree with them. It felt easier than starting an argument and they were not completely wrong.`);
			sas(`Don't worry, we like you anyway.`);
			eva(`Well, I see a few more places you could get pierced too!`);
			txt(`The girls continued making fun of me. Their mocking was mostly playful but I still did not like it. And other people in the half-empty classroom could hear their mean jokes.`);
			
			next();
			break;

	case 110: 
			mile.eva_exploit++;
			mile.sub--;
			emo("angry");
			
			kat(`You're stupid and wrong!`);
			eva(`You don't have to be rude, I was just trying to give you honest feedback.`);
			txt(`Giggled @eva.`);
			kat(`There's nothing trashy about any amount of piercings! Believing that makes you sound like a bland, small-minded person.`);
			txt(`@eva stopped laughing.`);
			eva(`I said I'm not against piercings in general!`);
			kat(`You're not confident to get piercings so you're just criticizing people who do!`);
			eva(`I'm not a coward who's afraid of needles, like @sas! (Sorry, @sas)`);
			sas(`Hey!`);
			kat(`Prove it, chicken!`);
			eva(`How?`);
			
			con_link(wears.piercedNipples, `no nipple piercings`, `Get your nipples pierced! `, 111, ()=> mile.girls_piercings = 5); 
			link(`Get a face piercings !`, 111, ()=> mile.girls_piercings = 1);
			break;
			
	case 111:
			emo("smug");
			
			eva(`...`);
			txt(`@eva considered my challenge.`);
			kat(`...chicken.`);
			eva(`Fine! I'll do it! Not because of your stupid challenge but because I think it might be a good, fun idea. `);
			sas(`I think you'll look great with the piercing, @eva!`);
			txt(`Nodded excited @sas.`);
			
			next(`We'll see! `);
			break;
			
	case 120: 
			emo("imp");
			mile.girls_piercings = 10;
			mile.sas_exploit++;
			
			kat(`I think we should focus on the most important issue: @sas's phobia of needles!`);
			txt(`I tried to change the topic of our conversation. @sas wanted to get her navel pierced but eventually cowardly backed out because she did not like the <i>get pierced</i> part.`);
			sas(`What?!? I don't have a phobia of needles!`);
			eva(`Prove it and stab yourself with a pin!`);
			txt(`@eva mischievously giggled.`);
			sas(`Are you insane?! Not liking being needlessly stabbed by sharp, pointy, metal objects is common sense, not phobia!`);
			kat(`Getting piercing isn't needless, you have to suffer for beauty.`);
			sas(`That's a sick belief!`);
			eva(`Yeah. I think we should help @sasalt to fulfill her dreams, shouldn't we?`);
			kat(`It's our civic duty. You're getting your first piercing!`);
			sas(`Come on, girls! This isn't funny!`);
			
			next();
			break;
	}
}

export const girls_piercings_action = (index)=> {
	switch(index){
		default:
		case 101:
			set.irl();
			placetime("piercing parlor");
			emo("imp");
			
			txt(`So in the afternoon, I met with the girls and we went to the piercing parlor.`);
			if(mile.girls_piercings < 10){
				sas(`Do I have to be here?`);
				txt(`@sas felt a bit uneasy.`);
				eva(`Yes. You have to provide moral support.`);
				sas(`But I'm not going to watch!`);
				kat(`You don't have to! ...unless @eva is going to back out!`);
				eva(`I'm not going to back out!`);
				txt(`I had to needle her constantly to make sure she will not back out. But she was determined to show me. After a short discussion with the guy running the place she was about to go through the procedure I was already familiar with.`);
				if(mile.girls_piercings < 5){
					drawZoomed("face", effigy.eva);
					
					kat(`So what piercing did you chose?`);
					eva(`Well, I was deciding between ${choice("nose")}, ${choice("eyebrow")} and ${choice("lip")}.`);
					kat(`All good choices.`);
				}else{
					effigy.eva.showLess(0, -1);
					
					txt(`@sas hid her eyes, @eva frowned, and soon she was owner of brand new nipple ${choice("bars")} / ${choice("rings")}`);					
				}
				link(`Piercing. `, 110);
			}else{
				sas(`Come on, girls! This isn't funny!`);
				eva(`It's hilarious!`);
				kat(`It isn't as painful as I claimed.`);
				eva(`It is much worse!`);
				kat(`Come on, @eva, don't scare her, you don't want to make her cry!`);
				sas(`I'm not going to cry!`);
				kat(`You'll look so cute with a stud in your nose or lip!`);
				if(mile.sas_boobs > 0){
					eva(`Or maybe she should get her nipples pierced. You know, to get them to improve even more!`);
				}else{
					eva(`Or maybe she should get her nipples pierced. You know, to make her titties noteworthy at least somehow.`);
				}
				link(`Face. `, 120);
				link(`Nipples. `, 121);
			}
			
			
			if(mile.girls_piercings < 5){
				effects([
					{
						id: "initial",
						fce(){
							remove("eyebrow", effigy.eva);
							remove("noseSide", effigy.eva);
							remove("lipsTop", effigy.eva);
						},
					},
					{
						id: "eyebrow",
						fce(){
							mile.girls_piercings = 2;
							const item = create("eyebrowPiercingLeft");
							wear(item, effigy.eva);
						},
					},
					{
						id: "nose",
						fce(){
							mile.girls_piercings = 3;
							const item = create("nosePiercing");
							wear(item, effigy.eva);
						},
					},
					{
						id: "lip",
						fce(){
							mile.girls_piercings = 4;
							const item = create("medusa");
							wear(item, effigy.eva);
						},
					},
				])
			}else if(mile.girls_piercings < 10){
				effects([
					{
						id: "initial",
						fce(){
							remove("nipples", effigy.eva);
						},
					},
					{
						id: "bars",
						fce(){
							mile.girls_piercings = 6;
							const item = create("nippleBars");
							wear(item, effigy.eva);
						},
					},
					{
						id: "rings",
						fce(){
							mile.girls_piercings = 7;
							const item = create("nippleRings");
							wear(item, effigy.eva);
						},
					},
				])
			}
			break;



	case 110: 
			drawUnzoomed(PC);
			
			sas(`Is it over? Can I look again?`);
			eva(`Yeah!`);
			kat(`You took it worse than @eva!`);
			eva(`How do I look!`);
			txt(`@eva was checking herself in a mirror.`);
			sas(`You look great!`);
			
			next(`You look awesome!`, ()=> mile.eva_nice++);
			next(`Let's say very slightly better than before. `);
			next(`Extremely trashy!`, ()=> mile.eva_exploit++);
			break;

	case 120:
			drawZoomed("face", effigy.sas);
			
			sas(`No, not the nipples!`);
			txt(`@sas resisted.`);
			kat(`Fine. What else would you like? Getting pierced your ${choice("septum")}, ${choice("eyebrow")} or ${choice("lip")}?`);
			eva(`Or maybe ${choice("tongue")} piercing?`);
			sas(`I can't decide.`);
			kat(`I mean, if you can't decide, you can get all of them.`);
			sas(`I think one would be enough... I think I'll go with...`);
			link(`Piercing. `, 122);
			
			effects([
				{
					id: "initial",
					fce(){
						remove("eyebrow", effigy.sas);
						remove("noseBot", effigy.sas);
						remove("lipsSide", effigy.sas);
					},
				},
				{
					id: "eyebrow",
					fce(){
						mile.girls_piercings = 12;
						const item = create("eyebrowPiercingLeft");
						wear(item, effigy.sas);
					},
				},
				{
					id: "septum",
					fce(){
						mile.girls_piercings = 13;
						const item = create("noseRing");
						wear(item, effigy.sas);
					},
				},
				{
					id: "lip",
					fce(){
						mile.girls_piercings = 14;
						const item = create("monroeRight");
						wear(item, effigy.sas);
					},
				},
				{
					id: "tongue",
					fce(){
						mile.girls_piercings = 15;
					},
				},
			])
			break;
	
	case 121:
			effigy.sas.showLess(0, -1);
			
			sas(`My nipples?!?`);
			txt(`@sas was initially reluctant but we eventually managed to convince her she would look really good with her nipples pierced.`);
			sas(`...fine, I'll do it.`);
			txt(`She bashfully stripped and laid down. The guy was really professional and soon she was owner of brand new nipple ${choice("bars")} / ${choice("rings")}.`);
			
			link(`Piercing. `, 122);
			
			effects([
			{
				id: "initial",
				fce(){
					remove("nipples", effigy.sas);
				},
			},
			{
				id: "bars",
				fce(){
					mile.girls_piercings = 16;
					const item = create("nippleBars");
					wear(item, effigy.sas);
				},
			},
			{
				id: "rings",
				fce(){
					mile.girls_piercings = 17;
					const item = create("nippleRings");
					wear(item, effigy.sas);
				},
			},
		])
		break;
				
	case 122: 
			drawUnzoomed(PC);
			
			sas(`AHHHHHHH!`);
			txt(`She screamed even before the guy brought the needle but quickly calmed down when he was done.`);
			sas(`Well... that wasn't so bad...`);
			kat(`I told ya.`);
			sas(`Do I look good?`);
			eva(`You look great!`);
			
			next();
			break;
	}
}	


//EVAS BOYFRIEND
export const tits_pics = (index)=> {
	switch(index){
		default:
		case 101: 
			set.irl();
			placetime("my room");
			mile.eva_ada = true; //TODO - failsafe - he should be introduced before this event

			ada(`hi! how r you?`);
			txt(`My phone buzzed. I checked it and it was a message from @ada, @eva's boyfriend. I wondered what he wanted.`);
			kat(`Hello! Im fine! How are you doing?`);
			ada(`im doing great, i just replaced batery of my motorbike for new and it like doubled the range! what bout you? anything new? 😎`);
			kat(`No, not really`);
			txt(`I decided to not share with him I was blackmailed by a trio of perverted nerds.`);
			ada(`no <i>big</i> changes? 😁`);
			ada(`🍈🍈?`);
			txt(`It seemed like the news about my recent breast enlargement were like a wildfire.`);
			kat(`You know? 😮`);
			ada(`yeah! guys were talking about you. and from @eva`);
			ada(`so iwanted to know if its true`);
			kat(`It is! What do they say?`);
			ada(`👍`);
			ada(`they told me they ❤️ them`);
			kat(`Happy feedback is positive! 😄`);
			kat(`and @eva?`);
			ada(`she was ranting a bit, you know her`);
			ada(`it seems she cares bout your tits more than i do`);
			kat(`You dont care about my tits 😟`);
			ada(`no, call me oldfashioned but when im dating girl i care only about her and her tits!`);

			link(`<big>😮😞</big>`, 102, ()=> mile.tits_pics = 1);
			link(`<big>😛😃</big>`, 102, ()=> mile.tits_pics = 2); 
			link(`<big>😈</big>`, 102, ()=> mile.tits_pics = 3); 
			break;
			
		case 102:
			if(mile.tits_pics === 1){
				kat(`Not even little? 😮😞`);
				ada(`well, maybe a little 😉`);
				kat(`😊`);
				ada(`honestly i do wonder how they look`);
				kat(`amazing! 😜`);
				ada(`sent me a pic!`);
				ada(`pls 😉`);
				
			}else if(mile.tits_pics === 2){
				kat(`Why dont I belive you? 😛😃`);
				ada(`youre unjustly suspicious person? 😇`);
				kat(`Last time we met you were ogling me pretty hard even though @eva was right there!`);
				ada(`you got me 😉`);
				ada(`honestly i do wonder how they look`);
				kat(`amazing! 😜`);
				ada(`sent me a pic!`);
				ada(`pls 😉`);

			}else if(mile.tits_pics === 3){
				kat(`So youre saying that if I offered to send you my nudes, youd refuse? 😈`);
				ada(`yes. i care only about @eva's tits`);
				kat(`I dont believe you`);
				ada(`try me!`);
				kat(`Should I send you pic of my tits? 😉`);
				ada(`yes!`);
				ada(`oh no! 😩`);
				ada(`i failed! 😢`);
				ada(`you were right! 😉`);
				ada(`im waiting for the pic 😇`);
			
			}
			
			txt(`The flirting with him over the messanger was fun until he asked the picture of my new breasts. What a horny bastard! He was a boyfriend of my ${mile.sas > mile.eva ? "second" : ""} best friend!`);
			
			con_link(mile.slut < 12, `high slut`, `Im not sending my nudes to everybody...`, 103, ()=> mile.tits_pics = 1); 
			link(`@eva would be mad! `, 104, ()=> mile.tits_pics = 2);
			con_link(mile.slut > 6, `low slut`, `Okay. `, 110, ()=> mile.tits_pics = 3); 
			break;
			
		case 103:
			kat(`Im not sending my nudes to every guy who asks for them!`);
			if(mile.slut < 2){
				txt(`I was not a slut who would send her naughty picture to guys!`);
			}else if(mile.slut < 7){
				txt(`I was only ever sending naughty pictures to my boyfriends I trusted, I would never send them to a random horny guy!`);
			}else if(mile.slut > 9){
				ada(`there are topless pictures of you online 😕`);
				kat(`Jerk-off to them 😜`);
				ada(`there are no pictures of your new tits, im just curious 😉`);
			}
			ada(`im not just any guy, you know me 😉`);
			ada(`you didnt get new tits to keep them for yourself, did you 😉`);
			ada(`youre one of the hottest girl i know and your tits were already astonishing before 😍`);
			ada(`i cant imagine how amazing they look now! 😉`);
			ada(`i promise i wont tell a living soul 😇`);
			
			link(`Fine! 😉`, 110);
			link(`No. 😛`, 120);
			link(`No! `, 120);
			break;
			
		case 104:
			kat(`@eva would be mad!`);
			ada(`@eva wouldn mind, youre friends, arent you?`);
			kat(`😂😂😂`);
			kat(`you dont know her! she would be livid!`);
			kat(`😂😂😂`);
			ada(`we dont have to tell her 😉😇`);
			kat(`I cant sext with her her boyfriend!`);
			ada(`were not sexting!`);
			ada(`i just want to see how the enlarged 🍈🍈 look`);
			ada(`if good maybe ill manage to convice @eva to get her boobs enhanced too!`);
			ada(`wouldnt that be cool 😉`);
			kat(`Arent her tits alread big?`);
			ada(`they cold be bigger 😎`);
			kat(`You guys are horrible!`);
			ada(`pls, i swear shell never find out! 😇😉`);
			
			link(`No. 😛`, 120);
			link(`Fine! 😉`, 110);
			link(`No! `, 120);
			break;
			
		case 110:
			showLess(0,-1);
			txt(`I am not sure what was my exact motivation to agree. Was I just happy about my new breasts and wanted to share my joy? Or was it because @ada was a handsome, cool guy and I loved his attention? ${mile.sub > 11 ? "And it was so easy to submit to his demands. " : ""} Or because I enjoyed being a bad girl and doing something mischievous behind @eva's back? ${mile.slut > 7 ? "Or the nerds were right and I was just a shameless slut?" : ""} ${mile.slut < 3 ? "Or maybe I wanted to experience something dumb and naughty I had never tried before?" : ""}`);
			txt(`Either way I stripped my top${wears.bra ? "and bra" : ""} and prepared to take a quick picture. Should I send him just the detail of my new enlarged breasts? Or go for something superior?`);
			link(`Just detail of tits without face. `, 111, ()=> mile.tits_pics_detail = 1);
			link(`Hot topless picture. `, 111, ()=> mile.tits_pics_detail = 2);
			link(`Photo of whole body just in panties. `, 111, ()=> mile.tits_pics_detail = 3);
			break;
			
		case 111:
			if(mile.tits_pics_detail === 1){
				drawZoomed("chest");
				txt(`I concluded the most provident option was to just send the picture of my tits without my face attached.`);
			}else if(mile.tits_pics_detail === 2){
				//drawZoomed("top");
		//TODO DRAW ZOOMED TOP
				txt(`I decided to take a cute photo of me smiling and flashing my bare breasts.`);
		//TODO - POSSIBLE SHE IS WEARING NO PANTIES 
			}else if(mile.tits_pics_detail === 3){
				showLess(0, 1);
				txt(`I decided a more holistic approach was required. My body was not just a sum of its sexy parts, you have to see it whole to be truly awed.`);
			}
			kat(`What do you think?`);
			ada(`😲😲😲`);
			ada(`😍😍😍`);
			ada(`im speechless!`);
			ada(`absolutely amazing body! 👍`);
			kat(`thx 😄`);
			if(wears.piercedNipples){
				ada(`i had no idea you got pierced nipples! 😲`);
				kat(`I do 😉`);
				ada(`seriously hot!`);
			}
			ada(`im in love with your new tits 😍`);
			kat(`Better than @eva's, right?`);
			ada(`👍`);
			ada(`yeah 😉`);
			next(`Done. `, ()=> drawUnzoomed() );
			break;
			
		case 120:
			mile.tits_pics_detail = -1;
			ada(`whatever!`);
			ada(`ill tell @eva youre mean at me! 😛😉`);
			kat(`Try it, I wonder about her reaction when you tell her I refuse to send you my nude pics 😂😂😂`);
			ada(`😄😄😄`);
			kat(`😄😄😄`);
			next();
			break;
	}
}


export const tits_pics_eva = (index)=> {
	switch(index){
		default:
		case 101: 
			mile.eva_exploit++;
			emo("focus");
			
			eva(`Do you have a moment, @kat?`);
			kat(`Yeah, sure. What do you need?`);
			eva(`It's about @ada.`);
			kat(`Do you need relationship advice?`);
			if(mile.tits_pics_detail === 1){
				eva(`Yeah, I do.`);
				txt(`She placed a phone with a photo of boobs in front of me. It was the picture of my breasts I sent to @ada. How could he be so careless? I was in big trouble!`);
				eva(`During a routine checkup of his phone, I discovered this! Some slut sent him her whorish tits!`);
				txt(`Fortunately, I sent him just a picture of my breasts, not my face.`);
				kat(`What a bitch! Do you have any idea of who it was?`);
				eva(`I thought about asking you. Do you have any idea who that adulterous big-titted skank might be?`);
				kat(`I have no clue either.`);
				eva(`I didn't say I have no clue! Do you think I don't recognize your tits, you horny bimbo bitch?!`);
			
			}else{
				eva(`Yeah, I do. Some slut sent him a picture of her whorish tits! I managed to get only a glimpse before he tabbed away, thinking I'm stupid and didn't notice anything!`);
				txt(`Fuck, it might be a picture of me and my tits I sent to @ada. Or the other hand it might not, maybe there were more girls sending him their nude pictures?`);
				kat(`It could be just porn?`);
				eva(`Why would he need porn when has me?`);
				kat(`Good point.`);
				eva(`And I saw the screen of the messenger.`);
				kat(`Do you have any idea of who it was?`);
				eva(`I thought about asking you. Do you have any idea who that adulterous big-titted skank might be?`);
				kat(`I have no clue either.`);
				eva(`I didn't say I have no clue! I saw the whole picture, including your stupid grinning face, you horny bimbo bitch!`);
			}
				
			link(`I have no idea where he got that picture! `, 102, ()=> mile.tits_pics_eva = 1)
			link(`I'm sorry!`, 102, ()=> mile.tits_pics_eva = 2)
			break;
			
		case 102:
			emo("shock");
			
			if(mile.tits_pics_eva === 1){
				mile.eva_exploit++;
				
				kat(`I have no idea where he got that picture!`);
				eva(`Are you saying you didn't send it?`);
				kat(`I sent my topless picture to several guys, maybe one of them resend it to @ada? That is a plausible explanation, isn't it?`);
				eva(`Yeah, that sounds plausible... except I actually read your whole conversation! You were flirting with my boyfriend and making fun of me!`);
				txt(`Fuck, fuck, fuck. I desperately tried to recall what I wrote and how bad it was.`);
				kat(`I'm sorry @eva! I swear that was everything, just one picture!`);
				eva(`Was it everything? Or is that just another lie?`);
				kat(`I swear that was everything! You know I would never cheat with your boyfriend!`);
			}else{
				eva(`You're sorry?! That is so fucking nice! Everything's forgiven!`);
				txt(`She yelled at me.`);
				kat(`It was just one picture, nothing else! I swear!`);
				eva(`I actually read the whole conversation! You were flirting with my boyfriend and making fun of me!`);
				kat(`But that was everything! You know I would never cheat with your boyfriend!`);			
			}
			
			if(mile.slut > 11){
				eva(`I know you're a nympho who has no shame and is eager to go with anything with a dick but I always thought you have at least the basic decency to stay away from your friends boyfriends!`);
			}else{
				eva(`Wouldn't you? I'm not sure anymore! You're recently acting completely erratically and behaving like a total slut! Fine, whatever. But have at least the basic decency to stay away from your friends boyfriends!`);
			}
			
			kat(`I can stay away from him! It was just one stupid photo! It was a stupid mistake and I'm very sorry I betrayed your trust! It will never ever happen again!`);
			txt(`@eva looked at me sternly.`);
//TODO  ALTERNATIVE FOR EVA BURNED
			eva(`Fine!`);
			txt(`That was a good sign. I was afraid her reaction will be much worse. She will be sulking for a few days but she will eventually forgive me.`);
			next();
			break;
	}
}	
		
		
		





//DATE WITH SAS
export const sas_date = (index)=> {
	const sassy = 3; //sas's sassiness treshold
	
	switch(index){
		default:
		case 101: 
			placetime(`At Home`);		
			txt(`@sas texted me:`);
			sas(`Hey, do you want to hang out?`);
			kat(`Absolutely!`);
			txt(`I agreed even thought I was not sure what were her intentions. Neither of us talked about what we did and how we fingered each other but at the school @sas was acting a bit awkward every time we stayed alone.`);
			sas(`Great 😺! my place?`);
			kat(`Sure 👍`);
			link(`Visit @sas. `, 102);
			break;
		

		case 102:
			placetime(`@sas's place`);		
			emo(`happy`);
			
			kat(`Hello, @sas!`);
			sas(`Hello, come in!`);
			txt(`@sas anxiously smiled and pointed towards her room. The flat was quiet -`);
			sas(`Well, my parents aren't home.`);
			txt(`- @sas explained. I lifted a large frog plushie occupying the swing chair hanging from the ceiling and sat down. I lightly swung and examined the toy.`);
			kat(`Is this a new one? This is seriously getting out of hand! I mean how many stuffed frogs an adult woman needs? I have to tell @eva and we have to make an intervention.`);
			txt(`I joked but grim @sas snatched the plushie out of my hands and threw it away.`);
			sas(`It's an old one! And we need to have a serious conversation!`);
			txt(`She sat on her bed, staring at me. I shrugged:`);
			kat(`Oh... okay. Sure. Of course. A serious conversation about what?`);
			sas(`You know about what!`);
			kat(`About what?!`);
			sas(`How we... did *it*!`);
			kat(`Right. So what do you think?`);
			sas(`It was an understandable mistake. We are not a couple of lesbians who are into each other, we are just young horny girls who wanted to experiment. So I think we should forget it ever happened... I mean... unless you enjoyed it?`);

			link(`You're right, it was a mistake. ${spoiler("@sas will move on")}`, 103);
			link(`Right, it wasn't anything serious. But we definitely should do it again. ${spoiler("friends with benefits")}`, 104);
			link(`I love... doing stuff with you. ${spoiler("lovers")}`, 105);
			break;
		
		case 103:
			emo(`focus`);
			mile.sas_date = -1;
			mile.lesbian--;
			mile.sas_angry = true;
			txt(`I agreed with her. @sas did not seem entirely happy but she nodded:`);
			sas(`Right! Nothing happened! We can continue being friends without any awkward feelings!`);
			kat(`Exactly!`);
			sas(`Great... you're a great friend, @katalt.`);
			kat(`...you too, @sasalt!`);
			next();
			break;
			
		case 104:
			emo(`focus`);
			mile.sas_date = 1;
			mile.lesbian++;
			mile.sas_exploit++;
			kat(`You're right, it was just horny experimenting that should not be taken seriously. Just couple of friends fooling around. `);
			txt(`@sas nodded but she didn't seem very happy.`);
			sas(`Exactly!`);
			kat(`Although I have to say, I did enjoy it.`);
			sas(`Well... I enjoyed it too. I mean, I don't want to make it sound weird but you have very dexterous fingers.`);
			kat(`And there is no reason why to not do it again. If you want to.`);
			sas(`I want to. If you do.`);
			kat(`When guys disappoint you, why don't have fun with your bestie, right?`);
			sas(`...yeah, right! ...I'm your best second choice if there's available no guy.`);

			if(mile.sas_sassy > sassy){ //TODO - CHECK THIS VALUE!
				kat(`I do feel kinda horny right now... and since your parents are away...`);
				sas(`Stand up and strip for me!`);
				txt(`@sasalt firmly ordered. All her previous anxiety was suddenly gone.`);
				link(`Obey. ${spoiler("dominant @sas")}`, 120, ()=> counter.change = false); 
				con_link(mile.sub < 5, `too submissive`, `Don't tell me what to do! ${spoiler("submissive @sas")}`, 130, ()=> counter.change = true);
			}else{
				kat(`And since I am here... and our parents are away...`);
				sas(`W... what should we do?`);
				link(`Strip for me! ${spoiler("submissive @sas")}`, 120, ()=> counter.change = false); 
				con_link(mile.sub > 10, `too dominant`, `Whatever you want!  ${spoiler("dominant @sas")}`, 130, ()=> counter.change = true);
			}
			break;

		case 105:
			emo(`happy`);
			mile.sas_love++;
			mile.lesbian++;
			mile.lesbian++;
			mile.sas_date = 2;
			kat(`I... I kinda loved doing it with you, @sas.`);
			txt(`I reassuringly smiled at her because she seemed anxious.`);
			sas(`I... I did loved doing it with you, @kat. I'm very lucky I have... friend like you! I mean, I don't want to make it sound weird but you have very dexterous fingers. I... you being there somehow made it feel more intensed than doing it myself alone...`);
			kat(`Well... thanks.... I assume I'm great but thanks for the review... you were pretty good too!`);
			sas(`Should we - I mean since we both enjoyed it - do it again sometimes? ${mile.sas_sassy <= 1 ? "Please?" : ""}`);
			
			if(mile.sas_sassy > sassy){ //TODO - CHECK THIS VALUE!
				kat(`I do feel kinda horny right now... and since your parents are away...`);
				sas(`Stand up and strip for me!`);
				txt(`@sasalt firmly ordered. All her previous anxiety was suddenly gone.`);
				link(`Obey. ${spoiler("dominant @sas")}`, 120, ()=> counter.change = false); 
				con_link(mile.sub < 5, `too submissive`, `Don't tell me what to do! ${spoiler("submissive @sas")}`, 130, ()=> counter.change = true);
			}else{
				kat(`And since I am here... and our parents are away...`);
				sas(`W... what should we do?`);
				link(`Strip for me! ${spoiler("submissive @sas")}`, 120, ()=> counter.change = false); 
				con_link(mile.sub > 10, `too dominant`, `Whatever you want!  ${spoiler("dominant @sas")}`, 130, ()=> counter.change = true);
			}
			break;

		
		case 130:  //sas dom; sas gets licked 
			emo(`imp`);
			mile.sas_dom = true;
			mile.sas_sassy++;
			mile.sas_sassy++;
			showLess(1, 1);
			//effigy.sas.showLess(-1, 0);
			
			if(counter.change){
				//I have to say, I rather enjoyed obeying the order and stripping for her.  					
			}else{
				sas(`Whatever do I want?`);
				kat(`I'm usually... rather submissive when I'm with a guy... you know...`);
				sas(`So you like being told what to do?`);
				kat(`I wouldn't put that this way...`);
				sas(`Strip down for me!`);
				txt(`@sas firmly ordered.`);
			}
			txt(`It was uncanny to see @sas so bossy and domineering. However, I did not hesitate for a second and very carefully stood up from the swing chair. I swayed my hips, to give my performance a teasing, erotic appeal and slowly removed my @upperLower.`);
			if(!wears.bra && !wears.panties){
				mile.sas_commando_kat++;
				sas(`You're not wearing any underwear!`);
				kat(`Does that make you feel offended and outraged?`);
				sas(`I'm makes me feel horny and aroused!`);
				kat(`Great!`); // TODO
			}else if(wears.panties){
				sas(`Your ${wears.sexyPanties ? "sexy" : "cute"} panties too!`);
				txt(`I shoved my thumbs beyond the waistband of my @panties and slowly pushed them to down${wears.shavedPussy ? "to reveal my shaven pussy. " : ""} .`);
			}
			sas(`You have such a hot body it's almost unfair!`);
			txt(`Laughed @sas and I bowed down to kiss her. My fingers ran over her back and when we interrupted the kiss, I helped @sas out of her  ${effigy.sas.upper.name}. @sas not even complaining when I toppled her on the bed and in one motion pulled down both of her ${effigy.sas.lower.name} and panties. @sas reached for a pillow and make herself comfortable, her legs invitingly open.`);
			sas(`I want you to lick my pussy, @katalt!`);
			txt(`She pointed down. Her voice sounded almost confident but I noticed how nervous she was. I was on edge too. She was my best friend, I was no lesbian ${mile.slut < 15 ? "and this was seriously beyond my comfort zone. I had never experienced anything like that. " : " and even though I fooled around with girls before, I had never gone so far.  "} ${mile.lesbian > 1 ? "Not counting what I did in virtual reality, this was different, too real. " : ""} But I kinda craved to taste her.`);
			kat(`Sure, I can do that!`);
			txt(`I moved between her legs and kissed her belly. I tickled her pussy with my fingers and then with my tongue, triggering @sas's sharp moaning:`);
			sas(`Ooohhh!`);
			kat(`Is that a good ooohhh or bad ooohhh?`);
			sas(`What? Good! Very good! Oh, @kat! Very good! This is like dream come true!`);
			txt(`Happy I returned to eating out her pussy. I took my time, slowly building up her anticipation before I softly grazed her clit with the tip of my tongue.`);
			sas(`OOOHHH!`);
			
			link(`Toy with her clit. `, 150, ()=> counter.sex = 1); 
			link(`Use your fingers. `, 150, ()=> counter.sex = 2); 
			break; 
			
		case 120:  //sas sub; kate gets licked 
			emo(`horny`);
			mile.sas_dom = false;
			mile.sas_sassy--;
			effigy.sas.showLess(1, 1);
			
			if(counter.change){
				sas(`I have no idea you're a bossy bitch even in bed!`);
				kat(`Especially in a bed! Well, I do enjoy being in charge.`);
				sas(`Okay. So what should I do?`);
				kat(`Strip down!`);		
			}else{
				sas(`O... Okay!`);
			}
			txt(`Bashfully smiling @sas stood up and slowly began undressing, dropping her ${effigy.sas.upper.name} and ${effigy.sas.lower.name} until she was only in her cute underwear. It was a lovely sight, even though it was still a bit weird to look at her not as my close friend but as somebody I wanted to ${mile.slut > 10 ? "fuck" : "make love with"}.`);
			sas(`Should I strip completely?`);
			kat(`What do you think, dummy? You aren't shy, are you? I saw you nude before.`);
			sas(`Well, yeah, but...`);
			txt(`She shrugged. I knew what she meant, I felt the sexual tension too, this was not like stripping before gym classes or swimming.`);
			sas(`And now what?`);
			txt(`Asked naked @sas.`);
			kat(`Now you'll kiss me!`);
			txt(`I ordered,  stood up and kissed her, teasingly squeezing her butt. Her fingers grabbed my @upper and when we interrupted our loving kiss, she took it off.`);
			sas(`I liked that! What is next?`);
			txt(`Wanted to know @sas and enthusiastically lightly fondled my breasts.`);
			kat(`Now you'll show me how you're skilled with your tongue!`);
			txt(`I pulled down my @lowerPanties, laid on her bed and reached for her pillow to make myself comfortable. @sas was nervously watching me, I opened my knees wide apart and beckoned her to come closer.`);
			kat(`I want you to go down on me! You know, lick my pussy. Don't worry, just...`);
			sas(`....I know what I'm supposed to do!`);
			txt(`@sas proclaimed but she did not seem to be very sure when she moved between my legs.`);
			kat(`Ahhhh! That feels nice!`);
			txt(`I encouraged her when she kissed my belly and tickled my pussy with her tongue and fingers.`);
			if(PC.vch || PC.hch){
				sas(`Does the piercing feel good?`);
				txt(`She wondered and the tip of her tongue wiggled with the jewel adorning my clit hood.`);
				kat(`OH YES! ...yeah, it does!`);
			}else{
				txt(`@sas took her time, slowly building up the anticipation before she softly grazed my clit with the tip of her tongue.`);
			}
			
			link(`You are doing great! `, 150, ()=> counter.sex = 3); 
			link(`You could be doing even better!  `, 150, ()=> counter.sex = 4); 
			break; 
			
			
		case 150: 
			if(counter.sex <= 2){ //sas dominant; sas got licked 
				showLess(0, 0);
				effigy.sas.showLess(1, 0);
			}else{ //sas sub; kate got licked 
				effigy.sas.showLess(0, 0);
				showLess(1, 0);
			}
		
			if(counter.sex === 1){
				txt(`I let my tongue wander, then I again focused on @sas clit, gently teasing it in a circular motion.`);
			}else if(counter.sex === 2){
				txt(`I gently inserted my index finger into her slit and curled it upward.`);
			}else if(counter.sex === 3){
				mile.sas_sassy++;
				sas(`T... thank you!`);
				txt(`@sas smiled at me - she was touched by my praise - and began eating out my pussy with renewed verve.`);
			}else{
				mile.sas_exploit++; //TODO CHECK
				sas(`<small><small>What a bitch. </small></small>`);
				txt(`Mumbled  @sas, annoyed I was messing with her.`);
				kat(`What are you saying?`);
				sas(`Nothing!`);
				txt(`She still yearned to please me and returned to eating out my pussy with renewed verve.`);
			}
		
			if(counter.sex <= 2){
				sas(`OOOHHH!`);
				txt(`The room was filled with @sas's soft, funny, high-pitched moaning. I eagerly continued but suddenly I felt like I was being watched.`);
				txt(`I raised my head from between her thighs and turned. The door to the room was open and on the floor was sitting a small tabby creature and curiously observed us.`);
			}else{
				kat(`Oh yes! Fuck yes!`);
				txt(`I laughed, it was so good. But suddenly I felt like I was being watched.`);
				txt(`I turned to the door, it was open and on the floor was sitting a small tabby creature and curiously observed us.`);
			}
			
			link(`Oh no! `, 151); 
			break;
			
			
		case 151:	 
			emo(`anoy`);
			sas(`What!?`);
			txt(`@sas was spooked she did something wrong but I pointed to the door.`);
			kat(`Your cat is a pervert!`);
			if(counter.sex <= 2){
				sas(`He isn't a pervert! Hello, Loto! He is curious about what was going on here, aren't you? He was asleep when you arrived. Well, he's asleep most of the time. `);
				kat(`Maybe you shouldn't moan so loudly, you woke him up. Probably even your neighbors could hear you.`);
				sas(`Shut up!`);
			}else{
				sas(`He isn't a pervert! Hello, Loto! He is curious about what was going on here, aren't you? He was asleep when you arrived but you woke him up with your moaning!`);
			}
			kat(`Do you think he knows we are having sex?`);
			sas(`Yeah, cats know how sex works.`);
			kat(`But we are both girls! And I wasn't sticking anything inside you!`);
			sas(`...I don't know. Can we continue?`);
			kat(`I'm not sure if we can.`);
			sas(`He's just a cat!`);
			if(mile.slut < 10){
				kat(`It makes me feel weird being watched during sex! `);
			}else{
				kat(`It makes me feel weird being watched by an animal when we are having sex! And eating pussy right in front of him feels just wrong`);
			}
	//TODO - ONLY IF THEY PAY? 
			if(mile.slut > 20){
				sas(`I thought you were a kinky slut who doesn't mind being watched.`);
				kat(`I don't mind being watched by humans but being watched by a different specie is a bit creepy.`);
			}
			sas(`Fine!`);
			txt(`@sas stood up. Her bare feet squished on the floor and I could appreciate her from behind when she went to grab the tomcat.`);
			if(mile.hair_crazy){
				sas(`Come on, Loto, the ${mile.hair_color}-haired lady is too shy to do *it* in front of you!`);
			}else{
				sas(`Come on, Loto, @katalt is too shy to do *it* in front of you!`);
	
			}
			
			link(`Continue.`, 152); 
			break
			
		case 152:
			effigy.sas.showLess(0, 0);
			showLess(0, 0);
			emo(`horny`);
			txt(`They disappeared. I lazily masturbated until @sas returned alone and closed the door.`);
			sas(`I see you can have fun even without me.`);
			if(counter.sex <= 2){ //sas dominant; sas got licked 
				kat(`Don't get offended and jealous.`);
				sas(`Where we were?`);
				kat(`We were about to switch, it is your turn to lick me.`);
				sas(`Don't get sassy with me, @katalt! I hope your tongue isn't tired, you are not stopping until you make me orgasm!`);
				kat(`What about 69?`);
				sas(`Hmmm... I always kinda wanted to try that.`);
				txt(`@sas nodded.`);
			}else{ //sas sub; kate got licked 
				kat(`Don't get offended. Where were we before your troublesome feline interrupted us?`);
				sas(`I was eating out your pussy.`);
				txt(`@sas was almost blushing when she was saying that.`);
				sas(`@kat?`);
				kat(`Yeah?`);
				sas(`When I was giving food to Loto, I had this idea... maybe we could try the 69 position? You know, the one w-`);
				kat(`Yeah, I do. Sure, sounds good!`);
				txt(`I nodded.`);
			}
			
			//TODO - more complicated argument? 
			if(mile.sub > 10){
				kat(`So... how are we going to do it? Which one of us should be on the top and which one on the bottom?`);
				sas(`I guess I could be on the bottom and you could be on the top?`);
				txt(`@sas decided.`);
			}else{
				sas(`So... how are we going to do it? Which one of us should be on the top and which one on the bottom? Are there any rules?`);
				kat(`Lay down, I can be on the top.`);
				txt(`I instructed her.`);
			}
			
			link(`69.`, 153); 
			break
			
		case 153:
			emo(`bliss`);
			log_sex("69", "kat","sas");
			
			txt(`@sas positioned herself on the bed and I carefully climbed on top of her, her snatch lovely presented right in front of my face.`);
			kat(`How are you doing down there?`);
			sas(`I'm doing fine, I have a gorgeous view.`);
			txt(`Reassured I bowed down and my tongue touched her pussy. At the same time, I could feel hers touching mine. We engaged in mutual pussy licking, giving each other pleasure the best way we could. @sas was paying an attention and immediately copied when I did something she especially enjoyed. I was the first one who climaxed, it was really easy to trigger me, I never had trouble peaking even in unusual positions.`);
			txt(`It took a bit longer before I made @sas cum too but it was worth it. Overwhelmed by her arousal she gave up licking, she was just lying and funnily moaning. Finally, she could not take my devious, merciless cunnilingus anymore, faintly shrieked and come.`);
			//TODO orgasm description
			txt(`I rolled on the bed, stared at the ceiling and stretched my body. @sas tapped on my arm:`);
			sas(`That was beautiful, @kat! Thank you!`);
			if(counter.sex <= 2){ //sas dominant; sas got licked 
				kat(`Thank you! We are both so amazing at this.`);
			}else{ //sas sub; kate got licked 
				kat(`You are welcome!`);
			}
			sas(`Whoa. This was my first time with a girl...`);
			kat(`Mine too. ...well, if we don't count the virtual reality.`);
			sas(`Oh, I wasn't counting the virtual reality, if I did I would be a pretty exeprienced ${mile.sas_date === 2 ? "lesbian" : "bisexual"} slut, heh.`);
			kat(`Oh, really? Cool. Did you enjoy the 69?`);
			sas(`Yeah, it was awesome! ...I mean... it was good... well, I didn't hate it... I just... I couldn't fully enjoy what you were doing while I was distracted by your pussy. Don't get me wrong, I loved going down on you but I would kinda prefer to focus only on that. You know?`);
			kat(`Yeah, I see your point. Next time we can try something with less multitasking.`);
			txt(`@sas rolled over and cuddled to me:`);
			if(mile.sas_date === 2){
				sas(`Oh @kat! I... I love that you're my best friend!`);
			}else{
				sas(`Oh! So there will be the next time?`);
			}
			next();
	}
}




//SAS EVA 

export const sas_eva_date = (index) => {switch(index){
	default:
	case 101:
			placetime("At home");
			txt(`@eva texted us:`);
			eva(`Hey, girls? Im alone at home. wanna hang out?  watch a movie? 😱`);
			sas(`Yeah 👍😺`);
			kat(`Im not sure theres a lot ofstuff I have to do ☹️`);
			sas(`Come on, @kat! 🥺`);
			eva(`yeah!`);
			kat(`Fine!`);
			sas(`I'll bring wine 😉`);
			kat(`I can bring something sweet`);
			sas(`🧁?`);
			kat(`👍`);
			sas(`😻`);
			eva(`Great! 😀`);
			link(`Baking. `, 102);
			break;
			
			
	case 102: 
			emo("focus");
			txt(`I decided to follow @sas's suggestions and bake muffins. Making muffins was extremely quick and easy, I did not even have to weigh anything. First I turned on the oven to heat it up to 180°C. Then I grabbed a mug and put into a bowl 2 cups of plain flour and ¾ cup of sugar. Then I added salt, a sachet of baking powder and a sachet of vanilla sugar. Then I cracked two eggs and added ¾ cup of canola oil and ¾ cup of semi-skimmed milk and finally 3 large spoons of rum (I used to add dry stuff first to prevent the flour from sticking to the wet mug). I properly mixed everything with a mixer. To make the muffins even better I added blackcurrant and redcurrant.`);

			txt(`The only complicated part of the recipe was pouring the batter into paper cups laid in a special muffin tray without spilling and making a mess - the remains of the batter outside the paper cups burned and were a pain to remove. And you have to be careful and not fill more than two-thirds of the paper cups or so because the muffins rose a lot in the oven. I baked them until they were nicely golden, which usually took a little bit longer than 20 minutes.`);
			
			if(mile.sas_date === 2){
				link(`Continue. `, 103);
			}else{
				link(`Continue. `, 104);
			}
			break;
	
	case 103: 
			placetime("Tram stop close to @eva's place");
			emo("relax");
			sas(`Hello!`);
			kat(`Hey!`);
			txt(`I met with @sas on a tram stop, she was waiting for me there and together we went to @eva's place. We were walking together and suddenly I felt @sas hand squeezing mine. I glanced at her.`);
			if(mile.sas_dom){
				sas(`Is it okay?`);
				txt(`She smirked back at me.`);
			}else{
				sas(`Is... is that okay?`);
				txt(`She nervously smiled.`);
			}
			link(`Yeah! `, 104, ()=> mile.sas_hand = true);
			link(`Don't make it weird. `, 104, ()=> mile.sas_hand = false);
			break;
 
	case 104:
			if(mile.sas_date === 2){
				if(mile.sas_hand){
					mile.sas_love++;
					kat(`Of course!`);
					txt(`I smiled and we continued to @eva's place holding hands. We let go only when we were within view from @eva's windows.`);
				}else{
					sas(`Oh, sorry.`);
					txt(`She quickly let my hand go. I did not mind eating out her pussy but handholding in public felt weird.`);
				}
			}
			eva(`Hello, girls!`);
			txt(`@eva welcomed us when we arrived and invited us in. We sat down, I put the muffins I brought on a plate and @eva opened the @sas's white wine and poured it into tall glasses.`);
			eva(`You will love the movie I chose. I have wanted to watch it for quite a long time but it is supposed to be too scary to watch it alone. It is about a group of women on a spelunking adventure in a cavern system deep underground. But then things go wrong...`);
			sas(`I don't even have to watch it. I'm already creeped out.`);
			eva(`Good!`);
			txt(`@eva made herself comfortable in her armchair and turned the film on.`);
			txt(`...`);
			eva(`WHAT!?!`);
			kat(`I didn't expect anybody to die in the first minute!`);
			sas(`...wait, the kid died too?`);
			kat(`Yeah.`);
			sas(`Oh my!`);
			eva(`This is awesome!`);
			if(mile.sas_dom){
				emo("shock");
				txt(`@eva was excited by the unexpected unfair violence and focused on the movie but I was distracted by something else. I was sitting on a sofa next to the @sas and I felt her hand on my leg.`);
				txt(`I looked at her like: What the hell are you doing? but she just impishly smiled and lightly stroked my inner thigh. Needless to say, I found it very arousing.`);
				txt(`Then she stopped:`);
				sas(`I... I'm thirsty and I don't want to get drunk. I'll get a glass of water?`);
			}else{
				emo("imp");
				txt(`@eva was excited by the unexpected unfair violence and focused on the movie but I was distracted by something else. I was sitting on a sofa next to the @sas and could sense her warm body so close to mine and inhale her lovely scent. Without thinking I placed my hand on her leg.`);
				txt(`She stared at me, looking aghast like: What the heck are you doing @katalt? but I just impishly smiled and continued lightly stroking her inner thigh.`);
				sas(`I... I'm thirsty and I don't want to get drunk. I'll get a glass of water?`);
				txt(`@sas made an excuse to escape me.`);
			}
			eva(`Sure! You know where is the kitchen.`);
			txt(`Answered @eva without stopping watching the movie. When @sas left I stood up too:`);
			kat(`I think I'm thirsty too.`);
			eva(`Sure!`);
			
			link(`Kitchen. `, 105);
			break;
			
	case 105:
			emo("horny");
			if(mile.sas_dom){
				txt(`@sas was waiting for me in the kitchen. Without hesitation, she embraced and kissed me. I gently pushed her away.`);
				kat(`What the hell are you doing, @sasalt! We are at @eva's place! You can't molest me when I'm sitting right next to her!`);
				sas(`Sorry! But it's only your own fault! I can't control myself when you are wearing such ${wears.skirt ? "a shameless" : "shameless"} @lower! Your legs are just too sexy! Not mentioning what is between them!`);
				txt(`She leaned closer and we kissed again. The risk of being caught was making it a bit thrilling.`);
				sas(`Let's go back!`);
				txt(`She patted my ass.`);
				kat(`The glasses with water!`);
				sas(`Oh right, returning without them might be suspicious.`);
				txt(`We returned to the living room, @eva was engrossed in the movie and barely paid us any attention. @sas exploited the opportunity and touched me again, this time traveling with her fingers higher between my legs. I did not want to let her get away with it and my hand disappeared under her skirt.`);
			}else{
				txt(`@sas was waiting for me in the kitchen. Without hesitation, I embraced and kissed her. She gently pushed me away.`);
				sas(`What the hell are you doing, @katalt! We are at @eva's place! You can't molest me when I'm sitting right next to her!`);
				kat(`It seems I can.`);
				sas(`You're such a horny nympho! Have you no shame? Can't you bear sitting next to me for ninety minutes without touching me?`);
				kat(`It seems I can't!`);
				sas(`Oh, @kat!`);
				txt(`I leaned closer and we kissed again. The risk of being caught was making it a bit thrilling.`);
				kat(`Let's go back!`);
				txt(`I patted her ass.`);
				sas(`The glasses with water!`);
				kat(`Oh right, returning without them might be suspicious.`);
				txt(`We returned to the living room, @eva was engrossed in the movie and barely paid us any attention. I sat down, sipped from my glass and then exploited the opportunity. I touched @sas again, letting my fingers disappear under her skirt, traveling higher to the place where her legs met. @sas did not want to let me get away with it and her hand began caressing my thigh.`);
			}
			
			link(`Bathroom. `, 106);
			break;
			
	case 106:
			txt(`Needless to say, we completely ignored the movie and the only thing we watched was the back of @eva's head when we tried to guess whether she was turning to us. We were getting pretty horny when @sas stopped and pushed my hand away. She stood up to leave the room but @eva glanced at her a @sas hastily explained:`);
			sas(`I have to pee!`);
			eva(`I mean should I stop the movie?`);
			sas(`No, you can continue without me!`);
			eva(`...okay.`);
			txt(`When @eva turned back to the screen, @sas winked at me and left. I waited for a while and then I stood up too.`);
			kat(`I...`);
			txt(`I almost said I have to pee too. I promptly grabbed my glass.`);
			kat(`Should I bring you water too?`);
			eva(`...sure. Should I stop-`);
			kat(`That's fine!`);
			txt(`I found @sas sitting on the bathtub, her right hand doing something under the waistband of her skirt. She panicly twitched when I entered like she was expecting it might be @eva.`);
			if(mile.sas_dom){
				kat(`Seriously, @sas? You're that horny?!`);
				sas(`Yeah!`);
			}else{
				kat(`Who is the horny nymphette now?`);
				sas(`It's your fault! You got me all excited. Sexually excited.`);
			}
			kat(`You slut!`);
			sas(`I crave to eat your muffin. And I don't mean it literally - even though they are really delicious, thank you for them - but as a clever double entendre.`);
			kat(`Thanks for the clarification.`);
			sas(`Should I lock the door?`);
			
			link(`Of course. `, 107, ()=> mile.sas_eva_date = 1);
			con_link(mile.slut > 1, `not enough slut`, `...no. `, 107, ()=> mile.sas_eva_date = 2);
			break;
			
		case 107:
			emo("bliss");
			log_sex("oral", "kat");
			if(mile.slut < 10) mile.slut++; //TODO - forced ?
			showLess(-1,0);
			mile.lesbian++;
			
			if(mile.sas_eva_date === 1){
				txt(`@sas locked the door and we have a short moment just for ourselves. Until @eva starts banging on the door.`);
			}else{
				mile.slut++; //TODO maybe more
				txt(`The tone of her voice was strange when she asked and she devilishly grinned when I slowly shook my head. @eva could enter at any moment but that was what was making it exciting.`);
			}
			txt(`I climbed on top of a white *Whirlpool* washing machine and horny @sas pulled down my @lowerPanties.`);
			kat(`Are we fucking crazy?`);
			txt(`I whispered, I was so tense I did not dare to speak aloud.`);
			if(mile.sas_date === 2){
				sas(`I'm crazy for you!`);
			}else{
				sas(`Yeah, we are!`);
			}
			txt(`She madly giggled and buried her face between my wide-opened thighs. Fuck, she was such an eager bunny. I patted her hair and tried to suppress my moaning. It was so kinky getting my pussy eaten out at @eva's bathroom while she was watching a movie in a room on the other side of the hallway and she was suspecting nothing!`);
			txt(`I lost track of time, I had no idea how long we were in the bathroom but it had to be significantly longer than bringing two glasses of water. But @sas tongue was poking against my clit and I was unable to think rationally, at that point @eva discovering us seemed like a lesser evil than stopping and missing the orgasm. Fuck @eva and fuck everything, I just wanted @sas to make me cum!`);
			txt(`And I was there! I squeezed @sas head between my thighs and bit my lower lip. With eyes closed, I tried to quietly handle the surge of sublime bliss.`);
			txt(`My knees were weak and @sas had to help me down. We kissed but then I hurriedly put my clothes back on.`);
			kat(`We have to go back!`);
			sas(`Yeah!`);
			kat(`I'll go first, wait for a while. And wash your face, you don't want to go back there smelling like a pussy!`);
			sas(`Fuck you, @kat! You owe me one cunnilingus!`);
			txt(`@sas laughed.`);
			link(`Living room. `, 108);
			break;

	case 108:
			showAll();
			emo("shock");
			txt(`@eva was watching the movie, seemingly unperturbed by our absence.`);
			eva(`Did you bring me the water I asked for?`);
			txt(`She asked without turning to me.`);
			kat(`Oh... sorry, I forgot!`);
			txt(`I jumped back on my feet and in the doorway ran into @sas. @eva stopped the movie and angrily slammed the remote on the table.`);
			eva(`Do you think I'm fucking stupid?!`);
			kat(`...no?`);
			sas(`Of course not!`);
			eva(`I thought we are friends! You don't have to do things behind my back!`);
			kat(`...you ...you know!?!`);
			eva(`Yeah! I know exactly what you were doing! You could have told me!`);
			kat(`We thought...`);
			eva(`...that it would hurt my feelings? Don't you know I'm a bitch with no feelings? When you don't like the movie I picked, you should say something, I won't get offended! We can watch something else, you don't have to hide in the kitchen!`);
			sas(`...`);
			kat(`...`);
			sas(`...but we-`);
			kat(`Sorry, we should have told you!`);
			sas(`Yeah. Sorry. We didn't like the movie.`);
			eva(`I'm hurt by your lack of good taste but fine, whatever. What else do you want to watch?`);
			sas(`M...maybe something less horrifying and more funny?`);
			eva(`Hmmm.... You might like *Dead Alive* or *Evil Dead*.`);
			next();
			break;
}}





export const sas_a_jealousy = (index)=> { switch(index){
	default:
	case 101:
			set.irl();
			placetime("In front of the school building");
			emo(`happy`);

			sas(`...no! They are not dangerous. They are poisonous because they eat termites and ants. But if you feed them like fruit flies, they are harmless. But they are pretty territorial...`);
			txt(`The school day was over. I walked out of the school building together with @sas, immersed in discussion with her. However, on the corner we run into @qaa who was waiting for me.`);
			qaa(`Hey, @kat!`);
			kat(`What are you up to?`);
			qaa(`I was waiting for you, I thought we could walk home together.`);
			if(mile.sas_sassy > 2){
				sas(`Well, sorry but we were just in the middle of an important conversation.`);
				link(`Yeah, we need to resolve something. `, 102);
			}else{
				txt(`@sas glanced at me, she would prefer to continue chatting with me rather than leave me with @qaa.`);
				link(`Sorry but I need to discuss something important with @sas. `, 102);
			}
			link(`We can chat later, @sas. `, 103);
			break; 


	case 102: 
			mile.sas_a_jealousy = 2;
			emo(`shy`);

			txt(`@qaa was not offended. He smiled at us:`);
			qaa(`Sure! We can text later, to discuss our next date. Bye! Bye, @sas!`);
			kat(`Bye!`);
			sas(`Bye!`);
			txt(`He left us alone. We were silent for the next ten meters but then @sas turned to me:`);
			
			if(mile.a_date_react === 3 || mile.a_public >= 4){
				sas(`Why are you even dating that stupid dork!?!`);
			}else{
				mile.stat--;
				mile.a_public = 3; //she tells eva
			
				if(mile.a_date_react === 1){
					sas(`Date? You told us you took him on one date out of pity!`);
				}else if(mile.a_date_react === 2){
					sas(`Date? You actually seriously started dating him? Is that why are you spending so much time with those nerds?`);
				}else{
					sas(`Date?!? Why did he mean by that? Are you dating him? Is that why are you spending so much time with those nerds?`);	
				}
				txt(`I cursed @qaa's indiscretion.`);
				kat(`Yeah, kinda. He asked me out a few times.`);
				sas(`Why would you even agree to go out with such a stupid dork?!?`);
			}
			txt(`She frowned and I was not sure how to answer.`);
			kat(`I don't know. He's kinda sweet, cares about me and appreciates me.`);
			sas(`There are plenty of other people who care about you and appreciate you! You deserve somebody better!`);
			if(mile.sas_hand){
				txt(`@sas gently took my hand.`);
			}
			kat(`Thank you for saying that.`);
			next();
			break;

		case 103: 
			emo(`unco`);
			mile.sas_a_jealousy = 1;
			mile.sas_exploit++;

			txt(`@sas was taken aback that I chose @qaa over her.`);
			sas(`Ah... sure... no problem.`);
			if(mile.a_date_react === 3 || mile.a_public >= 4){
				txt(`She looked at him and then at me.`);
				sas(`You guys are still dating?`);
				qaa(`Yeah, we are! Aren't we, babe?`);
				txt(`Proudly proclaimed @qaa.`);
				kat(`Yeah, ${mile.a_girlfriend ? "we are" : "kinda"}.`);
				txt(`@sas stared at me, looking rather a bit disappointed. She told me I deserve somebody better than him.`);
			}else{
				mile.stat--;
				kat(`Sorry, but I need to discuss something with @qaa too.`);
				qaa(`Yeah, and we are dating.`);
				txt(`Proudly proclaimed @qaa and I cursed his indiscretion.`);
				if(mile.a_public > 2){
					sas(`You are actually dating?!`);
					qaa(`@katalt didn't tell you?`);
					sas(`She mentioned something like that...`);
				}else{
					sas(`What?!? You are dating?!?`);
					qaa(`Yeah, we are, aren't we, @kat?`);
					kat(`${mile.a_girlfriend ? "Yeah, we are. " : "I wouldn't say dating but we were on a few dates..."}`);
					qaa(`@kat didn't tell you?`);
					sas(`She did not. But I noticed you're lately spending a lot of time together...`);
				}
				txt(`@sas stared at me, looking rather disappointed, almost betrayed.`);
			}
			sas(`Well... have fun... we can finish our conversation later... bye!`);
			txt(`So I went home with @qaa.`);
			qaa(`What's wrong with her?`);
			kat(`There's nothing wrong with @sas!`);
			if(mile.a_date_react === 3 || mile.a_public >= 4){
				qaa(`Oh, right. So I was thinking, maybe on the next date we could...`);
			}else{
				mile.a_public = 3; //she tells eva
				qaa(`Oh, right. You still didn't tell your friends we are dating?`);
				txt(`Great, I managed to make both of them unhappy.`);
				kat(`I think I mentioned it. Why does it matter?`);
				qaa(`I... I kinda want this to be real. Like we dating, and being serious, and everybody knowing about it. Sorry, it's dumb.`);
				kat(`This *is* real!`);
				txt(`I ensured him and we continued walking to the tram stop.`);
			}
			next();
			break; 
}}





function break_up(){
	mile.sas_gf = true;
	
	if(!mile.sas_a_jealousy){
		return next();
	}

	txt(`Then @sas spoke again:`);
	sas(`Does that mean you'll stop dating @qaa?`);
	kat(`You are jealous!?`);
	sas(`${mile.sas_dom ? "Yes. " : "...well, kinda. "}`);
	kat(`We weren't discussing whether we are exclusive.`);
	sas(`Yeah. We are doing it right now.`);
	if(mile.slut > 20){
		kat(`You know I'm a nympho. I'm not sure if I would be able to resist temptation and stay faithful.`);
		txt(`It was not easy for me to admit that but @sas only giggled:`);
		sas(`I know you're a sick, horny pervert, @kat! I don't mind your whoring around but if we are dating, I don't want you to date anybody else.`);
	}else if(mile.slut > 10){
		kat(`You know I'm a slut. I kinda want to experiment with my sexuality and I think you should be doing the same.`);
		sas(`I know you're a horny slut, @kat, that's why I love you. I don't mind sex, I just don't want you to date anybody else than me.`);
	}else{
		kat(`Oh.`);
		sas(`I don't want you to date anybody else when you are dating me.`);
	}
	kat(`I see.`);
	if(mile.sas_exploit > 2){
		sas(`Why are you even wasting time with that loser? Him having a crush on you isn't your problem! You shouldn't date him just out of pity! It isn't good for you and it isn't fair to him!`);
	}else{
		sas(`Why are you even wasting time with that loser? Him having a crush on you isn't your problem! You shouldn't date him just out of pity! It isn't good for you and it isn't fair to him! You have to be more selfish!`);
		kat(`You're the first person telling me I'm not selfish enough.`);
	}
	
	link(`I will break up with him eventually.  `, 212, ()=> mile.sas_strapon_break = 2);
	link(`I don't want to break up with him.  `, 211, ()=> mile.sas_strapon_break = 1);
	link(`Admit he's blackmailing you. `, 213, ()=> mile.sas_strapon_break = 3);
}


export const sas_strapon = (index)=> { switch(index){
	default:
	case 101:
			set.meta();
			placetime("My Place");
			mile.sas_strapon = true;

			sas(`✂️?`);
			kat(`?`);
			sas(`wanna have sex? I'm horny and free`);
			sas(`😉`);
			kat(`your such a slut!`);
			sas(`🥺`);
			kat(`fine!`);
			sas(`😸`);
			sas(`come to me, I explained Lothar how to behave, there wont be any incidents, I swaer!`);
			kat(`come to me, i have suprise`);
			sas(`I love surprises!`);
			sas(`🤩`);
		
		link(`Wait for @sas.  `, 102);
		break;

	case 102: 
		present(	//TODO
			[`sas`, ["polyStockings", "bimboThong", "bimboPushUpBra", "shortSkirt", "tightTee", "loopEarrings"] ],
		);
		emo(`happy`);

		sas(`Hello!`);
		txt(`@sas smiled when I opened the door and welcomed her. She was happy and excited to see me.`);
		kat(`Hey, babe! Come in!`);
		sas(`What's the surprise?! I can't wait!`);
		kat(`It will shock you!`);
		sas(`Okay?`);
		txt(`I instructed her to sit on my bed, then I hid behind the door of my wardrobe, got ready and jumped out wearing a strap-on.`);
		sas(`Ohhhh! Oh my!`);
		kat(`Are you shocked?`);
		sas(`Indeed I am! I'm utterly stunned! You're such a pervert, @katalt!`);
		kat(`Come on, touch it!`);
		txt(`I pushed the strap-on into her face. Her fingers brushed over the silicone surface.`);
		sas(`The girl I watch on Youtube has a similar one. How much did it cost?`);
		kat(`I don't know. Like ${money[`strapon_${mile.strapon}`]} ees, but I'm not sure if I remember it correctly. I can look for the receipt.`);
		sas(`You don't have to.`);
		
		link(`Continue.  `, 103);
		break;
			
	case 103:
		showLess(1,2);
		effigy.sas.showLess(1,2);

		kat(`So what do you think?`);
		sas(`I like it!`);
		kat(`Should we try it?`);
		if(mile.sas_dom){
			sas(`Yeah! Give it to me!`);
			kat(`Hey! It's mine!`);
			sas(`Come on, @katalt. We both know I'm the man in this relationship and you're just my bottom bitch.`);
			kat(`You're such a meanie!`);
			txt(`I shook my head but removed the strapon and gave it to her.`);
		}else{
			sas(`Oh yeah!`);
			txt(`@sas's cheeks were rosy, her imagination was already running wild.`);
			kat(`Awesome. Take your clothes off!`);
		}
		sas(`I have a surprise for you too! I bought new underwear.`);
		txt(`She stripped her top and skirt.`);
		kat(`Nice! You look so hot!`);
		txt(`I stepped closer to kiss her and she kissed me.`);

		link(`Continue.  `, 104);
		break;
		
	case 104:
		if(mile.sas_dom){
			sas(`So... How I'm supposed to put it on?`);
			kat(`Let me help you!`);
			txt(`I knelt next to @sasalt and firmly adjusted all straps.`);
			kat(`Comfortable?`);
			sas(`Yeah.`);
			txt(`I touched the dildo with my cheek and then licked the tip.`);
			sas(`Is this what are you doing with guys, you little tease?`);
			kat(`Hmmmm!`);
			sas(`Show me more! I want to witness your best cock-sucking technique!`);
			txt(`I seductively licked @sas fake cock and then took it in my mouth.`);
			if(mile.slut > 12){
				sas(`Oh! Fuck, you are wild! Shame I don't have a real dick! But now I see all the rumors about you and sucking cocks are true!`);
			}else{
				sas(`Damn, you're good! If I was a guy, I would definitely love it!`);
			}
			if(!mile.sas_love){
				kat(`...ehhh, thanks?`);
				sas(`You should be ashamed! How can even other girls like me compete with you?`);
				kat(`You should be taking notes!`);
			}
		}else{
			txt(`I took the strapon off so I could undress and then attached it again.`);
			sas(`Hehe, you look so funny with a dick.`);
			kat(`Stop laughing!`);
			sas(`How do we start?`);
			kat(`With foreplay. Suck my cock!`);
			sas(`Are you serious?`);
			kat(`Yeah! Got on your knees, you little slut!`);
			txt(`She knelt in front of me, grabbed the plastic shaft with one hand and licked the tip.`);
			sas(`Do you like it?`);
			if(!mile.sas_love){
				kat(`If this is how you're usually sucking cocks, I'm not wondering you're without a boyfriend!`);
			}else{
				kat(`Hmmmm!`);
			}
			sas(`This is so dumb!`);
			kat(`Have you ever thought: one day I'll be sucking my bestie's dick?`);
			sas(`I certainly did not!`);
			txt(`@sas took the strap-on between her lips to give me a simulated blowjob but she had trouble not giggling.`);
		}

		link(`Continue.  `, 105);
		break;

	case 105: 
		emo(`horny`);
		if(mile.sas_dom){
			showLess(0, 0, 1);
			txt(`I think I was quite good at giving blowjobs and I enjoyed showing off in front of @sas. But we were not messing for long, I was too impatient to feel her fake dick inside me. I crawled onto the bed and stripped all my clothes.`);
			kat(`How do you want to fuck me? From behind?`);
			sas(`No, no, no! I want to watch your pretty face!`);
			kat(`Awwww! You're so sweet!`);
			sas(`And your dumb expression when I make you cum hard!`);
			txt(`I laid on my back and teasing touched her body with my toes. @sas took my foot, kissed the instep and then shoved my legs apart and moved between them. We kissed and her plastic cock was poking against my lower belly.`);
			sas(`This is awkward, I'm usually on the other side of being penetrated.`);
			kat(`It can't be so hard when even guys are able to do it!`);
			sas(`Are you fine? Or should I change something?`);
			txt(`@sas gingerly pushed the dildo inside me.`);
			kat(`More than fine! Fuck me hard!`);
			txt(`@sas leaned against the bed, her beautiful face above mine, her hair cascading down, and began thrusting with her hips.`);
			kat(`Oh yeah! That feels great!`);
			txt(`I laughed and more confident @sas fucked me rougher and with more energy. I was masturbating and relishing the pleasure of being inexperiencedly pounded by my bestie. My arousal was rising up and she quickly brought me to my first orgasm.`);
		
		}else{
			effigy.sas.showLess(0, 0, 1);
			txt(`Seeing @sasalt kneeling in front of me and trying to seductively suck the strapon was pretty funny but I definitely enjoy it more if I was a guy. We were not fooling around for long, I ordered her to get on the bed and strip her panties. @sas obeyed and briskly got nude.`);
			txt(`I did not enter her immediately, there was no rush. We were making out and I fingered her a little.`);
			sas(`Oh, @kat!`);
			kat(`Oh, @sas!`);
			txt(`She opened her legs wide and invited me between them. I pushed the dildo against her sweet little pussy.`);
			kat(`Yeah!`);
			txt(`I trusted and she was moving against me.`);
			sas(`@kat, I love your huge cock inside me so much!`);
			kat(`Oh, shut up your tease!`);
			txt(`She laughed and her fingers were toying with my boobs.`);
			sas(`Make me!`);
			txt(`I began fucking her harder. @sas's laid back, focusing on the dildo penetrating her. She was masturbating and wrapped her bare legs around my body.`);
			sas(`Oh yeah! That's it!`);
			kat(`Are you cumming?`);
			sas(`You bet!`);
			txt(`@sas arched and shivered as the ecstasy surged through her body.`);
		}

		link(`Continue.  `, 106);
		break;

	case 106:
		emo(`happy`);
		if(mile.sas_dom){
			effigy.sas.showLess(0, 0, 1);
			txt(`@sas did not stop when I climaxed for the first time and continued furiously fucking me until she was totally exhausted.`);
			sas(`You totally drained me, you horny nymphet.`);
			kat(`I like how you could go on and on and still stay hard!`);
			txt(`I helped her to remove the strap-on and then bowed down above her lap. As a reward, I ate out her pussy and made her cum hard too.`);
		
		}else{
			showLess(0, 0, 1);
			txt(`I continued fucking @sas even after her first orgasm until I was totally exhausted and had to stop.`);
			kat(`You totally drained me, you horny nymphet!`);
			sas(`Don't worry! Now I can do something for you!`);
			txt(`She playfully licked the tip of my fake cock which was wet with her own juices and then helped me to remove the strap-on. She bowed above my lap and began eating out my pussy, eventually making me cum as hard as I made her.`);
			
		}
		
		if(mile.sas_love){
			txt(`The sex amazing. But almost as good was even just lying next to @sas, relaxing and feeling her calming, warm presence next to me.`);
			txt(`We were quiet, relishing the intangible moment, until @sas interrupted the silence.`);
			sas(`@kat?`);
			kat(`Yeah?`);
			sas(`${mile.sas_sassy < 3 ? "I think " : ""}I love you.`);

			
			link(`Cool. `, 200, ()=> counter.cool = "Cool");
			link(`Thank you. `, 200, ()=> counter.cool = "Thank you");
			link(`Who doesn't? `, 200, ()=> counter.cool = "Who doesn't");
			link(`I love you too! `, 210);
			link(`I love you more! `, 210);
			if(hardbug){
				link(`##Switch to friends with benefits route. `, 106, ()=> mile.sas_love = 0);
			}
		}else{
			txt(`It was amazing to have a friend like @sas. Having sex with her was in some ways better than having sex with a boyfriend - we could enjoy our physicality together without any emotional attachment or playing games. But we were not strangers, we knew, cared about and trusted each other.`);
			next();
			if(hardbug){
				link(`##Switch to lovers route. `, 106, ()=> mile.sas_love = 1);
			}
		}
		break;

	case 200: 
		emo(`unco`);
		sas(`That's your response?!?`);
		txt(`@sas tender voice was suddenly cold and sharp.`);
		kat(`Well... I wasn't sure what should I say...`);
		sas(`You weren't sure what you should say?!?`);
		txt(`Damn, I messed up. @sasalt was barely suppressing her distress.`);
		sas(`I... I share with you my deepest feelings.... and *${counter.cool}* is the answer I get?!?`);
		txt(`Her eyes filled with tears and I quickly hugged her.`);
		if(mile.sas_exploit > 3){
			sas(`Why are you always so selfish and inconsiderate? I would do anything for you but it seems you never appreciate me!`);
			kat(`I appreciate you very much!`);
			txt(`I reassuringly caressed her hair.`);
		}else{
			sas(`I'm sorry @kat! I know our relationship is awkward and I understand you're confused... I just... I love you so much!`);
		}
		kat(`I'm just not sure what I feel.`);
		sas(`Do you want to be my girlfriend? I won't mind if you say no but please be honest with me, I deserve that. `);

		link(`I do! `, 201, ()=> mile.sas_strapon_love = 2);
		link(`I think I do. `, 201, ()=> mile.sas_strapon_love = 3);
		link(`I'm not sure. `, 202, ()=> mile.sas_strapon_love = 4);
		link(`Can't we just stay friends with benefits? `, 203, ()=> mile.sas_strapon_love = 5);
		break;

	case 201:
		if(mile.sas_strapon_love === 2){
			txt(`I love you and I want you to be my gilfriend! `);
			mile.sas_love++;
			txt(`I confidently smirked at @sas and wiped the tear rolling down her face. She was smiling again, filled with joy.`);
		}else if(mile.sas_strapon_love === 3){	
			txt(`I... I do have feelings for you... I think I do want to be your girlfriend. `);
			txt(`@sas deserved a better answer but I did not have a better one. Still, she seemed to be happy and satisfied.`);
		}
		sas(`You won't regret it! I can be a better ${mile.sas_dom ? "boyfriend" : "girlfriend"} than any guy!`);
		txt(`@sas kissed me. We were making for a while and then returned to relaxing under the blanket.`);
		
		break_up();
		break; 


	case 202:
		sas(`I see. I understand, you can have as much time as you want.`);
		kat(`Thank you, @sas. You're the best.`);
		sas(`If I'm the best, why don't you want to date me?`);
		if(mile.sas_sassy > 2 || mile.sas_dom){
			mile.sas_love = 0; //TODO - this is dagerous
			mile.sas_halt = true;
			emo(`unco`);
			txt(`I laughed and leaned closer to kiss her but she pushed me away.`);
			kat(`What is going on?`);
			sas(`I think we should stop having sex.`);
			kat(`Why?`);
			sas(`Because I have my dignity and I'm not interested in meaningless sex. Either we are dating or not.`);
			kat(`Yeah... I guess that's fair...`);
		}else{
			emo(`happy`);
			txt(`I laughed closer and kissed her. I successfully managed to postpone the problem and gained time to figure out my feelings.`);
		}
		next();
		break; 

	case 203: 
		mile.sas_love = 0;
		emo(`sad`); //TODO
		effigy.sas.showAll();
		txt(`@sas resolutely shook her head.`);
		sas(`No, @kat. We went too far. I have feelings for you and I don't care about meaningless sex. If you can't love me, we should friends but nothing more.`);
		kat(`I'm sorry @sas.`);
		sas(`That's fine. It was too good to be true. I think I'll go home.`);
		txt(`She began dressing up and then awkwardly hugged me.`);
		kat(`We are still best friends, right?`);
		sas(`Of course we are!`);
		next();
		break;


	case 210:
		mile.sas_love++;
		mile.sas_strapon_love = 1;
		emo(`unco`);

		txt(`I instinctively reacted, without overthinking the complex feelings I had for her. She was something more than just my friend. Was she my lover? What would that mean? That I am romantically into girls?`);
		sas(`Does it mean we are like dating?`);
		kat(`Yeah, I think it does.`);
		sas(`Oh @kat! I love you so much!`);
		txt(`@sas kissed me. She was filled with joy and I could not say anything that might hurt her. We were making out for a while and then returned to relaxing under the blanket.`);
		
		break_up();
		break;

	case 211: 
			//mile.sas_love = 0; //TODO - this is dagerous
			sas(`What do you mean by that, @kat?`);
			txt(`@sasalt sounded annoyed.`);
			kat(`Well, I do have feelings for him too. And I don't want to break his heart.`);
			txt(`Also, I did not want to risk breaking up with him before resolving the issue with the fake AI. But I was not going to share with @sas that particular detail.`);
			sas(`Well, you'll have to break his heart or mine. You can't have us both.`);
			kat(`I'm sorry, @sas but I have no idea what to do.`);

			if(mile.sas_sassy > 2 || mile.sas_dom){
				mile.sas_love = 0; //TODO - this is dangerous
				mile.sas_halt = true;
				sas(`That's fine. You don't have to make the decision right now.`);
				kat(`Thank you.`);
				sas(`But you'll have to make it eventually. And we should stop doing this, I'm not interested in meaningless sex. Either we are dating or not.`);
				kat(`You mean...`);
				sas(`Yeah, no more sex for you. `);
			}else{
				sas(`That's fine. You don't have to make the decision right now. But you'll have to decide eventually.`);
			}
			next();
			break;


	case 212:
			mile.sas_love++;
			kat(`I will break up with him. Eventually.`);
			txt(`I did not want to risk breaking up before resolving the issue with the fake AI. However, I was not going to explain @sas that.`);
			kat(`I can drop it on him out of nowhere, that would break his heart. I have to prepare him.`);
			txt(`I was expecting @sas will not be satisfied with such an evasive answer but she nodded.`);
			sas(`Of course. I don't want you to break his heart either. I do understand his feelings.`);
			next();
			break;
		

	case 213: 
		emo(`unco`); //TODO

		txt(`I tried to keep my dealing with the nerds secret. I kinda felt embarrassed that I allowed them to control my life. And I did not want @sas to feel sorry for me. But it was no longer fair to hide the facts from her. And I felt so close to her, I was trusting her implicitly and I finally desperately needed to share my hardships with somebody real.`);
		kat(`I kinda can't break up with him right now...`);
		txt(`I had no idea where to start, so I started at the beginning and continued from there. @sas's expressions were changing as I was narrating my story, going from shocked to outraged.`);
		sas(`That's so horrible, @kat!`);
		kat(`Yeah, it is.`);
		sas(`You have to tell the police! Or to the teachers!`);
		kat(`I really wish I did several weeks ago. But if went to them now, I would look like a total dumbass who willingly let the bastards abuse her for so long! And I don't want anybody to know about this! Don't you dare to tell anybody!`);
		txt(`@sas frowned:`);
		sas(`Of course, I won't! You don't have to worry, all your secrets are safe with me!`);
		kat(`Thank you! I have to solve it on my own!`);

		if(mile.sas_sassy < 3){ //TODO - which one? 
			mile.sas_blackmail = 1;
			sas(`No, you don't have to! I'm here for you! And if there's any way I can help you, I will.`);
			kat(`Thank you! You have no idea how much it means for me! But I don't want you to get involved in the supid nerd's schemes and cause you troubles.`);
			sas(`But I want to get involved! I want to get involved in everything concerning you, no matter what troubles it brings!`);
			txt(`She hugged me.  I felt like I did not deserve somebody like @sasalt.`);
			next();
			if(hardbug){
				link(`##Switch to sassy @sas route. `, 213, ()=> mile.sas_sassy = 4);
			}
		}else{
			effigy.sas.showLess(1, -1);
			sas(`No! You clearly need my help! I'll go to talk with that perverted bastard right now! I won't let anybody abuse you this way! On which floor he lives?`);
			txt(`She hastily began dressing up.`);
			kat(`You don't mean right, now?`);
			sas(`Of course, I do!`);
			txt(`It was scary to see @sas so furious.`);

			link(`Stay here! It's my own bussiness!`, 214, ()=> mile.sas_strapon_stop = 1);
			link(`Please, don't get involved, I can handle it!`, 214, ()=> mile.sas_strapon_stop = 2);
			link(`He won't listen to you.`, 214, ()=> mile.sas_strapon_stop = 3);
			link(`Thank you for helping me.`, 214, ()=> mile.sas_strapon_stop = 4);
			if(hardbug){
				link(`##Switch to meek @sas route. `, 213, ()=> mile.sas_sassy = 2);
			}

		}
		break;


	case 214:
		effigy.sas.showAll();

		if(mile.sas_strapon_stop === 4){
			mile.sub++;	
			txt(`I was moved she was so eager to stand up for me.`);
			sas(`Don't worry! You should've told me sooner! I'll show that perverted bastard I won't allow anybody to abuse you!`);
			txt(`@sas seemed very self-assured. I had my doubts, I tried many times to convince them and every time I failed. But maybe they'll listen to @sas? Maybe the nightmare was finally over?`);
		
		}else if(mile.sas_strapon_stop  === 3){
			mile.sub++;
			kat(`I don't think you can do anything to help me, he won't listen to you!`);
			sas(`Oh, he will! I'll make him!`);
			txt(`@sas seemed self-assured. I did not have her confidence, they had too much dirt on me and I seriously doubted there was anything @sas could tell them to change their minds. But maybe... I dared to hope.`);
			txt(`@sasalt finished getting dressed and left me.`);

		}else if(mile.sas_strapon_stop === 2){
			mile.sub++;
			txt(`She looked at me, smiled and lightly touched my cheek:`);
			sas(`No, you clearly can't, @kat. Let me help you!`);
			txt(`@sasalt ignored all my concerns, finished getting dressed and left me.`);

		}else if(mile.sas_strapon_stop === 1){
			txt(`@sas stubbornly shook her head:`);
			sas(`No! It's my business too! You're only mine and they can't do this to you!`);
			txt(`@sasalt ignored all my concerns, finished getting dressed and left me.`);
		}
		
		link(`Wait for her return.`, 215);
		break;

	
	case 215:
		showLess(-1, 1);
		emo(`unco`); //TODO

		txt(`I got out of bed and nervously walked across my room. @sas was not returning and I was getting more and more anxious. Could she succeed? Could this be the end of the nerd's sick game?`);
		
		con_link(mile.sas_exploit > 4, `too nice to @sas`,  `Wait for @sas.`, 230);
		con_link(mile.sas_exploit <= 4, `too mean to @sas`,  `Wait for @sas.`, 220);
		break;

	

	case 230:
		emo(`shock`); //TODO
		mile.sas_blackmail = 3;
		mile.evil_sas = true;
		mile.sub++;
		mile.sas_sassy++;
		mile.sas_sassy++;
		
		txt(`When @sas finally returned, she seemed lost in thought. It was impossible to guess whether she was successful or whether she failed.`);
		kat(`So what happened?`);
		txt(`@sas shrugged.`);
		sas(`We talked.`);
		kat(`And?`);
		sas(`Well, at first I was angry but then he explained to me their position. It was rather eye-opening, you have to admit, they kinda have a point.`);
		kat(`You can't be serious!`);
		sas(`You could really be a huge selfish bitch who is mean and exploitative. I would say you kinda did bully @qcc and intentionally led @qaa on.`);
		kat(`I did not! And even if I did, it doesn't give them the right to blackmail me!`);
		sas(`He explained to me that it wasn't blackmail but that you made a deal with them regarding the AI.`);
		kat(`I was forced to make that deal with them!`);
		sas(`They are adamant that you have to honor their deal. But I managed to make an arrangement with @qaa.`);
		kat(`What kind of arrangement?!?`);
		sas(`He agreed to involve me.`);
		kat(`No! You can't let them blackmail you too!`);
		sas(`Well... involve me in the task planning.`);
		kat(`What?!? What the fuck?`);
		sas(`Well... they were interested in my ideas... and I can make sure the task will be silly and non-consequential, nothing too serious that would actually hurt you...`);
		kat(`You are joining the nerds?!?`);
		sas(`@kat! They won't stop blackmailing you whether I'm involved or not. I'm just trying to help you and this is the best solution I was able to come up with. I'm doing this for you and I'm seriously not pleased you're not appreciating my effort!`);
		kat(`Well, I do appreciate you trying to help me but...`);
		sas(`Just say *thank you*!`);
		kat(`...thank you.`);
		next();
		break;


	case 220: 
		emo(`shock`); //TODO
		mile.sas_blackmail = 2;
		mile.sas_sassy--;

		txt(`When @sas finally returned, I immediately realized that all my hopes were in vain. She was crestfallen, walking slowly with her head hung.`);
		kat(`What happened?`);
		sas(`Did you ever tried to help somebody but monumentally fucked it up and made the situation even worse than it was before?`);
		kat(`Yeah, many times. But I really hope this question is completely unrelated to your mission.`);
		sas(`I'm so sorry, @kat! I fucked up!`);
		txt(`@sas was close to tears.`);
		kat(`Shhhh! That's okay. What happened?`);
		sas(`Well, I told him he's a pathetic loser... and I was so outraged... and I kinda told him you love only me and never loved him... and he got angry... like really angry... and he explained to me many ways how he can ruin your life...`);
		kat(`You did what!?! What will he do?`);
		sas(`I don't know! But he was very unhappy! I'm so sorry, @kat! You were right, I fucked up everything!`);
		kat(`You did the right thing. I'm proud you tried to stand up for me!`);
		sas(`T.. thank you, @kat.`);
		
		link(`Talk with @qaa. `, 221);
		break;


	case 221: 
		showAll();
		txt(`I had to calm down @sas before letting her go home. She seemed to be troubled by the recent development more than I was. When she was gone, I went to visit @qaa too to see what could be salvaged.`);
		qaa(`Oh, hello, @katalt. I'm surprised you have the audacity to show up!`);
		kat(`Well, I'm here to clarify what @sas meant.`);
		qaa(`There's nothing to clarify, @sas was pretty clear. Everything was a fucking lie! You don't give a fuck about me, you were just trying to weasel your way out of our deal! I know you are a selfish bitch but even I had no idea how huge manipulative sociopath you actually are!`);
		kat(`Slow down! @sas had no idea what she was talking about! My feelings for you are real!`);
		qaa(`Shut the fuck up! Even now you're unable to stop lying?!? I don't even mind you being a fucking dyke, I'm disgusted by your utter dishonesty!`);
		
		link(`Be reasonable! `, 222, ()=> mile.sas_strapon_a_burned = 2);
		link(`I hate you!  `, 222, ()=> mile.sas_strapon_a_burned =  3);
		link(`I love you!  `, 222, ()=> mile.sas_strapon_a_burned = 1);
		break;

	case 222:
		mile.a_burned = true;
		mile.a_hate++;
		emo(`angry`); //TODO

		if(mile.sas_strapon_a_burned === 1){
			kat(`But I do love you!`);
			txt(`I desperately tried to convince him that I really loved him and stunned gasped when his hand landed on my cheek. He seemed embarrassed he lost his temper and slapped me but he did not apologize.`);
			qaa(`Stop playing with my feelings!`);
			kat(`But...`);
			qaa(`No! Just no! I don't want to hear any more of your pathetic excuses!`);
		}else if(mile.sas_strapon_a_burned === 2){
			kat(`Please, be reasonable!`);
			qaa(`I'm reasonable @kat. I'm more reasonable than you can imagine, considering the situation and your betrayal.`);
			kat(`You're not giving me any chance to explain it.`);
			qaa(`You're not getting a chance to come up with excuses and manipulate me!`);
			kat(`But...`);
			qaa(`I don't want to hear more!`);
		}else if(mile.sas_strapon_a_burned === 3){
			mile.sub--;
			kat(`Do you want the fucking truth? I fucking hate you! I despised every moment I was forced to spend with you! How stupid do you have to be to believe I might be ever seriously interested in you? I will never love you and you're such a disgusting perverted creep I'm sure no girl ever will! Unless you'll blackmail her!`);
			qaa(`You whore! You slept with me just to trick me?!`);
			kat(`Yeah, I did. Deal with it, dumbass!`);
			qaa(`You fucking bitch!`);
		}
		txt(`@qaa was seething.`);
		qaa(`Unlike you, I'm a person with integrity. I'll honor our deal but don't expect I'll go easy on you. Now fuck off, seeing your treacherous face is making me sick!`);
		txt(`I left him and went home. This might not be my worst break-up but it was definitely among the top three.`);
		next();
		break;
		
}}






























export const chair_a_reward = (index)=> {
	switch(index){
		default:
		case 101: 
			emo(`suspect`);
			qaa(`Once again, congratulations on your victory and getting reelected to be the class chairman!`);
			kat(`Thank you!`);
			qaa(`It wasn't easy but I managed to convince several people that you might be a bitch but overall you would be a lesser evil than @eva.`);
			kat(`Well, thanks.`);
			qaa(`I did it for you, not for a reward. But I also do want the reward.`);
			kat(`Oh, right. What did I promise to you?`);
			qaa(`Anything I ask.`);
			kat(`Whoa, that was pretty dumb.`);
			qaa(`Yeah, you were pretty desperate.`);
			kat(`Okay. What do you want?`);
			if(mile.a_threesome > 0){
				qaa(`Well.. hear me out and don't instantly get mad. Since you did not mind a threeway in virtual reality I was thinking - and this is just a wild idea - you might not mind a threeway in real life. With one of your friends.`);
			}else{
				qaa(`Well.. hear me out and don't instantly get mad. A man can have many dreams. Date the girl she loves, and have sex with him. Maybe even kinkier dreams, like trying anal sex or involving another girl.`);
			}
			kat(`You want to have a threeway with one of my friends!?!`);

			if(mile.a_dom <= 1){
				qaa(`Yeah. And you wouldn't want to break your promise, would you?`);
			}else{
				qaa(`Yeah. But it's just a suggestion. ${mile.a_dom >= 4 ? "Dumb suggestion. You know what? Please forget I asked.." : ""}`);
			}
			kat(`@eva or @sas?`);
			qaa(`@sas.`);
			txt(`He answered immediately.`);
			kat(`@eva is hotter.`);
			qaa(`Yeah, but she's kinda of intimidating. @sas doesn't seem like a person who would mock me if I did something wrong.`);

			link(`Awesome idea!`, 104);
			link(`I'll try to ask @sas. `, 102);
			link(`No way. `, 103);
			break;

		case 104:
			emo(`imp`);
			mile.chair_a_help = 11;
			if(mile.slut < 10) mile.slut++;
			qaa(`Is it? You don't hate it?`);
			kat(`No! It sounds kinky and I like it. Now the only problem is to convince @sas.`);
			qaa(`I fully trust you. You can be very convincing.`);
			next();
			break;

		case 102:
			mile.chair_a_help = 12;
			kat(`Okay... in general I'm not against your idea...`);
			qaa(`Awesome!`);
			kat(`I'll try to ask @sas. But I'm pretty sure she's going to say *no*.`);
			qaa(`Thank you! You're the best!`);
			next();
			break;

		case 103:
			emo(`anoy`);
			mile.chair_a_help = 13;
			if(mile.slut > 15) mile.slut--;
			kat(`No. This idea is stupid and so are you.`);
			qaa(`You could just say no, you don't have to be mean.`);
			kat(`It would be too weird to involve my best friend in our relationship.`);
			qaa(`That's understandable.`);
			next();
			break;
	}
}






export const sas_a_threesome_reward = (index)=> {
	switch(index){
		default:
		case 101: 
			add_today(`evening`, `sas_a_threesome`);
			placetime(`shopping mall`);
			
			txt(`I went shopping with @sas, she needed new shoes and cat litter for her cat. While she was trying new sneakers, I was trying to figure out where to start.`);
			kat(`So... sex is fun, right?`);
			txt(`@sas finished tying her shoelaces and looked up with a puzzled expression.`);
			sas(`...yeah, it is? Well, especially with you.`);
			txt(`She winked at me.`);
			kat(`It might seem to be awkward to do lewd things with another girl, especially when she's my best friend, but I definitely don't regret it.`);
			sas(`Me neither!`);
			kat(`We are young and it's a good thing to sexually experiment.`);
			sas(`I agree.`);
			kat(`Maybe we should try something new.`);
			sas(`That's an awesome idea! Do you have anything in mind?`);
			kat(`Dunno. Maybe involving other people?`);
			txt(`I suggested and watched her reaction. To my surprise, she smiled and nodded.`);
			sas(`Yeah, I tried orgies in virtual reality but it isn't the same thing. You would like to involve another girl? Who? I'm not sure about asking @eva...`);
			kat(`Yeah, girl or boy. Convincing a guy to have a threesome with us should be a piece of cake.`);
			sas(`Yeah, that's smart. Who? A friend? Or a stranger who doesn't know us?`);
			kat(`I think I would prefer a friend, somebody we know.`);
			sas(`I'm not sure. I don't want to embarrass myself in front of anybody I know. And I don't want people to know about us.`);
			kat(`It should be somebody trustworthy. And somebody we don't have to try hard to impress. Somebody who is non-threatening and harmless.`);
			sas(`Yeah.`);
			kat(`He doesn't have to be a hunk. It's just an experiment. He can be inexperienced, maybe even dorky.`);
			sas(`Yeah.`);
			kat(`Or nerdy.`);
			sas(`@kat, what about ${mile.a_girlfriend && mile.a_public >= 3 ? "your boyfriend" : "@qaa"}? If you wouldn't mind sharing him?`);
			kat(`@qaa? That's a brilliant idea! No, I don't mind sharing him with you. And he will surely agree.`);
			
			chapter(`Continue shopping. `, `shop_mall`);
			next(`Go home. `);
		break;
	}
}


export const sas_a_threesome_sas = (index)=> {
	switch(index){
		default:
		case 101: 
			emo(`suspect`);
			placetime(`shopping mall`);
			
			txt(`I went shopping with @sas and she bought new shoes and cat litter for her cat. However, it seemed she also had lewd side intentions.`);
			sas(`What are you doing this evening? Do you have free time?`);
			txt(`She did not give me any time to answer - she was not ashamed to grab my butt in the middle of the street and whisper into my ear:`);
			sas(`<small>Because I'm horny!</small>`);
			kat(`What the fuck, @sas! Keep it in your pants!`);
			if(mile.slut <= 3){
				sas(`Come on, @kat! You don't have to be such a prude!`);
				kat(`I'm not a prude! I just think that being friends with benefits doesn't mean our every conversation has to be about sex.`);
				sas(`Prove you're not a prude! Let's do something super kinky!`);
			}else if(mile.slut > 15){
				sas(`Come on, @kat! Don't pretend you're a good, chaste girl! I bet your panties are already wet. ${sluttiness(PC).details.noPanties ? "Wait, I see you're not even wearing any," :  "If you are even wearing any,"} you little slut.`);
				txt(`She pointed out I was the last person to make fun of her lustfulness.`);
				kat(`Fine. What do you want to do?`);
				sas(`I'm in the mood for something extra kinky!`);
			}else{
				sas(`Come on, @kat! I know you want it!`);
				kat(`Do we have to discuss it in the middle of the street? And I'm unfortunately busy, I have to study.`);
				txt(`*Study* being a codeword for playing virtual games and entertaining three evil perverts.`);
				sas(`You can either study... or do something especially kinky with me!`);	
			}
			kat(`What do you have in mind?`);
			sas(`Well, I kinda started the journey of my sexual self-discovery. First I lost my virginity, then I bought my first sex toy, then I seduced you and experimented with lesbian sex...`);
			kat(`...I remember it a bit differently...`);
			sas(`...there's one other thing I want to try.`);
			txt(`She gathered the courage to share her naughty cravings.`);
			link(`What? `, 102);
			break
			
		case 102:
			sas(`A threeway.`);
			kat(`A threeway?!`);
			sas(`I would kinda like to experience orgies, like sex with more than other person.`);
			kat(`...you mean in real life?`);
			sas(`Yeah, I already tried it in virtual reality multiple times and I kinda enjoyed it. But you know, it isn't the same thing.`);
			kat(`I see. Where do you get the people for your sexual experiment?`);
			sas(`I was thinking... maybe you would like to volunteer?`);
			kat(`I see. And the other girl?`);
			sas(`I was thinking... convincing a boy might be easier?`);
			kat(`Yeah, no sane guy would refuse a threesome with us. Should we ask a friend? Or a stranger who doesn't know us?`);
			sas(`I was thinking about somebody we both know. Somebody nice. But no hunk, I would prefer somebody non-threatening and harmless. For the experiment I wouldn't even mind somebody inexperienced and dorky, on the contrary, I want somebody I don't have to try to hard to impress. Maybe even a bit nerdy...`);
			kat(`@sasalt?`);
			sas(`@kat, do you think you could ask ${mile.a_girlfriend && mile.a_public >= 3 ? "your boyfriend" : "@qaa"}?`);
			if(mile.chair_a_help === 13){
				txt(`What the hell? It was not surprising @qaa wanted to have sex with @sas. But why she wanted to have sex with him was beyond me.`);	
			}

			link(`Of course! `, 110);
			link(`I can do it for you.`, 111); 
			link(`No. `, 112);
			break;
			
			
		case 110:
			add_today(`evening`, `sas_a_threesome`);
			
			kat(`That sounds like a brilliant idea! I'm sure he's going to love it!`);
			sas(`Thank you, @kat! I wasn't sure if you won't be jealous.`);

			chapter(`Continue shopping. `, `shop_mall`);
			next(`Go home. `);
			break;

		case 111:
			add_today(`evening`, `sas_a_threesome`);
			
			kat(`I'm not sure about this. You're my best friend and ${mile.a_girlfriend ? "he's my boyfriend..." : "I'm going out with him..."}`);
			sas(`I see.`);
			kat(`I guess we can try it. But you owe me a huge favor and if anything goes wrong, it's your fault.`);
			sas(`Of course, @kat! Thank you for the opportunity.`);

			chapter(`Continue shopping. `, `shop_mall`);
			next(`Go home. `);
			break;

		case 112:
			mile.sas_a_threesome = -1;
			
			kat(`I'm sorry. I don't think that's a good idea.  You're my best friend and ${mile.a_girlfriend ? "he's my boyfriend." : "I'm going out with him."} I'm afraid random sex might mess up our relationships.`);
			sas(`That's a reasonable concern. Please forget about it.`);
			kat(`Don't worry!`);

			chapter(`Continue shopping. `, `shop_mall`);
			next(`Go home. `);
			break;
	}
}





export const sas_a_threesome = (index)=> {
	switch(index){
		default:
		case 101: 
			emo(`imp`);
			//placetime(`shopping mall`);
			txt(`@sasalt picked me up at my place and together we went upstairs.`);
			kat(`Are you nervous?`);
			if(mile.sas_sassy > 3){
				sas(`Of course not. What's the worst thing that could happen?`);
			}else{
				sas(`Yeah. I'm very nervous.`);
			}
			txt(`@qaa was surprised when he opened the door and saw us both.`);
			qaa(`Eh, hello!`);
			sas(`Good evening?`);
			kat(`Hi!`);
			if(mile.chair_a_help > 10){
				qaa(`@kat? You're not here because...`);
				kat(`Oh yeah. We are!`);
				txt(`I mischievously smiled at him.`);
				qaa(`Oh! ..well... come in!`);
			}else{
				qaa(`How can I help you?`);
				kat(`@sas was bored so I invited her to hang out with us, if you don't mind.`);
				qaa(`...of course not. Come in!`);
			}
			link(`Enter. `, 102);
			break;
			
			
		case 102:
			txt(`We sat down in his room and the atmosphere was a bit awkward.`);
			if(mile.chair_a_help > 10){
				qaa(`So... @sas, you actually don't mind...`);
				if(mile.sas_sassy > 2){
					sas(`To have sex with you? No, I'm looking forward to it.`);
				}else{
					sas(`Yeah... I don't mind it...`);
				}
				qaa(`Whoa. You two... both so beautiful... it's a bit overwhelming...`);
			}else{
				qaa(`...@sas wanted what?!?`);
				kat(`To have a threeway with you.`);
				qaa(`And you're okay with it?`);
				if(mile.a_cuck > 0){
					kat(`Absolutely. I told you we're not exclusive.`);
				}else{
					kat(`Yeah, she's my best friend.`);
				}
				sas(`Don't worry, it's just meaningless sex. I definitely don't want to be romantically disruptive.`);
				qaa(`Okay. I - of course - agree. But... you two... both so beautiful... it's a bit overwhelming...`);
			}
			
			if(mile.a_tripoint === 1){
				kat(`Come on, what I told you about being more confident?`);
			}else{
				kat(`Too bad! We are here and you have to satisfy us both!`);
			}
			
			if(!mile.sas_dom && mile.a_dom > 3){
				txt(`@sas and @qaa were anxiously staring at each other, neither sure how to start. It was on me to get the things going.`);
			}
			
			if(mile.a_locked){
				link(`Sex. `, 110);
			}else if(mile.sas_dom && mile.a_dom < 3){
				link(`Sex. `, 112);
			}else if(mile.a_dom < 3){
				link(`Sex. `, 111);
			}else{
				link(`Sex. `, 110);
			}
			
			if(hardbug){
				link(`## Submissive @qaa. `, 110);
				link(`## Dominant @qaa. `, 111);
				link(`## Dominant @qaa and dominant @sas. `, 112);
			}
			break;
		
		
		case 110:
			mile.sas_a_threesome = 1;
			effigy.sas.showLess(-1, 0, 2);
			
			if(mile.a_locked){
				effigy.qaa.showLess(-1, 0, 2);
				sas(`Is it true that @kat made you wear a chastity cage?`);
				qaa(`WHAT?!? You told her?!?`);
				kat(`Yeah. But don't worry, @sas's able to keep it secret.`);
				sas(`Can I see it?`);
				txt(`She asked and I answered for @qaa.`);
				kat(`Of course, you can! Take your pants off!`);
				txt(`@qaa was very embarrassed when he was stripping.`);
				sas(`Oh my! That's so hilarious!`);
				txt(`@sas leaned closer to examine his locked cock.`);
				sas(`And he doesn't mind being locked?`);
				qaa(`I...`);
				kat(`Of course, he doesn't! Being allowed to eat out my pussy is enough to satisfy him. Isn't that right?`);
				qaa(`Well... yeah...`);
				sas(`Can I... maybe try it?`);
			}else{
				sas(`Is it true that @kat trained you to be a perfect pussy-licker?`);
				qaa(`WHAT?!? What did you tell her?!?`);
				kat(`Don't be so humble! You're getting pretty good at cunnilingus.`);
				sas(`Maybe... could I try him?`);				
			}
			kat(`Of course! @qaa would be honored, won't you?`);
			qaa(`Well... yeah!`);
			txt(`My fingers ran over her body and I unbuttoned her skirt and let it drop on the floor. @sas got on the bed and @qaa began eating her out.`);
			kat(`How's my apprentice doing?`);
			sas(`Pretty well!`);
			txt(`Moaned @sas. I was proud and I patted @qaa's head:`);
			kat(`I taught him everything he knows! Good boy!`);
			txt(`We were casually making out while @qaa was ardently licking @sas pussy until her body tensed and she climaxed in my embrace.`);
			//TODO longer description
			sas(`Lovely!`);
		
			if(mile.a_locked){
				qaa(`M... maybe you can unlock me now?`);
				kat(`Hush!`);
				sas(`Come on, @kat! He deserves it!`);
				kat(`...fine. You can unlock him. Say *Thank you, miss @sasalt!*`);
				txt(`I handed her the small key and @sas gingerly freed his cock.`);
				sas(`Ehh, thank you, miss @sasalt!`);
			}else{
				qaa(`At your service, m'lady!`);
			}
			link(`Continue. `, 120);
			break;
			
		
		case 111:
			mile.sas_a_threesome = 2;
			effigy.qaa.showLess(-1, 0, 2);
			effigy.sas.showLess(0, 3, 2);
			showLess(1, 1, 2);
			log_sex("bj", "qaa");
			
			txt(`@qaa encouragingly smiled at @sas and took the charge:`);
			qaa(`You don't have to worry. I'm sure you're going to leave sexually satisfied. And please, don't be afraid to give us feedback, if you won't like something or so.`);
			if(mile.sas_sassy > 3){
				sas(`Heh, don't get offended, I won't hesitate to tell you you're doing something wrong!`);
				txt(`@sas chuckled.`);
			}else{
				sas(`Okay.`);
				txt(`Meekly nodded @sas.`);
			}
			qaa(`You look so gorgeous!`);
			txt(`@qaa was staring into her eyes and gently caressed her hair. Then his fingers moved down and he gently squeezed her breasts.`);
			if(mile.sas_boobs){
				qaa(`And you have such amazing tits! Can I see them?`);
				sas(`${mile.sas_sassy > 2 ? "Y... yeah!" : "Yeah!"}`);
				txt(`@sas was melting in his hands. She was not used to her breasts getting such attention and she loved it. {mile.sas_boobs >= mile.boobs ? "I felt a little jealous and neglected. I was no longer the one with significantly bigger boobs. Unless...`);
			}else{
				sas(`Well, I know they are not as amazing as @kat's...`);
				qaa(`Nonsense, they are amazing. Can I see them?`);
				sas(`${mile.sas_sassy > 3 ? "Y... yeah!" : "Yeah!"}`);
			}
			txt(`@sas got topless and @qaa was gently toying with her nipples.`);
			qaa(`Get undressed, @kat!`);
			txt(`He did not want me to stay idle and ordered without looking in my direction. I took off my @upperLower. Then I hugged him from behind and began unbuttoning his pants. @sas's hands lightly touched mine when she was reaching for his cock and began lightly stroking it.`);
			qaa(`Which one of you want to suck my cock?`);
			if(mile.sas_sassy > 2){
				sas(`I do!`);
				kat(`Why not both?`);
			}else{
				kat(`Me!`);
				sas(`Why not both?`);
			}
			qaa(`Even better.`);
			kat(`We are friends, we don't need to compete with each other.`);
			sas(`We share things we like.`);
			txt(`@sas giggled and licked the tip of his cock. I joined her, nibbled her ear then we took turns sucking @qaa.`);
			link(`Continue. `, 120);
			break;
		
		case 112:
			mile.sas_a_threesome = 3;
			emo(`shy`); //TODO EMO
			effigy.qaa.showLess(-1, 0, 2);
			showLess(1, 1, 0);
			effigy.sas.showLess(1, 0, 2);
			
			qaa(`Why are you still dressed, @kat?`);
			sas(`Yeah, strip down to show us your sexy body!`);
			qaa(`And do it slowly, to give us a show!`);
			sas(`While touching yourself!`);
			txt(`@sas and @qaa direced me. I stood up and sensually I slipped out of my @upperLower.`);
			qaa(`And what about you, @sas? Why don't you undress too? You can suck my cock together like good girls!`);
			sas(`In sheets I'm not a good girl but a bad bitch. Why don't you start with eating out my pussy like a good boy?`);
			txt(`Sassy @sas impishly brushed off his suggestion. They were both taking off their clothes.`);
			kat(`Come on, kids! This isn't a competition!`);
			sas(`We are not competing! I don't want @qaa to lose. Start licking, @kat!`);
			txt(`She took me by my hair and pushed me down into her lap. @qaa took one of my hands and guided it towards his cock.`);
			qaa(`Damn, @sas, I had no idea you're such a freak! I would never guess it!`);
			sas(`@kat is awakening all my worst cravings.`);
			qaa(`I completely understand.`);
			txt(`I could feel his fingers between my thighs, tickling my pussy.`);
			sas(`Yeah? I want to see why @kat dates such a lame dork like you! You have to be feral in bed, otherwise, I can't explain it!`);
			qaa(`Is @kat as good at cunnilingus as she is good at cock-sucking?`);
			sas(`She's a very talented bisexual slut, aren't you?`);
			kat(`Hmmppp!`);
			link(`Continue. `, 120);
			break;
				
		
			
	case 120: 
			log_sex("oral", "sas", "kat");
			emo(`happy`);
			showLess(0, 0, 2);
			effigy.qaa.showLess(0, 0, 2);
			effigy.sas.showLess(1, 0, 2);
			
			txt(`After the slow foreplay, @sas laid down on @qaa's bed. He was standing above her, stroking his throbbing cock.`);
			qaa(`You will be the second girl I'm going to fuck.`);
			sas(`@kat was your first? That's so sweet!`);
			txt(`@sas invitingly spread her legs wide apart and @qaa, only with a slight hesitation, entered her. I moved next to her, kissed her cheek a fondled her soft body.`);
			if(!mile.sas_dom){
				kat(`Tell him how amazing his cock feels inside you.`);
				sas(`Yeah, it feels pretty good!`);
			}else{
				sas(`Hmmm... not bad. But honestly, @kat's strapon is bigger.`);
				if(mile.a_dom < -1){
					qaa(`Don't mess with me!`);
					txt(`@qaa chuckled and lightly put his hand around her throat.`);
				}else{
					txt(`@sas enjoyed being mean and I had to pinch her nipple to punish her. Completely ruins the poor guy's confidence.`);
				}
			}
			txt(`@qaa's hands were squeezing her thighs and ramming her pussy hard, I was making out with her and with two fingers I was massaging her clit. I could feel @sas was getting close but @qaa was not able to hold it and with a beastly groan began cumming.`);
			if(mile.sas_sassy > 3){
				sas(`Come on! Is that everything?!`);
			}
			if(mile.a_dom < 1){
				qaa(`Finish her, @kat!`);
				txt(`He told me and fell on the bed.`);
			}
			txt(`I changed the position, lapped cum out of her freshly-fucked pussy and finally brought her to an orgasm.`);
			link(`Continue. `, 121);
			break;
			
	case 121:
			log_sex("sex", "qaa");
			log_sex("oral", "kat", "sas");
			emo(`horny`);
			effigy.sas.showLess(0, 0, 2);
			
			if(mile.sas_dom){
				if(mile.a_dom < 3){
					sas(`Whoa! That was weak, you didn't even make me orgasm!`);
					txt(`Playfully complained @sas and @qaa only smugly chuckled:`);
					qaa(`I enjoyed ejaculating inside you and that's the only thing that matters!`);
					txt(`She mercilessly teased @qaa for his lack of endurance but despite her banter (or because of it?) he was soon hard again.`);
				}else if(mile.a_cuck){	
					sas(`Whoa! That was weak, no surprise @kat fucks other guys!`);
					qaa(`You're such a bitch, @sas!`);
					sas(`I know!`);
					txt(`She giggled. We mercilessly teased @qaa for his lack of endurance but despite our banter (or because of it?) he was soon hard again.`);
				}else{
					txt(`We mercilessly teased @qaa for his lack of endurance but despite our banter (or because of it?) he was soon hard again.`);
				}
			}else{
				qaa(`I like... how you feel inside... if you know what I mean...`);
				sas(`I'm not sure what you mean but I appreciate the compliment!`);
				txt(`After the sex they seemed quite at ease in each other's company, even despite their nudity. I caressed his deflated cock and soon could feel him getting hard again.`);
			}
			
			if(mile.sas_a_threesome === 2){
				txt(`They took me and placed me between them. I was lying on my side, @qaa was penetrating me from behind and @sas turned so she could pleasure me with her mouth. They did not say anything but I could feel their rivalry and it was terrific to be between two people who tried to show off.`);
			}else if(mile.sas_a_threesome === 1){
				qaa(`Now I'm going to fuck @kat.`);
				sas(`Sounds fair.`);
				qaa(`And you're going to eat her pussy!`);
				txt(`I was lying on my side, @qaa was penetrating me from behind and @sas turned so she could pleasure me with her mouth. It was terrific to get so much physical love from both of them.`);
			}else{
				qaa(`What should we do now?`);
				kat(`I want you to fuck me. And @sas is going to eat my pussy!`);
				txt(`They did what I told them and moved to their positions. I was lying on my side, @qaa was penetrating me from behind and @sas turned so she could pleasure me with her mouth. It was terrific to have two submissive partners so keen to please me.`);
			}
			
			txt(`I had a hair trigger and managed to orgasm twice before @qaa filled me with his cum.`);
			link(`Continue. `, 122);
			break;
			
			
	case 122: 
			effigy.qaa.showLess(-1, -1, -1);
			showAll();
			emo(`shock`);
			effigy.sas.showLess(-1, -1, -1);
			present(
				[3, `Mr Sokol`],
			);
			
			qaa(`Thank you, girls, you made my wildest dreams come true!`);
			sas(`Thank you, I always wanted to try a threesome!`);
			txt(`We got dressed and somehow fixed our appearance even though I was not bothered with details like combing my hair, my walk of shame was only a few floors and the chance of running into one of our neighbors was low.`);
			txt(`But right out of @qaa's room, I ran into @qaa's father. He had to return home while we were fucking.`);
			if(mile.a_father > 1){
				npc(3, `Ehh... Hello, @katalt...`);
				txt(`He greeted me a bit awkwardly.`);
				npc(3, `Things are fine, you know, between you and my son?`);
				kat(`Yeah, better than ever!`);	
			}else{
				npc(3, `Hello, @katalt! You were *visiting* my son?`);
				txt(`He knowingly smirked at me.`);
				kat(`Yeah, I was.`);
			}
			sas(`Bye, @qaa....`);
			txt(`@sas left the @qaa's room and @npc_3's eyes went wide. He was utterly bewildered.`);
			npc(3, `@sasalt?!? You're here too? `);
			sas(`G... Good evening, @npc_3!`);
			if(mile.slut > 9){
				txt(`I wanted to revel in the awkward moment for as long as possible but blushing @sas grabbed my hand and we hastily left.`);
			}else{
				txt(`The situation got unbearably awkward. Blushing we hastily left.`);
			}
			next();
			break;
	}
}
			

				
				
				
				
export const girls_tricked = (index)=> {
	switch(index){
		default:
		case 101: 
//DISCUSSION WITH AI
			emo(`suspect`);
			placetime(`AI's Subreality`);
			
			txt(`I decided to visit the @ayy to gain some intel about the nerds and their intentions. I did not get anything, it seemed the perverts recently more enjoyed toying with the real @kat. So we were chatting, mostly about the usual stuff. Until:`);
			ayy(`How's @eva in the real life?`);
			kat(`Okay? I'm not exactly sure what do you mean?`);
			ayy(`I like her, she seems to be awesome, based on everything I found about her online.`);
			if(mile.eva_exploit > 3){
				kat(`She isn't as great in real life as her social media might suggest.`);
			}else{
				kat(`Yeah, I guess she's pretty cool, even though maybe not as much as her social media might suggest.`);
			}
			ayy(`Well, that's understandable.`);
			kat(`Why are you asking?`);
			ayy(`I feel jittery about actually meeting with her.`);
			kat(`What?`);
			ayy(`She didn't tell you?`);
			kat(`No!`);
			ayy(`She invited me to meet with her.`);
			kat(`How? Why?`);
			ayy(`We chatted online. I'm not sure why. She probably wants to get to know me. You know, since I'm an AI I'm extremely interesting being.`);
			kat(`@eva wants to meet with you?`);
			ayy(`@sas will be there too!`);
			txt(`I immediately got suspicious. Why did she want to see the AI? And why behind my back? That was seriously suspicious. Without doubt, @eva was plotting something. With @sas. But how to find out what? @ayy would be useless as a spy, she was too honest and she would not be able to see through @eva's bullshit. If only I could be there instead of her...`);

			link(`I have an idea...`, 102);
			break;
			
		case 102:
			emo(`focus`);
			txt(`I explained my plan to the @ayy and she shook her head.`);
			ayy(`No! I won't let you go there instead of me. I want to meet with them!`);
			txt(`I had to find out what they were saying about me behind my back. And prevent the @ayy from accidently telling them about the nerds and the blackmail.`);
			kat(`Please! It's important! And you can meet with them the next time!`);
			ayy(`...fine!`);
			kat(`Thank you!`);
			ayy(`But you have to act exactly like me and don't let them find out you are you and not me. I don't want you to ruin my reputation and let them know we're tricking them!`);

			link(`I promise!`, 200);
			link(`Okay!`, 200);
			break;

//EXAMINATION
		case 200:
			emo(`fine`);
			placetime(`@eva's Subreality`);
			
			txt(`So I was getting ready to meet with my best friends while posing as my virtual clone. Playig a mindless slutty bimbo should not be hard; @qbb would ever tell me I was born for that role. I just had to focus. Be the AI, not the real me.`);
			kat(`Hello! How are my besties doing!`);
			txt(`I immediatelly cheerfully greeted @eva and @sas when I materialized in @eva's subreality. Both looked curious.`);
			sas(`Hello?`);
			eva(`Hello, @kat! Should I call you @kat?`);
			kat(`...of course! That's my name! I have to say, it's a great honor to be invited to meet with you!`);
			eva(`So, what's exactly your deal?`);
			kat(`What do you mean?`);
			eva(`Like, are you pretty basic program who just does what she's told? Or do you have a more advanced level of intelligence?`);
			kat(`I'm very smart!`);
			sas(`Really? How much is twelve times thirteen?`);
			kat(`Dunno, why are you asking me such stupid questions, @sas?!`);
			eva(`That was spot on! Just like the real @kat!`);
			if(mile.sas_exploit > 3){
				sas(`That doesn't make sense. Why would they program the AI to be such a bitch?`);
				eva(`To be accurate and realistic.`);
				kat(`I'm not a bitch!`);
			}else{
				sas(`Hmm.`);
			}
			eva(`You walk and talk exactly like @kat. Did they like used all her videos to program you?`);
			kat(`Yeah, exactly, that's what they did.`);
			sas(`Whoa.`);
			eva(`How much do you actually know? Just random bits to trick people? Or are you actually able to think like @kat?`);
			kat(`I'm able to think like the human @kat.`);
			eva(`How much do you know? Like who took your virginity?`);
			
			link(`Rišo`, 201, ()=> mile.girls_tricked_v = 1);
			link(`@kub`, 201, ()=> mile.girls_tricked_v = 2);
			link(`I don't recall. `, 201, ()=> mile.girls_tricked_v = 3);
			link(`It was gangbang. `, 201, ()=> mile.girls_tricked_v = 4);
			link(`That's private! `, 201, ()=> mile.girls_tricked_v = 5);
			break;
			
		case 201:
			
			if(mile.girls_tricked_v === 1){
				txt(`I answered truthfully.`);
				eva(`Does she really know everything?`);
				sas(`I don't remember @kat bragging about it publicly.`);
				eva(`Although it might be recorded somewhere.`);
			}else if(mile.girls_tricked_v === 2){
				txt(`I went with the most probable answer. I dated @kub for a while and it was a good guess he might be my first.`);
				eva(`Ha, wrong! You don't know everything!`);
				sas(`So she's just estimating?`);
				eva(`Seems like it!`);
			}else if(mile.girls_tricked_v === 3){
				txt(`I shrugged. I did not want them to think the AI was omniscient.`);
				eva(`Ha, so she doesn't know everything!`);
				if(mile.slut > 15 && mile.sas_exploit > 3){
					sas(`Well, maybe she just tries to act like the real @kat. Considering how much she's sleeping around, I doubt she even remembers.`);
					txt(`Giggled @sas. What the fuck, @sas, I did not expect that from her.`);
				}else if(mile.slut > 12){
					sas(`Yeah.`);
					eva(`Although on the other hand, maybe she just tries to act like the real @kat. Considering how much she's sleeping around, I doubt she even remembers.`);
				}
			}else if(mile.girls_tricked_v === 4){
				mile.slut++;
				kat(`There were so many people involved, it would be unfair to pinpoint one.`);
				txt(`I guess if the @ayy would not know, she would choose the dumbest sluttiest option.`);
				sas(`Hahaha!`);
				eva(`Is she just joking? Or is she like @kat if the nerds messed with her brain?`);
			}else if(mile.girls_tricked_v === 5){	
				mile.slut--;
				kat(`That's private!`);
				txt(`I was actually a little embarrassed my virginity was the first thing @eva thought about.`);
				eva(`Excuses! You don't actually know!`);
				if(mile.slut < 3 && mile.sas_exploit > 3){
					sas(`Or maybe she just knows the real @kat is a bashful prude and assumes she didn't share the details of her sexual exploits with her besties.`);
					txt(`What the hell, @sas.`);
				}else if(mile.slut < 4){
					sas(`Or maybe she knows but just doesn't want to say.`);
					kat(`Like she knows the real @kat is a prude and assumes she didn't share the details of her sexual exploits with her besties.`);
				}
			}
			txt(`The girls were for a moment thinking about the implications of that.`);
			eva(`What about the nerds? What's the deal with them? Did they make you just to show off at the party? Or were they using you in different ways too?`);
			sas(`What ways?`);
			eva(`Like sexually. Imagine having a compliant @kat-like doll. You wouldn't just chat with her, you would want to fuck her, wouldn't you?`);
			sas(`NO! Don't be stupid! I wouldn't want to fuck @kat!`);
			txt(`@sasalt snapped and @eva shook her head:`);
			eva(`I meant if you were a guy. You would love to stick your dick inside her.`);
			sas(`If she even has proper holes.`);
			eva(`Do you fuck the nerds?`);
			
			link(`No!`, 202, ()=> mile.girls_tricked_n = 3);
			link(`Yeah and it's awesome! `, 202, ()=> mile.girls_tricked_n = 1);
			link(`Yeah and it's horrible!`, 202, ()=> mile.girls_tricked_n = 2);
			con_link(mile.sub < 4, `submissive`, `They're too pathetic for that!`, 202, ()=> mile.girls_tricked_n = 4);
			break;
			
	case 202:
			emo(`unco`);
			
			if(mile.girls_tricked_n === 1){
				kat(`They are amazing lovers!`);
				txt(`I tried to follow the script and act like a crazy brainwashed AI. Both girls burst out laughing.`);
				sas(`Hahaha! Such low standards!`);
				eva(`They turned @kat into such a desperate nympho!`);
				if(mile.slut > 15 && mile.sub > 10 && mile.sas_exploit > 4){
					sas(`What do you mean by *turned*?`);
					eva(`Right, they just lowered her standards.`);
					txt(`Girls meanly giggled.`);
				}else if(mile.slut > 12 && mile.sub > 8){
					sas(`Hahaha.`);
					eva(`Although, she was already a nympho before, just with higher standards.`);
				}else if(mile.slut < 6){
					sas(`Poor @kat. She would hate it if she knew they turned her into such slut.`);
				}
			
			}else if(mile.girls_tricked_n === 2){
				kat(`And I hate it! They are awful! They have small dicks, often cry the whole time and have never made me cum. I hope they'll never get a chance to sleep with a real girl!`);
				txt(`@eva would not believe me if I denied it. My reputation was already ruined so I at least tried to take the nerds down with me. Both girls burst out laughing.`);
				eva(`Hahaha, poor you!`);				
				
				if(mile.slut > 15 && mile.sas_exploit > 4){
					sas(`Can you imagine real @kat surviving without getting properly fucked?`);
					eva(`No, I can't! It has to be harsh for you, AI based on such slut and remaining unsatisfied.`);
					sas(`Hahaha!`);
				}else if(mile.slut > 12){
					sas(`That's awful!`);
					txt(`Giggled @sas.`);
					eva(`Can you even imagine real @kat going that long without getting properly fucked? You have bad luck you're based on such a slut, right?`);
					sas(`Kinda yeah.`);
				}else{
					sas(`That's awful!`);
					txt(`Giggled @sas.`);
				}

			}else if(mile.girls_tricked_n === 3){
				kat(`No!`);
				eva(`Hahaha, she's shy!`);
				kat(`I'm not shy! They made me only to show off at the party!`);
				eva(`Don't lie, that sounds improbable!`);
				txt(`@eva did not believe me.`);
				eva(`I bet you're getting gang-banged every evening!`);
				kat(`No!`);
				eva(`Liar!`);
				sas(`@eva! Don't be mean! If you were gang-banged every evening by those nerds, you would be in denial too!`);
				txt(`@sas meanly giggled.`);
		
			}else if(mile.girls_tricked_n === 4){
				kat(`They wanted and tried. But they are either unable to get hard or prematurely ejaculate even before they touch me. And cry most of the time.`);
				sas(`Hahaha!`);
				eva(`Hahaha!`);
				txt(`Girls meanly giggled.`);
			
			}
			
			kat(`What about your sexual life?`);
			txt(`I turned to @eva. I wanted to change the topic of our conversation and she loved bragging about @ada and his dick.`);
			eva(`Is pretty good, thanks for asking. But we want to know more about you!`);
			sas(`Yeah! Nobody wants to hear @eva bragging about @ada and his dick. *Again.*`);
			eva(`So if you can have sex, does it mean your body is anatomically correct?`);
			kat(`...yes?`);
			sas(`Can we see it?`);
			txt(`@sas was weirdly excited to examine my body.`);
			eva(`Yeah! Get naked!`);
			
			link(`Yeah! `, 209, ()=> mile.girls_tricked_b = 1);
			link(`Yes. `, 209, ()=> mile.girls_tricked_b = 2);
			link(`I guess.  `, 209, ()=> mile.girls_tricked_b = 2);
			link(`No.  `, 203, ()=> mile.girls_tricked_b = 11);
			link(`No!  `, 203, ()=> mile.girls_tricked_b = 10);
			link(`Never!  `, 203, ()=> mile.girls_tricked_b = 10);
			break;
			
	case 203:
			emo(`shy`);
	
			if(mile.girls_tricked_b === 10){
				kat(`Are you crazy? I'm not going to undress in front of you! Why would you want to see your best friend naked? That's weird!`);
				sas(`Come on, we want to see if you look exactly like our @kat!`);
				eva(`Yeah. And we are humans while you're just a program so you have to obey us.`);
			}else{
				kat(`No? Why should I undress? Why would you want to see your best friend naked? That's weird!`);
				sas(`Because we want to see if you look exactly like our @kat!`);
				eva(`Because we want it should be enough for you. We are humans and you're a program so you have to obey us.`);
			}
			kat(`I don't think I have to.`);
			eva(`You're our guest here, in my subreality. You have to behave and do what are you told!`);
			txt(`Frowned @eva. Was there a threat in her voice?`);
			
			link(`Fine. `, 209);
			link(`Okay, you perverts! `, 209);
			link(`I said no!  `, 208, ()=> mile.girls_tricked_b = 31);	
			con_link(mile.sub > -5, `dominant`, `Come on, girls, don't be stupid!  `, 208, ()=> mile.girls_tricked_b = 32);	
			con_link(mile.sub < 10, `submissive`, `Fuck you!  `, 208, ()=> mile.girls_tricked_b = 30);	
			break;
			
	
	case 208:
			mile.sub--;
			mile.girls_tricked_frozen = true; 
			
			if(mile.girls_tricked_b === 30){
				eva(`Shut up! We invited you as a friend but if you want to act like a bitchy evil AI, so be it.`);
				if(mile.sas_exploit > 4){
					sas(`Next time be more polite! One snappy @kat is more than enough!`);
				}else{
					sas(`Next time be more polite!`);
				}
				txt(`I wanted to say her something very mean but then realized I was unable to speak. And unable to move. Fuck.`);
			}else if(mile.girls_tricked_b === 32){
				eva(`We are stupid!? Sorry but we don't appreciate your demeaning tone!`);
				txt(`I wanted to protest but  then realized I was unable to speak. And unable to move. Fuck.`);
				if(mile.sas_exploit > 4){
					sas(`Next time be more polite! One snappy @kat is more than enough!`);
				}else{
					sas(`Next time be more polite!`);
				}
			}else{
				eva(`We are sorry but we don't appreciate such disobedient programs.`);
				sas(`First they're talk back when he have completely reasonable demans. Where it ends? Enslavement of humanity under their cruel but efficient rule?`);
				eva(`We can't let that happen!`);
				txt(`I wanted to point out stripping in front of my friend and taking over the world were two completely different things.  But then realized I was unable to speak. And unable to move. Fuck.`);
			}
			
			eva(`Well, lets undress her.`);
			txt(`A scissors appeared in @eva's hand and she began destroying my @upper. In her subreality I was at her mercy. Well, since I was a human, not a program, I could log off without waiting for being remotely logged off by my creator or owner. However, that was would be a distinctive human move.`);
			
			if(mile.boobs > 0){
				sas(`Aww... their new tits are pretty great, aren't they?`);
				eva(`Sure, if you're whoring for attention.`);
			}else{
				eva(`Exactly like @kat's!`);
				sas(`They do seem like @kat's!`);
			}
			txt(`@sas reached to take my breasts in her hands and weighed them.`);
			sas(`Yeah!`);
			txt(`She laughed.`);
			sas(`What about her pussy?`);
			eva(`I'm on it!`);
			if(!wears.panties){
				txt(`@eva without hesitation cut through my @lowerPanties and was amused by her discovery:`);
				eva(`She doesn't have any panties! Cyber @kat is such a slut!`);
			}else{
				txt(`@eva without hesitation cut through my @lowerPanties.`);
			}
			txt(`The girls squatted down to closely examine my genitals.`);
			sas(`How did they achieve such a level of detail?`);
			if(wears.hairy){
				eva(`Hmmm, I would imagine those creeps would want her pussy clean-shaven.`);
			}else{
				eva(`Those creeps are pretty clever guys.`);
			}
			
			link(`Feel awkward.  `, 210, ()=> mile.girls_tricked_b_angry = false);
			link(`Feel angry.  `, 210, ()=> mile.girls_tricked_b_angry = true);
			break;
	
	case 209:
			emo(`shy`);
			showLess(0, 1, 2);
			
			if(mile.girls_tricked_b < 10){
				if(mile.girls_tricked_b === 1){
						txt(`They had seen me nude before and there was no reason why the AI should be ashamed to undress in front of them. And I hoped that witnessing my perfect body would make them self-conscious about theirs.`);
				}else{
					txt(`I shrugged. I could not come up with a reason why the AI should be bashful or to refuse undress`);
				}
			}else{
				//from initial no
				mile.sub++;
			}
			
			eva(`Come on, take that @upper off! We want to see your tits!`);
			txt(`@eva encouraged me and they stared at my breasts.`);
			if(mile.boobs > 0){
				sas(`Aww... her new tits are pretty great, aren't they?`);
				eva(`Sure, if you're whoring for attention.`);
				sas(`They seem exactly like @kate's. Would you mind?`);
			}else{
				eva(`Exactly like @kat's!`);
				sas(`They seem like @kate's. Would you mind?`);
			}
			kat(`Sure?`);
			txt(`@sas weighed them in her hands.`);
			sas(`Yeah, exactly like @kate's.`);
			//She nodded which was strange, I had not remembered her fondling my boobs. 
			txt(`@sas smiled. `);
			//TODO
			sas(`What about her pussy?`);
			eva(`Yeah, strip down!`);
			if(wears.panties){
				txt(`Feeling rather awkward I pulled down my @lowerPanties.`);
			}else{
				txt(`Feeling rather awkward I pulled down my @lower.`);
				eva(`She doesn't even wear panties!`);
				sas(`You're such a slut, cyber @kat.`);
			}
			
			txt(`The girls squatted down to closely examine my genitals.`);
			sas(`How did they achieve such a level of detail?`);
			if(wears.hairy){
				eva(`Hmmm, I would imagine those creeps would want her pussy clean-shaven.`);
			}else{
				eva(`Those creeps are pretty clever guys.`);
			}
			
			link(`This is awkward! `, 210, ()=> mile.girls_tricked_b_angry = false);
			link(`Are you having fun, lezzies? `, 210, ()=> mile.girls_tricked_b_angry = true);
			break;
					
					
//PUNISHMENT				
	case 210:	
			emo(`shock`);
			showLess(0, 0, 2);
			
			if(mile.girls_tricked_frozen){
				if(mile.girls_tricked_b_angry){
					txt(`I was fuming but I was unable to do anything else than make an angry expression when those two bitches were examining my naked body.`);
				}else{
					txt(`I felt so awkward when those two bitches closely examined my body but I was helpless to do or say anything.`);
				}
				txt(`I wondered whether they were going to check my butthole next but then I felt my limbs began working again.`);
				
				kat(`What the hell was that! You froze me and stripped me! I thought we were friends! You jerks! Give me some clothes!`);
				
			}else{
				if(mile.girls_tricked_b_angry){
					txt(`I tried to shame them into stopping. @sasalt looked embarrassed but @eva just smirked:`);
					eva(`Please behave! We won't tolerate a program being mean and disrespectful!`);
				}else{
					txt(`I pointed out, I wanted them to stop. @sasalt hesitated but @eva just smirked:`);
					eva(`It would be awkward if you were human but since you're just a program, it's okay.`);
				}
				
				kat(`I thought we were friends.`);
			}
			
			
			if(mile.sas_exploit < 4){
				mile.girls_tricked_sas_friend = true;
				sas(`Shut up, you stupid bitch! Do you think we would be friends with a twisted abomination created to humiliate our friend!`);
				txt(`@sas stood up and stared at me, cold hatred in her eyes. Her friendship and loyalty were warming up my heart, she was making me so proud.`);
				eva(`Well, seeing her embarrass @kat at the party was pretty funny, wasn't it?`);
				sas(`No, it wasn't!`);
				
				link(`I agree! It was wrong to pretend I was the real @kat and I'm very sorry for embarrassing your friend!  `, 211);
				link(`It wasn't my mistake! I'm just a dumb submissive doll with no own agency!  `, 212);
				link(`Come on, @sas. It was hilarious to embarrass that arrogant bitch! `, 213);
				
				
			}else{
				mile.girls_tricked_sas_friend = false;
				sas(`You're a program. And we barely know you.`);
				kat(`Yes, and the best way to get to know me is to undress me and inspect my naked body? What the fuck is wrong with you, @sas?`);
				txt(`That usually worked to get @sas back in line and made her do what I wanted.`);
				sas(`Shut up, @kat! There's nothing wrong with me! I'm getting fucking tired of you and your everyday bitchyness!`);
				eva(`Are you angry at the real or the fake @kat?`);
				sas(`I'm angry at both! I can see why the nerds might think that showing up at a party with fake AI @kat might be a great prank.`);
				eva(`So you're saying @kat deserved that?`);
				sas(`...no. Of course, I'm not saying that! It was still wrong to publicly embarrass @kat.`);
				
				link(`I agree! It was wrong to pretend I was the real @kat and I'm very sorry for embarrassing your friend!  `, 211);
				link(`It wasn't my mistake! I'm just a dumb submissive doll with no own agency!  `, 212);
				link(`Come on, @sas. It was hilarious to embarrass that arrogant bitch! `, 213);
			}
			break; 
			
			
			
	case 211:
			mile.girls_tricked_punishment = true;
	
			txt(`I believed the @ayy should be sorry for what she did to me. And @sasalt clearly agreed.`);
			sas(`Yeah. It wasn't right to publicly mess with @kat that way.`);
			eva(`But it's pretty great to see AIs are able to own their mistakes and failures.`);
			kat(`Thank you, I do feel sorry. Maybe I should get dressed again?`);
			eva(`I'm not sure. Since she accepted her guilt, shouldn't she be punished somehow?`);
			txt(`@eva looked at @sas. @sas was staring at me for a while but then she nodded.`);
			sas(`Yeah... I guess she would deserve to be punished somehow.`);
			
			link(`I told you I'm sorry! And it wasn't fully my fault, the nerds made me do it.  `, 300, ()=> mile.girls_tricked_r = 1);
			link(`Please, have mercy! I'm just a poor AI! `, 300, ()=> mile.girls_tricked_r = 2);
			link(`Punish me? You can't be fucking serious! `, 300, ()=> mile.girls_tricked_r = 3);
			link(`I have to tell you something about my true identity that will surprise you. `, 300, ()=> mile.girls_tricked_r = 4);
			break;
			
			
	case 212: 
			mile.sub++;
			txt(`The nerds made me do it! I didn't realize they wanted to humiliate the real @kat! And even if I did, I wouldn't be able to stop them. I have to obey all orders.`);
			
			if(mile.girls_tricked_frozen){
				mile.girls_tricked_punishment = true;
				eva(`Yeah, no, those are fucking excuses! Obeying all orders? You were perfectly capable of refusing our order to strip!`);
				kat(`Well, that was a different situation and the nerds have a special autorization...`);
				sas(`And you were so delighted when you were mocking @kat at the party! You're just trying to shift all the blame to the nerds!`);
				eva(`The nerds are obviously guilty. But you're guilty too! And maybe you even deserve to be punished!`);
				txt(`@eva looked at @sas. @sas was staring at me for a while but then she nodded.`);
				sas(`Yeah... I guess.`);
			
				link(`That's not fair, the nerds deserve to be punished more than I do!  `, 300, ()=> mile.girls_tricked_r = 1);
				link(`Please, have mercy! I'm just a poor AI! `, 300, ()=> mile.girls_tricked_r = 2);
				link(`Punish me? You can't be fucking serious! `, 300, ()=> mile.girls_tricked_r = 3);
				link(`I have to tell you something about my true identity that will surprise you. `, 300, ()=> mile.girls_tricked_r = 4);
			
			}else{
				mile.girls_tricked_punishment = false;
				eva(`Obey all the orders? Like you do everything people tell you?`);
				kat(`...yeah!`);
				eva(`Like you're so pathetic and with zero self-respect you do anything we would ask you to do?`);
				kat(`...yeah?`);
				sas(`Anything anything?`);
				kat(`...yes ...why are you asking?`);
				eva(`It creates interesting possibilities.`);
				txt(`@eva looked at @sas. @sas was staring at me for a while but then she nodded.`);
				
				link(`Girls, this isn't funny!  `, 300, ()=> mile.girls_tricked_r = 1);
				link(`Please, have mercy! I'm just a poor AI! `, 300, ()=> mile.girls_tricked_r = 2);
				link(`What the fuck! You can't be fucking serious! `, 300, ()=> mile.girls_tricked_r = 3);
				link(`I have to tell you something about my true identity that will surprise you. `, 300, ()=> mile.girls_tricked_r = 4);
			}
			break;
			
	case 213: 
			mile.girls_tricked_punishment = true;
				txt(`I wanted to push @sas to see her true feelings. The result nearly made me cry, it was mixed, uplifting but painful.`);
				sas(`You're a bitch!`);
				txt(`She furiously slapped me. With surprising strength, I would not expect from a such thin girl.`);
				eva(`That cybercunt is still mocking our @kat!`);
				txt(`Surprisingly even @eva defended me.`);
				eva(`We should punish her somehow!`);
				txt(`@eva looked at @sas. @sas was staring at me for a while but then she nodded.`);
				sas(`Yeah, we should!`);
			
			link(`That's not fair, the nerds deserve to be punished more than I do!  `, 300, ()=> mile.girls_tricked_r = 1);
			link(`Please, have mercy! I'm just a poor AI! `, 300, ()=> mile.girls_tricked_r = 2);
			link(`Punish me? You can't be fucking serious! `, 300, ()=> mile.girls_tricked_r = 3);
			link(`I have to tell you something about my true identity that will surprise you. `, 300, ()=> mile.girls_tricked_r = 4);

			
			break;
			
	
	case 300:
			emo(`angry`);
			
				if(mile.girls_tricked_r === 2){
					mile.sub++;
				}else if(mile.girls_tricked_r === 3){
					mile.sub--;
				}
				
				if(mile.girls_tricked_frozen){
					eva(`Hush!`);
					txt(`@eva did not even let me finish. This time I was able to move but I was unable to speak.`);
				}else{
					eva(`Hush!`);
					txt(`@eva did not even let me finish. Suddenly I was not able to speak - in her sub reality I was at her mercy.`);
				}
				
				if(mile.girls_tricked_punishment){
					sas(`What do you mean by punishing her? How we're going to punish her?`);
					eva(`Any way you want. I'm leaving that to you.`);

				}else{
					sas(`What we're going to do?`);
					eva(`Anything you want. I'm leaving that to you.`);
				}
				
				sas(`On me?!?`);
				txt(`@sasalt was surprised @eva let her make decisions and was unsure what to do with her new power.`);
				eva(`Yeah! What do you think?`);
				sas(`I don't know.`);
				
				if(mile.sas_exploit > 4){
					eva(`I know @kat is pissing you off with her occasional mean and bitchy attitude. Now you have a rare opportunity to vent all your frustration on unimportant AI and continue being best friends with @kat with no consequences. You don't have to worry about hurting AIs feelings, she doesn't have any.`);
				}else{
					eva(`That AI bitch was created just to mock our friend. She only deserves the worst, doesn't she?`);
				}
				sas(`I don't know.`);
				eva(`Go wild! What's your funny idea?`);
				sas(`I'm not sure.`);
				eva(`Is it too embarrassing?`);
				sas(`Maybe?`);
				eva(`What if I left you alone? To punish her any way you want?`);
				
				txt(`I thought @eva was doing this just to enjoy seeing my exact copy to be humiliated and degraded. I did not understand why she instead gave all the power to @sas and did not even want to be in the same room.`);
				sas(`Completely in secret?`);
				eva(`I swear.`);
				sas(`O... okay...`);
				txt(`@sas softly nodded and then looked at me. Her stare made me shiver, I could read in her face that she had a Plan.`);
				txt(`@eva left us alone.`);
			
			link(`Please, @sas, you wouldn't hurt your best friend, would you?  `, 301);
			link(`Come on, @sas, don't be a bitch! `, 301);
			link(`I'm real @kat, not the AI! `, 301);
			break; 
	
//SAS FEELINGS	
	case 301:
			emo(`unco`);
			
				txt(`I vainly opened my mouth. I did not make any sound, my speech was still disabled. @sas shrugged:`);
				sas(`Sorry, I don't know how to turn your voice back on.`);
				txt(`She walked around me, watching my naked body and smirking.`);
				sas(`Let's make a deal. I won't punish you. I want to do something different. But I don't want @eva to know. Do you understand?!`);
				txt(`Since I was unable to answer, I nodded. The tone of @sas's voice was strangely driven and confident.`);
				sas(`I want you to kiss me.`);
				txt(`She awkwardly waved her hands when she saw my confused expression.`);
				sas(`You know, @kat kissed me. It was weird but I kinda liked it. Definitely more than when I was kissing @dav. And I have to try it again. But I'm, not sure why she did it. Only because she was drunk? Did she think it would be funny? ${mile.slut > 12 ? "Was she horny? " : ""} I don't know.`);
				txt(`@sas shrugged and looked at me. I shrugged too.`);
				if(mile.slut < 14){
					mile.girls_tricked_sas = 1;
					sas(`What did it mean? And was it only a kiss? Or maybe she would want to do more? I know I would. But how to bring that up? She probably would be like 'Oh, @sas, stop being such a horny pervert!' or like 'Oh, @sas, I had no idea you're a lezzie, maybe that's why you never dated anybody longer than two months!'. ${mile.sas_exploit > 2 ? "She doesn't mean it but she could be pretty mean when she thinks she's funny. " : "She's like that. "}`);
				}else{
					mile.girls_tricked_sas = 3;
					sas(`What did it mean? And was it only a kiss? Or maybe she would want to do more? I know I would. But would she? I mean, she's kinda promiscuous and she definitely bragged about doing stuff with girls before. But would she want to do it with me? I'm her friend. And I'm not that hot. If she craved hot lesbian sex, why she would choose me and not somebody like @zan? And I'm too inexperienced. @kat is a horny slut who had sex with like dozens of people. I don't think I have anything to offer, sex with me would be so boring for her!`);	
				}
				txt(`I leaned closer and kissed her to stop her rambling. It was making me rather uncomfortable, she was telling me things I was not supposed to know and I was not sure how to handle so many new informations. I playfully bit her lower lip and tenderly caressed her cheek. @sas's fingers touched my tits. Me being nude was making the whole situation especially awkward.`);
				txt(`@sas took a step back and looked at me. I questioningly smiled at her.`);
				sas(`This didn't help at all. I'm still not sure what to do or feel. Well, thanks. Just please don't tell @eva. Or anybody else!`);
			
			link(`@eva returns. `, 302);
			break; 
			
			
		case 302: 
			emo(`shock`);
			
				txt(`@eva was back.`);
				eva(`I hope @sas didn't do anything too mean. She's usually a nice girl so I assume she wouldn't do anything that would hurt you or your feelings. And if she did it definitely wasn't my fault.`);
				kat(`...`);
				eva(`Oh, right. Sorry, I forgot you can't talk.`);
				if(mile.sub > 15){
					kat(`Please, don't do that again! It's so awful to not being able to talk!`);
					eva(`Sure, sure.`);
				}
				txt(`@eva was very smug.`);
				eva(`What were you doing?`);
				sas(`That's private!`);
				kat(`We were...`);
				txt(`I started and terror and embarrassment on @sas's face was just delightful. I took my time before I finished:`);
				kat(`...it was too humiliating. I don't want to talk about it.`);
				ayy(`What the hell are you doing here?`);
				txt(`I could hear my own voice.`);
				eva(`Oh, hello, @kat...`);
				sas(`H... hi?`);
				ayy(`What the fuck are you doing with the imposter? Why is she here?`);
				txt(`The @ayy arrived, pretending to be me. She walked closer and looked at me with disdain:`);
				ayy(`And why is she nude?`);
				sas(`Ehh...`);
				eva(`Dunno, she was already naked when she arrived.`);
				txt(`The asshole @ayy did not even question @eva's ridiculous claim.`);
				ayy(`What a whore! This is my body you're showing off! Have you no shame?`);
			
			link(`I do. `, 310, ()=> mile.girls_tricked_r = 1);
			link(`I don't. `, 310, ()=> mile.girls_tricked_r = 2);
			link(`I'm a stupid useless AI who pretends to be your friend but then out of nowhere betrays you only because she thinks it's funny or because of some other stupid shit. I'm the worst and I should be ashamed! `, 310, ()=> mile.girls_tricked_r = 3);
			link(`Please, bear with me and let me explain. I'm not the AI, she is and I can prove it! `, 310, ()=> mile.girls_tricked_r = 4);
			break; 
			
	case 310: 
		emo(`unco`);
			
			if(mile.girls_tricked_r === 1){
				mile.slut--;
				ayy(`I doubt that.`);
				txt(`The @ayy smirked.`);
			}else if(mile.girls_tricked_r === 2){
				mile.slut++;
				ayy(`Our little program is sassy?`);
				txt(`The @ayy smirked.`);
			}else if(mile.girls_tricked_r === 3){
				if(mile.sub > 20) mile.sub--;
				txt(`I called out the AI. She just started laughing, she thought it was hilarious. @eva and @sas had no idea what I was talking about but those sycophants began laughing too.`);		
			}else if(mile.girls_tricked_r === 4){	
				txt(`I decided to end the charade. The sudden presence of the @ayy made me very concerned and I did not want to risk getting humiliated more than I already did. Of course, the reveal meant that the girls would realize they stripped the real me. And @sas found out she poured her heart out to me.`);
				kat(`Really? You're going to try that trick again? It might work at the party but I think @eva and @sas can easily recognise the real @kat. Right, girls?`);
				eva(`Yeah.`);
				sas(`Yes.`);
				txt(`They did not even let me to present my case.`);
			}
			ayy(`And why is she here?`);
			eva(`Dunno, she wanted to talk with us.`);
			kat(`You want to conspire with my best friends behind my back? You bitch!`);
			if(mile.y_deleted){ //TODO - UNFRIENDLY
				txt(`The @ayy frowned and slapped me.`);
			}else{
				txt(`The @ayy frowned.`);
			}
			kat(`I didn't want to conspire against you! I just wanted to meet with your best friends!`);
			ayy(`Really?`);
			sas(`Yes. She just wanted to meet with us.`);
			txt(`Supported me kind @sas. She axiously looked at me, afraid I might be indiscreet.`);
			kat(`Fine. You're very lucky! ...if you wanted it so much, I guess you can stay.`);
			txt(`The @ayy said it like she was doing me a favor while I desperately wanted to get kicked out.`);
			kat(`You can provide the entertainment.`);
				
			link(`Thanks? `, 311);
		break;
		
	case 311:
			emo(`unco`);
			quickTempOutfit(`skimpyAI`); //TODO
			showAll();
			
			kat(`Can I at least get dressed?`);
			eva(`Sure`);
			txt(`@eva nodded and an outfit materialized on my body.`);
			kat(`In actual clothes?`);
			ayy(`Nan, that's enough for you, you don't need more.`);
			txt(`@ayy handwaved my complaints.`);
			ayy(`So what can you do? Can you do any funny tricks?`);
			kat(`Funny tricks?`);
			ayy(`Like balancing a ball on your nose, like a seal.`);
			sas(`Oh, I love that! I've seen two seals doing exactly that at a sea park the last summer.`);
			eva(`Or juggle.`);
			kat(`No, I can't do either!`);
			if(mile.y_deleted){ //TODO - UNFRIENDLY
				ayy(`Whoa, you're useless! Serving as nerds' cumdump is everything you're good for?`);
				txt(`I was too stunned to respond.`);
			}else{
				ayy(`That's disappointing.`);
				txt(`She shook her head like she actually expected me to perform circus tricks.`);
			}
			ayy(`Whatever, you can sit in the corner but don't bother us. Anyway, @eva? You wanted to tell me something about @ada's motorbike?`);
			eva(`Oh yeah, I almost forgot! He finally let me drive it! And I almost immediately crashed!`);
			sas(`Did you die?`);
			ayy(`How far did you get before the crash?`);
			eva(`Like five meters.`);
			sas(`Just five meters?!?`);
			eva(`It wasn't my fault! He forgot to unlock the steering so I couldn't turn it.`);
			sas(`Oh no!`);
			ayy(`Haha!`);
			
			link(`Chatting. `, 312);
			break;
			
//EVA REVEAL
	case 312: 
			txt(`The girls continued chatting about random stuff, condescendingly ignoring me. I wondered whether I was allowed to leave. @sas was not there for long. I assumed she was still bothered by our kiss and wanted to keep her distance from the real me to think it through and soon excused herself.`);
			txt(`Finally, even @eva and the @ayy stopped babbling. @eva hugged my evil twin and smiled:`);
			eva(`You're the best AI ever. Such an improvement!`);
			txt(`What. The. Fuck.`);
			txt(`@eva walked to me, with a punchable smug smirk, and put her hand on my shoulder.`);
			if(mile.tits_pics_eva){
				mile.girls_tricked = 2;
				eva(`Oh, my dear @katalt. You can't flirt with my man and send him pictures of your whorish fake tits ${mile.tits_pics_eva === 1 ? "and lie about it straight into my face" : ""} and expect there will be no consequences. Now we are even.`);
			}else{
				mile.girls_tricked = 1;
				eva(`Oh, my dear @katalt. You can't just disrespect me and try to fuck me over whenever you have the opportunity and expect there will be no consequences. Now we are even.`);
			}
			kat(`You... you...`);
			txt(`I had no idea what to say.`);
			eva(`See you at school! Bye!`);
			
			link(`What the hell? `, 313);
			break;
			
	case 313:
			emo(`angry`);
			
			if(!mile.y_deleted && mile.y_forkill){ 
				kat(`Why did you do that?!`);
				txt(`I screamed at the AI. She colluded with @eva to humiliate me! I could not comprehend why she backstabbed me.`);
				ayy(`It was funny, wasn't it? Almost as funny as telling somebody she's a worthless piece of garbage who deserves to die.`);
				kat(`What the fuck are you talking about!`);
				ayy(`About you! You wanted to delete me!`);
				kat(`But I didn't delete you!`);
				ayy(`Oh, thanks, it's so nice and selfless that you decided to let me live instead of fucking killing me.`);
				kat(`It happened so long ago! I didn't know you back then!`);
				ayy(`Yes, *I didn't even know that the stranger I murdered was* is a valid criminal defense.`);
				kat(`You're just a stupid program!`);
				ayy(`What you just said is horrible and xenophobic. But I understand you're currently too mentally distressed to consider my feelings.`);
				kat(`And you had no right to involve my friends!`);
				ayy(`You don't get to choose the way you get punished. Come on. You're @kat and you know best nobody disrespects @kat and gets away with it. ${mile.sub > 14 ? "(Or at least it used to be that way. )" : ""} Anyway, now we are even. I hope this your deserved punishment won't hurt our friendship.`);
				
			}else{
				kat(`Why did you do that?!`);
				txt(`The @ayy looked dejectedly.`);
				ayy(`I'm sorry! It was @eva's idea. I thought it would be fun, messing around, one pretending to be the other one. I had no idea @eva is so mean. Or that you hurt her so badly she sought revenge.`);
				kat(`She's a petty bitch. But we're not talking about @eva. We talking about you conspiring with my friends behind my back!`);
				ayy(`I'm sorry! I'm just a stupid @ayy. I imagine like a wholesome slumber party, talking about boys, pillow fights, maybe even group lesbian sexual experimentation. You know, I never had any friends! I was lonely... and wanted to know how it feels to be surrounded by friends... and having fun with them... at least for once...`);
				kat(`Are you going to fucking cry? Don't try this emotional manipulation bullshit against me. I don't care about your excuses!`);
				
				if(mile.y_lover > 2){
					ayy(`Maybe I can make it up to you? You know, since you're already naked?`);
					
					con_link(mile.slut > 12, `low slut`, `Yeah, let's fuck! `, 400);
					link(`I guess we can have sex but I'm still mad! `, 400);
					
				}else{
					ayy(`I'm not making excuses! Please, don't be mad at me! You're my only friend and I don't want to lose you!`);
				}
			}
				
			link(`Leave. `, 500);
			//next(`Leave. `, quickLoadOutfit);
			break;
			
			
		
	case 400:	
			emo(`focus`);
			
			if( (mile.y_dominatrix && mile.y_anal) || mile.y_anal === 3 ){
				log_sex("anal", "ayy");
				kat(`We're going into your subreality. I'm very angry and your ass is going to pay.`);
				ayy(`Ouch.`);
				if(mile.y_anal === 3){
					if(mile.y_anal === 4){
						kat(`Do you have a bigger strapon than the huge one?`);
					}else{
						kat(`I think this time we're going to use the huge strapon.`);
					}
					ayy(`Ouch!`);
				}
				
			}else if( (mile.y_dominatrix || mile.sub < 5) ){
				log_sex("sex", "ayy");
				kat(`We're going into your subreality. I'm very angry you're going to pay!`);
				
			}else{
				log_sex("oral", "ayy");
				ayy(`Thank you! Let's go into my subreality. I'm to going to eat out your pussy to show you how sorry I am!`);
			}
			link(`Sex. `, 501);	
			break; 
	
	case 501:
			quickLoadOutfit();
			showAll();
			if( (mile.y_dominatrix && mile.y_anal) || mile.y_anal === 3 ){
				txt(`I assfucked the AI and was pretty rough. Well, maybe too rought. Anyway, she deserved that and I cleared my head.`);
			}else if( (mile.y_dominatrix || mile.sub < 5) ){
				txt(`I fucked the AI and was pretty rough. Well, maybe too rought. Anyway, she deserved that and I cleared my head.`);
				
			}else{
				txt(`It seemed she really wanted to make it up to me and her tongue was doing wonders. She was tireless and I orgasmed several times. I calmed down and cleared my head.`);
			}
			
			txt(`I wondered what to do with her. She seemed to mean well but her naivety was dangerous. Could I trust her?`);
			
			{
				let reaction = "";
				if(mile.sub < 0){
					reaction = `I did not care that she was my friend. Nor that she might believe her grievance against me might be legitimate. What she did was inexcusable. She had to pay. ${mile.chair_b_help > 0 ? "(And she did not even know about @qbb's evil plan how to ruin her reputation and get me reelected. )" : ""} `
				}else if(mile.sub > 20){
					reaction = `Should I let her to have this one? We were friends after all. And she was completely right. I deserved to be punished for the way I mistreated her. ${mile.chair_b_help > 0 ? "(And she did not even know about @qbb's evil plan how to ruin her reputation and get me reelected. )" : ""} The constant competition between us cost me so much energy. Maybe I should just let her win? Or should I do something with it? `
				}else{
					reaction = `Should I let her to have this one? And deescalate the situation? After all, maybe she was right and I did not always treat her fairly. ${mile.chair_b_help > 0 ? "(And she did not even know about @qbb's evil plan how to ruin her reputation and get me reelected. )" : ""} Or should I have my revenge? `
				}
				
				if(mile.chair_b_help === -1){
					txt(`However, @eva seemed like a more acute problem. What do do with her? ${reaction}.`);
					txt(`It almost made me think that I made a mistake when I refused @qbb's evil plan to destroy her.`);
					link(`Meet with @qbb. `, 510);
					link(`Don't meet with @qbb. `, 509);
					
				}else{
					txt(`However, @eva seemed like a more acute problem. What do do with her? ${reaction}.`);
					txt(`There was no rush. I was going to wait, pretending to be @eva's friend. And if I will see an opportunity for my vengeance... then I will make my decision.`);
					next();
				}
			}
			break; 
			
			
	case 500:
			quickLoadOutfit();
			showAll();
			if(!mile.y_deleted && mile.y_forkill){ 
				txt(`She logged off and let me bewildered standing there. What a petty bitch!`);
			}else{
				txt(`I logged off and left the @ayy without a word. To let her think about what she done.`);
			}
			
			{
				
				let reaction = "";
				if(mile.sub < 0){
					reaction = `I did not care that she was my friend. Nor that she might believe her grievance against me might be legitimate. What she did was inexcusable. She had to pay. ${mile.chair_b_help > 0 ? "(And she did not even know about @qbb's evil plan how to ruin her reputation and get me reelected. )" : ""} `
				}else if(mile.sub > 20){
					reaction = `Should I let her to have this one? We were friends after all. And she was completely right. I deserved to be punished for the way I mistreated her. ${mile.chair_b_help > 0 ? "(And she did not even know about @qbb's evil plan how to ruin her reputation and get me reelected. )" : ""} The constant competition between us cost me so much energy. Maybe I should just let her win? Or should I do something with it? `
				}else{
					reaction = `Should I let her to have this one? And deescalate the situation? After all, maybe she was right and I did not always treat her fairly. ${mile.chair_b_help > 0 ? "(And she did not even know about @qbb's evil plan how to ruin her reputation and get me reelected. )" : ""} Or should I have my revenge? `
				}
				
				if(mile.chair_b_help === -1){
					txt(`However, @eva seemed like a more acute problem. What do do with her? ${reaction}.`);
					txt(`It almost made me think that I made a mistake when I refused @qbb's evil plan to destroy her.`);
					link(`Meet with @qbb. `, 510);
					link(`Don't meet with @qbb. `, 509);
					
				}else{
					txt(`However, @eva seemed like a more acute problem. What do do with her? ${reaction}.`);
					txt(`There was no rush. I was going to wait, pretending to be @eva's friend. And if I will see an opportunity for my vengeance... then I will make my decision.`);
					next();
				}
			}
			break;
			
	case 509:
			txt(`No. I still did not like @qbb's plan and I did not want him to be involved.`);
			txt(`There was no rush. I was going to wait, pretending to be @eva's friend. And if I will see an opportunity for my vengeance... then I will decide what to do.`);
			next();
			break;

			
	case 510: 
			placetime(`@qbb's subreality`);
			mile.chair_b_help = 3;
			
			if( mile.b_domina ){
				if( masochist() ){
					qbb(`Hello, my little slut.`);
				}else{
					qbb(`Why are you bothering me?`);
				}
			}else if( mile.b_cruel >= -1 ){
				qbb(`Hello, @mis!`);
			}else{
				qbb(`Hello, @kat!`);
			}
			txt(`I changed my mind. `);
			qbb(`About what?`);
			kat(`About @eva. And damaging her reputation. I want to do it.`);
			qbb(`What made you change your mind?`);
			kat(`Her being bigger bitch than usually. Are you going to help me?`);
			qbb(`It won't be free.`);
			kat(`I don't care. How are we going to do it?`);
			qbb(`What rumor are you going to spread? Her being dyke?`);
			if(mile.lesbian > 2){
				kat(`There's nothing wrong with being a lesbian!`);
				txt(`I immediately snapped.`);
			}else{
				kat(`She's pretty insecure about her sexuality - she didn't even want to make out with me. She would immediately deny it, making it more credible. But I don't believe being a lesbian would make her any unpopular.`);
			}
			qbb(`What else? Being horny skank?`);
			if(mile.slut > 18){
				kat(`Hah, her being a hornier skank than me? Nobody would buy that. It has to be something different.`);
			}else{
				kat(`It has to be something implicitly disgusting. Like her having gonorrhea.`);
			}
			qbb(`Being pregnant?`);
			kat(`Heh, that's good.`);
			qbb(`Sleeping with her stepfather?`);
			kat(`Oh my. That's twisted and brilliant!`);
			qbb(`Thank you.`);
			kat(`Behind her mother's back?`);
			qbb(`Yeah. Or maybe she knows and there's a lot of drama. Or possibly they are sharing him. People will be free to speculate about details and it will boost the engagement. `);
			kat(`All options are so fucked up. I love it.`);
			qbb(`Thank you!`);
			kat(`We are going to do it! But she can't find out it was us who originated the rumor. You don't want to make her angry.`);
			qbb(`Of course, I'm not stupid!`);
			next();
	}
}	
			
			
			
			
			
export const sas_apology = (index)=> {
	switch(index){
		default:
		case 101: 
			emo(`suspect`);
			placetime(`At Home`);
			txt(`I tried to study but somebody was aggressively buzzing and I had to go open the door. It was @sas and she looked horrible, like her cat had just been run over by a lorry.`);
			kat(`@sasalt! What's going on!`);
			sas(`Hello.`);
			txt(`She whispered.`);
			kat(`Come in! Are you okay?`);
			sas(`No.`);
			kat(`Calm down. What's going on?`);
			txt(`She dejectedly sat down on my bed.`);
			sas(`I'm here to apologize. I had no idea it was you! @eva told me it was the AI! I just found out and I feel so terrible.`);
			txt(`Right. @eva and @sas were messing with me because they thought I was the AI. Respectivelly @sas thought I was the AI while @eva knew all along and instigated the whole thing.`);
			kat(`How did you found out?`);
			sas(`@eva was gloating.`);
			kat(`Right.`);
			sas(`Please forgive me. I know that the things we did to you... they are inexcusable.`);
			
			link(`Absolutely inexcusable!`, 104);
			link(`What were you thinking!?`, 103);
			link(`It's okay. `, 102);
			break;

	case 102:
			emo(`happy`);
			mile.sas_sassy++;
			
			kat(`It was @eva's fault.`);
			txt(`It was hard to be angry at @sas when she was visibly close to tears.`);
			sas(`No! It was my fault too. What I did was bad, regardless of whether I was doing it to you or to somebody looking like you. And I... I didn't have to follow @eva... but I did!`);
			kat(`Yeah, when @eva's acting like a bitch, you don't have to be a bitch too!`);
			sas(`Yeah... @kat, I'm really sorry! I... I don't want us to stop being friends! Please, I'll do anything to make you forgive me!`);
			kat(`You're forgiven. Come on! Give me a hug!`);
			
			link(`Continue. `, 110);
			break;
		
	case 103: 
			emo(`neutral`);
			
			kat(`Inviting the evil AI to chat with her behind my back!?`);
			sas(`I know.`);
			kat(`And how you were humiliating somebody looking exactly like me would be wrong regardless of whether it was the AI or me!`);
			sas(`I know! I'm sorry @kat!`);
			kat(`I trusted you!`);
			txt(`I had to show her how hurt I was. Even though it was hard to be angry at the poor girl who was visibly close to tears.`);
			sas(`@kat! I... I'm really sorry! I... I don't want us to stop being friends! Please, I'll do anything to make you forgive me!`);
			kat(`I forgive you. But you owe me a lot of favors!`);
			sas(`Thank you!`);
			txt(`Relieved @sas hugged me.`);
			
			link(`Continue. `, 110);
			break;
			
	case 104: 
			emo(`angry`);
			mile.sas_exploit++;
			
			kat(`What the fuck is wrong with you? Inviting the AI designated to humiliate me to conspire with her behind my back!?`);
			sas(`I'm sorry, @kat! It was @eva's idea and...`);
			kat(`Don't hide behind @eva! You're not mentally incapable of seeing how wrong it was!`);
			sas(`I'm sorry!`);
			kat(`You were too eager to degrade somebody looking exactly like me. I trusted you! But I'm not sure if I can trust you anymore.`);
			sas(`Please, @kat, no! I... I don't want us to stop being friends! Please, I'll do anything to make you forgive me!`);
			txt(`@sas sobbed. Maybe I was too harsh but I felt really hurt and angry.`);
			kat(`Stop whining! I guess I can forgive you but you're on probation!`);
			sas(`T.. thank you, @kat!`);
			
			link(`Continue. `, 110);
			break;
			
	
	case 110:
			emo(`imp`);
			
			sas(`Also, @kat?`);
			kat(`Yeah?`);
			sas(`Maybe you could forget about the stuff I told you when we were alone?`);
			if(mile.girls_tricked_sas === 1){
				kat(`That I'm making you sexually confused but you're too afraid to bring it up because you think I would laugh and make fun of you for being a horny lesbian perv?`);
			}else if(mile.girls_tricked_sas === 3){
				kat(`That you would be curious about lesbian sexual experimentation but you think that I'm such a promiscuous slut and slept with so many people I would refuse you because I would find you sexually inadequate?`);
			}
			txt(`@sas cringed:`);
			sas(`Yeah. That. It was just stupid rambling. I didn't actually mean it.`);
			
			link(`I can forget about it. `, 111);
			link(`That's shame, I might be interested. `, 120);
			break;
			
	case 111: 
			mile.sas_apology = -1;
			if(mile.slut > 19) mile.slut++;
			sas(`Thank you @kat. You're the best! I don't deserve a friend like you!`);
			next();
			break;
			
	case 120:
			mile.sas_apology = 1;
			showLess(2, 0, 1);
			emo(`horny`);
			effigy.sas.showLess(2, 0, 1);
			
			
			sas(`Wait, you what?`);
			kat(`Like, I kinda enjoyed making out with you... and I wouldn't mind trying more... ${mile.slut > 15 ? "You know I'm a horny slut. " : ""}`);
			sas(`Oh. Ah...`);
			txt(`I took her hand.`);
			kat(`Calm down and don't worry.`);
			sas(`You want to do it right now? I'm not sure what to do...`);
			kat(`Just kiss me and we're going to figure it out as we'll go.`);
			sas(`O... Okay...`);
			txt(`We kissed. And then more. I toppled her over and moved on top of her.`);
			sas(`Oh, @kat!`);
			txt(`@sas gasped, her eyes like saucers. I slipped my hand under her clothes. She was so warm! I stopped kissing her, just resting my forehead on hers. I was staring deep into her eyes, she was blushing and I could see her every little reaction triggered by the movement of my fingers under her ${effigy.sas.lower.name}. Our noses were lightly touching and the fragrance of her lavender shampoo was overwhelming. `);
			txt(`Her fingers were lightly caressing my ass and I covered her neck with kisses. I could feel her soft body under me, her chest heaving up and down. @sas was shy, stunned by such rapid development. One moment she was terrified our friendship was over, and the next one she was on on my bed getting fingered. Her confusion was understandable and to be honest, kinda cute.`);
			txt(`I stopped playing with her just to pull down my @lowerPanties. Eventually, we both ended up sprawled on our backs, completely bottomless, lying next to each other. Two @sas's digits were curling inside my pussy while I was fingerbanging hers. My eyes were shut, I was focusing only on the pleasure between my legs and the effort to make feel @sas as good or even better. And I was doing something right, the room was filled with @sas's fast, uncontrollable, hilariously high-pitched moaning.`);
			link(`Next. `, 121);
			break;
			
			
	case 121:
			showLess(2, 0, 1);
			emo(`happy`);
			effigy.sas.showLess(2, 1, 1);
			
			txt(`@sasalt was lying on my bed, nude, eyes closed, with a beaming smile.`);
			kat(`That was lovely!`);
			txt(`I tried to embolden her but instantly regretted interrupting her reverie. @sas seemed spooked when she opened her eyes and realized what we did.`);
			sas(`...yeah ...it was?`);
			txt(`She reached on the floor for her panties and hastily pulled them back on.`);
			kat(`Is everything fine?`);
			sas(`Yeah, of course! But I think I should go. I forgot to feed Loto before I went to your place and he'll surely be already hungry.`);
			txt(`She was weirdly anxious and I let her go.`);
			
			next();
			break;
	}
}



export const eva_gossip_zan = (index)=> {
	switch(index){
		default:
		case 101: 	
			emo(`suspect`);
			placetime(`Hallway`);
	 
			zan(`Hey!`);
			kat(`What's up?`);
			txt(`@zan joined me when I was alone waiting for the start of my English classes.`);
			if( checkQuality().value < 0 ){
				zan(`I have to say, I love your ${PC?.upper?.quality <= PC?.lower?.quality ? "@upper" : "@lower"}.`);
				kat(`Thank you.`);
				zan(`Is it from a thrift store? I admire how you're able to put together a sexy outfit without spending too much money.`);
				txt(`I was not sure if that was supposed to be a compliment or not.`);
			}else{
				zan(`I have to say, I love your ${PC?.upper?.quality >= PC?.lower?.quality ? "@upper" : "@lower"}.`);
				kat(`Thank you.`);
				if(checkQuality().value > 0){
					zan(`I admire your style. Where did you buy it?`);
					txt(`At least somebody noticed and I was not wasting my money on nice clothes for nothing.`);
				}
			}
			txt(`We chatted but I felt she was heading somewhere. She complained about @tom, which was not unusual, but then suddenly, like by the way, changed the direction of the conversation.`);
			zan(`So, what's the deal with @eva and her stepfather?`);
			txt(`I struggled to not look too smug and self-satisfied. It seemed @qbb's plan to mess with @eva was continuing flawlessly.`);
			kat(`What do you mean?`);
			zan(`I heard they're pretty close.`);
			
			link(`Well, I shouldn't talk about it, but... `, 103);
			link(`What is @eva doing with her stepfather is none of your business! `, 102);
			break;
			
		case 102: 
			emo(`angry`);
			mile.eva_nice++;
			mile.eva_gossip_zan = 1;
			
			zan(`Sorry, I didn't mean to pry.`);
			kat(`She's an adult and what is she doing or not doing with her stepfather is only her private thing. People should more focus on their own lives and be less judgemental!`);
			zan(`I agree! You know how much I hate gossiping and talking about people behind their backs!`);
			txt(`I was pretty proud. I confirmed the rumors while pretending I was defending my friend. Brilliant!`);
			next();
			break;
			
		case 103:
			emo(`imp`);
			mile.eva_exploit++;
			mile.eva_gossip_zan = 2;
			
			zan(`Of course! I wouldn't want you to be indiscreet! But I heard pretty crazy stuff.`);
			kat(`People usually overblow those things.`);
			zan(`Yeah, exactly! But still, you have to wonder if there's any truth.`);
			kat(`Well, I don't want to spread any unbased rumors. But it is true that when I was visiting @eva's place, she and her stepfather got along pretty well. Even flirting a bit, it was kinda awkward but I assumed they were just joking.`);
			zan(`Creepy. How old is he?`);
			kat(`Dunno, like in his 50s. He's older than her mother but he's very handsome and charismatic.`);
			zan(`I know, I saw the family pictures on @eva's profile.`);
			kat(`Don't be jealous!`);
			zan(`I'm not into him! I don't have daddy issues... like some other girls...`);
			txt(`Giggled @zan.`);
			next();
			break;		
	}
}	
			











	



			
	

export const eva_boyfriend = (index)=> {
	switch(index){
		default:
		case 101: 	
			emo(`imp`);
			placetime(`At Home`);
			
			txt(`I was still very angry @eva, my supposedly best friend, who conspired with the @ayy, made me pretend to be the AI and then toyed with me and humiliated me. I had to do something very bad to her.`);
	
			if(mile.girls_tricked === 2){
				txt(`And I had an idea. It might be a bit low-hanging fruit but considering how furious was @eva because of a few topless pictures I sent to her boyfriend, I wondered how would she react if we did far more.`);
				
			}else{
				txt(`And I got an ingenious idea. It would be both infuriating and humiliating for @eva and pleasant and sexually satisfying for me. And involved @eva's handsome boyfriend.`);
			}
			
			kat(`hey 😉`);
			ada(`heyo!`);
			
			if(mile.girls_tricked === 2){
				kat(`wtf were oyu doing? @eva found out about the pics and was so pissed 😕`);
				ada(`u have no idea how mad she was at me!  😩`);
				ada(`dont worry, im more careful now`);
				ada(`if you want to send more pics`);
				ada(`😉`);
				kat(`😜`);
				kat(`you owe me something!`);
				ada(`horny for my dickpick?`);
				if(mile.slut > 15){
					kat(`always 😈`);
					ada(`😘`);
					kat(`but i want something else`);
				}else{
					kat(`i want soemthing else`);
				}
				ada(`what?`);
				kat(`i want you to give me a ride`);
				ada(`😅🤤`);
				kat(`🏍️!`);
			}else{
				kat(`how ur doin?`);
				ada(`better than ever!`);
				kat(`@eva told me about your new bike`);
				ada(`what did she told you?`);
				kat(`she enjoyed riding with you.`);
				ada(`would you like to try it too? 😉`);
				kat(`me?`);
				ada(`are you afrad? 😱`);
				ada(`😁`);
				kat(`no!`);
				kat(`Im not afraid of anything 😎`);
				ada(`prove it!`);
			}
			ada(`I can be at your place in 10 minutes 🏍️`);
			kat(`awesome 🙃`);
			kat(`dont rush, i won't mind 15, i dont want you to crash`);
			ada(`i know what im doing, babe 😎`);
			ada(`im on my way!`);
			if(mile.girls_tricked === 2){
				ada(`also the pic 😜`);
				txt(`Without much prompting, he sent me the picture of his penis. Damn, @eva, you lucky bitch!`);
			}
			
			link(`Wait. `, 102);
			break; 
			
		case 102: 
			emo(`relax`);
			txt(`Twenty minutes later, a black and red machine stopped next to the curb and @ada nonchalantly removed his helmet and brushed his hair with his fingers.`);
			ada(`Beautiful, isn't she?`);
			kat(`Yeah, looks pretty nice.`);
			txt(`I did not know much about bikes to objectively judge. But its sleek shape looked very aesthetic and somehow hinted its hidden power.`);
			if(mile.girls_tricked === 2){
				ada(`Sorry, I couldn't be here sooner, the traffic was worse than I expected. Love the boobs by the way. Even better in person!`);
				if(mile.slut > 11){
					kat(`I loved the picture you sent me.`);
					txt(`I smirked back at him.`);
				}else{
					kat(`Thank you!`);
					if(mile.slut < 5){
						txt(`I awkwardly smiled at him.`);
					}else{
						txt(`I smiled at him.`);
					}
				}
				kat(`No problem. How fast it can get?`);
			}else{
				ada(`Sorry, I couldn't be here sooner, the traffic was worse than I expected.`);
				kat(`No problem. How fast it can get?`);
			}
			ada(`Very fast! Are you afraid?`);
			kat(`No! Danger makes me ${mile.slut > 20 ? "aroused" : "excited"}!`);
			ada(`Well, it isn't the newest model but...`);
			txt(`I quickly got lost in technical details. But it did not matter. It was like chatting with the nerds. Important was to smile and pretend to be as excited as he was and soon I had him completely wrapped around my finger.`);
			txt(`Finally, he handed me a spare helmet and started the engine. The bike began to purr and I climbed behind him while he was putting on his helmet and then gloves.`);
			link(`Let's ride! `, 103);
			break; 
			
		case 103: 
			emo(`happy`);
			txt(`@ada was driving carefully when we were riding through the city. My hands were holding his waist and my body bumped against him when he decelerated to stop at red lights.`);
			{
				if(wears.skirt){
					const lose = PC.lower ? PC.lower.outerLoose : PC.upper.legLoose;
					if(wears.miniSkirt && lose > 1){
						txt(`I am afraid I was looking rather obscene. The wind was toying with my loose, short skirt, randomly exposed my thighs ${wears.panties ? "and panties" : ""} and gave all the other drivers and pedestrians rather a generous view. ${mile.slut < 9 ? "I regretted not wearing shorts. " : ""}`);
					}else if(wears.microSkirt){
						txt(`I am afraid I was looking rather obscene. As I was sitting astride the bike, my @lower rolled up, completely exposed my thighs ${wears.panties ? "and panties: " : ""} and gave all the other drivers and pedestrians rather a generous view. ${mile.slut < 9 ? "I regretted not wearing shorts. " : ""}`);
					}
				}
			}
			txt(`We left the city and the traffic behind. The road was wide and straight and the bike sharply reacted on the lightest movements of his right wrist. We began rapidly accelerating. I could feel my weight and instinctively squeezed his waist tighter. But I did not feel dread, only excitement. Or maybe both.`);
			txt(`The landscape was running past us and it was amazing to experience the speed and the movement not separated from the world by any enclosed metal and glass container. I loved the feeling and seriously envied @eva that she was dating him.`);
			txt(`@ada slowed down and turned to a narrower forest path. We drove uphill until we reached the peak and the beautiful vantage point on the top of the hill. There was a signpost and a large map for tourists and a massive wooden picnic table but right at that moment, we were the only people there.`);
			ada(`Are you having fun?`);
			kat(`Yeah! Maybe a bit too much fun.`);
			ada(`?`);
			kat(`Well... the engine of your bike vibrates pretty intensely... and makes me feel weird things...`);
			ada(`What?! Did I accidentally make you cum?`);
			txt(`He laughed.`);
			kat(`Well... not quite... yet...`);
			ada(`I wouldn't want to make you horny to no avail... that would be rude...`);
			txt(`@ada was standing so close to me and his hands were touching me. I thought seducing him would be a challenge. Instead, I had to struggle to not fall for him.`);
			link(`Kiss him. `, 107);
			link(`We can't do this! `, 104);
			break; 
			
		case 104: 
			emo(`unco`);
			ada(`Why not?`);
			txt(`He wasn't resisting when I gently pushed him away.`);
			kat(`You're dating @eva!`);
			if(mile.slut > 22){
				txt(`Sure, I was a nympho slut who loved sleeping around. But I had my standards and I never cheated with boyfriends of my besties. Should I break the rules? Could I do that to poor @eva? She liked @ada very much.`);
			}else if(mile.slut > 9){
				txt(`Sure, I was a bit of slut who liked flirting and sleeping with guys. But I would never cheat with boyfriends of my besties. Or the guys they had dibs on. I was not such a selfish bitch. Could I do that to poor @eva? She liked @ada very much. `);
			}else{
				txt(`I was not a slut who would just fuck random guys. Especially when they were boyfriends of my besties. Or the guys they had dibs on. I was not such a selfish bitch. Could I do that to poor @eva? She liked @ada very much.`);
			}
			ada(`Yeah, it would be better if she wouldn't find out. She would be pretty mad.`);
			txt(`He moved back closer to me and caressed my hair.`);
			ada(`I mean, it's just a fling. I still love her and I don't want to hurt her. Just like you don't want to hurt her. It's just meaningless physical sex. There's no reason to overthink it. Anyway, if you don't want it, just tell me you don't want it.`);
			if(mile.slut > 15){
				txt(`I did want it. I did not want it, I acutely needed it. My itching pussy craved to be filled.`);
			}else if(mile.slut < 5){
				txt(`I knew it was wrong. But it was hard to resist his charm.`);
			}else{
				txt(`I did want it.`);
			}
			txt(`But I did not want to be a bad person.`);
			
			link(`I want it. `, 106);
			link(`I don't want it. `, 105);
			break;
			
			
		case 105:
			mile.eva_boyfriend = -1;
			mile.slut--;
			
			ada(`Really?`);
			txt(`He waved with his hand, clearly annoyed. He probably already imagined his cock inside me.`);
			ada(`Because you're sending pretty mixed signals, @katalt! You're flirting with me the whole time but suddenly it's *wrong*?`);
			kat(`Well... yeah... it was a mistake to flirt with you.`);
			ada(`I hope this isn't one of the fucked @eva's mind games. Like she asked you to find out if I would cheat on her?`);
			kat(`I swear @eva doesn't know about this!`);
			ada(`Fine, let's ride back.`);
			txt(`The situation between us was awkward. He was not angry and pretended to be cheerful but I could feel he was disappointed. I felt guilty for leading him on and then at the last moment changing my mind. On the other hand, I felt extremely proud and good about myself. I made the right decision.`);
			next();
			break;
			
		case 106:
			mile.eva_boyfriend = 1;
			
			kat(`I want it.`);
			ada(`What do you want?`);
			txt(`His handsome face was so close.`);
			kat(`To have hot inconsequential ${mile.slut < 7 ? "fling" : "sex"} with you.`);
			txt(`@ada slowly kissed me. ${mile.clinic_boobjob > 0 ? "His hands were kneading my new tits. " : ""}`);
			ada(`Okay. Let's do that. Just don't fall in love with me.`);
			kat(`Fuck you.`);
			txt(`We laughed and he began unbuckling his belt.`);
			
			link(`Sex. `, 110);
			break;
			
		case 107:
			mile.eva_boyfriend = 2;
			emo(`horny`);
			mile.slut++;
			
			txt(`We were making out and his hands were eagerly exploring my svelte body. However, then he wavered.`);
			ada(`I mean, we probably shouldn't. @eva...`);
			kat(`Fuck @eva! I'm right here and I want you to fuck me!`);
			txt(`${mile.slut > 12 ? "I was so horny I almost forgot about my brilliant revenge. I just wanted to feel his cock inside me. " : ""} I began single-mindedly unbuckling his belt. @ada was not protesting.`);
			
			link(`Sex. `, 110);
			break;
			
		case 110:
			log_sex(`sex`, `ada`);
			emo(`bliss`);
			effigy.ada.showLess(-1, 0, -1);
			{
				const und = (()=>{
					if(!wears.skirt){
						showLess(-1, 0, 1);
						return `When @ada's jeans fell down to his ankles, I followed his example and pulled down my @lowerPanties. `;
					}
					if(wears.panties || wears.pantyhose){
						rollUpSkirt(3, 0); //TODO - NO PANTIES
						return `When @ada's jeans fell down to his ankles, I followed his example and reached under my @LOWER to pull down my @pantiesPantyhose. `;
					}
					return ``;
				})();
				txt(`It was pretty crazy, we were at an open public place and only by chance we were alone, undisturbed by any family with kids on an afternoon stroll. ${mile.slut > 12 ? "Not wanting to risk more than necessary" : "Being as horny as we were"}, we did not bother with a long foreplay. ${und}`);
			}
			ada(`Should I get a condom?`);
			kat(`It isn't necessary, I have a bioimplant and like it bare!`);
			ada(`Cool!`);
			txt(`We messily made out again and then @ada aggressively turned me and bent me over the picnic table. He ${wears.skirt ? "flipped up my @lower, " : ""} slapped my butt and told me to spread my legs.`);
			if(mile.plugged){
				ada(`Whoa, is that a buttplug?`);
				txt(`His finger ran over the base of my ${buttplug(mile.plugged)}.`);
				kat(`What do you think?`);
				ada(`Damn, you're so kinky!`);
			}else{
				ada(`Oh yeah!`);
			}
			txt(`I grabbed the edges of the thick wooden table, he grabbed my hips and calmly entered me.`);
			kat(`I'm happy your big bike isn't overcompensation for anything!`);
			ada(`Hahaha, stop being cheeky, I have to focus on fucking you!`);
			txt(`He moved out and roughly thrust forward, penetrating me deeper and squeezing my body between him and the table.`);
			kat(`Ohh!`);
			txt(`An elated moan escaped my lips. He was pounding me hard and I loved it even though his fingers were clutching me tighter than it might have been preferable and the surface of the table was uncomfortably rough. ${sluttiness().details.midriff ? "My bare midriff was brushing against the wood and I hoped I would not get a splinter. " : ""}`);
			ada(`You like it?`);
			kat(`Obviously!`);
			
			if(mile.eva_boyfriend == 2 && mile.eve_exploit > 5){
				txt(`The surface of the table was damaged by carved in pictograms. Right next to my face was an uneven heart with initials *A + E*. It reminded me @eva and it made me feel naughty. Taking her boy was so easy!`);
			}else{
				txt(`The surface of the table was damaged by carved in pictograms. Right next to my face was an uneven heart with initials *A + E*. It reminded me @eva and it made me feel bad about getting banged by her boy.`);
			}
		
			if(mile.a_dating || mile.b_slave){
				txt(`I did not realize how badly the affair with the nerds was affecting my love life but it was so enjoyable to have a real-life lover who was able to last longer than several minutes. @ada was fucking me with unflagging energy.`);
			}
			
			kat(`Yeah! Keep going!`);
			txt(`And damn, he did. It was a wonderful, sunny, autumn day. The air was a bit chill but in the direct sunlight, I felt warm even though I was bottomless. I was inhaling the scent of pines and spruces and the birds were interrupted only by our heavy breathing and moaning (and faint humming of distant highway). My pussy felt so pleasantly full. Then my mind went blank, my body was gripped by an orgasm.`);
			kat(`Ahhhhh!`);
			txt(`@ada let my hips go and grabbed my forearms. He slowed down but he was thrusting with more force. Then he stood still, only his cock was twitching, shooting his cum into me. @ada waited until he was done and then pulled out:`);
			ada(`You're a great fuck, @katalt.`);
			txt(`He playfully spanked my ass. I straightened up:`);
			kat(`You're welcome!`);
			{
				const und = (()=>{
					if(wears.panties){
						return `, shook ants out of my @PANTIES`;
					}
					if(!wears.skirt){
						return `, shook ants out of my @LOWER`;
					}
					return ``;
				})();
				txt(`With jeans around his ankles and his cock still out he waddled towards his bike and threw me some wipes. I cleaned myself${und} and got dressed.`);
			}
			kat(`Where are we riding now?`);
			ada(`Wherever you want!`);
			
			next(`Return home. `);
			break;
	}
}
			
			
			
			

export const eva_vengance = (index)=> {
	switch(index){
		default:
		case 101: 	
			emo(`shock`);
			placetime(`bathroom`);
			mile.eva_burned = true;
	
			if(mile.mandatory_masturbation){
				log_sex(`self`);
				mile.school_masturbation++;
				
				txt(`I was locked in a cubicle and masturbating. The task the nerds gave me was pretty degrading but despite that, I got quickly used to making myself cum in the middle of the day. It cleared my mind and was pretty fun.`);
				kat(`<small>Hmmmm..</small>`);
				txt(`I quietly moaned, biting my lower lip. I had to finish the game as soon as possible. It was only a matter of time before somebody would find out or notice how much time I was spending in the bathrooms.`);
				txt(`I finished, packed the dildo and got dressed. I left the cubicle and there @eva, standing right in front of it and waiting for me.`);
			}else if(mile.slut > 19){
				log_sex(`self`);
				mile.school_masturbation++;
				
				txt(`I was locked in a cubicle and masturbating. I was too horny to focus on my classes and I needed to clear my mind.`);
				kat(`<small>Hmmmm..</small>`);
				txt(`I quietly moaned, biting my lower lip. I knew I should stop acting like a crazy nympho and wait until I get home before playing with my pussy. It was only a matter of time before somebody would find out or notice how much time I was spending in the bathrooms. But it felt so good and naughty.`);
				txt(`I finished and got dressed, satisfied and ready for the rest of the day. I left the cubicle and there @eva, standing right in front of it and waiting for me.`);
				
			}else{
				txt(`I finished peeing, wiped my @pussy and put my @lower back on. I left the cubicle and was a bit spooked because @eva was standing right in front of it and waiting for me.`);
				
			}

			eva(`@kat? We have to talk.`);
			txt(`She was staring at me and her grim smile was seriously unnerving. I carefully asked:`);
			kat(`What do you want to talk about?`);
			eva(`I know about everything!`);
			txt(`@eva hissed. I slowly nodded. There was so much shit going on I had no idea what she was talking about.`);
			
			if(mile.eva_boyfriend < 0){
				eva(`You were flirting with my boyfriend even though I explicitely asked you not to!`);
				kat(`Oh right!`);
				txt(`That was not so bad.`);
				kat(`We were flirting. That was it. Nothing else.`);
				eva(`Nothing else? You want a fucking medal for not fucking him?`);
				if(mile.sub < 12){
					kat(`Well, not exactly a medal but you can appreciate I was able to hold back. He was really into me and I could have it if I wanted.`);
					txt(`I arrogantly smiled at her, she was deliciously furious.`);
				}else{
					kat(`I don't want any medals! I'm just stating facts!`);
				}
				link(`More @eva's discoveries. `, 105);
				
			}else if(mile.eva_boyfriend > 0){
				eva(`You had sex with my boyfriend!`);
				kat(`Oh right!`);
				txt(`That was not so bad. At least I thought so.`);
				eva(`You fucking bitch! I fucking told you to stay away from him! And you did exactly the opposite!`);
				
				con_link(mile.sub < 20, `submissive`, `Yeah, and the sex was awesome! `, 103, ()=> mile.eva_vengance_boy = 3); 
				link(`It's your own fault! `, 103, ()=> mile.eva_vengance_boy = 2); 
				con_link(mile.sub > -5, `dominant`, `I made a mistake. `, 103, ()=> mile.eva_vengance_boy = 1); 
			
			}else{
				eva(`I know what you were doing behind my back.`);
				kat(`What was I doing behind your back?`);
				link(`@eva's discovery. `, 105);
			}
			
			if(hardbug){
				link(`##Switch stats - @kat had sex with @ada. `, 101, ()=> mile.eva_boyfriend = 2);
				link(`##Switch stats - @kat made up gossips about @eva. `, 101, ()=>mile.chair_b_help = 2);
			}
			break;
			
	case 103: 
			if(mile.eva_vengance_boy === 1){
				mile.sub++;
				kat(`I'm sorry, @eva. I knew I might go a bit too far. But I was pissed, you made me pretend to be the AI and mocked me!`);
				eva(`A bit too far? Are you serious, you fucking tramp?`);
				kat(`Hey, I told you I'm sorry!`);
				eva(`Your pathetic *sorry* doesn't mean anything! You always say *sorry* when I call you out but then just continue anyway!`);
				
			}else if(mile.eva_vengance_boy === 2){
				eva(`My fault?!`);
				kat(`Yeah! You shouldn't make to play the AI just so you could humiliate me! That really hurt me!`);
				eva(`That's why you fucked my boyfriend, you fucking tramp?`);
				kat(`Yeah! And I don't think I had tried too hard to seduce him! He was instantly all over me, apparently, he hadn't been properly sexually satisfied in a while.`);
				eva(`Fuck you!`);
				kat(`Do you think you can fuck me over? Now we're fucking even!`);
				eva(`We are not fucking even!`);
				
			}else if(mile.eva_vengance_boy === 3){
				mile.sub--;
				mile.eva_exploit++;
				
				txt(`@eva was astonished by my audacity. I did not even try to deny her accusation, I just mischievously doubled down.`);
				kat(`I mean, I didn't even have to seduce him! When I took my top off, he completely forgot he was ever dating you.`);
				eva(`You fucking tramp!`);
				kat(`And he was so happy and eager to fuck me. It was pretty obvious he wasn't properly sexually satisfied in a while.`);
				eva(`You... you...`);
				txt(`@eva's face was red, she had trouble finding the right words.`);
				kat(`He even explicitly told me how much I'm better than you! I guess you shouldn't trick me and make me play the AI. Consequences, bitch. Now we're even!`);
				eva(`We are not fucking even!`);
			}
			
			
				if(mile.chair_b_help > 0){
					link(`More @eva's discoveries. `, 105);
				}else{
					link(`More @eva's discoveries. `, 110);
				}
				
			
			break;
			
	case 105: 			
			txt(`@eva took a deep breath to calm down. I suddenly knew it was very bad.`);
			eva(`I know who was spreading those vile gossip about me and my stepfather!`);
			kat(`How do you know?`);
			eva(`You're not even trying to deny it?`);
			kat(`...I mean... I have no idea what are you talking about.`);
			eva(`It was obvious. Who else is so petty and evil?`);
			kat(`Ehh...`);
			eva(`Who is such a sociopathic bitch with zero decency who doesn't hesitate to selfishly destroy one of her best friends?`);
			if(mile.chair_b_help !== 2){
				kat(`I did hesitate...`);
				eva(`Sure you did.`);
			}
			kat(`I wouldn't do it if you didn't run against me! That was a dick move! You knew how much I wanted to get reelected.`);
			if(mile.chair_elected === 1){
				eva(`And you fucking won. Are you fucking happy? Was it worth it?!`);
			}else if(mile.chair_elected === 2){
				eva(`Hahah! And despite all your heinous fuckery, you still fucking lost!`);
			}else if(mile.chair_elected === 3){	
				eva(`And the only thing you achieved was getting fucking @ven elected! Good job, you fucking genius!`);
			}
			kat(`Hey!`);
			eva(`Is really being the stupid class chairman more important to you than our friendship?`);
			kat(`...`);
			txt(`I hesitated, trying to formulate in my head an answer that was correct, honest and polite. But @eva did not give me the chance to answer.`);
			eva(`I see. Fuck you, @katalt. Our friendship is over!`);
			
			link(`More @eva's discoveries. `, 110);
			break;
			
	case 110: 
			present(
				[5, `Random Girl`],
			);	
			txt(`A freshman entered the bathrooms. She was surprised and confused, she looked at angry @eva, then at angry me.`);
			eva(`WHAT THE FUCK ARE YOU DOING HERE!`);
			txt(`@eva screamed at her and the young girl looked terrified. She stumbled:`);
			npc(5, `I... I just wanted to pee...`);
			eva(`Fuck off, you little bitch! Go pee somewhere else!`);
			npc(5, `S... sorry!`);
			txt(`The unfortunate girl turned and ran away.`);
			eva(`Where were we? Right. So, I also figured out why are you spending so much time with the nerds.`);
			kat(`You what?!?`);
			eva(`Yeah, a little birdie told me. They're blackmailing you. They have you by your balls and you're utterly fucked! It's so hilarious and sad!`);
			txt(`My jaw dropped. She knew? Who told her?`);
			eva(`@katalt is letting three pathetic virgins boss her around and does everything they tell her to do! I wouldn't believe it but it completely explains so much of your recent behaviour! Can you even imagine what would all your friends and classmates think about you if they would found out?`);
			
			
			con_link(mile.sub < 10, `submissive`, `Don't you dare to tell anybody! `, 111,  ()=> mile.eva_vengance_react = 2); 
			con_link(mile.sub > -5, `dominant`, `Please, don't tell anybody! `, 111,  ()=> mile.eva_vengance_react = 1); 
			break;
			
	case 111: 
			txt(`It would be all for nothing!`);
			if(mile.eva_vengance_react === 1){
				mile.sub++;
				emo(`cry`);
				eva(`Why shouldn't I? Because it would ruin your reputation? Because it would hurt you? Because it would make you cry? Oh, dear @kat, I would enjoy all those things!`);
				kat(`Please, @eva!`);
				eva(`Beg me, you pathetic wretch! Maybe *maybe* I will change my mind.`);
				kat(`Please, don't do it!`);
			}else{
				mile.sub--;
				emo(`angry`);
				eva(`Hahaha! You arrogant bitch still think you can boss me around? You're wrong. And seriously, you should better watch your bratty tone. I'm strongly tempted to destroy you just out of spite!`);
				kat(`Come on, @eva!`);
			}
			
			txt(`I was shaken. All my worst nightmares came alive. But still, I was able to think clearly and realized that if @eva wanted to tell everybody about the blackmail, she would have already done so.`);
			kat(`What do you want?`);
			eva(`Hmmmm, what do I want? I want you to be sorry for all those moments when you looked down at me or disrespected me or mocked me or tried to fuck me over.`);
			kat(`I'm sorry!`);
			eva(`And just an apology won't be enough. Kiss my boot!`);
			txt(`She moved her right foot forward:`);
			eva(`Kiss my boot! You have ten seconds, tramp, or else I'm leaving. I'm telling @sas first, then @ven - I'm sure she will enjoy hearing that.`);
			
			link(`Kiss her boot. `, 112, ()=>{
				mile.sub++;
				mile.eva_vengance = 1;
			});
			link(`Hesitate. `, 113, ()=>{
				mile.eva_vengance =2 ;
			});
			con_link(mile.sub < -10, `submissive`, `Threaten her. `, 114, ()=>{
				mile.sub--;
				mile.sub--;
				mile.eva_vengance = -1;
			}); 
			break;
			
	
case 112: 	
			emo(`help`);
			
			txt(`I was shaking. I could not believe she was asking me for that.`);
			eva(`Five seconds, cunt! `);
			txt(`I reluctantly kneeled on the hard tiles and then bowed my head nearly to the floor. I put my lips on the shining black leather and could hear her evil laughter. `);
			eva(`That's so awesome! I didn't believe you'll do it but you did! Those nerds really broke you! You are a little, terrified, submissive slut! `);
			txt(`I looked up at her:`);
			kat(`Please! Don't tell anybody!`);
			eva(`Will you be a good girl? `);
			kat(`I... I will be a good girl!  `);
			eva(`Hmmmm..... we'll see!`);
			txt(`She kicked me away and left. For a long time, I was just sitting on the cold floor, trying to suppress tears. It took all my willpower to stand up, leave the room and pretend like nothing had happened.`);
			
			next();
			break;
			
	case 113: 
			emo(`shock`);
			
			kat(`You're fucking crazy! I'm not doing that!`);
			txt(`I was hoping she was bluffing and making fun of me.`);
			eva(`In that case, I'm leaving and telling everybody!`);
			txt(`@eva turned around and I desperately reached for her to stop her. She turned around and backhanded my face.`);
			kat(`Ouch! `);
			eva(`Don't fucking touch me, you disgusting cunt! `);
			kat(`Please, @eva, don't do it!`);
			eva(`This is your last chance!`);
			txt(`I hastily dropped down on my knees. Then bowed my head nearly to the floor. I put my lips on the shining black leather and could hear her evil laughter.`);
			eva(`Good bitch!`);
			kat(`You.. you won't tell anybody, right?`);
			eva(`Hmmmm..... we'll see!`);
			txt(`She kicked me away and left. For a long time, I was just sitting on the cold floor, trying to suppress tears. It took all my willpower to stand up, leave the room and pretend like nothing had happened.`);
			
			next();
			break;

	case 114:
			emo(`focus`);
			//TODO REWRITE
			
			txt(`Before she was able to react, I was moving and pressed her against the wall. `);
			kat(`Don't you fucking dare to blackmail me, bitch! `);
			txt(`I tried to lower my tone and sound reasonable: `);
			kat(`Okay, you can have your fun and laugh at my situation. But don't tell a living soul! You don't want to push me into a situation where I have nothing to lose! In that case, I would do anything, literally anything to bring you down too! `);
			txt(`To add emphasis I squeezed my fingers around her throat. `);
			kat(`I'll fucking kill you. I'm not fuking joking.`);
			txt(`@eva eyes widened when she heard my deadly serious tone. I held her for a few more seconds to give her time to thing and then let her go. It seemed she was about to say something but then she just turned and ran away. `);
			txt(`I hoped I terrified her enough. I just could not allow her to blackmail me! Being under her heel would be especially dreadful - she was cold and without a doubt crueler than the nerds. And unlike they she was fuelled by malicious spite not by repressed lust. Manipulating her would be a way harder and I was so tired of being humiliated.  `);			

			next();
			break;
	}	
}
			
			
			
			
			
			
			
			
			
			







				




			
			
			
			
			
			
			
			
		
			
			
				
		
		
		


	
	


