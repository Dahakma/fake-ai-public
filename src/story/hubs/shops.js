import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects, choices_required} from "Loop/engine";
import {txt, bold, hint} from "Loop/text";

//import {recesses} from "Taskmaster/recesses";
import {next_event, name_timeslot, add_today} from "Taskmaster/scheduler";
import {new_optionals} from "Taskmaster/new_optionals";

import {capitalise} from "Libraries/dh";
import {placetime} from "Loop/text_extra";
import {dildo, strapon} from "Virtual/text"; //TODO

import {time} from "System/time";
import {shops} from "Data/items/shops";
import {INVENTORY, updateDraw, showLess, showAll, cloneFigurine} from "Avatar/index";

import {DISPLAY} from "Gui/gui.js";

import {money} from "Data/items/prices";
import {payment} from "System/bank_account.js";


import {inks} from "Story/tasks/tattoo.js";
import {jewels} from "Story/tasks/piercings.js";


const dildopolis = [];
function check_dildopolis(){
	dildopolis.length = 0;
	

//DILDOS
	function dildify(index, condit = true, foo = ()=>{}){
		if(!condit) return;
		if(mile[`dildo_${index}`]) return; //already owned
		
		dildopolis.push({
			name: dildo(index),
			price:  money.dildo[index],
			fce(){
				//mile.dildo_second = mile.dildo; //TODO - redundant
				if(index > mile.dildo) mile.dildo = index;
				mile[`dildo_${index}`] = true;
				foo();
			},
		})
	}

	if(mile.dildo || mile.video_dildo || mile.pink_dildo){ //unlocked by buying dildos or video with dildo tasks
		dildify(1);
		dildify(2);
		dildify(3);
		dildify(4, mile.slut > 4); //big purple
		dildify(5, mile.slut > 9); //beastly
		dildify(6, mile.slut > 15); //dragon
		dildify(7, mile.slut > 25); //horse
	}


//BUTTPLUG 
	if(mile.buttplug < 2 && ext.stats.plugged > 4) { //got buttplug & wears it
		dildopolis.push({
			name: `bigger steel buttplug`,
			price: money.buttplug[1],
			fce(){
				mile.buttplug = 2;
				add_today("afternoon2", "medium_buttplug");
			},
		})
	}
	

//CHASTITY CAGE
	let need_cages = 0;
	if(mile.b_dominatrix  && !mile.b_locked) need_cages++;
	if(mile.a_dom > 3 && !mile.a_locked) need_cages++; //TODO - STRICTER CONDITIONS!
	need_cages = need_cages - mile.chastity_cages;
	
	for(let i = need_cages; i > 0; i--){
		dildopolis.push({
			name: `male chastity cage`,
			price: money.chastity_cage,
			fce(){
				mile.chastity_cages++;
			},
		})
	}
	

//STRAPONS
	if(
		mile.y_y_strapon > 0 //tried virtual strapon with ayy
		|| mile.sas_date > 0 //doing lesbian stuff with sas 
		|| mile.b_dominatrix //played dominatrix to qbb
		|| mile.a_dom > 3 //qaa is very sumbissive to dominant kat
	) {
		if(!mile.strapon_1){
			dildopolis.push({
				name: strapon(1),
				price: money.strapon[1],
				fce(){
					mile.strapon_1 = true;
					if(1> mile.strapon) mile.strapon = 1;
				},
			})
		}
		if( (mile.sub < -6 || mile.sub > 11) && !mile.strapon_2 ){
			dildopolis.push({
				name: strapon(2),
				price: money.strapon[2],
				fce(){
					mile.strapon_1 = true;
					if(2 > mile.strapon) mile.strapon = 2;
				},
			})
		}

		if( (mile.sub < -12 || mile.sub > 17) && !mile.strapon_3 ){
			dildopolis.push({
				name: strapon(3),
				price: money.strapon[3],
				fce(){
					mile.strapon_3 = true;
					if(3 > mile.strapon) mile.strapon = 3;
				},
			})
		}
	}
	

	

}
				
				

export const shop_test = (index) => {
	switch(index){
	default:
	case 101:
		Object.keys(shops).forEach( key => {
			link(`${shops[key].icon} ${shops[key].name}`, 102, ()=> {
				INVENTORY.shop(key, ()=> main(101) );
			})
		});
		next();
		break;
	}
}


function vendor(key){
	link(`${shops[key].icon} ${shops[key].name}`, 102, ()=> {
		INVENTORY.shop(key, ()=> main(102) );
	})
}

export const shop_mall = (index) => {
	switch(index){
	default:
	case 101:
		check_dildopolis();
		mile.shop_mall++;
		if(mile.shop_mall <= 2){
			txt(`I took a tram and went downtown to a shopping mall. I was not a zoomer, I enjoyed going to various stores to see what I might like. `);
			txt(`There were many shops: the fancy boutique I enjoyed visiting but I rarely bought anything there; more affordable F&A Fashion and Discount shop and finally the Second-hand which was not only selling dirt-cheap clothes but it was also purchasing them. Then there were more specialized shops for shoes or accessories. `);
		}
		
		if(!mile.shop_lingerie){
			if(ext.rules.sexy_panties || ext.rules.sexy_legwear){
				mile.shop_lingerie = true;
				txt(`Nerds wanted to me wear sexier ${ext.rules.sexy_panties ? "panties" : "legwear"}. As much as was their demand disgusting and humiliating, at least it gave me an excuse to visit a fancier lingerie store. `);
			}else if( (ext.slut > 1 && mile.shop_mall >= 2) || (ext.slut >= 1 && mile.shop_mall >= 3) ){ //TODO!!
				mile.shop_lingerie = true;
				txt(`Today I felt especially naughty. So far I never dared to visit the store with fancier lingerie, it seemed too adult for me. But since I was an adult now, maybe I should give it a try and buy something really sexy? `);
			}
		}
		
		if(!mile.shop_alt && (mile.b_shopping_sub || mile.b_shopping_dom)  ){
			mile.shop_alt = true;
			txt(`Maybe I should try the alternative goth/punk store @qbb showed me? I will have to pay with my own money but on the other hand, I will be free from his creepy presence. `);
		}
		
		if(!mile.shop_bimbo && mile.c_shopping){
			mile.shop_bimbo = true;
			txt(`Was I in a pink, bimbo mood? Maybe I should check the cute shop @qcc showed me. `);
		}
	
		if(!mile.shop_gold && ext.irl_money > money.shop_gold){
			mile.shop_gold = true;
			txt(`Things went pretty well recently. Despite all the hard work and humiliation it took, I had on my account more money than ever before. Maybe I should buy something stupidly expensive? I deserved it and I knew how to make even more money. `);
		}
		
		
		if(!mile.shop_piercing && time.week >= 5 && mile.piercings_2){
			//mile.piercings_2 changed during reaction
			mile.shop_piercing = true;
			txt(`I noticed the Piercing Parlor was open. Maybe I could get another piercings, this time just because I wanted, not because I was forced by the nerds. `);
		}
	
		if(!mile.shop_tattoo && time.week >= 5 && mile.tattoo_2){
			if(head.tomorrow.morningReaction !== 'tattoo_2_reaction'){ 	//todo - stupidly specific 
				mile.shop_tattoo = true;
				txt(`Walking around the tattoo studio made me smirk. A few weeks ago I did not have any tattoos, now I had ${mile.sas_tattoo_kat ? "three" : "two"}. I still was not sure if I should keep them or not. Or maybe I should get even more? `);
			}
		}
			
			
		if(!mile.shop_haircut){
			mile.shop_haircut = true;
			if(head.day > 15){
				txt(`I noticed my hair was getting pretty long. Maybe I should get a haircut? `);
			}else if(mile.qcc_extend_hair){
				txt(`I was thinking about what @qcc said. Maybe I could try to get my hair extended. `);
			}
		}
		
		
		
		//break;
		
	case 102:
		vendor(`boutique`);
		vendor(`fa`);
		vendor(`discount`);
		vendor(`second`);
		vendor(`accessories`);
		vendor(`shoes`);
		vendor(`jewellery`);
		if(mile.shop_lingerie) vendor(`lingerie`);
		if(mile.glasses) vendor(`optician`);
		if(mile.shop_gold) vendor(`gold`);
		if(mile.shop_bimbo) vendor(`bimbo`);
		if(mile.shop_alt) vendor(`alt`);
		if(dildopolis.length){
			link(`🍆 Dildopolis. `, 200);
		}else if(hardbug){
			link(`##🍆 Dildopolis (nothing available).`, 200);
		}
		if(mile.shop_haircut){
			link(`✂️ Hair Salon. `, 300);
		}else if(hardbug){
			link(`##✂️ Hair Salon. `, 300);
		}
		
		if(mile.shop_tattoo){
			link(`💘 Tattoo Studio. `, 400);
		}else if(hardbug){
			link(`##💘 Tattoo Studio. `, 400);
		}
			
		if(mile.shop_piercing){
			link(`♦️ Piercing Parlor. `, 500);
		}else if(hardbug){
			link(`## ♦️ Piercing Parlor. `, 500);
		}		
		
		
		next(`Go home. `);
		
//TODO - SHOULD BE HARDBUG	
		if(hardbug){
			Object.keys(shops).forEach( key => {
				link(`##${shops[key].icon} ${shops[key].name}`, 103, ()=> {
					INVENTORY.shop(key, ()=> main(102) );
				})
			});
		}
		break;
	
//DILDOPOLIS
	case 200:
		dildopolis.forEach( item => {
			link(`${capitalise(item.name)} - ${item.price}€`, 200, ()=> {
				payment(-item.price, "dildo");
				item.fce();
				check_dildopolis();
			})
		});
		link(`Leave. `, 102);
		break;
	

//HAIR 
	case 300:
		link(`Get a haircut - ${money.haircut}€ `, 300, ()=>{}, ()=> DISPLAY.edit_avatar(PC, "haircut", (subject, report)=>{
			if(report) payment("-haircut", "hair");
		}));
		link(`Get a hair extension - ${money.hairext}€ `, 300, ()=>{}, ()=> DISPLAY.edit_avatar(PC, "hair", (subject, report)=>{
			if(report) payment("-hairext", "hair");
		}));
		if(hardbug){
			link(`##Get hair dyed - ${money.hairdying}€ `, 300, ()=>{}, ()=> DISPLAY.edit_avatar(PC, "dye", (subject, report)=>{
				if(report) payment("-hairdying", "hair");
			}));
		}

		link(`Leave. `, 102);
		break;
	
//TATTOO
	case 400:
			if(counter.new_tattoo){
				txt(`I paid ${money.tattoo} Euro for my new ${counter.new_tattoo.slutty ? "slutty" : ""} tattoo. `);
				payment("-tattoo","tattoo");
				delete counter.new_tattoo;
			}
			
			{	
			
				const slutty = mile.tattoo_2_slut || mile.slut >= 10 || mile.sub >= 10;
					
				con_link(ext.irl_money >= money.tattoo, `not enough money`, `Get a tattoo. `, null, ()=> DISPLAY.tattoo({
					slutty,
					slots: inks.all,
					}, PC, undefined, (report)=>{
						counter.new_tattoo = report;
						main(400);
				}));
			}	
			
			if(hardbug){	
				link(`##Get a tattoo (unlocked everything). `, null, ()=> DISPLAY.tattoo({
					slutty: true,
					slots: inks.all_forced,
					}, PC, undefined, (report)=>{
						counter.new_tattoo = report;
						main(400);
				}));
			}	
			
			if(softbug){
				link(`#Remove tattoo. `, 420);
			}
			
			link(`Leave. `, 102);
			break;
			
		case 420: 
			{
				showLess(0, 0);
				Object.keys(PC.apex).filter( key => key.startsWith(`tat_`) ).forEach( key => {
					link(`${key} - ${PC.apex[key]}`, 420, ()=> {
						delete PC.apex[key];
						updateDraw(PC);
					});
				})
				link(`Back. `, 400, showAll);
			}
			break;
			
			
//PIERCING	
		case 500:
			link(`Get pierced. `, 501);
			link(`Buy jewellery. `, null, ()=> INVENTORY.shop("piercing") );
			link(`Leave. `, 102, showAll);
			break;
			
		case 501: 
				counter = {}; //BEWARE
				
				jewels.second();

				choices_required(0, 3);
		
				link(`Done. `, 500, ()=>{
					Object.keys(counter).forEach( a => {
						if(counter[a]) PC.apex[a] = true 
					});
					if(counter.nipplesMulti){ //behaviour unexpected anyway (it assumes being expansion of existing "nipples"), this way it is in players benefit
						counter.nipples = true;
						PC.apex.nipples = true;
						updateDraw(PC);
					}
				});
	
			break;
			
		
		
	}
	
}



export const shop_online = (index) => {
	switch(index){
		default:
		case 101:
				check_dildopolis();
				//break;
				
		case 102:
			link(`Order Custom Clothes`, null, DISPLAY.designer);
			if(dildopolis.length){
				link(`🍆 Dildopolis. `, 200);
			}else if(hardbug){
				link(`##🍆 Dildopolis (nothing available).`, 200);
			}
			next(`Exit. `);
			break;

		case 103:
				//
				break;
				
		case 200:
			dildopolis.forEach( item => {
				link(`${capitalise(item.name)} ${item.price}€`, 200, ()=> {
					payment(-item.price, "dildo");
					item.fce();
					check_dildopolis();
				})
			});
			link(`Leave. `, 102);
			break;
	
	}
}

