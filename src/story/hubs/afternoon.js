import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, bold, hint} from "Loop/text";
import {placetime} from "Loop/text_extra";

//import {recesses} from "Taskmaster/recesses";
import {next_event} from "Taskmaster/scheduler";
import {new_optionals} from "Taskmaster/new_optionals";

import {capitalise} from "Libraries/dh";






export const afternoon = (index) => {
	switch(index){
	default:
	case 101:
		set.irl();
		
		placetime(undefined, `Afternoon`);
		
		next(`Procrastinate. `);
		new_optionals().forEach( a => chapter(`${a.name}.`, a.event, a.scene, () => {
			ext.last_optional = a.type;
		}));
			
			
		break;
	}
}


//TODO
/*
import {INVENTORY} from "Avatar/index";
export const eShop = (index) => {
	switch(index){
	default:
	case 101:
		set.irl();
		INVENTORY.shop("shop", next_event);
		break;
	}
}
export const testShop = (index) => {
	switch(index){
	default:
	case 101:
		set.irl();
		INVENTORY.shop("testShop", next_event);
		break;
	}
}
*/


