import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, bold, hint} from "Loop/text";
import {placetime} from "Loop/text_extra";

//import {recesses} from "Taskmaster/recesses";
import {next_event} from "Taskmaster/scheduler";
import {new_optionals} from "Taskmaster/new_optionals";

import {capitalise} from "Libraries/dh";


export const current_end = (index) => {
	
	placetime(undefined, `TO BE CONTINUE... `);
	txt(`This is current ending of the game. I hope you enjoyed the game so far.`);
	
	if(hardbug){
		txt(`Thank you extremely much for your support on <a href = "https://www.patreon.com/Dahakma" target="_blank">Patreon</a>!`);
	}else{
		txt(`(Did I mention I have <a href = "https://www.patreon.com/Dahakma" target="_blank">Patreon</a> where I publish the newest stuff?)`);
	}
	
	/*
	txt(`The next update will be public in a few weeks. (I am very sorry the current one took so atrociously long.)`);
	txt(`(Most of the <i>removed</i> events was not removed but will happen later, you do not have to be afraid.) `);
*/
}


export const error = (id) => {
	txt(`#Chapter "${id}" not found (or is broken)! 😿 `);
	next(`Next chapter.`);
}