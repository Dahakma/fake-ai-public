import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, bold, hint} from "Loop/text";
import {placetime} from "Loop/text_extra";
import {DISPLAY} from "Gui/gui";

import {time} from "System/time";
import {ra} from "Libraries/random";

function flavour(){
	const day = time.day_name;
	const key = time.day;
	
	const base = [];
	/*
	const base = [
		`I woke up. It was ${day}! `,
		`It was ${day}. `,
	];
	*/
	placetime(null, `${day} Morning`);
	

	if(key === "wen"){
		base.push(`${day}. Three more days to the weekend! `);
	}
	
	if(key === "sat"){
		base.push(
			`It was finally ${day}. `,
			`It was finally ${day}. At leas for one day I did not have to be bothered by those jerks! `,
		);
	}
	
	if(key === "sun"){
		base.push(
			`It was ${day}. I did not look forward to the Monday, it only meant another dumb task. `,
			`I always disliked Sundays. Sure, the day was free but it was immediately followed by Monday unlike carefree Friday and Saturday. `,
		);
	}

	return ra.array(base);
}
				
export const awakening = (index) => {
	set.irl();
	txt(flavour());
	
	//DISPLAY.text("morning");
	DISPLAY.dress_up();
	
	
	/*
	switch(index){
		case 101:
			
			
			morgen_one(102);
			break;
		
		case 102:
			//txt(`Morning`);
			morgen_two(102);
			break;
	}
	*/
}