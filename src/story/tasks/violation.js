import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";

import {emo, wdesc, effigy, createWear, removeUnsexyClothesFromInventory, wearRandomClothes, remove, wear, create, wears, showLess, showAll, violates, sluttiness } from "Avatar/index";
import {active_timeslot, next_event, no_morning_meeting} from "Taskmaster/scheduler";
import {placetime, task_intro, task_reaction} from "Loop/text_extra";
/*
export const rules_violation = ()=> { //TODO - SHOULDNT BE TRIGGERED VIA MAIN
	
	
}
*/

export const rules_violation_demerit = ()=> {
	mile.rv_demerit = true;
	emo("anoy"); 
	if(!no_morning_meeting()){
		qaa(`And one more thing!  `);
	}else{
		qaa(`@katalt?!  `);
		txt(`I was approached by the nerds.  `);
	}
	kat(`What?  `);
	qaa(`We have to address an important issue - you're breaking the rules!  `);

	const a = [];
	if(violates.short_skirt) a.push(`short skirts`);
	if(violates.show_legs) a.push(`short skirts or shorts`);
	if(violates.sexy_legwear) a.push(`sexy legwear`);
	if(violates.sexy_top) a.push(`sexy tops`);
	if(violates.choker) a.push(`choker`);

	if(a.length){
		qcc(`You're supposed to wear ${and(a)}!`);
		if(violates.no_bra){
			qaa(`And it seems to me you're wearing a bra! `);
		}
	}else{
		qcc(`You're not allowed to wear bras!`);
	}

	if(mile.sub < 0){
		kat(`Yeah? What are you going to do with that?  `);
	}else if(mile.sub > 3){
		kat(`Sorry! But I'm just unable to get used to that I can't wear what I want! `);
	}else{
		kat(`Hmmm. `);
	}

	qaa(`We have to give you a demerit.  `);
	kat(`A demerit?! What does that mean!?! `);
	qbb(`It's a warning! But you don't want to get another! `);

	qaa(`Look, @kat, you willingly agreed with our deal, didn't you?  `);
	kat(`Yeah, but I was under a lot of pressure and I thought it meant like two or three tasks!  `);
	qcc(`Don't blame us for your own wrong assumptions!  `);
	qaa(`And our tasks are pretty reasonable, aren't they? Never before you were troubled by dressing sexy! `);
	kat(`I think a few of the tasks went too far! `);
	qaa(`If you want to cancel our deal tell us!  `);
	txt(`I hesitated. Maybe that would be the best? I did not like to be their toy. On the other hand, if given up now, everything I did so far would be completely pointless. But if I persist for a few more days, till I finish the Game, I'll get rid of them and their stupid AI forever! `);
	kat(`...no.  `);
	qaa(`Then follow the rules! `);
	next(`Fine! `);
};



export const rules_violation_pants = (index = 101)=> {
	
	function panties_reaction(){
		txt(`They were unabashedly staring at my exposed ${wdesc.panties}. `);
		if(wears.sexyPanties){
				mile.saw_sexy_panties = true;
				qcc(`Cool panties! `);
				qbb(`As slutty as I expected! `);			
				qcc(`Do you always wear such sexy panties or were we just lucky? `);
				kat(`That's none of your bussiness! `);		
			}else{
				mile.saw_sexy_panties = false;
				qbb(`I imagined something better. `);
				if(mile.a_burned){
					qcc(`I think they are cute. `);
					qaa(`They are too boring and prude. `);
				}else{
					qaa(`I think they are cute. `);
					qcc(`They are too boring and prude. `);
				}
				if(mile.sub > 3){
					kat(`Sorry! I just don't wear sexy lingerie to the school every day. `);
					txt(`I was pretty embarassed by their dismissal. I did not expect I will undress in front of them! `);
				}else if(mile.sub < 0){
					kat(`Nobody asked for your opinion!`);
					txt(`I was pretty offended by their dismissal! I did not expect I will undress in front of them! `);
				}
				
				qaa(`We can do something about that. `);
				kat(`What do you mean?!`);
				qaa(`You don't have to be concerned about that right now. `);
			}
	}
	
	function no_panties_reaction(){		
		if(!mile.saw_pussy){
			qcc(`WHOA!`);
			qbb(`You're such a slut you don't even bother to wear underwear? `);
			txt(`Chuckled impressed @qbb`);
			if(mile.a_burned){
				qaa(`I guess not wearing panties saves her time while undressing!`);
				txt(`@qaa implyied I was a promiscuous slut. `);
			}
			
			if(!mile.saw_hairy && wears.hairyPussy){
				qcc(`I would never think you have such a hairy pussy!`);
				qbb(`Yeah, a nice beaver! `);
				kat(`Fuck you!`);
				mile.saw_hairy = true;
			}
			mile.saw_pussy = true;	
		}else{
			txt(`Their eyes widened when they realized I was wearing no underwear, even though they were not shocked like when they saw my minge for the first time. `);
			qaa(`Still not a big fan of panties, I see.`);
			qcc(`That's so hot!`);
			qbb(`Being a shameless slut, just the way we like her! `);
		}
	}
	
	switch(index){
		default:
		case 101:
			emo("shock"); 
			if(!no_morning_meeting()){
				qaa(`Also there's an important issue which has to be resolved.  `);
			}else{
				qaa(`@katalt?!  `);
				txt(`I was stopped by the nerds.  `);
				kat(`What's going on?  `);
				qaa(`There's an important issue which has to be resolved.  `);
			}
			txt(`They grabbed me and dragged me to the bathrooms. `);
			kat(`What's going on? `);
			qaa(`What are you supposed to wear? `);
			if(wears.skirt){
				kat(`...short skirts? But this one is rather short too, isn't it? `);
			}else{
				kat(`...skirts? But that's pretty dumb, I think I look pretty sexy in pants too...`);
			}
			qbb(`Nobody fucking asked you about your stupid opinions!` );
			qaa(`Whoa, slow down! @katalt, the thing is we disagree. Moreover, this isn't the first time you ignored our tasks!  `);
			qcc(`We need you to strip your ${wdesc.lower} right now!  `);
			kat(`You want me to strip my ${wdesc.lower}?!?  `);
			qbb(`Are you deaf or thick?  `);
			kat(`Right now?! Right here?! `);
			qbb(`Do it! `);
			
			link(`Do it.`, 102);
			link(`Refuse.`, 103);
			break;
	
	
	
		case 102: 
			mile.sub++;
			{
				const action = (()=>{
					if(wdesc.lower === `jeans` || wears.skirt){
						return `I reluctantly unbuttoned my @lower and pulled ${wears.pants ? "them" : "it"} down`;
					}else{
						return `I reluctantly pulled down my ${wdesc.lower}`;
					}
				})();
				
				if(!PC.panties){
					//TODO NO PANTIES RULE
					txt(`${action}. I did not want them to find out I was wearing no panties but I had no other choice. They started laughing when they saw my exposed minge. `);
					no_panties_reaction();
				}else{
					if(mile.slut < 1){
						txt(`I was horribly blushing and my fingers were shaking when ${action}. I felt so embarrassed and humiliated that those three mean jerks made me undress in front of them.`);
					}else if(mile.slut > 3){
						txt(`${action}. I was not ashamed that they might see me just in my underwear but did not like very much being forced to strip in front of them.`);
					}else{
						txt(`${action}. I felt anxious, stripping in front of them was embarrassing and humiliating. `);
					}
					panties_reaction();
				}
			}
			
			kat(`Fine! You humiliate me! I'm punished! Give me my clothes back!`);
			txt(`Instead, @qcc reached to the pocket and handed me what I had inside (my phone and the school ID). @qbb shrugged: `);
			qbb(`You're still not allowed to wear ${wdesc.lower}!`);
			kat(`What?!?..... HEY!.... Oh no! `);
			txt(`They left me bottomless at school bathrooms! `);
			remove(`lower`);
			remove(`belt`);
			link(`Wait until they return. `, 110);
			break;
			
		case 103:
			mile.sub--;
			emo("angry");
			if(mile.slut < 1){
				kat(`N..no! I can't do that!`);
				txt(`I was stunned and embarrassed. They could not expect me to undress in front of them! `);
			}else{
				kat(`No! You're out of your mind! `);
			}
			
			txt(`However they did not take no for an answer. They roughly grabbed me, I was so suprised by their audacity I was not able to react before they were holding me. I was sure I would be able to beat each of them individually but when they acted together - one holding my arms behind my back, one dragging down my ${wdesc.lower} and one writhing on the floor after I managed to kick him between legs - I had no chance. Finally they dropped me on the cold tiles. `);
			
			if(!PC.panties){
				no_panties_reaction();
			}else{
				panties_reaction();
			}
			
			kat(`Are you happy, assholes?! You humiliate me! I'm punished! Give me my clothes back!`);
			txt(`Instead, @qcc reached to the pocket and handed me what I had inside (my phone, card and the school ID). @qbb shrugged: `);
			qbb(`You're still not allowed to wear ${wdesc.lower}!`);
			kat(`What?!?..... HEY!.... Oh no! `);
			txt(`They left me bottomless at school bathrooms! `);
			remove(`lower`);
			remove(`belt`);
			link(`Wait until they return. `, 110);
			break;
			
			
		case 110:
			emo("anoy");
			txt(`I waited. Initially I was pretty spooked that they might be serious but they would not left me here bottomless, would they? `);
			link(`Wait.`,111);
			break;
			
		case 111:
			txt(`They just wanted to scare me. They will be back soon. `);
			link(`Wait.`,112);
			break;
			
		case 112:
			emo("angry");
			txt(`I could hear the school bell ending the break. I had to admire their determination.  `);
			link(`Wait.`, 113);
			link(`Leave.`, 120);
			break;
			
		case 113:
			emo("help");
			txt(`Where the hell they were!? I was getting pretty nervous. `);
			link(`Wait.`, 114);
			link(`Leave.`, 120);
			link(`Call @sas.`,130);
			break;
			
		case 114:
			txt(`Fuck. They will not return! `);
			link(`Leave.`, 120);
			link(`Call @sas.`,130);
			break;
			
			
			
			
		case 130: 
			present(["sas",["tee","shortSkirt","pushUpBra","socks","hipsters"]]);
			mile.sas++;
			emo("shy");
			txt(`I did not want to get my friends involved, this situation might be tricky. But I did not come up with any other solution. `);
			txt(`Luckily, the nerds let me keep my cards, id and phone. `);
			kat(`Eh, hello, @sas! I need you to go male bathrooom at the 2rd floor.  `);
			sas(`Male bathroom?!? `);
			kat(`Yeah! Right now! `);
			txt(`@sas soon arrived. She was very confused what was going on: `);
			sas(`Why are you half-naked!?! `);
						
			link(`Deflect questions. `,131);
			link(`Try to come up with an explanation. `,132);
			break;
			
		case 131:
			mile.sas_exploit++;
			emo("angry");
			txt(`Why do you always have so many stupid questions? Don't you see I desperately need your help? You should be trying to help me first, not interrogating me!`);
			sas(`Sorry!`);
			kat(`...fine!`);
			sas(`What do you need? `);
			
			link(`I need your to bring me my spare clothes. `,134);
			link(`I need your skirt. `,133);
			break;
		
		case 132:
			emo("focus");
			kat(`That's a very good question with a very good answer. `);
			sas(`Yeah? `);
			kat(`Well, my jeans got ripped. `);
			sas(`At male bathrooms?!?! `);
			kat(`NO!! Of course not! But... well they were the closest.... `);
			if(!PC.panties){
				sas(`Your panties got ripped too? `);
				kat(`No! I didn't wear any. `);
				sas(`Oh! Right! `);
				txt(`Just the idea of going to the school without underwear was making her uncomfortable. `);
			}
			sas(`Okay. That seems like a believable explanation.  `);
			kat(`Maybe you can help me instead of interrogating me? `);
			sas(`Sorry! What should I do? `);

			link(`I need your to bring me my spare clothes. `,134);
			link(`I need your skirt. `,133);
			break;
		
		case 133:
			{	
				mile.sas_exploit++;
				mile.rv_lower_repo = 3;
				const temp = [];
				remove("lower", effigy.sas, temp);
				wear(temp[0]);
			}
			emo("relax");
			kat(`I need your skirt!`);
			sas(`W... what?!`);
			kat(`I need your skirt!`);
			sas(`But... I can't give it to you!`);
			kat(`Once I need your help and you're not willing? `);
			sas(`I'd love to help you! But what would I do without my skirt? `);
			kat(`Don't you have some spare clothes? Or I could borrow you some? `);
			txt(`@sas finally calmed down when she realized I am not going to leave her here bottomless. `);
			sas(`I have gym pants in my locker. `);
			txt(`Without much enthusiasm, she undressed and handed me her skirt. I knew it will fit me, I borrowed her few of my old skirts before. `);
			kat(`Thank you very much, @sas! You saved my life!`);
			sas(`P...please hurry! `);
			txt(`She blushed. `);
			txt(`I rushed downstairs to the locker room in the basement, used the school ID card she gave me to unlock her locker and searched for her spare clothes. @sasalt was very relieved when I returned.`);
			link(`Classes. `, 150);
			break;
			
			
		case 134: 
			mile.rv_lower_repo = 2;
			emo("focus");
			kat(`I need you to take my ID, go to my locker and bring me my gym clothes. `);
			sas(`Okay, I can do that. `);
			kat(`Then go!`);
			sas(`Right! `);
			txt(`I was very impatiently waiting. Stupid @sas! What took her so long? But eventually, she was back and I could get dressed again. `);
			link(`Get dressed. `, 150);
			break;
		
		case 120:
			mile.rv_lower_repo = 1;
			emo("unco");
			txt(`The classes already started so the hallways were empty and the chance I might run into somebody was low. Still, I was terrified I can meet a classmate or even a teacher! I was not sure how I would be able to explain my naked ass!`);
			txt(`I instantly came up with a plan. There was a bag with spare gym clothes in my locker. It would not be perfect but still better than nothing. I only had to get from the second floor to the basement. `);
			txt(`I was sneaking through the empty hallways, terrified that at any moment anybody can open and door. It was nerve-racking. `);
			txt(`But I was very fortunate and nobody caught me. I could not be sure whether somebody did see me through a window but there was not much I could do with that. `);			
		/*
			TODO - TOM & ZANET in right phase 
		*/
			link(`Locker. `, 150);
			break; 
			
		case 150:
			mile.classes_late++;
			
			if(mile.rv_lower_repo === 3){
				txt(`@sas skirt suited me well and I felt good about outsmarting the nerds. Were they expecting I am going to spend the rest of day bottomless? Or that I am going to run home crying? `);
			}else{
				mile.stat--;
				if(!PC.panties){
					txt(`I was not wearing panties and the tight leggings snug too closely to my skin, creating an embarrassing cameltoe. People were staring at me the whole day, making me feel rather uncomfortable.  `);
				}else{
					txt(`I put on my gym leggings. They were very tight, not hiding any of my curves. People, and by people I don't mean only guys but also girls and teachers, were staring at my legs and butt the whole day. `);
				}
			}
			
			if(mile.rv_lower_repo === 1){
				maj(`You're late, @katalt!`);
				txt(`Frowned @maj when I finally get to the class.`);
				kat(`I'm sorry! `);
				txt(`I apologized and sat down. However, considering how boring her lessons usually were, I doubted I had missed anything important. `);
			}else{
				maj(`You're late, girls!`);
				txt(`Frowned @maj when we finally get to the class.`);
				kat(`I'm sorry! `);
				txt(`We apologized and sat down. However, considering how boring her lessons usually were, I doubted we had missed anything important. `);
			}
			
			
			createWear(`gymLeggings`);	
			next();
			break; 
	}
};			
			
			
			
			
			
			




export const rules_violation_ruler = (index = 101)=> {
	 
	
	switch(index){
		default:
		case 101:
			mile.rv_ruler = true;
			if(!no_morning_meeting()){
				qaa(`Before we let you go, we have talk about your breaking your rules.  `);
				qcc(`Again! `);
			}else{
				//txt(`I was approached by the nerds.  `);
				txt(`I was approached by the nerds. They grabbed me and dragged me to the bathrooms. `); //TODO
				qaa(`You're breaking our rules again, @katalt! `);
				
			}
			qbb(`Despite our warnings! `);
			
			{
				const a = [];
				if(violates.short_skirt) a.push(`short skirts`);
				if(violates.show_legs) a.push(`short skirts or shorts`);
				if(violates.sexy_legwear) a.push(`sexy legwear`);
				if(violates.sexy_top) a.push(`sexy tops`);
				qaa(`Did you forgot you're supposed to wear ${and(a)}!`);
			}
			
			qbb(`You have to be brutally punished! `);	
			link(`Brutally punished!?!`, 102);
			break;
	/*
			txt(`They grabbed me and dragged me to the bathrooms. `);
			kat(`What's going on? `);
			qaa(`What are you supposed to wear? `);
			
			
		qaa(`Whoa, slow down! @katalt, you constantly break our rules. We tried to be magnanimous but we cannot longer ignore your misconduct. You have to be punished!`);
*/

		case 102:
			showLess(-1,1,3);
			emo("pain");
			
			qbb(`Exactly! `);
			if(mile.aaa > 1 && !mile.a_burned){
				qaa(`I don't think a brutal punishment is necessary. Maybe it might be enough if she swore she won't be disobedient again? `);
				if(mile.c_friend > 0){
					qcc(`I don't think we should be too harsh on her! `);
				}else if(mile.ccc > 1){
					qcc(`Yeah?`);
				}
			}else{
				qaa(`Yeah. She needs to be punished. `);
				if(mile.c_friend > 0){
					qcc(`We don't have to be too harsh on her! I don't think a brutal punishment is necessary. `);
				}else if(mile.ccc > 1){ //TODO
					qcc(`Is that... necessary? `);
				}
				
			}
			qbb(`It's a matter of principle. We can't be inconsistent. Have you ever trained a dog? `);
			if(mile.a_burned){
				qaa(`Have you?`);
				qcc(`@kat is not a dog!`);
			}else{
				qcc(`Have you?`);
				qaa(`@kat is not a dog!`);
			}
			kat(`Thank you for standing up for me, chief.`);
			qbb(`Yeah, she is a bitch. Give me your ruler, @qcc! `);
			
			if( /*(PC.lower && PC.lower.innerLoose>=1) */ wears.skirt){
				txt(`@kat took from his bag his dorky blue 40-centimeter ruler. @qbb pushed me against the wall and flipped up my @lower. It took me too long to realize what was his intention. `);
			}else{
				txt(`@kat took from his bag his dorky blue 40-centimeter ruler. @qbb pushed me against the wall: `);
				qbb(`Pull down your pants! `);
				kat(`What?!`);
				qbb(`PULL DOWN YOUR PANTS! `);
//TODO YARRING - ADD BLACKMAI SWITCH
				txt(`He screamed at me. Confused I unbuttoned my pants and let them slide down. Too late I realised what was his intention. `);
			}
			txt(`He smacked my butt. I squealed in pain and @qcc screamed in panic. `);
			qcc(`What the hell are you doing! You'll break it! `);
			qbb(`What? No way! Look: `);
			txt(`Terrified @qcc watched how @qbb bend the ruler in a sharp arc. My bottom itched and I really hoped he will break it. But he did not. `);
			qbb(`See! Now is your turn! `);
			txt(`He threw the ruler to @qcc who nervously smacked my butt again. He was afraid to hit me too hard and it barely hurt. `);
			qaa(`Mediocre!`);
			qbb(`Try again!`);
			txt(`I really felt the next hit. `);
				
			link(`Cry and beg them to stop. `,110);
			link(`Endure the spanking. `,112);
			link(`Encourage them. `,113);
			break;
			
		case 110:
			mile.sub++;
			emo("fake");
			kat(`Please stop! It hurts so much! `);
			txt(`I desperately whined and begged them to stop. The ruler stung a bit but I seriously overreacted and the more I cried the less vigorous was their spanking. `);
			txt(`@qaa ended with the last feeble hit: `);
			qaa(`That... might be enough. `);
			kat(`Please, don't beat me anymore! I promise I won't do that again! I'll do anything you want! Just don't hurt me! `);
			txt(`I heartwrenchingly sobbed. `);
			qaa(`We are really sorry! Trust us, it hurt us more than you! But you have to exactly follow our task, that was the deal. `);
			kat(`Yes, yes I will!`);
			txt(`I sobbed and they finally let me go. `);
			next(`The rest of the day. `, showAll);
			break;
			
		case 112:
			emo("focus");
			txt(`I tried to mentally zone out and don't act undignified. The sharp hits hurt a bit but nothing too terrible and I withstood the whole execution without a word or a moan. `);
			txt(`@qaa ended with the last swift hit. `);
			qaa(`That might be enough. Trust us, it hurt us more than you! But you have to exactly follow our task, that was the deal. `);
			next(`Okay `, showAll);
			break;
			
		case 113:
			mile.sub--;
			emo("smug");
			mile.rv_ruler_broken = true;
			kat(`Is that all you got? I barely felt that! `);
			txt(`Which was a lie, it hurt a quite bit, but I certainly did not want to give them the satisfaction. `);
			txt(`However, I paid for my brattiness dearly. They were angry their disciplining had no effect on me and smacked my poor bottom without restraints. `);
			kat(`Harder! `);
			txt(`I shouted trying to suppress the pain. @qbb smacked my ass especially violently. Tears burst from my eyes but I was able to wipe them out before they noticed - they were distracted by something else. The top half of the ruler clanged on the floor while the bottom one was still in @qbb's hand.	`);
			
			qcc(`You broke it, you dumb fucking idiot! `);
			qbb(`Oops? It's not my fault you had such a shoddy ruler! `);
			qcc(`Shoddy?! SHODDY?!? `);
			
			if(!mile.a_burned){
				qaa(`You didn't have to spank her so hard! `);
				qbb(`What?!? She was asking for it! `);
			}else{
				qaa(`Calm down! It may be fixed... `);
				qcc(`Fixed?! FIXED!? This was a precise tool! `);
			}
			
			txt(`I loudly laughed and they looked very unhappy and pissed. `);
			next(`So long! `, showAll);
			break;		
			
	}
};





export const blackmail = (index = 101)=> {
	//game_night_initiation
	//rules_violation_ruler //TODO
	//rules_violation_impound
	switch(index){
		default:
		case 101:
			mile.blackmail = true;
			emo("shock");
			kat(`Yeah. I want to quit!`); 
			txt(`I did not want to quit. I wasted already too much time and suffered too many humiliations to quit. And the promise they will ${!mile.y_forkill || mile.y_friend > 0 ? `stop exploiting the AI and delete` : `delete the AI and`} all the data collected about me and will not ever bother me anymore was too sweet. But their demands went too far and I had to draw the line. They were acting tough but I was sure once I will firmly refuse they will quickly back out. They were enjoying toying with me too much and did not want me to quit either. They will make an excuse to not lose their face and will come up with a less awful task.`);
			qaa(`A... are you sure? `);
			kat(`Yeah, I'm sure!`);
			txt(`I smirked `);
			qaa(`Fine then. The deal is off. We'll keep the AI.`);
			qbb(`She doesn't know about the Cloud. Tell her about the Cloud!`);
			kat(`Cloud? `);
			qbb(`<i>Your</i> Cloud.`);
			qaa(`Well... long story short, we gained an access to your Cloud and made a copy. `);
			qcc(`We assumed such a huge amount of personal data will make our AI perfect. We did not want to snoop around or anything.`);
			kat(`But... that's impossible! My Cloud is completely secure! My password has numbers and weird characters and everything!`);
			qaa(`We did not have your password. But we had your secret password recovery question.`);
			kat(`...What!?...How?!`);
			qbb(`You've told it us yourself. `);
			txt(`Oh no! It really hurt to be betrayed this way by my best and closest childhood friend. I hope you suffer in bunny hell, Tlapka. `);
			if(mile.a_burned){
				qaa(`So far we haven't dig deep. But there have to be so many interesting things!`);
			}else{
				qaa(`So far we didn't check it. It didn't seem fair since you were so cooperative. `);
				qbb(`But maybe we should start digging, there has to be so much interesting information. `);
			}
			txt(`I was horrified. They had full access to all my private information! I could not recall what was the worst thing stored there but I was sure they will sooner or later find something awful. Now they literally had the power to ruin my life!`);

			link(`Resist. `, 102, ()=> mile.blackmail_resist = true);
			link(`Surrender. `, 102, ()=> mile.blackmail_resist = false);
			break;
			
		case 102:
			
			if(mile.blackmail_resist){
				emo("angry");
				mile.sub--;
				txt(`I was outraged. `);
				kat(`This is... you bastards! Those are my personal things! Don't you fucking dare to touch them!`);
				txt(`But the nerds just smiled. `);
			}else{
				mile.sub++;
				txt(`The nerds smiled when they saw my shock. `);
				kat(`Please, don't share anything! Those are my personal things!`);
				txt(`I begged them desperately. `);
			}

	//TODO 
			if(mile.a_burned){
				qaa(`Maybe we will, maybe we won't. It depends only on your actions. `);
			}else if(mile.a_love > 0){
				qaa(`Of course, we won't, @kat! You don't have to worry!`);
				txt(`Ensured me @qaa. However, the knowledge they could did not make me very content. `);
			}else{
				qaa(`We, of course, didn't intend to! `);
				txt(`Ensured me @qaa but it did not calm me up very much. `);
			}
			qcc(`The copy of your Cloud is a part of our deal. We'll delete it too if you win the game. `);
			qbb(`But you wanted to quit, didn't you?`);

			if(mile.blackmail_resist){
				kat(`Well, I don't anymore. Now when I found out you're not ashamed to blackmail me!`);
				qaa(`You don't have to be so dramatic. `);
				kat(`How else would you call it?`);
				qcc(`...she might be right, that's the correct term. `);
				if(mile.a_burned){
					//TODO
				}else{
					qaa(`She doesn't have to worry about anything, as long as she'll follow the rules!`);
				}
				qbb(`Have you reconsidered agreeing to our last command? `);
				kat(`Yeah, I fucking did! Fine! I'll do that! `);
				txt(`I needed more time to process what just happened and to find some defense. `);
				
			}else{
				kat(`No!`);
				qaa(`So do you want to continue?`);
				kat(`...yes.`);
				qbb(`Yes what!?`);
				kat(`Yes, I want to continue. `);
				txt(`I mumbled crestfallenly. `);
				qaa(`And will you fulfill all our tasks without excuses, the best way you can?`);
				kat(`...yes.`);
				qcc(`Full sentence!`);
				kat(`Yes, I will fulfill all your task without excuses, the best way I can. `);
				txt(`I was forced to promise. `);
				if(mile.a_burned){
					qaa(`And now apologize for being sassy and refusing to follow our very reasonable command.`);
				}else{
					qbb(`And now apologize for being sassy and refusing to follow our very reasonable command.`);
				}
				txt(`At that moment, I had no more willpower to resist. I needed a moment to process what just happened. `);
				kat(`I'm sorry for being sassy. `);
			}
			
			insert_return(`Next.`);
		
	}
};








export const rules_violation_impound = (index = 101)=> {
	switch(index){
		default:
		case 101:
			set.irl();
			mile.rv_impound = true; 
			ext.rules.enforced = true;
			emo("angry");
			removeUnsexyClothesFromInventory();
			placetime("at home");
			txt(`Not long after I arrived from school, somebody was ringing on our door. I went to open and it was the trio! `);
			kat(`What the hell are you doing here?!`);
			qaa(`Hello, @kat. Nice to see you too!`);
			qcc(`Won't you invite us in? `);
			txt(`They noticed how I hesitated, my instinct was to slam the door right into their faces. But I decided to let them in to see what they want. It was a horrible mistake. `);
			kat(`Why are you here? `);
			qaa(`We are here to help you. `);
			qcc(`It seems you have some problems with following our tasks. `);
			qbb(`We gave you only a few clothing rules but it seems you're too thick to remember them all. `);
			txt(`@qcc brazenly opened my wardrobe to see what is inside. `);
			kat(`Hey! What are you doing! `);
			qcc(`There are so many dresses!`);
			qbb(`We don't care about dresses, only the contraband. `);
			qaa(`Do you have any empty boxes, @kat?`);
			kat(`Boxes? Why?!? What are you doing?!?`);
			qaa(`For your clothes. You have troubles to remember which clothes you're allowed to wear and which not. So we decided the best will be to temporarily impound the banned unsexy pieces. `);
			kat(`You're crazy! You can't do that! Stop! `);
			txt(`While I was talking with @qaa, @qbb and @qcc were throwing my clothes out. `);
			kat(`You can't steal my clothes! `);
			qaa(`It isn't stealing, don't worry, we'll return them! `);



			if(!mile.blackmail){
				kat(`Stop! You can't do this!`);
				qaa(`Our deal was that you'll willingly follow all our rules but you don't! `);
				kat(`Maybe I don't like our deal anymore! `);
				qaa(`You want to quit? `);
				
				link(`Well... no. `, 103);
				link(`Yes, I do! `, 102);
			}else{
				kat(`But... `);
				qaa(`And we need to remind you, you willingly agreed to follow all our rules. `);
				kat(`Because you blackmailed me! `);
				emy.ab(`Yeah, exactly. And we still do! `);
				
				link(`Direct them. `,201);
				link(`Stop them. `, 202);
				link(`Cry. `, 203);
			}
			break;
				
		case 102: 
			insert("blackmail", 101, undefined, "rules_violation_impound", 103);
			break;
				
		case 103:
			qbb(`We're happy you're so resonable!`);
			link(`Direct them. `,201);
			//link(`Stop them. `, 202);
			link(`Cry. `, 203);	
			break;	
				
				
		case 201:
			mile.rv_impound_reaction = 1;
			emo("anoy");
			txt(`They seemed to be determined, no pleas or threats stopped them. Horrified by the idea of them rummaging through all my clothes I decided to intervene. `);
			kat(`Wait! You'll make a mess! `);
			qaa(`You're willing to cooperate? `);
			kat(`Well... yes. `);
			txt(`I reluctantly admitted and pushed them away. `);
			kat(`What do you want? `);
			qcc(`All unsexy clothes. `);
			kat(`Could you define them more accurately? `);
			txt(`We went through my wardrobe and I tried to save what I could. `);
			kat(`What if I'll go to visit my grandma? I can't be dressed like a slut!`);

			if(mile.burned){
				qcc(`That's a good point. `);
			}else{
				qcc(`That's a good point. `);
				qaa(`We should leave her one proper outfit. `);
			}

			kat(`What if I'll go to visit her two times in a row? `);
			qaa(`You can wear the same outfit! `);
			kat(`Wearing the same clothes two times in a row? I'm not a slob!`);
			qaa(`You can wash them. `);
			kat(`But what if they won't dry up overnight? `);
			qaa(`I seriously doubt you'll visit your grandma two times in a row!`);
			kat(`I have two grandmas! `);
			qbb(`Let her have some so she would finally shut up!`);
			qaa(`Okay... What's in this drawer? `);
			kat(`My underwear. `);
			qcc(`Open it! We have to check it! `);
			
			if(!ext.rules.sexy_panties && !ext.rules.sexy_panties){
				kat(`You don't! No task limited my panties! `);
				qbb(`No task so far. `);
				kat(`So what!`);
				qaa(`Well... she's is technically right. We can't go through her panties. `);
				kat(`Thank you!`);
			}else{
				txt(`I was helplessly forced to watch how they scattered around my panties, discussing every piece. `);
			}
			
			txt(`When they left, they took with them nearly half of my wardrobe. I had wanted to get rid of some my old clothes to free the space but this was a little bit too radical. `);
		
			next();
			break;



		case 202:
			mile.rv_impound_reaction = 2;
			mile.sub--;
			emo("angry");
			
			kat(`You can't do this! `);
			txt(`They pissed me so much I tried to physically stop them. Unfortunately, after a short scuffle, I got overwhelmed and pinned to the bed. @qaa and @qcc held me down while @qbb was restraining me by a fluffy bathrobe belt tie. `);
			txt(`I was cursing them and promising the most dreadful revenge but they did not care and audaciously went through all my shelves and drawers. I was absolutely helpless to stop them. They pulled out my clothes, joked and evaluated whether they are slutty enough. They even scatter my underwear drawer. `);
			
			if(!ext.rules.sexy_panties && !ext.rules.sexy_panties){
				kat(`You can't do that! There was nothing in the rules about panties! `);
				qaa(`So now you're caring about rules? `);
				kat(`I do you perverted bastards!`);
			}else{
				kat(`You perverted bastards!`);
			}
			
			qbb(`You whining is getting tiresome!`);
			txt(`@qaa looked at @qcc and then at @qbb. @qbb nodded. @qaa took several my panties he was holding and gagged my mouth with them. I was red with anger and humiliation.  `);
			txt(`When they finished, half of my wardrobe was gone.  I had wanted to get rid of some my old clothes to free the space but this was a little bit too radical. `);

			next();
			break;

		case 203:
			mile.rv_impound_reaction = 3;
			mile.sub++;
			emo("cry");
			
			kat(`You.. you jerks! `); 
			txt(`It was too much. Being constantly humiliated, having my life completely controlled and ruined by three mischievous perverted jerks! I was helpless even to stop this severe violation of my privacy! I tried but I could not stop. I began sobbing. `);

			if(mile.a_burned){
				txt(`@qcc felt uncomfortable and after a short hesitation, he approached me. `);
				qcc(`Are you okay,  @kat? `);
				kat(`Fuck you!`);
				txt(`I responded and ran out of the room. I did not want them to see me this way. I could hear them chatting behind the door. `);
				qcc(`Don't you think we went too far? `);
				qaa(`That bitch is faking it! `);
				qcc(`Do you think?`);
				qaa(`Yeah, that's what she's always doing, tricking people to get their sympathy.`);
				qcc(`I'm not sure. `);
				qbb(`You're missing the point. Either way, if we back out now, she'll see we're weak and will exploit it in future. `);

			}else{
				txt(` @qaa instantly reacted. `);
				qaa(`Are you okay,  @kat? `);
				kat(`Go to hell! `);
				txt(`I responded and ran out of the room. I did not want them to see me this way. I could hear them chatting behind the door. `);
				qaa(`Poor @kat! We went too far this time!`);
				qcc(`She's probably faking it. `);
				qaa(`It didn't look like fake tears. `);
				qcc(`Dunno. `);
				qbb(`You're missing the point. Either way, if we back out now, she'll see we're weak and will exploit it in future. `);
				qcc(`Yeah. `);

			}
			txt(`I locked myself on the bathroom and did not leave until they were gone. They took half of my clothes and the rest remained scattered on the floor. `);

			next();
			break;
	}
};
			
/*			
	
			kat(`Eh, hello, `+name.sas+`. I need you to take keys from my bag, go to my locker and bring me my gym clothes. `);
			sas(`But why? `);
			kat(`OMG! Do you have to be so nosy every time I need your help? `);
			sas(`Sorry. Bring where? `);
			kat(`Male bathrooms on the first floor. `);
			sas(`Male bathroom?!? `);
			kat(`Yea. Be quick! `);
			link(`Wait. `,234);
			break;
			
		case 234:
			mile.bottomlessBathrooms = 1;
			txt(`When the doors opened, I rushed to hide in a cabin in case it was somebody else than `+name.sas+`. `);
			txt(`As I quickly found out, it was not  `+NAME.sas+`. `);
			eva(NAME.kat+`? `);
			txt(`Damn! It was `+NAME.eva+`! `);
			kat(`Hi?`);
			eva(NAME.sas+` rushed to the class so I offered my selfless help.  `);
			kat(`Thanks. `);
			eva(`What are you half-naked? `);
			kat(`I'll explain later. `);
			txt(`However, unlike `+NAME.sas+`, `+NAME.eva+` was impossible to easily palm off. `);
			kat(`Well, my jeans got ripped. `);
			eva(`At male bathrooms?!?! `);
			kat(`NO!! Of course not! But... well they were the closest.... `);
			eva(`Sure. That seems like a believable explanation.  `);
			txt(`What a bitch! `);
			link(`Get dressed. `,245)	
					

			if(mile.qaa > 0){
				txt(`I was very relieved when I found out `+NAME.qaa+` was waiting outside. `);
				kat(`Oh! Thank God! Do you have my pants?`);
				qaa(`I have something better. `);
				txt(`He handed me a tiny piece of fabric. `);
				kat(`What is this? `);
				qaa(`A skirt! `);
				link(`You can't be serious! `,241)
				link(`Thank you! `,242)
				
			}else{
				txt(`Luckily, the classes already started so the chance I will meet somebody in hallways was low. I rapidly came up with a plan. There was a bag with spare gym clothes in my locker. It would not be perfect but still better than nothing.`);
				txt(`I only had to get from the first floor to the basement. `);
				txt(`I was sneaking through the empty halls, terrified that at any moment anybody can open and door. It was nerve-racking. `);
				txt(`But I was very fortunate and nobody caught me. I could not be sure whether somebody did see me through a window but there was not much I could do with that. `);
				link(`Locker. `,245)
			};
			break;
		
		case 241: 
			kat(`Are you crazy?!? I'm supposed to wear this!?! Can't you bring me some proper clothes? `);
			qaa(`Why do you have to always be such a bitch?! `);
			kat(`Why do you have to be always so useless? `)
			qaa(`You know what? Fuck you. I don't deserve this. You're on your own! `)
			txt(`He grabbed the <i>skirt</i> and ripped it from my hands. `);
			kat(`Wait!!! I didn't mean... ....fuck. `);
			txt(`He just let me there! `);
			
			txt(`Luckily, the classes already started so the chance I will meet somebody in hallways was low. I rapidly came up with a plan. There was a bag with spare gym clothes in my locker. It would not be perfect but still better than nothing.`);
			txt(`I only had to get from the first floor to the basement. `);
			txt(`I was sneaking through the empty halls, terrified that at any moment anybody can open and door. It was nerve-racking. `);
			txt(`But I was very fortunate and nobody caught me. I could not be sure whether somebody did see me through a window but there was not much I could do with that. `);
			link(`Locker. `,245)
			break;
					
		case 242: 
			kat(`Thank you. `);
			txt(`I reluctantly thanked him. That jerk brought me an humiliatingly tiny microskirt but it was still slightly better than nothing. `);
			qaa(`You're welcome, `+NAME.katarina+`. `);
			WARDROBE.createWear(`MicroSkirt`);
			if(!PC.panties){
				txt(`Unfortunately, the skirt was so short, I spent the rest of the day absolutely terrified that a clumsy move will expose to everybody that I was not wearing panties. `);
			}else{
				txt(`I was forced to spent the rest of day in the most embarrassing skirt you can imagine. And I am sure perverts like you can imagine very embarrassing garments. `);
			};
			link(`Afternoon.`,1702);
			break;
		
		case 233: 
			txt(`I did not want to get my friends involved, this situation might be really had to explain. But I did not come up with any other solution. `);
			txt(`Luckily, the nerds let me keep my cards, id and phone. `);
			kat(`Eh, hello, `+NAME.sas+`. I need you to take keys from my bag, go to my locker and bring me my gym clothes. `);
			sas(`But why? `);
			kat(`OMG! Do you have to be so nosy every time I need your help? `);
			sas(`Sorry. Bring where? `);
			kat(`Male bathrooms on the first floor. `);
			sas(`Male bathroom?!? `);
			kat(`Yea. Be quick! `);
			link(`Wait. `,234);
			break;
			
		case 234:
			mile.bottomlessBathrooms = 1;
			txt(`When the doors opened, I rushed to hide in a cabin in case it was somebody else than `+NAME.sas+`. `);
			txt(`As I quickly found out, it was not  `+NAME.sas+`. `);
			eva(NAME.kat+`? `);
			txt(`Damn! It was `+NAME.eva+`! `);
			kat(`Hi?`);
			eva(NAME.sas+` rushed to the class so I offered my selfless help.  `);
			kat(`Thanks. `);
			eva(`What are you half-naked? `);
			kat(`I'll explain later. `);
			txt(`However, unlike `+NAME.sas+`, `+NAME.eva+` was impossible to easily palm off. `);
			kat(`Well, my jeans got ripped. `);
			eva(`At male bathrooms?!?! `);
			kat(`NO!! Of course not! But... well they were the closest.... `);
			eva(`Sure. That seems like a believable explanation.  `);
			txt(`What a bitch! `);
			link(`Get dressed. `,245)
			
			

		
		 
		
		
//	mile.saw_panties = true;	
//mile.flash_panties_sexy = true;		
	
	txt(`They dropped my butt on the cold tiles. I was terrified how they going to punish me but they just grabbed my `+dh.decapitalise(PC.lower.name)+` and left. `);
			txt(`Anxiously I expected how they will punish me but they just grabbed my `+dh.decapitalise(PC.lower.name)+` and left. `);
		
	
	
	qaa(`Whoa, slow down! @katalt, you constantly break our rules. We tried to be magnanimous but we cannot longer ignore your misconduct. You have to be punished!
	if(mile.sub <
	
	

//no pants 
txt(NAME.qcc+` praised my look but `+NAME.qaa+` and `+NAME.qbb+` looked less satisfied. `);
				qaa(`We need to talk. `);
				txt(`They grabbed me and dragged me to the bathrooms. `);
				kat(`What's going on? `);
				qaa(`What are you supposed to wear? `);
				kat(`...skirts? But that's pretty dumb, I think I look pretty sexy in pants too...`);
				qbb(`Nobody fucking asked you about your stupid opinions!` );
				qaa(`You yourself agreed to cooperate with us. If you won't, we'll have to release everything we have on you. `);
				qcc(`Or like third of it. Just enough to make you a mocked outcast but not enough to ruin your life completely. So we will be still able to blackmail you with the rest. `);
				qaa(`You want that? `);
				kat(`... no. `);
				qbb(`Strip down your pants! `);
				link(`Okay.`,221);
				link(`No.`,222);
				
				
bb(`Nobody fucking asked you about your stupid opinions!` );
qaa(`You yourself agreed to cooperate with us. If you won't, we'll have to release everything we have on you. `);
qcc(`Or like third of it. Just enough to make you a mocked outcast but not enough to ruin your life completely. So we will be still able to blackmail you with the rest. `);
qaa(`You want that? `);
kat(`... no. `);
qbb(`Strip down your pants! `);
*/



 






/*
 
story.punishment = function(input){
	switch(input){
		case 100:
		//	WARDROBE.showLess(-1,1);
	
			mile.ruler = true;
			if(mile.qaa>0 && mile.burned!=true){
				qaa(`Is that necessary? What if she promises she won't be disobedient again? `);
				if(mile.qcc>2){
					qcc(`I don't think we should be too harsh on her! `)
				}else if(mile.qcc>1){
					qcc(`Yeah?`)
				}
			}else{
				qaa(`Exactly. She needs to be punished. `);
				if(mile.qcc>2){
					qcc(`You are too harsh on her! I don't think this is necessary. `);
				}else if(mile.qcc>1){
					qcc(`Is that... necessary? `);
				}
				
			};
			qbb(`It's a matter of principle. We can't be inconsistent. Have you ever trained a dog? `);
			if(mile.burned){
				qaa(`Have you?`);
				qcc(``+NAME.kat+` is not a dog!`);
			}else{
				qcc(`Have you?`);
				qaa(``+NAME.kat+` is not a dog!`);
			}
			kat(`Thank you for standing up for me, chief.`);
			qbb(`Yeah, she is a bitch. Give me your ruler, `+NAME.qcc+`! `);
			
			if( (PC.lower && PC.lower.innerLoose>=1) ){
				txt(``+NAME.qcc+` took from his bag his dorky blue 40-centimeter ruler. `+NAME.qbb+` pushed me against the wall and flipped up my skirt. It took me too long to realize what was his intention. `);
			}else{
				txt(``+NAME.qcc+` took from his bag his dorky blue 40-centimeter ruler. `+NAME.qbb+` pushed me against the wall: `);
				qbb(`Pull down your pants! `);
				kat(`What?!`);
				qbb(`PULL DOWN YOUR PANTS! `);
				txt(`He screamed at me. Confused I unbuttoned my pants and let them slide down. Too late I realised what was his intention. `)
			}
			txt(`He smacked my butt. I squealed in pain and `+NAME.qcc+` screamed in panic. `);
			qcc(`What the hell are you doing! You'll break it! `);
			qbb(`What? No way! Look: `);
			txt(`Terrified `+NAME.qcc+` watched how `+NAME.qbb+` bend the ruler in a sharp arc. My bottom itched and I really hoped he will break it. But he did not. `);
			qbb(`See! Now is your turn! `);
			txt(`He threw the ruler to `+NAME.qcc+` who nervously smacked my butt again. He was afraid to hit me too hard and it barely hurt. `);
			qaa(`Mediocre!`);
			qbb(`Try again!`);
			txt(`I really felt the next hit. `);
				
			link(`Cry and beg them to stop. `,101);
			link(`Endure the spanking. `,102);
			link(`Encourage them. `,103);
			break;
			
		case 101:
			mile.sub++;
			kat(`Please stop! It hurts so much! `);
			txt(`I desperately whined and begged them to stop. The ruler stung a bit but I seriously overreacted and the more I cried the less vigorous was their spanking. `);
			txt(``+NAME.qaa+` ended with the last feeble hit: `);
			qaa(`That... might be enough. `);
			kat(`Please, don't beat me anymore! I promise I won't do that again! I'll do anything you want! Just don't hurt me! `);
			txt(`I heartwrenchingly sobbed. `);
			qaa(`We are really sorry! Trust us, it hurt us more than you! But you have to exactly follow our task, that was the deal. `);
			kat(`Yes, yes I will!`);
			txt(`I sobbed and they finally let me go. `);
			link(`The rest of the day. `,null, () => {WARDROBE.showAll(); set.skipBack(); }	 );
			break;
			
		case 102:
			txt(`I tried to mentally zone out and don't act undignified. The sharp hits hurt a bit but nothing too terrible and I withstood the whole execution without a word or a moan. `);
			txt(``+NAME.qaa+` ended with the last swift hit. `);
			qaa(`That might be enough. Trust us, it hurt us more than you! But you have to exactly follow our task, that was the deal. `);
			link(`Okay. `,null, () => {WARDROBE.showAll(); set.skipBack(); }  );
			break;
			
		case 103:
			mile.sub--;
			mile.ruler_broken = true;
			kat(`Is that all you got? I barely felt that! `);
			txt(`Which was a lie, it hurt a quite bit, but I certainly did not want to give them the satisfaction. `);
			txt(`However, I paid for my brattiness dearly. They were angry their disciplining had no effect on me and smacked my poor bottom without restraints. `);
			kat(`Harder! `);
			txt(`I shouted trying to suppress the pain. `+NAME.qbb+` smacked my ass especially violently. Tears burst from my eyes but I was able to wipe them out before they noticed - they were distracted by something else. The top half of the ruler clanged on the floor while the bottom one was still in `+NAME.qbb+`'s hand.	`);
			
			qcc(`You broke it, you dumb fucking idiot! `);
			qbb(`Oops? It's not my fault you had such a shoddy ruler! `);
			qcc(`Shoddy?! SHODDY?!? `);
			
			if(!mile.burned){
				qaa(`You didn't have to spank her so hard! `);
				qbb(`What?!? She was asking for it! `);
			}else{
				qaa(`Calm down! It may be fixed... `);
				qcc(`Fixed?! FIXED!? This was a precise tool! `);
			};
			
			txt(`I loudly laughed and they looked very unhappy and pissed. `);
			link(`So long! `,null, () => {WARDROBE.showAll(); set.skipBack(); }	 );
			break;	
	}
}
*/