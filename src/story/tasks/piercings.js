


import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects, choices_required} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";
import {rollUpSkirt, emo, drawZoomed, drawUnzoomed, wdesc, removeActual, update, updateDraw, effigy, allClothes, crew, wearRandomClothes, remove, wear, create, wears, showLess, showAll, violates, INVENTORY, sluttiness, createWear, quickSaveOutfit, quickLoadOutfit  } from "Avatar/index";
import {stringifyHSLA} from "Libraries/color/index";
import {cloneFigurine} from "Avatar/canvas/body";
import {next_event} from "Taskmaster/scheduler";
import {placetime, task_intro,  task_reaction} from "Loop/text_extra";

import {payment} from "System/bank_account.js";
import {money} from "Data/items/prices.js";


let figurine;
function pierce(name, slot, blueprint){
	//not displayed (tongue)
	if(!blueprint){
		txt(`${choice(name, slot)}`);
		effects([{
				id: `initial`,
				fce(){
					counter[slot] = false;
				},
			},{
				id: slot,
				fce(){
					counter[slot] = true;
				},
		}]);		
	}
	
	//creates and displays green placeholder 
	const item = create(blueprint);
	item.hue = 100;
	item.light = 50;
	item.satur = 100;
	item.fill = stringifyHSLA(item);
	item.stroke = item.fill;
	item.radius = 2 * item.radius;
	
	//already has one
	if(PC.apex[slot]){
		wear(item, figurine);
		updateDraw(figurine);
		return;
	}
	
	//creates choice
	txt(`${choice(slot, name)}`);
	effects([{
			id: `initial`,
			fce(){
				counter[slot] = false;
				remove(slot, figurine);
				updateDraw(figurine);
			},
		},{
			id: slot,
			fce(){
				counter[slot] = true;
				wear(item, figurine);
				updateDraw(figurine);
			},
	}]);
}
			
			
export const jewels = {
	first(subject){
			
			figurine = cloneFigurine(subject);
			updateDraw(figurine);
			
			//already has those, they are only displayed
			pierce(`Bellybutton piercing`, `bellybutton`,`bellyPiercingSimple`);
			pierce(`Earrings`, `earrings`,`ballEarrings`);
			
			//face piercings
			pierce(`Eyebrow`, `eyebrow`,`eyebrowPiercingLeft`);
				
			pierce(`Nose bridge`, `noseTop`,`noseBridge`);
			pierce(`Nostril`, `noseSide`,`nosePiercing`);
			pierce(`Septum`, `noseBot`, `noseRingSimple`); 
				
			pierce(`Medusa piercing above lips`, `lipsTop`,`medusa`);
			pierce(`Labret piercing below lips`, `lipsBot`,`labret`);
			pierce(`Monroe piercing close to lips`, `lipsSide`, `monroeRight`);
				
			pierce(`Tongue piercing`, `tongue`,`tonguePiercing`);
			
			if(mile.slut > 1){ //TODO SLUT
				pierce(`Nipple piercings`, `nipples`,`nippleBars`);
			}else if(hardbug){
				pierce(`##Nipple piercings (low slut)`, `nipples`,`nippleBars`);
			}
			
			if(mile.slut > 6){ //TODO SLUT
				pierce(`Vertical clit hood piercing`, `vch`,`VCHPiercing`);
				//pierce(`Labia piercing`, `labia`,`labiaPiercing`);
			}else if(hardbug){
				pierce(`##Vertical clit hood piercing (low slut)`, `vch`,`VCHPiercing`);
				//pierce(`##Labia piercing (low slut)`, `labia`,`labiaPiercing`);
			}
				
	},
	
	second(subject){
			
			figurine = cloneFigurine(subject);
			updateDraw(figurine);
			
			//already has those, they are only displayed
			pierce(`Bellybutton piercing`, `bellybutton`,`bellyPiercingSimple`);
			pierce(`Earrings`, `earrings`,`ballEarrings`);
			
			//face piercings
			pierce(`Eyebrow`, `eyebrow`,`eyebrowPiercingLeft`);
				
			pierce(`Nose bridge`, `noseTop`,`noseBridge`);
			pierce(`Nostril`, `noseSide`,`nosePiercing`);
			pierce(`Septum`, `noseBot`, `noseRingSimple`); 
				
			pierce(`Medusa piercing above lips`, `lipsTop`,`medusa`);
			pierce(`Labret piercing below lips`, `lipsBot`,`labret`);
			pierce(`Monroe piercing close to lips`, `lipsSide`, `monroeRight`);
				
			pierce(`Tongue piercing`, `tongue`,`tonguePiercing`);
			
			//nipples
			pierce(`Nipple piercings`, `nipples`,`nippleBars`); //nipple piercings could be forced 
			pierce(`More nipple piercings`, `nipplesMulti`,`verticalNippleBars`);
			
			if(mile.slut > 5){ //TODO SLUT
				pierce(`Labia piercing`, `labia`,`labiaPiercing`);
				pierce(`Christina piercing`, `pubic`,`christinaPiercing`);
				pierce(`Vertical clit hood piercing`, `vch`,`VCHPiercing`);
				pierce(`Horizontal clit hood piercing`, `hch`,`HCHPiercing`);
				
			}else if(hardbug){
				pierce(`##Labia piercing (low slut)`, `labia`,`labiaPiercing`);
				pierce(`##Christina piercing (low slut)`, `pubic`,`christinaPiercing`);
				pierce(`##Vertical clit hood piercing (low slut)`, `vch`,`VCHPiercing`);
				pierce(`##Horizontal clit hood piercing (low slut)`, `hch`,`HCHPiercing`);
			}
		
		
		
		
	},
	
	
}
			
export const piercings_initiation = (index)=> {
	task_intro();
	qaa(`Your next task will be fairly straightforward. We decided that you should get a piercing. `);
	kat(`I thought the deal was you'll not ask me for anything extreme? `)
	qcc(`Since you already have earrings and navel piercing we assumed you're not against the whole concept. `);
	qbb(`And piercings shouldn't be considered to be extreme since you can easily remove them and the hole will close. `)
	qaa(`And you can pick whatever piercing you want! If you don't want anything showy, you can pick am more subtle, hidden one. `);
	if(mile.skirt == 1) qbb(`But don't try to be a smartass like with that skirt - just a new pair of earrings won't cut it. `)
	kat(`But you'll want to see it, won't you? `);
	qcc(`Yeah!... I mean.. Yes. `);
	qaa(`But just to check you actually got it, we promise no needless ogling. `);
	next();
}

export const piercings_action = (index)=> {
	switch(index){
		default:
		case 101: 
			set.irl();
			placetime("piercing parlor");
			txt(`So after the classes, I went to the place where I get my navel piercing. That was like a year ago. Back then I was scared and I spent a lot of time searching for the right, trustworthy place. And they did a great, professional job. I was almost surprised how quick and easy it was. `);
			txt(`The nerds were not wrong, I did not oppose piercings and always assumed I might get more at some point in the future. Well, I guess it will be today. `);
	
txt(`#TODO: at the moment, getting pierced is free. `);	//TODO ASAP
			link(`Enter the shop. `, 200);
			break;
			
		case 200:
			set.irl();
			counter = {}; //BEWARE
			
			/*
			figurine = cloneFigurine(PC);
			updateDraw(figurine);
			emo("focus");
			*/
			
			txt(`I had only to decide which part of my anatomy I would like to get pierced. `);
			
			jewels.first();
					
			choices_required(1, 3);
			link(`Done. `, 201);
			break;
			
		case 201:
				Object.keys(counter).forEach( a => {
					if(counter[a]) PC.apex[a] = true 
				});
				counter = {};
				INVENTORY.shop("piercing", next_event);
				break;
	}
}

export const piercings_reaction = (index)=> {
	function piercings_links(){
		if(PC.tongue && !counter.tongue) link(`Stick out tongue. `, 110);
		if(wears.piercedNipples && !counter.nipples) link(`Strip your top. `, 111);
		if(wears.piercedGenitals && !counter.pussy) link(`Strip your @lower. `, 112);
		if(counter.face || counter.tongue || counter.nipples || counter.pussy) link(`That's all! `, 200);
		if( !(PC.tongue || wears.piercedFace || wears.piercedNipples  || wears.piercedGenitals) ) link(`I kinda forgot. `, 201);
	}
	
	switch(index){
		default:
		case 101: 
			task_reaction();
			mile.piercings = 0;
			txt(`I met with the nerds before classes. They were very eager to see my new piercing. `); 
			
			if(wears.piercedFace){
				counter.face = true;
				drawZoomed("face");
				mile.piercings = 2;
				qcc(`Wow. `);
				txt(`@qcc was immediately excited when he saw me. `);
				txt(`@qbb nodded, only @qaa was not very excited: `);
				qaa(`A bit trashy. `);
				qbb(`Just like she is! `);
				kat(`Thanks for the compliment, you're always so charming! `);
				qcc(`Is that everything? `);
								
			}else{
				qaa(`So? How did you cope with your task? `);
				qcc(`Well, your nose isn't pierced so we have to wonder.... where? `);
				qbb(`Don't be shy and start stripping. `);
			}
			
			piercings_links();
			break;
	

		//tongue
		case 110:
			drawZoomed("face");
			counter.tongue = true;
			mile.saw_tongue_piercing = true;
			if(mile.piercings < 1) mile.piercings = 1;
			txt(`I sticked out my tongue at them. `);
			qbb(`I wonder how that would feel during oral. `);
			if( (mile.advice_3 === 2 || mile.advice_4 === 2) ){
				//txt(`I shivered when he mentioned that. I really hoped I will not ever have to suck his dick again. `);
				txt(`@qbb quickly winked at me, fortunately @qaa and @qcc did not notice. `);
				kat(`...you ...you'll never find out! `);
				qbb(`Or will I?`);
			}else{
				kat(`Too bad you'll never find out. `);
				qbb(`Or will I?`);
			}
			
			if(mile.a_love > 2){
				qaa(`Stop being always such a creep. `);
				qbb(`What? Are you jealous? Would you mind if she sucked my dick and not yours? `);
				qaa(`Shut up!`);
			}else if(mile.a_burned && mile.slut > 5){
				qaa(`I'm surprised she did not get one sooner, to boost her proverbial cock-sucking abilities. `);
			}
			
			piercings_links();
			break;
			
		//nipples
		case 111: 
			counter.nipples = true;
			mile.saw_nipple_piercing = true;
			if(mile.piercings < 3) mile.piercings = 3;
			mile.slut++;
			showLess(0,-1);
			drawZoomed("chest");
			
			if(mile.slut > 6){
				emo("imp");
				txt(`I kinda enjoyed stripping in front of those nerds, their pathetic stares locked at my chest. I took my time and gave them a good show. `);
				txt(`Nerds were really amazed by my bosom and new nipple piercings. I really should have them pierced a long time ago.  `);
			}else{
				emo("shy");
				txt(`I hated stripping in front of them. I tried to make it as quick and unerotic as possible. `);
				txt(`Nerds were really amazed by my bosom and my brand new nipple piercings. `);
			}
			
			qcc(`Wow. `);
			qaa(`You passed this task with flying colors. `);
			qbb(`Good choice. `);
			qcc(`I hoped but I assume she will go for something lamer. `);
			qbb(`Nah, I told you she is a slut who will definitely get her nipples pierced. `);
			
			if(mile.a_burned){
				qaa(`Yeah, sluts like her love drawing attention to their tits. `);
				qcc(`And that's a good thing! `);
				if(mile.slut < 4 && mile.sub < 3) kat(`Hey! I'm not a slut! `)
			}else if(mile.a_dating){
				qaa(`Having her nipples pierced doesn't mean she is a slut. `);
				qbb(`I really think it does. `);
				qcc(`There is nothing wrong with being a slut. `);
				qaa(`Don't listen to them, you are amazing, @kat! `);
			}else{
				qaa(`You are great @kat! `);
			}
			
			piercings_links();
			break;
			
		//genitals
		case 112:
			counter.pussy = true;
			mile.saw_pussy = true;
			mile.saw_genital_piercing = true;
			if(mile.piercings < 3) mile.piercings = 4;
			
			mile.slut++;
			mile.slut++;
			showLess(-1,0);
			drawZoomed("groin");
			
			
			if(mile.slut > 7){
				emo("imp");
				txt(`With a smile, I unzipped my @lower. `);
				kat(`You really want to see my brand new piercing, don't you? `);
				txt(`I teased them. It made them pretty hyped with high expectations.  `);
			}else{
				emo("shy");
				txt(`With sight, I unzipped up my @lower. Needless to say, this move made them pretty hyped with high expectations. `);
			}
			
			if(!PC.panties && !ext.rules.no_panties){ //TODO - NOT RULE!
				mile.saw_commando = true;
				qbb(`You don't even bother with panties anymore, do you? `);
				if(mile.slut < 8){	//TODO MILE.SLUT
					txt(`I blushed. `);
					qcc(`Nice! `);
				}else{
					txt(`I shrugged. `);
					kat(`I guess I don't! `);
				}
			}
			
			if(!mile.saw_hairy && wears.hairyPussy){
				mile.saw_hairy = true;
				//cant find under that bush?
				qbb(`Nice beaver! `);
				kat(`Shut up!`);
			}
			
			txt(`They moved closer to admire my new piercing. `);
			qaa(`Wow. `);
			qbb(`Nice! `);
			qbb(`You should definitely get your clit pierced! `);
			kat(`Are you crazy?!`);
			qcc(`Did it hurt, getting pierced... there? `);
			kat(`Immensly!`);
			txt(`I ensured him and enjoyed his uneasy look. `);
			//qcc(`I like it. It looks really great. `);
			//qaa(`You definitely passed this task. `);
			
			piercings_links();
			break;	



		case 200:
			drawUnzoomed();
			showAll();
			
			if(counter.nipples || counter.pussy){
				emo("relax");
				if(mile.slut < 10){ //TODO!
					qbb(`You very pleasantly surprised us, @kat! You're more daring than we thought! `);
					kat(`You have no idea how daring I am! `);
				}else{
					qbb(`We knew how daring you are, @kat, but still, great job! `);
					kat(`I'm happy I made you proud! `);
				}
				if(counter.pussy){
					qcc(`I would never think you'll actually get your clit pierced! `);
				}else{
					qcc(`The nipple piercings are perfect for you, they amazingly complete your look! `);
				}
				
				next();
			}else{
				emo("anoy");
				if(mile.slut < 6){ //TODO!
					qcc(`Why are always such a gutless prude? `);
					kat(`What do you mean? `);
					qcc(`You would look so hot with nipple bars! But you're too afrad! `);
					kat(`I'm not afraid of anything! I just don't want to get my nipples pierced just to entertain three perverted nerds! `);	
				}else{
					qaa(`You pretty disappointed us today! `);
					kat(`Come on! `);
					qcc(`Seriously! Imagine how hot would you look with nipple piercings! You're not bashful, are you? `);
					kat(`I'm not bashful! I just don't want to get my nipples pierced just to entertain three perverted nerds! `);
				}
				qcc(`Excuses. `);
				qbb(`He's right, you're a chicken. `);
				kat(`I'm not a chicken! `);
				qcc(`Prove it! `);
				qbb(`By getting your nipples pierced! `);
					
				next(`No!`);
				next(`Never!`);
				next(`Dream about it, creeps!`);
			}
			
			break;
			
		case 201:
//TODO
			drawUnzoomed();
			showAll();
			next();
			break;
	}
}	











export const piercings_2_initiation = (index)=> {
	mile.piercings_2_nipples = false; 
	
	//nice
	if(aly.s > 1){
		
		if(emy.s){
			emy.abc(`Your next task will be extremely painful. `);
			kat(`What?!?`);
			reac(`Don't worry, he's just making fun of you. `);
			kat(`Okay?`);
		}
		
		if(mile.piercings){
			aly.acb(`I think you'll enjoy the task, you like piercings, don't you? `);
		}else{
			qcc(`We decide you might look even better with a few more piercings.`);
		}
		kat(`You want me to get another piercings?`);
		if(mile.piercings === 4){
			qbb(`The last time you seriously suprised us!`);
			qaa(`We really didn't expect that.`);
			qcc(`But there a are still a few other places you could get pierced.`);
		}else if(mile.piercings === 3){
			qbb(`The last time you nicely suprised us!`);
			qcc(`But there are ways to go even naughtier and top that.`);
			qaa(`Just a suggestion.`);
		}else{
			qbb(`The last time you disappointed us a little.`);
			qcc(`We expected a bit more daring piercings.`);
			qaa(`But you have a unique opportunity to correct that.`);
		}
		kat(`But I still choose any piercing I want?`);
		aly.abc(`Of course, we don't want to force you into anything you wouldn't like. `);
		emy.ab(`Some of us wanted but were overvoted. `);
		next();
	
	}else if(mile.piercings <= 2 && emy.s){
		mile.piercings_2_nipples = true; //nipples 
		emy.cba(`Let's be honest, you extremely disappointed us when we asked you to get a piercing. `);
		kat(`I did exactly what you told me to do and got a piercing!`);
		emy.abc(`You disappointed us by being the classic @kat and always lazily doing the least amount of work possible!`);
		kat(`That isn't true!`);
		if(mile.slut > 5){
			emy.bacf(`We seriously expected more from a slut like you! But trusting your slutty instinct and giving you too much freedom was a mistake.`);
		}else{
			emy.bacf(`But honestly, it was our mistake, giving a gutless tease like you too much freedom. `);
		}
		reac.cba(`The mistake we're going to fix!`);
		kat(`What do you want?`);
		qaa(`We want you to get your nipples pierced.`);
		kat(`MY NIPPLES?!?`);
		qaa(`Exactly those.`);
		kat(`Both of them?!?`);
		qcc(`We believe asymmetrical piercings lack the desired aesthetical quality.`);
		kat(`My poor nipples!`);
		qcc(`Think about how sexy you'll look with cute nipple bars!`);
		qbb(`Or huge slutty rings!`);
		next();
	
	}else{
		aly.abcf(`We were quite impressed when we asked you for a piercing - any piercing you wanted - and you got you willingly got your ${mile.piercings === 4 ? "clit" : "nipples"} pierced! `);
		emy.abc(`Ehh, it was quite expectable from an attention-addicted slut like @kat.`);
		kat(`What is the next task?`);
		emy.ba(`Something a piercing-obsessed tramp like you will definitely enjoy!`);
		qcc(`We concluded you would look even sexier with even more piercings.`);
		kat(`You want me to get even more piercings? Are you already running out of ideas?`);
		qaa(`Yes and no.`);
		if(mile.piercings === 4){
			qcc(`The nipple piercings might be hot and naughty but there's a way to top them if you know what I mean!`);
		}else if(!PC.apex.nipples){
			qcc(`You would look extremely hot with nipple piercings - just a suggestion!`);
		}else{
			qcc(`Maybe one or two face piercings would be nice? You know, for a change?`);
		}
		kat(`...I will consider it.`);
		txt(`Maybe getting my ${mile.piercings === 4 ? "clit hood" : "nipples"} pierced was a mistake, it was only encouraging their twisted fantasies.`);	
		next();
	
	}
}


export const piercings_2_action = (index)=> {
	switch(index){
		default:
		case 101: 
			set.irl();
			placetime("piercing parlor");
			txt(`So after the classes, I again went to the piercing parlor. `);
			if(mile.piercings_2_nipples){
				showLess(0,1);

				if(mile.slut < 5){
					txt(`I was a bit embarrassed when I explained what I wanted and when I was forced to strip down.	However, they were professionals. After a short consultation and two consecutive quick sensations of pain caused by the hollow needle penetrating my nipples, I had two new piercings. `);
				}else{
					txt(`I was not particularly excited about getting my nipples pierced. I was sure they will look amazing and sexy but being forced to suffer just because of nerds' whims was terribly annoying. Anyway, after a short consultation and two consecutive quick sensations of pain caused by the hollow needle penetrating my nipples, I had two new piercings. `);
				}
				
				createWear("nippleBars");
				PC.apex.nipples = true;
				emo("pain");
				link(`Consider other piercings. `,  200);
				link(`Done. `, 201);
			}else{
				link(`Choose new piercing. `, 200);
			}
			
			break;
			
		case 200:
			set.irl();
			counter = {}; //BEWARE
			
			/*
			figurine = cloneFigurine(PC);
			updateDraw(figurine);
			//emo("focus");
			*/
			
			jewels.second();
			
			if(mile.piercings_2_nipples){
				choices_required(0, 2); //one already done
			}else{
				choices_required(1, 3);
			}
			link(`Done. `, 201);
			break;
			
		case 201:
				Object.keys(counter).forEach( a => {
					if(counter[a]) PC.apex[a] = true 
				});
				if(counter.nipplesMulti){ //behaviour unexpected anyway (it assumes being expansion of existing "nipples"), this way it is in players benefit
					counter.nipples = true;
					PC.apex.nipples = true;
				}
				//counter = {}; //TODO - COUNTER HERE IS NOT CLEARED, UNLIKE THE 1ST PIERCINGS
				ext.counter = {...counter};
				INVENTORY.shop("piercing", next_event);
				break;
	}
}


export const piercings_2_reaction = (index)=> {
	function piercings_links(){
		if(PC.tongue && counter.tongue && !counter.done_tongue) link(`Stick out tongue. `, 110);
		if( ((counter.nipples && !mile.saw_nipple_piercing) || counter.nipplesMulti ) && !counter.done_nipples) link(`Strip your @top. `, 111);

		if(!counter.done_pussy && wears.piercedGenitals){
			if(!mile.saw_genital_piercing || (PC.labia || PC.hch || PC.pubic) ){
				link(`Strip your @lower. `, 112); 
			}
		}
		
		if(counter.new_face || counter.done_tongue || counter.done_nipples || counter.done_pussy){
			link(`That's all! `, 200);
		}else{
			link(`I kinda forgot. `, 201);
		}
	}
	
	switch(index){
		default:
		case 101: 
			counter = {
				...counter,
				...ext.counter,
			}
			ext.counter = {};
			counter.new_face = ["eyebrow", "noseTop", "noseBot", "lipsTop", "lipsBot", "lipsSide"].filter( key => counter[key] && PC[key] ).length;
			counter.old_face = ["eyebrow", "noseTop", "noseBot", "lipsTop", "lipsBot", "lipsSide"].filter( key => !counter[key] && PC[key] ).length;
			counter.new_genital = ["vch", "labia", "hch", "pubic"].filter( key => counter[key] && PC[key] ).length;
			
			task_reaction();
			if(mile.piercings_2_nipples){
				drawZoomed("chest");
				counter.done_nipples = true;
				showLess(0, 1);
				mile.piercings_2 = 3;
				
				qaa(`So? Did you get your nips pierced, as we asked?`);
				kat(`Yes, I did.`);
				if(mile.slut < 6){
					qaa(`...`);
					kat(`...`);
					emy.cbaf(`We'll need a visual confirmation. `);
					txt(`I sighed and reluctantly flashed them my tits.`);
				}else{
					txt(`Before they could ask, I already moved my top to flash them my tits.`);
				}
				qaa(`Good.`);
				qcc(`I think you'll agree you look especially sexy with pretty jewelry in your nipples.`);
				kat(`I guess.`);
				emy.abc(`Trashy slut should have her nipples pierced! `);
				if(counter.new_face){
					counter.done_face = true;
					txt(`I frowned at him.`);
					aly.caf(`Anyway, despite your eye-catching boobs, don't think we didn't notice your new face ${counter.new_face > 1 ? "piercings. They make" : "piercing. It makes"} your gorgeous face even gorgeouser! `);
					reac.abc(`Are you trying to convince us you're able to do more than the bare minimum? Any more surprises? `);
				}else{
					reac.abc(`Is that all? Or maybe you decided to be a good girl and surprise us with something else? `);
				}
				if(counter.nipplesMulti){
					kat(`I got my nipples pierced multiple times, that should be enought for you! `);
				}
				
			}else{
				txt(`I met with the nerds who were extremely curious about my new piercing. `); 
				if(counter.new_face){
					drawZoomed("face");
					mile.piercings_2 = 2;
					counter.done_face = true;
					
					if(counter.new_face && counter.old_face){
						qbb(`Are those new or old face piercings?`);
						qaa(`Definitly new, she didn't have ${counter.new_face > 1 ? "those piercings" : "that piercing"} before.`);
					}else if(counter.new_face){
						qbb(`${counter.new_face > 1 ? "Are those new face piercings" : "Is that a new face piercing"}?`);
						qaa(`Yeah, definitely.`);
					}
					qcc(`Lovely new jewel! It makes your pretty face even prettier!`);
					kat(`Thanks I guess.`);
					qaa(`That would be enough to pass the task. Or maybe you have a surprise for us?`);
						
				}else{	
					if(counter.old_face){
						qbb(`${counter.new_face > 1 ? "Are those" : "Is that"} a new face piercing?`);
						qaa(`No, she had that one before.`);
						qbb(`Oh, right.`);
					}
					qaa(`So? What was your decision?`);
					qcc(`Tell us, @kat!`);
				}
			}
			
			piercings_links();
		break;
		
	//tongue
		case 110:
			drawZoomed("face");
			counter.done_tongue = true;
			mile.saw_tongue_piercing = true;
			if(mile.piercings_2 < 1) mile.piercings_2 = 1;
			
			txt(`I sticked out my tongue at them. `);
			qcc(`Cool!`);
			if(mile.b_domina){
				if(mile.a_burned && mile.slut > 5){
					qaa(`I'm surprised she did not get one sooner, to boost her proverbial cock-sucking abilities. `);
					kat(`Such a shame you'll never experience them!`);
					txt(`I smirked at him and he seemed mad.`);
				}else if(mile.a_dating){
					qaa(`I wonder how the piercing would feel during an oral sex...`);
					txt(`@qaa indiscreetly daydreamed.`);
					qcc(`I'm sure amazing!`);
				}
				
			}else{
				qbb(`I wonder how that would feel during oral. `);
				if( (mile.advice_3 === 2 || mile.advice_4 === 2 || mile.b_slave)  ){
					txt(`@qbb quickly winked at me, fortunately @qaa and @qcc did not notice. `);
					kat(`...you ...you'll never find out! `);
					qbb(`Or will I?`);
				}else{
					kat(`Too bad you'll never find out. `);
					qbb(`Or will I?`);
				}
				
				if(mile.a_love > 3){
					qaa(`Stop being always such a creep. `);
					qbb(`What? Are you jealous? Would you mind if she sucked my dick and not yours? `);
					qaa(`Shut up!`);
				}else if(mile.a_burned && mile.slut > 5){
					qaa(`I'm surprised she did not get one sooner, to boost her proverbial cock-sucking abilities. `);
				}	
			}
			
			piercings_links();
			break;
			
	//nipples
		case 111: 
			if(wears.visiblePiercedNipples){
				txt(`I pointed at my tits. The thin fabric of my @upper was not really hiding much.`);
				kat(`Isn't it obvious what else I got pierced?`);
				qaa(`We didn't want to be rude and waited until you mentioned it yourself. Would you mind giving us a better view?`);
			}
			
			if(mile.slut > 6){
				emo("imp");
				txt(`I kinda enjoyed stripping in front of those nerds, their pathetic stares locked at my chest. I took my time and gave them a good show. `);
				txt(`Nerds were really amazed by my bosom and new nipple piercings. `);
			}else{
				emo("shy");
				txt(`I hated stripping in front of them. I tried to make it as quick and unerotic as possible. `);
				txt(`Nerds were really amazed by my bosom and my brand new nipple piercings. `);
			}
			
			if(mile.saw_nipple_piercing){
				qbb(`Didn't you already got your nipples pierced the last time? `);
				qaa(`No, those are new. `);
				qcc(`Lovely addition, @kat!`); 

				if(mile.b_domina){
					qbb(`Yeah, they look nice.`);
					kat(`I didn't expect such appreciation from you. No rude comments about cheap nipple-pierced sluts?`);
					qbb(`...no.`);
					txt(`I teased @qbb. It was nice to see he learned to choose his words more carefully.`);
				}
				
			}else{
				qaa(`Wow. `);
				qcc(`Awesome! It took you two attempts but you finally dared! I think you won't regret following our suggestions! You look especially sexy with cute jewels next to your nipples.`);
				kat(`Yeah, I guess they are quite sexy.`);

				if(mile.b_domina){
					qbb(`Yeah, they look nice.`);
					kat(`I didn't expect such appreciation from you. No rude comments about cheap nipple-pierced sluts?`);
					qbb(`...no.`);
					txt(`I teased @qbb. It was lovely to see he learned to choose his words more carefully.`);
				}else{
					qbb(`Nah, I told you she is a slut who will definitely get her nipples pierced. `);
					if(mile.a_burned){
						qaa(`Yeah, sluts like her love drawing attention to their tits. `);
						qcc(`And that's a good thing! `);
						if(mile.slut < 4 && mile.sub < 3) kat(`Hey! I'm not a slut! `)
					}else if(mile.a_dating){
						qaa(`Having her nipples pierced doesn't mean she is a slut. `);
						qbb(`I really think it does. `);
						qcc(`There is nothing wrong with being a slut. `);
						qaa(`Don't listen to them, you are amazing, @kat! `);
					}else{
						qaa(`You are great @kat! `);
					}
				}
			}
			
			counter.done_nipples = true;
			mile.saw_nipple_piercing = true;
			if(mile.piercings_2 < 3) mile.piercings_2 = 3;
			if(mile.slut < 12) mile.slut++;
			showLess(0,-1);
			drawZoomed("chest");
			
			piercings_links();
			break;

	//genital
		case 112:
			kat(`Well, I kinda do have a surprise for you.`);
			qcc(`Really!?!`);
			if(mile.slut > 8){
				emo("imp");
				txt(`@qcc was excited when I winked at him and began unzipping my @lower.`);
			}else{
				emo("shy");
				txt(`I noded and with sight, I began unzipping up my @lower. It still felt a bit humiliating to undress in front of them.`);
			}
			if(!mile.saw_genital_piercing){
				qaa(`You got a new piercing <i>down there</i>?!?`);
				kat(`Yeah.`);
			}
			txt(`They moved closer to admire my new ${counter.new_genital > 1 ? "piercings" : "piercing"}. `);
			
			if(mile.piercings === 2){
				qbb(`Honestly I didn't expect this! Count me as surprised, @katalt.`);
				qaa(`Yeah, I too expected just nipple piercings but you went straight to...`);
				txt(`He pointed at my pussy.`);
			}else if(mile.piercings === 3){
				qbb(`Well, you definitely managed to top the last time.`);
				qaa(`Yeah, good job.`);
			}else if(mile.piercings === 4){
				qbb(`${counter.new_genital > 1 ? "Whoa, you got even more piercings?!?" : "Whoa, you got another piercing?"}`);
				qaa(`She did!`);
			}
			qcc(`I like the new ${counter.new_genital > 1 ? "piercings" : "piercing"}! Sexy, but noo too obnoxious, just perfect.`);
			
			if(mile.a_dating){
				qaa(`I hope it didn't hurt too much!`);
				kat(`Nothing I couldn't handle!`);
				txt(`I smiled at him, it was nice he was concerned.`);
			}else if(!mile.saw_genital_piercing){
				qcc(`Did it hurt, getting pierced... there? `);
				kat(`Immensly!`);
				txt(`I ensured him and enjoyed his uneasy look. `);
			}
			
			counter.done_pussy = true;
			mile.saw_pussy = true;
			mile.saw_genital_piercing = true;
			if(mile.piercings_2 < 4) mile.piercings_2 = 4;
			drawZoomed("groin");
			rollUpSkirt(3,0); //showLess(0,-1);
			
			 piercings_links();
		break;
			
			
	case 200:
			drawUnzoomed();
			showAll();
			emo("anoy");
			
			if(mile.piercings_2_nipples){
				qaa(`Good job. It wasn't so hard, was it?`);
				
				if(counter.new_face || counter.done_tongue || counter.done_nipples || counter.done_pussy){
					qcc(`And you even subverted our expectations by going beyond the minimum requirement.`);
					emy.ba(`It shows we can't go easy on her but when we push her and order her around, she quickly submits. `)
						
				}else{
					qcc(`Although it's kinda disappointing that you again didn't do anything beyond the required minimum.`);
					txt(`I'm just not into piercings that much!`);
				}
				
			}else if(mile.piercings <= 2 && mile.piercings_2 <= 2){
				qaa(`You passed the task but I have to say you again disappointed us!`);
				qcc(`I don't understand why you didn't get your nipples pierced! Your tits are great but it would move the up to another level!`);
				txt(`@qcc was exasperated but I just shrugged.`);
				emy.abc(`It shows we can't go easy on her but when we push her and order her around, she quickly submits. `)
				
			}else if(mile.piercings <= 2 && mile.piercings_2 >=3){
				qaa(`I think it's safe to say you fulfilled the task.`);
				qcc(`The last time you chickened out but today you nicely surprised us with your new naughty, sexy piercings!`);
				//BBB quip
			}else{
				qaa(`Good job, you fulfilled our task.`);
				//TODO
			}			

			if(!mile.piercings_2) mile.piercings_2 = -1; //TODO - fallback; check if nothing is broken
			next();
			break;
			
	case 201:
			if(!mile.piercings_2) mile.piercings_2 = -1; //TODO - fallback; check if nothing is broken
			txt(`omg you're playing the game wrong`);
			txt(`TODO`);
			next();
			break;
	}
}
		
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
	
			
			
			
			
			
/*			
			


//pierce 
			
			if(mile.tattoo_got &&  (mile.socialImplants > -1 || mile.sasa_video_demand) ){
				txt(`${NAME.alexandra} refused my suggestion to get her nipples pierced, too terrified to have her nipples pierced through with a thick needle. However, she offered to stay with me and support me, as long as she does not have to watch. `);
				
				if(mile.pierced_showed_clit){
					txt(`I was extremely annoyed, what else the nerds they wanted? `);
				}else if(mile.pierced_showed_nipples){
					txt(`The demands of the nerds made me a bit concerned, if getting my nipples pierce was not enough, what were they expecting? `);
				}else{
				
				}
	
			}else{	
				if(mile.pierced_showed_clit){
					txt(`After the school I visited the piercing studio. I was extremely annoyed, what else they wanted? `);
				}else if(mile.pierced_showed_nipples){
					txt(`After the school I visited the piercing studio. They demands made me a bit concerned, if getting my nipples pierce was not enough, what were they expecting? `);
				}else{
					txt(`After the school I visited the piercing studio, thinking what would be enough to satisfy their demands. `);
				}
			}
		
		
	
			

	
		//mile.pierced_showed_clit	
		//mile.pierced_showed_nipples		
				if(!face){
					txt(`A brief look did not reveal that I was wearing any new piercing. And that made the nerds even more excited.  `);
					
					if(tits){
						mile.pierced_showed_nipples = true;
						WARDROBE.showLess(0,-1);
						if(mile.nippleChain){
							//Without waiting for their instruction, I began removing my top. 
						}else if(mile.nippleChain){
							txt(`Without waiting for their instruction, I began removing my top. They could see my nipples were encirclet by several piercings. ${mile.slut > 7 ? `I lightly rubbed them with fingers just to tease them. ` : ``}  `);
						}else{
							txt(`Without waiting for their instruction, I began removing my top. They could see my nipples were pierced. ${mile.slut > 7 ? `I lightly rubbed them with fingers just to tease them. ` : ``}  `);
						}
					}
					
					if(cunt || labia){
						mile.pierced_showed_clit = true;
						WARDROBE.showLess(-1,0);
						txt(`To show them the piercing in my pubic region, I had to lift my skrit${PC.panties ? ` and push my panties away. ` : `. `}  `);
					}
					
					qcc(`Wow! `);
					qaa(`Nice. `);
					qbb(`I like them. Good job.  `);
					txt(`They very excited and approved my choice. `);
					qcc(`Did it hurt? `);
					kat(`Obviously! `);
					
					if(u.a){
						qaa(`Good. `);
					}else{
						qaa(`Oh no, poor girl! `);
					}
					
					if(cunt){
						qbb(`How does feel to have your clit pierced?  `);
						kat(`It doesn't go strainght through my clit, I would be afraid of that. But I had to admit... it's quite stimulating. `);
					};
					
					qcc(`Well, you passed this task. `);
					
				}else if(face && (tits || cunt)){
					txt(`One of my new piercings was obvious on the fist sight. A thin chain was hanging between my nose and ear and dangled whenever I jerked with head. Other piercings remained hidden.  `);
					if(d.a){
						qaa(`That looks kinda cool!  `);
					}else{
						qcc(`You look kinda cool!  `);
					}
					kat(`Thanks. `);
					if(u.a){
						qaa(`I hope that isn't all, slut.  `);
					}else if(u.b){
						qbb(`That better shouldn't be all! `);
					}else{
						qcc(`Is that everything?  `);
					}
					
					if(tits){
						mile.pierced_showed_nipples = true;
						WARDROBE.showLess(0,-1);
						if(mile.nippleChain){
							//Without waiting for their instruction, I began removing my top. 
						}else if(mile.nippleChain){
							txt(`Without waiting for their instruction, I began removing my top. They could see my nipples were encirclet by several piercings. ${mile.slut > 7 ? `I lightly rubbed them with fingers just to tease them. ` : ``}  `);
						}else{
							txt(`Without waiting for their instruction, I began removing my top. They could see my nipples were pierced. ${mile.slut > 7 ? `I lightly rubbed them with fingers just to tease them. ` : ``}  `);
						}
					}
					
					if(cunt || labia){
						mile.pierced_showed_clit = true;
						WARDROBE.showLess(-1,0);
						txt(`To show them the piercing in my pubic region, I had to lift my skrit${PC.panties ? ` and push my panties away. ` : `. `} `);
					}
					
					qcc(`Wow! `);
					qaa(`Nice. `);
					qbb(`I like them. Good job.  `);
					txt(`They very excited and approved my choice. `);
					qcc(`Did it hurt? `);
					kat(`Obviously! `);
					
					if(u.a){
						qaa(`Good. `);
					}else{
						qaa(`Oh no, poor girl! `);
					}
					
					if(cunt){
						qbb(`How does feel to have your clit pierced?  `);
						kat(`It doesn't go strainght through my clit, I would be afraid of that. But I had to admit... it's quite stimulating. `);
					};
					
					qcc(`Well, you passed this task. `);
			
			
				}else{
					txt(`My new piercings was obvious on the fist sight. A think chain was hanging between my nose and ear and dangled whenever I jerked with head. `);
					if(d.a){
						qaa(`That looks kinda cool!  `);
					}else{
						qcc(`You look kinda cool!  `);
					}
					kat(`Thanks.  `);
					if(d.a){
						qcc(`Wait, that's all? That's extremely disapointing.  `);
					}else{
						qaa(`You caon't be serious! Just that?  `);
					}
					
					kat(`I think it looks pretty and sluty enough! `);
				}
			
			
			
			
if(  WARDROBE.equipedGarment(`nipples`,PC) ){
	sas(`${NAME.kat.toUpperCase()}! When did you get your nipples pierced and why you didn't tell!?!?!`);
	txt(`She was mesmerized by my nipple piercings. `);
	kat(`A few weeks ago... Sorry, I completely forget about that!`);
	sas(`You're recently full of surprises!`);
	kat(`You too! The way you burned ${NAME.eva}!`);
}






















			
			
			
			
	case 254:
			mile.piercings_required = -1;
			
			txt(`I had to consider which part of my anatomy I would like to get pierced. `);
			txt(` ${choice(`Eyebrow bridge`,`eyebrow`)} `);
			
			txt(` ${choice(`Nose bridge`,`noseTop`)} `);
			txt(` ${choice(`Nose stud`,`noseSide`)} `);
			txt(` ${choice(`Nose ring`,`noseBot`)} `);
			
			txt(` ${choice(`Medusa piercing above lips`,`lipsTop`)} `);
			txt(` ${choice(`Labret piercing below lips`,`lipsBot`)} `);
			txt(` ${choice(`Monroe piercing close to lips`,`lipsSide`)} `);

			txt(` ${choice(`Tongue piercing`,`tongue`)} `);
			
			if(mile.slut > 1){
				txt(` ${choice(`Nipple bars`,`nipples`)} `);
			}
			
//TODO mile.slut
			if(mile.slut > 5){
				txt(` ${choice(`Labia piercing`,`labia`)} `);
				txt(` ${choice(`Vertical clit hood piercing`,`vch`)} `);
			}
			
			link(`Start over`,254);
			link(`Done`,255);
			break;
		
		case 255:
			{
				if(!mile.pierced) mile.pierced = {};
				const keys = Object.entries(counter).filter( a => a[1] ).map( a => a[0]);

				if( keys.length > 0 && keys.find( a => counter[a] ) ){
					keys.forEach( a => mile.pierced[a] = true );
					txt(`Now I had to buy some nice piercing or piercings. `);
					link(`Selection`,256);
				}else{
					//TODO SPELL
					txt(`Damn! I did not want to get any piercing! But I had to at least pretend at least some token compliance. `);
					link(`Start over`,254);
				};
				break;
			}
		case 256:
			//MERCHANT
			break;
		

		case 257:
			{
				const keys = Object.entries(counter).filter( a => a[1] ).map( a => a[0]);
				
				//?????
				const inve = WARDROBE.unpackArray(inventory);
				
				if(
					keys.find( a => PC[a] ) || keys.find( a => inve.find( b => b.slot === a ) )
				){
//ASAP TODO - SOME READCTION TO THE PIERCINTS 
					link(`Morning`,260);
				}else{
					txt(`Now I had to buy and wear a nice piercing or piercings. `);
					link(`Selection`,256);		
				};
				
			}
			//MERCHANT
			break;

			
		case 260:
			txt(`I was glad it was finally Friday. `);
			morgen.one(`Friday`)
			break;
			
		case 261:
			txt(`I was glad it was finally Friday. `);
			morgen.two(262);
			break;	
			
			
		case 262:
			mile.piercings_required = 1;
		
			txt(`I met with the nerds before classes. `); //
			if(
					PC.eyebrow
				|| PC.noseTop
				|| PC.noseBot
				|| PC.noseSide
				|| PC.lipsTop
				|| PC.lipsBot
				|| PC.lipsSide
			){
				qcc(`Wow. `);
				txt(NAME.qcc+` was very excited when he saw me. `);
				qbb(`Nice. `);
				txt(NAME.qbb+` nodded, only `+NAME.qaa+` was not very excited: `);
				qaa(`A bit trashy. `);
				qbb(`Just like she is. `);
				//qcc(`S `);
				qaa(`Is this all? `);
				link(`Yes.`,268);
				
				if(PC.tongue){
					link(`Stick out tongue. `,263);
				};
				
				if(PC.nipples){
					if(PC.bra){
						link(`Take off shirt and bra. `,264);
					}else{
						link(`Take off shirt. `,264);
					}
				}else if(PC.labia || PC.vch){
					if(PC.panties){
						link(`Take off skirt and panties. `,265);
					}else{
						link(`Take off skirt. `,265);
					};
				};
				link(`Yeah, what else would you want!`,268);
				//ASAP both
				
			}else{
				qaa(`So? How did you cope with your task? `);
				qcc(`I don't see anything. Does it mean... `);
				qbb(`Don't be shy and start stripping. `);
				
				if(PC.tongue){
					link(`Stick out tongue. `,263);
				};
				
				if(PC.nipples){
					if(PC.bra){
						link(`Take off shirt and bra. `,264);
					}else{
						link(`Take off shirt. `,264);
					}
				}else if(PC.labia || PC.vch){
					if(PC.panties){
						link(`Take off skirt and panties. `,265);
					}else{
						link(`Take off skirt. `,265);
					};
				};
				 
				//link(`##Something is broken. `,270);
			
			};
			break;
		
		//tongue
		case 263:	
			qcc(`Nice. `);
			qaa(`I like it. `);
			qbb(`I wonder how that would feel during oral. `);
			if( mile.advice2 == 2 ){
				txt(`I shivered when he mentioned that. I really hoped I will not ever have to suck his dick again. `);
			};
			kat(`Too bad you'll never find out. `);
			qbb(`Or will I?`);
			if(mile.gaa>=2){
				qaa(`Stop being always such a creep. `);
				qbb(`What? Are you jealous? Would you mind if she sucked my dick and not yours? `);
				qaa(`Shut up!`);
			};
			
			if(PC.nipples){
				if(PC.bra){
					link(`Take off shirt and bra. `,264);
				}else{
					link(`Take off shirt. `,264);
				}
			}else if(PC.labia || PC.vch){
				const skirt = WARDROBE.type(`lower`);
				if(PC.panties){
					link(`Take off `+skirt+` and panties. `,265);
				}else{
					link(`Take off `+skirt+`. `,265);
				};
			};
			
			link(`That's all. `,268);
			break;
		
		//nipplses
		case 264:		
			mile.pierced_showed_nipples = true;
			mile.slut++;
			mile.sub++;
			mile.warned = 0;
			WARDROBE.showLess(0,-1);
	//WUT? THIS IS HAPPENING IRL!!!
 	if(mile.slut>4){
				let inter=``;
				if(mile.boobs>1)inter+=`- now significantly expanded -`
				txt(`I kinda enjoyed stripping in front of those nerds, their pathetic stares locked at my `+inter+` chest. I took my time and gave them a good show. `);
				txt(`Nerds were really amazed by my bosom and new nipple piercings. I really should have them pierced a long time ago.  `);
			}else{
				let inter=``;
				if(mile.boobs>2){
					inter+=` - especially now when `+NAME.qcc+` turned my breast into this huge udders`;
				}else if(mile.boobs>1){
					inter+=` - especially now when `+NAME.qcc+` expanded my chest.`;
				}
				txt(`I hated stripping in front of them`+inter+`. I tried to make it as quick and unerotic as possible. `);

	TODO
	mile.slut - be shameles

				
				txt(`Nerds were really amazed by my bosom and my brand new nipple piercings. `);
			};
 		
			if(mile.slut>5){
				txt(`I kinda enjoyed stripping in front of those nerds, their pathetic stares locked at my chest. I took my time and gave them a good show. `);
				txt(`Nerds were really amazed by my bosom and new nipple piercings. I really should have them pierced a long time ago.  `);
			}else{
				txt(`I hated stripping in front of them. I tried to make it as quick and unerotic as possible. `);
				txt(`Nerds were really amazed by my bosom and my brand new nipple piercings. `);
			};
			
			qcc(`Wow. `);
			qaa(`You passed this task with flying colors. `);
			qbb(`Good choice. `);
			qcc(`I hoped but I assume she will go for something lamer. `);
			qbb(`Nah, I told you she is a slut who will definitely get her nipples pierced. `);
			if(mile.burned){
				qaa(`Yeah, sluts like her love drawing attention to their tits. `);
				if(mile.slut<4 && mile.sub<3)kat(`Hey! I'm not a slut! `)
			}else if(mile.gaa > 2){
				qaa(`Having her nipples pierced doesn't mean she is a slut. `);
				qbb(`I really think it does. `);
				qcc(`There is nothing wrong with being a slut. `);
				qaa(`Don't listen to them, you are great `+NAME.kat+`! `);
			}else{
				qaa(`You are great `+NAME.kat+`! `);
			};
			
			if(PC.vch || PC.labia){
				link(`And that isn't everything!`,265);
				link(`That's everything!`,268);
			}else{
				link(`New task.`,270);
			};
			break;
		
		//pubic
		case 265:
		//if(!mile.pierced.hood) pierce(`hood`,`HoodPiercing`);
	//if(!mile.pierced.labia)
		
			mile.pierced_showed_pubic = true;
			mile.slut++;
			mile.sub++;
			mile.warned = 0;
			WARDROBE.showLess(-1,0);
			if(mile.slut > 5){
				txt(`With a smile, I unzipped my skirt. `);
				kat(`You really want to see my brand new piercing, don't you? `);
				txt(`I teased them. It made them pretty hyped with high expectations.  `);
			}else{
				txt(`With sight, I rolled up my skirt. Needless to say, this move made them pretty hyped with high expectations. `);
			};
			
			if(!PC.panties){
				qbb(`You don't even bother with panties anymore, do you? `);
//TODO MILE.SLUT
				if(mile.slut < 8){
					txt(`I blushed. `);
					qcc(`Nice! `);
				}else{
					txt(`I shrugged. `);
					kat(`I guess I don't! `);
				}
			}
			txt(`They moved closer to admire my new piercing. `);
			qaa(`Wow. `);
			qbb(`Nice! `);
			//qbb(`You should get a clit piercing. `);
			//kat(`Are you crazy?! I'd never allowed anybody to pierce me... there.  `);
			qcc(`I like it. It looks really great. `);
			qaa(`You definitely passed this task. `);
			//if( WARDROBE.equipedGarment(`nipples`,PC) )link(`And that isn't everything! `,264);
			
			link(`New task.`,270);
			break;	
		
		//not enough
		case 268: 
			if(PC.nipples || PC.pubic || PC.vch || PC.labia){
				let plural = PC.nipples ? true : false;
				mile.sub--;
				mile.slut++;
//SPELL ASAP ODHODLAT dared
				txt(`I decided to do not tell them about my <i>other</i> `+(plural ? `piercings` : `piercing`)+`. I was thinking about `+(plural ? `them` : `it`)+` for some time but only now I dared to finally get `+(plural ? `them` : `it`)+`. However, I did it for myself, not for those three creeps! `);
			
			}else{
				mile.sub--;
				//mile.slut--;
				qbb(`That wasn't much. `);
				kat(`What else would you want?`);
				if(mile.burned){
					qaa(`Something more than the minimal effort. `);
				}else{
					qcc(`Something more than the minimal effort. `);
				}
				kat(`I finished your task exactly according to your directions!`);
				if(mile.burned){
					qcc(`Yeah, you did. `);
				}else{
					qaa(`Yeah, you did. `);
				}
			}
			
			link(`What is the next task?`,270);
			break;
			
			
			



*/
			
			
			
			
			
			