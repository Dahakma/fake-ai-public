import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, tom, zan, ven, mik} from "Loop/text";
import {mol, maj, ped, bio, dur} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise, niceness, last, chat} from "Loop/text";
import {placetime, task_intro, task_reaction} from "Loop/text_extra";

import {ra} from "Libraries/random";
import {stringifyHSLA} from "Libraries/color/index";
import {emo, draw, rollUpSkirt, wdesc, removeActual, removeEverything, update, updateDraw, effigy, allClothes, crew, wearRandomClothes, remove, wear, create, wears, showLess, showAll, violates, sluttiness, createWear, quickSaveOutfit, quickLoadOutfit  } from "Avatar/index";
import {active_timeslot, next_event, add_rule} from "Taskmaster/scheduler";
import {DISPLAY} from "Gui/gui";
import {cloneFigurine} from "Avatar/canvas/body";
import {rules_violation} from "Taskmaster/new_rules_check";
import {masochist} from "Virtual/text";

import {payment} from "System/bank_account.js";
import {money} from "Data/items/prices.js";


export const inks = { //reused by hubs/shops.js
	//first time x sas small
	small: [ 
		"ankle", 
		"cleavage",
		"shoulder", 
		"hipSmall",
	],
	//sas big one, second time 
	big:  [
		"ankle", 
		"cleavage", 
		"shoulder", 
		"hipSmall",
		
		"forearm",
		"chest",
		"underBreasts", 
		"belly",
		"side", 
		"thigh",
	],
	only_big: [
		"forearm",
		"chest",
		"underBreasts", 
		"belly",
		"side", 
		"thigh",
	],
	
	only_sexy: [
			//"ankle", 
			"cleavage", 
			//"shoulder", 
			//"hipSmall",
			//"forearm",
			"chest",
			"underBreasts", 
			//"belly",
			"side", 
			"thigh",
			"lowerBelly",
			"breasts",
	],
	
	get all(){ 
		const slots =  [
			"ankle", 
			"cleavage", 
			"shoulder", 
			"hipSmall",
			
			"forearm",
			"chest",
			"underBreasts", 
			"belly",
			"side", 
			"thigh",
		];
		
		if(mile.slut > 3 || hardbug) slots.push("lowerBelly");
		if(mile.slut > 6 || hardbug) slots.push("breasts");
		
		return slots;
	},
	
	all_forced: [
			"ankle", 
			"cleavage", 
			"shoulder", 
			"hipSmall",
			
			"forearm",
			"chest",
			"underBreasts", 
			"belly",
			"side", 
			"thigh",
			
			"lowerBelly",
			"breasts",
	],
	
}


//TATTOO 1 
export const tattoo_initiation = (index)=> {
	task_intro();
		
	if(mile.a_burned){
		qaa(`The next task is absolutely perfect for a slut like you!`);
	}else{
		qaa(`I'm not sure if you will like our next task. But I think it's a good one.`);
	}
	kat(`Okay?`);
	qaa(`We want you to get a tattoo.`);
	kat(`A tattoo!?! I won't do anything permanent!`);
	

	
	if( emy.s ){
		emy.bac(`After you finally finish the game, we want you to have a nice reminded of our time together! `);
		kat(`Fuck no, I don't want to have any reminders! Actually, I hope they'll invent a device that will allow me to erase the last weeks from my memory!`);
		reac.abc(`He's just joking! You don't have to keep the tattoo after finishing the game. And since tattoos are easily removable, they can't be really counted as a permanent change. `);
	}else{
		qcc(`Well, tattoos are easily removable and they therefore can't be considered to be a permanent change!`);
	}
	
	kat(`Still, this task is going too far!`);
	qcc(`You would look so sexy with a cute tattoo!`);
	qbb(`And we'll be so nice we'll even allow you to choose any place and any design you want!`);
	qaa(`Surprise us!`);
	kat(`You're insane!`);
	txt(`Truth be told, I always wanted to get a tattoo. However, I just could not give up without a fight, that would only encourage them in the future.`);
	qcc(`Come on, @kat! One tattoo, could be even small and wherever you want.`);
	if( emy.is.a || emy.is.b ){
		emy.ba(`I would suggest a trashy tramp stamp across your whole lower back. To notify everybody even from a distance how huge tramp you are.`);
		kat(`Thanks but no. I'm sure I'll be able to come up with something better.`);
	}else{
		kat(`...fine. `);
	}
	next();
	
}

 

		


export const tattoo_action = (index)=> {
	switch(index){
		default:
		case 101:
			placetime("tattoo parlor");

			{
				let text = `To fulfill my next task, I headed to a tattoo parlor. `;
				if(mile.lesbian <= 1){
					text += `The woman running the shop, with pierced nose, heavily pierced ears and tattooed from neck to toes (presumably, based on her arms and cleavage, she was not nude) seemed intimidating but she was actually very nice. ${wears.heavilyPiercedFace ? "She even told me she loves my piercings!" : ""} `;
				}else{
					text += `The woman running the shop, with pierced nose, heavily pierced ears and arms, neck and cleavage completely covered with tattoos seemed intimidating but she was actually very nice. ${wears.heavilyPiercedFace ? "She even told me she loves my piercings!" : ""} `;
					if(mile.slut > 6){
						text += `Her tattoos were teasing my imagination. I tried to flirt with her to find out what was hidden under her tank top and jeans, if her whole body was really covered from the neck to her toes. She laughed and admitted her breasts and chest were tattooed too. Then she waved in the direction of her lower belly, smirked and told me she had a very sexy and slutty tattoo there but refused to provide details. `;
					}else{
						text += `Her tattoos were teasing my imagination, she was wearing a tank top and jeans and I could only wonder whether was her whole body really covered from neck to toes.`;
					}
				}
				txt(text);
			}
			
			txt(`She explained the whole procedure. It was supposed to be quick and completely painless and the laser was able to create any tattoo I imagined. Well, any normal tattoo, not luminescent or animated ones - they required specialized equipment - but really I was not into that tacky transhumanist crap. I was a bit overwhelmed by the choices and asked her for suggestions. (I was only sure I definitely do not want a tramp stamp.) She advised that for my first tattoo I should get something small and simple, that I will have plenty of opportunities to get a bigger one in the future.`);
			
			txt(`I could get a cute and subtle tattoo above my ankle. It would be easy to hide and people might not even notice it. Although I doubted the nerds will be happy with that.`);
			txt(`Or I could get a tattoo on my upper arm. That would be pretty badass and certainly boost my confidence.`);
			txt(`A little tattoo above my breasts would be very sexy and draw more attention to my cleavage.`);
			txt(`On the other hand, a tattoo on my side would look awesome and hot with a bare midriff.`);
			
			link(`Get a tattoo`, null, ()=> DISPLAY.tattoo({slots: inks.small}, PC, undefined, (report)=>{
					mile.tattoo = report?.id;
					main(110);
			}));

//TODO - CHANGE HIP SMALL

	/*
	
	
					
			{
				const list = [
					
					"underBreasts", 
					"breasts",
					"thigh",
					"belly",
					"lowerBelly",
					"chest",
					"forearm",
				];
				link(`Get a tattoo`, null, ()=> DISPLAY.tattoo({slots: list}, PC, undefined, (report)=>{
					mile.tattoo = report?.id;
					main(110);
				}));
			}
			
		
			txt(`Finally, the school was over. To fulfill my task, I headed to a tattoo parlor. They ensured me the whole procedure is instant and painless. The only remaining question was what tattoo did I want and where? `);
			txt(`I could get one above an ankle. That would be quite tame and easy to hide. ${mile.sub < 0 ? "The nerds probably will not be very satisfied but I really did not care. " : "I was only afraid the nerds might not be satisfied just with that. "}`);
			txt(`Or I could get some badass tattoo on my arm. Such tattoo would certainly greatly boost my confidence. `);
			txt(`Full sleeve might have a similar effect but I was afraid it might look too trashy and excessive.  `);
			txt(`Maybe some bigger tattoo on my side? That would be pretty classy and sexy. `);
			if(mile.slut > 3) txt(`A lower belly tattoo would be even sexier. Maybe a bit too much sexy - unless I would be naked, a large part of the tattoo would stay hidden under my clothes, teasing everybody's imagination. I bet those nerds would like that. `);
			if(mile.slut > 10 && mile.sub > 10) txt(`Or maybe... I could get a tattoo which would instantly manifest everybody how huge pathetic slut I am. It was fun to play with that idea but I did not think it would be a very bright decision. `);
	*/
			break;
			
		case 110:
			if(mile.tattoo === "ankle"){
				txt(`I got ankle tattoo. `);
				mile.slut--;
			}else if(mile.tattoo === "shoulder"){	
				txt(`I got shoulder tattoo. `);
				mile.sub--;
			}else if(mile.tattoo === "cleavage"){	
				txt(`I got cleavage tattoo. `);				
			}else if(mile.tattoo === "hipSmall"){	
				txt(`I got a tattoo on my side. `);
			}else{
				link(`Get a tattoo`, null, ()=> DISPLAY.tattoo({slots: ["ankle", "cleavage", "shoulder", "hipSmall"]}, PC, undefined, (report)=>{
					mile.tattoo = report?.id;
					main(110);
				}));
				return;
			}
			txt(`The woman smiled at me, she told me that the tattoo really suits me and since it was my first one, she charged me only ${money.tattoo_first} Euro. `);
			payment("-tattoo_first","tattoo");
			next();
			break;
	}
}	


export const tattoo_reaction = (index)=> {
	
	qaa(`So? Did you get a tattoo?`);
	kat(`Of course, I did. ${mile.sub > 15 ? "I'm your obedient slave, am I not? " : ""}`);
	qbb(`Show us.`);
	
	switch(mile.tattoo){
		//could be same as before
		case "ankle": 
				txt(`I lifted up my leg and showed them the tattoo above my ankle.`);
				break;
		case "shoulder":
				if(PC.upper?.armCoverage > 0.05){
					txt(`I rolled up the sleeve of my @upper to show them my new tattoo.`);
				}else{
					txt(`I pointed on my shoulder at my new tattoo.`);
				}
				break;
		case "cleavage":
				if(PC.upper?.cleavageCoverage){
					txt(`I grabbed the cleavage of my @upper and stretched it so they could see the whole tattoo.`);
				}else{
					txt(`I showed them the tattoo above my breasts.`);
				}
				break;
		case "hipSmall":
				txt(`I readjusted my @upper to show them my new tattoo.`);
				break;
		default:
			break;
	}	
	
	qcc(`Cute! I like it!`);
	if(mile.a_burned){
		qaa(`Nah, mediocre!`);
	}else{
		qaa(`Yeah, very nice!`);
	}
	qbb(`That isn't the tramp stamp I imagined!`);
	kat(`Are you disappointed?`);
	qbb(`No, not really. Honestly, my expectation wasn't very high. Maybe the next time.`);
	kat(`Next time!?!?!`);
	
	next();
	
}





//TATTOO SAS 
export const sas_tattoo = (index)=> {
	switch(index){
		default:
		case 101:
			placetime("tattoo parlor");
		
			txt(`When we were stripping for our gym classes, very observant @sas immediately noticed my new tattoo. She was totally amazed and thought it was the coolest thing ever.`);
			txt(`She shared that she always wanted a tattoo too and I suggested we should get her one. So in the afternoon we met and headed to the tattoo parlor. Halfway there @sas began to have second thoughts and have dumb questions like whether the laser hurts or how complicated is to remove the tattoo if she changed her mind.`);
			txt(`However, the friendly tattooed woman who ran the tattoo parlor eagerly explained everything and dispelled all her doubts. Everything went well until the tattooed woman told @sas she can choose any tattoo she wants. I knew that was bad. @sasalt was infamously indecisive and I really did not want to spend several hours there going through pictures of countless tattoos and eventually still return without one because @sas was not able to decide. I had to help her.`);
			kat(`So what would you want?`);
			sas(`I have no idea!`);
			kat(`At least in general?`);
			sas(`I think something small and not too flashy.`);
	
			
			link(`Let's see...`, null, ()=> DISPLAY.tattoo({slots: inks.small}, effigy.sas, undefined, (report)=>{
					mile.sas_tattoo = report?.id;
					main(110);
			}));
			
			link(`Don't be stupid, you deserve an awesome, huge tattoo!`, null, ()=> DISPLAY.tattoo({slots: inks.big}, effigy.sas, undefined, (report)=>{
					mile.sas_exploit++;
					mile.sas_sassy++;
					mile.sas_tattoo = report?.id;
					main(120);
			}));
			break;
			
		case 110:
			if(mile.sas_tattoo === undefined){
				link(`Let's see...`, null, ()=> DISPLAY.tattoo({slots: inks.small}, effigy.sas, undefined, (report)=>{
						mile.sas_tattoo = report?.id;
						main(110);
				}));
				return;
			}
			
			effigy.save("sas");
			txt(`It was a struggle but eventually, we managed to choose a nice design and @sas was pretty happy with the result.`);
			txt(`Then the tattoo artist suggested that when I was already there, maybe I should get another tattoo. It would cost me around ${money.tattoo} Euro. `); //TODO - BASE ON SIZE
			
			next(`No, thanks. `);
			con_link(ext.irl_money >= money.tattoo, `not enough money `, `Sounds like great idea!`, null, ()=> DISPLAY.tattoo({slots: inks.big}, PC, undefined, (report)=>{
					mile.sas_tattoo_kat = report?.id;
					main(130);
			}));
			break;
			
			
		case 120:
			if(mile.sas_tattoo === undefined){
				link(`Don't be stupid, you deserve an awesome, huge tattoo!`, null, ()=> DISPLAY.tattoo({slots: inks.big}, effigy.sas, undefined, (report)=>{
						mile.sas_tattoo = report?.id;
						main(120);
				}));
				return;
			}
			
			effigy.save("sas");
			txt(`@sas was initially reluctant but I eventually managed to convince her an awesome big tattoo was exactly what she wanted.`);
			kat(`It does look good, doesn't it?`);
			sas(`Yeah, I guess it does. But it isn't fair.`);
			kat(`What isn't fair?`);
			sas(`You made me pick a tattoo you wouldn't pick yourself!`);
			kat(`I would, I like it.`);
			sas(`Really? Then get it! (Or something similar, obviously not exactly the same design as me. )`);
			
			next(`Ehh... Maybe another time. `);
			con_link(ext.irl_money >= money.tattoo, `not enough money `, `Fine!`, null, ()=> DISPLAY.tattoo({slots: inks.only_big}, PC, undefined, (report)=>{
					mile.sas_exploit--; //TODO - ??????
					mile.sas_tattoo_kat = report?.id;
					main(130);
			}));
			break;
			
		case 130:
			if(mile.sas_tattoo_kat === undefined){
				link(`Get a tattoo.`, null, ()=> DISPLAY.tattoo({
					slutty: false,
					slots: inks.only_big,
				}, PC, undefined, (report)=>{
						mile.sas_tattoo_kat = report?.id;
						main(130);
				}));
				
				next("<small>#Something is broken, pls let me continue. </small>");	
			}else{
				txt(`So even I was leaving the tattoo parlor with a brand new tattoo.`);
				payment("-tattoo","tattoo");
				next();	
			}
	}
}









//TATTOO 2
export const tattoo_2_initiation = (index)=> {
	switch(index){
		default:
		case 101:
				task_intro();
			
			if( niceness() > 0){
				emo(`anoy`);
				mile.tattoo_2_task = 1;
				
				emy.abf(`Our new task gives you an opportunity to redo one task you failed in the past. We want you to get another tattoo.`);
				kat(`I didn't fail that task, I got a tattoo exactly as you wanted!`);
				qbb(`Nah, you got a pretty lame one. You could do better!`);
				kat(`I'm not getting tattooed only to satisfy your perverted wishes!`);
				qaa(`Come, @kat! Stop playing! If you really despised tattoos as much as you pretend, you wouldn't get another on your own!`);
				kat(`Well, that was different...`);
				txt(`They were kinda right, I got tattooed with @sas.`);
				aly.cba(`Come on, @kat! It will look cute! `);
				emy.abc(`An insecure bimbo slut has to have several trashy tattoos, that's just the way it is. Don't fight it. `);
				aly.abc(`And we will be so nice we even let you choose any place and any motive you want! `);
				txt(`They were not nice, ${aly.list} did not want to push too far and risk making me angry.`);
				qbb(`But it better should be something good! ${emy.is.b ? "I'm still waiting for that tramp stamp! " : "Maybe you reconsidered the tramp stamp?"}`);

				next(`Fine! `);
				con_link(mile.sub < 10, `too submisive`, `Okay, but you will pay for it. `, 102);
				
			}else{
				emo(`shock`);
				mile.tattoo_2_task = 2;
				
				qaa(`Our new task gives you an opportunity to redo one task you failed in the past. We want you to get another tattoo.`);
				emy.bcf(`But a proper one this time!  `);
				kat(`I didn't fail that task, I got a tattoo exactly as you wanted!`);
				aly.cbaf(`Nah, you got a pretty lame one. You could do better! `);
				kat(`No! I don't want to get my skin forever marked with something ridiculous just because of your perverted fetishes!`);
				emy.abcf(`Stop being such a drama queen. You can get easily remove it later, we already went through this! `);
				reac.abc(`Yeah! And when you already got one tattoo done, why is it such a big deal to get another one?  `);
				emy.bcf(`An insecure bimbo slut has to have several trashy tattoos, that's just the way it is. Don't fight it. `);
				txt(`The nerds did not allow me to argue with them.`);
				aly.acbf(`You can still get any tattoo you want, anywhere you want! `);
				reac.cab(`Well, with a few limits. We can't give you too much freedom.  `);
				//TODO - obliging x not obliging
				kat(`What limits?`);
				emy.acf(`The tattoo has to be in a sexy place and it has to be very distinctive. And the sluttier, the better!`);
				kat(`How much slutty?`);
				emy.baf(`What about a tramp stamp with *I love anal* written in nice gothic letters? That would be cute, wouldn't it?  `);
				if(mile.slut > 13 && mile.sub > 12){
						txt(`I helplessly shrugged.`);
						kat(`Maybe? `);
				}else{
					kat(`Fuck you!`);
				}
				txt(`The trio only laughed. Those bastards. I probably made a mistake when I agreed with the first one. I did not mind getting a small tattoo but the nerds, as always, pushed things too far.`);
				next(`Fuck! `);
			
			}	
				break;
				
		case 102:
				emo(`imp`);
				mile.sub--;
				payment(`tattoo_claim`, `qcc`);
				
				kat(`Fine, but if you want me to get a tattoo, give me cash. I'm not going to waste my own money because of your stupid tasks.`);
				qaa(`Well, maybe...`);
				kat(`This is non-negotiable.`);
				qcc(`How much a tattoo cost?`);
				kat(`Like  ${money.tattoo_claim}. At least.`);
				txt(`They reluctantly agreed and sent me the money.`);

				next();
				break;
	}
}



export const tattoo_2_action = (index)=> {
	switch(index){
		default:
		case 101:
			emo(`focus`);
			placetime(`tattoo parlor`);
	present([5, `Tattoo Artist`]); //TODO
			txt(`So after school, I went to the tattoo parlor again. The heavily-tattooed woman smiled at me.`);
			npc(5, `I knew I will see you again!`);
			kat(`Yeah. I'm back for another tattoo.`);
			
			if(mile.tattoo_2_task === 2){
				txt(`I tried to figure out how to satisfy the outrageous demand of the nerds. They did not expect me to get an actually slutty and humiliating tattoo, did they? In the catalog were several designs so ridiculous I was not even sure why they were included. No sane, self-respectful woman would ever agree to carry something so degrading on her body! But maybe I could hide it under my clothes... and nerds would be so shocked I really did it...`);
										
				link(`Get a tattoo. `, null, ()=> DISPLAY.tattoo({
					slutty: true,
					slots: inks.only_sexy,
				}, PC, undefined, (report)=>{
						counter.tattoo = report;
						if(report === undefined){
							main(101);
						}else if(report.slutty){
							main(102);
						}else{
							main(110)
						}
				}));
				
				
		}else{
				txt(`I tried to figure out what tattoo I wanted to get. I would prefer to have more time to think about it, not being rushed by those supid nerds. `);
				
				
				const slutty = mile.slut >= 10 || mile.sub >= 10;
					
				if(slutty){
					txt(`When I was flipping through the catalog, I noticed several designs so ridiculous I was not even sure why they were included. No sane, self-respectful woman would ever agree to carry something so humiliating and degrading on her body! However, the more I was thinking about it, the more I was enjoying toying with the idea. I would love to see the nerds' faces if I arrived with something so outrageous on my skin. Or see how baffled would @eva and @sas be. `);
					
				}
					
				link(`Get a tattoo. `, null, ()=> DISPLAY.tattoo({
					slutty,
					slots: inks.all,
					}, PC, undefined, (report)=>{
						counter.tattoo = report;
						if(report.slutty){
							main(102);
						}else{
							main(110)
						}
				}));
				
				if(!slutty && hardbug){
					link(`##Get a tattoo (slutty tattoos unlocked) `, null, ()=> DISPLAY.tattoo({
						slutty: true,
						slots: inks.all_forced,
					}, PC, undefined, (report)=>{
							counter.tattoo = report;
							if(report === undefined){
								main(101);
							}else if(report.slutty){
								main(102);
							}else{
								main(110)
							}
					}));
			}
		}
		break;
		
		case 102:
				emo(`angry`);
				mile.tattoo_2_slutty = true;
				mile.slut++;
				mile.sub++;
				
				txt(`@npc_5 looked surprised: `);
				npc(5, `Are you sure? Do you really want this? `);
				txt(`Before she asked me, I had my doubts. But I did not want her to see me wavering. I frowned at her: `);
				kat(`Of course I do! It shows that I... `);
				npc(5,`You don't have to justify yourself! I met worse weirdos than you`);
				link(`Continue.`, 110);
				break;
				
				
		case 110:
				mile.tattoo_2 = counter.tattoo.id;
				if( ["cleavage",  "shoulder",  "hipSmall",  "side"].includes(counter.tattoo.slot)  ) showLess(0, -1); //bra
				if( ["chest", "underBreasts", "belly",  "breasts"].includes(counter.tattoo.slot)  ) showLess(0, -1); //topless
				if( ["thigh", "lowerBelly"].includes(counter.tattoo.slot)  ) showLess(-1, 0); //bottomless
				if( ["thigh"].includes(counter.tattoo.slot)  ) showLess(-1, 1); //panties
				
				payment("-tattoo","tattoo");
								
				txt(`Once I decided what tattoo I wanted, everything went quickly. The laser promptly burned the pigment into my skin. It was not a pleasant feeling but it was not unbearably painful. `);
				
				if( ["lowerBelly", "breasts", "thigh", "underBreasts"].includes(counter.tattoo.slot) ){
					npc(5, `If you won't mind... `);
					switch(counter.tattoo.slot){
						case "underBreasts":
							txt(`She began rubbing a calming ointment over my skin. I was topless and a few times she lightly brushed against my breasts with the back of her hand. `);
							break;
							
						case "breasts":
							txt(`She poured a calming ointment over my breasts and began rubbing it into my skin. She was not trying to stimulate me but her fingers several times nicely brushed over my nipples. `);
							break;
							
						default: 
							txt(`She brought a cold, calming ointment and began rubbing it into my skin. Her hands gently moved in the immediate proximity to my private region, several times she almost brushed with her fingers against my pussy. `);
					}
					npc(5, `Sorry, this is a normal procedure. It will help your skin to heal faster, I'm not trying to make it awkward. `);
					
					if( mile.lesbian >  1 && mile.slut > 6){
						emo(`imp`);
						kat(`Are you saying you're not trying to subtly seduce me into doing twisted lesbian stuff with you? `);
						npc(5, `No, of course not! `);
						kat(`What a shame! `);
						txt(`She giggled when I was flirting with her. `);
						if(counter.tattoo.slutty){
							npc(5, `With a tattoo like that, I really shouldn't be surprised you're such a shameless tease!`);
							kat(`What? ....oh! Right! `);
							txt(`I remembered what I chose as my new permanent adornment. `);
							kat(`Yeah, that's why I got it. To let everybody see how crazy bitch I am! `);
						}
					}else{
						if(mile.sub < 6){
							emo(`suspect`);
							kat(`You know, you could let me use the ointment on myself without your help. `);
							txt(`I pointed out and smirked at her. `);
							npc(5, `Oh... I guess I could.`);
							if(counter.tattoo.slutty){
								txt(`She looked a bit embarrassed. Was she just innocently trying to assist me? Or was she encouraged by my new tattoo, believing I was a crazy, horny slut who did not mind being groped? `);
							}else{
								txt(`She looked embarrassed, it was hard to decide whether she innocently wanted to assist me or whether she exploited the opportunity to grope my body. `);
							}
						}else{
							emo(`unco`);
							kat(`Oh... sure, I understand. Do what you need to do. `);
							txt(`Without thinking I sheepishly agreed.`);
							npc(5, `Good girl! `);
							if(counter.tattoo.slutty){
								txt(`She smirked. She seemed to be really encouraged by the new tattoo, probably believing I was a crazy, submissive slut who did not mind being groped. `);
							}else{
								txt(`Was she encouraged by my submissive attitude to rub me more eagerly? Or was I just imagining that? `);
							}
						}
					}	
					

				}else{
					emo(`relax`);
					txt(`The tattoo artist gently rubbed a calming ointment over my skin and then asked for ${money.tattoo} Euro.  `);
				}
				
				next(`Done. `, showAll);
	}
}	
	


export const tattoo_2_reaction = (index)=> {
	/*
	switch(index){
		default:
		case 101:
	*/
				const details = sluttiness(PC).details;
				//TODO EMO
	
	console.warn(details);
	
				if( details.sluttyTattoo ){
					if(mile.slut < 12) mile.slut++;
					qcc(`Whoa!`);
					qbb(`Nice!`);
					txt(`The nerds were speechless when they noticed the tattoo I got. And even more that I was not even bothering with hiding it.`);	
				}else if( details.hintedSluttyTattoo ){
					qcc(`What is it?`);
					qbb(`Show us!`);
					txt(`The nerds noticed my new tattoo. It was not clearly visible but their imagination was immediately captivated and they wanted to see more.`);
				}
					
				switch(mile.tattoo_2){
					//could be same as before
					case "ankle": 
							txt(`I lifted up my leg and showed them the tattoo above my ankle.`);
							break;
					case "shoulder":
							if(PC.upper?.armCoverage > 0.05){
								txt(`I rolled up the sleeve of my @upper to fully show them my new tattoo.`);
							}else{
								txt(`I pointed on my shoulder at my new tattoo.`);
							}
							break;
					case "cleavage":
							if(PC.upper?.cleavageCoverage){
								showLess(1,-1);
								txt(`We went to bathrooms where I could strip my @upper and show them my new tattoo in its full glory. `);
							}else{
								txt(`I showed them the tattoo above my breasts.`);
							}
							break;
					case "hipSmall":
							txt(`I readjusted my @upper to show them my new tattoo.`);
							break;
					//new ones
					case "forearm":
							txt(`I put in front of them my forearm so they could fully see my new tattoo.`);
							break;		
					case "chest":
							showLess(1,-1);
							txt(`We went to bathrooms where I could strip my @upper and show them my new tattoo in its full glory. `);
							break;				
					case "underBreasts":
							if(PC?.bra?.botStrapCoverage > 1.5){
								showLess(0,-1);
								txt(`We went to bathrooms where I could strip my @upperBra and show them my new tattoo in its full glory. `);
							}else{
								showLess(1,-1);
								txt(`We went to bathrooms where I could strip my @upper and show them my new tattoo in its full glory. `);
							}
							break;	
								
					case "side":
					case "belly":
							if(details.bareMidriff){
								txt(`I arched a bit and fully showed them my brand new tattoo. `);
							}else{
								showLess(1,-1);
								txt(`We went to bathrooms where I could strip my @upper and show them my new tattoo in its full glory. `);
							}
							break;		
							
					case "breasts":
							showLess(0,-1);
							txt(`We went to bathrooms where I could strip my @upperBra and show them my tattooed tits. `);
							break;	
				
						case "thigh":
						case "lowerBelly":
							if(mile.slut > 13){
								showLess(-1, 0);
								txt(`We went to bathrooms where I could pull down my @lowerPanties and show them my new tattoo in its full glory. `);
							}else{
								showLess(-1, 1);
								txt(`We went to bathrooms where I could pull down my @lowerPantyhose and show them my new tattoo in its full glory. `);
							}
							break;	
					
					default: 
						break;
				}	
	
	
			if(mile.tattoo_2_task === 2){
								
				if(mile.tattoo_2_slutty){
	//TODO obey obedience
					txt(`They were stunned by how obscene the design was:`);
					qbb(`Lovely tattoo! Far better than the first lame one.`);
					txt(`@qbb was clearly satisfied.`);	
					qcc(`You're such a madwoman! I can't even believe it's real!`);
					txt(`@qcc shook his head.`);
					kat(`Do you want to make sure it's real?`);
					txt(`I teased him and allowed him to touch my body and make sure it was not just body paint.`);
					emy.abc(`I think it's very good that @kat finally accepted who she really is and isn't afraid to show it. You wouldn't want people to mistake you for a modest, self-respectful girl, would you? `);
					
				 }else{
					qbb(`Lovely tattoo! Far better than the first lame one.`);
					txt(`@qbb was clearly satisfied.`);	
					emy.abc(`I told you, all it took was to give her clear instructions! `);
					txt(`@${last} condescendingly smirked.`);
					qcc(`It looks amazing!`);
					if(aly.is.a){
						qaa(`Yeah, great choice, @kat! Good job!`);
						txt(`@qaa lovingly smiled at me.`);
					}else if(emy.is.a){
						qaa(`I like how trashy it is. You wouldn't want people to mistake you for a modest, self-respectful girl, would you?`);
					}
					
				 }
				
			
			}else{
				if(mile.tattoo_2_slutty){
					qcc(`Is... is it real?`);
					qbb(`This is awesome!`);
					txt(`@qcc was shaking his head, he could not believe it. @qbb was thrilled and excited.`);
					emy.ab(`I think it's very good that @kat finally accepted who she really is and isn't afraid to show it. You wouldn't want people to mistake you for a modest, self-respectful girl, would you? `);
					kat(`Do you want to make sure it's real?`);
					txt(`I allowed @qcc to touch my body and make sure it was not just body paint.`);
					qcc(`You're such a madwoman!`);
					
				}else if( inks.only_sexy.includes(mile.tattoo_2) ){
					qcc(`Oh! Cool! Very sexy!`);
					qbb(`Honestly, far better than I expected!`);
					if(aly.is.a){
						qaa(`Yeah, great choice, @kat! Good job!`);
						txt(`@qaa lovingly smiled at me.`);
					}else if(emy.is.a){
						qaa(`I like how trashy it is. You wouldn't want people to mistake you for a modest, self-respectful girl, would you?`);
					}
					txt(`The nerds were very pleased when they examined my new tattoo. I almost got used to being half-dressed and demeaningly posing for them.`);
					if(mile.sub < 9){
						kat(`It's such a joy to see you're satisfied!`);
					}else{
						kat(`Thanks, I guess.`);
					}
				
				
				}else{
					txt(`The reaction of the nerds was mixed.`);
					if(aly.is.b){
						emy.caf(`She could do better, right @qbb? `);
						qbb(`Well, I think she made the right choice.`);
						txt(`@qbb uncharacteristically agreed with my decision. The other nerds were surprised but I was not. I knew why he did not want to annoy me.`);
						
					}else{
						qbb(`Seriously @kat, I imagined something far better!`);
						txt(`Immediately scolded me @qbb.`);
						qcc(`I disagree, I think it looks very cute and very sexy!`);
					}
					
					if(emy.is.a){
						qaa(`This tattoo doesn't suit you at all! It's too subtle. You're a trashy slut with no self-respect and your tattoos should be reflecting that! You wouldn't want people to mistake you for a modest, self-respectful girl, would you?`);
					
					}else if(aly.is.a){
						qaa(`Yeah, great choice, @kat!`);
						txt(`@qaa lovingly smiled at me.`);
					}
					
				}
				
			}
			

			if(aly.is.a && mile.tattoo_2_slutty){
					qaa(`It's horrible and too vulgar! I can't understand why would you want such a demeaning tattoo! Are you out of your mind, @kat?`);
					txt(`The only one who was not happy was @qaa.`);
					kat(`You wanted me to get a sexy tattoo!`);
					qaa(`We wanted you to get something subtle and teasing, not something so stupidly obvious.`);
					qbb(`Maybe you did. This is exactly what *I* wanted.`);
					txt(`@qaa was really annoyed that I ruined my perfect body:`);
					qaa(`Now you look like a whore, @kat!`);
					kat(`Whoa!`);
					qcc(`Don't be mean! If she wants to look like a whore, she can! It's her right!`);
					qbb(`Yeah, exactly!`);	
			}	
				
				
			next(`School. `, showAll);
//	}
}
				



export const girls_slutty_tattoo = (index)=> {
	switch(index){
		default:
		case 101:
			{
				placetime("schoolyard");
				const details = sluttiness(PC).details;
				

				if( details.sluttyTattoo ){
					txt(`During the break, I went to go to chat with the girls.  @sas for the first time noticed my new tattoo and gasped:`);
					sas(`Oh, @kat!`);
					txt(`@eva looked at puzzled @sas, then at me. And then her eyes widened too.`);
					eva(`What the fuck, @katalt?!`);
				
					
				}else if( details.hintedSluttyTattoo ){
					txt(`During the long break, I was chatting with the girls. We were talking about normal stuff but then @sas suddenly asked:`);
					sas(`Come on, @kat! Stop teasing us!`);
					kat(`What do you mean?`);
					sas(`Your new tattoo! We want to see it!`);
					txt(`She noticed it poking from under my clothes.`);
					eva(`Yeah! Show us!`);
					txt(`I quickly readjusted my clothes to flash the tattoo on my ${details.hintedSluttyTattooList[0]}.`);
					
				}
					
				sas(`Is it real?`);
				if(mile.sub < 1){
					kat(`Yeah.`);
					txt(`I proudly nodded. They reacted exactly the way I imagined!`);
				}else if(mile.sub > 11){
					kat(`Yeah...`);
					txt(`I shrugged. It was fun imagining *what would girls say* but now when I was actually facing them, I felt a bit embarrassed for my especially suggestive tattoo.`);
				}else{
					kat(`Yeah.`);
					txt(`I shrugged. I was a bit anxious, their reaction was even stronger than I anticipated.`);
				}
				
				eva(`Are you out of your mind, @katalt?!`);
				txt(`@eva immediately berated me.`);
				eva(`What do you think people will think about you when they'll see it?`);
				
				if(mile.sas_date > 0){ //TODO or love
					if(mile.sas_dom){
						sas(`That she's a submissive sexual freak.`);
					}else{
						sas(`That she's a horny sexual freak.`);
					}
					txt(`Smirked daydreaming @sas.`);
				}else{
					sas(`Yeah. Having such a tattoo is pretty weird!`);
					txt(`@sas supported her.`);
				}
				
				if(mile.sub < 7){
					kat(`Come on! It isn't so bad! And it isn't permanent, I can get it removed if I want.`);
				}else{
					kat(`I thought it would be funny... and it isn't permanent, I can get it removed...`);
				}
				
				eva(`You should get it removed immediately! This your new *I am a trashy bimbo slut with zero self-respect* phase is getting out of hand!`);
			
			}
			con_link(mile.sub < 13, `too submissive`, `You wish to be as cool as me! `, 102);
			link(`You are probably right.... `, 103);
			con_link(mile.slut > 13, `not enough slutty`, `But I am a wanton slut! `, 104);
			break;
			
		case 102:	
			emo("angry");
			mile.sub--;
			mile.eva_exploit++;
			mile.girls_slutty_tattoo = 1;
			
			kat(`I'm getting tired of you constantly berating me! You should stop being so tight-laced, overconcerned with *what will people say* and overall *boring*! I don't give fuck and just do what I enjoy! I love to try to see how far I can go! And my new tattoo is awesome and provocative! I want people to see I am bold and open-minded!`);
			txt(`@sas unwittingly nodded, swayed by my excited speech. @eva shook her head, unwilling to accept her defeat:`);
			eva(`Whatever. It's your body, I don't care!`);
			next();
			break;
			
		case 103:
			emo("unco");
			mile.sub++;
			mile.girls_slutty_tattoo = 2;
			
			txt(`I feebly surrended to her arguments. She was right, what was I thinking when I decided for such an utterly ridiculous tatoo? I spent too much time listening to nerds and their arguments, they were trying to gasslight me into accepting I was just a wanton slut.`);
			eva(`Of course I am right!`);
			sas(`Yeah, @kat, the tattoo is really bad!`);
			eva(`You should be ashamed you even got it made! Please, next time consult such big changes with us before doing something stupid!`);
			next();
			break;
			
		case 104:
			emo("imp");
			mile.slut++;
			mile.girls_slutty_tattoo = 3;
			
			kat(`But I am a wanton slut! And I enjoy it. And I chose the tattoo because I want people to see it!`);
			txt(`@eva was bewildered, she definitely did not expect such a confession. Finally, she shrugged:`);
			eva(`I'm not sure if I want to be friends with such a wanton slut.`);
			next();
			break;
	}
}		
				
				
				
				
				
				
				
				
				
				
				
				
				
				
/*


					
				//Oh, @kat, 
			}else if(mile.sas_date === 2){
				sas(`I don't like it.`);
				txt(`Frowned @sas.`);
				sas(`It doesn't suit you. If anybody should wear a collar, it should be me! ...I mean... I had this idea I'll get one... because it looked fashionable... but now @kat got one... so it would look like I'm just copying her... that's what I meant...`);
				txt(`I winked at @sas, unknowing, confused @eva just ignored her rambling.`);
			}
				
				
				
				
				
				
				
details.sluttyTattooList = visible_list;
		details.hitedSluttyTattooList = hinted_list;



		
				if( details.sluttyTattoo ){
					if(mile.slut < 12) mile.slut++;
C					Whoa!
B					Nice! 
X					The nerds were speechless when they noticed the tattoo I got. And even more that I was not even bothering with hiding it. 	
				}else if( details.hintedSluttyTattoo ){
C					What is it? 
B					Show us! 
X					The nerds noticed my new tattoo. It was not clearly visible but their imagination was immediately captivated and they wanted to see more. 
				}





//TATTOO 1 
export const tattoo_initiation = (index)=> {
	task_intro();
		
	if(mile.a_burned){
		qaa(`The next task is absolutely perfect for a slut like you!`);
	}else{
		qaa(`I'm not sure if you will like our next task. But I think it's a good one.`);
	}
	kat(`Okay?`);
	qaa(`We want you to get a tattoo.`);
	kat(`A tattoo!?! I won't do anything permanent!`);
	
	
	  txt(`Right after school, I meet with ${NAME.sas}. She was initially very shocked by my new obscene tattoo but after a while admitted she always wanted to get tattooed. Not with something so vulgar, maybe with a butterfly or birds or so. `);
            }else if(mile.tattoo_got > 2){
                txt(`Right after school, I meet with ${NAME.sas}. She was rather impressed by my new tattoo and wanted one too, albeit smaller and less striking. `);
            }else{
                txt(`Right after school, I meet with ${NAME.sas}. She was rather impressed by my new tattoo and wanted one too. `);
            };
            
            txt(`She was absolutely unable to decide. Making up her mind took her longer than to me the whole process.  `);
            
            link(`Get a new tattoo before she decides. `, 331);
            link(`Wait for her. `, 332);
    
	
	
	if(mile.tattoo_got == 8){
								 txt(`My midriff was bare and a part of my new tattoo visible. It certainly attracted attention. I did not believe that from the brief glances was anybody able to figure out the audacious obscene inscription. But maybe somebody did and knew? Just thinking about that was making me wet. `);
								// if(mile.socialImplants < 1) txt(`Only ${NAME.eva} and ${NAME.sas} had mean and hurtful comments. `);
							}else{
								 txt(`My midriff was bare and a part of my new tattoo visible. It attracted attention and several people commented on it. The feedback was mostly positive. At least I chose to interpret their words that way. `);
								if(mile.socialImplants < 1) txt(`Only ${NAME.eva} and ${NAME.sas} had mean and hurtful comments. `);
							};	
							
							
						}else if(
							temp.shoulders && (mile.tattoo_got == 2 || mile.tattoo_got == 3)
						){
							 txt(`My arms were bare and my new tattoo fully visible. It attracted a lot of attention and several people commented on it. The feedback was mostly positive. At least I chose to interpret their words that way. `);
							if(mile.socialImplants < 1) txt(`Only ${NAME.eva} and ${NAME.sas} had mean and hurtful comments. `);
						}else{				
							 txt(`The classes were not very exciting which was a good thing. There was already a way too much excitement in my life. Nobody even noticed my new tattoo.`);					
						}
					
					}else{	
						 txt(`The classes were not very exciting which was a good thing. There was already a way too much excitement in my life. `);					
					};
					
					link(`The second break.`, 190);



qaa(`Good morning, slut! We have a new task for you! `);
					txt(`${NAME.qaa} started very harshly. That did not predict anything good. `);
					kat(`Hello?`);
					qaa(`We came up with a brilliant task to further your transformation into a trashy slut! `);
					qbb(`Sure, at first glance you already look like a whore. But something was missing. `);
					qcc(`And then we browsed some pictures and realized that. `);
					qaa(`A tattoo! `);
					kat(`Tattoo?!?! T
					
					
					txt(`### Tattoos are still wip`);
			//txt(`### You will get the chance to get more on Wednesday `);
			txt(`Finally, the school was over. To fulfill my task, I headed to a tattoo parlor. They ensured me the whole procedure is instant and painless. The only remaining question was what tattoo did I want and where? `);
			txt(`I could get one above an ankle. That would be quite tame and easy to hide. ${mile.sub < 0 ? "The nerds probably will not be very satisfied but I really did not care. " : "I was only afraid the nerds might not be satisfied just with that. "}`);
			txt(`Or I could get some badass tattoo on my arm. Such tattoo would certainly greatly boost my confidence. `);
			txt(`Full sleeve might have a similar effect but I was afraid it might look too trashy and excessive.  `);
			txt(`Maybe some bigger tattoo on my side? That would be pretty classy and sexy. `);
			if(mile.slut > 3) txt(`A lower belly tattoo would be even sexier. Maybe a bit too much sexy - unless I would be naked, a large part of the tattoo would stay hidden under my clothes, teasing everybody's imagination. I bet those nerds would like that. `);
			if(mile.slut > 10 && mile.sub > 10) txt(`Or maybe... I could get a tattoo which would instantly manifest everybody how huge pathetic slut I am. It was fun to play with that idea but I did not think it would be a very bright decision. `);

			//BEWARE MILE SLUT LEVELS ARE HARDCODED IN EDIT.TATTOO_2!!!! HAS TO BE CHANGED THERE TOO!!!
			link(`Get a tattoo. `,121)
			break; 
			
			
			EDIT.tattoos_2();
			if(mile.tattoo_got === 8){
				npc_set([2,"Tattoo artist"]);
				txt(`The guy asked me: `);
				npc(2,`Do you really want this? `);
				txt(`Before he asked me I had my doubts. But now I was completely sure! `);
				kat(`Yes. It fully expressed who I truly am. You see... `);
				npc(2,`You don't have to justify yourself! I met worse weirdos than you`);
			};
			*/
