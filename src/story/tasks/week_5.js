/**
	task assigned during the 5th week
	- collar 
*/

import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, tom, zan, ven, mik} from "Loop/text";
import {mol, maj, ped, bio, dur} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise, niceness, last, chat} from "Loop/text";
import {placetime, task_intro, task_reaction, log_sex} from "Loop/text_extra";

import {ra} from "Libraries/random";
import {stringifyHSLA} from "Libraries/color/index";
import {emo, draw, rollUpSkirt, wdesc, removeActual, removeEverything, update, updateDraw, effigy, allClothes, crew, wearRandomClothes, remove, wear, create, wears, showLess, showAll, violates, sluttiness, createWear, quickSaveOutfit, quickLoadOutfit  } from "Avatar/index";
import {active_timeslot, next_event, add_rule} from "Taskmaster/scheduler";
import {DISPLAY} from "Gui/gui";
import {cloneFigurine} from "Avatar/canvas/body";
import {rules_violation} from "Taskmaster/new_rules_check";
import {masochist, buttplug, dildo} from "Virtual/text";




//COLLAR
export const collar_initiation = (index)=> {
		task_intro();
		ext.rules.collar = true; //apply immediatelly this day; directly 
		mile.collar = true; //enables collars in shops and settings 
		mile.collar_task = true;  //because it was task (I guess it could be used ext.rules.collar  instead)
				
		qaa(`Dear @katalt, you're surely wondering what is the next task?`);
		qcc(`Don't worry, we didn't forget about it.`);
		if(wears.collar){ //FALLBACK - this should not happen
			qbb(`Don't worry, this one will be an easy one.`);
			qaa(`We kinda enjoy your collar.`);
			qcc(`It looks very sexy and visually implies to everybody you're a very freaky girl.`);
			kat(`What is the task?`);
			qaa(`We want you to always wear a collar on your neck.`);
			qbb(`From now till you finish the game.`);
			qcc(`Or even after that if you will feel like it.`);
			
		}else{
			qbb(`We even brought you something.`);
			txt(`I sighed:`);
			kat(`What did you bring me?`);
			qbb(`A new permanent part of your apparel.`);
			if(ext.rules.choker){
				qcc(`The good news is that you do not have to wear chokes anymore.`);
				kat(`And the bad news?`);
			}else if(wears.choker){
				qaa(`We really like your choker.`);
				qcc(`It is very sexy.`);
				qaa(`And so we decided to upgrade it.`);
			}
			txt(`@qbb brought from his bag a leather collar and gave it to me.`);
			kat(`You want me to wear this?!?`);
			qaa(`Obviously.`);
			kat(`In the school?!?`);
			qcc(`Yeah. Pretty kinky, isn't it?!`);
			if(mile.choker){
				kat(`The choker was at least subtle! This is too far!`);
				qbb(`Nah, it isn't too far, it's just a natural progression.`);
			}
			kat(`It would make me look like... some kind of BDSM sex slave!`);
			emy.cbaf(`That's the point! To show everybody how freaky girl you are. `);
			txt(`I shook my head in disbelief.`);
			
			if(mile.slut > 13 && mile.sub > 13 && emy.s){
				emy.abc(`Come on, @kat. Everybody already knows you're a sexually submissive nympho.`);
				txt(`@last meanly smirked.`);
			}
			
			if(mile.slut < 12){
				if(mile.sub < 9){
					kat(`I don't want them to think that!`);
					txt(`I defiantly stared at them.`);
				}else{
					kat(`I... I don't want them to think that...`);
					txt(`I nervously fiddled with the collar in my hands.`);
				}
			}else{
				kat(`The school isn't really the best place to show that!`);
			}
			
			aly.cbaf(`You're overthinking it, @kat. People will assume it is just a silly extravagant jewelry. `);
			txt(`@last tried to convince me. I wanted to believe he was right but the idea of me wearing the collar publicly on my neck still made me uneasy.`);
			
			if(niceness() < 0){ //TODO - or on least one enemy? 
				kat(`I don't like it. `);
				emy.abc(`Well, such a shame! Too bad we don't care, you'll wear it whether you want or not!`);
				txt(`He snatched the collar from my hands and uncomfortably tightly fastened it around my throat.`);
			}else{
				aly.abcf(`Come on, @kat, it's going to be fine.`);
				txt(`Ensured me @last, took the collar from my hands and fastened it around my throat.`);
			}
			qcc(`Perfect!`);
			
			remove("collar", PC, PC.inventory)
			createWear("everydayCollar");
		}
		next();
}
		
	
export const collar_action = (index)=> {
	switch(index){
		default:
		case 101:
			emo("unco");
			mile.stat--;
			
			txt(`I was not dismayed by the reaction of my classmates but rather by the lack of it. In the classrooms or in the hallways there were people who curiously checked my ${PC.collar.name} and their faces for a fleeting expressed amusement, scorn or sexual lust. However, overall nobody was particularly shocked or surprised I was wearing a collar. They had already gotten used to my uninhibited slutty outfits. I had to wonder, after this, will I be ever able to restore my reputation?`);
			
			if(!mile.sas_burned && !mile.eva_burned && !mile.sas_eva_burned){
				link(`Break. `, 102);
			}else{
				next();
			}
			break;
		
		case 102:
			txt(`The only one who directly mentioned the collar to me was @eva.`);
			if(mile.choker){
				eva(`You didn't drop the choker but decided to double down? Your fashion style might be questionable but I admire your audacity!`);
			}else{
				eva(`Cute collar!`);
			}
			txt(`She condescendingly complimented me.`);
			kat(`Thanks!`);
			txt(`I shrugged, pretending I was not bothered by her teasing.`);
			
			if(mile.sas_dom){
				txt(`@sas put an index finger into the ring on the collar and drew my face closer to hers.  ${mile.sas_date === 2 ? "For a moment I was spooked she is going to kiss me right in front of @eva but she just looked into my eyes and smirked: " :  "She looked into my eyes and smirked: "}`);
				sas(`I like it! It's very sexy! I wonder, does it symbolizes your desire to experiment to fulfill your naughty sexually-submissive fantasies?`);
				kat(`Maybe?`);
				txt(`I flirted with her and @eva who had no idea what was going on between us just shook her head. `);
				//Oh, @kat, 
			}else if(mile.sas_date === 2){
				sas(`I don't like it.`);
				txt(`Frowned @sas.`);
				sas(`It doesn't suit you. If anybody should wear a collar, it should be me! ...I mean... I had this idea I'll get one... because it looked fashionable... but now @kat got one... so it would look like I'm just copying her... that's what I meant...`);
				txt(`I winked at @sas, unknowing, confused @eva just ignored her rambling.`);
			}else if(mile.sas_angry){
				sas(`I don't like it. It looks stupid.`);
				txt(`Frowned @sas. She seemed to be in a bad mood.`);
			}else{
				sas(`I love it! It looks sexy and cool!`);
				txt(`On the other hand, @sas was fascinated by my new accessory and her praise seemed to be genuine.`);
			}
			next();
			break;	
	}
}
			
//TODO
	
	
	
	
	
	
	
	
