import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, tom, zan, ven, sve} from "Loop/text";
import {mol, maj, ped, bio, dur} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";
import {placetime, task_intro, task_reaction, log_sex} from "Loop/text_extra";

import {ra} from "Libraries/random";
import {stringifyHSLA} from "Libraries/color/index";
import {emo, quickTempOutfit, wdesc, removeEverything, removeActual, update, updateDraw, effigy, allClothes, crew, wearRandomClothes, remove, wear, create, wears, showLess, showAll, violates, sluttiness, createWear, quickSaveOutfit, quickLoadOutfit  } from "Avatar/index";
import {active_timeslot, next_event, add_rule} from "Taskmaster/scheduler";
import {DISPLAY} from "Gui/gui";
import {cloneFigurine} from "Avatar/canvas/body";
import {popup} from "Gui/gui"; 
import {uniforms} from "Data/items/uniforms";
import {payment} from "System/bank_account.js";
import {money} from "Data/items/prices";
import {masochist, buttplug, dildo} from "Virtual/text";

//POLEDANCING
export const poledancing_initiation = (index)=> {
		placetime(`schoolyard`);
		emo("suspect");
		qaa(`There's no new task today!`);
		kat(`Yay?`);
		txt(`I was not celebrating yet.`);
		qcc(`We want to appreciate your honor and determination and that you play along.`);
		kat(`Because I have to. Are you finally going to stop with the blackmail?`);
		if(mile.a_dating){
			qaa(`We're not blackmailing you!`);
		}
		qbb(`Something even better!`);
		qcc(`We have a gift for you!`);
		kat(`Okay, now I'm scared.`);
		qaa(`You don't have to be scared! We got you free gym lessons!`);
		kat(`Lessons of what?`);
		qaa(`Dancing.`);
		qcc(`Well, pole dancing.`);
		kat(`I'm not a stripper!`);
		qaa(`You don't have to be a stripper to pole dance, those are two completely different crafts!`);
		qcc(`Exactly! Stop being so closed-minded. Pole dancing is just a form of exercise and art. Pole dancing is great for exercise and self-expression. It enhances your flexibility, is great for joints and bones, improves your kinesthetic awareness and balance, helps your cardiovascular health and boosts your self-confidence.`);
		kat(`When it's so great why aren't you doing it yourself?`);
		qcc(`Well, I don't think I would look very esthetic on a pole.`);
		qbb(`If you would be able to hold onto one.`);
		qcc(`Well, yeah.`);
		qaa(`Just give it try, maybe you find out you enjoy it.`);
		qbb(`Maybe you find out enjoy being a stripper... I mean... pole dancer.`);
		kat(`So the first lesson is today? I'm not sure, maybe I'll drop by...`);
		qbb(`The lesson is mandatory for you.`);
		kat(`You told me there's no task today!`);
		qaa(`There's no task today but you taking the lesson is mandatory.`);
		next();
}


export const poledancing_action = (index)=> {
	switch(index){
		default:
		case 101:
			set.irl();
			mile.poledancing = 1;
			present(
				/*[`sve`, `sexy`],*/
				[6, `Magda`, `poledancer`],
			);
			placetime("street");
			txt(`Mandatory pole dance lessons seemed more like tasks than a gift! However I have to admit, I was a bit curious. A while back a saw a clip from a talent show of a pole dancer doing absolutely incredible things.`);
			txt(`So I set out, going on the address @qcc gave me. I checked the studio online and it seemed legit but when I got to their supposed location, I got suspicious. The building did not seem like a gym. I guess I looked lost because a passing-by woman approached me:`);
			sve( `Hello! Are you looking for something?`);
			txt(`Her voice was soothing but with a noticeable accent.`);
			if(mile.slut < 8){
				kat(`Well, I'm looking for ...dance lessons.`);
				sve( `Pole dancing lessons?`);
				txt(`I looked at her more properly. She was in her early twenties, blonde, beautiful, curvy and fit. She could be a stripper.`);
				kat(`Well, yeah. <small>Pole dancing</small> lessons.`);
				sve( `You don't have to be ashamed! You don't have to be a stripper to do pole. There's nothing lascivious or shady on pole fitness, @npc_6 will certainly explain you that.`);
			}else{
				kat(`Well, I'm looking for pole dancing lessons... But don't worry, I'm not a stripper.`);
				sve(`Good to know!`);
				kat(`I mean, there's of course nothing wrong with being a stripper.`);
				txt(`I quickly added. She was in her early twenties, blonde, beautiful, curvy and fit. She could be a stripper.`);
				sve(`It isn't but I'll warn you, @npc_6 doesn't like this kind of jokes, she doesn't like pole fitness to be associated with stripping.`);
			}
			kat(`Who's @npc_6? I'm @kat, nice to meet you!`);
			sve(`Hi, I'm @sve! She is the instructor.`);
			kat(`Are you going to the lessons too?`);
			sve(`Yeah. The entrance to the gym is in the courtyard.`);

			link(`Thanks! `, 102);
			break;
			
			
		case 102:
			present(
				[`sve`, `poledancer`],
				[6, `Magda`, `poledancer`],
			);
			emo("happy");
			
			quickTempOutfit(["shorts", "halterTop"]);
			placetime("dance studio");
			txt(`We went to a small basement gym, the space probably served a different purpose before it was remade into a dance studio consisting of a dressing room, tiny bathroom and large room with a few poles and one wall covered with mirrors. There were already several women of various ages and body proportions.`);
			npc(6, `Hello, you're the new one?`);
			kat(`Hi! I'm @kat!`);
			txt(`I was greeted by a cheerful woman, she had to be in her late thirties or early forties but her body - on display due to her very skimpy gym clothes - was admirably firm and fit and she moved with casual finesse.`);
			if(mile.slut < 3 || mile.sub > 5){
				npc(6, `I'm @npc_6! Come no, don't be shy!`);
				txt(`She smiled at me encouragingly when she saw my doubts.`);
			}else{
				npc(6, `I'm @npc_6!`);
				txt(`She shook my hand.`);
			}
			npc(6, `Do you have any previous experience with pole dancing or are you a complete newbie?`);
			kat(`No, but I did a bit of gymnastic and normal modern dancing.`);
			npc(6, `What made you interested in pole dancing?`);
			kat(`The lessons were a gift, and well...`);
			txt(`I shrugged.`);
			npc(6, `People wrongly associate pole dancing with something lewd but they couldn't be more wrong! Pole dancing is awesome! It's great for exercise and self-expression. It enhances your flexibility, is great for joints and bones, improves your kinesthetic awareness and balance, helps your cardiovascular health and boosts your self-confidence. It has ancient roots and it's based on the Chinese pole and the ancient Indian mallakhamb traditions!`);
			txt(`She lengthily explained and then told me to warm up and stretch. During exercise, I chatted with @sve and then we helped each other to stretch. I noticed how incredibly flexible she was.`);
			
			link(`Pole dancing. `, 103);
			break;

		case 103:
			emo("focus");
			txt(`I went to one of the poles and checked how firmly it was anchored.`);
			kat(`Is it supposed to spin?`);
			txt(`I observed and the girls laughed. @npc_6 explained to me the basic moves, grabbing the pole and spinning forward and backward, with my leg wrapped around the pole or just holding it with my hands. Then @npc_6 made the pole stop spinning and showed me how to hang on a static pole.`);
			txt(`I lacked her strength and grace, however, when I checked myself in the large mirror, even the simple moves looked pretty good. Or I thought so until I turned around and saw what was @sve doing. She was wildly spinning around the pole, in a normal way and then upside down, climbed up and slid downwards and while hanging horizontally she spread her legs in ways I would find challenging even while sitting on the floor. ${mile.lesbian > 1 ? "Seeing such incredible dexterity made me imagine all sorts of naughty things. " : "If I was into girls, she would be certainly triggering something inside me."}`);
			txt(`When we were done, my whole body - even the muscles I had no idea existed - was sore but I felt very pleasantly tired and stretched.`);
			txt(`@sve was accompanying me when we were waiting for our trams.`);
			kat(`You're so good!`);
			sve(`I'm not that good! There are many more things I have to learn! I started because I wanted to expand my works skills but discovered I love it. Now I spend most of my free time hanging head down like a bat and I'm thinking about becoming an instructor too.`);
			kat(`Wow.`);
			sve(`But you're great! You are dexterous and with a bit of training, you could be amazing too!`);
			kat(`I bet @npc_6 bribed you and you're telling this to all the beginners to stop them from quitting!`);
			sve(`Hah, no. Trust me, I have an eye for this! Oh, that's my tram. See you at the next lesson!`);
			kat(`Well, I'm not sure if I... bye!`);
			txt(`I hastily cried when she entered the tramcar and waved me goodbye.`);
			next(`Done. `, quickLoadOutfit);
			
	}
}




export const sveta_stripper = (index)=> {
	switch(index){
		default:
		case 101:
		
		present(
			//[4, `Světa`, `slutty`, `sve`],
			/*[`sve`, `slutty`],*/
			[6, `Magda`, `poledancer`],
		);
		
		
		quickTempOutfit(["sexyShorts", "sexyHalterTop"]);
		placetime("dance studio");
		
		txt(`I enjoyed the exercise and was deeply impressed by the skill of @npc_6 and @sve. So I decided I will not care the pole dancing lessons were given to me by a trio of perverted nerds who probably enjoyed imagining me writhing around a pole and throwing my clothes away. If they were so dumb and horny they willingly paid for the lessons, I would be stupid if I did not take advantage.`);
		txt(`This time I knew where to go and what to expect. I even brought a bit sexier clothes. ${mile.slut < 10 ? "I was less embarrassed in front of strangers" : "I was less concerned they will think I am a shamelessly showing-off"}, moreover, in the clothes I was wearing the last time I was constantly sliding down. To do the tricks properly you needed enough friction between the chrome pole and your bare skin.`);
		txt(`I was looking forward to seeing @sve but to my chagrin, she was not present. I did not get along with other women so well. We stretched and then @npc_6 explained to us what to do, she focused mostly on me since I was the least experienced begginer.`);
		txt(`About ten minutes before the supposed end, @sve finally arrived. She was limping, walking with a crutch, her left leg in orthose.`);
		npc(6, `Oh my! What happened to you!`);
		txt(`Threw her hands up @npc_6.`);
		kat(`Did you fall off a pole?`);
		sve(`There are dozens of ways to twist your ankle completely unrelated to pole dancing. But if you really want to know it, coincidentally I did.`);
		npc(6, `Poor girl! You're barely walking! You didn't have to come personally, you could just text me!`);
		sve(`I was in the neighborhood, I went to regen therapy. And I wanted to say hi to the girls. Hi girls!`);
		kat(`Hi, @sve!`);
		txt(`I continued with my training I eventually quit only when @npc_6 began looking really impatient, I was the last one in the gym. I went outside and was a bit surprised @sve was waiting for me.`);
		sve(`Do you have time, @kat?`);
		kat(`Sure, what do you need?`);
		sve(`Well... I'm not sure how to start...`);

		link(`Start at the beginning. `, 102, ()=> counter.temp = 1)
		link(`What were you actually doing when you broke your leg? `, 102, ()=> counter.temp = 2)
		link(`Are you asking me out for a date? `, 102, ()=> counter.temp = 3)
		break;
		
	case 102:
		quickLoadOutfit();
		emo("shy");
		if(counter.temp === 2){
			kat(`Were you doing someting extremely humiliatig or stupid? Is there a reason you couldn't say the truth in front of @npc_6?`);
			sve(`No! This isn't about my leg. Well, it is about my leg but only indirectly.`);
		}else if(counter.temp === 3){
			mile.poledancing_gf = true;
			sve(`What!?! No! Did I somehow give you an impression... sorry, you're very attractive but I have a boyfriend.`);
			kat(`I was just joking!`);
			sve(`Oh! Me too! I was just playing along, I would never fall for such a dumb joke.`);
		}
		txt(`@sve shrugged.`);

		sve(`Well, you know that not every pole dancer is a stripper? Well, some of them are.`);
		kat(`Are you telling me prim @npc_6 has a dark secret?`);
		sve(`No! I was talking about myself! I'm working as an exotic dancer and sometimes a stripper.`);
		kat(`That sounds interesting! Are you making a lot of money?`);
		sve(`Yes.`);
		kat(`Cool.`);
		sve(`But due to obvious reasons, I'm currently unable to perform. Even though I have already arranged gigs and it's too late to cancel them.`);
		kat(`I still don't understand.`);
		sve(`Do you want to make some cash? You were already, eighteen, right?`);
		
		link(`My pole dancing is far from perfect!`, 103);
		link(`I'm horrible at pole dancing!`, 103);
		break;
		
	case 103:
		sve(`This isn't pole dancing, just stripping. You just need to sway sexily and trust me, you're more than qualified for that! And I can give you a quick crash course what to do.`);
		if(mile.slut < 9){
			kat(`I'm too embarrassed to strip in front of complete strangers!`);
			sve(`You don't have to, you're hot. And it isn't a big deal, like, you're not the first girl they saw naked!`);
		}
		kat(`I'm not sure. You probably should better ask one of your stripper colleagues or somebody more experienced from the classes!`);
		sve(`Well... I already kinda did.`);
		kat(`Knowing you're asking me only because you're getting desperate really boosts my confidence.`);
		sve(`I wasn't going to tell you you weren't my first choice. You have only yourself to blame! But I wouldn't ask you if I wasn't sure you can do it.`);
		kat(`What would you want me to do? Dance on a stage in a club?`);
		sve(`No, it's a small private birthday party. Don't worry, I know the guy. He hired me before and was always nice and there never were any problems. And I'm sure they will forgive you if your performance won't be perfect. I would hate to let him down!`);
		txt(`@sve shrugged.`);
		sve(`But of course, I don't want to push you! You should not agree if it makes you uncomfortable. But I thought I'll try to ask you.`);
		
		if(mile.slut < 6){
				txt(`Her suggestion was making me very uncomfortable. Removing clothes in front of people was not for me! On the other hand, @sve needed my help. And I needed money. And, honestly, I was a quite curious. How does it felt to be a stripper? Would I be able to do it? `);
		}
		
		con_link(mile.slut > 9, `low slut`, `Of course! Thanks for the opportunity! `, 110);
		link(`I can do it for you. `, 111);
		link(`Sorry but no. `, 112);
		link(`How dare you, I'm not a stripper! `, 113);
		break;
		
//TODO MENTION STRIPPING IN VIRUTAL REALITY 
	case 110:
		mile.sveta_stripper = 2;
		mile.slut++;
		emo("imp");
		sve(`You'll do it?`);
		kat(`Yeah! I'm not saying I want to be a stripper but I always wondered what it's like to be one.`);
		sve(`If that's your only reason, you could just ask.`);
		kat(`No, I want to experience it. But you have to explain to me exactly what to do!`);
		sve(`Don't worry!`);
		kat(`Awesome!`);
		next();
		break;
		
	case 111:
		mile.sveta_stripper = 1;
		emo("relax");
		if(mile.slut < 8) mile.slut++;
		kat(`I'm not sure. But I guess I could do it if you really need my help.`);
		sve(`Are you sure you would be okay with it?`);
		kat(`...yes.`);
		sve(`Awesome! Don't worry, I'll explain you exactly what to!`);
		next();
		break;
		
	case 112:
		mile.sveta_stripper = -1;
		if(mile.slut > 12) mile.slut--;
		emo("neutral");
		kat(`I'm sorry but I can't do that!`);
		sve(`No problem! I'm sorry I even suggested it! I hope you're not offended!`);
		kat(`No! I don't mind that or you being a stripper. I just can't do it myself.`);
		next();
		break;
		
	case 113:
		mile.sveta_stripper = -2;
		mile.slut--;
		emo("angry");
		kat(`How dare you even ask me that?! We barely even know each other but you're not ashamed to ask me to get undressed in front of some sleazy horny guys just to fulfill your obligations?`);
		sve(`I'm sorry, @kat! I didn't want to offend you! I thought I just ask and...`);
		kat(`I'm not judging you for being a stripper but don't try to drag me into it too!`);
		sve(`I'm sorry, @kat! Please forget I even asked!`);
		next();
		break;	
	}
}



//TODO - ADD FLAVOUR FOR LOW SLUT
export const sveta_stripper_party = (index)=> {
	switch(index){
		default:
		case 101:	
			present(
				[1, `Jožin`],
				[2, `Guy`],
				[3, `Birthsday boy`],
				/*[`sve`, `yoga`],*/
			);
			
			placetime("@sve's place");
			{
				const vr = (()=>{
					if(mile.goth_stripper === 3){
						return `I admited that tried stripping in a virtual game and that it got pretty wild`;
					}else if(mile.goth_stripper === 2){
						return `I admited that tried stripping in a virtual game, even though I did not went completely nude`;
					}else if(mile.goth_stripper === 1){
						return `I admited that I tried seductive dancing in a virtual game but I was too embarrassed to strip`;
					}else{
						return `I admited I was a complete novice`
					}
				})();
				
				
				txt(`Afternoon I went to @sve's place where she gave me a quick crash course about stripping. When she asked me if I ever tried it, ${vr}. `);
				txt(`@sve was a pretty good teacher and seemed she had plenty of experience with being a dancer and stripper. She explained to me that the crucial thing was attitude, bringing enough energy and enthusiasm. When we were done with the lesson, she opened her wardrobe to borrow me a costume. `);
				if(mile.boobs && !mile.poledancing_boobs ){ //TODO!!§
					mile.poledancing_boobs = true;
					txt(`When I was changing, she out of nowhere observed:`);
					sve(`I love your tits, by the way.`);
					kat(`What?`);
					sve(`Would you mind?`);
					kat(`${mile.slut < 8 ? "Ehhh, no? " : "Sure, go ahead! "}`);
					txt(`She curiously squeezed them.`);
					sve(`Amazing work! The sarcophagus?`);
					kat(`Yeah!`);
					sve(`I thought so. Some of the girls I know have sarcophagus-enlarged tits too. When I got mine done, it wasn't available yet so they are done in the old-fashioned way. Do you want to check them?`);
					txt(`I touched @sve's breasts. I had already suspected they were not natural. They were too firm and perfectly shaped.`);
					kat(`...are you happy you got your tits enlarged?`);
					sve(`Damn yeah! It was the single best decision I ever made! And a shrewd investment, they made me far more money than I spent on them!`);
				}else{
					txt(`Our builds were fairly physically similar, we had the same height, just with one big difference. Respectively, two big differences. @sve confessed her look was not completely natural and original. And explained big fake tits were the best investment a woman could make. However, she still possessed several older slutty outfits from times before her <i>upgrade</i>.`);
				}
					
				txt(`(TODO: sexy costumes)`);
			}
			link(`Try the outfit. `, 102);
			break;
			
		case 102: 
				placetime("@npc_1's flat");
				emo("shy"); //emo??
				txt(`@sve had tons of interesting stories. We were chatting and had fun until was time for me to go. @sve offered she will chaperone me but I did not want to force her to limp along with me.`);
				txt(`So I alone took a tram to the opposite part of the city and used my phone to find the right address. I was pretty anxious when I rang the bell. But the jovial man in his early forties cheerfully greeted me and invited me inside.`);
				if(mile.poledancing_gf){
					npc(1, `So you're Joana's girlfriend? I had no idea she's bisexual and has a girlfriend.`);
					kat(`What!?! Me neither! I mean, not before I found out she's bisexual and became her girlfriend!`);
					txt(`Haha, very funny, @sve!`);
				}else{
					npc(1, `So you're Joana's friend?`);
					kat(`Yeah, I am!`);
				}
				npc(1, `How is she doing? She told me she had injured her leg?`);
				kat(`Yeah. But nothing serious, she's on regen so she'll soon be fine!`);
				npc(1, `Great! Tell her we wish her a quick recovery! Even though...`);
				kat(`...what?`);
				npc(1, `I kinda like the replacement she sent us.`);
				txt(`He smiled.`);
				npc(1, `I'm @npc_1!`);
				txt(`He firmly squeezed my hand.`);
				
				link(`I'm Candy. `, 103, ()=> mile.poledancing_name = "Candy");
				link(`I'm Destiny. `, 103, ()=> mile.poledancing_name = "Destiny");
				link(`I'm Trixie. `, 103, ()=> mile.poledancing_name = "Trixie");
				link(`I'm... `, 300, ()=> mile.poledancing_name = "Candy");
				break;
		
		case 300:
				popup.prompt(`Stripper name: `, mile.poledancing_name, (a) => mile.poledancing_name = a, 
					["Amber", "Bambi", "Brandy", "Bunny", "Candi", "Candy", "Chastity", "Cherry", "Chrystal", "Cookie", "Crystal", "Destinee", "Destiny", "Diamond", "Ginger", "Jade", "Jewel", "Jinx", "Kitten", "Kitty", "Krystal", "Lexie", "Lola", "Misty", "Nikki", "Roxi", "Roxie", "Sinnamon", "Star", "Sugar", "Synnamon", "Tiffany", "Trisha", "Trixie", "Venus", "Vivian", "Bunny", "Candi", "Jewel", "Staci", "Sindy"],
				);
				npc(1, `Delight to meet you!`);
				link(`Next. `, 103)
				break;
				
				
		case 103:
				emo("fine");
				txt(`@npc_1 led me to the living room. The room was filled with cigar smoke and several guys of the same age as my host were sitting in armchairs around the table covered with bottles of liquor and plates with snacks.`);
				npc(1, `Boys! Let me introduce to you ${mile.poledancing_name}!`);
				npc(2, `Hello!`);
				txt(`They welcomed me and laughed.`);
				kat(`Good evening guys! Who's the birthday boy?`);
				npc(3, `That would be me! I thought you were joking about the stripper!`);
				npc(1, `I never joke about strippers! Open sandwitch? Or some other snack?`);
				txt(`@npc_1 agressively pushed the plate with open sandwitches to me. `);
				npc(1, `Don't be shy! `);
				kat(`No thanks!`);
				npc(1, `Come on! I prepared them myself!`);
				kat(`Thank you, maybe later!`);
				npc(2, `What are you drinking?`);
				txt(`One of the guys already reached for a clean shot glass.`);
				
				link(`Rum. `, 104);
				link(`Vodka. `, 104);
				link(`Jäger. `, 104);
				link(`Nothing, thanks.  `, 104, ()=> counter.water = true);
				break;
				
		case 104:
				emo("shock");
				if(counter.water === true){
					txt(`The guy ignored my wish and poured me a very generous double shot. We clinked but when he was not paying attention, I placed the full glass back on the table. I felt it would be better if I stayed sober.`);					
				}else{
					txt(`He poured me a very generous double shot. I took it, feeling my confidence could use a little boost.`);
					npc(2, `Cheers!`);
					txt(`We clinked and I drank it.`);
				}
				npc(1, `What are you going to need?`);
				txt(`I instructed them how to arrange the armchairs in a half-circle. Then we spend the next forty minutes trying to pair up my phone with his stereo system but we eventually were successful. The dooring buzzed again, @npc_1 showed me the bathroom where I could change and went to greet his last quest.`);
				txt(`I instinctively locked the door behind me before I realized how silly it was. They will soon see me nude anyway! I quickly put on my sexy ${choice("costume")}, fixed my makeup and returned among the guys, physically and mentally ready to give them my best performance.`);
				dur(`@KATALT!?!`);
				txt(`Gasped the newcomer when I swayed back in the living room.`);
				kat(`@dur!?!`);
				txt(`I was stunned when I recognized my history teacher. Other guys were confused for a moment but as soon as they realized what was going on, they burst out laughing.`);
				npc(2, `She's one of your students!?! Hahaha!`);
				dur(`Is... is this a joke?!?`);
				npc(1, `No. I had no idea!`);
				txt(`@dur grabbed me and pushed me to the hallway where we could talk privately.`);
				dur(`What are you doing here!?! You're a stripper?!?`);
				
				effects([{
					id: `costume`,
					fce(){
						quickSaveOutfit();
						removeEverything(PC);
						createWear("sexyPushUpBra", undefined, {color: "nearlyBlack"});
						createWear("thong", undefined, {color: "nearlyBlack", ruffling: true});
			//TODO GARHTERBELT
						createWear("fishnetStockings");
						createWear("miniSkirt", undefined, {color: "dark", outerLoose: 1.7 });
						createWear("sexyHalterTop", undefined, {color: "cute", alpha: 0.95});
						updateDraw(PC);
					},
				}])
				
		
		
				link(`No!`, 105, ()=> mile.poledancing_job = 1);
				link(`Yes.`, 105, ()=> mile.poledancing_job = 4);
				con_link(mile.sub < 8, `submissive`, `That's none of your business! `, 105, ()=> mile.poledancing_job = 3);
				con_link(mile.sub > 2, `dominant`, `Please, don't tell anybody! `, 105, ()=> mile.poledancing_job = 2);
				break;
				
		case 105:
				mile.poledancing_job >= 3 ? emo("shock") : emo("help");
				
				if(mile.poledancing_job === 4){
					mile.slut++;
					kat(`Yes. Do you have a problem with that?`);
					txt(`I shamelessly challenged him.`);
					dur(`...well... no. I guess you now being a stripper explains a lot.`);
					txt(`Were the recent changes in my wardrobe and behavior really so noticeable?`);
				}else if(mile.poledancing_job === 3){
					mile.sub--;
					kat(`That's none of your business!`);
					txt(`@dur for a moment looked offended by my sassiness before he remembered we are not in a class.`);
					dur(`I guess you're right. Hmm... you being a stripper explains a lot.`);
					txt(`Were the recent changes in my wardrobe and behavior really so noticeable?`);
				}else if(mile.poledancing_job === 2){
					mile.sub++;
					kat(`Please, don't tell anybody!`);
					txt(`I desperately begged him. He could get me in very serious trouble if he wanted. He looked sternly but then nodded.`);
					dur(`Fine! But you don't tell anybody about this party either!`);
					kat(`Fine!`);
				}else if(mile.poledancing_job === 1){
					mile.slut--;
					kat(`No! I'm not a stripper! This is just a huge misunderstanding! I'm just a helping friend, who got injured and...`);
					txt(`I was desperately explaining.`);
				}
			
				if(mile.poledancing_job !== 2){
					dur(`I would prefer if you didn't tell anybody about... this situation!`);
					kat(`I wanted to ask you for the same thing! Please!`);
					dur(`Deal. As far as I'm concerned, this didn't happen.`);
					kat(`Thank you.`);
				}
				
				dur(`You're still going to strip?`);
				kat(`Yes.`);
				dur(`Maybe it would be better if I skipped the striptease. Considering our public relationship, it wouldn't be right! You're my student!`);
				
				link(`Thank you, that would be the best!`, 106, ()=> mile.poledancing_teacher = -1);
				link(`That wouldn't be fair! `, 106, ()=> mile.poledancing_teacher = 1);
				con_link(mile.slut > 7, `low slut`, `I want you to watch!`, 106, ()=> mile.poledancing_teacher = 2); //TODO
				break;
				
		case 106:
				emo("relax");
				if(mile.poledancing_teacher === -1){
					mile.slut--;
					kat(`Yeah, I think that would be the best, after all, you're my teacher and I'm your student. Thank you for being so considerate.`);
					dur(`Yeah, the best.`);
					txt(`He nodded distantly while staring at my body in the skimpy stripper costume. Despite his restrained stance and everything he said, I saw in his eyes he wanted to see me naked.`);
				}else if(mile.poledancing_teacher === 1){
					kat(`Yes, this is pretty awkward. But I don't want to ruin the party. It wouldn't be fair to ostracize you, you should be able to enjoy everything with your friends.`);
					dur(`I'm not sure, @katalt.`);
					kat(`I think we're both adults and who can keep their public and private lives separate.`);
					dur(`You're an adult only barely! ...but fine. I have no problem handling the situation if you don't mind.`);
					txt(`Despite his restrained stance and everything he said, I saw in his eyes he wanted to see me naked.`);
				}else if(mile.poledancing_teacher === 2){
					mile.slut++;
					emo("imp");
					kat(`But I want you to watch!`);
					dur(`What!?`);
					kat(`I want you to see me in a different light than when we are in school! To see the real, uncovered, me!`);
					dur(`But I'm your teacher and you're my student! I fully understand if this is awkward for you. I don't want you to feel uncomfortable while... doing your job.`);
					kat(`It doesn't make me uncomfortable at all! I want you to be there and have fun with your friends!`);
					txt(`Despite his restrained stance and everything he said, I saw in his eyes he wanted to see me naked.`);
				}
				txt(`We returned.`);
				npc(1, `What's going on? Is the striptease canceled?`);
				if(mile.poledancing_teacher === -1){
					dur(`No! But I think I'll rather stay in the kitchen. There's... conflict of interests...`);
					txt(`The guys heartlessly ridiculed him but respected him more for his decision.`);
				}else{
					kat(`Of course not! If you're all ready, I am!`);
				}
				link(`Striptease. `, 108);
				break;
				
		case 108:
				emo("imp");
				kat(`Okay, let's go!`);
				txt(`I deeply bend down, giving them a nice view, and turned the music on my phone on. Syncing the phone with the stereo system took us long but now the stereo automatically synchronized with smart lights and instantly turned the average living room into a flashy dance club. Okay, let's do it.`);
				txt(`I confidently strode towards the birthday boy, placing my high-heeled feet one in front of the other, seductively swaying my hips. He was sitting on a chair and I was authoritatively stood right above him. I playfully brushed my fingers against his cheek and then went behind him and with a silk scarf tied his hands.`);
				txt(`I walked back into the center of the room, loosened my ${choice("top")} and threw it away while smirking at him. The guys loudly applauded, I could feel their energy, they were loving my performance. ${mile.poledancing_teacher >= 1 ? "And I saw even @dur was clapping. " : ""}`);
				txt(`Still maintaining the eye-contact with him, I was gyrating, hands on my hips. I turned my back at him, slightly leaned forward and extremely slowly tugged my ${choice("skirt")} down. The skirt fell on the floor, I stepped out of it and returned closer to the birthday boy. I was running my hands along my body, I tried to imagine how would he want to touch me if his hands were not tied.`);
				link(`Strip down. `, 109);
				
				effects([{
					id: `top`,
					fce(){
						remove("upper");
					},
				}])
				effects([{
					id: `skirt`,
					fce(){
						remove("lower");
					},
				}])
				break;
				
				
		case 109: 		
				txt(`It took only a glance and @npc_1 understand and sent my way a prop chair. I cleverly stopped it with the tip of my feet and then threw my leg on top of it. Very slowly I undid my gather and then rolled the ${choice("stocking")} down. When they were both gone and I was barefoot, I put my stilettos back on. I was only in my (respective, @sve's) lingerie and the crow was getting wild.`);
				txt(`I teasingly turned my back to them when I was unclipping my ${choice("bra")}. I removed it, hold it in a stretched arm and then nonchalantly let it fall down. The guys were absolutely thrilled when I finally showed them my tits and fondled them right in front of them. I even went so far I sit astride on the birthday boy's lap and let him see every glorious detail.`);
				txt(`Then was time for the big finale. Again, I returned to the center of the room and turned back to them. I wiggled with my ass, pushed my fingers under the string holding the front and back pieces of my tiny, sexy ${choice("panties")} together and leisurely slipped them down. The guys enthusiastically cheered when I ultimately posed before them wearing only my heels.`);
				txt(`I was focusing only on my movement, trying to look sexy and flirty and maintaining the connection with the birthday guy. Until the music played, there was no time to be ashamed or feel weird or really think about anything.`);
				
				if(mile.slut < 15){
					txt(`But when I finished and switched the music off, the spell was broken. I felt very awkward when I was collecting my garments scattered all over the living room.`);
				}else{
					txt(`Still, even when I switched the music off, I was not bashful and stayed nude while joking and chatting with them for a little longer.`);		
				}
				
				effects([{
					id: `stocking`,
					fce(){
						remove("socks");
					},
				}])
				effects([{
					id: `bra`,
					fce(){
						remove("bra");
					},
				}])
				effects([{
					id: `panties`,
					fce(){
						remove("panties");
					},
				}])
			
				link(`Continue. `, 110);
				break;
				
		case 110: 
				quickLoadOutfit();
				emo("relax");
				if(mile.poledancing_teacher === -1){
					kat(`@dur? `);
					txt(`I peeked into the kitchen, covering myself with my collected clothes. `);
					dur(`@katalt? `);
					kat(`I'm finished! You can go back!`);
					dur(`Thank you!`);
					kat(`Thank you for being so... nice!`);
					txt(`I smiled at him and went to get dressed. `);
				}else if(mile.poledancing_teacher === 1){
					txt(`I was collecting my clothes and @dur bashfully handed me the bra I dropped close to him.`);
					kat(`I hope you don't regret staying? `);
					dur(`I think it was a wrong decision. But I don't regret it! `);
					txt(`We laughed and I went to get dressed. `);
				}else if(mile.poledancing_teacher === 2){
					txt(`After my performance, I went to chat with @dur. He bashfully handed me the bra I dropped close to him.  `);
					kat(`What do you think? Was I good? I really care about your opinion!`);
					dur(`Yes, you were, @katalt. I wish you were as good in my history classes. `);
					kat(`Hah, don't be such a buzzkill! `);
					txt(`I teasingly tapped his shoulder and went to get dressed. `);
				}
				
				txt(`When I returned, the guys still made fun of poor @dur: `);
				if(mile.poledancing_teacher === -1){
					npc(3, `You missed a lot! She's a sexy little minx with a perfect tight body!`);
				}
				npc(2, `Are all your students so hot? Shit, I should've become a teacher! `);
				npc(1, `I couldn't be a teacher! I wouldn't be able to teach with a raging hard-on in my pants. `);
				txt(`@npc_1 laughed until he noticed I was already back in the room and caught him fantasizing about b${String.fromCharCode(97)}rely leg${String.fromCharCode(97)}l girls. `);
				npc(1, `Well, anyway. Great performance, @katalt. ...I mean... ${mile.poledancing_name}.`);
				txt(`I handwaved it. My secret identity did not last for very long. `);
				
				link(`Finish. `, 111);
				break;
				
		case 111:
				emo("anoy");
				payment("striptease","cash");
				txt(`They would love me to stay but I decided to leave. @npc_1 was loudly praising my skills and handed me the envelope with money. I did not even open it, I was still not fully able to comprehend what just happened. I was a stripper and it seemed a pretty decent one!`);
				txt(`I rode back to @sve to return her the clothes. She wanted to hear everything and was full of praise too. @sve took the envelope from me, opened it, quickly counted all the bills, took like half of them and handed the rest back to me.`);
				kat(`Hey, what are you doing?`);
				sve(`Taking my cut.`);
				kat(`But I did all the actual work!`);
				sve(`But I'm your manager and I got you the job and the costume. Oh you sweet naive girl, you have a lot the learn about the business!`);
				next();
				break;
				
	}
}

/*

	something something duro
	
	together with světa

*/








//STRIPSHOW - lesbian show with sve 
export const stripshow = (index)=> { switch(index){
	default:
	case 101:
		present(
			[1, `Jožin`],
			//[4, `Světa`, `slutty`, `sve`],
			/*[`sve`, `slutty`],*/
			[6, `Magda`, `poledancer`],
			[`sve`, `poledancer`],
		);

	//TODO SPORTS BRA
		showLess(1, 1);
		effigy.sve.showLess(0, -1);
		emo(`happy`);
		placetime("dance studio");


		txt(`I again went to the pole dancing classes. I enjoyed it and I felt I was quickly getting better. And what was making the lesson even better, in the small changing room I ran into topless @sve.`);
		kat(`Hello!`);
		sve(`Hey!`);
		kat(`I didn't expect to see you here! How's your ankle?`);
		sve(`Not yet completely healed but I'm allowed to do light exercises. The regen therapy is doing wonders.`);
		kat(`I'm happy to hear that! At least I won't have to fill in for you!`);
		sve(`@kat! I'm so fucking grateful you helped me. I owe you anything you ask! And you have my deepest respect, because you dared to do it!`);
		 if(mile.slut > 10){
			kat(`Maybe you can give me another stripping lesson? Or maybe get me another gig?`);
			sve(`Hah! You imp, you enjoy taking your clothes off in front of strangers, don't you?`);
			if(mile.slut > 20){
				kat(`Hah! I do! And when I also get paid, what's there not to love?`);
			}else{
				kat(`Hah, I enjoy getting paid.`);
			}
		 }else{
			kat(`Thanks! If I will need a stripper, I'll ask you!`);
		 }
		txt(`She cheerfully laughed and put on a sports bra.`);
		sve(`But seriously, you did an amazing job. I talked with @npc_1 and everybody absolutely loved your performance!`);
		 
		 if(mile.slut > 10 && mile.sub < 10){
			kat(`I'm sure they did! I think I absolutely killed it!`);
		 }else if(mile.slut < 7 || mile.sub > 20){
			kat(`Did they? I was absolutely terrified! I'm glad they didn't notice how nervous I was.`);
			sve(`They noticed but they thought it was cute. They are great guys.`);
		 }else{
			kat(`Thank you! You know how to boost my confidence!`);
		 }

		link(`Poledancing. `, 102);
		break;


	case 102: 
		showAll();
		effigy.sve.showAll();
		quickTempOutfit(["sexyShorts", "sexyHalterTop"]);
		placetime("dance studio");
		counter.details = sluttiness(PC).details;

		txt(`After a short warming up and stretching, I continued learning new tricks. I got paired with @sve - since she was still not in the condition to do her crazy exercises, she spent most of her time lecturing me. She showed me a pirouette and fireman's spin and chair spin and how to combine them with other moves into routines.`);
		sve(`Point your toes! It looks so much better!`);
		kat(`I know! You already told me! I'm trying, I'm just unable to focus on ten different things at the same time!`);
		txt(`I knew what I was supposed to do but took a bit of training to drill how to move with each of my limbs and keep the right esthetical posture.`);
		
		if(counter.sluttyTattoo || counter.hintedSluttyTattoo){
			sve(`I love the tattoo, by the way!`);
			kat(`Oh! Thanks!`);
			if(counter.hintedSluttyTattoo){
				txt(`I quickly readjusted my clothes to give her the full view.`);
			}
			sve(`It is neat! I like it!`);
			if(mile.girls_slutty_tattoo){
				kat(`Right!?! My friends didn't like it, they thought it is too far!`);
				sve(`Find better friends! I know people with far crazier tattoos!`);
			}
		}
		
		if(!mile.poledancing_boobs && mile.boobs){
			mile.poledancing_boobs = true;
			txt(`Eventually, I got more daring and @sve got tired of watching me doing the basic tricks so we tried experimenting with more advanced stuff. @sve was helping me and showing me how to position my body. She was behind me and suddenly I realized her hands were squeezing my breasts.`);
			sve(`Nice!`);
			if(mile.slut < 11){
				kat(`Excuse me?!`);
			}else{
				kat(`They are, aren't they?`);
			}
			sve(`Your tits are amazing. The sarcophagus, right?`);
			kat(`Yeah.`);
			sve(`I thought so. Some of the girls I know have sarcophagus-enlarged tits too. When I got mine done, it wasn't available yet so they are done in the old-fashioned way.`);
			if(mile.slut < 4 && mile.sub > 10){
				txt(`Being casually touched by her was not making me especially comfortable but I did not stop her.`);
			}else if(mile.slut < 4){
				kat(`Maybe the next time you could ask before groping me?`);	
				sve(`Oh! Sorry! I just wanted to see the comparison. If I wanted to get my tits even bigger, I would definitely try the sarcophagus.`);
			}
		}

		link(`End of the class. `, 103);
		break;
		
			
	case 103: 
		present(
			[`sve`, `sve`], //TODO CHANGE	
		);
		quickLoadOutfit();
		emo(`relax`);
		
		txt(`We quit and went to change. My joints ached like after medieval torture but otherwise, I was in a great mood. When @sve told me she will go downtown to do some shopping, I decided to tag along.`);

		if(mile.slut > 10){
			sve(`Were you just joking when you asked about another gig or are you actually interested?`);
			kat(`Do you have something for me?`);
		}else{
			kat(`I hope you're not going to try to trick me into another stripping gig!`);
			sve(`No, I won't. I mean, unless you want to.`);
			kat(`...you would have something for me?`);
			txt(`I tried to work as a stripper once. It was kinda fun but once was enough for me. But I was curious.`);
		}
		
		sve(`Ehhh, maybe. But it might be a bit beyond your comfort zone. I'm booked for a private party and I can take you with me. But I don't know the guys - even though they are supposed to be fine - and you would have to do stuff with me.`);
		kat(`What stuff?`);
		sve(`Sexual stuff. Nothing serious, but definitely some touching and making out.`);
		kat(`You told me you have a boyfriend!`);
		sve(`I do. I'm straight. But I don't mind being lesbian for money.`);

		if(mile.slut > 16 && mile.lesbian > 2){
			txt(`That sounded pretty exciting. Honestly, I would not mind doing lesbian stuff with @sve even without getting paid for it.`);
		}else if(mile.slut > 8){
			txt(`That sounded pretty interesting. And working together with @sve, what could go wrong?`);
		}else{
			txt(`I hesitated. This was not for me, was it?`);
		}

		
		con_link(mile.slut > 9, `low slut`, `I'm in! `, 110,  ()=> mile.stripshow = 3);
		link(`Yes, I don't mind being a bit lesbian for money either.  `, 110,  ()=> mile.stripshow = 2);
		link(`Okay... but there will be only stripping and maybe making out, right?  `, 110,  ()=> mile.stripshow = 1);
		link(`Sorry, but that isn't for me. `, 110, ()=> mile.stripshow = -1);
		break;

	case 110: 
		if(mile.slut < 10 && mile.stripshow > 0) mile.slut++;
		
		if(mile.stripshow === -1){
			if(mile.slut > 10) mile.slut--;
			txt(`Of course! I understand, I suggested it only because you asked.`);
			
		}else if(mile.stripshow === 1){
			kat(`Nothing else, right? We are not expected to have sex in front of people?`);
			sve(`No! Well, maybe I'll play with some toys but you absolutely don't have to!`);
			kat(`Fine... in that case... I think I will do it.`);
			sve(`Are you sure you want to do it?`);
			kat(`Yes, I am sure.`);
		}else if(mile.stripshow === 2){
			txt(`I winked at her.`);
			sve(`Awesome! And don't worry, it will be fun and you don't have to do everything I will be doing.`);
		
		}else if(mile.stripshow === 3){
			sve(`You slut! You were convinced when I mentioned doing the lesbian stuff with me, weren't you?`);
			txt(`Laughed @sve.`);
		}
		
					
		
		txt(`We were chatting for a while, mostly about dancing and stripping, but eventually parted ways when @sve had to go to meet with her boyfriend.`);

		chapter(`Go shopping. `, `shop_mall`);
		next(`Go home. `);
		break;
	}
}
		




export const stripshow_action = (index)=> { switch(index){
	default:
	case 101:
		present(
			[`sve`, ["animalGString", "sexyCageBra"] ],
		);
		placetime("@sve's place");

//TODO - JOANA - SVETAS ALT
		counter.quip = ``;
		if(mile.dildo >= 6) counter.quip = `(even though none of them was bigger than my crazy ${dildo(mile.dildo)}.`;
		if(mile.dildo >= 4) counter.quip = `(almost the size of my ${dildo(mile.dildo)}.`;
		if(mile.dildo >= 3) counter.quip = `(even bigger than my ${dildo(mile.dildo)}.`;

		txt(`I met with @sve at her place. She was in the middle of preparation, trying to figure out what lingerie she should wear. On the coffee table in the living room was laid out rather impressive collections of sex toys.`);
		kat(`What the hell, @sve, is your boyfriend unable to pleasure you sexually?`);
		sve(`No, they are mostly just for shows.`);
		txt(`I did not want to pry too much - I would be uncomfortable if anybody checked my private toys (even though she admitted hers were not so private) - but I was curious. There were several huge dildos ${counter.quip} but my eyes were especially drawn to an almost industrial-looking vibrator.`);
		sve(`Have you tried the magic wand?`);
		txt(`@sve noticed my interest and she had no restraints.`);
		kat(`No.`);
		sve(`It's awesome!`);
		txt(`She picked the vibrator up and turned it on. It immediately began violently buzzing, more reminding of a drill than a sex toy. @sve pressed the magic wand against my crotch and even through @lower I could feel the powerful vibrations.`);
		kat(`Oh my!`);
		sve(`It's fucking powerful!`);

	//TODO	link(`I have to get one! `, 110);
		link(`Lovely! `, 110);
		link(`Maybe a bit too powerful for me. `, 110);
		break;

	case 110:
		emo(`unco`);
		present(
			[1, `Jožin`],
		);

		createWear("fishnetStockings", undefined, undefined, effigy.sve);  //TODO - TEMP
		createWear("sluttyMinidress", undefined, undefined, effigy.sve); 
		createWear("highHeels", undefined, undefined, effigy.sve); 
		quickTempOutfit([
			"gString", 
			"sexyPushUpBra",
			"fishnetStockings",
			"sluttyMinidress2",
			"highHeels",
		]);

		txt(`She laughed and then found sexy lingerie and outfit even for me. We called a taxi and the self-driving car took us to the very edge of the city. The event was taking place in a large, secluded villa. It was surrounded by a tall concrete wall with barbed wire on top of it. From the outside, the place looked a bit unsettling.`);
		kat(`Do we have the right address?`);
		sve(`Yeah, I think so.`);
		kat(`It looks a bit creepy. Are you sure those guys aren't murderers or something like that?`);
		sve(`Are you asking whether they are murderers in general or whether they are going to kill us?`);
		kat(`Are they going to kill us?`);
		sve(`No!`);
		kat(`Are you hundred percent sure?`);
		sve(`Oh my God, @kat, do you think I would really take a job if there was the slightest chance I might get murdered?`);
		kat(`No.... but you implied they might be murderers?`);
		sve(`I heard they are involved in pretty shady stuff.`);
		kat(`Oh fuck!`);
		sve(`Calm down, I'm just messing with you. They are embezzling money from government contracts, they are not violent thugs and they are not killing random sluts. They know my boss and he told me it's going to be fine.`);
		kat(`I'm not sure if I want to strip in front of criminals.`);
		sve(`Be more open-minded and don't believe every gossip. Maybe you already stripped for criminals?`);
		kat(`@dur isn't a criminal.`);
		sve(`Maybe he's stealing chalk or paper clips? And where do you think @npc_1 gets money to hire strippers?`);
		kat(`He's a thief?`);
		sve(`No, he does something in IT. Come on, @kat! I'm just messing with you again. Calm down, everything will be fine, I guarantee it.`);
		link(`Enter. `, 111);
		break;

	case 111: 
		present(
			[1, `Random Man 1#`],
			[2, `Random Man 2#`],
			[3, `Random Man 3#`],
			[6, `Laura`],
		);

		txt(`The security guard was professional, he was not groping us more than he had to when he was searching us and he did not even smirk when @sve opened her bag and showed him the sex toys inside. We waited with the security guard until the arrival of a guy who took us inside.`);
		txt(`The living room was bigger than our flat. There were hunting trophies everywhere and even one whole stuffed chamois. The room was opened into a terrace and overall there could be like two dozen people. We began to mingle, @sve unabashedly chatted and flirted with guys and I tried too even though I felt a bit awkward. All the men and women were wearing fancy and very expensive clothes while I was just in @sve's trashy, sluty dress. I was so nervous I was glad when it was finally time for our performance.`);
		npc(1, `And now Joana and...`);
		kat(`${mile.poledancing_name}`);
		npc(1, `and her *very good friend* ${mile.poledancing_name} will show us their skills!`);
		txt(`Some people sleazily laughed and we got lukewarm applause. @sve took my hand and together we waltzed into the middle of the room. We began swaying our hips and dancing. I hoped the crowd focused mostly on @sve, she was so much better than me. At least we were blinded by the lights aimed at us, it was easier to forget we were ogled by so many people.`);
		txt(`@sve turned to me, her hand began sensually running over my body. I tried to follow her lead and copy her movements and coyly smiled when she gently took my chin and leaned closer to kiss me.`);
		txt(`The crowd cheered.`);
		txt(`@sve let me go and squatted down. Her dress rolled up, showing her panties when she frantically and very suggestively moved with her pelvis and her blonde hair was wildly flying around her head. When she straightened up, she crossed her arms and in one elegant move she  ${choice("sve_dress", "stripped her dress ")} and threw it away.`);
		txt(`We danced together, @sve crouched again, her hands on my body. As she was rising, her fingers were sliding over my thighs and sides, slowly rolling my dress higher. I lift my arms, allowing her to  ${choice("kat_dress", "undress me")}. I embraced her and we kissed again, this time more violently. When our lips parted, @sve quickly whispered:`);
		sve(`You're doing great!`);
		
		link(`Follow @sve's lead. `, 112, ()=> counter.star = false);
		link(`Get more confident. `, 112, ()=> counter.star = true);

		effects([{
			id: `sve_dress`,
			fce(){
				remove("upper", effigy.sve);
			},
		}]);
		effects([{
			id: `kat_dress`,
			fce(){
				remove("upper", PC);
			},
		}]);
		break;

			
	case 112: 
		emo(`relax`);
		if(counter.star){
			if(mile.sub > 10) mile.sub--;
			txt(`Her words made me cocky. Why she should be the biggest star? I moved forward to show off with my moves and the reaction of the crowd was enthuistiatic. I zealously swayed, suggestively presenting my teen, lean body to their hungry eyes. For a while, @sve let me to enjoy my moment of fame. I barely noticed when she moved closer and suddenly, in swift, offhand move ${choice("kat_bra", "unclasped my bra")}.`);
			
		}else{
			if(mile.sub < 10) mile.sub++;
			txt(`Her words boosted my courage. I was happy she was there with me, her confidence was calming me down and encouraging me. I tried to keep up with her and danced better than ever before. @sve without warning pushed me forward. She stayed behind, for a moment forcing me to be the star. I am proud to say I did not hesitata and zealosly swayed, suggestively presenting my teen, lean body to the hungry crowd. @sve sudenly swiftly waved with her hand and offhandly ${choice("kat_bra", "unclasped my bra")}`);
		}

		txt(`I pretended I was embarrassed, holding it covering my breasts, but then I let it fall down and shamelessly stretched. @sve was behind me, her body rubing aganst mine, her hands caressing my body, expecially my exposed breasts. And that was not all, she half-turned me, bowed down and her tongue touched my ${wears.piercedNipples ? "pierced" : ""} nipples.`);
		txt(`The crowd liked it.`);
		txt(`@sve ${choice("sve_bra", "dropped her bra")} too. She was passionately dancing around me, almost like I was her pole, even lewdly humping me. There was a sofa we could use. @sve crawled on top of it and wiggled with her butt. I understood, slapped her ass and then ${choice("sve_panties", "pulled down her sexy thong")} . She sat down and raised her legs high into air, letting everybody to witness her cleanly-shaved pussy.`);
		txt(`The crowd liked it a lot.`);

		link(`Strip your @panties. `, 113, ()=> counter.keep = false);
		con_link(mile.slut < 16, `too big slut`, `Keep your panties. `, 113, ()=> counter.keep = true);
		
		effects([{
			id: `sve_bra`,
			fce(){
				remove("bra", effigy.sve);
			},
		}]);
		effects([{
			id: `sve_panties`,
			fce(){
				remove("panties", effigy.sve);
			},
		}]);
		effects([{
			id: `kat_bra`,
			fce(){
				remove("bra", PC);
			},
		}]);
		break;
		

	case 113:
		if(counter.keep){
			if(mile.slut > 12) mile.slut--;
			txt(`I was a bit ashamed to completely strip in front of so many strangers. Before the show, @sve suggested I do not have to go fully nude so I decided to keep my @panties. Everybody had eyes only for her anyway. She was completely nude, except for her heels and stockings.`);
		}else{
			if(mile.slut < 14) mile.slut++;
			txt(`I did not want to stay behind and ${choice("kat_panties", "stripped my @panties")}. I stood up, hooked my thumbs under the waistband of my panties and slowly and deliberately pushed them down. We were both completely nude, except for our stockings and heels.`);
		}

		sve(`Ahhh!`);
		txt(`Playfully moaned @sve behind me. She was sitting on the sofa, her legs were shamelessly spread, she was holding a pink dildo and rubbing it against her slit. I climbed onto the sofa next to her, careful to not block the view at her, and she handed me the dildo. I lasciviously licked it and then began stimulating her pussy. It was an awkward situation, I pushed the dildo into her pussy and she seemed delighted but I suspected she was faking it. I am not sure I would be able to enjoy it if I was at her place.`);
		sve(`Oh yeah! Fucke me hard, ${mile.poledancing_name}!`);

		con_link(mile.sub > -8, `too dominant`, `Fuck her gently. `, 114, ()=> mile.stripshow_action = 1);
		con_link(mile.sub < 16, `too submissive`, `Fuck her hard. `, 114, ()=> mile.stripshow_action = 2);
	//TODO - mile.lesbian to 2
		con_link(mile.slut > 12 && mile.lesbian > 1, `low slut/low lesbian`, `Lose all inhibitions. `, 114, ()=> mile.stripshow_action = 3);

		effects([{
			id: `kat_panties`,
			fce(){
				remove("panties", PC);
			},
		}]);
		break;
			

	case 114:
		emo(`horny`);
		if(mile.stripshow_action === 2){
			kat(`Take it, bitch!`);
			txt(`I recklessly laughed, I had such a fun. I rammed the pink dildo into @sve's pussy and the crowd cheered me up. I even leaned closer to take her hard nipple between my teeth and lightly pulled it.`);
		}else if(mile.stripshow_action === 1){
			kat(`You would love that, wouldn't you?`);
			txt(`I laughed and eagerly wiggled with the dildo inside her. She smiled at me and I leaned to kiss her. We were making out while I was fucking her with the dildo and the crowd was cheering us up.`);
		}else if(mile.stripshow_action ===  3){
			mile.slut++;
			txt(`I fucked her with the dildo. I had such fun, I almost forgot we were surrounded by horny strangers. @sve's pussy looked so delicious and I just could not resist. I slipped from the sofa down on the floor and crawled between @sve's legs. I burried my face into her pussy and began eating her out.  We did not plan this, I was just improvising. The crowd went wild and cheered me up.`);
		}
		sve(`OH YEAH! AHHHH!`);
		txt(`@sve cried out. So close to her I was able to tell her orgasm was not real but she was faking it perfectly. When our last song stopped, we were rewarded by an excited applause.`);
			
		if(mile.stripshow_action === 2){
			kat(`Sorry. I got carried away.`);
			sve(`That's fine! You did a great job!`);
		}else if(mile.stripshow_action ===  3){
			kat(`Sorry.`);
			sve(`You're such a nympho! Don't do anything we didn't discuss!`);
			kat(`Sorry. I got carried away.`);
			sve(`That's fine. You did a great job.`);
		}
		txt(`I was a bit ashamed to meet @sve's eyes but she hugged me to ensure me our friendship was not harmed.`);
			
		link(`Continue. `, 120);
		break;

	case 120:
		emo(`unco`);
		txt(`We went to the bathroom to freshen up and then we returned to mingling. The experience was no longer strange, it became utterly bizzare because @sve did not allow me to get dressed. And nobody even pretended to respect us. There were women who were not hiding their disgust and most of the men had sleazy and demeaning comments.`);
		npc(1, `You are a couple of such hot lesbian sluts!`);
		kat(`Ehh.. thanks...`);
		npc(2, `Are you together?`);
		kat(`Yeah, Joana is my gilfriend. I'm living with her and she's training me to be a perfect lesbian slut.`);
		npc(1, `Do you take dicks too or are you on pussy-only diet?`);
		kat(`I'm bisexual, I love all sex and I can't get enough.`);
		npc(2, `Have you tried shooting porn?`);
		kat(`Not yet but becoming a pornstar is my biggest dream.`);
	//TATTO IS HOT?
		npc(1, `Are you just a stripper or do you do escorting too?`);
		kat(`I'm just a stripper.`);
		if(mile.whore){
			txt(`It was another lie but @sve warned me we are not whores.`);
		}else{
			txt(`I blushed when he implyied he was willing to pay to fuck me.`);
		}
		npc(3, `Can I ask for a lapdance?`);
		if(mile.slut < 7){
			txt(`I looked at @sve, hoping she will save me but she just shrugged, leaving it on me.`);
		}else{
			txt(`I glanced at @sve but she just shrugged, leaving it on me.`);
		}
		
		link(`Sure! `, 121);
		link(`Sorry! `, 130);
		break;

	case 121: 
		emo(`focus`);
		mile.stripshow_lapdance = true;
		txt(`We found a private place. The guy comfortably sat down on a sofa and I mounted him. We were face to face, I was sitting astride on top of him.`);
		npc(3, `Come on, bitch! Do your worst!`);
		txt(`I began grinding against him, my pussy brushing against his crotch. He instantly got hard, I could feel his cock under his clothes. The guy was obviously enjoying it, he was grinning and his eyes were on my lightly bouncing tits.`);
		txt(`I was getting annoyed by his face so I turned around and began energetically rubbing my but against his lap.`);
		npc(3, `Oh yeah!`);
		txt(`He was very satisfied, I almost made him cum.`);

		link(`Back to party. `, 130);
		break;

	case 130: 
		emo(`unco`);
		txt(`As I said, most of the women were jealous and did not like to see young attractive sluts around their men. But there were exceptions. When I was returning from the bathrooms I was cornered by an elegantly dressed brunette. She could be in her forties or fifties but she was very good-looking and clearly took good care of her appearance. Her eyes were cold and she radiated a strange aura of authority. She seemed like somebody who is used to people doing exactly what she wants.`);
		npc(6, `Hello, sweetie!`);
		kat(`Eh, hello!`);
		txt(`She blocked my way and was standing uncomfortably close.`);
		npc(6, `Are you really a dyke or are just playing it for the perverts?`);
		if(mile.lesbian === 0){
			kat(`It's just a job.`);
		}else if(mile.lesbian <= 2){
			kat(`I'm mostly straight.`);	
		}else{
			kat(`I actually do like girls.`);
		}
		txt(`I shrugged.`);
		npc(6, `Well, it doesn't really matter.`);
		txt(`She maliciously smirked.`);
		kat(`Do you want to order a stripper?`);
		npc(6, `I want to do far more with you than just watch you undress.`);
		txt(`She casually pinched my left nipple.`);
		if(mile.sub < 10){
			kat(`Hey!`);
		}
		npc(6, `You might not enjoy it, I'm rough when I'm playing with my toys, but I definitely will.`);
		txt(`The brunette was making me kinda uneasy.`);
		npc(6, `What is your name, sweetie?`);
		kat(`${mile.poledancing_name}.`);
		npc(6, `Your real name!`);

		con_link(mile.sub < 12, `too submissive`, `That's none of your business!  `, 131, ()=> mile.laura_name = 1);
		con_link(mile.sub < 22, `too submissive`, `I can't tell you.  `, 131,  ()=> mile.laura_name = 2);
		link(`@katalt. `, 131,  ()=> mile.laura_name = 3);
		break;

	case 131: 
		if(mile.laura_name === 3){
			mile.sub++;
			txt(`She was leaning so close to me that I was feeling her hot breath. Her stare was so stern I was not able to resist.`);
			kat(`<small>@katalt</small>.`);
			txt(`I mumbled.`);
			npc(6, `I did not hear you! Speak clearly!`);
			txt(`She scolded me.`);
			kat(`I'm @katalt.`);
			npc(6, `I'm @npc_6. Nice to meet you, @katalt!`);

		}else if(mile.laura_name === 2){
			txt(`She was leaning so close to me that I was feeling her hot breath. Her stare was so stern, I was almost unable to resist.`);	
			kat(`I... I shouldn't tell you...`);
			txt(`She smirked, enjoying my distress.`);
			npc(6, `Don't worry, sweetie. Keep your secrets. I have my ways to find out.`);

		}else if(mile.laura_name === 1){
			mile.sub--;
			txt(`I tried to brush her off, she was too nosy. I did not expect her reaction: she immediately slapped me.`);
			kat(`AH!`);
			npc(6, `I don't like brats! Always treat your superior with respect!`);
			txt(`She did not raise her voice, it was as calm and cold as before.`);
			if(mile.sub < 0){
				emo(`angry`);
				kat(`I did not answer, I just defiantly looked back at her.`);
			}else{
				emo(`shock`);
				kat(`S... sorry.`);
			}
			txt(`The intimidating brunette shook her head.`);
			npc(6, `Fine, sweetie. Keep your secrets. I have my ways to find out.`);

		}

		sve(`${mile.poledancing_name}?`);
		txt(`@sve arrived in the right moment. `);
		kat(`Joana?`);
		txt(`@npc_6 reluctantly let me go.`);
		npc(6, `We'll continue our conversation later!`);
		txt(`She promised and villainously smiled at me. She gave @sve a nod and left us.`);
		sve(`What's going on?`);
		kat(`Nothing. Who the hell was that?`);
		sve(`@npc_6 Varga. You don't want to get involved with her.`);
		kat(`I really don't! Damn, I'm almost afraid to go back.`);
		sve(`Good news, you don't have to. I got the money and we can leave.`);
		kat(`Awesome!`);
		
		link(`Go home. `, 140);
		break;


	case 140: 
		present(
			[`sve`, `sve`], //TODO CHANGE	
		);
		quickLoadOutfit();
		emo(`relax`);

		{
			let cash = money.stripshow;
			if(mile.stripshow_lapdance) cash += money.stripshow_lapdance;
			payment(cash,"cash");
			txt(`We got dressed and drove back to @sve's place. There she divided the money. Her cut was substantially better but I was amazed when I found out I in total made ${cash} Euro.`);
		}
		next(`Done. `, quickLoadOutfit);
		break;
}}

