/**
	weekend tasks assigned and executed on Friday evening
*/

import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, tom, zan, ven, mik} from "Loop/text";
import {jus} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";
import {placetime,  task_intro,  task_reaction, log_sex, rumor_pussy} from "Loop/text_extra";

import {ra} from "Libraries/random";
import {stringifyHSLA} from "Libraries/color/index";
import {emo, quickTempOutfit, draw, rollUpSkirt, wdesc, removeActual, removeEverything, update, updateDraw, effigy, allClothes, crew, wearRandomClothes, remove, wear, create, wears, showLess, showAll, violates, sluttiness, createWear, quickSaveOutfit, quickLoadOutfit  } from "Avatar/index";
import {active_timeslot, next_event, add_rule} from "Taskmaster/scheduler";
import {DISPLAY} from "Gui/gui";
import {cloneFigurine} from "Avatar/canvas/body";
import {rules_violation} from "Taskmaster/new_rules_check";

import {NAME} from "System/variables";

import {clamp, plural, x_items} from "Libraries/dh";
import {BARS} from "Virtual/index";


import {bills} from "Virtual/text";
import {payment} from "System/bank_account.js";
import {money} from "Data/items/prices.js";





//WRESTLING
export const wrestling_initiation = ()=> {
	set.irl();
	placetime("schoolyard");
	emo("anoy");
	qaa(`Oh, @kat? `);
	kat(`Yeah? `);
	qaa(`We wanted to discuss our weekend plans. `);
	kat(`What weekend plans? `);
	qaa(`Your task for the weekend. `);
	kat(`There'll be no task!`);
	qcc(`You willingly agree to fulfill our tasks every day!`);
	kat(`I agreed to fulfill your task every workday!`);
	txt(`We argued but @qaa eventually decided:`);
	qaa(`We'll meet in the Metaverse this afternoon. We'll come up with a compromise you'll like!`);
	next();
};

export const wrestling_action = (index)=> {
	switch(index){
		default:
		case 101:
			set.meta();
			placetime("Nerd's subreality");
			emo("anoy");
			txt(`I met with them and did not like their compromise. It was just another humiliating game. They offered me a bet - if I win their brand new challenge, I will have a free weekend without any new task.`);
			kat(`What kind of challenge? `);
			qaa(`You can pick what kind of challenge you want!`);
			kat(`Okay...`);
			qcc(`It could an intelligence challenge (trivia quiz)...`);
			kat(`No! I absolutely despise those. `);
			qbb(`That isn't very surprising. `);
			if(mile.sub < 0) kat(`Shut up! `);
			qcc(`... or a physical challenge (fight)...`);
			kat(`I'll take it. `);
			qcc(`... or it could be...`);
			kat(`I want the physical challenge (fight). `);
			qcc(`... but I have six more options... `);
			qbb(`She picked the physical challenge (fight). `);
			qcc(`So I prepared the list for nothing?!?!?`);
			link(`Yes! `, 102);
			break;
  
		case 102:
			emo("suspect");
			if(ext.background === 2){
				txt(`It was a no-brainer. I was pretty sure I was actually stronger and faster than any of them. Unlike theirs, my physical exercises went beyond training my right hand. I exercised every day, often went to a gym, to swim or to run a few kilometers. I trained very hard and was overall in quite good physical condition. I was not a fighter and I was no expert on any martial art but in the past, I messed a little with aikido and fitness boxing. This will be easy, I thought. `);
			}else{
				txt(`I was pretty sure I was actually stronger and faster than any of them. Unlike theirs, my physical exercises went beyond training my right hand. When I was younger I trained gymnastic and I still went to the gym every week and jogged regularly. Or at least I used to before their stupid game swallowed all my free time. `);
			}
			txt(`I maybe even get a chance to do my opponent something especially painful. `);

			qaa(`But if you lose, you'll spend the Saturday as a slave of one of us.`);
			if(mile.a_burned){
				txt(`That poor bastard probably still hoped I will pick him even after everything he said to me! Like I would enjoy being his personal toy without the other two interferings! Overconfident and naive I agreed:`);
			}else{
				txt(`What a sly fox, he probably hoped I will pick him. Being his personal toy without the other two interfering. Overconfident and naive I agreed:`);
			}
			kat(`Sure, whatever! `);
			qaa(`You will fight against our champion. `);
			kat(`WHAT?!? I didn't agree with that! That's unfair! Why I can't have a champion too?`);
			qcc(`You absolutely can. Those are the rules. `);
			qbb(`Should we send a message to somebody? `);
			kat(`...`);
			qbb(`As I thought. `);
			kat(`I won't fight any virtual beast! `);
			qbb(`Hmmm, a best? Why we didn't think about that?`);
			qcc(`It might be really cool...`);
			qaa(`Don't worry. I promise your opponent will be exactly as strong as you. `);
			if(mile.y_deleted === true){
				link(`Okay?`, 103);
			}else{
				link(`Okay... ...wait, you don't mean... `, 104);
			}
			break;
			
		case 103:
			emo("shock");
			ayy(`Hello! `);
			txt(`My blood ran cold when I heard that enthusiastic voice sounding just like I sounded on videos. It was the vile, identity-stealing @ayy! `);
			kat(`What the hell! I thought I killed you, bitch! `);
			ayy(`I'm not so easy to kill, bitch! But I'll make you pay for trying! `);
			kat(`Yeah?! I deleted you once and I can to it again, as many times it takes! `);
			txt(`I boasted but her appearance shook my confidence. `);
//TOOD I REALLY HAD TO FINISH THE GMAE
			qaa(`Calm down, girls. Keep that for the ring. `);
			kat(`How?!?! `);
			
			qaa(`We had to restore her from a backup after you so brutally murdered her! `);
			qcc(`It was an act of wanton savage neoludditism. `);
			qbb(`A manifestation of the worst despicable cyberxenophobia. `);
			if(mile.y_forkill){
				kat(`It isn't even a real person! `);
			}else{
				kat(`She isn't even a real person! `);
			}
			qbb(`That's a horrible thing to say! `);
			//qbb(`Htler said exactly the same thing. `);
			if(mile.sub > 2){
				kat(`But... I'm sorry for deleting her but this not a fair comparison! `);
			}else{
				kat(`Are you crazy?!? And no, I will not fight myself! `);
			}
			qcc(`There is no way to make the match more fair. `);
			kat(`I hate you! `);
			if(mile.a_burned){
				qaa(`Great, we hate you too! `);
			}
			qcc(`Just think about the prize! `);
			qbb(`Or what will happen if you fail! `);
			link(`Uhg! Okay! `, 105);
			link(`I hate you all! `,105);
			break;
			
		case 104: 
			emo("shock");
			ayy(`Hi, me! Nice to see me again! `);
			txt(` I turned around when I heard the voice which sounded exactly like I sounded on videos. It was @ayy! `);
			kat(`No, no, no, no. You can't be fucking serious!?! I'll not fight myself! `);
			qcc(`There is no way to make the match fairer. `);
			if(mile.y_forkill){
				kat(`I should have to kill you! Giving you mercy was a huge mistake!`);
				ayy(`You're so mean!`);
				qcc(`She doesn't mean that, it's just trash talking. `);
				kat(`I absolutely mean that! `);
			}
			kat(`Only you three can come up with something so fucked up. `);
			qbb(`Thank you! `);
			kat(`That was not a compliment! I'm disgusted by you. I mean, even more than usually. `);
			qcc(`Just think about the prize! `);
			qbb(`Or what will happen if you fail! `);
			link(`Uhg, Fine! `,105);
			link(`I hate you all! `,105);
			break;


		case 105: 
			//quickSaveOutfit();
			emo("anoy");
			quickTempOutfit(["wrestlingTop", "wrestlingBot"]);
			present(["ayy",["redWrestlingTop", "redWrestlingBot"]]);
			txt(`I expected something like octagon but what materialized in front of us was a shallow pond filled with viscous pink liquid. `);
			kat(`What is it?!`);
			qaa(`Synthetic mud. `);
			qcc(`Well, technically it is not a mud at all. And it has strawberry scent and flavour. `); 
			ayy(`Really? That is so smart!`);
			kat(`Uhhg! `);
			txt(`The AI took down off her boots and pulled down her skirt. A similar outfit instantly rendered on me, only mine was blue while hers was red. `);
			txt(`Then we both entered the pond. The mud was about ankle deep and quite slippery. `);
			if(mile.y_deleted){
				txt(`My clone sternly stared at me. She wanted her revenge.  `);
			}else{
				txt(`My clone playfully  smirked. That bitch was enjoying this! `);
			}
			
			link(`I will destroy you! `, 106, ()=> counter.temp = 3);
			link(`(Stern, annoyed look) `, 106, ()=> counter.temp = 2);
			link(`This is so stupid! `, 106, ()=> counter.temp = 1);
			break; 
			
			
		case 106: 
			qbb(`Do you want to bet on the victory? `);
			qcc(`Yeah!`);
			qaa(`Sure!`);
			qbb(`Who do you think will win? `);
			if(mile.a_burned){
				qaa(`I think being a stone-cold, shameless, calculating, emotionless bitch may give you an edge in this challenge. So I bet on the real @kat.`);
				if(mile.sub < 0) kat(`Thank you! `);
			}else{
				qaa(`I think I will bet on the real @kat. I trust her and our AI is not perfect. `);
			}
			qbb(`I'll bet on the @ayy. AI's are generally smarter, faster and more capable than humans and I believe it especially applies in the case of @katalt.`);
			qcc(`I don't think the lack of actuall sentience and imperfection of our AI will matter in this short fight. They are basically the same person, there are only a few variables. Like the colors of their outfits. And the research shows the athlete in a red jersey is more likely to win! So I bet on @ayy.`);
			qbb(`Well, that's why we picked those colors. `);
			txt(`I was not sure whether to be more outraged by their subtle attempts to rig the match or that they favorised @ayy over me! But I was even more determined to win! `);
			//link(`Fight. `, 200, initiate_fight);
			chapter(`Fight. `, `fight`);
			if(softbug){
				link(`#Win the fight `, 111);
				link(`#Lose the fight `, 110);				
			}
			break;
			
		case 110: 
			mile.fight = -1;
			DISPLAY.bars_off();
			emo("help");
			
			txt(`I stared in disbelief at @qbb who raised the hand of victorious @ayy! How could I fail so badly? It was not a fair fight, I had to remind myself. `);
			qaa(`So? `);
			kat(`What? `);
			qbb(`About our bet. `);
			qcc(`You willingly promised to spend the Saturday as a servant of one of us. `);
			qaa(`So? Who will you choose? `);
			kat(`I can pick anybody?  `);
			qcc(`Yeah! Obviously one of the present. `);
			kat(`Okay, I choose the @ayy. `);
			qaa(`What!?! You can't do that?! `);
			kat(`Why not? `);
			qcc(`I believe she can do that. `);
			qbb(`Well, we'll have to let her enjoy her submissive lesbian fantasies. `);
			kat(`I don't have any submissive lesbian fantasies! I obviously choosing her just out of spite because I hate you all more than her! `);
			ayy(`Thank you! `);
			
			next(`Weekend. `, ()=> quickLoadOutfit(false) );	
			break;
			
		case 111: 
			mile.fight = 1;
			DISPLAY.bars_off();
			emo("smug");
			
			txt(`Damn! That was a way harder than I thougt! But I eventually triumphed over the soulless @ayy just as I expected. `);
			kat(`That's it. Did you have enough fun, jerks?!!!`);
			qcc(`You were supposed to say: are you not entertained? `);
			kat(`Fuck you. `);
			
			qaa(`Congratulations. A great fight. `);
			qbb(`You could do this for I living. I'm pretty sure the record of your match would be very popular. `);
			
			kat(`If you <i>ever</i> share it anywhere I will find you and break both your arms. `);
			txt(`I was full of adrenaline and I meant every word. `);
			
			next(`Weekend. `, ()=> quickLoadOutfit(false) );	
			
			break;

		case 112: 
		//TODO LEGACY
		/*
			bold(`I am sorry, this is everything for now :( `);
			bold(`But there will be more soon! `);
		*/	
			next(`Weekend. `);	
			break;
			
			
			
			
	}		
};



export const fight = (index)=> {
const a = [];
let out;

	function attack(){
		alt_link(`Charge! `, 211, `-2 Stamina`, undefined, undefined, counter.stam > 1);
		alt_link(`Attack. `, 212, `-1 Stamina`, undefined, undefined, counter.stam > 0);
		alt_link(`Wait. `, 214, `+1 Stamina`);
	}
	
	
	
	function standing(){
		emo("imp");
		txt(`I was standing above my opponed helplessly crawling in the mud. `);
		alt_link(`Rest. `, 220, `+1 Stamina`, ()=> counter.action = 3);
		alt_link(`Taunt her. `, 220, `+1 Stamina`, ()=> counter.action = 1);
		alt_link(`Be concerned. `, 220, `+1 Stamina`, ()=> counter.action = 2);
		alt_link(`Attack. `, 221, `-1 Stamina`, undefined, undefined, counter.stam > 0);
		//alt_link(`Wait `, 214, `+1 Stamina`);
	}
	
	function ayy_standing(){
		emo("help"); //emo??
		txt(`I was on all fours, coughing up mud while the @ayy was standing above me. `);
		alt_link(`Crawl from her reach. `, 230, `+1 Stamina`, ()=> counter.action = 1);
		alt_link(`Try to stand up.  `, 230, `-1 Stamina`, ()=> counter.action = 2, undefined, counter.stam > 0);
		alt_link(`Attack. `, 230, `-1 Stamina`, ()=> counter.action = 3, undefined, counter.stam > 0);
		//alt_link(`Reach for her panties `, 215, `-1 Stamina`);
	}
	
	
	
	
		
	function grapple(status){
		const a = [];
		if(status === true){
			counter.pin++;
			a.push(`I was clutching her limbs in a way she was totally helpless! `);
			a.push(`I got lucky and pressed the @ayy against the ground! `);
			a.push(`We were fiercly struggling but I managed to pin that cyberslut down! `);
			txt(ra.array(a));
			qbb(["","ONE!","TWO!!","THREE!!!"][counter.pin]);
			
			if(counter.pin === 3){
				emo("smug");
				link(`I won! `, 301);
			}else{
				emo("focus");
				alt_link(`Hold her`, 250, ``, ()=> counter.action = 1);
				alt_link(`Hold her tightly`, 250, `- 1 Stamina`, ()=> counter.action = 2, undefined, counter.stam > 0);
			}
		
		}else if(status === false){
			counter.pin++;
			a.push(`The damn @ayy was tightly holding me, pinning me to the ground! `);
			a.push(`The @ayy was on top of me and I was not able to get away! `);
			a.push(`After a brief struggle the @ayy pressed me to the ground. `);
			txt(ra.array(a));
			qbb(["","ONE!","TWO!!","THREE!!!"][counter.pin]);
			
			if(counter.pin === 3){
				emo("angry");
				link(`I lost! `, 300);
			}else{
				emo("focus"); //emo?? pain
				alt_link(`Do not resist. `, 260, `+ 1 Stamina`, ()=> counter.action = 1);
				alt_link(`Escape. `, 260, `- 3 Stamina`, ()=> counter.action = 2, undefined, counter.stam > 2);
			}
	
		}else{
			emo("focus");
			counter.pin = 0;
			a.push(`We were rolling through mud, viciously struggling. `);
			a.push(`Our bodies were pressed agains each other, limbs tightly intertwined, we both tried to get the upper hand. `);
			a.push(`I tried to grab the bitch but it was not easy, she was constantly slippinga away, her body was covered by the slithery pink mud. `);
			txt(ra.array(a));
			
			alt_link(`Fight feebly. `, 240, ``, ()=> counter.action = 1);
			alt_link(`Fight fiercely. `, 240, `- 1 Stamina`, ()=> counter.action = 2, undefined, counter.stam > 0);
			alt_link(`Fight desperately. `, 240, `- 2 Stamina`, ()=> counter.action = 3, undefined, counter.stam > 1);
			//if(counter.round > 1) link(`Fight dirty. `);
		}
		

	}
	

			
	

	
	
	function ayy_attack(){
		if(counter.ayaya === 1 && counter.ayy_stam < 2) counter.ayaya = 0;
		if(counter.ayaya === 2 && counter.ayy_stam < 1) counter.ayaya = 0;
		
		let out;
		if(counter.ayaya){
			out = counter.ayaya;
		}else{
			if(counter.ayy_stam === 0){
				out = [3];
			}else if(counter.ayy_stam === 1){
				out = [3,3,3,3,3,2];
			}else if(counter.ayy_stam === 2){
				out = [3,3,3,3,3,3,2,2,2,1];
			}else if(counter.ayy_stam < counter.ayy_stam / 2){
				out = [3,3,2,1];
			}else{
				out = [3,2,1];
			}
			out = ra.array(out);
		}
		
		counter.ayaya = 0;
		return out;
	}
	
	function stamina(value){
		//BARS.change("stam", value);
		counter.stam += value;
		counter.stam = clamp(counter.stam , 0, counter.max_stam);
	}
	
	function ayy_stamina(value){	
		counter.ayy_stam += value;
		//BARS.change("ayy_stam", value);
		counter.ayy_stam = clamp(counter.ayy_stam, 0, counter.max_stam);
	}
	
	function score(){
		/*
		bold(`Points: ${counter.points}`);
		bold(`Stamina: ${counter.stam}`);
		bold(`AI points: ${counter.ayy_points}`);
		bold(`AI stamina: ${counter.ayy_stam}`);
		*/
	}
	
	function both_tired(){
		const limit = 6;
		return counter.ayy_stam < limit && counter.stam < limit;
	}
	
	function initiate_fight(){
		
		counter = {};
		counter.points = 0;
		counter.ayy_points = 0;
		counter.round = 0;
		
		counter.await = 0;
		
		counter.taunt = 0;
		counter.trick = 0;
		counter.ayaya = 0;
		
		counter.max_stam = 10;
		counter.ayy_stam = counter.max_stam;
		counter.stam = counter.max_stam;
		counter.pin = 0;
		
		counter.dirty = false;
		counter.beg = false;
		counter.taunt = false;
		
		
		DISPLAY.bars_on(["fight_stam","fight_ayy_stam"]);
		/*
		BARS.create([{
			id: "stam",
			label:	"Stamina",
			value:	counter.stam,
			max:	counter.max_stam,
			color:	80,
		},{
			id: "ayy_stam",
			label:	"AI stamina",
			value:	counter.ayy_stam,
			max:	counter.max_stam,
			color:	140,
		}]);
		*/
		
	}
	
	
	
	switch(index){
		default:
		case 101:	
			initiate_fight();
			//break; //SIC
      
		case 200:
		 
			counter.round++;
			stamina(3);
			ayy_stamina(3);
			counter.pin = 0;
			if(counter.round === 1){
				txt(`We were sizing each other up, carefully moving in the slipery pink ooze. `);
			}else{
				txt(`Covered from heads to toes by the pink goo, we again stood against each other. `);
			}
			attack();
			if(mile.sub > 0 && /*points.ayy*/counter.ayy_points  === 2 && !counter.beg) link(`Beg for mercy. `, 201);
			if(counter.round > 1 && !counter.taunt) link(`Taunt her. `, 202);
			score();
			break;
			
			
		case 201:
			mile.sub++;
			counter.beg = true;
			emo("shy");
			txt(`It was extremely humiliating but I was getting really desperate. I leaned closer to my clone: `);
			kat(`Please! I don't want to spent Satuday as a slave of one of them! But you have nothing to lose! `);
			if(mile.y_deleted){
				txt(`She mischievously smirked: `);
				ayy(`Well, too bad! `);
			}else{
				txt(`She gave me a sad smile: `);
				ayy(`I'm sorry! I like you but I can't throw away a fight! That wouldn't be fair! `);
			}
			attack();
			score();
			break;
			
		
		case 202:
			counter.taunt = true;
			emo("imp");
			//txt(`It was extremely humiliating but I was getting really desperate. I leaned closer to my clone: `);
			kat(`Come on you stupid useless program! I'll fucking destroy you for your audacity to impersonate me! Dumb cow! `);
			txt(`She seemd to be pretty angry and annoyed. `);
			counter.ayaya = 1;
			attack();
			score();
			break;
			
		//CHARGE	
		case 211: 
			stamina(-2);
			counter.await = 0;
			switch(ayy_attack()){
				default: 
					txt(`ERROR`);
					grapple(); 
					break;
				case 1:
					ayy_stamina(-2);
					//BOTH HARD DOWN
					a.push(`I charged against the @ayy and she charged against me. We both crashed and fell down to the mud, splashing it everywhere. `);
					txt(ra.array(a));
					grapple(); //TODO SHOCK
					break;
				case 2:
					//ON TOP
					ayy_stamina(-1);
					txt(ra.array([
						`The @ayy tried to tackle me but her attack was too hesitant and weak. I managed to violently push her down into the mud. `,
						//TODO
					]));
					grapple(true);
					break;
				case 3:
					txt(ra.array([
						`I heedlessly attacked her but the damn @ayy anticipated my charge. She moved away and exploited my forward momentum to send me head fist to the mud. `,
						`I tried to charge but she dodged me. I slipped and haplessly fell down right on my butt, splashing the pink mud everywhere. The nerds bursted out laughing. `,
					]));
					ayy_standing();
					break;	
			}
			score();
			break;
			
			
		//ATTACK	
		case 212:
			stamina(-1);
			counter.await = 0;
			switch(ayy_attack()){
				default: 
					txt(`ERROR`);
					grapple(); 
					break;
				case 1:
					ayy_stamina(-2);
					a.push(`I carefully moved forward, trying not to slip. The @ayy surprised me when she aggressively charged against me and quickly gained the upper hand and forced me into the mud. `);
					txt(ra.array(a));
					grapple(false);
					break;
				case 2:
					ayy_stamina(-1);
					txt(`We were pushing against each other, our arms locked, our feet slipping on the slippery pink goo. Then we both lost our ballance and crashed down. `);
					grapple();	
					break;
					
				case 3:
					if(counter.ayy_stam <= counter.max_stam / 2){
						txt(`The exhausted @ayy was trying to catch her breath but I did not gave her any chance, attacked and pushed her down into the pink mud. `);
					}else{
						txt(`The @ayy briefly hesitated and I exploited the opportunity and pushed her down into the pink mud. `);
					}
					grapple(true);	
					break;	
			}
			score();
			break;
			
		//WAIT
		case 214:
			stamina(1);
			counter.await++;
			
			switch(ayy_attack()){
				default: 
					txt(`ERROR`);
					standing();
					break;
				case 1:
					//STANDING
					ayy_stamina(-2);
					a.push(`@ayy furiously charged agaist me. I easily eveded her, stepping aside, while she slipped and fell down. `);
					txt(ra.array(a));
					standing();
					break;
				case 2:
					//ON DOWN
					ayy_stamina(-1);
					txt(ra.array([
						`The @ayy carefully attacked, tackled me and toppled me over. I could feel my legs slipping and then I fell backwards into the mud. `,
						`The @ayy carefully moved forward and grabbed me. For a moment we were struggling and then she moved her leg behind mine and tripped me over. `,
					]));
					grapple(false);
					break;
				case 3:
					ayy_stamina(1);
					if(counter.await > 3){
						if(ra.b){
							qbb(`Come on, girls! You're boring us! `);
							qcc(`Go, @kat! I mean, not you, the other @kat! `);
						}else{
							qbb(`FIGHT! `);
							qaa(`We wanted to see some action! `);
						}
						if(ra.b){
							txt(`Impatient nerds were yelling at us. `);
						}else{
							txt(`Excited nerds wanted us to hurt each other. `);
						}
						counter.ayaya = ra.integer(1, 2);
					}
					if(both_tired()) a.push(`We were both waiting, flushed, trying to catch our breath before continuing with the fight. `);
					a.push(`We were slowly circling each other, neither one of was willing to risk and attack first. `);
					txt(ra.array(a));
					attack();
					break;	
			}
			
			score();
			break;
			
			
			
//STANDING 
		case 230:
			{
				let out;
				if(counter.ayy_stam === 0){
					out = 1;
				}else if(counter.ayy_stam === counter.max_stam){
					out = 2;
				}else{
					const a = [1,2];
					if(counter.ayy_stam <= 2) a.push(1);
					out = ra.array(a);
				}
				if(out === 1){
					ayy_stamina(1);
					switch(counter.action){
						default: 
							txt(`ERROR`);
							standing();
							break;
						case 1: 
							stamina(1);
							if(ra.b){
								ayy(`You're so pathetic! `);
								txt(`Loudly lauged the @ayy above me, mocking and ridiculing me. `);
							}else{
								txt(`I slithered away but she didn't try to catch me, she was just heavily breathing, taking her time. `);
							}
							ayy_standing();
							break;
							
						case 2: 
							stamina(-1);
							ayy(`You still don't have enough?  `);
							txt(`She laughed at me when I was slowly getting back on my feet. `);
							attack();
							break;
							
						case 3: 
							stamina(-1);
							ayy(`That should show you, you bi- AHH! `);
							txt(`She gasped when I suddenly grabbed her and pulled her down. `);
							grapple();
							break;
					}	
				}else{
					ayy_stamina(-1);
					switch(counter.action){
						default: 
							txt(`ERROR`);
							standing();
							break;
						case 1: 
							stamina(1);
							txt(`I tried to move away but @ayy grabbed me by my ankle and demeaningly dragged me back. `);
							grapple(false);
							break;
							
						case 2: 
							stamina(-1);
							ayy(`Not so fast, slut! `);
							txt(`I tried to get back on me feet but giggling @ayy pushed me back down. `);
							grapple(false);
							break;
							
						case 3: 
							stamina(-1);
							txt(`The @ayy tried to exploit her height advantage but I was ready for her. `);
							grapple();
							break;
					}
				}
			}
			score();
			break;
			 
			
			
			
		case 220:{
				stamina(1);
				switch(counter.action){
					case 1: 
						emo("smug");
						kat(ra.array([
							`Do you have enough, bitch!?!`,
							`You have no chance, you stupid AI slut! `,
						]));
						txt(`I loudly laughed, mocking my foe. The nerds cheered and clapped. `);
						break;
					case 2: 
						emo("oops");
						kat(`Are you okay? `);
						txt(`The way she fell down looked painful. `);
						kat(`I'm fine! `);
						txt(`She snapped. `);
						break;
					default:
					case 3: 
						emo("focus");
						txt(`I was breathing, getting ready for the next phase of the fight. `);
						break;
				}
				
				let out = 0;
				if(counter.ayy_stam === 0){
					out = 1;
				}else{
					const b = [1,2,3];
					if(counter.ayy_stam <= 2) b.push(1);
					if(counter.dirty && PC.panties) b.push(4);
					out = ra.array(b);
				}
			
				switch(out){
					default: 
						txt(`ERROR`);
						standing();
						break;
					case 1: 
						ayy_stamina(1);
						txt(`Pathetic AI was crawling away, trying to stay out of my reach. `);
						standing();
						break;
						
					case 2: 
						ayy_stamina(-1);
						switch(counter.action){
							default: 
								txt(`ERROR`);
								attack();
								break;
							case 1: 
								counter.ayaya = 1;
								emo("smug");
								txt(`The furious @ayy slowly stood up, wiping the mud from her face. `);
								ayy(`I'll destroy you, bitch! `);
								attack();
								break;
							case 2: 
								counter.ayaya = 3;
								emo("imp");
								txt(`The @ayy slowly stood up, wiping the mud from her face. `);
								ayy(`I'm ready for more!  `);
								txt(`She claimed.  `);
								attack();
								break;
							case 3: 
								emo("focus");
								txt(`The angry @ayy slowly stood up, wiping the mud from her face. `);
								attack();
								break;
						}
						break;
			  
					case 3: 
						ayy_stamina(-1);
						emo("shock");
						txt(`I thought the @ayy was stunned and unable to fight. Her movement was slow and tired. Then she briskly reached for my leg, grabbed my calf and pulled me down. That bitch tricked me! `);
						grapple();
						break;
						
					case 4:
						ayy_stamina(-1);
						emo("shock");
						txt(`The @ayy crawled closer to me. I was ready to defend myself but I was shocked when she shamelessly reached for my panties. Her fingers grabbed the waistband and tried to pull them down. `);
						alt_link(`Protect your panties. `, 222);
						alt_link(`Attack her. `, 222);
						break;
					
					
				}
				score();
			}
			break;
		
		case 221: {
			stamina(-1);
				let out = 0;
				if(counter.ayy_stam === 0){
					out = 1;
				}else{
					const b = [1,2,3];
					if(counter.ayy_stam <= 2) b.push(1);
					out = ra.array(b);
				}
				
				switch(out){
					default: 
						txt(`ERROR`);
						grapple();
						break;
					case 1: 
						ayy_stamina(1);
						txt(`The @ayy was trying to desperately crawl away from my reach. But she was not fast enough and I swiftly grabbed her. `);
						grapple(true);
						break;
					
					case 2: 
						ayy_stamina(-1);
						txt(`The @ayy trying to stand up but I did not allow it and pushed her back down into the mud. `);
						grapple(true);
						break;
					
					case 3: 
						ayy_stamina(-1);
						txt(`I tried to push her down but she was ready for my attack. `);
						grapple();
						break;
				}
			}
			break;
			
		case 222:
			score();
			if(counter.action === 1){
				txt(`I firmly clenched my panties, not allowing that brazen slut to expose me in front of those perverst! My decency was saved but she expoited my distraction and toppled me down. `);
				grapple(false);
			}else{
				txt(`I attacked her, harshly pushed her down to the mud. Unfortunatelly it was too late for my panties, she managed to pull them down and exposed me in front of those three perverts!`);
				remove("panties");
				grapple(true);
			}
			score();
			break;
		
			
			
			
			
//GRAPPLING
	//HOLDING
	case 250: {
				let out;
				
				if(counter.ayy_stam === counter.max_stam){
					out = [2];
				}else if(counter.ayy_stam < 3){
					out = [1];
				}else if(counter.ayy_stam < counter.ayy_stam / 2){
					out = [1,1,1,2];
				}else{
					out = [1,2];
				}
				out = ra.array(out);
		
				if(counter.action === 1){
					if(out === 1){
						ayy_stamina(1);
						txt(`I was sitting on top of the subdued the @ayy, enjoying her helpless wriggling under me. `);
						grapple(true);
					}else{
						ayy_stamina(-3);
						txt(`I was sure by my victory but the @ayy suprised me. Her lean body slippery with the pink mud broke loose out of my firm embrace. `);
						grapple();
					}
				}else if(counter.action === 2){
					stamina(-1);
					if(out === 1){
						ayy_stamina(1);
						txt(`I clenched the helpless @ayy a bit more firmly, it would be shame if she slipped out. `);
						grapple(true);
					}else{
						ayy_stamina(-3);
						ayy(`Ahhhh! `);
						txt(`The @ayy was violently tossing and wriggling but despited that she was not able to escape. `);
						grapple(true);
					}
				}
				score();
			}
			break;
			
			
	//ESCAPING
	case 260: {
			let out = [];
			if(counter.ayy_stam === counter.max_stam){
				out = [2];
			}else if(counter.ayy_stam === 0){
				out = [1];
			}else if(counter.ayy_stam === 1){
				out = [1,1,2];
			}else if(counter.ayy_stam > counter.ayy_stam / 2){
				out = [1,2,2];
			}else{
				out = [1,2];
			}
			out = ra.array(out);
	
			if(counter.action === 1){
				stamina(1);
				if(out === 1){
					txt(`I was just breathing, immobilized and helpless. `);
				}else{
					ayy_stamina(-1);
					txt(`The @ayy was painfully tigthly holding me, making sure I will not even think about more fighting. `);
				}
				grapple(false);
			}else if(counter.action === 2){
				stamina(-3);
				if(out === 1){
					txt(`The @ayy naively thought the fight was over but I luckily managed to wrestle my way out of her embrace. `);
					grapple();
				}else{
					ayy_stamina(-1);
					kat(`Ahhhh! `);
					txt(`The @ayy applied a bit more force on my joints, punishing my escape attempt. `);
					grapple(false);
				}
			}
			score();
		}
		break;
	
	
	
	
	//STRUGGLE
	case 240: {
			
			let out = [];
			if(counter.ayy_stam === counter.max_stam){
				out = [2,3];
			}else if(counter.ayy_stam === 0){
				out = [1];
			}else if(counter.ayy_stam === 1){
				out = [1,1,1,2];
			}else if(counter.ayy_stam === 2){
				out = [1,1,1,1,1,2,2,2,3];
			}else if(counter.ayy_stam < counter.ayy_stam / 2){
				out = [1,1,2,2,3];
			}else{
				out = [1,2,2,3,3];
			}
			out = ra.array(out);
			
			if(counter.action === 1){
				switch(out){
					default: 
						txt(`ERROR`);
						grapple();
						break;
					case 1:
						txt(`We were both lightly fighting, trying to not tire ourselves too soon. `);
						grapple();
						break;
					case 2: 
						ayy_stamina(-1);
						txt(`Maybe I went too easy on her. She brutally attacked and tightly clenched me. `);
						grapple(false);
						break;
					case 3:
						ayy_stamina(-2);
						txt(`Maybe I went too easy on her. She brutally attacked and tightly clenched me. `);
						grapple(false);
						break;
				}
			}else if(counter.action === 2){
				stamina(-1);
				switch(out){
					default: 
						txt(`ERROR`);
						grapple();
						break;
					case 1:
						txt(`The defence of the @ayy was too weak and I was able to subdue her.  `);
						grapple(true);
						break;
					case 2: 
						ayy_stamina(-1);
						txt(`We were furously struggling but our strength and dexterity were matched and neither of us was clearly better. `);
						grapple();
						break;
					case 3:
						ayy_stamina(-2);
						txt(`I was valiantly fighting but the sheer ferocity of my opponent suprised me and she subdued me. `);
						grapple(false);
						break;
				}
			}else{
				stamina(-2);
				switch(out){
					default: 
						txt(`ERROR`);
						grapple();
						break;
					case 1:
						ayy(`Aaaaahhh! `);
						txt(`Squealed the @ayy surprised by my brutal attack before I pushed her face into the mud. `);
						grapple(true);
						break;
					case 2: 
						ayy_stamina(-1);
						txt(`The @ayy was strong and quick but I was stronger and quicker. `);
						grapple(true);
						break;
					case 3:
						ayy_stamina(-2);	
						kat(`Fuck! `);
						ayy(`Ahhh! `);
						txt(`We aggressively struggled. We both desperately craved to win and we were holding nothing back.  `);
						grapple();
						break;
				}

			}
			score();
		}
		break;			
			
			
			
	//VICTORY
	case 301: 
			counter.points++;
			score();
			emo("smug");
			if(counter.points === 3){
				qcc(`@katalt has 3 poinst which means that she won! `);
				chapter(`Victory! `, `wrestling_action`, 111, DISPLAY.bars_off);
			}else if(counter.ayy_points === 2 && counter.round === 2){
				qcc(`The @kat won two rounds!  `);
				kat(`So I won? `);
				qbb(`No, there are five rounds! `);
				link(`Fine! `, 302);
				chapter(`Hell no! `, `wrestling_action`, 111, DISPLAY.bars_off);
			}else if(counter.points === counter.ayy_points){
				qcc(`@kat ${x_items(counter.points, "point")}, the other @kat ${x_items(counter.ayy_points, "point")}! `);
				qaa(`Now that's interesting! `);
				if(counter.round === 4){
					qbb(`The next round will decide! `);
					qcc(`What a drama! `);
				}
				link(`Next round. `, 200);
			}else{
				qcc(`@katalt won this round! She has ${x_items(counter.points, "point")}!`);
				link(`Next round. `, 200);
			}
			break;
			
	case 300:
			counter.ayy_points++;
			score();
			emo("angry");
			if(counter.ayy_points === 3){
				qcc(`The @ayy has 3 poinst which means that @katalt lost! `);
				chapter(`Defeat! `, `wrestling_action`, 110, DISPLAY.bars_off);
			}else if(counter.round === 1){
				qaa(`What the hell, @kat! I bet on you! Do something! `);
				kat(`Fuck! `);
				txt(`It was just the first round but it definitely was not a good start. `);
				link(`Next round. `, 200);
			}else if(counter.ayy_points === 2 && counter.round === 2){
				qcc(`The @ayy won two rounds!  `);
				link(`Three of five? `, 302);
				chapter(`Defeat! `, `wrestling_action`, 110, DISPLAY.bars_off);
			}else if(counter.points === counter.ayy_points){
				qcc(`@kat ${x_items(counter.points, "point")}, the other @kat ${x_items(counter.ayy_points, "point")}! `);
				qaa(`Now that's interesting! `);
				if(counter.round === 4){
					qbb(`The next round will decide! `);
					qcc(`What a drama! `);
				}
				link(`Next round. `, 200);
			}else{
				qcc(`@ayy won this round! She has ${x_items(counter.ayy_points, "point")}! `);
				link(`Next round. `, 200);
			}
			break;
			
	case 302:
			kat(`Ehh.... three of five? `);
			txt(`I tried to convinece them. Fortunately, they were enjoying the spectacle and did not want it to be over. `);
			qaa(`Sure! Five rounds! `);
			link(`Next round. `, 200);
			break;	
			
	}
};






//GAME NIGHT 
export const game_night_initiation = (index)=> {
	switch(index){
		default:
		case 101: 
			set.irl();
			placetime("hallway");
			emo("anoy");
			txt(`@qaa mischievously smiled at me: `);
			qaa(`Are you excited about your weekend task? `);
			kat(`Not really? `);
			qaa(`It will be a fun one! My parents are out tonight...`);
			kat(`I don't like where this is going! `);
			qaa(`...don't worry! We're just having a game night `);
			qcc(`And you're invited!`);
			qbb(`And participation isn't voluntary! `);
			kat(`I'm already playing one videogame, you won't make me play more! `);
			qcc(`We're going to play board games. `);
			qbb(`And you're not there to play, you're here to provide service!`);
			kat(`Service? `);
			qaa(`As our maid!`);
			kat(`I won't be your maid! `);
			qbb(`It's our task. Or would you prefer to quit? `);

			link(`No, of course not! `, 103);
			link(`...no. `, 103);
			if(!mile.blackmail) link(`Yeah! `,102);
			break;

	
		case 102: 
			insert("blackmail", 101, undefined, "game_night_initiation", 103);
			break;
			
		case 103:
			emo("anoy");
			qbb(`We're happy you're so resonable!`);
			qaa(`Half past seven, my place!`);
			next();
			break;

	}
};




export const game_night_action = (index)=> {
	let mono = `Monopoly`;
	let drink = `coke`;
	let ludo = `Ludo`;
	if(SETTING.local === `czech` || SETTING.local === `slovak`){
		mono = `Races and Bets`;
		drink = `Kofola`;
		ludo = `Man, Don't Get Angry!`;
	}else if(SETTING.local === `polish` || SETTING.local === `hungarian`){	
		drink = `Kofola`;
		ludo = `Don't Get Angry!`;
	}
			
	const cagame = `Mariage`; 
	/*	if(SETTING.local === `czech` || SETTING.local === `slovak`){
			cagame = `Maryáš`;
		}else if(SETTING.local === `hungarian`){	
			cagame = `Ulti`;
		}else if(SETTING.local === `polish`){
		}
	 */		
			
	switch(index){
		default:
		case 101: 
				set.irl();
				placetime("@qaa's place");
				quickSaveOutfit();
				remove("shoes", PC, PC.inventory); //TODO
				emo("anoy");
				
				

/*
	},{
		id: `bb`,
		label: `humiliatingly beg @qbb for his help`,
		fce(){
			mile.advice_1 = 2;
		},
	},{
		id: `cc`,
		label: `ask @qcc for his advice`,
		fce(){
			mile.advice_1 = 3;
		},
}]);				
*/
//crew("maidUpper", "maidLower", "maidHad", "maidApron", "openFishnetPantyhose");


			txt(`It was almost seven. I sighed, another Friday evening completely wasted. I went upstairs, @qaa was already awaiting me. `);
			if(mile.a_burned){
				qaa(`Evening, slut! `);
				txt(`He smiled. `);
				kat(`Hey!`);
			}else{
				qaa(`Good evening, @kat!`);
				kat(`Hey! `);
			}
			qaa(`Don't be so gloomy! Maids are supposed to be more upbeat! `);
			kat(`Whatever. Where's everybody? `);
			qaa(`I told you to arrive early so you could prepare everything. `);
			kat(`Like what?`);
			if(mile.a_burned){
				qaa(`Like, prepare some refreshment. I don't expect you'll be able to cook anything, but we can order something, like pizza. Just putting it on plates shouldn't be beyond our abilities.`);
				kat(`Excuse me?!? Do you think I'm not able to cook something as simple as pizza? `);
				qaa(`Yes. `);
				kat(`I fucking am!`);
				qaa(`I don't believe you!`);
				kat(`I don't care. `);
				qaa(`Prove it! I want to see you try and embarrass yourself!`);
				
			}else{
				qaa(`Like, prepare some refreshment. I don't want to embarrass you, you don't have to cook anything, we can order something, like pizza. But you could put it nicely on plates or something like that. `);
				kat(`Excuse me? What do you mean by embarassing me!?!`);
				qaa(`...by making you cook?`);
				kat(`Do you think I'm not able to cook something as stupid as pizza? `);
				qaa(`Yes! ...no? Are you? `);
				kat(`Fuck you!`);
				qaa(`Sorry! I thought you're too pretty to be an able cook!`);
				txt(`That softened my anger.`);
				qaa(`So... will you cook one?`);
			}
			txt(`I wanted to send him to hell, I was not his servant! But my desire to blow his mind and show off was stronger. `);
			kat(`Fine! I'll need a flour, oil, yeasts. An then some topping, tomatoes, onions, cheese, salami, ham, maybe mushrooms, corn and tomato sauce.`);
			qaa(`I'm not sure whether we have tomato sauce, can't we use ketchup? `);
			link(`No! `, 102);
			link(`Absolutely no! `, 102);
			break;

		case 102:
			emo("focus");
			txt(`We moved to the kitchen and he was bringing me what I demanded.`);
			/*
				todo more water? 
				one teaspoon
				2 teaspoon
				1 teaspoon
			*/
			kat(`Get me a big bowl, half a kilo of flour, 300 ml of tepid water, two tablespoons of olive oil, sugar, salt and yeasts. You just mix it all, wait an hour or so for the dough to rise, then make two pizzas, put on top, put it in an oven and it's done!`);
			qaa(`For how long? `);
			kat(`Until it's done! `);
			qaa(`Until it's done?!! `);
			kat(`Like ten or fifteen minutes. Look at the damned oven! Do you see the glass door?! You can look through it and see whether it's done or not! `);
			qaa(`That doesn't sound very scientific. `);
			kat(`It's baking, not rocket surgery!`);
			qaa(`But I read on Reddit that baking is science and you can't just wing it. `);
			kat(`That's the dumbest thing I ever heard! If you know what are you doing you can absolutely just wing it!`);
			txt(`The only thing we were not able to locate were yeasts. `);
			kat(`Well, you'll have to go to buy some!`);
			qaa(`Can't you make a pizza without them? `);
			kat(`No!`);
			if(mile.a_burned){
				qaa(`Don't you have any at your place? `);
				kat(`No!`);
				qaa(`Are you sure!`);
				kat(`Yes!`);
			}
			qaa(`The shop will be closing soon. `);
			kat(`Then you better run! Go! And don't forget to buy olives too! `);
			qaa(`What olives?`);
			link(`Black! `, 103, ()=> mile.olives = true);
			link(`Green! `, 103, ()=> mile.olives = false);
			break;
		
		case 103:
			emo("imp");
			txt(`I was sitting on a couch watching TV when he flushed returned. `);
			qaa(`Here are your yeasts! `);
			kat(`Thanks! However, I won't need them, I remembered I have a pack at home. The dough is done. `);
			if(mile.a_burned){
				qaa(`You told me you didn't have any!`);
				kat(`Sorry, I wasn't sure so I went to check them and I found a pack. `);
				qaa(`You told me you were sure!`);
				kat(`Well, I guess I made a small mistake!`);
				qaa(`You bitch, you made me pointlessly walk all the way to the shop and back!`);
			}else{
				qaa(`You couldn't remember sooner? You made me walk all the way to the shop and back completely pointlessly!`);
			}
			kat(`Not pointlessly - I hope you bought those ${mile.olives ? "black" : "green"} olives. They are absolutely essential!`);
			qaa(`And what are you doing now!? Slacking around?? `);
			kat(`I'm working! I'm waiting for the dough to rise! You can't speed that up! Well, I guess by putting the dough in a warm place you can. `);
			txt(`The rest of the crew arrived before he could begin ordering me around. `);
			qaa(`Hello!`);
			qcc(`Hi!`);
			qbb(`Hey! `);
			kat(`Hello! `);
			txt(`@qbb and @qcc put down their bags. @qbb took out a huge box, @qcc a soft package. `);
			kat(`What is that? `);
			qcc(`Your uniform!`);
			kat(`No way!`);
			txt(`It was a maid uniform! I could not believe their audacity! Like serving them was not enough, they wanted me to prance around in these fetish clothes!?!`);
			qaa(`Come on, @kat!`);
			qcc(`You'll look so sexy!`);
			qbb(`You staying in your ordinary clothes will ruin the whole experience!`);

			link(`Agree reluctantly. `, 110);
			con_link(mile.slut > 1, `low slut`, `Agree slightly intrigued.`, 111);
			con_link(mile.sub < 4, `submissive`, `Refuse resolutely. `, 112);
			break;

		case 110:
			mile.sub++;
			mile.slut--;
			mile.game_night_maid = 1;
			emo("anoy");
			
			kat(`No!`);
			txt(`I tried to resist but they were not stopping. `);
			qaa(`Come on, @kat!`);
			txt(`I was not able to withstand their coercion for very long. `);
			kat(`Fine! `);
			txt(`I finally capitulated and went to the bathroom to get dressed. `);
			link(`Get dressed. `, 115);
			break;
			
		case 111:
			mile.slut++;
			mile.game_night_maid = 2;
			emo("anoy");
			
			kat(`No! `);
			txt(`I refused but I have to admit I was slightly intrigued. I definitely wondered how I would look dressed like a sexy maid. `);
			kat(`Come on, @kat!`);
			txt(`I resisted for a bit longer, to not make them think I was a pervert who enjoyed dressing up, and then eventually surrendered. `);
			kat(`Fine!`);
			txt(`I went to the bathroom to get dressed. `);
			link(`Get dressed. `, 115);
			break;
			
		case 112:
			mile.sub--;
			mile.game_night_maid = -1;
			emo("anoy");
			
			kat(`No!`);
			qbb(`Come on, @kat!`);
			kat(`I said no! You'll never see me wearing this! `);
			txt(`They tried to convince me but they failed. `);
			qaa(`Fine! We let you wear your ordinary clothes. This time. 	`);
			link(`This time?!? `, 120);
			break;
			
		case 115:
			txt(`I locked the door, ${choice("ss")} and then checked what I was even supposed to wear. The most scandalous piece were fishnet ${choice("pp")}. The short black ${choice("ll")} and thin ${choice("uu")} felt almost normal, they were not as frilly as I feared. When I was dressed there were still two white pieces remaining. It was a maid headgear and ${choice("bb")}. I sighed and put them on too. `);
			
			if(mile.game_night_maid === 2){
				txt(`I checked my reflection in the mirror. Well, I did look like a sexy maid! The uniform was not too trashy, just the right amount! `);
			}else{
				txt(`I checked my reflection in the mirror. This was so demeaning! `); // Fortunately the uniform was not too trashy.
			}
			link(`Leave the bathroom. `, 120);

			effects([{
					id: `ss`,
					label: `stripped`,
					fce(){
						remove("upper");
						remove("upperOuter");
						remove("lower");
						remove("socks");
						remove("belt");
					},
			}]);

			effects([{
					id: `pp`,
					label: `pantyhose`,
					fce(){
						createWear("openFishnetPantyhose", PC);
						
					},
			}]);

			effects([{
					id: `pp`,
					label: `pantyhose`,
					fce(){
						createWear("openFishnetPantyhose", PC);
					},
			}]);	

			effects([{
					id: `uu`,
					label: `blouse`,
					fce(){
						createWear("maidUpper", PC);
					},
			}]);	

			effects([{
					id: `ll`,
					label: `skirt`,
					fce(){
						createWear("maidLower", PC);
					},
			}]);	


			effects([{
					id: `ll`,
					label: `skirt`,
					fce(){
						createWear("maidLower", PC);
					},
			}]);	

			effects([{
					id: `bb`,
					label: `apron`,
					fce(){
						createWear("maidApron", PC);
						createWear("maidHat", PC);
					},
			}]);
			break;
				

		case 120:
		
/*
TODO
if(mile.game_night_maid === 2){
				txt(`I left the batroom and made an ironic curtsey. All the eyes were pinned on me.  `);
				kat(`Masters? `);
				qaa(`Come on, @kat!`);
				qbb(`Come on, @kat!`);
				qbb(`Come on, @kat!`);
			}else{
	*/
			
			

			kat(`What are you going to play? ${mono}? Catan? ${ludo}?`);
			txt(`The box seemed to be huge. @qcc smugly smiled. `);
			qcc(`No! We're going to play Starship-of-the-line Tactical Simulator! `);
			qbb(`It's the most accurate simulation of the space combat ever. `);
			qaa(`The game has amazingly complex rules for movement and battle damage!`);
			kat(`That sounds pretty complicated. `);
			qbb(`Yeah, it is! `);
			txt(`Announced @qbb pretty proudly like that was a good thing. `);
			qbb(`Hey maid, don't just stay around! Bring us something to drink! `);//lelkovat
			if(mile.sub > 2){
				kat(`Yes, sir!`);
				txt(`I answered like a good maid and went to the kitchen for ${drink} they ordered.  `);
			}else{
				kat(`Fine!`);
				txt(`I groaned and went to the kitchen for Kofola they ordered.  `);
			}
			if(!mile.a_burned){
				qaa(`Thank you, @kat!`);
			}else{
				qcc(`Thanks!`);
			}
			qbb(`You don't have to thank her!`);
			aly.acf(`Why not?`);
			qbb(`Imagine you're a high feudal lord and she's just a lowly useless wench. You wouldn't bother thanking her! She should be very grateful that you let her work for you instead of sending her straight into your sex dungeon.  `);
			if(mile.a_hate < 0){ //TODO???
				qaa(`That's a good point. I'll try to be more arrogant. Please, forget I thanked you, @kat!`);
				kat(`I won't!`);
				qaa(`Nevermind, I won't thank you for the next time!`);
			}else{
				qcc(`That's a good point. I'll try to be more arrogant. Please, forget I thanked you, @kat!`);
				kat(`I won't!`);
				qcc(`Nevermind, I won't thank you for the next time!`);
			}
			if(mile.sub < -1){
				kat(`<small>I already know into which drink I spit the next time. </small>`);
				txt(`I mumbled quietly. `);
				qbb(`Were you saying something? `);
			}
			kat(`Are you going to drink just cola the whole evening? This is the lamest party ever!`);
			qcc(`This isn't a party at all! We're here to play the game, not to have fun! `);
			qbb(`We gathered to puruse higher intelectual goals, not to get wasted. `);
			kat(`Why not do both, like ancient Greeks? `);
			qaa(`Well, there's wine in the cabinet, if you...`);
			kat(`Awesome! Where's the corkscrew? `);
			txt(`I did not intend to stay sober, the evening would be unbearable. `);
			link(`Continue. `, 121);
			break;

		case 121:
			txt(`They discussed what scenario to play for like a half of an hour. Then finally decided on commerce raiding and began setting the board. `);
			txt(`I went to the kitchen to finish the pizzas. I turned on the oven and then I split the dough into two parts and made two, mostly circular pizzas. `);
			kat(`Hey, @qaa! Where do you have spices!?! I need oregano and basil! `);
			txt(`I screamed at him.`);
			qaa(`Somewhere in the kitchen! Don't bother me now, maid! I'm getting attacked by two royal light cruisers and civilian lives are at stake! `);
			txt(`He screamed back. `);
			txt(`I put on the first pizza the tomato sauce, spices, salami, bacon, tomatoes, onion, corn, black olives and cheese. `);
			txt(`While was the first pizza baking, I prepared the second one, then switched them and brought the baked one to the living room. I was pretty proud but those twits were not paying any attention, they were focusing on the game, screaming random things about vampires and armor levels. The pizza was getting cold and it was fucking infuriating. `);
			qaa(`NOOOO!`);
			txt(`Cried @qaa suddenly. `);
			kat(`What happened!?!`);
			qaa(`My Fusion Two just exploded!`);
			kat(`Is that a bad thing? `);
			txt(`He was staring at me like I was retarded but at least they started eating and could be impressed by my prowess. `);
			txt(`After the meal they began discussing the next scenarion and I opened the second wine bottle. `);
			link(`Can I play too? `, 122);
			break;


		case 122:
			txt(`It seemed slightly less boring and annoying than just to stand around and let them command me around. `);
			qaa(`You're here to serve us! `);
			qbb(`Yeah, we don't play with maids!`);
			qaa(`Moreover, the game is too complicated for you. `);
			qcc(`Yeah, it's pretty complex. `);
			kat(`Are you saying I'm too dumb to understand it? `);
			if(mile.a_burned){
				qaa(`Yes.`);
				kat(`I'm not!`);
				qbb(`Well, let her try! I want to get a good laugh!`);
				qaa(`She'll only waste our time! `);
				qcc(`It's true, she probably wouldn't have a chance.`);
			}else{
				qaa(`No! I'm not saying you're dumb! Just that the game is very complicated. `);
				kat(`I'm not dumb! `);
				qbb(`Well, let her try! I want to get a good laugh!`);
				kat(`I'm a pretty fast learner!`);
				qcc(`I'm not sure if her, as a beginner, would be able to keep up!`);
			}
			
			link(`Maybe you can pair me with the best of you to give him handicap? `, 123);
			//TODO
			break;
			
		case 123:
			
			txt(`When their very delightfully entertaining heated argument about who is the best player finally inconclusive ended, they tried to explain to me the rules. `);
			kat(`So, the smaller ships have a higher speed.`);
			qaa(`NO!`);
			kat(`But you just said...`);
			qaa(`No! They have the same type of particle shields so their maximal speed is the same! They have higher acceleration!`);
			qcc(`The inertial compensators are exponentially less effective with the increasing tonnage of the ship.`);
			qaa(`The ships have acceleration several hundreds of G's so you should better pray to not get your compensators damaged while you're accelerating. Your whole crew will be turned into jello. `);
			qbb(`Basically smaller ships are maneuvering faster. `);
			kat(`Okay. `);
			qcc(`That was the movement. Now sensors. There are three types of sensors, optical, thermal and electromagnetic. In addition, electromagnetic could be divided into active and passive ones. `);
			kat(`This is too complicated! My head hurts!`);
			qbb(`I've told you the game's too complicated for her!`);
			kat(`Can't we play cards or something like that? `);
			qcc(`NO! We're playing Starship-of-the-line Tactical Simulator!`);
			qaa(`Yeah!`);
			qbb(`Yeah! Unless the loser would have to strip. `);
			qaa(`Wait, that sounds interesting! Yeah, we should do that! `);
			qcc(`But this night we wanted to play Starship-of-the-line Tactical Simulator?`);
			qbb(`You heard @kat. She prefers to get stripped while playing cards. `);
			kat(`I never said that! `);
			qcc(`...we can play strip Starship-of-the-line Tactical Simulator!`);
			kat(`I don't want to strip!`);
			qaa(`Then why did you suggested it? `);
			//qcc(`It might be fun!`);
			qbb(`Are you scared you might lose? `);
			kat(`Fine! But when I win I won't have to be your maid anymore!`);
			txt(`I did most of the drinking and overestimated my luck. 			`);
			qbb(`So what should we play? ${cagame}? `);
			kat(`I don't know how to play ${cagame}! What about Raining? But I never said I want to st-`);
			qcc(`Raining is for kids!`);
			txt(`I knew ${cagame} but it was a skill-based game and I needed something simpler. Establishing I was stupid and unable to understand more complex rules definitely helped to convince them.`);
 
			counter.round = 0;
			counter.players = [true,true,true,true]; //TODO
			link(`Slow game `, 151);
			link(`Quick game `, 131);
			
			break;



		case 151:
			txt(`
				<strong>Raining</strong><br>
				<ul>
				<li>the goal is get rid of all cards 
				<li>the card could be played only if the suit or value are the same (e.g. on ♣8 could be played either ♥8 or ♣9)
				<li>if the player has no playable card, she draws a new one from the deck
				<li>Ace forces the next player skip the round, this could be evaded by playing another Ace
				<li>7 forces the next player draw two cards, this could be evaded by playing another 7, the third player then has to draw 4 cards, etc. 
				<li>Queen could be played on any card except active 7 or Ace and its player may then change a suit
				</ul>
			`);
			
			txt(`
				<strong>Slow game</strong><br>
				<ul>
				<li>when the first player gets rid of al the cards, everybody with remaining cards removes one piece of clothes
				</ul>
			`);
			
			
			console.log(counter.players);
			counter.round++;
			
			
		
			link(`Play `, 152, ()=> {}, 
				()=> DISPLAY.minigame({
					game: "rain",
					victory: (result = 1) => { //TODO!!!! ASAP
						counter.result = result;	
						main(160);	
					}, 
					defeat: (result) => {
						counter.result = result;	
						main(160);	
					}, 
					people: counter.players.map(a=>a),
					setting: {
						only_winer: true,
					},
					no_links: true,
				}),
			);
			
			/*
			rain( ()=> {
					counter.result = rain_result();	
					main(160);								
				}, ()=> {
					counter.result = rain_result();
					main(160);				
				} , "div_text", counter.players.map(a=>a), {only_winer: true} ),
			);
			*/
			break;
			
		case 152:
			if(softbug) {link(`#Win.`, 160, () => {
				counter.result = [0,2,2,2];
			});}
			if(softbug) {link(`#Lose.`, 160, () => {
				counter.result = [2,0,0,0];
			});}
			break;
			
			
		case 160:
				counter.kate = counter.result[0] ? 1 : 0;
				counter.remove = undefined;
				console.log(counter.result);
				console.warn(counter.players);
				if(!counter.kate){
					txt(`I ended the round with no remaining cards! `);
				}else{
					txt(`I had ${counter.kate} remaining cards. `);
				}
				
				
				["","qaa","qbb","qcc"].forEach( (a,i) => {
					if(i === 0) return; //kate
					if(!counter.players[i]) return; //no longer in game
					if(!counter.result[i]){
						txt(`@${a} ended the round with no remaining cards! `);
					}else{
						let text =`@${a} had ${counter.result[i]} remaining cards. `;
						const clothes = allClothes.filter(b => effigy[a][b]);
						const log = [];
						//for(let k = counter.result[i]; k > 0 && clothes.length > 1; k--){
							let what;
							if(clothes > 2){
								const i = ra.integer(0,1);
								what = clothes[i];
								clothes.splice(i, 1);
							}else{
								what = clothes.shift();
							}
							log.push(effigy[a][what].name);
							removeActual(what, effigy[a]);
						//}
						text += `He ${ra.array(["stripped","removed"])} his ${and(log)}. `;
						
						if(clothes.length <= 1){
							text += `Wearing only his ${effigy[a].panties.name}, he felt too embarassed to continue. `;
							counter.players[i] = false;
						}
						txt(text);
						update(effigy[a]); //TODO
					}
				});
				
//console.warn(counter.players);

				if(counter.round === 1 && (counter.result[1] + counter.result[2] + counter.result[3])){
					qcc(`W.. wait... we're supposed to undress too!?!`);
					kat(`Oh yeah! `);
					txt(`I loudly laughed! `);
					qaa(`Yeah, those are rules. `);
					qbb(`Or are you too bashful for this?`);
					qcc(`No! `);
				
				}
	
				if(!counter.kate){
					console.log(counter.players.filter(a => a).length);
					if(counter.players.filter(a => a).length === 1){
						link(`Victory `, 201);
					}else{
						link(`Next round. `, 151);
					}
				}else{
					txt(`I had ${counter.kate} remaining cards. `);
					link(`Strip. `, 162);
				}
				
				break;
				
		case 162:
				
				if(counter.remove){
					const slut = sluttiness().details;
					if(counter.remove === "bra" && PC.upper){
						txt(`I just shrugged  and reached under my ${wdesc.upper} to remove my bra. `);
					}else if(counter.remove === "bra" && slut.bra){
						txt(`I hesitated. Was this a good idea? But then I just sighted and unhooked my bra and exposed my tits to their lust-filled eyes. `);
					}else if(counter.remove === "upper" && !PC.bra && !PC.upperOuter){
						txt(`I hesitated. Was this a good idea? But then I just sighted and stripped my ${wdesc.upper} and exposed my tits to their lust-filled eyes. `);
					}else if(counter.remove === "panties" && wears.skirt){
						txt(`All eyes were on me when I reached under my ${wdesc.lower} and put on the pile of discarded clothes my ${wdesc.panties}. `);
					}else if(counter.remove === "panties" && slut.panties){
						txt(`All eyes were on me when I pulled down my ${wdesc.panties} and put them on the pile of discarded clothes. It was embarrassing but I was so drunk I did not care. Only important thing was to win and I needed one more round! `);
					}else{
						txt(`I stripped my ${PC[counter.remove].name}`);
					}
					counter.kate--;
					remove(counter.remove);
				}
				
				if(counter.kate){
					allClothes.filter(a => PC[a]).forEach( a => {
						if(a === "bra" && mile.slut < -1){ //TODO SLUT
//TODO							
						}else if(a === "panties" && mile.slut < 1){ //TODO SLUT
//TODO							
						}else{
							link(`Strip ${PC[a].name} `, 162, ()=> counter.remove = a);
						}
					});
					link(`Give up. `, 203);
				}else if(counter.players.filter(a => a).length === 1){
						link(`Draw. `, 202);
				}else{
					link(`Next round. `, 151);
				}
				break;





				
		case 131:
			txt(`
				<strong>Raining</strong><br>
				<ul>
				<li>the goal is get rid of all cards 
				<li>the card could be played only if the suit or value are the same (e.g. on ♣8 could be played either ♥8 or ♣9)
				<li>if the player has no playable card, she draws a new one from the deck
				<li>Ace forces the next player skip the round, this could be evaded by playing another Ace
				<li>7 forces the next player draw two cards, this could be evaded by playing another 7, the third player then has to draw 4 cards, etc. 
				<li>Queen could be played on any card except active 7 or Ace and its player may then change a suit
				</ul>
			`);
			
			txt(`
				<strong>Quick game</strong><br>
				<ul>
				<li>when the first player gets rid of al the cards, you have to remove as many pieces of clothes as you have remaining cards in your hand
				</ul>
			`);
			
			
			console.log(counter.players);
			counter.round++;
			
			link(`Play `, 132, ()=> {}, 
				()=> DISPLAY.minigame({
					game: "rain",
					victory: (result = 1) => { //TODO!!!! ASAP
						counter.result = result;	
						main(140);	
					}, 
					defeat: (result) => {
						counter.result = result;	
						main(140);	
					}, 
					people: counter.players.map(a=>a),
					setting: {
						only_winer: true,
					},
					no_links: true,
				}),
			);
			
			/*
			link(`Play `, 132, ()=> {}, 
				()=> rain( ()=> {
					counter.result = rain_result();	
					main(140);								
				}, ()=> {
					counter.result = rain_result();
					main(140);				
				} , "div_text", counter.players.map(a=>a), {only_winer: true} ),
			);
			*/
			break;
			
		case 132:
			if(softbug) {link(`#Win.`, 140, () => {
				counter.result = [0,2,2,2];
			});}
			if(softbug) {link(`#Lose.`, 140, () => {
				counter.result = [2,0,0,0];
			});}
			break;
			
			
		case 140:
				counter.kate = counter.result[0];
				counter.remove = undefined;
				console.log(counter.result);
				console.warn(counter.players);
				if(!counter.kate){
					txt(`I ended the round with no remaining cards! `);
				}else{
					txt(`I had ${counter.kate} remaining cards. `);
				}
				
				
				["","qaa","qbb","qcc"].forEach( (a,i) => {
					if(i === 0) return; //kate
					if(!counter.players[i]) return; //no longer in game
					if(!counter.result[i]){
						txt(`@${a} ended the round with no remaining cards! `);
					}else{
						let text =`@${a} had ${counter.result[i]} remaining cards. `;
						const clothes = allClothes.filter(b => effigy[a][b]);
						const log = [];
						for(let k = counter.result[i]; k > 0 && clothes.length > 1; k--){
							const what = clothes.shift();
							log.push(effigy[a][what].name);
							removeActual(what, effigy[a]);
						}
						text += `He ${ra.array(["stripped","removed"])} his ${and(log)}. `;  
						
						if(clothes.length <= 1){
							text += `Wearing only his ${effigy[a].panties.name}, he felt too embarassed to continue. `;
							counter.players[i] = false;
						}
						txt(text);
						update(effigy[a]); //TODO
					}
				});
				
//console.warn(counter.players);

				if(counter.round === 1 && (counter.result[1] + counter.result[2] + counter.result[3])){
					qcc(`W.. wait... we're supposed to undress too!?!`);
					kat(`Oh yeah! `);
					txt(`I loudly laughed! `);
					qaa(`Yeah, those are rules. `);
					qbb(`Or are you too bashful for this?`);
					qcc(`No! `);
				
				}
	
				if(!counter.kate){
					console.log(counter.players.filter(a => a).length);
					if(counter.players.filter(a => a).length === 1){
						link(`Victory `, 201);
					}else{
						link(`Next round. `, 131);
					}
				}else{
					txt(`I had ${counter.kate} remaining cards. `);
					link(`Strip. `, 142);
				}
				
				break;
				
		case 142:
				
				if(counter.remove){
					const slut = sluttiness().details;
					if(counter.remove === "bra" && PC.upper){
						txt(`I just shrugged  and reached under my ${wdesc.upper} to remove my bra. `);
					}else if(counter.remove === "bra" && slut.bra){
						txt(`I hesitated. Was this a good idea? But then I just sighted and unhooked my bra and exposed my tits to their lust-filled eyes. `);
					}else if(counter.remove === "upper" && !PC.bra && !PC.upperOuter){
						txt(`I hesitated. Was this a good idea? But then I just sighted and stripped my ${wdesc.upper} and exposed my tits to their lust-filled eyes. `);
					}else if(counter.remove === "panties" && wears.skirt){
						txt(`All eyes were on me when I reached under my ${wdesc.lower} and put on the pile of discarded clothes my ${wdesc.panties}. `);
					}else if(counter.remove === "panties" && slut.panties){
						txt(`All eyes were on me when I pulled down my ${wdesc.panties} and put them on the pile of discarded clothes. It was embarrassing but I was so drunk I did not care. Only important thing was to win and I needed one more round! `);
					}else{
						txt(`I stripped my ${PC[counter.remove].name}`);
					}
					counter.kate--;
					remove(counter.remove);
				}
				
				if(counter.kate){
					allClothes.filter(a => PC[a]).forEach( a => {
						if(a === "bra" && mile.slut < -1){ //TODO SLUT
//TODO							
						}else if(a === "panties" && mile.slut < 1){ //TODO SLUT
//TODO							
						}else{
							link(`Strip ${PC[a].name} `, 142, ()=> counter.remove = a);
						}
					});
					link(`Give up. `, 203);
				}else if(counter.players.filter(a => a).length === 1){
						link(`Draw. `, 202);
				}else{
					link(`Next round. `, 131);
				}
				break;
			/*
			quickLoadOutfit(true);
			
			
			*/
			
		case 201: 
			{
				const details = sluttiness().details;
				
				if(details.topless || details.bottomless){
					mile.slut++;
					
					txt(`I was almost completely nude, ${!details.bottomless ? "my tits were exposed" : ""}${!details.topless ? "my pussy was exposed" : ""} but I won! `);
					if(mile.slut > 5){
						txt(`Despite the lack of clothes, I was not ashamed at slightest. I felt only pride and triumph!`);
					}else{
						txt(`I was blushing but the pride I felt was stronger than the shame! `);
					}
					
					kat(`Wow, pretty pathetic, guys! `);
					txt(`I loudly laughed. The nerds were only in their underwear and seemed very embarrassed. `);
					
				}else if(details.panties || details.bra){
					const a = [];
					if(details.panties) a.push("panties");
					if(details.bra) a.push("bra");

					if(mile.slut > 2){
						txt(`I lost many pieces of my clothes - those horny perverts could stare at my exposed ${and(a)} - but I did not mind! I crushed them!`);
					}else{
						mile.slut++;
						txt(`I lost many pieces of my clothes - I felt embarrassed when those horny perverts stared at my exposed ${and(a)} - but  I crushed them!`);
					}
					
					kat(`Wow, pretty pathetic, guys! `);
					txt(`I loudly laughed. The nerds were only in their underwear and seemed very embarrassed. `);
					
				
				}else{
					kat(`Pretty pathetic! `);
					txt(`I loudly laughed. The nerds were only in their underwear and seemed very embarrassed. On the other hand, I felt great! I defeated them all! `);
					
				}
				kat(`Find somebody else to work a your stupid maid! `);
				txt(`I got dressed and left them. `);
				next(`Go home. `, ()=> quickLoadOutfit() );
			}
			break;
		
		case 202: 
				txt(`TODO Draw`);
				//SIC break;
			
		case 203: 
			{
				const details = sluttiness().details;
				if(!allClothes.filter(a => PC[a]).length){
					mile.slut++;
					if(mile.slut > 5){
						txt(`I felt so crushed. I did what I could but I lost all my clothes and those perverted bastards could freely stare at my exposed nude body. `);
					}else{
						txt(`I felt so crushed and humiliated. I tried my best, vainly hoping I might win. But I lost and they took all my clothes, I was blushing, vainly trying to cover my tits and crotch with arms. `);
					}
				}else if(details.topless || details.bottomless){
					mile.slut++;

					if(mile.slut > 3){
						txt(`I was crushed when I was forced to give up, I could not continue anymore. I was almost completely nude, ${!details.bottomless ? "my tits were exposed" : ""}${!details.topless ? "my pussy was exposed" : ""}.`);
					}else{
						txt(`I was crushed by the defeat and horribly ashamed. I was too embarrassed to strip further, I was almost completely nude, feebly ${!details.bottomless ? "covering my tits" : ""}${!details.topless ? "covering my crotch" : ""}.`);
					}
					
				}else if(details.panties || details.bra){
					const a = [];
					if(details.panties) a.push("panties");
					if(details.bra) a.push("bra");
					
					mile.slut--; //TODO
					
					txt(`I decided to play it save and quit. I already showed them way too much skin - the perverts could freely admire my exposed ${and(a)} - and I was afraid I might lose the next round too. `);
					
				}else{
					txt(`Maybe I could lose a few more garments without embarassing myself too much but I decided to quit. The game was obviously rigged against me and it was just a matter of time before they would strip me completely. `);
					
				}
				
				qbb(`It seems like you lost, @katalt!`);
				kat(`Well, yeah. `);
				qaa(`You'll have to stay our maid for the rest of the evening!`);
				qcc(`Please, just focus on the serving and don't bother us too much! We have to focus on the tactical simulator!`);
				kat(`Fine!`);
				txt(`I reached for my clothes but @${emy.abcf()} stopped me. `);
				emy.abcf(`You lost them and you don't need them anymore! You'll stay as you are!`);
				kat(`You can't be serious! Fuck!`);
				next(`Weekend. `, ()=> quickLoadOutfit() );
			}	
			break;
	}
};		
			
		












//KISSING GIRLS 
export const kissing_girls_initiation = (index)=> {
	//task_intro(); 
	placetime("schoolyard");
	qaa(`We have a weekend task for you.`);
	kat(`Forget about the weekend task! I don't have time for that, @mik invited us to hang out in a bar.`);
	qaa(`We know you're going. And we crafted the new weekend task exactly for this situation!`);
	kat(`Okay?`);
	qaa(`We want you to kiss a girl.`);
	kat(`Like you want me to make out with the AI?`);
	qcc(`...that's a brilliant idea!`);
	qaa(`No, we meant a real girl.`);
	qbb(`We want to see two girls kissing.`);
	qaa(`Do you think you can convince another girl to kiss you?`);
	kat(`Sure, absolutely.`);
	qaa(`In the case you're so sure, let's make it even more challenging - you have to kiss three girls.`);
	kat(`Three!?! Come on!`);
	qaa(`That's the deal. Don't worry, you're a very charming and there will be plenty of girls`);
	qbb(`And we need to witness every kiss.`);
	qcc(`So you have to arrange our invitation.`);
	kat(`You're aware that the bar is a public place and @mik said that everybody from the class is welcomed?`);
	qaa(`Yeah, but did he mean us too?`);
	kat(`Oh my! If you're not sure if you're invited, you just go. People won't kick you out!`);
	qcc(`People don't kick you out because you're cool and popular.`);
	kat(`I'm cool and popular because I'm not overthinking this dumb stuff. But fine, I can arrange you won't get kicked out.`);
	next();
	
}

function kiss_crash(){
	emo("shock");//emo?? oops
	mile.kiss_crash = true;
	mile.stat--;
	const details = sluttiness().details.titfall;
	let exposed = ``;
	const heels = (()=>{
		if(wears.highHeels){
			return `I regreted I wore such an absurdly high heels and I blame them for what happened next.`;
		}else if(wears.heels){
			return `I blame my high heels for what happened next.`;
		}
		return ``;
	})();
	
	txt(`I did too many shot, I was beyond feeling pleasantly warm and confident, I reached the stage when I was loud and my movement lacked finer coordination. ${heels}`);
	txt(`I tripped and fell down. I reflexivelly tried to grab a table that was nearby to stop the fall. I was not very successful. I crashed into the table, scattering empty and half-empty glasses. Moreover, I painfully hit my elbow and still slipped down and collapsed on the floor.`);
	txt(`My spectacular mishap and the sound of broken glass attracted attention of everybody in the closes vincity, friends and random strangers alike. I tried to collect myself but it was not easy, I felt dizzy, my arm hurt and to add insult to injury, spilled beer, juice, coke and whatever else were dribbling down from the edge of the table right down onto me.`);
	npc(1, `What a klutzy bimbo!`);
	if(details.titfall && details.nipslip){
		exposed = `breasts`;
		txt(`They were laughing and only now I realised my panic (and eventually vain) flailing around was too much for my @upper which was not able hold my tits contained and now they were exposed to the whole word.`);
	}else if(wears.skirt){
		rollUpSkirt(2);
		exposed = `panties`;
		txt(`They were laughing and only now I realised my disgacefull fall rolled up my skirt and ${wears.panties ? "flashed to everybody my @panties" : "showed everybody I was wearing no panties"}.`);
		if(!wears.panties){
			exposed = `@kitty`;
			mile.stat--;
			/*
			mile.rumor_beaver = 4;
			mile.rumor_commando++;
			*/
			rumor_pussy(4);
		}
	}else{
		//TODO 
		txt(`#TODO clothes malfunction wearing shorts.`);
	}

	if(mile.slut < 5){
		txt(`I blushed, overwhelmed by panic and hastily tried to fix my clothes. Two or three flashes of light affirmed I was not quick enough and my humiliation was forever immortalized. This was an fucking nightmare, everybody near could see my ${exposed}! And who was not near will see the photos!`);
	}else if(mile.slut < 15){
		txt(`I clumsily tried to fix my clothes. Two or three flashes of light affirmed I was not quick enough and my humiliation was forever immortalized. That was very bad, the pictures of me drunk and disgracefully lying exposed on the floor will be soon shared by everybody!`);
	}else{
		txt(`I tried to sit up and evade the dribbling the liquid. Then I fixed my clothes. The vultures very much enjoyed the sight of my exposed ${exposed} and three flashes of light affirmed my disastrous, disgracefull accident was immortalized for everybody.`);
	}
	sas(`@kat? Are you okay?`);

	if(mile.eva_exploit > 1){
		txt(`Worried @sas. @eva put her phone away and rushed to help me too. Together they lifted me back on my feet.`);
	}else{
		txt(`@sas and @eva rushed to help me. Together they lifted me back on my feet.`);
	}
	eva(`Maybe you should stop with drinking!`);
	txt(`@eva warned me and examined me. There were shards of broken glass on the table and on the floor but fortunately I was not bleeding. The girls took me at the bathrooms where we tried to fix my appearance and clean my stained clothes.`);
	
	mile.kiss_crash_exposed = exposed;
	
	link(`Back to the bar `, 200, showAll);
}


export const kissing_girls_action = (index)=> {
	switch(index){	
		case 101:
			set.irl();
			placetime(`at home`);
			txt(`I took shower, shaved my legs${wears.hairyPussy ? " and armpits" : ", pubic region and armpits"} and painted my nails. I hoped the stupid task to kiss three girls will not ruin my Friday evening too much.`);
			link(`Get dressed.`, 102);
			break;
			
		case 102:	
			DISPLAY.dress_up(103);
			break;
	
		case 103:
			emo("happy");
			mile.drunk = 0; //kiss task drunkeness counter
			mile.qcc_drunk = false; //qcc got drunk 
			mile.qcc_sas = 0; // 1 - qcc & sas interacted during kiss task
			mile.sas_kiss = 0; //kissing sas
			mile.tom_zan_kiss = 0; //tom x zan
			mile.eva_kiss = 0; //eva's refusal
			mile.qaa_kiss_angry = false; //angry argument with qaa
			mile.jus_kiss = false; //kissed justine
			mile.kiss_crash = false; //embarrassed herself
			
			counter.kisses = 0;
			
			counter.generous = 0;
			counter.money = 0;
			
			counter.eva_drunk = 0;
			counter.sas_drunk = 0;
			counter.zan_drunk = 0;
			counter.qaa_drunk = 0;
			counter.qbb_drunk = 0;
			counter.qcc_drunk = 0;
			
			counter.eva_sas = 0; //chat
			counter.eva_ven = 0; //chat
			
			counter.sas_off = false; //sas already asked for kiss
			counter.sas_off = false; //sas disable temporaly
			
			counter.force = false; //force whoring without whorish clothes in hardbug mode
			
			placetime(`Requiem`);
			
			present(
				[1, `Random Stranger. `], 
				[2, `Some Guy. `], 
			);
			
			
			txt(`*Requiem* was a stylish bar consisting from three maze-like underground floors. I descended down to the lowest level where @mik's friends and people from our class were already drinking.`);
			mik(`Hey, @kat!`);
			kat(`Hello!`);
			sas(`Hey!`);
			mik(`Good thing you're here! We need your opinion! What is the most important quality of your potential boyfriend? Sense of humor or being handsome? Sense of humor is winning so far 2 to 1 even though some vain people - I don't want to name them but it is @eva-`);
			eva(`Hey, fuck you, you hypocrite! Don't tell me you would date an ugly girl! And I said both things are important!`);
			mik(`- prefer superficial good look. What is the most important for you?`);
			if(mile.slut > 7){
				kat(`Of course his dick size! Duh!`);
			}else{
				kat(`Of course if he has money! Duh!`);
			}
			txt(`I answered without hesitation and people laughed. I sat down next to @eva and chatted with the group. Shortly after me arrived even the nerds. They looked a bit confused and lost.`);
			kat(`Hello!`);
			txt(`I greeted them and turned to @mik:`);
			kat(`@qaa was helping me with homework and I told him they should come. Is that okay?`);
			mik(`...sure! Why not? I was inviting everybody! Take a seat, guys!`);

			link(`Evening. `, 200);
			break;
		
		default:
		case 200:
			emo("relax");
			
			if(  !mile.kiss_crash && (mile.drunk >= 4 && Math.random() < 0.5)  ){
				kiss_crash();
			}else{
				let desc = `I was in <i>Requiem</i>, literally underground bar. `;
				desc += (()=>{ 
					switch(mile.drunk){
						case 0: return `I was uncomfortably sober and felt like I might need a drink. `;
						case 1: return `I could feel pleasant warmth spreading throughout my whole body. `;
						case 2: return `I could feel I am already drunk and frolicsome. `;
						case 3: return `I was pretty drunk and adventurous. `;
						default:
						case 4: return `I felt a bit dizzy. Maybe the last series of shots was a mistake. `;
					}
				})();
				if(counter.kisses == 0){
					desc += `My task was to kiss three girls. `;
				}else if(counter.kisses == 1){
					desc += `My task was to kiss two more girls. `;
				}else if(counter.kisses == 2){
					desc += `To finish my task I had to kiss one more girl. `;
				}
				
				
				txt(desc);
				
				link(`Shots. `, 201);
				
				//CHAT
				if(!counter.eva_sas) link(`Chat with @sas and @eva. `, 320); 
				if(!mile.qcc_sas && mile.qcc_friend > 0) link(`Chat with @qcc. `, 400); //TODO BOOST
				
				if(!counter.eva_ven && counter.eva_drunk > 0) link(`Chat with @eva and @ven. `, 330); 
				
				//if()
				
				//KISS
				if(!mile.sas_kiss && !counter.sas_off) link(`Kiss @sas. `, 300);
				if(
					( (mile.drunk > 0 && mile.sub < 10) || mile.drunk > 1 )
					&& !mile.eva_kiss
					&& counter.eva_drunk
				){
					link(`Kiss @eva. `, 310);
				}
				if(
					( (mile.drunk > 0 && mile.sub < 10) || mile.drunk > 1 )
					&& !mile.tom_zan_kiss
				){
					link(`Kiss @zan. `, 500);
				}
				if(
					counter.kisses == 2 
					&& !mile.just_kiss
					&& (  (mile.drunk > 1 && mile.sub < 5) || (mile.drunk > 2 && mile.sub < 15) || mile.drunk > 3  ) 
				){
					link(`Kiss random woman at bar. `, 600);
				}
				
				if(counter.kisses >= 3){
					link(`Leave the bar. `, 900);
				}

			}	
			break;
				


//SPELL		
		case 201: 
				mile.drunk++;
				{
					const one =  money.requiem_shot; 
					const two = 2 * money.requiem_shot;
					const three = 3 * money.requiem_shot;
					
					con_link(ext.irl_money > one, `not enough money`, ` Alone (${one}€). `, 205);
					if(counter.sas_drunk < 2) con_link(ext.irl_money > three, `not enough money`, `Invite @eva and @sas  (${three}€). `, 206);
					if(counter.sas_drunk < 2) link(`With @eva and @sas. `, 202);
					if(counter.zan_drunk < 1) con_link(ext.irl_money > two, `not enough money`, `Invite @zan (${two}€). `, 207);
					if(counter.zan_drunk < 1) link(`With @zan. `, 203);
					if(counter.qaa_drunk < 2) link(`With @qaa, @qbb and @qcc. `, 204);
				}
				link(`Back. `, 200);
				break;
		
		case 205: 
				txt(`I went to the bar and ordered a shot. `);
				counter.money -= money.requiem_shot;
				link(`Back. `, 200);
				break;
				
	//counter.generous = 0;
		case 202: 
				emo("happy");
				counter.generous--;
				counter.eva_drunk++;
				counter.sas_drunk++;
				counter.sas_off = false;
				eva(`Shots?`);
				kat(`Sounds great.`);
				sas(`Yeah!`);
				eva(`Let's go!`);
				txt(`We went to the bar and ${counter.sas_drunk === 1 ? "@eva" : "@sas"} ordered colorful shots in fancy vials.`);
				eva(`Cheers!`);
				txt(`We clinged and bottomed them up.`);
				link(`Back. `, 200);
				break;
			
		case 206:
				counter.generous++;
				counter.money -= 3 * money.requiem_shot;
				kat(`Girls! Let's get shots!`);
				sas(`Yay!`);
				txt(`I went to bar and bought us colorful shots in fancy vials.`);				
				txt(`We clinged and bottomed them up.`);
				link(`Back. `, 200);
				break;
			
			
		case 203: 
				counter.zan_drunk++;
				counter.generous--;
				zan(`Hey, @kat! What are you drinking?`);
				txt(`@zan was standing next to the bar and noticed me passing by. I joined her and we got shots.`);
				link(`Back. `, 200);
				break;
		
		case 207: 
				counter.zan_drunk++;
				counter.generous++;
				counter.money -= 2 * money.requiem_shot;
				kat(`Hey, @zan! What are you drinking?`);
				txt(`I waved at @zan. She joined me at the bar an I bought us vials with shots. `);
				zan(`Thanks, @kat! `);
				link(`Back. `, 200);
				break;
			
		case 204: 
				counter.qaa_drunk++;
				counter.qbb_drunk++;
				counter.qcc_drunk++;
				if(counter.qcc_drunk === 2){
					kat(`Hey! Wanna do more shots?`);
					if(mile.qaa_kiss_angry){
						qaa(`Fuck off.`);
						qbb(`Come on, be nice to a lady. Of course we want!`);
					}else{
						qbb(`Yeah!`);
						qaa(`Why not?`);
					}
					qcc(`I think I had enough.`);
					qbb(`Come on, don't be a pussy!`);
					link(`Yeah, don't be a pussy! `, 210, ()=> mile.qcc_drunk = true);
					link(`He doesn't have to drink if he doesn't want to. `, 210, ()=> mile.qcc_drunk = false);
				}else{
					txt(`The nerds were staning in the queue next to the bar.`);
					qbb(`@katalt, will you join us for shots? @qaa is paying.`);
					qaa(`I am?`);
					kat(`...sure, why not.`);
					txt(`@qaa ordered us shots. Me, @qaa and @qbb drank them and then watched @qcc carefully sipping his and listened @qbb's explanation why using stylish vials instead of ordinary glasses was stupid.`);
					link(`Back. `, 200);
				}
				break;
				
		case 210:
				if(!mile.qcc_drunk){
					counter.qcc_drunk--;
					mile.qcc_friend++;
					kat(`Come on, don't force him when he doesn't want to, he know best his limits.`);
					qcc(`Thanks, @katalt.`);
					if(mile.slut > 6){
						kat(`If you feel too drunk, slow down and drink some water. You don't want to get so drunk you'll blackout and wake up the next morning next to a dude whose name you don't even remember.`);
						qbb(`Does that happen often?`);
						if(mile.slut > 14){
							kat(`Every once in a while.`);
							txt(`I shrugged.`);
						}else{
							kat(`...it never happened to me.`);
						}
					}else{
						kat(`If you feel too drunk, slow down and drink some water, that will help.`);
					}
					txt(`I ordered three shots and outrageously overpriced glass of fizzy water with ice.`);
				}else{
					if(mile.qaa_kiss_angry){
						txt(`@qaa and @qcc were not very excited - @qcc did not want to drink anymore and @qaa did not wanted to drink with me - but we ordered them shots too.`);
					}else{
						txt(`We teased @qcc and he eventually agreed to get drunk with us.`);
					}
				}
				if(counter.kisses == 0){
					qbb(`Who is going to be the first girl you're going to kiss?`);
					qaa(`My bet is @sas!`);
				}else if(counter.kisses == 1){
					qcc(`So how does it feel to kiss a girl?`);
					kat(`You don't know?`);
					qcc(`...what? ...of course I do! I mean if you're both girls! Is it any different than kissing a guy?`);
					kat(`Yes and no.`);
				}else if(counter.kisses == 2){
					qaa(`Cheers!`);
					qbb(`So far good job, @katalt! You have to kiss just one more girl!`);
				}else{
					qbb(`To @kat! And her lesbian-kissing achievements!`);	
				}
				link(`Back. `, 200);
		break;
				
		
		
	//KISS SAS
		case 300:
			if(counter.sas_drunk === 0){
				txt(`I searched for the @sas and found her next to the bar on the highest floor. She was alone, this way my chance!`);
			}else if(counter.sas_drunk === 1){
				txt(`I chose the right moment. @eva was on a dance floor and @sas was alone, resting and sipping a big green and red cocktail with a tiny umbrella and really long straw.`);
			}else{
				txt(`@sas just returned from the dance floor. She was still lightly swaying to the rhythm which could be faintly heard from above and obviously had good fun.`);
			}
			txt(`I beckoned the nerds to watch from distance and approached my friend.`);
			
			link(`Suggest kiss. `, 301);
			if(
				mile.sub < -5
				|| (mile.sub < 5 && mile.drunk > 0)
				|| (mile.sub < 13 && mile.drunk > 1)
				|| mile.drunk > 2
			) link(`Forcefully kiss her. `, 302);
			break;
	
		case 301:
			emo("unco");
			if(!counter.sas_asked){
				kat(`Have you ever kissed a girl?`);
				sas(`WHAT?! No, I did not?`);
				kat(`We should totally kiss!`);
				
				if(counter.sas_drunk === 0){
					sas(`But why, @kat?`);
					txt(`I did not expect such a firm resistance from @sas.`);
					kat(`Dunno, for fun?`);
					sas(`No thanks.`);
				}else if(counter.sas_drunk === 1){	
					sas(`But why, @kat?`);
					txt(`I did not expect such a firm resistance from @sas.`);
					kat(`Why not? It could be fun!`);
					sas(`I'm not sure about that, @katalt. I'm not a lesbian!`);
					kat(`Me too! That's what makes it crazy! Come on, don't be such a buzzkill!`);
					txt(`@sas hesitated but then she leaned forward. I quickly kissed her. Our kiss was short but not bad. Not bad at all!`);
					mile.sas_kiss = 2;
					counter.kisses++;
				}else{		
					txt(`Confused @sas looked at me like I was crazy but then she suddenly clumsily leaned forwards and her lips connected with mine. I could smell and taste peppermint liquor from her breath but the kiss was lovely.`);
					mile.sas_kiss = 3; 
					counter.kisses++;
				}
			}else{
				if(counter.sas_drunk === 1){	
					kat(`Hey! I thought maybe you changed your mind about the kiss..`);
					sas(`No, I did not!`);
				}else{
					kat(`Hey, @sas! I was thinking...`);
					txt(`I was not able to finish. @sasalt clumsily leaned forward and out of nowhere her lips connected with mine and began kissing me.  I could smell and taste peppermint liquor from her breath but the kiss was lovely.`);
					mile.sas_kiss = 3; 
					counter.kisses++;
				}
			
			}
			counter.sas_asked = true;
			counter.sas_off = true;
			link(`Back. `, 200);
			break;
			
		case 302: 
			mile.sas_kiss = 1; 
			mile.sas_exploit++;
			counter.kisses++;
			emo("unco");
			
			txt(`I was not sure how to convince @sas to kiss me. So I decided to act and not ask for her approval. Suddenly I leaned closer and kissed her red lips.`);

			if(counter.sas_drunk === 0){
				txt(`She immediately pushed me away.`);
			}else if(counter.sas_drunk === 1){	
				txt(`She did not resist at first but then she backed away.`);
			}else{		
				txt(`I did not expect her reaction - she passionately kissed me back. Still, when was the kiss over, she protested:`);
			}
			sas(`What the hell! What are you doing, @kat!`);
			kat(`Nothing! I was thinking... I never kissed a girl... so I wanted to try it... and you were there... looking hot...`);
			sas(`Are you a lesbian or what?!`);
			kat(`No! It was a completely heterosexual kiss!`);
			sas(`Never kiss me again without asking! I'm angry at you!`);
			kat(`Of course! I'm sorry!`);
			link(`Back. `, 200);
			break;
			
			
	//KISS EVA 
		case 310:
			mile.eva_kiss = 1;
			txt(`Approaching @eva was a more delicate matter. While she generally accepted my leadership she never let me to push her around like @sas did.`);
			txt(`I waited for a moment when she was alone and then - secretly followed by nerds - dared to talk with her and bring the kiss up.`);
			kat(`Pretty crazy night`);
			if(counter.eva_drunk > 1){
				eva(`Fuck yeah!`);
			}else{
				eva(`I guess a bit crazier than usually.`);
			}
			kat(`What's a crazy thing you always wanted to do but never did?`);
			
			if(counter.eva_drunk > 1){
				eva(`Rob a jewellery store!`);
				kat(`Maybe a bit less crazier.`);
			}
			eva(`Like what?`);
			kat(`Dunno, like fuck a black guy or make out with a girl.`);
			
			if(counter.eva_drunk > 1){
				eva(`I would love that!`);
				kat(`To kiss a girl?`);
				eva(`Nah, to fuck a black guy! I want to see myself whether they're really so huuuuge or if it's just a meme.`);
				kat(`I guess it depends on a guy...`);
				eva(`Can you even imagine it? I've seen pics with such monstrous dicks ${mile.slut > 9 ? "even a slut like you wouldn't be able to handle! " : "it wouldn't even fit inside you! "}`);
				kat(`Well, the size isn't everything...`);
				eva(`Of course it isn't! But you know, just for once I would love to experience being with a guy with a dick so big it would hurt when he shoves it inside me! You wouldn't?`);
				if(mile.slut < 4){
					kat(`No, I think I prefer dicks of reasonable size`);
				}else if(mile.slut < 12){
					kat(`Hmmm, yeah, I guess. It might be an interesting experience.`);
				}else{
					kat(`Fuck yeah. I know exactly what you're talking about!`);
				}
				
				eva(`Or like, being fucked by two black guys. Or three!`);
				kat(`You mean... one after another or all at the same?`);
				eva(`Yes!`);
				 if(mile.slut > 8){
					txt(`I have to admit, @eva's daydreaming was making me a bit aroused. I was glad I inspired her, however, I wanted to talk about something else:`);
				}else{
					kat(`You wouldn't be able to walk for days!`);
					eva(`Worth it!`);
				}
			
			}else{
				eva(`I think that might be fun!`);
				kat(`Kissing a girl?`);
				eva(`Nah, having sex with a black guy. Are they really so well-endowed? Finding out myself! You wouldn't?`);
				if(mile.slut < 7){
					kat(`Well, maybe.`);
				}else{
					kat(`Yeah, I definitely would!`);
				}
				txt(`@eva smiled, her distant look revealed she imagined the experience. I was glad I inspired her, however, I wanted to talk about something else:`);
				
			}
			
			kat(`What about kissing a girl?`);
			eva(`I'm not really interested in that.`);
			kat(`Really? Not even wanting to try it? With me, for example?`);
			if(mile.a_public >= 3){
				eva(`I see that nerd doesn't know how to make you satisfied! When you beg your bestie for a kiss!`);
			}else{
				eva(`You're without boyfriend for too long, @kat. There are plenty of guys, just pick one of them and he'll be happy to fuck you, you don't have to beg your bestie for a kiss!`);
			}
			kat(`I'm not begging you! It was just a suggestion!`);
			
			link(`Push it. `, 311);
			link(`Back away. `, 312);
			break;
			
		case 311:
			mile.eva_exploit++;
			emo("shy");
			kat(`Come on, @eva! Don't be such a prude buzzkill!`);
			txt(`I moved closer, invading her personal space.`);		
			if(counter.eva_drunk > 1){
				mile.eva_kiss = 3;
				eva(`What the hell is wrong with you @kat?`);
				txt(`She angrily shoved me away from her.`);
				if(mile.slut < 1){
					eva(`Oh no! Suddenly I am the prude one?! Miss goody two-shoes suddenly decided she wants to be the biggest slut in the school? Is that why you recently dress up like a whore?`);
					kat(`I don't dress up like a whore!`);
					eva(`Oh, you do! You're free to do whatever you want, experimenting with being as big slut as you wish to, but leave me out of it!`);
				}else if(mile.slut > 13){
					eva(`Did you run out of all guys so you're now bothering girls too?`);
					kat(`What do you mean, @eva?`);
					eva(`You know exactly what I mean, @kat! You used to be a slut but recently you're acting like a desperate nympho eager to fuck anything that moves!`);
					kat(`I wouldn't say...`);
					eva(`Have some shame! Or at least leave me out of it!`);
				}else{
					eva(`Are you on an experimental slutty streak? That's the reason for your recent shameless outfits and erratic lewd behavior? I'm not judging, you, @kat, if you enjoy acting like a whore, good for you but leave me out of it!`);
				}
			}else{
				mile.eva_kiss = 2;
				eva(`Stop it, @kat! You're drunk! I'm not going to make out with you!`);
				txt(`She shoved me away from her.`);
				kat(`But @eva!`);
				eva(`I TOLD YOU I'M NOT INTERESTED! What the hell is wrong with you tonight?`);
				kat(`...nothing.`);
			}
			txt(`Something told me I will not convince @eva to kiss me.`);
			link(`Back away. `, 312);
			break;
			
			
		case 312:
			emo("anoy");
			txt(`Nerds meet me to discuss my failure:`);
			if(mile.eva_kiss > 1){
				qaa(`What did you do to her? We could hear her annoyed yelling?`);
				if(mile.eva_kiss === 3){
					kat(`She was a bitch to me! Because of you! You're fucking ruining my life!`);
					qbb(`Don't be so melodramatic, @kat, it's your own fault that you're having such a bitchy friends!`);
				}else{
					kat(`She isn't into making out with girls.`);
					qbb(`If she says that, it absolutely means she's gay and she's in denial.`);
					kat(`Yeah, I don't think so.`);
				}
			}else{
				kat(`That didn't went well.`);
				qbb(`What happened?`);
				kat(`She wasn't interested.`);
			}
			aly.acb(`Strange, I can't believe there's somebody able to resist your charm! `);
			link(`Back. `, 200);
			break;
			
			
	//CHAT EVA SAS	
		case 320:
			emo("happy");
			counter.eva_sas = true;
			eva(`...so that's everything for now but we have the next date tomorrow. I'll keep you updated!`);
			sas(`Good for you!`);
			kat(`Oh my, your story almost make me jealous. And horny.`);
			eva(`Don't cream your panties, @ada is mine and I won't let you steal him!`);
			kat(`You wouldn't be able to stop me if I really wanted!`);
			txt(`I chuckled.`);
			eva(`What about you, @sas? Why you didn't bring @dav?`);
			sas(`It didn't work.`);
			eva(`What do you mean? It seemed he was perfect for you?`);
			sas(`He constantly kept talking about himself and his work and the money he's making. And constantly made those horrible sleazy jokes.`);
			kat(`You shouldn't be such a prude!`);
			sas(`I don't mind flirting and suggestive jokes. But they weren't smart nor subtle, just obnoxious!`);
			eva(`You're such a trainwreck, @sas! Even such a hunk isn't good enough for you?`);

			link(`There's nothing wrong with being forever lonely and loved by no one. `, 321);
			link(`Look at that tight body and pretty face! She shouldn't settle for the first jerk! `, 322);
			break;

		case 321:
			mile.sas_exploit++;
			sas(`Thanks, girls, for being always so supportive. I'm not even sure why are we friends.`);
			eva(`Come on, @sas, I was just joking!`);
			if(mile.a_public >= 3){
				emo("anoy");
				sas(`At least you shouldn't make fun of me, @katalt, you're dating a pathetic nerd.`);
				kat(`Yeah, you know what? At least I'm dating somebody!`);
			}else{
				sas(`And not like @katalt is dating anybody either!`);
				kat(`So what? I can get anybody I want!`);
			}
			eva(`Calm down girls! Don't worry, @sas, we'll find you somebody hot!`);
			link(`Back. `, 200);
			break;
			
			
		case 322:
			mile.sas_sassy++;
			emo("anoy");
			sas(`Thanks, @katalt. At least somebody is supportive.`);
			eva(`Come on, @sas. I'm just joking. And you know @katalt would be the first one making fun of you if she wasn't in the exactly same situation.`);
			if(mile.a_public >= 3){
				sas(`She isn't in the same situation. She's at least dating that nerd.`);
				eva(`Sometimes dating nobody might be a better alternative than going out with the first random guy.`);
			}
			kat(`@eva, you're such a bitch tonight.`);
			eva(`Thanks! I'm trying my best!`);
			link(`Back. `, 200);
			break;

	//CHAT EVA VEN
		case 330:
			counter.eva_ven = true;
			emo("anoy");
			
			txt(`I was searching for my friend and found her taking shots with my worst enemy.`);
			kat(`@eva?`);
			eva(`Don't worry! I'm getting @ven drunk to make her reveal why she decided to run against you. And the truth will shock you! It was only because ${mile.vendy_makeup < 0 ? "you were such a cruel bitch to her" : "she was envious of you having everything she ever wanted"}.`);
			txt(`Drunk @eva explained but drunk @ven fumblingly waved her hands and argued:`);
			//Argued @ven, the alcohol she drank made her less articulate.
			ven(`H... hey! That isn't true! I told you I thought it might be cool to be the class chairman. `);
			if(mile.vendy_makeup < 0){
				eva(`@kat didn't hesitate for a moment to terribly publicly humiliate you because she thinks you're just a useless piece of garbage! Doesn't that make you pissed and craving revenge?`);
			}else{
				eva(`@kat is popular - unlike you, beautiful - unlike you and overall amazing - unlike you. Doesn't that make you pissed and craving to totally destroy her out of spite?`);
				//??
			}
			ven(`Well, I guess I might be a little pissed. `);
			eva(`Damn right, you have the full right to be angry at her! See, @katalt, how great I am at reading people?`);
			txt(`Drunk @eva was even more smug and insufferable than usual.`);
			eva(`Why do you even care about that dumb cow is running?`);
			ven(`I'm still here!`);
			eva(`Hush! I'm not talking with you anymore. Being the chairman is just a stupid waste of time!`);
			kat(`It is not!`);
			ven(`Yeah! It isn't!`);
			kat(`It is a very important position!`);
			ven(`And give you a chance to matter and change things for the better!`);
			eva(`Like what? @kat never changed anything for the better.`);
			ven(`Because she's selfish and useless and doesn't care about the greater good. `);
			kat(`I changed plenty of things for the better! You have no idea how hard is to make anything approved! The principal usually agrees only with things she wanted to do anyway!`);
			ven(`That's a lot of excuses! You want to be the student council president only because of the benefits! `);
			eva(`There are benefits?`);
			ven(`So many!`);
			kat(`Maybe a few.`);
			eva(`You're almost making it sound cool.`);
			ven(`Of course, it is!`);
			kat(`Yeah! And that's why you never, ever be the class chairman, @ven! You're too lame and pathetic! Face it, you needy bitch! Nobody likes you!`);
			link(`Back. `, 200);
			break;




	//CHAT QCC
		case 400:	
			mile.qcc_sas = 1;
			txt(`@qcc was standing in a corner, slowly sipping his beer. @qbb and @qaa were not around and without them, he looked kinda abandoned. I took pity on him and approached him. I soon regreted it.`);
			txt(`We chatted for a while. I did not want to spend with him the whole evening but he was getting clingy and did not get any of my hints we should part ways. I was getting desperate when I noticed @sas walking past us and decided to sacrifice her.`);
			kat(`Hey, @sas!`);
			sas(`What?`);
			kat(`@qcc, did you know @sas is collecting ceramic frogs? You're a nerd, you certainly know some interesting facts about frogs, right?`);
			qcc(`I'm not sure. Amphibians were never my forte.`);
			kat(`You like history, right? @sas, you like history too, right? You sent me that video about how fashion changed in the last three centuries.`);
			sas(`Yeah, I did. I thought it was kinda interesting, wasn't it ?`);
			kat(`@qcc, do you know anything about historical fashion?`);
			sas(`Well, I recently read something about renaissance and baroque lingerie...`);
			kat(`That sounds like a topic for a wonderful enlightening conversation. Discuss it! I have to go to pee.`);
			txt(`I escaped them.`);
			link(`Back. `, 200);
			break;
	
	
	
	//KISS ZAN
		case 500:
			counter.kisses++;
			emo("imp");
			kat(`Hey, @zan!`);
			if(counter.zan_drunk){
				zan(`Hey, @kat! How are you doing?`);
				txt(`She already had a few drinks and was in a pretty good mood if you know what I mean.`);
				kat(`Fine! ...so, have you ever kissed a girl?`);
				zan(`Yeah! ...did you?`);
				kat(`Kinda. ...we should kiss!`);
				zan(`...okay! But not here, I want that asshole to see us!`);
				
				if(mile.tom_zan < 4){
					mile.tom_zan = 4;
					kat(`Which one? @tom?!?`);
					zan(`We broke up because that bastard cheated on me!`);
				}else{
					kat(`Which one? Oh, you mean @tom!`);
					zan(`Yeah, that cheating bastard! `);
				}
			}else{
				zan(`Hey, @kat.`);
				txt(`She seemed gloomy.`);
				kat(`Are you fine?`);
				zan(`Yeah. I'm just surprised some people have the audacity to show up after what they did.`);
				
				if(mile.tom_zan < 4){
					mile.tom_zan = 4;
					txt(`She nodded towards @tom.`);
					kat(`What? You're not dating anymore? !?`);
					zan(`No! We broke up! That bastard cheated on me!`);
				}else{
					txt(`She nodded towards @tom. She was still pissed because of his cheating.`);
				}
				
				kat(`I know a way how to make him jealous.`);
				zan(`Do tell!`);
				kat(`We should kiss right in front of him!`);
				zan(`...yeah. Brilliant idea, @katalt!`);
			}
			link(`Kiss @zan. `, 501);
			break;
			
			
		case 501:
			emo("shy");
			txt(`She grabbed me and maneuver me through the crowd until she was sure @tom (and coincidentally two of three nerds) noticed us and then she kissed me.`);
			txt(`@tom's jaw dropped when he saw us very erotically kissing right in front of him. Seriously, @zan was not holding anything back and was wildly making out with me in front of everybody.`);
			tom(`G.. girls? What are you doing? `);
			zan(`I'm testing the hypothesis that even a girl kisses better than you!`);
			tom(`Than me?! You're out of your mind!`);
			txt(`He suddenly pulled me closer and passionately pressed his lips against mine.`);
			txt(`${mile.slut > 10 ? "I was kissing him for a while, enjoying the moment, before I" : "I immediately"} stepped back and pushed him away. @zan gave me a very nasty look, she was not happy I was kissing @tom even though I had no chance to stop him.`);
			tom(`So? Who is the better kisser, @katalt?`);
			txt(`He proudly asked me.`);
			
			link(`I don't know!`, 502);
			link(`@tom.`, 520);
			link(`@zan.`, 510);
			link(`You are both great!`, 502);
			link(`You are both awful!`, 502);
			break;
			
		case 502:
			txt(`I tried to calm down the situation and not make anybody angry but neither of them was satisfied with a draw.`);
			txt(`@zan immediately grabbed me and kissed me again. She was all in, the kiss was very long and ardent, with her tongue nearly touching my tonsils and her fingers gently caressing my face and neck.`);
			txt(`@tom did not give me even a second to collect myself. He held my head tightly but carefully, ruffling my hair and violently plundering my mouth.`);

			tom(`So? `);
			zan(`Soooo? `);
			kat(`Ehh...`);
			txt(`I was catching my breath. `);
			link(`@zan`, 510);
			link(`@tom`, 520);
			break;
		
		case 510:
			emo("imp");
			txt(`@tom was not a very graceful loser.`);
			tom(`Of course you wanton bitches are sticking together!`);
			txt(`He yelled at us and left very angry, humiliated by his loss to a girl.`);
			zan(`@kat has probably even bigger dick than you!`);
			txt(`@zan screamed at him. It did not even make sense since I had no penis. Unless she was speaking metaphorically.`);
			
			if(counter.zan_drunk){
				mile.tom_zan_kiss = 2;
				zan(`Thank you, @kat! That was awesome! Let's go!`);
				kat(`Go where?`);
				txt(`She dragged me to a dark corner hidden behind a column and again began making out with me.`);
				zan(`Fuck, you're awesome at this, @kat!`);

				link(`You're taking this too far!`, 511);
				link(`You're hot too!`, 512);
				link(`Let's have hot lesbian sex! `, 513);
			
			}else{
				mile.tom_zan_kiss = 1;
				zan(`Thank you, @kat, That was awesome! I owe you one!`);
				link(`You are welcome. `, 200);
			}
			break;
			
		case 511:
			mile.slut--;
			mile.lesbian--;
			emo("shy");
			
			txt(`@zan let me go and looked embarrassed.`);
			zan(`Sorry, @katalt. It was your idea and I thought you'd enjoy it.`);
			kat(`I did! But I'm not a lesbian.`);
			zan(`Me neither! But somebody I wish I were, all guys are such stupid jerks!`);
			kat(`Yeah, I won't argue against that.`);
			link(`Back. `, 200);
			break;
			
		case 512:
			emo("smug");
			
			zan(`Fuck, @kat! I wish you were a guy! It would make my life so easier.`);
			kat(`You would bang me if I were I guy?`);
			zan(`Damn yeah, right here! All guys are such stupid jerks, why isn't there somebody as cool as you but-...`);
			kat(`With a huge dick?`);
			zan(`Damn yeah! Shame I'm not into muff-diving!`);
			txt(`Her fingers brushed against my lap, like to make sure I was not hiding anything. I slapped her ass:`);
			kat(`Then stop teasing me, you straight nympho!`);
			txt(`She giggled and we parted ways.`);
			link(`Back. `, 200);
			break;
			
		case 513:
			mile.slut++;
			mile.lesbian++;
			emo("shy");
			
			txt(`@zan let me go and looked embarrassed.`);
			zan(`Sorry, @katalt. I'm not a lesbian. Even though sometimes I wish I were, some guys are making me so angry!`);
			kat(`Me neither, of course! But it might be a fun experience.`);
			zan(`Hmmm... I always wanted to try it but now I'm not sure... maybe some other time... I'm so sorry, @kat, for teasing you and then backing out!`);
			kat(`No problem!`);
			link(`Back. `, 200);
			break;
			
		
		case 520:
			mile.tom_zan_kiss = -1;
			txt(`@zan was not a very graceful loser.`);
			zan(`Everybody knows you are a deceitful snake and a promiscuous whore! You were just using me because you want to get railed by @tom, don't you, slut!`);

			if(mile.sub < 6){
				emo("angry");
				kat(`That's rich! A tramp like you is calling me a whore! `);
				zan(`Fuck you! You easy slut! `);	
				kat(`Fuck you too! `);
			}else{
				emo("help");
				kat(`Sorry, I didn't mean to... `)
				txt(`I tried to expain and feebly defend myself but she did not listen. `);
				zan(`Fuck you, you easy slut!  `);	
			}
			txt(`@tom led me away from pissed @zan. `);
			tom(`Sorry for the scene. But @zan could be such an irational bitch. And thank you that you choose me. I can say you're also a better kisser than @zan. Hmmm, maybe I should text her that.`);
			kat(`I would prefer it if you did not.`);
			tom(`Right. So do you want to hang out? I know about a spot where we can hang out privately...`);

			link(`No, thanks. `, 200);
			link(`I would love to but I promised @eva I'll meet her at the bar. `, 200);
			con_link(mile.sub < 4, `submissive`, `Fuck off, I told you I'm not interested. `, 200);
			con_link( (mile.slut > 10 || (mile.slut > 3 && mile.drunk > 1) || mile.drunk > 2), `low slut or too sober`,	`Show me! `, 521);
			break;
		
		
		case 521:
			mile.tom_zan_kiss = -2;
			if(mile.slut < 10) mile.slut++;
			emo("horny");
			
			txt(`He showed a nice place under the staircase and hidden behind a column. There was only one armchair but he offered me to sit on his lap.`);
			tom(`Maybe we should continue with the kissing?`);
			txt(`He suggested, his hand suggestively patting my thigh. Instead of answering, I leaned closer and we began making out. Sure, it was @tom but I was drunk and horny and very prone to bad decisions.`);
			
			{
				const intro = `I was about to ${mile.slut > 14 ? "unbutton his pants and have a very risky quickie right there" : "suggest we should find even more private place"}`;
				if(mile.a_cuck > 0){
					txt(`${intro} when we were disturbed by @qaa.`);
					qaa(`@kat? I was looking for y... w... what are you doing!?!`);
					txt(`He was stunned when he caught me with another man.`);
					kat(`I'm making out with @tom! How does it looks-like, dumbass?`);
					qaa(`B... but...`);
					kat(`Fuck off! Or I guess you can stay and watch if you want, perv?`);
					txt(`I teased him but he kinda ruined the mood and made me have second thoughts about having sex with @tom while being intoxicated.`);
					link(`Back. `, 200);
				/*
				}else if(){
					${intro} when I realized my task was still not finished and instead of kissing @tom I should be kissing girls. I excused myself, hoping that maybe we would be able to continue as soon as I finish my quest. 
				*/
				}else{
					txt(`${intro} when we were disturbed by @sas.`);
					sas(`@kat? I need to talk with you.`);
					kat(`I don't have time!`);
					txt(`I snapped, she could not read the room and had to ruin the mood!`);
					sas(`It it's very important.`);
					kat(`...fine!`);
					txt(`I left @tom behind and followed her.`);
					kat(`I hope it's something really important!`);
					sas(`Well, you told me that if you got drunk and horny and wanted to have sex with @tom, I should stop you, any means necessary.`);
					kat(`Did I? When?`);
					sas(`When you broke up with @kub.`);
					kat(`That was long ago! The current circumstances are completely different!`);
					sas(`Sorry, but I can't let you have sex with @tom until you relinquish the order while fully sober.`);
					
					link(`Thanks, @sas. `, 200);
					link(`You're such an annoying twatswat! `, 200, ()=> mile.sas_exploit++);
					
				}
			}				
			break;
			
		case 600:
			mile.jus_kiss = 1;
			counter.kisses++;
			
			txt(`I was kinda running out of my currently present female friends I was not afraid to ask for a kiss. I guess I could just go to a random drunk girl on the dance floor and kiss her but that seemed kinda rude.`);
			if(mile.qcc_drunk < 2){
				qbb(`So who will be your next victim?`);
				txt(`The nerds urged me.`);
				kat(`I'm not yet sure. And please don't use the word <i>victim</i>. Kissing me is an extremely pleasurable experience!`);
				qbb(`What about her?`);
				txt(`@qbb pointed at an alluring woman in her late twenties with jet-black hair, pale skin and arms covered with tattoos. If somebody was a kinky bisexual, it was her.`);
				kat(`Why not?`);
			}else{
				txt(`Alone at the bar was sitting an alluring woman in her late twenties with jet-black hair, fair skin and arms covered with tattoos. If somebody was a kinky bisexual, it was her.`);				
			}
			txt(`I felt drunk and bold enough to confidently approach her.`);
			
			kat(`Hello!`);
			txt(`She curiously looked at me.`);
			jus(`Hi? How are you?`);
			kat(`Fine! ...well, actually not so fine.`);
			if(mile.kiss_crash){
				jus(`What happened to you? `);
				txt(`She pointed at my stained @upper. `);
				kat(`That's a long story... well, maybe not so long, I had a collision with a table. `);
				jus(`Ouch! `);
				kat(`But that isn't my main problem.`);
				jus(`What is? `);
			}else{
				jus(`Something is troubling you, hon? `);
			}
			kat(`I made a stupid bet that I'll kiss a girl.`);
			jus(`Don't say nobody is interested, you look quite cute! `);
			kat(`Well, maybe. But I'm too shy to just walk around and ask strangers whether they want to kiss me. It would be awkward. `);
			jus(`Nonsense! Just go to the first girl you find attractive and ask her. If she says no, her loss!`);
			kat(`Thank you for the advice!`);
			jus(`You're welcome!`);
			kat(`So... would you like to kiss me?`);
			jus(`Aww! Sure, I wouldn't want such a lovely girl to lose her bet.`);
			txt(`She put her index finger under my chin to turn my face to hers, leaned closer and kissed me. Her eyes were so deep it was almost scary and her lilac perfume was heavy and overwhelming. She was kissing me slowly and deliberately and with nonchalant confidence. `);
			jus(`You're welcome, hon. `);
			kat(`T.. Thank you.`);
			if(mile.lesbian < 2){
				txt(`I was pretty sure I was not a lesbian but her single kiss was enough to make me question my sexuality.`);
			}else{
				txt(`That might be the first time when a mere kiss made me wet.`);
			}
			jus(`I'm @jus.`);
	
			if(NAME.katalt != NAME.kat){
				kat(`I'm @katalt... but friends call me @kat.`);
				jus(`Nice to meet you... @kat.`);
			}else{
				kat(`I'm @kat.`);
				jus(`Nice to meet you.`);
			}
			txt(`She enigmatically smiled.`);
			
			link(`Back. `, 200);
			break;
		
		
	case 900: 
			if(counter.generous < 0) mile.stat--;
			if(counter.generous > 0) mile.stat++;
//TODO EXPLAIN
			payment(counter.money, "requiem"); //already negative number
			
			if(mile.drunk >= 4){
				txt(`I am not sure when I left the <i>Requiem</i> but it had to be long after midnight. I was staggering forward towards the tram station to catch the night bus home.`);
			}else if(mile.drunk >= 2){
				txt(`It was long after midnight when I left the <i>Requiem</i>. The chill air was refreshing even though I felt I was intoxicated and my walk was less sure. I have to cross two streets to get to the tram station to catch the night bus home.`);
			}else{
				txt(`It was long after midnight when I left the <i>Requiem</i>. I have to cross two streets to get to the tram station to catch the night bus home.`);			
			}
			
			{
				const details = sluttiness().details;
//TODO details.slut < 2	
				if(details.slut <= 1 && !counter.force){
					txt(`I was wearing demure clothes and nothing bad or interesting happened to me.`);
					next();
					if(hardbug) link(`##Assume slutty clothes. `, 900, ()=> counter.force = true);
				}else{
					emo("shock");
					
					txt(`As I was walking along the road, a silver BMW suddenly stopped next to me. I naively assumed the driver might want to ask for directions but he just yelled at me:`);	
					npc(2,`How much?`);
					kat(`What? For what?`);
					npc(2,`For sex, obviously. `);
					
					if(mile.drunk >= 2){
						txt(`My drunk brain struggled to solve the riddle but when I figure out what he meant, I was aghast:`);
					}else{
						txt(`I was aghast when I realized what he meant:`);
					}
					kat(`I'm not a prostitute! `);	
					npc(2,`Are you sure? `);
					txt(`He chuckled and shamelessly checked my body.`);
				
					{
						let level = details.slut === 2 ? `slut` : `whore`;
						if(details.slut === 4) level = `streetwalker`; 
						
						const outfit = (()=>{
							const list = []
							
							if(details.genitals){
								list.push(`I was very obviously wearing no panties`);
							}else if(details.pantiesBelow){
								list.push(`I was wearing @lower so short my panties were visible`);
							}else if(details.tightLower){
								list.push(`my @lower was so tight it left no space to imagination`);
							}else if(details.miniSkirt){
								list.push(`I was wearing ${wears.microSkirt ? "ridiculously" : ""} short skirt`);
							}
							
							if(details.titfall){
								list.push(`the @upper struggled to contain my breasts`);
							}else if(details.cleavage){
								list.push(`my cleavage was maybe a bit too generous`);
							}else if(details.midriff){
								list.push(`the @upper exposed ${wears.piercedNavel ? "my pierced bellybutton" : "my flat belly"}`);
							}else if(details.sexyTop){
								list.push(`my @uper was sexy`);	
							}
							
							if(wears.collar || wears.choker) list.push(`on my neck was a provocative @collar`);
							
							if(!wears.bra) list.push(`I left my bra at home`);
							
							if(mile.makeup) list.push(`my makeup was heavy`);
							
							if(wears.fishnetLegwear){
								list.push(`my long legs were encased in fishnet ${wears.stockings ? "stockings" : "pantyhose"}`);
							}else if(wears.sexyLegwear){
								list.push(`my long legs were encased in tempting @socks`);
							}
							
							//return  capitalise( and(list) );
							return and(list);
						})();
						
						
						//PRUDE
						if(mile.slut < 6){
							if(mile.sub < 5){
								txt(`I was shocked and seriously offended! I might be dressed like a ${level} but it did not mean I was one! ${capitalise(outfit)}. But that still did not gave him the right to solicit sex from me!`);
							}else{
								txt(`I was stunned and embarrassed. I might be dressed like a ${level} but it did not mean I was one! ${capitalise(outfit)}. But that still did not gave him the right to solicit sex from me!`);
							}
						
							if(mile.sub < -5){
								kat(`YES! I'M NOT A WHORE!`);
								txt(`I yelled but he was not fazed.`);
							}else if(mile.sub < 10){
								kat(`Yeah! I'm not a prostitute!`);
								txt(`I scowled.`);
							}else{
								kat(`Yeah! I'm sorry, sir but this is a mistake!`);
								txt(`I tried to explain.`);
							}
						
						//SLUT
						}else if(mile.slut > 15){
							if(mile.sub < 5){
								txt(`I cheerfully laughed. He had a point: ${outfit}. I really looked like a ${level}.`);
							}else{
								txt(`I anxiously laughed. He had a point: ${outfit}. I really looked like a ${level}.`);
							}
							
							if(mile.sub < 10){
								kat(`Yeah, I'm not a prostitute, I just sometimes enjoy dressing up as one.`);
							}else{
								kat(`I'm sorry sir to disappoint you but I'm really not a prostitute, I'm just dressed up as one.`);
							}
							
						
						//NEUTRAL
						}else{
							if(mile.sub < 5){
								txt(`I was surprised and a bit offended. I might look like a ${level}: ${outfit}. But that did not mean I was one!`);
							}else{
								txt(`I was surprised and a bit embarrassed. I might look like a ${level}: ${outfit}. But that did not mean I was one!`);
							}
							
							if(mile.sub < 10){
								kat(`Yeah, I'm positive I'm not.`);
							}else{
								kat(`I'm sorry, sir, but I'm pretty sure I am not.`);
							}
						}
					}
					
					
					//			I thought we were going to have a laugh about the misunderstatement and part our was but 
				
					npc(2, `I see. But I like you. I'll pay you ${money.bj_low} Euro just for a blowjob. `);
					kat(`No!`);
					npc(2, `${money.bj_mid} Euro. `);
					txt(`I wavered. That was a lot of money for a blowjob. Fuck, was I really so drunk I was actually considering it?`);
					
					
					link(`No. `, 920, ()=> mile.prostitution = -2);
					link(`Okay. `, 950);
					if(mile.sub < 10){
						link(`Fuck off, creep! `, 920, ()=> mile.prostitution = -3);
					}else{
						link(`I... I'm sorry, sir... but I can't do it! `, 920, ()=> mile.prostitution = -1);
					}
					con_link(mile.sub < 9, `submissive`, `${money.bj_high} €!`, 901);
				}	
			}
			
			break;
			
	case 901: 
			emo("focus");
			txt(`He chuckled, amused by my sassiness. I felt excited about the haggling, like it was just a game.`);
			npc(2, `You are cute but you aren't worthy ${money.bj_high} Euros, slut. `);
			
			if(mile.slut > 7 && mile.sub < 2){
				mile.prostitute_nego = 2;
				mile.sub--;
				kat(`I'm worth every eurocent. `);
				tom(`I stated confidently. He eyed me for a brief moment and then nodded. `);
				npc(1,`Okay, I believe you. ${money.bj_mid}. Get in the car and impress me. `);	
				link(`Get in the car.`, 950);
			}else{
				mile.prostitute_nego = -1;
				link(`Fine, I'll do it for ${money.bj_mid}. `, 950, ()=>{mile.sub++} );
				link(`Your loss! `, 902);
			}
			break;
			
	case 902:
			mile.prostitute_nego = 1;
			mile.sub--;
			txt(`I turned to walk away. I might agree to become a whore but I would never agree to become a <i>cheap</i> whore.`);
			npc(2, `Come on! Don't be stupid! Fine, ${money.bj_high}!`);
			kat(`Good. You won't regret it.`);
			npc(2, `Get in the car and impress me!`);
			link(`Get in the car.`, 950);
			break;
			
			
	case 920: 
			if(mile.prostitution === -3){
				emo("angry");
				txt(`I showed him a middle finger and walked away. Fuck him!`);
			}else if(mile.prostitution === -2){
				emo("neutral");
				txt(`I refused resolutely. He saw I could not be convinced and closed the window.`);
			}else{
				emo("shy");
				txt(`I was babbling. It felt uncomfortable to tell him <i>no</i> but I just could not do it.`);
			}
			
			if(mile.slut < 2){
				txt(`I felt so dirty and ashamed. He thought I was a whore! Even worse, I was a whore in my own mind because I did not refuse his offer outright but actually thought about it.`);
			}else if(mile.slut > 11){
				txt(`Sure, I was a proud slut who loved sex but this was something completely different! I was not a prostitute! Taking money from a stranger for sucking his cock would be so degrading!`);
			}else{
				txt(`I felt a bit ashamed he mistook me for a prostitute. I might not be a completely chaste girl but I was no whore and I would never take money for sex!`);
			}
			
			txt(`I returned home without further incidents.`);
			next();
			break;
			
	case 950:
			mile.prostitution = 1;
			mile.whore++;
			if(mile.slut < 10) mile.slut++;
			mile.slut++;
			emo("relax");
			log_sex("bj",  "npc_2");
			
			txt(`He was driving with one hand on the wheel, the other one placed on my thigh.`);
			npc(2, `What is your name, slut?`);
			kat(`${NAME.kat[0]}... @zan.`);
			txt(`I was getting a bit concerned about where were we going but he just drove his bavarian to a nearby dark parking lot. There he stopped the car and turned to me:`);
			if(mile.prostitute_nego > 0){
				npc(2, `Show me what you got.`);
			}else{
				npc(2, `You can start.`);
			}
			txt(`He did not have to tell me twice. I leaned down to unfasten his belt, unzip his pants and took his dick out.`);
			npc(2, `Hmmm.`);
			txt(`He approvingly growled when I fondled his cock. I lowered my head down and took the tip in my mouth, wiggling my tongue against it. He possessively grabbed me by my hair but did not force me and let me continue to suck in my own tempo.`);
			txt(`I felt a bit hazy. I was drunk and the whole situation was so surreal. Was I really sucking a stranger for money? The guy let my hair go and instead reached to feel my ass. Then his fingers slipped between my thighs to roughly rub my pussy. That was not technically a part of our deal but I could not argue with my mouth stuffed with a dick.`);

			if(mile.slut < 3){
				txt(`I always thought I was a good girl. Not a boring prude but I had my dignity. On the first date, I never let boys do more than grope me and I would never have sex with somebody I did not know. But tonight I agreed to give a blowjob to a guy I just met, whose name I did not know and who was paying me. It was so fucked up. I wish I could blame the nerds but this was not one of their stupid tasks. To become a whore was my own willing decision.`);
			}else if(mile.slut > 15){
				txt(`It was a very odd experience. I sucked plenty of dicks before but I was never getting paid. It was hauntingly exciting to cross the line between slut and whore. Maybe I should have tried it sooner? Fuck, it was so fucked up! I could not even blame the nerds, this was not one of their stupid tasks. Everything was my own willing decision.`);
			}else{
				txt(`It was a bizarre experience. I was no bashful virgin but still, I was not sleeping around with every random guy. But tonight I agreed to give a blowjob to a guy I just met, whose name I did not know and who was paying me. It was so fucked up. I wish I could blame the nerds but this was not one of their stupid tasks. To become a whore was my own willing decision.`);
			}
			
			txt(`My reverie was impolitely interrupted when the cock in my mouth began to twitch and filled it with its load. I tried to stop the cum from dripping down to my @upper while he was nonchalantly shoving ${bills(mile.prostitute_nego > 0 ? "bj_high" : "bj_mid")} into my cleavage.`);
			npc(2, `Good girl! I really needed that tonight!`);
			if(mile.prostitute_nego > 0){
				kat(`Was I worth it?`);
				npc(2, `...hmmmm. Honestly, I had better hookers but I enjoyed your amateurish enthusiasm.`);
			}
			kat(`Thanks.`);
			npc(2, `Where should I drop you?`);
			kat(`The nearest tram stop would be okay.`);
			
			payment(mile.prostitute_nego > 0 ? money.bj_high : money.bj_mid, "cash");
			
			next();
			break;

/*
			txt("In the morning my head ache and it was especially hard to leave the warm bed. ");
			if(mile.prostitute){
				txt("I was not really sure whether I actually sold myself out or whether it was just a dream. Until I found three 50 € bills. Fuck. Was it worth it? ");
			};
*/			
			

			
	}
}




/*

That @upper is very cute. 
What? 
I was saying, <i>that @upper is very cute</i>. I think you look lovely. 
Intoxicated @qbb was in very pensive mood. 
I thought you were saying <i>@kat, you selfish cunt</i>. 
I smirked at him. 
NO! ...come on, @katalt! Why are you always doing this! 
Doing what? 
Being so sardonic, cold and inaccessible. Not letting anybody get closer to you. 
I was baffled by his question. 
I don't know. This is the way I am. 
I was drunk too and telling him truth was easier than lie. 
if(mile.slut < 1){
	What is even the point of all your games? You're constantly teasing guys, leading them on and then just meanly laught them off. 
	I don't believe that's accurate. 
	All the guys - and I know because I hear them talking in the locker room - believe you're just an arrogant, frigid attention whore. Nobody really respects you or likes you. 
	I don't believe that's accurate either.
	Sure, live in denial! They discuss a lot how to get into your panties but beyond that nobody really cares about you!
	Unlike you who deeply cares about me?
}else if(mile.slut > 10){
	Why is even the point of your sleeping around with every guy? 
	Every guy except of you. And the point is I'm enjoying it. 
	All the guys - and I know because I hear them talking in the locker room - believe you're just a disposable whore. Nobody respects you or sees more in you than your slutty body. 
	Unlike you who see the real me? 
}else{
	You know all the girls thinks you're a hot slut? 
	That's nice. 
	You don't understand. All the guys - and I know because I hear them talking in the locker room - talk only about how to get into your panties or good you were in bed. Nobody of them respect you or cares about anything else than your sexy body. 
	Unlike you who see the real me? 
}
...yes. 
Fuck off. You're exactly the same like them. Actually no, you're even worse! If you cared about me even a little you wouldn't try to punish me for not going out with you. 
Nobody is trying to punish you! 
Yeah? There's no other explanation for your cruel scheme to ruin my life!
We're not trying to ruin your life! You willingly agreed with our deal!
if(mile.blackmail){
	That's only because you tricked me! And now you're not even ashaming to blackmail me!
	We wouldn't blackmail but you were constantly breaking the rules you yourself agreed to follow! 
	I wouldn't break them if they weren't ridiculous and unreasonable! 
	They were perfectly reasonable! 
}else{	
	That's only because you tricked me! 
	Nobody tricked you!
}


*/
	/*
	
	
@kat? 
@qaa wanted to talk with me. He was drunk and did not respect my personal space.
What is going on? 
Watching you kiss another girl... such a lovely sight... it makes me so horny... 
Ehh... sure? 
This was not the right moment, we were surrounded by other people who might overhear our conversation. 
What do you want? 
A kiss!
Right here?! Right now?!
if(){
	Yes. 
	Well, I think...
	Before I was able to finish my thought he leaned forward and forcibly kissed me, right in front of everybody!
	
	Kiss him. 
	Push him away. 
	Don't touch me you fucking nerd!
	
}else{
	Please, @kat!
	
	Kiss him. 
	Meet me outside in five!
	Get lost, nerd!
}





mile.stat--;
I was shocked by his audacity but I submitted and willingly continued making out with him until he himself stopped. The people were staring, shocked I was making out with such a loser. But at that moment, I did not care. I was free to do whatever I wanted!


I quickly pushed him away.
Stop it! Not now you idiot!
A few people spotted us but I tried to 
But @kat!
Not now!


mile.stat++;
I screamed at him and pushed him away with such force he stumbled and fell backward. I did not mean to do that but I did not go to help him. 
People around were loudly laughing, amused at how I handled the loser who was harassing me. Humiliated @qaa stood up and walked away. 



mile.stat--;
I dared to lean forward and kiss him. He drew me closer and we were making out. The people were staring, shocked I was making out with such a loser. But at that moment, I did not care. I was free to do whatever I wanted!

He hesitated but then quickly nodded and immediately left the bar. I did not want to make out with him publicly but I agree to meet him alone. 
Outside the bar I allowed him to kiss me and let him knead my breast for a while before returning back. 

I sharply refused him. A few people around chuckled at how I handled the loser who thought he might a chance with me. Disappointed @qaa left. But it was only his fault, if he thought I would kiss him publicly he was insane!

*/

	 
	
	
	
	
	
	
	/*
"A guy told me he wanted to show me something upstairs in his room, but as we were walking up the stairs, I realized what was really happening. So, in my drunk logic, I decided to fall down the stairs to avoid the situation."
 Well, at some point, we all started talking about orgasms, and apparently, I pointed directly at my ex and yelled, "You won't get one from that guy!"
 y boyfriend was the manager at a cool cocktail spot, and we were there on New Year's Eve. I'd had five drinks when I started chatting with two new bar friends, and for some reason, I went into graphic detail about how talented my boyfriend is at anal sex. Turned out my two new bar friends were the head chef and general manager...to whom my boyfriend (and now husband) became a legend."
 
 */