/**
	task assigned during the first week
	- short skirt
	- show legs (short skirt alternative)
	- sexy selfie
	- porn review
	- cucumber (low slut)
	- makeup
	- flash panties
	- sexy top
	- no bra
*/

import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, choice_random, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";
import {raise_skirt_panties, placetime, task_intro, task_reaction, log_sex} from "Loop/text_extra";
import {toHSLA, nameColor, parseHSLA} from "Libraries/color/index";
import {emo, wearRandomBrutalClothes, wdesc, quickSaveOutfit, quickLoadOutfit, crew, updateDraw, violates, draw, effigy, sluttiness, makeup, wearRandomClothes, remove, wear, create, wears, showLess, showAll} from "Avatar/index";
import {add_rule, add_mile, add_timer} from "Taskmaster/scheduler";
import {rules_violation} from "Taskmaster/new_rules_check";
import {DISPLAY} from "Gui/gui"; /*TODO*/



//SHORT SKIRT 
export const short_skirt_initiation = ()=> {
	task_intro();
	add_rule("short_skirt");
		
	qaa(`Are you prepared to learn about your task for tomorrow? `);
	kat(`Yes? `);
	txt(`I was prepared for the worst. `);
	qaa(`Tomorrow you will go to the school dressed in a very short skirt. `);
	kat(`... Okay.`);
	txt(`Really? That was not so bad. Was that the worst thing they were able to came up with? Probably only the worst thing they dared to suggest me. `);
	qaa(`Actually, you are now forbidden wearing anything other than short skirts or dresses. `);
	qcc(`It's shame girls don't wear skirts more. `);
	txt(`I usually tended to wear pants or shorts, but this was hardly a dramatic change to my wardrobe. `);
	qbb(`Don't worry, the next assignment will be way harsher. `);
	kat(`Thanks for warning me.`);
	
	next(`Next. `);
};

export const short_skirt_action = ()=> {
	set.irl();
	placetime("my room", "morning");
	wearRandomClothes("noDress");
	remove("lower", PC, PC.inventory);
	emo("anoy");
	
	txt(`I woke up a bit tired. I took a quick shower and started dressing up. When I was about to pull on my jeans, I remembered their stupid task. `);
	txt(`A <i>short skirt</i> was rather an ambiguous term which could be interpreted in many ways. Even my prudish ${choice(0)} could be by some definition called <i>short</i>, at least in comparison with some floor-length ones. But it would definitely not made my tormentors happy. `);
	txt(`Then there was my ${choice(1)}. Reaching to my mid-thighs but close-fitting and nicely accenting my flawless bottom - which was something those perverts would without doubts appreciate. That would be a neutral choice. `);
	txt(`Finally, maybe I should try a bit of pandering and wore actually the very short ${choice(2)}? `);
	if(mile.slut > 2){
		txt(`Or... there was still my extremely short ${choice(3)}. It was quite shameless, always just one unwary moment from flashing my panties and I would not dare to wear it on the public under different circumstances but... `);
	}else if(hardbug){
		txt(`## Or... there was still my extremely short ${choice(3)}. It was quite shameless, always just one unwary moment from flashing my panties and I would not dare to wear it on the public under different circumstances but... `);
	}
	
	{
		//create skirts
		const temp = [];
		for(let i = 0; i < 4; i++){ 
			temp.push( create(`shortSkirt${i}`) );
		}
		
		//create effects
		const array = [{
			id: "initial",
			fce(){
				remove("lower");				
			},
		}];
		temp.forEach( (a, i) => array.push({
			label: a.name,
			fce(){
				mile.short_skirt = i + 1;
				wear(a);
			},
		}));
		effects(array);
	}			
				
	next(`To School. `);
};

export const short_skirt_reaction = ()=> {
	task_reaction(`schoolyard`);
	mile.freepass = true;
		
	txt(`The cursed trio was impatiently awaiting me in front of the school. I already regretted that I agreed with they stupid scheme. I greeted them with a nod and reluctantly approached them. `);
	switch(mile.short_skirt){
		case 1:
			mile.sub--; //WHICH ONE
			mile.slut--;
			mile.obedient -= 1;
			
			
			txt(`As I assumed they were not exactly thrilled by my outfit. `);
			qbb(`What the hell is this? `);
			kat(`A short skirt, just like you wanted. `);
			qcc(`We imagined something way shorter. `);
			kat(`How the hell should I know what you imagined? I'm not a damn mind-reader. `);
			qaa(`You better should take this seriously! `);
			kat(`I do. You wanted a short skirt, I have a short skirt!`);
			qcc(`You are trying rule lawyering! `);
			qaa(`I'm sorry @katalt, but we can't accept such half-assed tasks in the future! `);
			qbb(`Or else we'll make you regret! `);
			qaa(`Slow down, we are not threating her! But we expected more from you or ther will be consequences! You agreed with this yourself, didn't you? `);
	
			next(`I'm so sorry! I promise It won't happen again! (Meekly) `, () => mile.sub++  );
			next(`Okay. `);
			next(`Whatever. `);
			next(`I'm so sorry! I promise It won't happen again! (Sarcastically) `);
			break;
			
		case 2:
			txt(`They seemed quite pleased that I really did what they had told me. However, @qbb was not completely satisfied: `);
			qbb(`I imagined something shorter. `);
			qcc(`It is absolutely perfect. Nice skirt, @katalt. `);
			kat(`Thank you! `);
			qbb(`We shouldn't be too lenient! `);
			qaa(`I think she definitely passed our task. Good job! `);
			txt(`I shrugged, being forced to wear what they told me did not seem like something I could be proud on. As I was walking away, I could physically feel their stares nailed to my rump.`);
			next(`Classes. `);
			break;
			
		case 3:
			mile.sub++;
			mile.obedient += 1;
			
			txt(`They seemed very pleased that I really did what they had told me, hungrily stared at my exposed long legs and enthusiastically nodded. `);
			qcc(`Wow.`);
			qbb(`You decided to be our obedient slut, didn't you?`);
			txt(`I found beneath my level to respond to that. `);
			qaa(`You passed this task with flying colors. We are glad you are taking this seriously. Great job! `);
			txt(`I shrugged, being forced to wear what they told me did not seem like something I could be proud on. `);
			next(`Thank you! `);
			next(`Okay. `);
			next(`Sure, whatever. `);
			break;

		case 4:
			mile.sub++;
			mile.slut++;
			mile.obedient += 2;
			emo("imp");
			
			txt(`The seemed they cannot believe their luck! They stared at me with jaws dropped, ogling my explosed long legs and thighs, surprised I really did what they had told me. `);
			qcc(`Whoa.`);
			qbb(`You are enjoying this, aren't you?`);
			qaa(`You did not have to go so far. A miniskirt would be sufficient. `);
			qbb(`I have told you that deep inside she's just a shameless teasing slut and the only thing she needs is a little push. `);
			txt(`I just frowned, responding to that was beneath my level. `);
			qcc(`Maybe she just doesn't have any other short skirts? `);
			qaa(`She definitely does! I mean, not like I'm keeping track of her short skirts, I just remember seeing her in them because of my unmanageably good memory. `);
			qcc(`She definitely passed.`);
			qaa(`Yeah, you did! Great job. `);
			
			next(`Thank you! `);
			next(`Okay. `);
			next(`Sure, whatever. `);
			break;
			
		default:
			txt(`ERROR: NO SKIRT SET`);
			next(`Classes. `);
	}
};



//SHOW LEGS - short skirt alternative
export const show_legs_initiation = ()=> {
	task_intro();
	add_rule("show_legs");
	
	qaa(`We have a suggestion for you. `);
	qcc(`And it's less a suggestion and more a new task for you. `);
	kat(`Okay? `);
	txt(`I was prepared for the worst. `);
	qaa(`It's about your legs. `);
	qbb(`We think you should show them more. `);
	qcc(`They're perfect and it's shame to hide them under formless loose jeans. `);
	kat(`So you want me to wear only skirts? `);
	qaa(`Short skirts, yeah.`);
	qbb(`Or sexy shorts. `);
	qcc(`Until finish you the game. `);
	qbb(`However, we can renegotiate this task if you won't finish the game before the winter. We are reasonable. `);
	kat(`It won't take me till the winter! `);
	qaa(`So you're fine with this task? `);
	kat(`Well... yeah. `);
	txt(`I decided to agree before they came up with something much worse. After all, my legs <i>did</i> look good and I liked to display them. This was hardly a dramatic change of my style. `);
	next(`Next. `);
};

export const show_legs_action = ()=> {
	set.irl();
	wearRandomClothes("noDress");
	remove("lower", PC, PC.inventory);
	placetime("my room", "morning");
	emo("anoy");
	
	txt(`I woke up a bit tired. I took a quick shower and started dressing up. When I was about to pull on my jeans, I remembered their stupid task. `);
	txt(`I was banned from wearing pants and needed to show my legs, however, the rest of the details depended on my interpreation. Even my prude ${choice(0)} would show my sexy calves. But it would definitely not made my tormentors happy. `);
	txt(`Or I did not have to wear skirts, the rules allowed me to wear my ${choice(1)}. I was sure those perverts would appreciate how tightly they encased my flawless bottom. That would be a neutral choice. `);
	txt(`Finally, maybe I should try a bit of pandering and wore actually the very short ${choice(2)}. `);
	if(mile.slut > 2){
		txt(`Or... there was still my extremely short ${choice(3)}. It was quite shameless, always just one unwary moment from flashing my panties and I would not dare to wear it on the public under different circumstances but... `);
	}else if(hardbug){
		txt(`## Or... there was still my extremely short ${choice(3)}. It was quite shameless, always just one unwary moment from flashing my panties and I would not dare to wear it on the public under different circumstances but... `);
	}
	
	{
		//create skirts
		const temp = [];
		for(let i = 0; i < 4; i++){
			if(i === 1){
				temp.push(  create(`shortJeansShorts3`) );
				continue;
			}
			temp.push( create(`shortSkirt${i}`) );
		}
		
		
		//create effects
		const array = [{
			id: "initial",
			fce(){
				remove("lower");				
			},
		}];
		temp.forEach( (a, i) => array.push({
			label: a.name,
			fce(){
				mile.show_legs = i + 1;
				wear(a);
			},
		}));
		effects(array);
	}			
					
	next(`To School. `);
	//choice_random();
};

export const show_legs_reaction = ()=> {
	task_reaction(`schoolyard`);
	mile.freepass = true;
	
	txt(`The cursed trio was impatiently awaiting me in front of the school. I already regretted that I agreed with they stupid scheme. I greeted them with a nod and reluctantly approached them. `);
	
	switch(mile.show_legs){
		case 1:
			mile.sub--; //TODO WHICH ONE
			mile.slut--;
			mile.obedient -= 1;
			
			txt(`As I assumed they were not exactly thrilled by my outfit. `);
			qbb(`What the hell is this? `);
			kat(`A short skirt, just like you wanted. `);
			qcc(`We imagined something way shorter. `);
			kat(`How the hell should I know what you imagined? I'm not a damn mind-reader. `);
			qaa(`You better should take this seriously! `);
			kat(`I do. You wanted a short skirt, I have a short skirt!`);
			qcc(`You are trying rule lawyering! `);
			qaa(`I'm sorry @katalt, but we can't accept such half-assed tasks in the future! `);
			qbb(`Or else we'll make you regret! `);
			qaa(`Slow down, we are not threating her! But we expected more from you or ther will be consequences! You agreed with this yourself, didn't you? `);
			
			next(`I'm so sorry! I promise It won't happen again! (Meekly) `, () => mile.sub++  );
			next(`Okay. `);
			next(`Whatever. `);
			next(`I'm so sorry! I promise It won't happen again! (Sarcastically) `);
			break;
			
		case 2:
			txt(`They seemed quite pleased that I really did what they had told me. However, @qcc was not completely satisfied: `);
			qcc(`I think a skirt would suit her better. We should've banned shorts too! `);
			qbb(`They could be sexier than skirts! `);
			qaa(`Yeah, don't be silly! Nice shorts, @katalt. `);
			kat(`Thank you! `);
			qaa(`I think she definitely passed our task. Good job! `);
			txt(`I shrugged, being forced to wear what they told me did not seem like something I could be proud on. As I was walking away, I could physically feel their stares nailed to my rump. `);
			next(`Classes. `);
			break;
			
		case 3:
			mile.sub++;
			mile.obedient += 1;
			
			txt(`They seemed very pleased that I really did what they had told me, hungrily stared at my exposed long legs and enthusiastically nodded. `);
			qcc(`Wow.`);
			qbb(`You decided to be our obedient slut, didn't you?`);
			txt(`I found beneath my level to respond to that. `);
			qaa(`You passed this task with flying colors. We are glad you are taking this seriously. Great job! `);
			txt(`I shrugged, being forced to wear what they told me did not seem like something I could be proud on. `);
			next(`Thank you! `);
			next(`Okay. `);
			next(`Sure, whatever. `);
			break;

		case 4:
			mile.sub++;
			mile.slut++;
			mile.obedient += 2;
			emo("imp");
			
			txt(`The seemed they cannot believe their luck! They stared at me with jaws dropped, ogling my explosed long legs and thighs, surprised I really did what they had told me. `);
			qcc(`Whoa.`);
			qbb(`You are enjoying this, aren't you?`);
			qaa(`You did not have to go so far. A miniskirt would be sufficient. `);
			qbb(`I have told you that deep inside she's just a shameless teasing slut and the only thing she needs is a little push. `);
			txt(`I just frowned, responding to that was beneath my level. `);
			qcc(`Maybe she just doesn't have any other short skirts? `);
			qaa(`She definitely does! I mean, not like I'm keeping track of her short skirts, I just remember seeing her in them because of my unmanageably good memory. `);
			qcc(`She definitely passed.`);
			qaa(`Yeah, you did! Great job. `);
			
			next(`Thank you! `);
			next(`Okay. `);
			next(`Sure, whatever. `);
			break;
			
		default:
			txt(`ERROR: NO SKIRT SET`);
			next(`Classes. `);
	}
	
};



//SEXY SELFIES //TODO
export const sexy_selfie_initiation = ()=> {
	task_intro();
		
	qaa(`We were for a very long discussing what would be a suitable task for you. `);
	qbb(`We surely did! `);
	txt(`@qbb and @qcc keenly nodded. `);
	txt(`I grimaced, I could imagine the discussion, one trying to outdo the other, comming up with new peverted ideas how to humiliate poor blackmailed @katalt. I could only hope they will not dare to actually suggest any of them. `);
	kat(`Okay? `);
	qaa(`You tend to prodigally share things with the world but now we want you to share them only with us. Do you have an idea where I'm heading? `);
	kat(`No.`);
	qaa(`You'll send us a couple of your exclusive sexy selfies. `);
	kat(`Selfies? That's all?`);
	qcc(`Sexy selfies!`);
	txt(`@qcc emphasized, I shrugged:`);
	kat(`Duh, all my selfies are sexy! `);
	if(mile.a_burned){
		qaa(`Your usual general sluttiness won't be enough. We expect more! Make sexy poses...`);
	}else{
		qaa(`Sorry, but your usual beauty and grace won't be enough. We expect a bit of effort. Come up with a sexy pose...`);
	}
	qcc(`...or wear sexy clothes...`);
	qbb(`...or you don't have to wear any clothes at all!`);
	kat(`Thanks, what a wonderful suggestion. `);
	qaa(`Do you understand what we want from you? `);
	kat(`Of course I do! Sexy selfies. Fine! `);
	
	next(`Next. `);
};

//TODO REWORK
export const sexy_selfie_action = (index)=> {
	switch(index){
		default:
		case 101:
			set.irl();
			placetime("my room", "late evening");
			txt(`I was about to go to sleep but I felt like I forgot about something. And then I remembered the dumb task. Sexy selfies. There was a plenty of my hot pictures all around my social networks but they wanted me to model just for them. For a brief moment I considered reusing one of my old photos, however, I suspected they might be familiar with all the pictures I posted before. Probably jerking in front of them every night. Eww, I did not want to image that! `);
			txt(`I did not want to do it. The problem were not the pictures, it was the fact I was forced to do it and that I was forced to do it for <i>them</i>. `);
			txt(`Anyway, time to get over with it. `);
			txt(`I checked my apparance in my large vanity mirror. I did not want to waste any time or effort but sending away a picture which was not perfect felt wrong.`);
			txt(`What should I wear? Something ${choice(0)} to show I do not care? Or put on something ${choice(1)} just like they wanted? `);
			txt(`Or - and that was a pretty naughty idea - I could take a picture only in my ${choice(2)}. That would blow their minds and make them droll! Show them something they can have only on pictures and maybe in their twisted virtual fantasies but never in real life? `);
			if(mile.slut > 3){
				txt(`I could go even further and confirm them that I am indeed a shameless slut who does not even mind posing ${choice(3)} for any random guys. Thinking about their reaction to such pictures made me giggle but it was not probably the best idea. `);
			}else if(hardbug){
				txt(`## I could go even further and confirm them that I am indeed a shameless slut who does not even mind posing ${choice(3)} for any random guys. Thinking about their reaction to such pictures made me giggle but it was not probably the best idea. `);
			}

	//TODO - WEARS.NAME instead
			effects([{
				id: "0",
				label: "completely ordinary?",
				fce(){
					mile.sexy_selfie = 1;
					//wearRandomClothes();
					wearRandomBrutalClothes(0, "conservative");
					mile.task_temp = PC.upper?.name;
				},
			},{
				id: "1",
				label: "sexy",
				fce(){
					mile.sexy_selfie = 2;
					//wearRandomClothes("sexy");
					wearRandomBrutalClothes(3);
				},
			},{
				id: "2",
				label: "underwear",
				fce(){
					mile.sexy_selfie = 3;
					wearRandomClothes("underwear");
					mile.task_temp = `${PC.panties?.name || ""} and ${PC.bra?.name || ""}`;
				},
			},{
				id: "3",
				label: "topless",
				fce(){
					mile.sexy_selfie = 4;
					wearRandomClothes("underwear");
					remove("bra");
					mile.task_temp = PC.panties?.name;
				},
			}]);
			
			link(`Take selfies. `, 102);
			//choice_random();
		break;
		
	case 102:
			if(mile.sexy_selfie >= 3){
				emo("imp"); //emo??
				txt(`I undressed to my underwear, brushed my hair, tidied up the background a bit, checked the mirror and then was finally ready to make a few lewd pictures. `);
			}else{
				txt(`I changed my clothes, brushed my hair, tidied up the background a bit, checked the mirror and then was finally ready to make a few sexy pictures. `);
			}
			txt(`Hmm... what pose should I try? `);
			link(`Nonchalant cute smirk. `,103);
			link(`Confortably spreaded on my bed. `,103);
			link(`Arching back to emphasize my curves. `,103);
		break;
		
	case 103: 
			txt(`I took several pictures. They all looked amazing, which was not very surprising, I was not doing this for the fist time. `);
			if(mile.sexy_selfie >= 2){
				txt(`But maybe they were a bit too sexy? I hesitated. Maybe this was a bad idea and I should try something more subtle? Finally, I resolved to press send. `);
			}else{
				txt(`Okay. That will do. I sent them to the nerds. `);
			}
			next(`Morning. `);
		break;
	}	
};

export const sexy_selfie_reaction = (index)=> {
	task_reaction();
	txt(`Maybe I am a bit vain but I was looking for the feedback and only pretended to be annoyed when the nerds approached me. `);
	kat(`So were those pictures enough? `);
	
	if(mile.sexy_selfie === 1){
		mile.slut--;
		mile.obedient -= 1;
		if(mile.a_burned){
			qaa(`Of course not, we asked you for something sexy!`);
			kat(`I think they were sexy enough!`);
			qbb(`They absolutely were not!`);
			qcc(`I think she looked cute, wearing that ${mile.task_temp}.`);
			qaa(`You'd better should start taking this seriously, or our deal is over!`);
			kat(`Fine! `);
		}else{
			qbb(`No. `);
			qaa(`Yes, I think they were fine!`);
			qbb(`I'd say they were more elegant and artsy than sexy. `);
			qaa(`What's wrong with that? `);
			qbb(`That our assigment was completely differet. `);
			qcc(`I think she looked great in that ${mile.task_temp}!`);
			qaa(`Thank you for the pictures, @katalt. `);
			kat(`You're welcome, I guess. `);
		}
	}else if(mile.sexy_selfie === 2){
		if(mile.a_burned){
			qcc(`They were pretty sexy so yeah!`);
			qaa(`Only barely! I expected something naughtier, considering the other stuff you're not shared with strangers!`);
			qbb(`Yeah! There's no reason to hide that deep inside you're slut!`);
			kat(`Thanks for you very nice encouragement but I'm not a slut!	`);
			
		}else{
			qaa(`Yeah, they were hot! `);
			qbb(`I don't think they were not sexy enough. Should be naughtier!`);
			qcc(`I liked them. `);
			qaa(`Thank you for the pictures, @katalt. `);
			kat(`You're welcome, I guess. `);
		}	
	}else if(mile.sexy_selfie === 3){
		mile.saw_tits = true;
		mile.slut++;
		mile.obedient++;
		emo("imp"); 
		
		qaa(`Absolutely! They were so hot!`);
		qcc(`You looked so stunning in those sexy ${mile.task_temp}!`);
		qbb(`You totally unabashed, aren't you? `);
		txt(`I smirked, their pathetic praise was making me feel good. I did the right thing!`);
		kat(`You're welcome, I guess.`);
		
	}else if(mile.sexy_selfie === 4){
		mile.slut++;
		mile.slut++;
		mile.obedient += 2;
		qcc(`Of course they were! I'd never thought I'll ever get such picture from @katalt @katsur!`);
		if(mile.a_burned){
			qbb(`You enjoy this, aren't you? Doing what we tell you? Acting like a shameless slut? `);
			qaa(`What did I told you? She's a little nympho with no self-respect! `);
			//EXX
		}else{
			qbb(`You enjoy this, aren't you? Doing what we tell you? Using it as an excuse to act like a slut you are?! Just maybe the next time you should go all the way and drop even the panties!`);
			txt(`Teased me @qbb but there was a strange respect in his voice. Only one who was not excited was @qaa:`);
			qaa(`Maybe you went too far. We asked for you to be a bit sexy, not pose nude! `);
			qcc(`She wasn't completely nude, she was wearing ${mile.task_temp}.`);
			qaa(`Still, it isn't smart to share such explicit photos! `);
			qbb(`She's hot, she doesn't have to be smart! `);
		}
	}
	next(`Next. `);
};



//PORN REVIEW
export const porn_review_initiation = ()=> {
	task_intro();
	mile.porn_review = true;
	emo("shy"); 
	
	qaa(`For tomorrow we prepared a very special task. `);
	qcc(`We think you'll really enjoy it!`);
	kat(`I have my doubts. `);
	qaa(`Do you remember how you made me write your book report the last year? `);
	if(mile.a_burned){
		kat(`No, people are doing stuff for me all the time, I'm not keeping track. `);
		txt(`Of course I remembered but I hoped this answer will annoy him more. And boy it did! He seemed to be seriously seething.`);
	}else{
		kat(`I didn't make you do anything! You eagerly offered your help! I thought you just enjoy writing book reports.`);
		qaa(`Why would I enjoy writing book reports? `);
		kat(`Because you're such a nerd! `);
		qaa(`Well... I didn't!`);
		kat(`Oh, you should've said something!`);
	}
	qbb(`You probably wouldn't even be able to write a book report on your own!`);
	kat(`I absolutely would! In elementary school - before I got my boobs - I wrote all the book report on my own!`);
	qbb(`Yeah, at elementary school. But now we're talking about serious reviews. `);
	kat(`I'm able to review anything!`);
	qaa(`Great! Today we'll give you an opportunity to write a review for us. `);
	kat(`Okay, I can do that. Enjoy your 5's!`);
	qbb(`It isn't for school. We are aware nature graced you in other areas than intellectual pursuits. `);
	kat(`Thank you!`);
	txt(`I smiled at him, ingenuously trying to deflect his insult. But he only chuckled, like I just confirmed his stupid bias. `);
	qaa(`Since trying to make you read a book would be probably pointless, it's a movie.`);
	kat(`Awesome. `);
	txt(`His implication I never read a book was very offensive, I just did not understand why to bother with nationalistic 19th-century dumbasses who were rightfully forgotten by everybody, with exception of literature teachers. However, the movie did not sound so bad. It will be probably some sci-fi or fantasy schlock but hopefully it will not be very long and I even might find it enjoyable. Even better, I might be able to find reviews online and shamelessly rip them off.`);
	qcc(`We're expecting at least three pages. `);
	kat(`Three pages?!!?!`);
	qaa(`Yeah. And if we won't be satisfied, you'll have to redo them from the scratch. `);
	next(`Damn! `);
};

export const porn_review_action = (index)=> {
	switch(index){
		default:
		case 101:
			set.irl();
			placetime("my room", "late evening");
			emo("suspect"); //emo??
			
			txt(`I opened the movie which the nerds wanted me to review and began watching. The first scene involved the main heroine leaving a bathroom, her curly blonde hair was wet and she was completely bottomless. She entered a hotel room and asked her male companions whether they have not seen her panties and I began to suspect I was not watching a mainstream production. My suspicion was soon confirmed, the scene of her flirting with one of her friends quickly escalated when she dropped on her knees to give him a very graphic blowjob. `);	
			txt(`Damn! Those stupid pervs wanted me to review porn!?! Do not get me wrong, I am open-minded and I am not a prude, I watched porn before. But I was no connoisseur, I generally found videos of two people fucking too vulgar and lowbrow, there were better ways to... reach sexual satisfaction. How the hell I am supposed to write a review for this?! The production values were not bad but the plot was not hard to follow. They explored the Amazon jungle which was mostly an excused to show the main heroine in a very tight outfit consisting of a white tee, khaki pants and pith helmet. It was getting soaked with rain and sweat, becoming more and more transparent until several very improbable accidents completely tore it apart. `);
			txt(`The main heroine was at first gang-banged by her friends, then captured by wild savages and gang-banged by them. However, despite her perilous situation, she was able to befriend the daughter of the chief (by having hot lesbian sex with her). Finally, she was saved by a hunk hero and rewarded him by passionate love-making. `);
			links_mix(
				[`Skip through the movie to finish it quickly. `, 102, ()=> counter.temp = 1],
				[`Pay focus and take notes. `, 102, ()=> counter.temp = 2],
				[`Rub your pussy. `, 102, ()=> counter.temp = 3],
			);
			break;
			
		case 102:
			emo("focus"); 
			if(counter.temp === 1){
				txt(`I skipped forward, I did not have 60 minutes for this trash. I needed to watch only a few scenes to figure out the rest. `);
			}else if(counter.temp === 2){
				txt(`I reached for a notepad and pen. There were nearly no notes to take but at least holding the pen reminded me that I am not a sad, lonely pervert but I was made to watch this. `);
			}else{
				mile.slut++;
				showLess(-1, 0, 2);
				log_sex("self");
				txt(`Well... I guess the story was making me a bit horny... And when I was already forced to watch it I could as well enjoy it... I shoved my hand ${wears.skirt ? "under my " : "into my "} ${wdesc.lower}. Oh yeah! It was hard to stop and focus on my report. `);
				//TODO - high slut climaxing / low slut just rubbing? 
			}
			txt(`Describing the story was fairly simple and straightforward. But when I was done I realized that took me only three lousy paragraphs. I felt the movie did not deserve more but I had to pad the review somehow. `);	
			links_mix(
				[`<i>The main heroine...</i>`, 103, ()=> counter.temp = 1],
				[`<i>The setting...</i> `, 103, ()=> counter.temp = 2],
				//[`Rub your pussy. `, 103, ()=> counter.temp = 3],
			);
			break;
		
		case 103:
			if(counter.temp === 1){
				txt(`<i>The main heroine is a bold adventuress... </i> I assumed no coward would explore the Amazon jungle. How to say she was a shameless whore? <i>...not bound by conventional morals. </i> There I hesitated, her character was not developed much further: <i>She is a very attractive blonde with a large bosom... </i> I just started describing her body, since exploiting her body seemed to be the point of the movie. <i>... her charm helps her to get out of any dangerous situation. </i>. Oh yeah, charm. `);
			}else if(counter.temp === 2){
				txt(`<i>The setting is the Amazon jungle. The descend further into the dark heart of the jungle symbolizes descend into human debauchery... </i> I paraphrased the quote I heard somewhere, it may as well apply. Her comrades were way more eager to grope her the further they were advancing. And then there were all those mercilessly violent and sexually depraved (unfortunately enforcing pretty racist and imperialist stereotypes) savages and cannibals. `);
			}else{
				//TODO
			}
			
			links_mix(
				[`<i>My favourite scene was the brutal capture...</i>`, 104, ()=> counter.temp = 1],
				[`<i>My favourite scene was the meeting of the main heroine and the chief's daughter...</i>`, 104, ()=> counter.temp = 2],
				[`<i>My favourite scene was the climax of the movie...</i> `, 104, ()=> counter.temp = 3],
			);
			break;
			
		case 104:
			if(counter.temp === 1){
				mile.porn_review_favourite = 1;
//mile.masochist?? TODO mile.b_mas??
				mile.sub++;
				txt(`<i>... where is the main heroine surrounded by savages and after a short struggle overpowered, dragged to their village, stripped and forced to sexually satisfy them. </i> I reread what I just wrote and considered deleting it. I probably should not admit I enjoyed such a horrible and degrading scene. But it indeed impacted me the most. Being so helpless, surrounded by strangers, their hands and dicks everywhere... It had to be really exciting, I mean terrifying experience. `);
			}else if(counter.temp === 2){
				mile.lesbian++;
				mile.porn_review_favourite = 2;
				txt(`<i>... who cares for her, dressed only in a loincloth and tribal paint. They became strongly sexually attracted to each other. </i> I did not want nerds to think I am a lesbo who gets wet by watching hot girl-on-girl action, however, it was the only slow and gentle scene where the young primitive girl seemed to be more interested in the main heroine than just cumming into her busty body and moving on. And to be completely honest, I guess I found the scene a bit arousing. The contrast of bronze and dark skin when they were 69ing each other certainly had some aesthetic appeal. `);
			}else{
				mile.porn_review_favourite = 3;
				txt(`<i>... where the main heroine makes love with the adventurer who just saved her. </i> You are supposed to save the girl because that is the right thing to do, not because you expect sex in return. However, being at the main heroine's place, I would not act differently. Such a bold hunk, with a ruggedly handsome face and muscular body glistening with sweat... I would not mind him saving me several times every day if you know what I mean. `);
			}
			
			links_mix(
				[`<i>It is a cinematic masterpiece...</i>`, 105, ()=> counter.temp = 1],
				[`<i>I masturbated and orgasmed several times while watching...</i>`, 105, ()=> counter.temp = 2],
				[`<i>Only a sad virgin would enjoy a movie like this one...</i> `, 105, ()=> counter.temp = 3],
			);
			break;
			
		case 105:
			emo("imp"); 
			if(counter.temp === 1){
				mile.porn_review_end = 1;
				txt(`Damn, it was impossible to take this trash seriously and my review was getting more ironic. <i> ... and I believe the comparison to </i>Citizen Kane <i>is not an overstatement. The main heroine is a very resourceful and daring woman. She is a great role model and when I grow up, I want to be just like her. It is a travesty that this gem was ignored by the Academy. </i>`);
			}else if(counter.temp === 2){
				mile.porn_review_end = 2;
				txt(`<i>I fingered myself while imagining experiencing the same thrilling sexual encounters as the heroine. </i> I felt deeply embarrassed while typing that. However, I was sure the creeps will enjoy reading about a girl getting badly aroused after watching their twisted porn. Hopefully, so much they will ignore any other possible shortcomings of my review. `);
			}else{
				mile.slut--;
				mile.porn_review_end = 3;
				txt(`Writing the review was making me very annoyed. I opted to be completely honest and express what I thought about the movie and them: <i>The plot is stupid and unrealistic. It is deeply misogynous and its only purpose is to sexually exploit the poor lead actress. It is a complete waste of time and it's puzzling what kind of pathetic pervert would willingly watch it. </i>`);
			}
			next(`Send. `);
	}
};

export const porn_review_reaction = (index)=> {
	task_reaction();
	emo("angry");
	const pervert = [``, `a pervert who would love to get brutally gang-banged by a tribe of savages`, `a lesbian pervert who enjoys seeing girls eating each other snatches!`, `a thirsty nympho. `][mile.porn_review_favourite];
	
	txt(`Yesterday I was tired and very annoyed by their dumb task. I tried to finish it as quickly as possible and send them the first draft without much editing. Having a very bad feeling, I read my review again on my phone on my way to school and was frankly shocked I was not ashamed to write such filthy, incoherent rambling. `);
	txt(`The nerds were chuckling when I met them for the final evaluation. `);
	qaa(`Very... interesting review! It seemed you really enjoyed the movie!`);
	
	if(mile.porn_review_end === 3){
		qcc(`Even though you tried to sound like you didn't!`);
		kat(`I really didn't!`);
		qbb(`Come on, @kat! You don't have to hide anything! We knew you're secretly ${pervert}.`);
		kat(`I'm not!		`);
	}else{
		qbb(`Who would thought you're such ${pervert}.`);
	}
	
	if(mile.porn_review_end === 2){
		qcc(`And you weren't even embarrassed to state how many times and in which way you were masturbating!`);
	}else{
		qcc(`Your opinions were enlightening. `);
	}

	txt(`It seemed they were satisfied. Until: `);
	qcc(`But we have to talk about the more important thing. `);
	qaa(`Your typos. `);
	kat(`My typos!?!?!`);
	qaa(`Yeah, they were so bad they made the review hard to read. `);
	qbb(`Do you even have an autocorrect on your computer? `);
	kat(`Yeah! And it couldn't be so bad! I scanned the text before sending it! `);
	qcc(`Obviously, you weren't throughout enough! `);
	qaa(`After our deliberation, we decided to grade your review good, 3. `);
	kat(`3!?! Are you serious?!? I wasted the whole evening with that dumb thing! I deserve at least 2!`);
	
	if(mile.a_burned){
		qbb(`You should be happy for 3. Some of us thought you should get 4.`);
		txt(`@qbb glanced at @qaa who was clearly biased against me. `);
		qaa(`Yeah! You don't deserve a better grade! And stop throwing a tantrum! `);
	}else{
		qbb(`Well, you don't. And stop throwing a tantrum! `);
	}
	kat(`I'm not throwing a tantrum! Fuck you! This is the last porn review I have ever written for you! `);
	next();
};



//CUCUMBER 
export const cucumber = (index)=> {
	switch(index){
		default:
		case 101:
			set.irl();
			emo("shy");
			placetime("parking lot next to mall");
			txt(`I met with the nerds after the classes. `);
			kat(`So why did you want to meet me outside?`);
			qaa(`We want you to go to the shop and bring us several items. `);
			kat(`What do you want?`);
			qaa(`1. condoms, 2. lube, 3. huge cucumber, 4. bottle of vodka.`);
			kat(`What!? But people will think that...`);
			qaa(`...exactly!`);
			qcc(`No porn magazine as I suggested?`);
			qaa(`No, we decided to be more subtle. `);
			kat(`Oh my! So lube, condoms, vodka and cucumber?`);
			qcc(`<i>Huge</i> cucumber. `);
			qbb(`The biggest and girthiest they have. `);
			link(`Shopping. `, 102);
			break;
		
		case 102:
			emo("help");
			txt(`They waited for me outside while I went on the scavenger hunt. `);
			txt(`I was not very happy about the place @qaa chose. The supermarket was two stops from our tower block. I often went there, alone or with my parents and remembered several cashiers. `);
			txt(`I quickly found lube, condoms and the cheapest vodka. Then I went to the vegetable section and hesitated which cucumber should I chose. Were they serious it has to be the biggest one? I had to grab one, the longer I was lingering and picking, the weirder it looked. `);
			link(`Grab the very long but thin one. `, 103, ()=> mile.cucumber = 1);
			link(`Grab the short and thick one. `, 103, ()=> mile.cucumber = 2);
			link(`Grab the big but slightly curved one. `, 103, ()=> mile.cucumber = 3);
			break;

		case 103:
			emo("unco");
			present([1,"Cashier"]);
			txt(`Damn! Like every afternoon there was a long queue. I did not understand why they could not open more checkout! `);
			txt(`I was getting nervous. The young woman with a little kid in front of me and the middle-aged guy behind me could clearly see what was in my basket. He was mindlessly staring, there was no way to tell if he was lost in random unrelated thoughts or whether he imagined me violently shoving the cucumber in my pussy. The woman just glimpsed, she either did not connect the items or was too ashamed or offended to pay me more attention. `);
			txt(`I awkwardly put the items on the belt. The cashier was a young guy. He scanned the items without thinking until he noticed the condoms and very lightly smirked. Oh yeah, a pretty girl is buying condoms, deal with it, creep. Then he paused and without a word looked straight at me. I felt judged, my blushing was giving away this was not a random combination of items. I just hoped he realized the truth that this was just an embarassing bet and I definitely did not intend to use the cucumber to masturbate. `);
			kat(`What!?`);
			txt(`I finally snapped. `);
			npc(1,`Oh, sorry, your ID?`);
			//kat(``);
			kat(`Oh, right!`);
			txt(`I hastily searched for my ID, attracting more attention than I would prefer. I paid with a phone and rushed outside. The nerds loudly laughed when they saw me. `);
			txt(`I gave them the stuff I purchased. The way they examined the cucumber and commented its shape was not making me very comfortable. `);
			qaa(`So this is what you like? `);
			qcc(`Do you have many erotic experiences with phallically-shaped vegetable?`);
			kat(`NO!`);
			txt(`@qbb threw me the cucumber back. `);
			qbb(`You can keep it! In the case you felt lonely. `);
			if(mile.sub < 3){
				txt(`I threw him the condoms. `);
				kat(`In the case... wait, nevermind, I don't think you'll need them. `);
			}else{
				kat(`I wouldn't need a cucumber! I mean... I meant I don't feel lonely, not that I... `);
			}
			next();
			break;
	}
}



//MAKEUP
export const makeup_initiation = ()=> {
	task_intro();
	set.meta(); //TODO - necesary??
	qaa(`We wanted to ask...`);
	kat(`What? `);
	qaa(`Why don't you wear nicer makeup to school? `);
	kat(`I'm always wearing very nice makeup. `);
	qbb(`You could give it a bit more effort. `);
	kat(`I look good and I don't need to redo my whole face every morning.`);
	qcc(`It's a bit too subtle. `);
	kat(`That's the point. More is less, that's what tramps like @eva or @zan are unable to comprehend. You overdo it and you look like a circus clown or tawdry hooker. `);
	qaa(`And that's what we want!`);
	kat(`What? Me looking like a circus clown? `);
	qbb(`No, you looking like a tawdry hooker! `);
	kat(`What?!?!`);
	qcc(`That's your next task. `);
	if(mile.a_burned){
		qaa(`Since you're a whore you should also look like one!`);
	}else{
		qaa(`We thought you might look good with a bit heavier, sluttier makeup. `);
	}
	kat(`So you want me to go to school with heavier makeup?`);
	qbb(`With extremely slutty makeup.`);
	if(mile.a_burned){
		qaa(`The more the better!`);
	}else{
		qaa(`It doesn't have to be extremely slutty, a bit slutty would be enough.`);
		qcc(`But you of course could go with extremely slutty if you want to. `);
	}
	next();
};


export const makeup_action = (index)=> {
	mile.makeup = true;
		
	if(index === 101){
		placetime("bathroom", "morning");
		txt(`Oh no! I almost forget about about the  <i>slutty</i> makeup. I was working quickly and nimbly, I hesitated only when I was picking the appropriate lipstick and eye shadows. Something noticeable but not too far. `);
		link(`Makeup. `, 102, ()=>{}, ()=> DISPLAY.edit_avatar(PC, "makeup") );
	}else if(index == 102){
		txt(`I anxiously checked my reflection in the mirror. Not bad, I had to admit. A bit gaudier than I was used to but pretty sexy. `);
		link(`Go to school. `, 103);
	}else{
		placetime("schoolyard", "before classes");
		add_mile(3, "sas_makeup", true); //in three days sas will wear makeup too
	
		txt(`I smiled at the nerds. I was pretty pleased that my extra seductive look seemed to make them nervous. `);
		kat(`Fine? `);
		if(mile.a_burned){
			qaa(`Not bad!`);
		}else{
			qaa(`You look lovely!`);
		}
		qcc(`Awesome! `);

		const lips = PC.apex.irl_lips ? toHSLA(PC.apex.irl_lips) : {};
		if(lips.h < 10 || lips.h > 340){
			qbb(`I imagined something better. Like really dark eyeshadows or some crazy lipstick like green or purple or something like that!`);
			kat(`Anybody with good taste would tell you those are horrible ideas!`);
		}else if(lips.h < 20 || lips.h > 260){
			qbb(`I imagined something better. Like really dark eyeshadows or some crazy lipstick like green or blue or something like that!`);
			kat(`I think ${nameColor(lips)} lipstick is crazy enough! `);
		}else{
			qbb(`I imagined something different. Like really dark eyeshadows. But otherwise pretty cool!`);
			txt(`He appreciated my ${nameColor(lips)} lipstick.`);
		}
		next();
	}
};



//FLASH PANTIES
export const flash_panties = (index)=> {
	const pantycol = (()=>{
		if(!PC.panties) return "black";
		const a = PC.panties.name.split(" ");
		a.pop();
		return a.join(" ");
	})();
	
	switch(index){
		default:
		case 101:
			mile.sub < 2 ? emo("angry") : emo("anoy"); //emo??
			mile.flash_panties = true;
			mile.saw_panties = true;
			placetime(`locker room`);
			txt(`The nerds did not contact me with another demand. I hoped that maybe they had enough messing with me. However, my hopes were vain. The trio awaited me in school basement locker room. `);
			kat(`Good morning? `);

//TODO TEST			
			if(violates.short_skirt || violates.sexy_top || violates.no_bra || violates.show_legs){
				rules_violation(true);
				
				qaa(`Good morning... @kat... `);
				kat(`What?`);
				const a = [];
				if(violates.short_skirt) a.push(`short skirts`);
				if(violates.show_legs) a.push(`short skirts or shorts`);
				if(violates.sexy_legwear) a.push(`sexy legwear`);
				if(violates.sexy_top) a.push(`sexy tops`);
				
				if(a.length){
					qcc(`You're supposed to wear ${and(a)}!`);
					if(violates.no_bra){
						qaa(`And it seems to me you're wearing a bra! `);
					}
				}else{
					qcc(`You're not allowed to wear bras!`);
				}
				
				kat(`Well...`);
				qaa(`You agreed you'll follow the rules but you're unable to keep your word? `);
				kat(`Of course, I am but your rules are dumb!`);
				qaa(`We wanted to give you an easy task but now we see we'll have to punish you with something harsh. `);
				qbb(`You have only yourself to blame!`);
				kat(`W... what do you want me to do!??`);
				qbb(`Show us your panties!`);
						
			}else{		
				qbb(`We wondered what kind of panties you are wearing today. `);
				kat(`What?!?! That is none of your business!!! `);
				txt(`I was completely taken aback by his sudden brazen demand. `);
				qcc(`Well, to explain, we decided that this is your next task. To flash us your panties. `);
			}

			kat(`Are you fucking serious!?! I'm not doing that! `);
			qbb(`You're too shy and scared? `);
			kat(`I'm not shy nor scared! `);
			qaa(`Or you found out that you'll never be able to finish the game and want to cancel our deal? `);
			kat(`No! I'm doing great! I'm just not going to flash my panties to random perverts! `);
			qaa(`Funny how you don't mind prancing around in those tini bikini of yours in front of complete strangers! `);
			kat(`There's obviously difference between a bikini bottom and panties! `);
			qbb(`There's absolutely no difference! `);
			qcc(`There's no difference! `);
			kat(`The fabric and the social contex are completely different! `);
			link(`Find a compromise. `,102);
			
			break;
			
		case 102:
			emo("shy");
			if(sluttiness().details.panties){
				kat(`I believe you can already get a pretty good idea... `);
				txt(`I pointed out, my clothes were not covering too much and the panties were peeking out a little. `);
				qbb(`We appriciate your slutty clothes but that won't be enough! `);
				kat(`They aren't so slutty. `);
				qaa(`They are! `);
				kat(`Well, maybe a bit slutty. `);
				qcc(`When you're not ashamed to show half of them to everybody why are you ashamed tho show the whole thing just to us? `);
				kat(`Fine! Just shut up, your absurd logic makes my head hurt! `);
			}else{
				kat(`I can vividly describe my panties to you?`);
				qcc(`So? `);
				txt(`I cringed: `);
				kat(`Well, they are ${pantycol}...`);
				qcc(`That wasn't very vivid! `);
				qbb(`She's so evasive I'm starting to believe she isn't wearing any panties at all! `);
				kat(`Are you crazy?!? Of course I'm wearing panties! `);
			}
			qaa(`Then show us! `);
			kat(`Right here, right now?!? `);
			if(mile.a_burned){
				qaa(`Yeah! Right here, right now!!! `);
				qcc(`Really? Somebody can see us here! `);
				qbb(`You say it like its a bad thing! `);
			}else{
				qaa(`Well, it doesn't have to be right here, of course. `);
			}
				
			if(mile.slut < 2){
				txt(`I was horrified by their shameless, depraved task but they were adamant. I looked around. I was reluctant to do it in the locker room even though we were alone. Anybody could just walk in and I would not survive to be seen flashing my underwear to those nerds! `);
			}else{
				txt(`I looked around. I was reluctant to do it in the locker room even though we were alone. Anybody could just walk in and I would not survive to be seen flashing my underwear to those nerds! `);
			}
			txt(`On the other hand, admitting that looked like admitting weakness. I needed to stay calm and careless, to show them I am not fazed by their blackmail.`);
			if(mile.slut > 3){
				txt(`And it sounded pretty naughty.`);
			}
			
			link(`Do it right here and quickly get over it. `,103, ()=> counter.temp = true);
			link(`Go to some more secluded place. `, 103, ()=> counter.temp = false);
			break;
			
		case 103:
			emo("unco");
			txt(`I quickly ${raise_skirt_panties()}, just to give them the glimpse of the ${pantycol} fabric. ${counter.temp ? "I was anxious, anybody could come at any moment. " : "I hoped that finding a safe place would be better but being tighly packed in a small maitenance closet was making everything even weirder. "}` );
			qaa(`Not good enough. `);
			txt(`I angrily groaned and ${raise_skirt_panties()} again. They leaned closer to creepily examine my panties. `);
			
			if(wears.sexyPanties){
				mile.saw_sexy_panties = true;
				qcc(`Cool! `);
				qbb(`Nice! `);
				if(mile.a_burned){
					qaa(`As slutty as I expected! `);
				}else{
					qaa(`Lovely! `);
				}
				txt(`They appreciated my ${wdesc.panties}. I liked to wear sexy underwear, it usualy made me feel more confident. But not at that particular moment. `);
				qcc(`Would you mind if I... `);
				txt(`I had to slap away the hand of mesmerised @qcc and quickly readjusted my clothes. `);
				kat(`What the hell! Of course I would mind! `);
				txt(`The other laughed. `);
				qbb(`Do you always wear such luscious panties or were we just lucky? `);
				kat(`That's none of your bussiness! `);		
			}else{
				mile.saw_sexy_panties = false;
				qbb(`I imagined something better. `);
				if(mile.a_burned){
					qcc(`I think they are cute. `);
					qaa(`They are too boring and prude. `);
				}else{
					qaa(`I think they are cute. `);
					qcc(`They are too boring and prude. `);
				}
				txt(`I was offended by their dismissal! Not only I was forced to stand in front of them just in my ${wdesc.panties} but it was not good enough for them?!? `);
				kat(`I really didn't expect showing them anybody. You didn't expect I wear to school a sexy lacy lingerie every day? `);
				qcc(`Well, kinda. `);
				qaa(`We can do something about that. `);
				kat(`What do you mean?!`);
				qaa(`You don't have to be concerned about that right now. `);
			}
			txt(`Needles to say, I did not felt extremely comfortable when I was forced to discuss my choice of underwear with them. I was was irksome and humiliating and I was very happy when everything was over. `);
			
			next(`Run away. `, showAll);
			next(`Walk away. `, showAll);
			break;
	}	
};



//SEXY TOPS
export const sexy_top = (index)=> {
	task_intro();
	emo("help");
	add_rule("sexy_top");
	mile.sexy_top = true;
	
	if(!mile.freepass && (violates.show_legs || violates.short_skirt) ){
		rules_violation(true);
		qaa(`Important question: why don't you wear a short skirt like you're supposed to? `);
		kat(`Well...`);
		qbb(`Trick question - we don't care why!`);
		qaa(`Do you want to end our deal? `);
		kat(`...no. `);
		qaa(`Then you have to uphold your part.`);
		qcc(`And wear only short skirts. ${violates.no_bra ? "And no bras. " : ""}`);
		qaa(`And sexy tops. `);
		kat(`Sexy tops? `);
		qaa(`Yeah, from tomorrow you're allowed to wear only sexy tops.`);
	}else{
		qaa(`Don't you wonder what task we prepared for tomorrow? `);
		kat(`Yeah, I fear how dumb it is. `);
		qaa(`Well, we already improved your lower part so now we have to do something with your upper part. `);
		kat(`What do you mean by my upper part? `);
		qbb(`Your top.`);
	}
	kat(`What's wrong with my ${wdesc.upper}?`);
	if(wears.sexyTop){
		qcc(`Nothing! It looks lovely! `);
		qaa(`And we want you to wear similar ones. So you see, it isn't a difficult task. `);
	}else{
		qcc(`It's bland and boring. You have a perfect figure, you don't have to be shy to show it!`);
	}
	qbb(`Yeah, we want you to wear sexy tops, showing a bit more skin. With deeper cleavages or bare midriff. `);
	if(ext.slut > 0){ //TODO SLUT
		qaa(`It isn't such a difficult task, is it? We know you have plenty of those!`);
	}
	next(`Fine! `);
	
};

export const sexy_top_reaction = (index)=> {
	switch(index){
		default:
		case 101:
			task_reaction();
			emo("shock");
			if(violates.sexy_top){
				//TODO other clothes violations 
				rules_violation(false); //false - no clothes warning because immediate punishmet? 
				
				txt(`I thought I look pretty cute but my ${wdesc.upper} was not sexy enough for them. `);
				if(mile.clothes_warning < 1){
					qaa(`You didn't understand our task?  `);
					qcc(`I thought we were clear. `);
					qbb(`She just intentionally messing with us! `);
				}else{
					qaa(`You broke our rules again! `);
					qbb(`Are you doing this on purpose? `);
					qcc(`They aren't so difficult, are they? `);
				}
				qaa(`What should we do? `);
				kat(`Maybe... `);
				qaa(`Nobody was asking you! `);
				qcc(`We should punish her somehow. Or.. `);
				qbb(`Maybe there's a way! `);
				txt(`I twitched when he suddenly brought out a razor-sharp switchblade. `);
				link(`NO!`, 102);
			}else{
				if(mile.slut < 1){
					txt(`I approached the nerds. I it was making me a bit uncomfortable when they just creepily stared at my chest, evaluating my ${PC.upper.name}. Wearing such clothes to school just seemed wrong. `);
				}else{
					txt(`I approached the nerds and for the moment just let them creepily stare at my chest, evaluating my ${PC.upper.name}. `);
				}
				qaa(`We're happy to see you take your task seriously! `);
				qbb(`Pretty sexy ${wdesc.upper}! `);
				if(!PC.bra){
					qcc(`Especially with no bra! `);
				}else{
					qcc(`Yeah! Looks lovely! `);
				}
				qbb(`Althougt it might be even better. `);
				qcc(`Now when you're saying it... I've seem some really cool designs... `);
				if(!mile.a_burned){
					qaa(`I think it's perfect! What more would you want! `);
				}else{
					qaa(`Yeah. We let you pass but the next time wear something sluttier! `);
				}
				next();
			}
			break;
			
		case 102:
			emo("help");
mile.stat--; //TODO 
			kat(`W... what are you going to do! A.. are you even allowed to bring such things into school?`);
			qbb(`I'm going to fix your @upper! `);
			qcc(`That's a great idea! `);
			kat(`No! You'll ruin it!`);
			qaa(`Maybe that makes you take our tasks seriously!`);
			qbb(`I strongly suggest you not struggle and stay perfectly still, otherwise, I might cut you. `);
			aly.acf(`Please be careful, we don't you to accidentally hurt her.`);
			qbb(`Accidentally?`);
			txt(`I decided to follow his advice. I did not trust @qbb when he was holding a knife. It was an absolute massacre and my ${PC.upper.name} was totally destroyed. `);
			if(PC.upper.dad === "wTee" || PC.upper.subtype === "tee"){
				PC.upper.waistCoverage = 1;
			}
			if(PC.upper.armCoverage){
				PC.upper.armCoverage = 0;
			}
			if(PC.upper.cleavageCoverage){
				PC.upper.cleavageCoverage += 0.2;
			}
			
			if(wears.dress){
				if( (violates.show_legs || violates.short_skirt) ){
					qcc(`Isn't her dress a bit too long? `);
					qaa(`Definitely! That should also be fixed! `);
					qbb(`I'm on it!  `);
					kat(`You jerks! `);
					if(PC.upper.legCoverage) PC.upper.legCoverage = 0.2;
				}
				
			}else{
				if(PC.upper.legCoverage){
					PC.upper.legCoverage -= 0.2;
				}
			
				if( (violates.show_legs || violates.short_skirt) && wears.skirt){
					qbb(`When I'm already doing it, let's fix your skirt too! `);
					kat(`Nooo! You already ruined my @upper!`);
					qaa(`You have only yourself to blame, @katalt. `);
					
					if(PC.lower.legCoverage) PC.lower.legCoverage = 0.2;
				}
			}
			
			
			updateDraw(PC);
			next();
			break;
	}
};	



//NO BRA
export const no_bra = (index)=> {
	task_intro();
	mile.no_bra = true;
	add_rule("no_bra");
	emo("shy");
	
	qaa(`We have a new quest for you. `);
	kat(`Okay? `);
	qaa(`So we were discussing your breasts. `);
	kat(`Somehow I'm not surprised. `);
	qcc(`And how great they are. `);
	if(mile.sub > 0){
		kat(`Thanks? `);
		txt(`It was a crude compliment but I took it. I enjoyed to be reassured I look hot. `);
	}else{
		kat(`I know.`);
		txt(`I shurgged, it was pretty obvious and understandable those dorks were captivated by my tits. `);
	}
	
	if(PC.bra){
		qaa(`And how they spent every day so tightly imprisoned in your bra. `);
		txt(`They concern for my well-being was surprising. `);
		kat(`Well, it isn't so bad...`);
		qcc(`You should let them free! `);
		kat(`What?!?`);
		qcc(`That's your next task.`);
		qbb(`Tomorrow you'll go to the school without a bra and you're not allowed to wear bras until you finish with the game. `);
		kat(`I don't want to look like a slut! `);
		qcc(`That's a fairly misogynous way of thingking! There's nothing slutty about freeying your nipples!`);
		qbb(`And we're sure you'll quickly get used to dress up like a slut! Or a slutty feminist, or whatever!`);
		kat(`But...`);
		qaa(`Don't worry! You'll be be able to wear whatever you want again once you'll finish the game!`);
		
	}else{
		qaa(`And how great it is you don't bother with bras!`);
		qcc(`And how daringly you show nearly everything!`);
		txt(`That made me squirm a little. Indeed, my nipples were faintly poking through the fabric. However, I went braless because it was more comfortable, not because I wanted to be ogled by perverts. `);
		qbb(`And we want to it to stay this way. `);
		qaa(`So our next task is that from now on you're not allowed to wear bras! `);
		txt(`My first reaction was to immediately protest but that might seem hypocritically, I (visibly) had no troubles with not wearing bras. `);
		kat(`Fine!`);
		
	}
	
	next();
};

export const no_bra_reaction = (index)=> {
//	switch(index){
//		default:
//		case 101: 
			task_reaction();
			mile.freepass = true;
			emo("shy");
			
			const skirt = !(violates.show_legs || violates.short_skirt);
			const bra = !wears.bra;
			const top = !violates.sexy_top; //both "wears sexy top" and "doesn't have to wear a sexy top"!
			
			
			if(bra && skirt){
				if(top){
					txt(`Needles to say, the nerds were pretty satisfied and could not take their eyes off from my chest, my nipples almost lightly visible nipples under the thin fabric. `);
					if(mile.a_burned){
						qbb(`You look great and slutty, @kat!`);
						qaa(`No surprise!`);
					}else{
						qaa(`You look fantastic, @kat!`);
						qbb(`And slutty!`);
					}
				}else{
					txt(`The nerds could not take their eyes off from my chest, my nipples almost lightly visible nipples under the thin fabric. However, they were not fully satisied: `);
					qaa(`What about your top? We tould you to wear something sexy!`);
					kat(`This isn't sexy enough for you? `);
					qcc(`No. You're supposed to follow our rules exactly! `);
					qbb(`Tomorrow you better should wear no bra and sexy top or there will be cosequences!`);
					qaa(`But you at least nailed the no bra part, good job!`);
				}				
				qcc(`This look is great! You don't need any bras, just like in Star Wars! `);
				kat(`Great. It's such a delight to let you decide what underwear I should or should not wear. `);
				qbb(`Don't play touchy, we know you're enjoying this! `);
				kat(`I'm not! Can I go now? You already waste enough of my time!`);
				txt(`For a few more seconds which felt like a creepy eternity they fixedly stared at my tits.`);
				if(mile.a_burned){
					qaa(`Sure, you can go. `);
				}else{
					qaa(`Sure, you can go. Have a nice day!`);
				}

			}else if(skirt){
				//mile.obedient -= 1;
				rules_violation(false); //punishment imminent
				
				if(top){
					txt(`The nerds appreciate my hot @upper but were not very happy about my @bra. `);
					qaa(`What was you task for today, @kat? `);
					kat(`Well... I thought since I'm already wearing this hot top...`);
					//qaa(`And? It was <i>and</i> not <i>or</i>!`);
				//	kat(`Well... not wearing a bra? `);
					qbb(`You're not able to comprehend one simple task? `);
				//	kat(`I am!`);
					qaa(`You have to follow all our tasks! `);
					qbb(`Take it off!`);
					txt(`@qbb sharply snapped. `);
					kat(`What?!`);
					if(mile.a_burned){
						qaa(`STRIP YOUR BRA!`);
					}else{
						qaa(`Remove you bra. I'm sorry @kat but you have to follow our assignments exactly. Or if you would prefer to give up? `);
						kat(`...no. `);
					}
				}else{
					txt(`The nerds were not very happy. `);
					qaa(`What was you task for today, @kat? `);
					kat(`Ehhh...`);
					qbb(`We'd like to know whether you're such a bimbo you're not able to comprehend one simple task or whether you're just begging us to be punished. `);
					kat(`Well, neither! `);
					qbb(`Take it off!`);
					txt(`He pointed at my chest. `);
					kat(`My top? You can't be serious! `);
					qaa(`No, just your bra. We'll be nice, you can keep your ${wdesc.upper} for the rest of the day. `);
					txt(`Nice! They knew I would refuse if they forced me to do that! But a few days without a bra were not a reason to give up. `);
				}

				txt(`I reluctantly slipped my hands under my ${wdesc.upper} and unhooked the bra while they were watching. Then I took it off without removing removing my top. @qcc immediately grabbed it. `);
				qcc(`So you wouldn't be tempted to put it back. `); 
				qaa(`He'll give it to you back once you finish the game. `);
				qbb(`Nice tits! You don't even need wearing bras with tits like this? `);
				kat(`You're such jerks!`);
				qbb(`You should be grateful we won't punish you for failing our task!`);
				
			}else if(!skirt){
				/*
				mile.obedient -= bra ? 1 : 2;
				mile.clothes_warning++;
				*/
				rules_violation(true, bra ? 1 : 2); 
				
				txt(`The nerds were not very amused. `);
				qbb(`What the hell? `);
				qaa(`You're completely ignoring our task! `);
						
				qcc(`Did you already forgot you are not allowed to wear ${wears.skirt ? "long skirts" : "pants"}? `);
				qbb(`Are you really so dumb? `);
				if(mile.sub < 1){
					kat(`I'm not dumb!`);
					qbb(`You are not even able to follow the simples instructions! `);
				}
				
				
				qbb(`We should tell her to take off her ${wdesc.lower} and confiscate ${wears.skirt ? "it" : "them"}. `);
				if(mile.a_burned){
					qaa(`That is a brilliant idea. `);
					qcc(`But what would she do in the school just in her underwear? `);
					qaa(`That would be her problem, not ours. `);
				}else{
					qaa(`And leave her just in her panties? `);
					qcc(`But what would she do in the school just in her underwear? `);
					qbb(`That would be her problem, not ours. `);
				}
				qcc(`But people would probably notice... `);
				qaa(`Yeah, you're right... We will be extremely nice and let you pass this time. But the nex one...`);
			
				if(!bra){				
					qbb(`Take your bra it off!`);
					txt(`@qbb sharply snapped. `);
					kat(`What?!`);
					if(mile.a_burned){
						qaa(`STRIP YOUR BRA!`);
					}else{
						qaa(`Remove you bra. I'm sorry @kat but you have to follow our assignments exactly. Or if you would prefer to give up? `);
						kat(`...no. `);
					}
					txt(`I reluctantly slipped my hands under my ${wdesc.upper} and unhooked the bra while they were watching. Then I took it off without removing removing my top. @qcc immediately grabbed it. `);
					qcc(`So you wouldn't be tempted to put it back. `);
					qaa(`He'll give it to you back once you finish the game. `);
					qbb(`Nice tits! You don't even need wearing bras with tits like this? `);
					kat(`You're such jerks!`);
					qbb(`You should be grateful we won't punish you for failing our tasks!`);
				}
			}
			
			if(!bra) remove("bra"); //strips bra
			next();
//	}
};

export const no_bra_consequences = (index)=> { //TODO - would better fit to sexy top
	placetime("canteen", "noon");
	emo("unco");
	
	if(mile.slut < 3){
		txt(`I was so embarrassed and anxious when I walked through the school corridors without wearing any bra. I felt like my bralessness was extremely obvious to everybody. Why I just did not ignore their stupid task? `);
	}else{
		txt(`The rest of day was not much better. My outfit definitely raised some eyebrows and not only eyebrows if you know what I mean. `);
	}
	
	txt(`During the lunch, as usually, I was sitting with @eva and @sas. `);
	eva(`Nice outfit. `);
	txt(`@eva snidely noted. WHAT A BITCH! How she dares, considering the trash she is usually not ashamed to wear?!? `);
	kat(`Thank you. I really like your shoes. `);
	txt(`Her face went pale but she did not say anything, just forcedly smiled. `);
	sas(`Are you going to Bronze tonight? `);
	kat(`No, I don't have time. I need to... study. `);
	eva(`Again? You told us you studied for the whole weekend? `);
	kat(`Yeah... but... I need to study even more because... exams. `);
	txt(`Those damn nerds were slowly, gradually destroying my social life! `);
	next(`Afternoon. `);	
};


