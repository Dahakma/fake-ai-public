import * as intro from "./other/intro";
import * as skip from "./other/skip";
import * as random from "./other/random";
import * as chairman from "./other/chairman";
import * as classes from "./other/classes";
import * as combat_simulator from "./other/combat_simulator";
import * as surgery from "./other/surgery";
import * as home from "./other/home";
import * as work from "./other/work";

import * as week_1 from "./tasks/week_1";
import * as week_2 from "./tasks/week_2";
import * as week_3 from "./tasks/week_3";
import * as week_4 from "./tasks/week_4";
import * as week_5 from "./tasks/week_5";
import * as fridays from "./tasks/fridays";
import * as piercings from "./tasks/piercings";
import * as tattoo from "./tasks/tattoo";
import * as poledancing from "./tasks/poledancing";
import * as violation from "./tasks/violation";
import * as swimming from "./tasks/swimming";

import * as aaa from "./npcs/aaa";
import * as bbb from "./npcs/bbb";
import * as ccc from "./npcs/ccc";
import * as ayy from "./npcs/ayy";
import * as sas from "./npcs/sas";
import * as zan from "./npcs/zan";
import * as nerds from "./npcs/nerds";
import * as week_recap from "./npcs/week_recap";

import * as awakening from "./hubs/awakening";
import * as afternoon from "./hubs/afternoon";
import * as weekend from "./hubs/weekend";
import * as current_end from "./hubs/current_end";
import * as shops from "./hubs/shops";

import * as death from "./game/death";
import * as feyd from "./game/feyd";
import * as goth from "./game/goth";
//import * as goth from "./game/goth";
import * as voyage from "./game/voyage";
import * as battleship from "./game/battleship";
import * as ship from "./game/ship";
import * as no_virtual_game from "./game/no_virtual_game";

export const story = {
	...intro,
	...skip, 
	...random,
	...chairman,
	...classes,
	...surgery,
	...combat_simulator,
	...home,
	...work,
	
	...week_1,
	...week_2,
	...week_3,
	...week_4,
	...week_5,
	...fridays,
	...piercings,
	...tattoo,
	...poledancing,
	...violation,
	...swimming, 
	
	...awakening,
	...afternoon,
	...weekend,
	...current_end,
	...shops, 
	
	...aaa,
	...bbb,
	...ccc,
	...ayy,
	...sas,
	...zan,
	...nerds, 
	...week_recap, 

	...death,
	...feyd,
	...goth,
	...voyage,
	...battleship,
	...ship,
	...no_virtual_game,
};

 