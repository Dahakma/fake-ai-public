/**
	cosmetical transformation - breast expansion  
*/

import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, tom, zan, ven, mik} from "Loop/text";
import {mol, maj, ped, bio, dur} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";
import {placetime, task_intro, task_reaction} from "Loop/text_extra";

import {ra} from "Libraries/random";
import {stringifyHSLA} from "Libraries/color/index";
import {emo, draw, rollUpSkirt, wdesc, removeActual, removeEverything, update, updateDraw, effigy, allClothes, crew, wearRandomClothes, remove, wear, create, wears, showLess, showAll, violates, sluttiness, createWear, quickSaveOutfit, quickLoadOutfit  } from "Avatar/index";
import {active_timeslot, next_event, add_rule, free_today, add_workday} from "Taskmaster/scheduler";
import {DISPLAY} from "Gui/gui";
import {cloneFigurine} from "Avatar/canvas/body";
import {rules_violation} from "Taskmaster/new_rules_check";
import {masochist} from "Virtual/text";
import {money} from "Data/items/prices";




//ASDFOAWEF
export const clinic_boobjob = (index) => {
	function pic_a(index){
		link(`Clothed. `, index, ()=> counter.pic = 1);
		link(`Underwear. `, index, ()=> counter.pic = 2);
		link(`Topless. `, index, ()=> counter.pic = 3);
	}
	function pic_b(guy = true){
		if(counter.pic === 3){
			if(guy && mile.slut < 10) mile.slut++;
			showLess(0, -1);
		}else if(counter.pic === 2){
			if(guy && mile.slut < 1) mile.slut++;
			showLess(1, -1);
			if(!wears.bra) createWear("sexyPushUpBra");
		}else{
			if(guy && mile.slut > 12) mile.slut--;
			showAll();
		}
	}

	switch(index){
		default:
		case 101:
			set.irl();
			placetime(`Cosmetic Clinic`);
			present(
				[1, `Doctor`, ["whiteTightTee", "whitePants", "whiteSneakers"]], //TODO
				[6, `Receptionist`, ["bimboThong", "sleevedTubeTop", "miniSkirt", "polyStockings", "bimboLoops", "multiNecklace","bellyPiercing","nippleBars"], "bimbo"], //TODO Pamela
			);
			
			{
				const intro = `Every day when I went from the school to the tram station I walked by a small cosmetic clinic. And today I decided to give it a visit. `;
				const outro = ``; //`And I already learned that if I did not understand something, the best course of action was to ask somebody who did. `;
				if(mile.c_boobjob === 1){
					txt(`${intro} I was completely satisfied with my body and did not want to change it in any way but the discussion with @qcc made me curious. ${outro}.`);
				}else if(mile.c_boobjob === 2){
					txt(`${intro} I was not convinced my body needs any cosmetic augmentation but the discussion with @qcc made me curious. ${outro}.`);
				}else{
					txt(`${intro} Having bigger breasts might be interesting but I did not intend to get actually augmented, I was just curious about the process. ${outro}.`);
				}
			}
			
			npc(6,`Good afternoon! `); //TODO TIME DAY!!!
			txt(`I was instantly greeted by a smiling receptionist. Her chest was truly enormous. Was she supposed to serve as a living advertisement? That was really tacky! Or maybe free augmentations were an employee benefit? `);
			kat(`Hello! `);
			npc(6,`You're very lucky, doctor Nagy has time for your consultation right now! `);
			kat(`Ehh... I just wanted to ask if you don't have any leaflets or booklets or something... `)
			npc(6,`Don't worry! Doctor Nagy will explain everything! `);
			txt(`She basically pushed me into his doors. `);
			link(`Consultation with the doctor. `, 102);
			break;

		case 102:
			//mile.implants = 2; //TOOD
			emo("shy");
			npc(1,`Good afternoon, I'm doctor Milan Nagy.`);
			kat(`Good afternoon. I'm @katalt @katsur. `);
			
			npc(1,`Nice to meet you. So you're thinking about cosmetic surgery? If you excuse me, you're already beautiful. But there is - of course - always room for improvement. `);
			kat(`I was thinking about getting bigger breasts... but not seriously! I just wanted to learn how that works. I didn't even want to bother you but your receptionist demanded I should see you. `);
			npc(1,`Yeah, Pamela might be a bit pushy. But don't worry, it's my job to answer all your questions. The process is extremely easy! Our clinic just get a brand new sarcophagus - have you heard about them? `);
			kat(`Yeah, I've seen them on tv. `);
			npc(1,`It takes a half-hour and you will leave or clinic with boosted bust, no further recovery is needed. It's cutting-edge technology. In ten years, every woman will have as big breasts as she desires! What would be your preference: small, medium or large upgrade? `);
			
			if(mile.sub > 7){
				kat(`The smallest one! `);
				txt(`I answered before remembering I was not sure whether I do want any upgrade at all. The doctor seemed to be very excited about breast augmentations and it was not easy to resist his enthusiasm.`);
			}else if(mile.sub < 0){
				kat(`I'm not sure whether I want any upgrade at all! `);
			}else{
				kat(`The smallest one! Wait, I'm not sure whether I want any upgrade at all! `);
			}

			npc(1,`That is usually a sensible choice, starting with the basic smallest option, but in this case, I would strongly discourage that. We have plenty of cases of <i>buyers remorse</i> when a wavering woman decides for the smallest size and then is back next week because it was not exactly what she wanted. And then she has to waste money on another full transformation! So I would strongly suggest medium or large. Are you at least 18 years old? `);
			kat(`I'm 18 years old. `);
			txt(`I told him. He smiled. `)
			npc(1,`Excellent. Would you mind to undress? I need to check your breasts to give you a proper calculation. `);

			if(mile.slut > 7){
				kat(`Sure! `);
				txt(`I quickly dropped my @upper. I had no restrains to undress in front of strangers. `);
			}else if(mile.slut < 2){
				txt(`My face was red, I was ashamed to undress in front of this stranger. But he was a doctor so it was okay? `);
			}else{
				txt(`I very reluctantly stripped my @upper. Well, he was a doctor, was he not? `);
			}
			
			link(`Undress. `, 103);
			break;
			
		case 103: 
			showLess(0,-1);
			npc(1,`Beautiful! `);
			txt(`He put on latex gloves and then began gently cupping my boobs. `);
			if(wears.piercedNipples){
				mile.clinic_piercings = true;
				npc(1,`Nice piercings!`);
				kat(mile.slut < 5 ? `Ehh, thanks. ` : `Thank you! `);
				npc(1,`The sarcophagus isn't MRI, so you don't have to remove them. `);
				kat(`No magnets are involved? `);
				npc(1,`Well, yes and no. It's a bit hard to explain. But they don't interfere with piercings, cyberware or metal hips, if you have one. `);
				kat(`I have the bioimplant, I forgot to mention that. `);
				npc(1,`Yes, even bioimplants don't interfere with the sarcophaqus. `);
			}else{
				npc(1,`And so firm! `);
			}
			txt(`He measured my breasts with some kind of laser device and then began typing something on the computer`);

			if(mile.sub > 10){
				txt(`I nervously watched him. I felt cold and awkward with my tits out but he did not allow me to dress up again. `);
			}else if(mile.sub > 4){
				txt(`I felt cold and awkward with my tits out but he did not told me to dress up again and I was not sure if the examination was over. `);
			}else{
				kat(`Can I dress up now? `);
				npc(1,`Yeah, of course! `);
			}
			
			
			txt(`Then he finally turned the monitor to me and show me the basic prices. I was shocked! It was only ${money.boobs_total} Euro.`);
			kat(`So cheap!?! Is that just the price for one boob? `);
			txt(`He chuckled. `)

			npc(1,`No, we don't upgrade breast separately. That wouldn't look nice. But you're very lucky! Your adolescent body is still very malleable and transformable, the complexity of the procedure and the price quickly rises with the age. And as I said, we got our sarcophagus only recently so we still have very low introductory prices to attract new customers. You have to promise to tell all your friends how amazing we are! `);
			kat(`I won't be your test subject, will I? There won't be any freaky accident which leaves me with tits so huge I won't be able to walk through a door? `);
			npc(1,`Of course not! I worked at a clinic in Prague and have plenty of experience with this technology! `);
			kat(`Althought it's still a lot of money for a student. `);
			txt(`He understandingy smiled: `);
			npc(1,`We coud you offere a very favourable payment plan, from ${money.boobs} Euro weekly. `);
			kat(`I think I would definitely prefer that. `);
			
			link(`Enter the Sarcophagus. `, 110);
			link(`Make excuses. `, 104);
			break;

		case 104:
			mile.bimbo_mods--;
			kat(`I'm still not very sure...`);
			npc(1,`Don't be afraid! The procedure is completely painless, the probability of any complications is negligible and it's mostly reversible. `);
			kat(`So it's reversible? `);
			npc(1,`Yes. Mostly. You can home and think about it for some time, but our special discounts won't be offered for long! `)
			if(mile.sub > 6){
				txt(`I still felt hesitant but I did not want to waste so much of his time pointlessly. And I did not want to lose the discount, I probably would not be able to afford the procedure without it.`);
			}else if(mile.sub < 3){
				txt(`Was this really something I wished? But I did not want to lose the discount, I probably would not be able to afford the procedure without it. `);
			}
			//txt(`The doctor was rather pushy and I felt strong urge to what he told me to do. I did not wanted to waste his time pointlessly. `);
			
			link(`Enter the Sarcophagus. `, 110);
			next(`I have to think about it! `, ()=> mile.clinic_boobjob = -1);
			break;
			
			
			
		case 110:
			add_workday("morning","boobjob_reaction_morning");
			add_workday("classes","boobjob_reaction");

			emo("neutral");
			showLess(0, -1);
			txt(`The sarcophagus was bigger then I imagined. And very comfortable as I found out when I lay down. `);
			npc(1,`So what size will you pick? `);
			
			if(mile.sub > 8){
				txt(`I decided to follow his advice, he probably knew better than me. So I could chose between ${choice("medium")} and ${choice("large")} upgrade. `)
			}else{
				txt(`I could chose between  ${choice("small")}, ${choice("medium")} and ${choice("large")} upgrade. `)
			}
			
						
			txt(`The procedure was indeed completely painless and very quick. `);
			
			effects([{
					id: "small",
					fce(){
						mile.clinic_boobjob = 1;
						mile.boobs = 1;
					},
					relative: {
						breastSize: 14,
						breastPerkiness: 5,
					},
				},{
					id: "medium",
					fce(){
						mile.clinic_boobjob = 2;
						mile.boobs = 2;
					},
					relative: {
						breastSize: 20,
						breastPerkiness: 9,
					},
				},{
					id: "large",
					fce(){
						mile.bimbo_mods++;
						mile.clinic_boobjob = 3;
						mile.boobs = 3;
					},
					relative: {
						breastSize: 32,
						breastPerkiness: 12,
					},
			}]);
				
				
			link(`Done.`, 111);
			break;
			
		case 111:
			emo("shock");
			kat(`Whoa! `);
			txt(`I still could not believe it! Seeing my new breasts in the mirror, feeling their weight on my chest. `);
			npc(1,`Satisfied? Is everything fine? `);
			kat(`I'm satisfied. Thank you, doctor! `);
			npc(1,`You're welcome! I hope your new breast will make your life better and happier! And don't forget we're offering many other transformations! `);
			
			if(mile.bimbo_mods > 3){
				kat(`I won't! I feel we're not seeing each other for the last time!`);
			}else if(mile.bimbo_mods < 2){
				kat(`Thank you but I don't think I need more augmentations. `);
			}else{
				kat(`I will think about that. `);
			}
			link(`Return home.`, 200);
			break;
			
		case 200:
			set.irl();
			mile.ccc++;
			placetime(`my place`);
			emo("help");//emo??
			showLess(0,-1);
			txt(`I just stood in my room in front of a large vanity mirror and stare at my expanded breasts. I was still not able to get used to that sight and on the unusual weight constantly resting on my chest. `);
			
			{
				let size = "";
				//wanted it
				if(mile.bimbo_mods > 3 && mile.c_boobjob !== 1){ //TODO - maybe mile.bimbo_mods > 2
					if(mile.clinic_boobjob === 2){
						size = `Or maybe... maybe the mistake was to shy away and not go for the largest upgrade? Even bigger breasts might look even better... `;
					}else if(mile.clinic_boobjob === 1){
						size = `Well... they actually were not so huge... maybe the real mistake was me being a coward and not choosing a bigger size? Even bigger breasts might look even better... maybe the doctor was right about the buyer's remorse... but of course, I did not listen... `;
					}
					txt(`I was thinking about getting my breasts enlarged but it was just a silly idea, not a serious consideration. And now I actually did it! I still could not believe it, everything went so fast! Did I make a huge mistake? ${size}`);
				//considered it
				}else if(mile.bimbo_mods > -1){
					if(mile.clinic_boobjob === 3){ 
						size = `I even went all in and chose the largest offered upgrade! Was I too greedy? `;
					}else if(mile.clinic_boobjob === 2){ 
						//EMPTY 
					}else{ 
						size = `At least I chose the smalles size... or was he right about the buyer's remorse? They were not so much bigger... `;
					}
					txt(`It was fun to imagine my body getting upgraded but I never seriously considered it. I was just curious but the doctor managed to convince me. Everything went so fast! Did I make a huge mistake? ${size}`);				
				//was pushed into it
				}else{
					if(mile.clinic_boobjob === 3){ 
						size = `And why I decided for this massive size? I was so susceptible, greedy and dumb...`;
					}else if(mile.clinic_boobjob === 2){ 
						size = `And instead of getting the smallest upgrade I even let him to convince me to go bigger... `;
					}else{ 
						size = `At least I chose the smalles size... `;
					}
					txt(`I probably made a huge mistake. What the hell was I thinking! I did not actually even want to get my breasts enlarged but the doctor was so pushy and everything went so fast... ${size} What should I do now?!? Go back, begging him to reverse the process? Try to get used to them?!?`);		
				}
			}
			//+(mile.gf>1?`Not `+NAME.qaa+`. We were getting too close. `:``)
			txt(`I need to share my doubts with somebody else. Not the girls, I was not sure whether they would approve or told me I got crazy. Eventually, I decided to text the guy who caused this! `);
			txt(`@qcc wrote me he had to see them with his own eyes. `);
			
			
			if(mile.slut > 11){
				txt(`When the ring buzzed, I did not bother with dressing up and opened him completely topless. @qcc was very perplexed, I had to grab him by his shirt and pull him inside - I would prefer neighbors to keep thinking I am a nice, decent girl. `);				
			}else if(mile.slut > 4){
				txt(`When the ring buzzed, I quickly put on a sheer bathrobe. @qcc was very perplexed when he instantly notice my new bigger tits only barely covered by the thin fabric. I had to pull him inside. In my room, I dramatically opened the bathrobe. He was stunned.	 `);
			}else{
				txt(`When the ring buzzed, I quickly dressed and went to let @qcc in. He instantly noticed my bigger tits. `);
				qcc(`You actually... `);
				kat(`Yeah! Come in! `);
				qcc(`Can I see them? `);
				kat(`Ehh...`);
				qcc(`Come on!`);
				txt(`I removed my top to present my new assets. He was stunned. `);
			}
			
			qcc(`That's amazing! `);
			txt(`He was staring at my breasts with admiration and awe like they were Mona Lisa or aurora borealis. ${mile.sub > 5 ? "I was anxious but his reaction confirmed that people will like them. " : ""}`);
			qcc(`I can't even... I can't believe I convince you to get your tits enlarged! ${mile.c_boobjob === 1 ? "Even though you initially claimed you don't want to..." : ""} `);
			
			if(mile.sub > 7){
				kat(`Yeah... I guess I'm more susceptible than I thought...`);
				txt(`It was a bit embarrassing to admit the guy I mocked just a month ago persuaded me to alter my body in such a dramatic fashion!`);
			
			}else if(mile.sub > -1){
				kat(`Well, of course you didn't convince me, my decision was influenced by many other things...`);
				txt(`I felt uncomfortable to admit to him (or to myself) I altered my body in such dramatic fashion only because of his suggestion!`);
			
			}else{
				kat(`Whoa, slow down! You didn't make me do anything! This way my own decision, your suggestion played no significant role!`);
				
			}
			
			/*
				qcc(`That's amazing! I convinced `+NAME.katarina+` Pokorná to get her breasts enlarged! This is like a dream come true! `);
				kat(`Nice to know what are you dreaming about. `);
			*/
			
			qcc(`You know... would you mind... can I touch them? `);
			
		
			con_link(mile.slut > 2, `low slut`, `Of course! Try them! `, 201);
			link(`...sure, why not! `, 202);
			link(`No! `, 203);
			break;
			
		case 201:
			if(mile.slut < 12) mile.slut++;
			emo("imp");
			kat(`Yeah, of course. Try them, don't be shy. `);
			qcc(`Thanks, @kat! `);
			txt(`He eagerly grabbed them with his cold hands. `);
			qcc(`They feel so real...`);
			kat(`Yeah, they're not virtual! `);
			qcc(`I mean I don't feel any implants, just your soft fat and flesh `);
			kat(`Yeah. I have no idea how that technology works but there are no implants. `);
			qcc(`They're so awesome! For the rest of my life, I don't want to do anything else... `);
			kat(`Hmmmm...`);
			txt(`I had a bit different idea about the rest of my life but I let him continue when he enjoyed it so much. `);
			
			link(`There so many things to live for other than my breasts. `, 210)
			break;
			
			
		case 202: 
			emo("sarc");
			kat(`Sure, why not! My tits are for everybody! Just grope them as much as you wish! `);
			txt(`I groaned. He was too horny to understand my subtle sarcasm and just grabbed them with his cold hands. `);
			qcc(`They feel so real...`);
			kat(`Yeah, they're not virtual! `);
			qcc(`I mean I don't feel any implants, just your soft fat and flesh `);
			kat(`Yeah. I have no idea how that technology works but there are no implants. `);
			qcc(`But it works well! They're so awesome! `);
			link(`Maybe this might be enough? Please, stop now. `, 210)
			break;
			
		case 203:
			mile.slut--;
			emo("anoy");
			if(mile.sub > 9){
				kat(`No! `);
				qcc(`Come on! `);
				kat(`I said no! `);
				qcc(`Stop playing with me and teasing me! `);
				kat(`I'm not teasing you! `);
				qcc(`She said nearly naked, shamelessly exposing her new tits to me. You're intentionally messing with me! `);
				kat(`I'm not!.... ahhh! Okay, just touch them if you want it so much! `);
				txt(`I angrily groaned and yielded to his demands. He eagerly grabbed them. `);
				qcc(`They feel so amazing, better than I expected! `);
				link(`That might be enough?! `, 210);
			}else{
				showAll();
				kat(`No! `);
				qcc(`Please! `);
				kat(`Don't be creepy! `);
				txt(`I quickly reached for some top and put it on. This discussion would go nowhere if he stayed distracted by my bosom. `);
				qcc(`Oh....`);
				link(`I already shown you too much! `, 210);
			}
			break;
			
		case 210:
				emo("imp");
				kat(`So?`);
				qcc(`So?`);
				kat(`I mean, what do you think?`);
				qcc(`I think you made a great decision! You don't have to be afraid, everybody will love them.`);
				if(mile.sub < 5){
					kat(`I was not afraid, I just wanted to hear your opinion!`);
					txt(`I objected but actually felt calmed down.`);
				}else{
					txt(`His reassurance calmed me down.`);
				}
				
				if(mile.clinic_boobjob === 3){
					qcc(`...and they are so huge...`);
					kat(`Well, the doctor told me the small transformation makes no sense... and when I was already there...`);
					qcc(`You don't have to make excuses! I wouldn't mind if they were even bigger!`);
				}else{
					qcc(`Although... maybe they could be a bit bigger...`);
					kat(`They are not big enough for you!?`);
					qcc(`Well, they weren't offering bigger upgrades?`);
					kat(`They were... but I deciced for ${mile.clinic_boobjob === 1 ? "the smallest" : "the medium"} one.`);
					qcc(`I guess that was reasonable, considering this was your first augmentation... in the future, you can get them enlarged even more... personally I wouldn't mind if they were even bigger.`);					
				}
				kat(`Because you're a pathetic horny nerd brainwashed by unrealistic Japanese cartoons and virtual games!`);
				
				if(mile.c_friend > 2){
					txt(`@qcc chuckled, he learned to handle my banter.`);
				}else{
					txt(`@qcc was so excited about my tits he was not even offended.`);
				}
				
				link(`See him out. `, 300);
				break;
				
		case 300:
				showAll();
				emo("relax");
				txt(`@qcc's very positive reaction made me feel better. Should I tell anybody else?`);
				
				if( mile.b_slave && masochist() ) link(`@qbb`, 320);
				if( mile.a_needle > 0 ) link(`@qaa`, 310);
				link(`@sas`, 330);
				if(mile.a_dating) link(`@qaa`, 340);
				break;
			
			
		case 310: 
				emo("smug");
				mile.clinic_boobjob_react = -1;
				txt(`I decided to send a pic to @qaa. To make him seethe.`);
				pic_a(311);
				break;
				
		case 311:
				pic_b();
				mile.a_hate++; //TODO
				
				kat(`Look what I got, dumbass 😂`);
				kat(`Do you like them?`);
				kat(`Would you liek touch them? 😝`);
				kat(`Would you like to shove you 🍆 between them and let me stroke it it until cumming all over my face?`);
				kat(`To fucking bad. 😄😄😄`);
				kat(`Its never gonna happen! 😈`);
				kat(`But your free to play with your tiny dickled while daydreaming about me.`);
				kat(`You pathetic perv.`);
				qaa(`Are those real? Its you irl?`);
				txt(`Somebody could not anymore pretend he was ignoring me and had to ask!`);
				kat(`Damn yeah they are! 😎`);
				next();
				break;
		
		case 320: 
				mile.clinic_boobjob_react = 2;
				txt(`I decided to send a pic to @qbb. I thought he might be happy his bitch got an upgrade.`);
				pic_a(321);
				break;
				
		case 321:
				pic_b();
				mile.b_mas++;
				emo("help");//emo?? pain?
				kat(`Hey! Gues whats different! 😜`);
				txt(`I expected him to be pleased but he was not.`);
				qbb(`What is that?!`);
				kat(`YOure not happy? 😕`);
				qbb(`Who owns you?`);
				kat(`You.`);
				kat(`You own me.`);
				qbb(`That's right, bitch. And who owns your tits?`);
				kat(`You.`);
				kat(`You own my tits.`);
				qbb(`Did you ask for a permission to do something with them?`);
				kat(`No. Im sorry! I tought youll like it!`);
				kat(`😞`);
				kat(`Sorry!`);
				qbb(`Slap your face and twist your nipples as hard as you can.`);
				kat(`Yes, Master!`);
				txt(`I did not cherish this development. But I had to obey. I clumsily smacked my face, accidentally harder than I anticipated. And then took both my nipples, pinched them and roughly twisted them until it hurt.`);
				kat(`Done! 😢`);
				qbb(`Good.`);
				qbb(`Nice tits, bitch 👍`);
				next();
				
				break;
					
				


		case 330: 
				mile.clinic_boobjob_react = 3;
				txt(`I decided to send a pic to @sas. She was less judgemental and I wanted to test the water.`);
				pic_a(331);
				break;
				
				
		case 331: 
				pic_b(false);
				kat(`@sas? I did something crazy 😟`);
				sas(`@kat? Is that real you? 😲`);
				kat(`Yes, everyhings real!`);
				sas(`😲😲😲`);
				sas(`Are you serious?`);
				kat(`Yeah!`);
				sas(`😲😲😲`);
				kat(`Isit bad? 😟`);
				sas(`Its great! 👍`);
				kat(`😌 Awesome`);
				sas(`I cant believe it! You crazy slut 😄😄😂`);
				kat(`😎😀😀😁`);
				sas(`Im jelous 😍`);
				kat(`😆`);
				next();
				break;
				
				
		case 340: 
				mile.clinic_boobjob_react = 1;
				txt(`I decided to send a pic to @qaa. I was sure he will be very surprised.`);
				pic_a(341);
				break;
				
		case 341:
				pic_b();
				qaa(`Nice! 😍`);
				qaa(`Wait, is that irl you?`);
				kat(`Yeah 😜`);
				qaa(`😮`);
				qaa(`Like seriously?? 😮`);
				kat(`Yeah 😉`);
				qaa(`😮`);
				qaa(`I have to see it with my own eyes!`);
				chapter(`@qaa's visit. `, `a_boobjob`);
				break;			
				
	}
}



export const boobjob_reaction_morning = (index)=> {
	txt(`I did not plan to get my breasts enlarged. My decision was rash and I did not realize there might be consequences. When I was choosing what top wear to school, I found out ${["", "none fit properly", "all were too small", "all were way too small"][mile.clinic_boobjob]}. I decided to pick ${PC.upper.name}, hoping it will be fine. However, when I arrived at the tram station and people began weirdly staring at me, I began suspecting I maybe had made a mistake. My @upper ${["", "was stretched to the max", "was stretched, desperately struggling to contain my big tits", "was desperately struggling to contain my huge tits, threatening to burst at any moment"][mile.clinic_boobjob]}, lewdly drawing everybody's attention to my chest. But it was too late to go back to get changed.`);
	next();
}	

export const boobjob_reaction = (index)=> {
	switch(index){
		default:
		case 101:
				if( free_today("morningReaction") && free_today("morningNew") ){
					qaa(`@katalt?`);
					kat(`What?`);
					qaa(`We need to ask you something.`);
					txt(`The nerds ambushed me. They wanted to talk with me, for a very obvious reason:`);
					kat(`Ask me what?`);
					qcc(`Ask you about your tits.`);
					
				}else{
					qaa(`...wait! Don't go anywhere yet, there's something else we need to discuss!`);
					kat(`What?`);
					txt(`I wondered but it was pretty obvious how they the whole time stared at my enhanced chest.`);
					if(mile.clinic_boobjob === 3){
						qbb(`Your tits, of course! They're so huge you really can't miss them.`);
					}else if(mile.clinic_boobjob === 1){
						qcc(`Your slightly improved tits, of course!`);
						qbb(`Improved?`);
						qaa(`Yeah, they're bigger, you didn't notice?`);
						qbb(`Huh.`);
						qcc(`They're not so much bigger.`);
					}else{
						qaa(`Your breast, of course.`);
					}
				}

				if(mile.slut < 4 && mile.sub < 4){
					kat(`Yeah, I got my breasts enlarged. But that's only my own business!`);
					txt(`I shut them down.`);
				}else if(mile.slut > 6){
					kat(`Yeah, I got my breast enlarged. I wanted to look even sexier. Do you like them?`);
					txt(`I grinned at them and teasingly cupped my tits.`);
					qcc(`Yeah!`);
				}else if(mile.sub > 7){
					kat(`Well... I kinda got my breasts enlarged. Do you like them?`);
					txt(`I asked them a bit anxiously, wanting to know their honest opinions.`);
					qcc(`Yeah, we do!`);
				}else{
					kat(`Yeah, I got my breast enlarged.`);
					txt(`I shrugged.`);
				}
				
				qbb(`Definitely improvement!`);
				qaa(`Sure.`);
				
				qcc(`And you told me she would never agree to do it and she'll tell us to go fuck ourselves with such a task. I could save you one task, @kat, if they listened to me!`);
				qaa(`Nobody could expect she would agree with such radical change!`);
				qbb(`Guys, I think we accidentally awakened something inside her. Her inner bimbo slut. Mark my words, in three months we won't recognize her and she'll drop from school the follow her calling and pursue a meagerly-successful career in the sex industry.`);
				if(mile.slut > 13 && mile.yw_whore > 0){
					txt(`I was offended:`);
					kat(`Meagerly-successful!?!?!`);
				} 
				if(mile.b_domina){
					txt(`I frowned at him and he quickly wavered.`);
					qbb(`Sorry! I meant it in a good way!`);
				}else if(mile.b_slave){
					txt(`He winked at me:`);
					qbb(`And we should do anything we can to help her to follow her dreams!`);
				}
				qcc(`Being a bimbo slut doesn't mean she has to drop from school!`);
				if(mile.a_burned){
					qaa(`Let's be realistic, she was never the sharpest tool. The sooner she realizes her sexy body is her only redeeming quality, the better.`);
				}else{
					qaa(`@katalt isn't a bimbo slut!`);
					if(mile.slut > 9 && mile.a_dom >= 0 && mile.bimbo_mods >= 0){
						txt(`@qaa tried to defend my honor, he seemed to be more offended when they called me a bimbo slut than I was.`);
						kat(`Well, maybe I am.`);
						txt(`I smiled at him, toying with him.`);
					}else if(!mile.b_domina){
						qbb(`Of course she is, just look at her! You're in denial!`);
					}
					qcc(`There's nothing wrong with being a bimbo slut!`);
				}
				
				link(`I'm getting tired of this conversation! `, 102);
			break;
			
			
			
		case 102:
				emo("shy"); //??
				eva(`Good morning, @katalt! I see there's something new about you!`);
				txt(`The girls were no less curious.`);
				sas(`@kat! You tits!`);
				
				if(mile.sub < 1){
					kat(`Yeah, I got my breast enlarged. Aren't they absolutely awesome?`);
					sas(`They look nice!`);
				}else{
					if(mile.sub > 10){
						kat(`Yeah.. I got my breast enlarged a bit... I thought they might look good... what do you think?`);
					}else{
						kat(`Yeah, I got my breasts enlarged a bit!`);
					}
					
					if(mile.clinic_boobjob >= 2){
						sas(`I'd say more than a bit! ${mile.clinic_boobjob === 3 ? "They're so huge!" : ""}`);
						txt(`@sas chuckled.`);
					}
				}
				
				eva(`Interesting. You always claimed you're against such surgeries and artificial improvements and mocked women who got them?`);
				kat(`Well, only idiots don't change their minds.`);
				eva(`So you no longer have to be an insecure needy slut who is o-kay with being objectified to get your breasts enlarged?`);
				sas(`It's true you said something like that!`);
				txt(`@sas nodded, curious about my answer.`);
				kat(`No, of course not. If getting an upgrade is so easy, why settle for whatever nature gave you? What's your problem?!`);
				eva(`I'm just saying I'm satisfied with my body and prefer guys being attracted by my personality rather than being reduced just to a pair of tits. I mean it well, I'm just afraid some people might get the wrong impression and think that you're insecure and desperately need the huge rack to boost your fragile confidence.`);
				
				link(`Yeah... people might think that...`, 103);
				link(`Nobody will think that. `, 104, ()=> mile.girls_boobjob = 1 ); //TODO - con_link ???
				link(`You're just envious!`, 104, ()=> mile.girls_boobjob = 2 );
				
			break;
			
		case 103:
				emo("help"); 
				mile.sub++;
				mile.bimbo_mods--;
				//mile.stat--; //moved to 110
				
				kat(`Yeah... I guess some people might think that...`);
				txt(`I reluctantly agreed with @eva. I could see she had a point - the breast enlargement revealed I deep down I was unsatisfied and anxious about my body. And did I really want to be known just as that girl with big tits? Moreover, unlike naturally busty @eva, I willingly had my body changed just to pander to male sexual fantasies.`);
				txt(`@eva saw how crestfallen she made me and smugly smiled:`);
				eva(`Come on, @kat! I'm sure not everybody will think you're just desperately seeking attention whore. And your new tits do look good!`);
				
				if(mile.sas_sassy > 2){
					mile.girls_boobjob = -1;
					
					sas(`They look great! I don't understand why you have to be such a bitch, @eva!`);
					eva(`What is that supposed to mean, @sas?`);
					sas(`You know well what is that supposed to mean! You're just envious @kat has now bigger tits than you!`);
					eva(`Don't be ridiculous! I'm not that petty!`);
					sas(`You are!`);
				}else{
					mile.girls_boobjob = -2;
					
					sas(`Yeah, they look great!`);
					txt(`@sas tried to cheer me up too but I could see she was agreeing with @eva and thought getting my breasts enlarged was dumb and unnecessary.`);
				}
				
				link(`Continue. `, 200);
				break;
				
		case 104:
				mile.bimbo_mods++;
				if(mile.girls_boobjob === 2){
					emo("angry"); 
					mile.sub--;
					mile.eva_exploit++;
					
					kat(`You're just envious!`);
					txt(`I attacked her.`);
					eva(`I have no idea what are you talking about!`);
					kat(`Of course you do! You always show off with your big tits, wearing the tightest tops and sexiest bikini, and now you're pissed and envious my new upgraded boobs are even bigger than yours!`);
					eva(`Don't be ridiculous, @kat! I'm not that petty!`);
					kat(`Oh yes, you are! Instead of being supportive and happy for me, you're being a complete bitch! Tell her, @sas.`);
					sas(`...what, me? I guess she could be a bit more supportive...`);
					kat(`See? Even @sas thinks you're acting like a completely unreasonable jealous bitch!`);
					txt(`I scolded her.`);
					eva(`You don't have to be so fucking rude, @kat! I just wanted to point out things you might not realize. But if you don't want to hear the truth, fine!`);
					txt(`@eva was angry. She knew she lost the argument and tried to quickly end it without embarrassing herself further.`);
					eva(`I just personally don't believe the boobjob was the best choice.`);
				
				}else{
					emo("smug"); 
				
					eva(`Oh! I see! I understand your concern @eva! But you don't have to worry, I think you're too pessimistic. Only maybe a few sexist jerks or envious bitches might think that but I don't value the opinions of those people anyway. I believe most people will like my new breasts. Moreover, I'm not so basic to have my whole personality overshadowed by my tits.`);
					txt(`I did not lose my cool and calmly answered @eva. She was such a huge bitch! But I knew well why - I was more good-looking and popular and she was beating me only in one category - her bustier body. And now she was the second in everything. Well, she should better get used to it.`);
					eva(`You sound too optimistic. But I hope you're right, I don't want people to think you're just an attention whore.`);
					txt(`@eva was not backing down.`);
					kat(`You wouldn't even think about such cosmetic transformation?`);
					eva(`Of course not! As I said, I'm fully satisfied with my body!`);
				
				}
				
				kat(`What about you, @sas?`);
				txt(`I ostentatiously ignored @eva and turned to @sas instead.`);
				sas(`Me?!?`);
				kat(`Yeah! With a little bigger rack you'd be the hottest girl in the school!`);
				sas(`You mean the second hottest, after you.`);
				txt(`@sas was too genuine to mean it as a subtle poke against @eva, but @eva was quietly seething anyway.`);
				kat(`The whole procedure is extremely simple, quick and cheap! I can vouch for that!`);
				if(mile.sas_sassy > 1){
					sas(`Well, having bigger breasts might be good... It does sound interesting.`);
				}else{
					sas(`Well, I'm not sure about it.`);
				}
				eva(`You shouldn't headlessly rush to commit such a huge change. It's your body and...`);
				kat(`Come on, you shouldn't needlessly overthink it, they won't offer low introductory prices for long. And you'll look awesome, you can trust me!`);
				if(mile.sas_exploit > 2){
					txt(`I was sure I will be able to push very suggestible @sas into improving her bust too. That will show @eva to not mess with me!`);
				}
				if(mile.sas_sassy > 2){
					sas(`I think I want to do that.`);
				}else if(mile.sas_sassy <= 0){
					sas(`I'm still not sure whether it's a good idea... but I'll think about it...`);
				}else{
					sas(`I will think about it.`);
				}
				
				link(`Continue. `, 200);
				break;
		
		case 200: 

			if(mile.clinic_boobjob === 1){
				txt(`My breast enlargement was decent and subtle so not everybody immediately noticed. However, those who did spread the gossip and soon the whole school knew. And nobody was talking about anything else.`);
			}else if(mile.clinic_boobjob === 3){
				txt(`Nobody could miss my brand-new huge breasts. The whole school was not talking about anything else but it was kinda understandable, there was nothing more exciting.`);
			}else{
				txt(`The gossip about my new enlargement breasts quickly spread and soon the whole school was not talking about anything else. This was honestly quite understandable, there was not much going on at our school and my new tits were definitely the most exciting thing.`);
			}
			
			txt(`The guys gave me lust-filled covetous stares, on the other hand, many girls seemed annoyed and envious. Small groups loudly discussing something immediately suspiciously shut up when I got within hearing distance of them. And of course, even teachers noticed, with reaction going from the disapproving glare of @maj to the improperly long perplexed stare of @dur and to the blatantly lecherous ogling of @mol.`);
			
			if(mile.girls_boobjob < 0){
				emo("unco");
				mile.stat--;
				
				if(mile.slut > 6){ 
					txt(`What @eva said was lingering with me. Under other circumstances, I would enjoy the attention and being the object of sexual desire. But @eva made me question whether I had made the right choice. Of course, I wanted people to notice my hot body but I also wanted to be more than just my tits.`);
				}else{
					txt(`What @eva said was lingering with me. It was causing that all the unescapable attention was making me anxious and uncomfortable. Will people focus only on my big tits and respect me less as a person? I began doubting whether I made the right choice and almost regretted getting my breasts enhanced.`);
					
				}
			}else{
				if(mile.slut > 10){ //TODO
					emo("imp");
					txt(`I absolutely loved the attention and being the target of sexual desire! It was awesome that everybody was talking just about me, getting my tits enlarged was definitely worth it! I sincerely hoped that many guys, the next time they will have sex or jerk off, will imagine me instead of their stupid flat girlfriends or random pornstars.`);
				}else if(mile.slut < 3){
					emo("unco");
					txt(`All the unescapable attention was making me rather awkward and uncomfortable. I would prefer to keep my tits just for myself and a few chosen ones, not have them discussed publicly by everybody.`);
				}else{
					emo("relax");
					txt(`I very much enjoyed being the center of attention and looking sexy and desirable. On the other hand, I have to admit, seeing everybody discussing my breasts was making me feel rather awkward and uncomfortable. All things considered, it seemed that getting my breasts enhanced was a good idea.`);					
				}
			
			}
			
			next();
			break;
	}
}




/*
	girls_boobjob: 0, 
		-2 - eva made her feel bad
		-1 - eva made her fell bad but sas defended
		1 - calmly resisted eva
		2 - angrily argued with eva
*/
export const clinic_sas_boobjob = (index)=> {
	const sassy_picks_herself = 2; //mile.sas_sassy bigger than this means sas choses size herself
	
	switch(index){
		default:
		case 101:
		placetime(`tram`);
		set.irl();
		if(mile.girls_boobjob === -2){
			txt(`@sas claimed she liked my new breasts. I assumed she just wanted to be nice and did not expect her to call and ask if I have time to visit the cosmetic clinic with her.`);
		}else{
			txt(`I suggested @sas should get her breast enlarged too. But I did not press the issue further and was a bit surprised when she out of nowhere called me and asked me if I have time to visit the cosmetic clinic with her.`);
		}
		txt(`Of course I said yes and we met on the tram.`);
		kat(`So you're actually thinking about getting your boobs bigger too?`);
		sas(`Yeah, I do. I don't want you and @eva to make fun of my flat chest anymore.`);
		kat(`It's just banter, don't take it seriously.`);

		if(mile.sas_sassy > 2){
			sas(`I want to be as hot and confident as you.`);
			kat(`That isn't just about tits!`);
			sas(`But having big tits doesn't hurt, does it?`);
			kat(`Yeah. Don't worry. The doctor is a nice guy, he'll explain everything and you'll be able to decide what is best for you.`);
			sas(`I'm already decided. I checked how the process works and the benefits and downsides and the reviews of the clinic. Just going there and making a rash decision on the spot based only on what the guy running the place tells you would be just stupid.`);
			kat(`...yeah, you're right. ...I did it the same way.`);
			sas(`I was afraid of what might people say. And that @eva will mock me. But if you could handle them I can too!`);
			kat(`Damn right, go girl!`);

			if(mile.sas_exploit > 2){
				txt(`I had rarely seen @sas so driven and decisive but I was pretty happy about it. Her augmented tits would help to normalise mine, mess with envious @eva and divert some unwanted attention from me to @say. So many flies swatted in one hit!`);
			}else{
				txt(`I had rarely seen @sas so driven and decisive.`);
			}
		}else{
			sas(`I don't like it. I want to have nice big tits. Like you.`);
			kat(`Thanks!`);
			sas(`Am I being too dumb? I'm still not sure whether this is a good decision or not.`);

			if(mile.sas_exploit > 2){
				kat(`I'm absolutely sure this is a good idea!`);
				txt(`I was not sure whether this was a good idea. But her augmented tits would help to normalise mine, mess with envious @eva and divert some unwanted attention from me to @say. So many flies swatted in one hit!`);
			}

			kat(`Don't worry. The doctor is a nice guy, he'll explain everything and you'll be able to decide what is best for you.`);
			sas(`I already checked everything about the process and all the benefits and downsides and reviews of the clinic. Just going there and making a rash decision based only on believing whatever the guy running the place tells you would be just stupid.`);
			kat(`...yeah, you're right. ...I did it the same way. ...And?`);
			sas(`There are almost no downsides. It is a really good deal.`);
			kat(`Then what is the problem?`);
			sas(`I... well, I'm afraid of what will @eva and our classmates say.`);
			kat(`You don't have to be, getting your boobs enlarged is the norm now, I set a new trend!`);
			sas(`Thanks, @kat!`);
		}
		
		link(`Go. `, 102);
		break;
			
	case 102:
		placetime(`Cosmetic Clinic`);
		present(
			[1, `Dr. Nagy`, ["whiteTightTee", "whitePants", "whiteSneakers"]], //TODO
			[6, `Pamela`, ["bimboThong", "sleevedTubeTop", "miniSkirt", "polyStockings", "bimboLoops", "multiNecklace","bellyPiercing","nippleBars"], "bimbo"], //TODO Pamela
		);
		txt(`We entered the Cosmetic Clinic. @sas nervously looked around but I confidently walked straight to the big-titted receptionist who smiled at me.`);
		kat(`Hello!`);
		npc(6, `Hello! How can I help you?`);
		kat(`We would like to talk with doctor Nagy, if he's here and free.`);
		txt(`This time we had to wait several minutes before our consultation. It was nice to see their business was taking off. The doctor instantly recognized me.`);
		npc(1, `@Afternoon, miss @katsur! Great to see you again. Are you satisfied with your new breasts? No problems?`);
								
		if(mile.bimbo_mods > 2 && mile.clinic_boobjob === 1){
			kat(`There are no problems. Althought... now I regret a little that I chosen the smallest option instead of listening to you and getting a bigger one. `);
			npc(1, `If only people listened to their doctors. Anyway, it isn't something that can't be fixed in the future. `);
			kat(`Well, not today, I'm just a chaperone. `);
		}else if(mile.bimbo_mods > 1){
			kat(`There are no problems. And I'm more than satisfied, they are awesome!`);
		}else{
			kat(`There are no problems. I guess I'm satisfied although I'm still unable to get used to them.`);
		}
		npc(1, `And I see you brought a friend! Good afternoon, I'm doctor Milan Nagy.`);
		sas(`Hello, I'm @sasalt @sassur.`);
		if(mile.sas_sassy > 0){
			npc(1, `Are you considering getting your breasts enlarged too? Or maybe you're interested in a different transformation?`);
			sas(`I would like to have bigger breasts.`);
		}else{
			kat(`@sas is thinking about getting her breast enlarged too.`);
			sas(`Well... yeah... I do.`);
		}
		npc(1, `You don't have to be afraid. Your friend can confirm the whole procedure is quick and comfortable.`);
		
		link(`I can! `, 103, ()=> counter.temp = false);
		link(`Yeah, it totally wasn't extremely painful! `, 103, ()=> counter.temp = true);
		break;
		
	case 103:
		if(counter.temp){
			txt(`@sas frowned`);
			sas(`I do know the procedure is painless, I read about the sarcophagus technology!`);
		}else{
			txt(`@sas nodded:`);
			sas(`I know. I read about the sarcophagus technology.`);
		}
		npc(1, `Isn't it wonderful?`);
		txt(`The doctor made @sas undress and she blushed when his fingers in latex gloves gently kneaded her breasts.`);
		npc(1,`What size would you prefer? `);
		
		if(mile.sas_sassy > sassy_picks_herself){ //TODO - SUB? 
			if(mile.boobs == 3){
				mile.sas_boobs = 2;
				sas(`Definitely the medium enlargement. I do want to look sexy but I think too large breasts would look ridiculous! `);
				txt(`She answered without hesitation. And I felt a bit offended. What the hell she meant by that?!`);
			}else{
				mile.sas_boobs = 3;
				sas(`Definitely ${choice("large")}! `);
				txt(`She boldly answered without any hesitation. I was pretty surprised, she had not mentioned she wanted to go that far. Her new boos will be even bigger than mine!`);
			}
		}else{
			sas(`I'm not sure... what do you think, @kat?`);
			kat(`Definitely ${choice("small")} / ${choice("medium")} / ${choice("large")}.`);
		}
		npc(1, `That's a good decision! I can ensure you your new breasts will look amazing!`);
		sas(`Yeah... I think I'm ready...`);
		
		
		link(`Sarcophagus. `, 110);
			
		effigy.sas.showLess(0, -1);
		
	
			effects([{
					id: "small",
					subject: effigy.sas,
					fce(){
						mile.sas_boobjob = 1;
						mile.sas_boobs = 1;
					},
					relative: {
						breastSize: 14,
						breastPerkiness: 5,
					},
				},{
					id: "medium",
					subject: effigy.sas,
					fce(){
						mile.sas_boobjob = 2;
						mile.sas_boobs = 2;
					},
					relative: {
						breastSize: 20,
						breastPerkiness: 9,
					},
				},{
					id: "large",
					subject: effigy.sas,
					fce(){
						mile.sas_mods++;
						mile.sas_boobjob = 3;
						mile.sas_boobs = 3;
					},
					relative: {
						breastSize: 32,
						breastPerkiness: 12,
					},
			}]);
		
		break;
		
		
	case 110:
		npc(1,`What about you? `);
		kat(`What about me? I'm fully satisfied with my current size. `);
		npc(1,`Maybe you want to improve something else? `);
		kat(`I don't think I can afford another transformation right now. `);
		npc(1,`I thought you're here because of our promotion program! `);
		kat(`? `);
		npc(1,`You brought us a new customer so you can get one minor transformation for free, if you want. `);
		
		if(mile.bimbo_mods > 2){
			kat(`What?!? That is awesome! I had no idea! I definitely want it! What is a minor transformation?`);
		}else{
			kat(`Oh...`);
			txt(`I did not plan to get any other enhancements. However, it would be a waste to not exploit the free stuff, right?`);
		}
		npc(1,`For example, we can ${choice("lips", "enlarge your lips")},  ${choice("belly", "tone your belly")} / or ${choice("ass", "slightly augment your buttocks")}. Of course, I don't want to force you if you're ${choice("no", "not interested. ")}`);
		
		link(`Continue. `, 120);
		
		effects([
			{
				id: "lips",
				fce(){
					mile.clinic_minor = 1;
				},
				relative: {
					/*
					lipTopSize: 20,
					lipBotSize: 30,
					lipWidth: 30,
					*/
					lipTopSize: 10,
					lipBotSize: 20,
					lipWidth: 15,
				},
			},
			{
				id: "belly",
				fce(){
					mile.clinic_minor = 2;
				},
				relative: {
					waistWidth: -5, //-8
				},
			},
			{
				id: "ass",
				fce(){
					mile.clinic_minor = 3;
				},
				relative: {
					buttFullness: 8,
					legFem: 3, 
				},
			},
			{
				id: "no",
				fce(){
					mile.clinic_minor = 3;
				},
				relative: {
					buttFullness: 8,
					legFem: 3, 
				},
			},
		]);
		
		break;
		
		
	case 120:
			effigy.save("sas");
			if(mile.sas_boobs === 3) mile.sas_sassy++; 
			mile.sas_sassy++; 
			if(mile.clinic_minor !== -1) mile.bimbo_mods++;
			add_workday("classes","sas_boobjob_reaction");
			 
			txt(`@sas left the sarcophagus. She looked pretty amazing with her new ${(() => {switch(mile.sas_boobs){
				case 1: return "sizable";
				default:
				case 2: return "big";
				case 3: return "enormous";
			}})()} tits. `);
			if(mile.clinic_minor !== -1){
				txt(`I switched places with her to get my own transformation. Again I could experience light vibrations and strange warmth when the sarcophagus was doing its magic.`);
				txt(`When I was done and left it, @sas was still standing in front of a large mirror, totally mesmerized by her new figure`);
				kat(`You're looking awesome! `);
				sas(`You too! `);
				txt(`She returned the compliment. `);
			}else{
				kat(`You're looking awesome! `);
				sas(`Am I? `);
				txt(`She rushed to the large mirror and there stood still, totally mesmerized by her new figure.`);
				kat(`Yeah! You're hot! `);
				txt(`I complimented her. `);
			}
			
			if(mile.sas_sassy > 2){
				kat(`I'm sure @eva will be pretty envious!`);
				sas(`Fuck her, I don't care what anybody thinks, I like them!`);
				txt(`She smiled at me and I smiled at her. It seemed the transformation did not boost only her rack. `);
			}else{
				sas(`What do you think will @eva say? `);
				kat(`Who cares? Fuck her! You like them, don't you? `);
				sas(`Yeah!`);
			}
			
			next();
			break;
	}
}			
			
			


export const sas_boobjob_reaction = (index)=> {
	switch(index){
		default:
		case 101:
		//placetime(`tram`);
		//set.irl();
		mile.sas_cool = true; //TODO!!!!!
		effigy.sas.draw();
		txt(`Just like I predicted, for once it was not me who was the center of attention. And I rather enjoyed that people were gossiping about somebody else's body. @sas was not the person you would expect will get her breasts enlarged and her brand new tits did cause a bit of commotion. ${mile.sas_boobs >= 3 ? "Especially since they were so huge! " : ""}`);
		txt(`@sas really enjoyed her five minutes of fame. The size of her transformed chest was only highlighted by her tiny tight top. I was not sure whether she realized her old clothes would not fit her anymore too late (like me) or whether it was a completely deliberate decision. `)

	//console.warn( effigy.sas.getDim("breastSize") )
		if(effigy.sas.getDim("breastSize") > PC.getDim("breastSize") ){
			let tex = `I admit I was getting a bit jealous. Despite my own enhancements, @sas had now even bigger breasts than me!`;
			if(mile.bimbo_mods > 3){
				tex = `Maybe I should visit the Cosmetic Clinic again and ask for another breast enlargement? Damn, if only I had the money for it! `
			}
			txt(tex);
		}

		txt(`@sasalt was handling the attention with surprising newfound confidence: `)
		if(mile.sas_sassy > 2){
			sas(`Thank you, @kat, that you supported me instead talking me out of it. This is the best thing that ever happened to me! `);
		}else{
			sas(`Thank you, @kat, that you counvinced me it was a good decision. This is the best thing that ever happened to me! `);
		}
		kat(`You're welcome! I'm happy you're happy! `);

		txt(`But not everybody was happy. @eva approached us with a smug smile:`)
		
		if(mile.sas_sassy > 3){ //TODO 
			eva(`So you really did it, didn't you? You convinced poor @sas to get her tits enlarged and now she's looking like a tacky bimbo! Hope you're proud of yourself!`);
			txt(`I was about to say something witty but I did not get the opportunity.`);
			sas(`You know I'm able to speak for myself and make my own decisions, right? @kat did not convince me to do anything!`);
			eva(`But...`);
		}else{
			eva(`So you really did it, didn't you? You let @kat convince you to get your tits enlarged and now you're looking just like a tacky bimbo! Why you didn't listen?`);
		}
		sas(`Shut up! You should care more about your fat ass than my tits!`);
		txt(`@sasalt snapped. I and several other witnesses of the exchange were amazed. She was was usually a meek girl and there were few people not afraid to be verbally destroyed by sarcastic @eva.`);
		sas(`I have enough of your criticism! You're just jealous, you petty bitch!`);
			
		link(`Don't get involved.`, 110);
		link(`Calm your friends down. `, 111);
		link(`Double down.`, 112);
		break;
		
		
	case 110:
			mile.sas_boobjob_reaction = 2;
			emo("imp");
			
			txt(`It was just amazing to watch furious @sas yelling at dumbfounded @eva. I had not seen anything so hilarious in a long time. @eva definitely deserved it! I just wish somebody was recording so I could replay the scene again and again.`);
			if(mile.eva_exploit > 2 && mile.sas_exploit > 2){
				txt(`However, I wisely decided to stay out of their fight. Without openly picking sides I could only benefit from their conflict.`);
			}else{
				txt(`However, I wisely decided to stay out of their fight.`);
			}
			txt(`Destroyed and humiliated @eva and eventually hastily retreated. For the rest of day she was sulking and refused to talk with us.`);
			
			next();
			break;
			
	case 111: 
			mile.sas_boobjob_reaction = 3;
			emo("focus");
			mile.eva++;
			mile.eva_nice++;
			
			txt(`@eva was for a moment totally dumbfounded. It was hilarious to see her being scolded, she definitely deserve it. But @eva was not going to let @sas win and began yelling back at her. The argument was getting more heated and drew more attention so I decided to intervene and stop my friends before they embarrass themself further or even get violent.`);
			kat(`Come on, girls! Calm down! There's no reason to get offended, I believe the important thing is that both @sas's breasts and @eve's butt look amazing, right?`);
			if(mile.eva_exploit > 2 && mile.sas_exploit > 2){
				mile.stat--; //TODO!
				emo("shock");
				sas(`Yeah?! You're not better than her! You just love constantly criticizing me too!`);
				eva(`It was you who came up with this stupid idea of getting your tits enlarged!`);
				sas(`Nothing I ever do is good enough for you!`);
				eva(`Like dressing like slut yourself wasn't enough, now you want to turn into a slut @sas too?!?`);
				txt(`They ganged up on me, yelling at me from both sides. That was what I got for trying to help them.`);
				eva(`You know what? Fuck you, @kat!`);
				sas(`Yeah, fuck you, @kat!`);
				txt(`They both walked off in different directions. For the rest of the day they were sulking and were not talking with each other.`);
			}else if(mile.sas_exploit > 2){
				sas(`Yeah?! You're not better than her! You just love constantly criticizing me too!`);
				kat(`Well, not always...`);
				sas(`You know what? I won't let you two bitches ruin my day! Farewell!`);
				txt(`@sas walked off. For the rest of the day she was sulking and refused to talk with us.`);
			}else{
				eva(`I guess...`);
				sas(`...yeah...`);
				txt(`They were both still fuming and remained annoyed for the rest of the day but I somehow managed to defuse the situation.`);
			}
			
			next();
			break;
			
	case 112:
			mile.sas++;
			mile.eva_exploit++;
			mile.sas_boobjob_reaction = 1
			emo("imp");
			
			kat(`Yeah, fuck you! You're just envious because you can't handle our glorious new tits! `);
			txt(`I stood firmly behind @sas. I had enough of @eva's bullshit too! And it was so hilarious to see her embarrassed and dumbfounded. `)
			txt(`Poor, humiliated @eva was totally destroyed and forced to quickly retreat. She was seething for the rest of the day and refused to talk with us. `)
			
			next();
			break;
	}
}	









