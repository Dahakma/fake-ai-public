import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, tom, zan, ven} from "Loop/text";
import {mol, maj, ped, bio, dur} from "Loop/text";
import {bold, hint} from "Loop/text";
import {log_sex} from "Virtual/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";
import {ra} from "Libraries/random";
import {emo, rollUpSkirt, saveWornOutfit, wearOutfit, removeEverything, wdesc, removeActual, update, updateDraw, effigy, allClothes, crew, wearRandomClothes, remove, wear, create, wears, showLess, showAll, violates, sluttiness, createWear, quickSaveOutfit, quickLoadOutfit  } from "Avatar/index";
import {placetime} from "Loop/text_extra";
import {nameColor} from "Libraries/color/index";
 
import {asy} from "Loop/text";


/*
	Literature
	- Majerová
	- class teacher
	
	German
	- Molnár
	- oral examination 
	- sleazy teachyer 
	
	Chemistry
	- lab work
	- study chemistry
	
	Biology
	- oral examninaton
	- chastised 
	
	Physic Education
	
	History 
	
	Geography?

*/

/*
	mile.grades
	2+/- biology
	+/- history

*/

/*
	2nd week
	HISTORY 
	flash_breast_class event
	+/-
*/





//BIOLOGY
export const study_biology = (index)=> {
	switch(index){
		default:
		case 101:
			set.irl();
			emo("sad");
			placetime("my room");
			txt(`Reluctantly I sat down at the desk and opened my biology textbook. The order in which @bio orally examined students followed a fairly predictable pattern. He was dangerously close to my surname, there was like 33% chance I will be examined this week.`);
			txt(`So I tried to focus on the chapter about the human body but it was full of Latin terms and so boring I was falling asleep. I took a short break, brew a strong black tea and then I moved to the bed, hoping that a more comfortable position improve my concentration.`);
			txt(`On the other hand, 33% chance was not so high...`);

			link(`Continue studying. `, 102);
			link(`Text friends. `, 103);
			con_link(mile.slut > 1, `low slut`,`Masturbate. `, 104); //TODO SLUT
			break;
			
		case 102:
			mile.study_biology = 1;
			txt(`I resisted all distractions and continued until I re-read the chapter and then I went through my notes and highlighted the most important things.`);
			next();
			break;
		
		case 103:		
			emo("happy");
			mile.study_biology = -1;
			kat(`Hey!`);
			sas(`Hey!`);
			kat(`Im studying biology 😦`);
			eva(`Hello!`);
			sas(`😞`);
			eva(`Poor you!`);
			sas(`Youre getting examined?`);
			kat(`Probably, yeah 😢`);
			sas(`😿`);
			sas(`I was examined last week, it wasnt so bad`);
			kat(`I think maybe Ill just wing it, its just human body!`);
			eva(`Well, youre human so it shouldnt b so hard!`);
			txt(`We continued chatting mostly about the school and the biology, so it should be technically counted as studying.`);
			next();
			break;
			
		case 104:
			emo("bliss");
			mile.study_biology = -2;
			if(mile.slut < 6) mile.slut++; //+SLUT
			txt(`Hmmm... maybe... I was a bit horny and already in bed... I slipped a hand under the waistband of my @lower and began caressing myself...`);
			txt(`It was definitely more entertaining than the theory.`);
			//TODO
			next();
			break;	
	}
}


export const test_biology = (index)=> {
	switch(index){
		default:
		case 101:
			set.irl();
			emo("focus");
//TODO LOCAL EDUARD
			placetime(`classroom`, `biology`);
			txt(`@bio entered the room. He dragged Eduard the Skeleton to the front of the class and then checked his notebook.`);
			bio(`Miss @katsur?`);
			txt(`Oh no! I shivered when I heard my name. Reluctantly I stood up and went to him.`);
			bio(`Would you mind describing us the human arm?`);
			kat(`Of course not!`);
			if(mile.study_biology === 1){
				txt(`I confidently turned to the skeleton.`);
				kat(`Well, the shoulder girdle consists of the shoulder blade and the collar bone. And they are joined by...`);
				
				links_mix(
					[`Humerus.`, 110, ()=> counter.answer = 2],
					[`Femur.`, 110, ()=> counter.answer = -1],
				);
				
			}else{
				txt(`Faking confidence I turned to the skeleton.`);
				kat(`Well, there is the shoulder blade and the collar bone...`);
				txt(`I slowly started and pointed at the skeleton.`);
				kat(`And they are joined by...`);
				
				links_mix(
					[`Humerus?`, 110, ()=> counter.answer = 1],
					[`Femur?`, 110, ()=> counter.answer = -2],
				);
			}
			break;
			
		case 110:
			counter.points = counter.answer > 0 ? 1 : 0;
			counter.answer > 0 ? emo("relax") : emo("help");
			
			
			if(counter.answer === 2){
				txt(`He smilled and nodded. `);
			}else if(counter.answer === 1){
				bio(`Are you stating that or asking me that?`);
				kat(`Stating that?`);
				bio(`Fine. Continue, please.`);
			}else if(counter.answer === -1){
				bio(`No.`);
				kat(`I meant humerus!`);
				txt(`I quickly tried to fix my mistake.`);
			}else if(counter.answer === -2){
				bio(`Try it again!`);
				kat(`Well...`);
				bio(`Humerus. Continue please!`);
			}
			
			if(mile.study_biology === 1){
				kat(`Elbow joint is a hinge joint between the arm and the forearm...`);
			}else{
				kat(`Ehhh... then there's the elbow... the joint that connects the arm and the forearm... `);
			}
			txt(`I was describing Eduard's arm. `);
			if(mile.study_biology === -2){
				txt(`Now I seriously regretted I succumbed to my low urges and spend my free time masturbating instead of studying. Why was I such a stupid nympho!?!`);
			}else if(mile.study_biology === -1){
				txt(`This was all @eva & @sas fault! If they had not distracted me from studying, I would have learned everything and aced the exam. `);
			}
			kat(`It connects three bones. The thicker, outer one is...`);
			
			links_mix(
				[`...Tibia...`, 111, ()=> counter.answer = false],
				[`...Radius...`, 111, ()=> counter.answer = true],
			);
			break;
			
		case 111:
			if(counter.answer){
				emo("relax")
				txt(`@bio nodded. `);
				counter.points++;
			}else{
				emo("help")
				txt(`@bio frowned. `);
			}
			kat(`...and the thinner, inner is... `);
			links_mix(
				[`...Fibula...`, 112, ()=> counter.answer = false],
				[`...Ulna...`, 112, ()=> counter.answer = true],
			);
			break;
			
			
		case 112: 
			if(counter.answer){
				bio(`That's correct. `);
				counter.points++;
			}else{
				bio(`That's wrong. `);
			}
			
			//study + knowledge
			if(counter.points > 1 && mile.study_biology === 1){
				mile.grades -= 2;
				mile.biology = 2;
			//good luck
			}else if(counter.points > 1){
				mile.biology = 1;
				if(counter.points === 3) mile.grades -= 1;
				
			//study + wrong answers 
			}else if(mile.study_biology === 1){
				mile.biology = -1;
				if(counter.points === 0) mile.grades += 1;
				
			//everything wrong
			}else{
				mile.biology = -2;
				mile.grades += 2;
			}
			
			
			
			if(mile.study_biology === 1){
				txt(`@bio nodded and smiled at me, even thought his eyes struggled not to dart between my cleavage and legs.`);	
			}else{
				if(mile.slut <= 3){
					txt(`I noticed @bio eyes were darting between my cleavage and legs which made me even more nervous.`);
				}else{
					txt(`I noticed @bio eyes were darting between my cleavage and legs. I vainly hoped he might go easy on a sexy young girl and give me a hint but I was wrong.`);
				}				
			}
			
			
			if(mile.biology === 2){
				emo("happy");
				txt(`I got several more questions but I was able to answer them all correctly.`);
				bio(`1! Well done! `);
				
			}else if(mile.biology === 1){
				if(counter.points === 3){
					emo("relax");
					txt(`He gave me a few more questions and I was mostly lucky. My answers were far from perfect but considering how little I studied I think I did a good job.`);
					bio(`2! Sit down.`);
				}else{
					emo("help");
					txt(`He gave me a few more questions, gradually revealing my knowledge of the subject was far from perfect. But considering how little I studied I think I did a good job.`);
					bio(`3! Sit down.`);
				}
			}else if(mile.biology === -1){
				emo("anoy");
				txt(`I got several more questions but I was able to answer most of them correctly, redeeming at least a little my initial mistakes. I was very annoyed, I spent too much time studying but he was asking me all the wrong questions!  `);
				if(counter.points === 0){
					bio(`3! Sit down.`);
				}else{
					bio(`2! Sit down.`);
				}
			}else{
				emo("cry"); //emo??
				txt(`He gave me a few more questions. They were supposed to save me from the failing grade and show I knew at least something. They did the exact opposite.`);
				bio(`5! Sit down.`);
				txt(`He looked disappointed and almost offended by my lack of knowledge.`);
			}
			next();

/*
		case 110:
			mile.biology = -1;
			mile.grades += 1;
			
//TODO		Then there is the  ??-?? 
			kat(`Ehhh...`);
			if(mile.slut < 4){
				txt(`I noticed @bio eyes were darting between my cleavage and thighs which made me even more nervous.`);
			}else{
				txt(`I noticed @bio eyes were darting between my cleavage and thighs. I vainly hoped he might go easy on a sexy young girl and give me a hint but I was wrong.`);
			}
			kat(`Ehh...`);
			bio(`I see.`);
			txt(`He gave me a few more questions and I got lucky several times.`);
			bio(`3. Sit down.`);
			next();
			break;
			
		case 111:
			mile.grades += 2;
			mile.biology = -2;
			
//TODO		Then there is the  ??-?? 
			if(mile.slut < 4){
				txt(`I noticed @bio eyes were darting between my cleavage and thighs which made me even more nervous.`);
			}else{
				txt(`I noticed @bio eyes were darting between my cleavage and thighs. I vainly hoped he might go easy on a sexy young girl and give me a hint but I was wrong.`);
			}
			kat(`Ehh...`);
			bio(`I see.`);
			txt(`He gave me a few more questions. They were supposed to save me from the failing grade and show I knew at least something. They did the exact opposite.`);
			bio(`5. Sit down.`);
			txt(`He looked disappointed and almost offended by my lack of knowledge.`);
			next();
			break;
			
		case 120:
			mile.grades -= 2;
			mile.biology = 2;
//TODO !!! Elbow ??-??
			txt(`He smiled at me even though his eyes struggled not to dart between my cleavage and thighs.`);
			
			
			next();
			break;
			
		case 121:
			mile.biology = 1;
			mile.grades += 0;
			
//TODO !!! Elbow ??-??
			if(mile.slut < 4){
				txt(`@bio nodded but his eyes darting between my cleavage and thights were making me a bit nervous.`);
			}else{
				txt(`@bio nodded and smiled at me, even thought his eyes struggled not to dart between my cleavage and thighs.`);
			}
			
			next();
			break;
*/
	}
}


export const bio_chastise = (index)=> {
	switch(index){
		default:
		case 101:
			{
				placetime(`classroom`, `biology`);
				const details = sluttiness().details;
				const list = [`sexualy provocative outfits`];
				if(details.cleavage) list.push(`shameless clavage`);
				if(wears.bra) list.push(`the fact you don't even bother to wear a bra`);
				if(wears.skirt && details.pantiesBelow){
					list.push(`skirts so ridiculously short you're almost flashing your underwear`);
				}else if(wears.microSkirt){
					list.push(`improperly short skirts`);
				}
				if(details.midriff) list.push(`exposed stomach`);
				if(mile.makeup) list.push(`heavy makeup`);
				if(mile.hair_crazy) list.push(`${mile.hair_color} hair`);
				if(wears.heels) list.push(`high heels`);
				if(wears.fishnetLegwear) list.push(`fishnet stockings`);
				if(wears.collar) list.push(`suggestive collar`);


				let tex = ``;
				if(mile.open_legs) tex += `The way you provocatively sit with your legs wide apart, almost letting me to see under your skirt. `;
				if(mile.plugged) tex += `And how you're constantly fidgety, unable to sit calmly. `;
				if(details.panties) tex += `For gods sake, @katalt, I should not be able to tell you're wearing ${nameColor(PC.panties)} panties! `;
				
				emo("shy");
				
				bio(`And please, read the chapters about the musculoskeletal and circulatory systems again, I might orally examine somebody. That's everything. You can go. Miss @katsur? would you mind?`);
				txt(`@bio explained and then let everybody go, except for me. I wondered what he wanted.`);
				bio(`I would like to talk with you privately.`);
				txt(`I followed him to his office.`);
				kat(`What would you like to talk about, @bio?`);
				bio(`About you. Respective of your recent behavior and the way you're presenting yourself.`);
				kat(`I'm still not sure what exactly do you mean?`);
				bio(`Your flagrant sexuality! About your ${and(list)}! ${tex}`);
				kat(`Oh. You mean those things.`);

				if(mile.biology < 0){
					bio(`Yes. And your biology knowledge is unsatisfactory. Think about your grades! You should spend more time studying and less on doing your makeup and picking your clothes!`);
				}else{
					bio(`I know you're not a bad student, your grades are good. But if they weren't, I would be already complaining to your form teacher or the principal.`);
				}
				kat(`Oh.`);
				bio(`I don't want to shame you, miss @katsur! I understand you want to look attractive. But not in my classes! You're distracting other students!`);
				txt(`I admit I was a bit stunned. I did not expect such a tirade, especially not from @bio!`);

				link(`Be embarrassed. `, 102);
				con_link(mile.slut > 5, `low slut`,`Seduce him. `, 105);
				con_link(mile.sub < 4, `submissive`,`Be bratty.  `, 104);
				con_link(mile.sub > 6 && mile.slut > 5, `dominant or low slut`,`Ask to be punished.  `, 103);
				

				
			}
			break;

		case 102:
				mile.bio_chastise = 1;
				kat(`Oh no! I'm extremely sorry, sir! I... I was just experimenting with a few new things...`);
				bio(`I did not tell him I was blackmailed by three horny nerds who loved to see me dressing up like a hooker.`);
				kat(`It absolutely wasn't my intention to be distractive!`);
				bio(`You don't have to be embarrassed. And you don't have to stop experimenting. But maybe just don't experiment with everything at the same time? It's too much!`);
				kat(`I'm very sorry! I'll try to wear something a little bit less slutty.`);
				bio(`That would be a good thing to do. That's everything I wanted, you can go, miss @katsur!`);
				next();
				break;
				
		case 103:	
				mile.sub++;
				mile.bio_chastise = 2;
				emo("imp");
				kat(`I'm very sorry, you're right, I look like a whore.`);
				bio(`Please, miss @katsur! I never said <i>whore</i>! I just believe you should wear a bit less provocative outfits.`);
				kat(`My behavior was absolutely unacceptable. I think I deserve to be punished.`);
				bio(`That won't be necessary! I just wanted to explain to you my problem with you and now when you see my point, further disciplining would be unnecessary!`);
				kat(`No, you don't know me! When people chastise me, I pretend I listening and that I'm sorry, but I'm actually ignoring them. I won't change unless you punish me.`);
				bio(`Oh. Fine, in that case, I should punish you, I guess. I could tell your form teacher...`);
				kat(`Please, don't get @maj involved!`);
				bio(`Fine, so what should I do?`);
				kat(`Since I am a misbehaving young girl, you should punish me like a misbehaving young girl!`);
				txt(`To show him what I meant I obediently bent over his desk to nicely present him my ass.`);
				bio(`Miss, @katsur! You can't be serious!`);
				kat(`I am. I believe this is the most proper punishment. Please, don't get anybody else involved and don't ruin my afternoon with extra homework!`);
				txt(`He reluctantly made a step forward and I pulled down my @lower, showing him ${wears.panties ? "my @panties" : "I was wearing no panties"}.`);
				bio(`If you really want it...`);
				kat(`I insist!`);
				txt(`He lightly slapped my ass.`);
				kat(`You have to be rougher. It barely hurt.`);
				bio(`I don't want to hurt you, miss @katsur.`);
				kat(`You have to, this is supposed to be punishment!`);
				bio(`Fine!`);
				txt(`He spanked my butt four more times. When he was done, my ass was red and itching.`);
				kat(`Thank you, sir! I'll try to improve my behavior!`);
				bio(`...great!`);
				txt(`I fixed my clothes, smiled at him and left.`);
				next();
				break;
				
		case 104: 
				mile.sub--;
				mile.bio_chastise = 3;
				emo("angry");
				txt(`His accusations were serious and reasonable. Therefore the best defense was an attack.`);
				kat(`How is that my business? The way I dress or behave is only my problem, not theirs. You should ask <i>them</i> to not get distracted and focus on biology, not bother me!`);
				bio(`That isn't so easy, miss @katsur! Men, especially in their teens, can't not be distracted by an attractive woman dressed like... dressed to preset herself provocatively.`);
				kat(`Still seems like their problem. And are you talking only about other students? Or am I distracting teachers too?`);
				txt(`He frowned but then slowly nodded.`);
				bio(`Yes, if you need to know, I personally find you distractive too.`);
				kat(`You're sexually aroused by barely eighteen-year-old students because your old bland wife is unable to sexually satisfy you and it's somehow my fault?`);
				txt(`I saw his wife once or twice with him and I was not very impressed. I fully understood he would prefer me over her, I just wanted to make him embarrassed and ashamed. But I made a miscalculation.`);
				link(`Punishment. `, 106);
				break;
				
		case 105: 
				mile.slut++;
				mile.bio_chastise = 4;
				emo("imp");
				kat(`Am I distracting only other students? Or am I distracting teachers too?`);
				txt(`He frowned but then slowly nodded.`);
				bio(`Yes, if you need to know, I personally find you distractive too.`);
				kat(`Because you want to have sex with me.`);
				bio(`MISS @KATSUR! No, I don't want to have sex with you, miss @katsur! The fact I find you biologically sexually attractive doesn't change the fact you're my under${String.fromCharCode(97)}ge student -`);
				kat(`I'm not under${String.fromCharCode(97)}ge, I'm already adult for three months.`);
				txt(`I explained and moved closer to him.`);
				bio(`- and that I'm not available!`);
				kat(`I believe seeing me fully naked and having sex with me would make my body mundane to you and you wouldn't be so distracted anymore!`);
				bio(`I'm pretty sure my wife wouldn't approve of that. And I refuse to discuss this with you further!`);
				kat(`She doesn't have to know! Why do you care about that old bland broad when the hottest girl in the school is willing to do <i>anything</i> for you!`);
				txt(`I saw his wife once or twice with him and I was not very impressed. And it was a nice challenge to make him cheat on her. My fingers lightly brushed against his crotch. But I made a miscalculation.`);
				link(`Punishment. `, 106);
				break;

		case 106: 
				rollUpSkirt(2);
				emo("pain");
				bio(`DON'T DARE TO TALK ABOUT MY WIFE THAT WAY!`);
				txt(`He yelled at me and grabbed my hand. Before I realized what was going on, he twisted my army behind my back and bend me over a desk. Then he ${wears.skirt ? "flipped up my @lower" : "reached to unbutton and pull down my @lower"}. I was getting horny but ${wears.panties ? "instead of pulling down my panties too" : ""} he roughly smacked my ass.`);
				bio(`You little floozie! I really tried to be nice and reasonable! I'll have to punish you the way you'll understand!`);
				txt(`He continued harshly slapping my butt. It was not a light, playful spanking, he really wanted to show me the errors of my ways and did not care about my whining or kicking.`);
				txt(`My poor ass was red and on fire before he ended. When he decided, he had enough, he pushed me away and looked down at me like I was a pile of garbage:`);
				bio(`I hope you'll remember you should always behave and be respectful! Now get the fuck out of my office!`);
				next();
				break;
	}
}





//CHEMISTRY - only if qaa's lover after the first sex!
export const study_chemistry = (index)=> {

//like 10!!
	const questions = [
		{
			question: "Isotopes have different number of...",
			right: "Neutrons.",
			wrong: "Electrons.",
		},
		{
			question: "Iodine and Fluorine are...",
			right: "Halogens.",
			wrong: "Noble gases.",
		},
		{
			question: "Alkali metals reacting with water produce...",
			right: "Hydroxides.",
			wrong: "Acids.",
		},
		{
			question: "Name example of Alkali metal...",
			right: "Potassium.",
			wrong: "Gallium.",
		},
		{
			question: "Distilation is used to separate substances with different...",
			right: "Boiling temperature.",
			wrong: "Mass.",
		},
		{
			question: "How many valence electrons has carbon? ",
			right: "Four.",
			wrong: "Two.",
		},
		{
			question: "Avogadro constant is related to...",
			right: "Number of particles.",
			wrong: "Mass of nucleons.",
		},
		{
			question: "How many oxygen atoms are contained in the water?",
			right: "One.",
			wrong: "Two.",
		},
		{
			question: "Oxidized copper is...",
			right: "Green.",
			wrong: "Red.",
		},
		
		{
			question: "How many carbon atoms are contained in the ethanol?",
			right: "Two.",
			wrong: "Four.",
		},
		{
			question: "H2O2 is chemical formula of...",
			right: "Hydrogen peroxide.",
			wrong: "Propane.",
		},
		{
			question: "Hydrogen atom has ...",
			right: "One electron.",
			wrong: "Two electrons.",
		},
		{
			question: "Table salt is...",
			right: "Sodium chloride.",
			wrong: "Calcium fluoride.",
		},
		{
			question: "The most abundant element in the earth’s atmosphere is...",
			right: "Nitrogen.",
			wrong: "Oxygen.",
		},
		{
			question: "Dynamite is an explosive made of...",
			right: "Nitroglycerin.",
			wrong: "Trinitrotoluene.",
		},
		{
			question: "H2SO4 is formula of...",
			right: "Sulfuric acid.",
			wrong: "Formic acid.",
		},
		
		
	];
	
	//like 4 wrong
	
	function epilogue(){
		if(counter.wrong > 2){
			mile.study_chemistry = 1;
			mile.grades--;
			txt(`Needless to say, after such throughout studying, I did pretty well.`);
		}else{
			mile.study_chemistry = 2;
			mile.grades -= 2;
			txt(`Our studying method might be unconventional but the next day I totally aced the test!`);
		}
		next("Next. ", quickLoadOutfit);
	}
	
	
	switch(index){
		default:
		case 101:
			quickSaveOutfit();
			counter.index = 0;
			counter.correct = 0;
			counter.wrong = 0;
			counter.questions = ra.shuffle(questions);
			//questions = ra.shuffle(questions);
			//questions = ra.shuffle(questions);
//TODO EMOJI	
			placetime("my room");
			txt(`I had to study for the tommorrow test. But as soon as I opened my notes, @qaa texted me:`);
			qaa(`Wanna hang out? 😉`);
			kat(`Sry, I study for the test 😭`);
			qaa(`Me to 😢`);
			kat(`Maybe we could study together 😉?`);
			qaa(`👍`);
			kat(`I'll be there in ten!`);
			txt(`@qaa was a smart guy. He surely could help me and explain me the things I did not fully grasped. Who was I kidding - I welcomed any distraction from the boring studying.`);

			link(`Go to @qaa's place. `, 102);
			break;

		case 102:
			placetime("@qaa's room");
			qaa(`Hey!`);
			kat(`Hello!`);
			qaa(`Are you ready for hard studying?`);
			kat(`Yeah!`);
			txt(`We went to his room. He sat down in a computer chair and opened his notes while I comfortably lay down on his bed.`);
			kat(`What should we do?`);
			qaa(`Maybe I could try to give you questions and you could try to answer them?`);
			kat(`Sounds like a good idea!`);
			qaa(`Or maybe we should make it more interesting.`);
			kat(`How?`);
			qaa(`Dunno. Like you stripping a piece of your clothes every times you get the answer wrong?`);
			kat(`Why?`);
			qaa(`To motivate you.`);
			kat(`How's that supposed to motivate me?`);

			if(mile.slut < 2){
				qaa(`I know how bashful you are!`);
			}else if(mile.slut > 11){
				txt(`I smiled at him:`);
				kat(`You can just say you want to see me naked!`);
				qaa(`Yeah, but this would be more thrilling!`);
			}else if(mile.sub < 3){
				qaa(`Dunno, sorry, it was just a silly idea.`);
			}else{
				qaa(`Come on! You aren't afraid of the challenge, are you?`);
			}
			kat(`Fine!`);
			qaa(`Okay, let's start!`);
			link(`First question. `, 200);
			break;
			
		case 200:
			{
				const a = counter.questions[counter.index];
				if(!a){
					txt(`Error: question not found.`);
					link(`Correct answer. `, 220);
					link(`Wrong answer. `, 210);
					next(`Quit event. `);
				}else{
					txt(a.question)
					links_mix(
						[a.right, 220],
						[a.wrong, 210],
					)
				}
			}
			break;
			
		case 210:
			counter.wrong++;
			counter.index++;
			qaa(`Wrong!`);

			if(PC.upperOuter){
				kat(`So now I have to strip?`);
				qaa(`Yeah, give me your @upperOuter!`);
				remove("upperOuter");
			}else if(PC.lower){
				qaa(`Okay. Now give me your @lower!`);
				kat(`My @lower?!`);
				qaa(`Yeah! I want to see your panties!`);
				if(!wears.panties){
					txt(`I ${mile.slut < 5 ? "blushed" : "smirked"}:`);
					kat(`I'm not wearing any!`);
					txt(`Excited he watched me to strip:`);
					qaa(`Oh my! You'll be compeltely nude way faster than I thought!`);
					txt(`I frowned at him and crossed my leg to not show him anything innapropriate${wears.hairy ? " with the exception of glimpse of my pubic hair. " : ""}. `);
				}else{
					kat(`Fine!`);
					txt(`I ${mile.slut < 4 ? "blushed" : "smirked"} when I pulled down my @lower and showed him my @panties.`);
				}
				remove("lower");
			}else if(PC.socks){
				qaa(`I would like to see your feet without those @socks.`);
				//stockings? pantyhose? sexier
				if(wears.pantyhose || wears.stockings){
					kat(`My @socks are not sexy enough for you?`);
					txt(`I teasingly wigled with my toes.`);
					qaa(`Sure they are! But now strip them!`);
				}
				txt(`I pulled down my @socks.`);
				kat(`Satisfied?`);
				qaa(`extremely!`);
				txt(`He playfully tickled my feet.`);
				remove("socks");
			}else if(PC.upper){
				qaa(`Okay, drop your @upper!`);
				if(wears.collar){
					kat(`You don't want to ask me to strip my @collar first? To slowly build the anticipation?`);
					qaa(`No, I like you topless and collared.`);
					kat(`Perv!`);
				}
				txt(`I very slowly stripped the @upper. @qaa's eyes were bolted on my svelte body.`); //upper??
				if(!wears.bra){
					kat(`You're still amazed by staring at my bare breasts, aren't you?`);
					qaa(`I don't think I'll get ever tired of staring at your tits.`);
				}
				remove("upper");
			}else if(PC.bra){
				txt(`He gleefully smiled:`);
				qaa(`Take off your bra, please!`);
				kat(`Okay!`);
				txt(`I reached behind my back. He was completely mesmerised by the sight in front of him.`);
				kat(`You're still amazed by staring at my bare breasts, aren't you?`);
				qaa(`I don't think I'll get ever tired of staring at your tits.`);
				remove("bra");
			}else{
				qaa(`Wrong again!`);
				kat(`That's not fair! I can't focus on the questions when you're touching me!`);
				txt(`His fingers were gently caressing my leg.`);
				qaa(`Sorry! I can't help it! You calves are just so sexy!`);
				kat(`Maybe the stripping was a stupid idea! I don't think I learned anything!`);
				qaa(`Maybe we should get rid of our hornyness first. That would help us to focus better!`);
				if(mile.slut > 12){
					kat(`Yeah! Let's fuck! We can study later!`);
				}else{	
					kat(`I'm not horny! You are!`);
					qaa(`Not even a little?`);
					kat(`Well... maybe a little...`);
				}
				txt(`@qaa leaned closer, cupped my face and then went for a kiss.`);
				qaa(`Oh @kat!`);
				txt(`We were making out and he deliberately moved on top of me.`);
				if(wears.panties){
					txt(`His fingers rubbed my snatch, he craved to do it ever since I exposed it to him.`);
				}else{
					txt(`I raised my ass and pulled down my @panties and his fingers instantly moved there.`);
				}
				txt(`The foreplay did not last long. Our messing around and my provocative slow striptease got us both aroused.`);
				qaa(`Oh yes!`);
				txt(`@qaa plunged deep inside me. He was thrusting hard, burriyng himself inside me. We both completely succumbed to our lust. @qaa pushed us to the edge and beyond, climaxing in my pussy.`);
//TODO				
				if(counter.correct > 1){
					mile.study_chemistry = -1;
					mile.grades++;
					txt(`Needless to say, we did not get back to the chemistry that evening and the test did not went particulary well.`);
				}else{
					mile.study_chemistry = -2;
					mile.grades += 2;
					txt(`Needless to say, we did not get back to the chemistry that evening and the test was a total disgrace.`);
				}
				
				remove("panties");
				log_sex("sex", "qaa");
				return next("Next. ", quickLoadOutfit); //next();
			}
			
			link(`Next question. `, 200);
			break;
		
		
		case 220:
			counter.correct++;
			counter.index++;
			qaa(`Correct!`);
			
			if(counter.correct === 1){
				kat(`You look almost disappointed i got it right!`);
				qaa(`I'm not! I'm happy you're doing well!`);
			}else if(counter.correct === 2){
				if(counter.index === 2){
					txt(`@qaa smirked: `);
					qaa(`Come on, @kat? Can't you get at least one wrong?`);
					kat(`No!`);
					txt(`I stick out my tongue at him.`);
				}else{
					kat(`Awesome! `);
					txt(`I smirked at him, I could see he would prefer me to continue undressing.`);
				}
			}else if(counter.correct === 3){
				kat(`Ask me something harder! `);
				
			}else if(counter.correct === 4){
				if(counter.wrong <= 2){
					txt(`@qaa shrugged: `);
					qaa(`I imagined this a bit differently.`);
					kat(`Just admit it! You thought I'm a stupid and this was just a cheap ploy to get me naked!`);
					qaa(`Of course not!`);
				}else{
					kat(`Don't ogle me and ask me another question! `);
					//txt(`He smiled and with delight watched my half-naked body. `);
					
				}
			}else{
				txt(`@qaa studied his notes: `);
				qaa(`Hmmm, I think that's everything!`);
				
				if(counter.wrong === 0){
					kat(`I think I'm ready for the test. `);
					qaa(`You didn't even removed your @upper!`);
					kat(`You were right, not having to strip in front of annoying horny pervert was a great motivation! `);
				}else if(counter.wrong > 2){
					kat(`That was close! You almost got me completely naked! `);
					qaa(`Maybe we can go throught geography or physics too? `);
					kat(`No thanks, maybe next time. `);
				}else{
					kat(`Great! Do you think I'll ace the test?!`);
					qaa(`Yeah! You're smarter than I thought!`);
					kat(`Is that a compliment or an insult?`);
					qaa(`I meant it as a compliment!`);
					kat(`I'll try to not be offended!`);
				}
				txt(`I began packing my notes.`);
				qaa(`You're already going?!`);
				kat(`Yeah, I have to go to play a virtual game because I'm blackmailed by three pathetic nerds!`);
				qaa(`Don't I deserve at least a quick reward?`);


				con_link(mile.slut < 7, `high slut`,`A kiss. `, 222);
				con_link(mile.slut > 1, `low slut`, `A blowjob.`, 223);
				con_link(mile.a_dom > 1, `not dominant gf`, `No. `, 221);
				return;
			}
			link(`Next question. `, 200);
			break;
			
		case 221:
				mile.a_dom++;
				emo("anoy");
				kat(`No. I spent an evening with you and even gave you a chance to see me naked. What more you would want!`);
				qaa(`Oh, I guess that's fair!`);
				txt(`I had to keep him on a short leash. I did not wish him getting used to being sexually awarded for anything minor he does for me.`);

				mile.study_chemistry_reward = 1;
				epilogue();
				break;	
				
				
		case 222:
				emo("imp");
				txt(`I moved hair away from my face and then leaned closer to hive him a long, romantic kiss on his lips.`);
				kat(`Thank you for help, my prince!`);
				qaa(`At your service, my fair damsel!`);
				
				mile.study_chemistry_reward = 2;
				epilogue();
				break;
		
		case 223:
				log_sex("bj", "qaa");
				kat(`Fine! But just a quick one! Take off your pants!`);
				txt(`He giggled and unbuttoned his jeans while I was kneeling in front of him. I took his cock, fondled it and then engulfed it in my mouth.`);
				qaa(`Oh @kat!`);
				txt(`@qaa was patting my head as I was sucking him until he orgasmed in my mouth.`);
				kat(`Thanks for your help with the chemistry!`);
				txt(`I said after swallowing all his semen.`);
			//TODO - less slut?
				qaa(`You're welcome! And don't be afraid to ask if you need a help with math or literature or anything else!`);
				
				mile.study_chemistry_reward = 3;
				epilogue();
				break;	
	}	
}


export const distilation = (index)=> {
	switch(index){
		case 101:
			quickSaveOutfit();
			placetime("laboratory", "chemistry lesson");
			present([1,"Chemistry Teacher"]);
			txt(`The chemistry lesson did not take place in the usuall classroom but in the laboratory. `);
			npc(1, `Today we'll do several experiments related to the separation of different substances! How many of you are? Divide into pairs.`);
			 txt(`I was thinking whether I should pick @eva or @sas. @sas was more dutiful and diligent and able to do the bulk of the assignment. On the other hand, working with @eva was always more entertaining. Even though we spent more time gossiping than doing the actual work.`);
			link(`@eva? `, 102, ()=> counter.eva = true);
			link(`@sas? `, 102, ()=> counter.eva = false);
			break;
		
		default:
		case 102:
			emo("shock");
			if(counter.eva){
				mile.eva++;
				kat(`@eva?`);
				eva(`Sorry!`);
			}else{
				mile.sas++;
				kat(`@sas?`);
				sas(`Sorry!`);
			}
			
			txt(`With horror, I realized that before I finished my internal monologue, they had already formed a pair together, completely abandoning me. Why would @eva  betray me and choose helplessly dull and annoyingly meticulous @sas? Why would @sas want to work with that lazy slut @eva instead of me?`);
			txt(`I looked around. Only one remaining was`);

			if(mile.a_burned){
				link(`@qaa. `, 250);
			}else{
				link(`@qaa. `, 200);
			}
			
			link(`@qbb. `, 300);
			link(`@qcc. `, 400);
			break;


	//aaa
		case 200:
			mile.distilation_nerd = 1;
			emo("relax");
			qaa(`Hey, @kat! It seems we're going to be working together!`);
			kat(`Great! What are we supposed to do?`);
			qaa(`Hmmm...`);
			txt(`He was carefully reading the paper with our instructions.`);
			qaa(`I think we can divide the work, I'll prepare the chemicals and you can assemble the still.`);
			kat(`Fine!`);
			txt(`He gave me the less complicated task which might be a bit condescending but I really did not mind.`);

			link(`Assemble the still. `, 201);
			break;

		case 201:
			emo("anoy");
			txt(`I began assembling the laboratory glassware, following the schema in our instructions, connecting the Bunsen burner to the gas and condenser to water, while @qaa was mixing the chemicals.`);
			qaa(`Are you done?`);
			txt(`@qaa finished first.`);
			kat(`A moment... yeah, done.`);
			txt(`However, instead of being satisfied with my work, he immediately began readjusting the height of the round-bottomed flask and the condenser.`);
			kat(`What are you doing! Did I do something wrong?!`);
			qaa(`No! You did great! I'm just a bit improving it... wait, you actually connected the water incorrectly.`);
			kat(`What do you mean?!`);
			qaa(`Look at the schema! The water should enter the bottom of the condenser and exit the top. Not the other way. But no big deal, that's an easy mistake to make.`);
			kat(`So?! What's the difference?!?`);
			qaa(`Well, I guess you don't want the coldest water to cool the hottest vapor or something like that. Don't worry, I'll fix it!`);
			txt(`He began disconnecting the condenser and I had to push him away.`);
			kat(`Hands off, that's my job!`);
			txt(`I was so annoyed I was not paying full attention when I was reattaching the rubber tubes to the condenser and faucet. Which should have horrible consequences.`);
			
			link(`Catastrophe  `, 600);
			break;
			
			
	//aaa burned	
		case 250: 
			emo("anoy");
			mile.distilation_nerd = 1;
			kat(`Hey, it seems I'll have to work with you.`);
			qaa(`No way!`);

			if(mile.sub > 0){
				kat(`Too bad, your friends left you behind and we're only two left!`);
				qaa(`I'm surprised both @eva and @sas abandoned you. What is going on? They don't like you anymore?`);
				txt(`@qaa arrogantly smirked.`);
				kat(`Nothing is wrong! Shut up! What should we do?`);
			}else{
				kat(`Too bad, we're only two left!`);
				qaa(`Heh, you were left behind by your <i>friends</i>?`);
				kat(`No, we're taking turns, this time @eva works with @sas, the next time I'll work with ${counter.eva ? "@eva" : "@sas"}. Why did you get abandoned?`);
				txt(`@qaa mumbled something and was pretending he is reading our instructions.`);
				kat(`Okay, what should we do?`);
			}
			qaa(`Your job will be to stay here and don't touch anything.`);
			kat(`Are you serious?`);
			qaa(`Yes. Doing everything on my own will be faster than fixing everything you'll do wrong.`);
			txt(`I was outraged by his condescending lack of trust.`);
			kat(`Okay, but you're not ever allowed to say I let you do everything without helping you with anything!`);
			txt(`He was thinking for a moment and then shrugged:`);
			qaa(`Fine, you can try to assemble the still. But don't touch anything else!`);

			link(`Assemble the still. `, 251);
			break;
			
			
		case 251: 
			txt(`I began assembling the laboratory glassware, following the schema in our instructions, connecting the Bunsen burner to the gas and condenser to water, while @qaa was mixing the chemicals. The teacher was walking through the laboratory, checking our progress.`);
			npc(1, `Do you understand everything? How are you doing?`);
			qaa(`Not so great, @katalt incorrectly connected the inflow of water to the top of the condenser instead of the bottom!`);
			txt(`Immediately maliciously shared @qaa. However, it did not have the result he expected.`);
			npc(1, `Then why you didn't tell her! You're supposed to work together!`);
			txt(`The teacher scolded  @qaa in front of the whole class.`);
			kat(`Hehe, better learn to be a better team player!`);
			txt(`I rubbed it in when the teacher moved to the next group.`);
			qaa(`It's your fault, you bimbo!`);
			kat(`Damn yeah, it is!`);
			txt(`I just laughed. I was so amused I was not paying attention when I was reattaching the rubber tubes to the condenser and faucet again. Which should have horrible consequences.`);
			
			link(`Catastrophe.  `, 600);
			break;

	//bbb
		case 300: 
			mile.distilation_nerd = 2;
			emo("anoy");
			kat(`Hey, can I work with you?`);
			qbb(`Absolutely no!`);

			if(mile.sub > 0){
				kat(`Too bad, your friends left you behind and we're only two left!`);
				qbb(`I actually prefer to work alone without being distracted. But I'm surprised both @eva and @sas abandoned you. What is going on? They don't like you anymore?`);
				txt(`@qbb loftily smirked.`);
				kat(`Nothing is wrong! Shut up! What should we do?`);
			}else{
				kat(`Too bad, we're only two left!`);
				qbb(`Heh, you were left behind by your <i>friends</i>?`);
				kat(`No, we're taking turns, this time @eva works with @sas, the next time I'll work with ${counter.eva ? "@eva" : "@sas"}. But you seem to be abandoned. Are you too insufferable even for the other nerds?`);
				txt(`@qbb mumbled something and was pretended he was reading our instructions.`);
				kat(`Okay, what should we do?`);
			}
			qbb(`Your job will be to stay here and don't touch anything.`);
			kat(`Are you serious?`);
			qbb(`Yes. Doing everything on my own will be faster than fixing everything you'll do wrong.`);
			txt(`I was outraged by his condescending lack of trust.`);
			kat(`${mile.sub < 2 ? "Fuck you" : "Come on"}, I'm not an egghead but I'm not stupid and I'm more than capable to assemble a still!`);
			txt(`He was thinking for a moment and then shrugged:`);
			qbb(`Fine! But don't touch anything else!`);

			link(`Assemble the still. `, 301);
			break;
			
			
		case 301: 
			txt(`I began assembling the laboratory glassware, following the schema in our instructions, connecting the Bunsen burner to the gas and condenser to water, while @qbb was mixing the chemicals. The teacher was walking through the laboratory, checking our progress.`);
			npc(1, `Do you understand everything? How are you doing?`);
			qbb(`Not so great, @katalt incorrectly connected the inflow of water to the top of the condenser instead of the bottom!`);
			txt(`Immediately maliciously shared @qbb. However, it did not have the result he expected.`);
			npc(1, `Then why you haven't told her! You're supposed to work together!`);
			txt(`The teacher scolded  @qbb in front of the whole class.`);
			kat(`Hehe, better learn to be a better team player!`);
			txt(`I rubbed it in when the teacher moved to the next group.`);
			qbb(`It's your fault, you bimbo!`);
			kat(`Damn yeah, it is!`);
			txt(`I just laughed. I was so amused I was not paying attention when I was reattaching the rubber tubes to the condenser and faucet again. Which should have horrible consequences.`);
			
			link(`Catastrophe. `, 600);
			break;

	//ccc
		case 400:
			emo("imp");
			mile.distilation_nerd = 3;
			kat(`Hey! It seems we were both abandoned by our friends!`);
			qcc(`Yeah, it seems so.`);
			kat(`Come on, be more excited, it's going to be fun!`);
			qcc(`Hmmm.`);
			kat(`What are we supposed to do?`);
			txt(`I hoped for a quick summary but instead he handed me the paper with our assignment. Well, the task was pretty straightforward and there were even pictures.`);
			kat(`Okay, let's do it!`);
			txt(`I finished reading and reached for a beaker and chemicals.`);
			qcc(`What are you doing? You have to measure the volume.`);
			kat(`The paper says 40 milliliters. You can't pour a shot without measuring it?`);
			qcc(`Well, no. Maybe I could do it while you assemble the still? I think... maybe we should try to be as accurate as possible?`);
			txt(`He bashfully suggested. `);
			kat(`Okay.`);
			qcc(`And please follow the schema exactly.`);
			kat(`OMG, fine!`);

			link(`Assemble the still. `, 401);
			break;
			
			
		case 401:
			txt(`I began assembling the laboratory glassware, following the schema in our instructions, connecting the Bunsen burner to the gas and condenser to water, while @qcc was mixing the chemicals. The teacher was walking through the laboratory, checking our progress.`);
			npc(1, `Do you understand everything? How are you doing?`);
			kat(`We divided the work and we're doing great!`);
			txt(`I proudly claimed him but he glanced at the still and instructed us:`);
			npc(1, `You connected the inflow of water to the top of the condenser instead of the bottom. You should connect it to the bottom, to ensure the whole condenser is filled and the heat transfer is efficient.`);
			kat(`Oops! I'm fixing it right away! ...Don't say anything!`);
			txt(`I warned smug @qcc.`);
			qcc(`I wasn't going to say anything! I think you're doing a great job!`);
			txt(`I reattached the rubber tubes connecting the condenser and the water faucet. I was working hastily which should have horrible consequences.`);
			
			link(`Catastrophe.  `, 600);
			break;
	
	//catastrophe
		case 600:
			emo("shock");
			showAll(); //when not showed all, the updateDraw will cancel the alpha
			PC.upper.alpha = 0.25;
			updateDraw();
			
			txt(`When we later started with the distillation, I - without thinking - turned the faucet on, all the way. The negligently attached rubber tube did not withstand the maximal water pressure and slipped off. The water suddenly began splashing everywhere.`);
			kat(`FUCK!`);
			if(mile.distilation_nerd === 3){
				qcc(`WHAT THE FUCK!`);
			}else if(mile.distilation_nerd === 2){
				qbb(`WHAT THE FUCK!`);
			}else{
				qaa(`WHAT THE FUCK!`);
			}			
			kat(`I DON'T KNOW!`);
			txt(`I panicked.`);
			if(mile.distilation_nerd === 3){
				qcc(`DO SOMETHING!`);
			}else if(mile.distilation_nerd === 2){
				qbb(`DO SOMETHING!`);
			}else{
				qaa(`DO SOMETHING!`);
			}
			txt(`I was forced to lean into the torrent of cold water and turn the faucet off.`);
			kat(`Fuck!`);
			if(mile.distilation_nerd === 3){
				txt(`Pairing with @qcc was a huge mistake. He was usually prone to such accidents and now it seemed his bad mojo was affecting me too. I thought he will be sassy and enjoy my misfortune but when I turned to him, he did not say anything, he just stared at my chest. I looked down and realized my drenched @upper was transparent and tightly clinging to my skin like I was a participant in a wet t-shirt contest. Since I was wearing no bra, I could as well be topless.`);
			}else if(mile.distilation_nerd === 2){
				txt(`I thought @qbb will be angry about me ruining the experiment but instead seemed amused, his eyes on my chest. I looked down and realized my drenched @upper was transparent and tightly clinging to my skin like I was a participant in a wet t-shirt contest. Since I was wearing no bra, I could as well be topless.`);
			}else{
				txt(`I was afraid @qaa will be smug and rub it in but instead he just speechlessly stared at my chest.`);
				if(mile.a_burned){
					qaa(`@kat! You exhibionistic tramp!`);
				}else{
					qaa(`Ehh, @katalt...`);
				}
				txt(`He pointed down and I realized my drenched @upper was transparent and tightly clinging to my skin like I was a participant in a wet t-shirt contest. Since I was wearing no bra, I could as well be topless.`);
			}				
			txt(`And of course, the incident attracted the attention of the whole class. Everybody was staring at me. Some people even reached for their phones.`);

			con_link(mile.slut < 9, `high slut`, `Be embarrassed. `, 630);
			con_link(mile.slut > -2, `low slut`, `Pretend indifference. `, 620)
			con_link(mile.slut > 7, `low slut`, `Be shameless. `, 610)
			break;
			
	case 610: 
			emo("imp");
			mile.slut++;
			mile.stat--;
//TODO GRADES?
			mile.distilation_end = 3;
			
			kat(`Ooops, I had an accident.`);
			txt(`I made a joke. I loved how everybody was staring at my almost-exposed @tits. Horny boys with eyes full of lust, fellow girls with eyes full of envy.`);
			kat(`Oh no, my tits!`);
			txt(`I looked down, playfully pretending only now I discovered my @upper was transparent.`);
			kat(`Damn, I wish I didn't forget to wear my bra today.`);
			qbb(`You shouldn't stay in wet clothes.`);
			kat(`You're right, maybe I shoud-`);
			npc(1, `Enough! This is a laboratory! You can't undress here!`);
			txt(`He feebly began but my smug smile enranged him:`);
			npc(1, `You can strip in front of others in your free time - or at work - not here! Is acting like a slut and flashing your breast the only way to make yourself interesting? Clean up the mess, get changed and get back to work! The lesson is ending soon and you won't get any extra time, I don't care about your tomfoolery!`);
			kat(`S... sorry!`);
			txt(`Nobody had ever seen him so furious. The whole class was silent when he was shouting at me and took a moment before they again began quietly chatting and chuckling about how he owned me.`);
			
			if(mile.distilation_nerd === 3){
				qcc(`Seriously, did you have to make him angry?`);
				txt(`We quickly worked but it was obvious we will not be able to finish in time and our grades will suffer.`);
				kat(`I had no idea he'll be so outraged by a beautiful female form!`);
	
			}else if(mile.distilation_nerd === 2){
				kat(`Okay, I'm a trainwreck who fucks up everything. I had no idea he'll be outraged by beautiful female form so much!`);
				txt(`We quickly worked but it was obvious we will not be able to finish in time and our grades will suffer.`);
				qbb(`Don't worry! Seeing you humiliated and called slut by our chemistry teacher was more than worth the bad grade!`);			
			}else{
				if(mile.a_burned){
					qaa(`You stupid cow!`);
					txt(`We quickly worked but it was obvious we will not be able to finish in time and our grades will suffer.`);
					kat(`I had no idea he'll be so outraged by a beautiful female form!`);
				}else{
					qaa(`Do you always have to act like such an exhibitionistic slut?`);
					txt(`I was not sure whether he was angry because it was obvious we will not be able to finish in time or because I let everybody ogle my boobs.`);
					kat(`Yeah, I do. Although I had no idea he'll be so outraged by a beautiful female form.`);
				}
			}	
	
			next(`The end of laboratory classes. `, quickLoadOutfit);
			break;
			
	case 620:
			mile.distilation_end = 2;
			emo("imp");
			
			kat(`Ooops, I had an accident.`);
			if(mile.slut < 9){
				txt(`I make a joke. Everybody was staring at my @tits and I was horribly embarrassed. However, I knew if I would admit how awful I felt, it would be much worse. The trick was not let people to see your weakness.`);
			}else{
				txt(`I made a joke. My @tits being exposed was not the worst thing ever but the accident made me look like a clumsy dork. And I could not admit how embarrassed I felt, the trick was not let people to see your weakness.`);
			}
			txt(`People were laughing with me rather than at me but I still wished they put away their phones.`);
			npc(1, `Miss @katsur! What were you doing-`);
			txt(`The old chemistry teacher was about the scold me but then his eyes slipped down. The class noticed his bewilderment and laughed even harder. But he quickly regained his composure and sharply ordered:`);
			npc(1, `Everybody! Go back to your work! You're running out of time!`);
			txt(`Then he turned to me, giving one more brief glance to my chest before instructing me`);
			npc(1, `You! Get dry, clean up the mess and continue with your work too!`);
			/*			
			if(mile.distilation_nerd === 3){
			}else if(mile.distilation_nerd === 2){
			}else{
				if(mile.a_burned){
				}else{
				}
			}	
			*/
			next(`The end of laboratory classes. `, quickLoadOutfit);
			break;

	case 630: 
			mile.distilation_end = 1;
			mile.slut--;
			mile.stat--;
			mile.sas_sassy++;
			emo("unco");
			
			kat(`Oh no!`);
			txt(`I gasped. My face immediately went red, I could not handle everybody staring, even the eyes of our chemistry teacher were pinned on my almost-exposed @tits. I crossed my arms to cover my chest and ran out of the class. I slammed the door but I still could hear my classmates loudly laughing behind them.`);
			if(mile.slut < 1){
				txt(`I went to the bathroom where I stripped and tried to dry my @upper under the hands dryer. I barely suppressed tears - the whole class saw me almost naked, laughed at me and recorded me on their phones.`);
			}else{
				txt(`I went to the bathrooms where I stripped and tried to dry my @upper under the hands dryer. I felt horrible for being so publicly humiliated, laughed at and recorded on phones.`);
			}
			txt(`I was startled when somebody opened the door but it was @eva and @sas.`);
			sas(`Are you okay, @kat?`);
			kat(`Yeah.`);
			eva(`I knew you are a show-off but I definitely didn't expect that!`);
			txt(`I shrugged but @sas saw how upset I was and untypically flared up:`);
			sas(`Do you have to be always so mean!`);
			eva(`Sorry! I just tried to lighten the mood. Are you sure you're fine, @kat? Could we do anything for you?`);
			kat(`Thank you, I'm fine!`);	
					
			remove("upper"); //SIC

			next(`The end of laboratory classes. `, quickLoadOutfit );
			break;
	}
}






//LITERATURE
//IMPROPER CLOTHES
export const improper_clothes = (index)=> {
	const details = sluttiness(PC).details;
	switch(index){
		default:
		case 101:
			emo("shy");
			placetime(`classroom`, `literature`);
			mile.improper_clothes = true;
			{
				const reason = [""];
				if(details.titfall && !wears.bra) reason.push("readjust my dress and make sure my breasts are not falling out");
				if(mile.plugged) reason.push("readjust the buttplug I was wearing");
				txt(`I stopped at the bathrooms ${ra.array(reason)} and arrived in the class late. @maj, our form teacher who also taught literature, usually did not rush and I did not expect her to arrive before me. However, she was already behind her desk and disapprovingly glared at me:`);
			}
			maj(`You're late, @katalt! And why are you dressed like a harlot!?!`);
			txt(`She did not like my provocative outfit. I faltered. There was no time nor place to explain I was blackmailed and ${mile.slut > 13 ? "the truth I loved when guys were getting hard staring at me" : "deflective answer like <i>Why not?</i>"} would only make her angrier.`);
			maj(`You can't be serious! Wearing such ${wears.skirt ? "a scandalous @lower" : "scandalous @lower"}!`);
			if(mile.slut < 3){
				txt(`The whole class was laughing and my face went red. It was so humiliating and unjust. I did not want to dress like a whore, I was forced by those three nerds! But I could not say anything.`);
			}else{
				txt(`The whole class was laughing, @maj humiliated me when she basically claimed I look like a whore.`);
			}
			kat(`Well, I...`);
			maj(`I can tolerate a lot but enough is enough. This time you went too far! But I don't have time to bother with you, I have a lesson to teach. Go to the principal's office, she'll decide what to do with you!`);
			txt(`I turned and left the class.`);
			
			link(`Principal's office. `, 102);
			break;
			
		case 102:
			placetime("teacher's lounge", "");
			emo("fine");
			txt(`I was quite anxious. The principal rather liked me and she was overall pretty reasonable. However, I could not deny I was dressed rather whorishly. I knocked on the principal's door and was almost relieved when nobody answered.`);
			asy(`What do you need, miss @katsur? Can I help you?`);
			txt(`The door to the teacher's lounge was open and @asy, the deputy principal, noticed me and invited me in. He was sitting there with @dur, the history teacher, they were taking their break and having a coffee.`);
			kat(`Well, I was sent to the principal office...`);
			txt(`He stopped smiling and looked at me seriously:`);
			asy(`What did you do?`);
			if(mile.slut < 9){
				kat(`@maj felt that I'm improperly dressed.`);
			}else{
				kat(`@maj thought I'm dressed like a slut.`);
			}
			txt(`@asy now looked perplexed, he was clearly not prepared to handle that.`);
			asy(`I see.`);
			dur(`Do we even have a dress code?`);
			asy(`Technically no, the rules just say the students have to wear clean and proper clothes.`);
			dur(`So what constitutes improper?`);
			asy(`I guess it depends on the assessment of individual cases... miss @katsur, come closer!`);
			txt(`He ordered me to stand next to the window so he could evaluate me in direct sunlight.`);

			link(`Evaluation.  `, 103);
			break;
			
		case 103:
			emo("unco");
//TODO FISHENT SHIRT
//TODO MAKEUP

			asy(`Hmmm... that @upper is clearly borderline.`);
			txt(`@asy examined my outfit and @dur nodded.`);
			
		//no bra
			if(!wears.bra){
				dur(`And she's wearing no bra under it.`);
				txt(`Casually noted @dur. ${mile.slut < 6 ? "It made me feel embarrassed. " : "I just shrugged, my poking nipples were making it obvious. "}`);
				asy(`Yeah, that could be an issue! Her top is so thin you can observe her nipples under it. Even the younger students can see her breasts!`);
				dur(`They had probably already seen much worse things on the Internet.`);
				asy(`Hmmmm.`);
			}
		
		//enhanced breasts
			if(mile.boobs > 0){
				txt(`They stared at my chest, that at each other, then back at my chest.`);
				kat(`Is there something wrong with my breasts?`);
				if(mile.boobs > 2){
					asy(`They are noticeably bigger, aren't they?`);
					kat(`Yeah, ${mile.slut > 10 ? "thanks for noticing!" : "they are."} I got them a little enhanced.`);
					asy(`It seems more than a little. No offence!`);
				}else if(mile.boobs > 0){
					dur(`No, nothing wrong!`);
					asy(`Please, don't get offended, miss @katsur, but they seem to be... bigger? ${wears.bra? "I'm sorry for asking but are you wearing a padded bra or..."  :  ""}`);
					kat(`Yeah, they are a bit bigger. I got them a bit enhanced.`);
					asy(`Oh, right. That answers it.`);
				}
				
				dur(`Your breasts looked great even before! Sorry, I didn't want to sound like a creep but I think you're a very attractive young woman and you don't have to be insecure about any body issues.`);
				if(mile.slut < 6){
					txt(`He might mean it well but discussing the size of my breasts with my history teacher and the deputy principal was making me pretty uncomfortable.`);
				}else if(mile.slut > 12){
					kat(`Come on! Don't tell me men don't prefer women with bigger breasts!`);
				}else{
					kat(`Ehm, thank you.`);
				}
			}

		//midrift
			if(details.midriff){
				asy(`And you probably shouldn't show so much of your stomach${wears.piercedNavel ? ", especially with that lascivious piercing. " : ""}`);	
			}

			link(`Meekly let them continue. `, 104, ()=> mile.improper_clothes_sub = 1);
			link(`So what is the result? `, 104, ()=> mile.improper_clothes_sub = 2);
			link(`Why does it take so long? Should I pose for you or what? `, 104, ()=> mile.improper_clothes_sub = 3);
			break;
			
		case 104:
			switch(mile.improper_clothes_sub){
				case 1:
					mile.sub++;
					txt(`I obediently stood there, letting the two older men ogle my body.`);
					break;
				
				default:
				case 2:
					asy(`It's hard to decide.`);
					txt(`The deputy principal shrugged, clearly having no idea how to decide.`);
					break;
					
				case 3:
					mile.sub--;
					asy(`Stop getting sassy, miss @katsur. There's no precedent and we owe you to be thorough to reach the correct conclusion.`);
					txt(`The deputy principal explained, clearly having no idea how to judge my case.`);
					break;
			}
			asy(`Could please step on a chair?`);
			kat(`Why?`);
			asy(`I'd like to measure how long - respecitively short - your @lower ${wears.skirt ? "is" : "are"}.`);
		//heels
			if(wears.heels){
				dur(`But better remove those heels first, we don't want you to fall down.`);
				asy(`Hmm, high-heels are hardly appropriate shoes to wear to a class.`);
				showLess(-1, -1, 2);
				txt(`I took my shoes off and stepped up on a chair. Their heads were roughly at the level of my stomach and @asy reached for a tape measure to measure my @lower.`);
			}else{
				txt(`I nodded and stepped up on a chair @dur pushed next to me. Their heads were roughly at the level of my stomach and @asy reached for a tape measure to measure my @lower.`);
			}

		//TODO TATTOO
		
		//no panties
			if(details.genitals || (details.glimpse && wears.crotchlessPanties)  ){
				mile.improper_clothes_no_panties = true;
				mile.improper_clothes_show = true;
				dur(`I think the skirt is obviously too short.`);
				asy(`I agree.. oh-`);
				txt(`They now had a better view and could not miss the glimpse of my uncovered flesh.`);
				if(wears.panties){
					asy(`W... what is that? I can see your...`);
					txt(`@asy was stunned by my underwear.`);
					kat(`Crotchless panties.`);
					dur(`Are those a new stupid fashion trend among young people?!?`);
					kat(`Well...`);
				}else{
					asy(`You're wearing no panties!`);
					dur(`She's not wearing any panties!`);
					if(mile.sub < 0){
						kat(`Yeah, I'm not wearing any panties!`);
					}else{
						txt(`They were stunned. I could not deny it, the proof was right in front of their eyes.`);
					}
				}
				asy(`She definitely can't go around the school, flashing her... flashing to everybody.`);
				dur(`What the hell were you thinking, @katalt!`);
				asy(`You have to change, immediately.`);
				
				link(`Get changed. `, 110);
				return;
		
		//panties visible
			}else if(details.panties){
				if(details.glimpse){
					dur(`You don't even have to measure anything! Her skirt is obviously too skirt, I can even spot her panties!`);
				}else{
					dur(`Showing your panties to everybody definitely isn't appropriate!`);
					asy(`I agree. You're not allowed to do that, miss @katsur.`);
				}
				if(wears.sexyPanties && mile.slut > 8){ //TODO not thong?
					kat(`Those are not panties but @panties.`);
					asy(`Is there a difference?`);
					dur(`@panties panties are those panties with just a thin strip in the back, leaving the whole ass exposed.`);
					asy(`Oh, right, I know what you mean.`);
				}
					//todo - want to see?
		//panties?
			}else if(!wears.bra){
				dur(`If you don't wear any bra, I hope you're at least wearing panties!`);
				txt(`Joked @dur.`);
				
				if(wears.panties){
					if(mile.slut < 1){
						kat(`O.. of course!`);
						txt(`I awkwardly laughed, embarrassed that he might even suggested I was wearing no underwear.`);
					}else if(mile.slut < 11){
						if(mile.sub > 5){
							kat(`Of course! Do.. do you want to see them?`);
							txt(`I hesitantly, submissively suggested, taking them aback.`);
							kat(`No! That won't be necessary, we don't want to embarrass you!`);
						}else{
							kat(`Of course! Do you want to see them or what?!`);
							dur(`No, we trust you!`);
							txt(`@dur laughed.`);
						}
					}else{
						mile.improper_clothes_show = true;
						rollUpSkirt(2);
						kat(`Absolutely I do!`);
						txt(`I confirmed and before they were able to react, I ${wears.skirt ? "pulled up" : "pulled down"} my @lower to flash them my @panties and show them I was not lying.`);
						asy(`Oh!`);
						dur(`We... we believed you!`);
						asy(`Miss @katsur! Please put your @lower back!`);
					}
				}else{
					if(mile.slut < 7){
						kat(`Eh... Of course I do!`);
						txt(`I stammered out a lie. @dur stared at my face, my blush made the truth obvious.`);
						dur(`Oh... okay.`);
						if(wears.skirt){
							txt(`At least @asy was not paying attention. He was fumbing with the edge of my @lower and the tape measure and for a moment I dreaded he might soon out find out for himself that I was not wearing any underwear but he did not.`);
						}else{
							txt(`At least @asy, fumbling with the measure, did not notice my quity expression.`);
						}
						
					}else if(mile.slut > 14){
						mile.improper_clothes_no_panties = true;
						mile.improper_clothes_show = true;
						rollUpSkirt(2);
						kat(`Of course I don't!`);
						txt(`I playfully admitted. @dur smirked, @asy frowned but obviously neither of them believed me. So before they were able to react I ${wears.skirt ? "pulled up" : "pulled down"} my @lower and quickly flashed them my bare pussy.`);
						asy(`Miss @katsur! Put your @lower back on!`);
						dur(`We... we weren't asking you for this!`);
						txt(`I completely shocked them both.`);
					
					}else{
						mile.improper_clothes_no_panties = true;
						kat(`Well... I kinda don't.`);
						dur(`What?!`);
						kat(`I mean, I'm not wearing any panties.`);
						dur(`Oh!`);
						asy(`That's absolutely not allowed! You have to wear underwear!`);
						kat(`${mile.sub < 3 ? "Is it written in school rules? " : "Sorry!"}`);
						asy(`I hope you're doing it only for your personal comfort... or gratification... not to harass other students... or staff... with your... you know!`);
						if(mile.slut > 10){
							kat(`Of course not! You're the only two people who know I never wear any underwear.`);
							dur(`By never you mean...`);
							kat(`Never.`);
							asy(`Oh!`);
						}else{
							kat(`Of course not! You're the only two people who know I'm wearing no underwear today!`);
						}
						
					}
				}
			}
		//behind
			txt(`They looked at each other and then at me:`);
			asy(`Could you please turn around?`);
		
			if(details.whaletail){
				txt(`I did so so they could check even my ass. I almost expected I will feel their hands groping my bottom but they resisted their urges. I did not see what they were doing but at least did not have any comments about my whaletail.`);
			}else{
				txt(`I did so so they could check even my ass. I almost expected I will feel their hands groping my bottom but they resisted their urges. `);
			}
		
		//TODO
		//TODO TATTOO

			asy(`Well, that's everything.`);
			txt(`Concluded @asy and @dur took my hand and helped me to step down. `);

			asy(`All things considered... your outfit is borderline improper. However we'll solve it with just a warning, I don't believe there's a reason to punish you.`);


			link(`I'll try to wear more reasonable clothes. `, 105, ()=> mile.improper_clothes_slut = 1);
			link(`Thanks. `, 105, ()=> mile.improper_clothes_slut = 2);
			link(`I mean, what is wrong with dressing sexy?  `, 105, ()=> mile.improper_clothes_slut = 3);
			break;
		

		case 105:
			showAll()
			switch(mile.improper_clothes_slut){
				case 1:
					mile.slut--;
					dur(`We don't want to restrict you, we're just asking you to be more reasonable!`);
					break;
				case 3:
					mile.slut++;
					dur(`In your free time, nothing but school is an official institution and you have to wear respectable clothes.`);
					break;	
				default: //
					break;
			}
			txt(`I was about to leave but @asy stopped me:`);
			asy(`Wait a moment. There's a chance the principal might change my decisions so I need to take a picture of you... for the record.`);
			kat(`Fine!`);
			txt(`I shrugged. I posed for him, from the front, in profile and from behind. He quickly took three pictures of me with his phone and saved them for later. Only then they let me go.`);
			
			next(`Return to the class. `);

			break;
			
		case 110:
			remove("lower", PC, PC.inventory);
			createWear(`gymLeggings`);

			txt(`I had spare leggings in my locker room. I got changed in the bathrooms and then returned back to the teacher's lounge. The leggings were tight and after a closer examination, a hint of cameltoe was visible. Still, to the deputy principal and the history teacher, it was more acceptable than my bare pussy exposed.`);
			asy(`You're not allowed walk around the school with your... vulva exposed! We'll let you go with a warning but we hope you won't forget your underwear the next time!`);

			link(`I'll try to wear more reasonable clothes. `, 105, ()=> mile.improper_clothes_slut = 1);
			link(`Thanks. `, 105, ()=> mile.improper_clothes_slut = 2);
			link(`I mean, what is wrong with dressing sexy?  `, 105, ()=> mile.improper_clothes_slut = 3);
			break;
			
	}

}


 


//GERMAN
//GERMAN CONSEQUENCES 
export const german_consequences = (index)=> {
	placetime(`classroom`, `german lesson`);
	mile.mol = 2;
	if(mile.mol_perv < 0){
		emo("smug");
		txt(`Maybe for the first time ever, I was looking forward to my German classes. @mol was anxious even to look at me, he was still afraid I might tell anybody what had happened during the detention. It was awesome to see the old creep humiliated!`);
		txt(`He still could not skip me when we were practicing conversation - that would be suspicious - but when I did not know the right answer, he just handwaved it without any of his usually demeaning remarks.`);	

	}else if(mile.mol_perv === 1){
		emo("shy");
		txt(`I was not looking forward to my German classes. Apart from the obvious reason - the German language - I still felt weird about what happened during the detention. How @mol improperly touched me and our awkward conversation.`);
		txt(`However, it did not seem @mol felt awkward, on the contrary, he was cheerful and casual. When the class ended and we were leaving the classroom, he winked at me:`);
		mol(`Nice @upper! Very sexy!`);
		txt(`I just quickly walked away. I was clear his creepy advances are making me uncomfortable and I am absolutely not interested, was I not?`);
				
	}else{
		emo("shy");
		const intro = `I was not looking forward to my German classes. For multiple reasons. `;
		const reason = mile.sub > mile.slut ?  `Maybe I made a mistake. The vibrating panties were making me crazy and I was not able to resist his authority. ` : `Maybe I made a mistake. The vibrating panties were making me crazy and I was so horny I was not able to think straight. `;
		if(mile.mol_cun){
			txt(`${intro} I put my books on the desk and then remembered that only a few days ago, I had been sitting there and my teacher had been eating my pussy. ${reason}`);
		}else{
			txt(`${intro} When I sat down and opened my textbook, there were still noticeable cum stains I was incapable clean properly without destroying the page. ${reason}`);
		}
		
		const reaction = (()=>{
			if(mile.slut < 4) return `I blushed and quickly averted my eyes. `;
			if(mile.slut > 9) return `I reflexively smiled back at him a wondered if I would do it again. `;
			return ``;
			//TODO - horny? 
		})();
		
		txt(`@mol seemed to be less conflicted about our encounter. He was strangely cheerful, smiled at me and once - when he assumed nobody could see him - even winked at me. ${reaction}`);
	}
	next();
}


//GERMAN GRADES
export const german_grades = (index)=> {
		placetime(`classroom`, `german lesson`);
		mile.mol = 3;
		showLess(-1, 1, -1);
		emo("shy");
		
		mol(`Miss @katsur? I need to talk with you about your... grades. `);
		kat(`Yes, @mol? `);
		txt(`Everybody else left the class, I stayed alone with my german teacher. `);
		kat(`Come on, after what we did you're going to bother me with grades?  `);
		mol(`It takes a bit more effort before I'll give you 1 on your report card. `);
		txt(`Annoyed I bit my lower lip. The greedy old pervert wanted more? `);
		if(ext.background !== 3) kat(`Thanks, I'm okay just with not failing. `);
					
		if(mile.mol_cun && mile.plugged){
			mol(`I wonder... are you wearing your special vibrating panties? `);
			if(mile.slut > 8){
				txt(`I felt especially mischievous so I grinned and admitted: `)
				kat(`I'm trying something different today. `);
				txt(`He desired to know more. `);
				mol(`What do you mean, you little slut? `);
				kat(`Today I'm wearing a buttplug in my bum! `);
				mol(`Show me! `);
													
			}else{
				txt(`I faltered. `);
				mol(`Don't dare to lie to me! I see you blushing! Come on, @katalt! `);
				kat(`I... I'm not wearing my panties... `);
				mol(`What are you wearing, you little slut? `);
				kat(`I... eh.. I'm wearing... a buttplug. `);
				mol(`A buttplug? Show me! `);
			}
			
			txt(`He sharply ordered. I turned, ${wears.skirt ? "raised up" : "puled down"} my @lower, ${wears.panties? "moved the @panties aside. " : ""} and bent over. `)
			mol(`Amazing. How big it is? Who would think you're such a huge pervert! `);
			txt(`He chuckled, touched the base and wiggled with it.`);
			kat(`Ahhh! Stop it! ${mile.slut > 11 ? "You'll make cum! " : ""}`);

		}else{
			mol(`Also, I think you need to be disciplined for the way you unabashedly teased me! `);
			kat(`I wasn't teasing you!`);
			const intro = `Of course you were, you shameless little slut!`;
			if(mile.open_legs && wears.miniSkirt){
				mol(`${intro} Wearing such short skirts and obscenely spreading your legs, showing me ${wears.panties ? "your @panties" : "you're wearing no panties"}!`);
			}else if(mile.open_legs && wears.shorts){
				mol(`${intro} Wearing such tight shorts and obscenely spreading your legs, showing me how much you want it! `);
			}else{
				mol(`${intro} Prancing around in such ${wears.skirt ? "short" : "tight"} @lower!`);
			}
			
			if(mile.sub < 1){		
				kat(`What are you doing! `);
				txt(`I protested when he pushed my face against a wall and ${wears.skirt ? "flip up" : "pull down"} my @lower. `);
				mol(`What you deserve, you naughty whore! `);
				txt(`He ensured me and gave a harsh slap to each cheek. `);
			}else{
				txt(`With resignation, I let him push my face against a wall and ${wears.skirt ? "flip up" : "pull down"} my @lower. `);
				txt(`He teasingly fondled my ass and then gave a harsh slap to each cheek. `);
			}
			
			
			if(!wears.panties){
				mol(`And you're such a little nymphet you don't even bother to wear any underwear! That will be two more! `);
				//TODO
				if(mile.sub > 2){
					kat(`Please no!`);
					txt(`I was desperately - and vainly - begging him. He smacked me two more times. `); 
				}else{
					txt(`He smacked me two more times. `); 
				}
			}	
			
			if(mile.pluggged && (!wears.panties || wears.sexyPanties) ){
				if(wears.sexyPanties){
					txt(`The thin string in the back of my @panties was not fully hiding the base of the buttplug I was wearing. And @mol noticed it. `);
				}else{
					txt(`And of course, he discovered the gleaming base of my buttplug. `);
				}					
				mol(`What is this!?! Amazing! How big it is? I would never think you're such a huge pervert! I have to-... `);
			}

		}
		
	txt(`And at that moment the door handle creaked and several freshmen entered the room. I had only a split second to fix my clothes before the door fully opened. They have not seen anything - or at least I hoped so - but I was blushing and my heart was violently beating. `);
	txt(`I stuttered something to excuse myself and rushed to the next class. It was on the other side of the building and I was scolded for being late.  `);
	
	next();
	
}	












export const maj_paper = (index)=> {
	/*
		- chance to exploit eva
		- chance to damage relationship with Majerova
		- hints about Bosáková to make the chapter seem less pointless
	*/

	const leg = wears.highSocks || wears.stockings ? " and @socks" : ""; 
	switch(index){
		default:
		case 101: 
			emo(`shock`);
			placetime(`hallway`);
			
			txt(`In the hallway, I ran into @maj. She seemed to be angry.`);
			mol(`@katalt?`);
			kat(`Yes, @maj?`);
			mol(`Where are the papers I need?`);
			kat(`Papers?`);
			mol(`The report I asked you and @eva to bring me from @ped!`);
			kat(`Oh, right! That one!`);
			txt(`I completely forgot. There were too many way more important issues on my mind than her stupid papers. And not like it would be so hard to go to @ped's office and get them herself.`);
			
			link(`Apologize for forgetting. `, 102);
			link(`Throw @eva under the bus. `, 103);
			break;

		case 102:
			mile.maj_paper = 1;
			mile.maj_exploit++;
			kat(`I'm very sorry, @maj, I forgot.`);
			mol(`How could you forget!?! I'm old enough to forget stuff but you're not! If your memory is so bad, make a note!`);
			kat(`I'm sorry, it won't happen again. I'll get you the papers before the end of the day.`);
			link(`Get the report. `, 110);
			break;
		
		case 103:		
			mile.maj_paper = 2;
			mile.eva_exploit++;
			kat(`@eva didn't give them to you?`);
			mol(`@eva? She didn't give me anything!`);
			kat(`She has them and was supposed to give them to you. I'm sorry, @maj, she probably forgot. Don't worry, I'll get them and bring them to you before the end of the day!`);
			link(`Get the report. `, 110);
			break;

		case 110: 
			emo(`neutral`);
			txt(`${mile.maj_paper === 2 ? "What a stupid chore! " : "I escaped @maj's ire but I still had to get the report. "} Instead of chatting with my friends, I had to go to the other side of the building and walk up two staircases, @ped had a remote office above the gym.`);
			ped(`Hello, @katalt! How can I help you?`);
			kat(`I need to get papers for @maj from you.`);
			ped(`What papers?`);
			kat(`Some kind of report.`);
			ped(`Oh right! I have it somewhere!`);
			txt(`@ped searched for the folder among the mess she had on her table.`);
			ped(`Oh, here!`);
			kat(`Thanks.`);

			if(wears.sexyTop && !wears.bra){
				ped(`Cute top, by the way. It fits you nicely.`);
				txt(`At first, was taken aback by her comment, assuming it was an ironic critique - was my bralessness so obvious she had to point it out? But her compliment seemed genuine.`);
			}else if(wears.shorts){
				ped(`Cute shorts${leg}, by the way. It fits you nicely.`);
				txt(`At first, was taken aback by her comment, assuming it was an ironic critique - were my @lower too close-fitting? But her compliment seemed genuine.`);
			}else if(wears.miniSkirt){
				ped(`Cute skirt${leg}, by the way. ${leg ? "They fit" : "It fits"} you nicely.`);
				txt(`At first, was taken aback by her comment, assuming it was an ironic critique - was my @lower too short? But her compliment seemed genuine.`);
			}
			kat(`...thanks!`);
			next();
			break;
	}
}






 




/*

 zdravotní sestřičky, letušky, námořnice, školačky, kabaretní tanečnice, hostesky F1. vojandy






vermeil


TODO - cumming inside while eyes to stranger hidden

TODO - accident in chemistry classes
dropped pens
tested in biology, rant
disucssin slutiness
tom making out

TODO - sending pictures of having sex, her phone


TODO - LEVEL UP FEYD_SMUG
TODO - ARTIFACT LIST
TODO - DEATH DOOR
TODO - ITEM DESCRIPTION LOOTING
TODO - CRIODAGEAR
TODO - necklaceof initiative
TODO - sirael VII without clothes X
TODO - lysa clothes TEST
TODO - marhsal - major chain


TODO - walking from combat
TODO - goblins laugahbly weak CUTLASS
TODO - prolong potion effect
TODO - rething potions

TODO - lysa not interested in girls

TODO - FISHNET SHIRT 
TODO - MASTURBATION IN PUBLIC IN PUBLIC
TODO - girls selfsesteem depends on how many poeple wahnts to use her
TODO - throw away any and all underwerwar 

*/
/*


if(mile.a_first_sex



		
	
*/
