import {link, con_link, alt_link, links_mix, next, main, present, set, insert, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";

//import {NAME} from "System/variables";
//import {create_schedule} from "System/scheduler"; //BEWARE - the fist time has to accessed manually


import {number2word} from "Libraries/dh";
import {ra} from "Libraries/random";

import {quickTempOutfit, wear, create, draw, effigy, wearRandomClothes, wears, edit, wearsPink, quickLoadOutfit, quickSaveOutfit, updateDraw, createWear, remove, showAll, showLess, crew} from "Avatar/index";

import {initiate_game_character} from "Virtual/initiate";
//import (makeup} from "System



import {seven} from "Minigames/seven";
import {start_game, initiate_secondary_stuff} from  "System/initiate_game";


//import {assign_events} from "Story/assign_events";

import {add_tomorrow, add_today, schedule_initiate, add_workday} from "Taskmaster/scheduler"; //has to be triggered manually
 

function update_subslut(){
	["slut","sub","stat"].forEach( key => {
		counter[key].filter( a => a !== undefined ).forEach( a =>  mile[key] += a );
	});
}				
				
				
export function skip(input){
	
		
	
	switch(input){
		
		default:
		case 101:
				quickSaveOutfit();
				
//console.error(PC.upper);
//console.error(ext.outfits);

				
				set.game();
				crew(`fBlandPaties`,`fBlandBra`); 
			
				counter = {
					slut: [],
					sub: [],
					stat: [],
				};
				
				txt(`At the party, I discovered those dorks made a bot based on me and used it to humiliate me in front of everybody! I was rightfully ${choice("react_1")} / ${choice("react_2")}! `);
				txt(`But I knew a solution! I visited @anj, a hacker. I convinced him ${choice("sol_3")} / ${choice("sol_2")} / ${choice("sol_1")} and he came up with a flawless plan to delete their records and the AI abomination. `);
				txt(`I invade her personal subreality, proving how ${choice("smart")} / ${choice("determined")} / ${choice("indolent")} I was. However, she surprised me. I explained I am there to  ${choice("kill_1")} / ${choice("kill_2")} but the lewd AI was getting sexually aggressive which ${choice("cys_1")} / ${choice("cys_2")} / ${choice("cys_3")} / ${choice("cys_4")}. But eventually I managed to penetrate deeper, stole the data ${choice("del_1")} / ${choice("del_2")}. `);
				txt(`On my way out I was again caught, this time by the nerds. They confused me for the AI and I was almost able to escape, ${choice("dig_1")} / ${choice("dig_2")}, but @qaa recognized me instantly. `);
				txt(`It seemed they bore some kind of unjust grudge against me caused by my being a popular hot bitch and them being envious horny losers. We made a deal, they get rid of the AI and stop ever bother me if I finish their dumb game and fulfill their task every day until I do. Which at the time seemed like a bright idea. `);
				txt(`In the game, I chose a character of ${choice("r_human", "human")} / ${choice("r_elf", "half-elf")} / ${choice("r_orc", "half-orc")} ${choice("c_harlot", "harlot")} / ${choice("c_hellion", "hellion")} / ${choice("c_fallen", "fallen priestess")} with bonus ${choice("b_str", "strength")} / ${choice("b_dex", "dexterity")} / ${choice("b_int", "intelligence")} / ${choice("b_char", "charisma")} and finished the first quest. `);
				txt(`It was a waste of time ${choice("imm_1")} / ${choice("imm_2")} / ${choice("imm_3")}. `);
		
		

		
		
		
		
			//PARTY
				ext.background = 3; //TODO
				mile.meta_upgrade = 2; //TODO - WAIST

			//HACKER
				effects([{
						id: "react_1",
						label: "horrified",
						fce(){
							counter.sub[0] = 1; //+sub
						},
					},{
						id: "react_2",
						label: "outraged",
						fce(){
							counter.sub[0] = -1;
						},
				}]);
				
				effects([{
						id: "sol_1",
						label: "sucking his dick",
						fce(){
							counter.slut[0] = 1; 
							mile.hacking_solution = 1;
						},
					},{
						id: "sol_2",
						label: "flashing my tits",
						fce(){
							counter.slut[0] = undefined; 
							mile.hacking_solution = 2;
						},
					},{
						id: "sol_3",
						label: "using my charm",
						fce(){
							counter.slut[0] = false; 
							mile.hacking_solution = 3;
						},
				}]);
				
			//HACKING
				effects([{
						id: "smart",
						fce(){
							mile.smart = 1;
						},
					},{
						id: "determined",
						fce(){
							mile.smart = 0;
						},
					},{
						id: "indolent",
						fce(){
							mile.smart = -1;
						},
				}]);
				
			//AI
				mile.hacking_attempts = 1;
				mile.hacking_alone = false;
			
				effects([{
						id: "kill_1",
						label: "save her",
						fce(){
							mile.y_forkill = false;
						},
					},{
						id: "kill_2",
						label: "kill her",
						fce(){
							mile.y_forkill = true;
						},
				}]);
			
				
				effects([{
						id: "initial",
						fce(){
							counter.slut[1] = undefined;
						},
					},{
						id: "cys_1",
						label: "I immensely enjoyed",
						fce(){
							mile.y_cyberlesbo = 2;
							counter.slut[1] = 1;
							mile.lesbian = 1;
						},
					},{
						id: "cys_2",
						label: "I kinda liked",
						fce(){
							mile.y_cyberlesbo = 1;
							mile.lesbian = 1;
						},
					},{
						id: "cys_3",
						label: "made me uncomfortable",
						fce(){
							mile.y_cyberlesbo = -1;
							mile.lesbian = 0;
						},
					},{
						id: "cys_4",
						label: "was absolutely disgusting",
						fce(){
							mile.y_cyberlesbo = -2;
							counter.slut[1] = -1;
							mile.lesbian = -1;
						},
				}]);
			
				effects([{
						id: "del_1",
						label: "and deleted it",
						fce(){
							mile.y_deleted = true;
						},
					},{
						id: "del_2",
						label: "but spared her",
						fce(){
							mile.y_deleted = false;
						},
				}]);
				
			//CAUGHT BY NERDS
				effects([{
						id: "dig_1",
						label: "handling the situation with dignity and defiance",
						fce(){
							counter.slut[2] = undefined;
							counter.sub[2] = undefined;
						},
					},{
						id: "dig_2",
						label: "desperately doing my best to not get recognized",
						fce(){
							counter.slut[2] = 1;
							counter.sub[2] = 1;
						},
				}]);
				
				
			//VIRTUAL GAME
				mile.feyd_keryk_attempts = 0;
				mile.feyd_keryk_name = "Keryk";
				mile.feyd_keryk = true;
				mile.feyd_rats = true;
				
				effects([{
					id: `initial`,
					fce(){
						ext.race = 0;
						PC.apex.game_lips =  undefined;
						PC.apex.game_nails =  undefined;
						PC.apex.game_mascara_top =  undefined;
						PC.apex.game_mascara_bot =  undefined;
						
						PC.apex.game_tat_faceLeft =  undefined;
						PC.apex.game_tat_sleeve = undefined;
						PC.apex.game_tat_thighLeft = undefined;

						//updateDraw(PC);
						crew(`fBlandPaties`,`fBlandBra`);
					},
					relative: {
						
					},
				},{
					id: `c_harlot`,
					fce(){
						ext.class = 1;
						mile.class = 1;
						
						PC.apex.game_lips =  `#b63326`;
						//PC.apex.game_nails =  undefined;
						PC.apex.game_mascara_top =  `black`;
						PC.apex.game_mascara_bot =  `black`;
						
						crew(`defHarlotSkirt`,`defHarlotTop`,`defHarlotCorset`,`defHarlotStockings`);
						//updateDraw(PC);
					},
					relative: {
						faceFem: 5,
						eyelashLength: 3,
						waistWidth: -3,
						hipWidth: 6,
						breastSize: 5,
						breastPerkiness: 2,
						legFem: 2,
						legFullness: 3,
					},
				},{
					id: `c_hellion`,
					fce(){
						ext.class = 2;
						mile.class = 2;
						
						PC.apex.game_tat_faceLeft = "triangle";
						PC.apex.game_tat_sleeve = "dragonBlue;0;0;2";
						PC.apex.game_tat_thighLeft = "blueBand2;0;-25;2";

						crew(`defBarbaricBra`,`defBarbaricLoincloth`);
					},
					relative: {
						height: 1,
						upperMuscle: 4,
					},
				},{
					id: `c_fallen`,
					fce(){
						ext.class = 3;
						mile.class = 3;
						
						PC.apex.game_lips =  `#523b38`;
						PC.apex.game_nails =  `black`;
						PC.apex.game_mascara_top =  `black`;
						PC.apex.game_mascara_bot =  `black`;
						
						//updateDraw(PC);
						crew(`defFallenTop`,`defFallenLoincloth`);
					},
					relative: {
						eyelashLength: 2,
						irisSaturation: ext.race === 1 ? 20 : 20, //90-PC.Mods.irisSaturation,
						irisLightness: ext.race === 1 ? 10 : 70 - PC.Mods.irisLightness, 
						skinLightness: 3,
					},
				}]);
			
				effects([{
					id: `r_human`,
					fce(){
						ext.race = 0;
					},
					relative: {
					 
					},
				},{
					id: `r_elf`,
					fce(){
						ext.race = 1;
					},
					relative: {
						height: 3,
						earlobeLength:3,
						
						neckWidth: -4,
						armThickness: -8,
						waistWidth: -10,
						hipWidth: -8,
						legFullness: -8,
						
						cheekFullness: -10,
						faceFem: 20,
					
						irisSize:4,
						irisSaturation: 30, //90-PC.Mods.irisSaturation,
						irisLightness: 70 - PC.Mods.irisLightness,
						
						eyeTilt:5,
						eyeWidth:1,
						eyeTopSize:1,
						eyeBotSize:1,
						eyeSize:2, 
						
						skinSaturation: 10,
					},
				},{
					id: `r_orc`,
					fce(){
						ext.race = 2;
					},
					relative: {
						height:-1,
						armLength:7,//?
						handSize:30,
						upperMuscle: 21 - PC.basedim.upperMuscle,
						
						legFullness:24,
						hipWidth:6,
						
						neckWidth:2,
						
						earlobeLength:0.5,
						faceWidth:1,
						
						
						browOutBias:2,
						browThickness:2,
						eyeTilt:-3,
						limbalRingSize:10,

						noseHeight:3,
						noseLength:-12,
						noseWidth:5,
						nostrilSize:5,
						
						//skinHue:80,
						skinSaturation:-10,
					},
				}]);
				
				effects([{
					id: `b_str`,
					fce(){
						ext.origin = 1;	
					},
				},{
					id: `b_dex`,
					fce(){
						ext.origin = 2;	
					},
				},{
					id: `b_int`,
					fce(){
						ext.origin = 3;	
					},
				},{
					id: `b_char`,
					fce(){
						ext.origin = 4;	
					},
				}]);
				
				effects([{
						id: `imm_1`,
						label: "and a horrible chore",
						fce(){
							mile.immersion = -1;
						},
					},{
						id: `imm_2`,
						label: "but the game was not as bad as thought",
						fce(){
							mile.immersion = 0;
						},
					},{
						id: `imm_3`,
						label: "but the game seemed quite interesting",
						fce(){
							mile.immersion = 1;
						},
				}]);
		
				
			/*	
			next(`The next day. `, ()=>{
				update_subslut();
			
				schedule_initiate();
				add_workday("classes", "girls_weekend_confront");
				add_workday("afterSchool", "a_apology");
				mile.skip_random = true;
				initiate_game_character();
				
				set.irl();
				quickLoadOutfit();
			})
			*/
			link(`The next day. `, 77, ()=>{
				
				update_subslut();
				initiate_secondary_stuff();
				mile.grades = [3,4,3,2][ext.background] + (mile.smart * -1); 
				
				schedule_initiate();
				
				add_today("awakening", null); 
				add_today("morning", null); 
				add_today("noon", null); 
				add_today("afternoon", null); 
				add_today("evening", null); 
				
				add_workday("classes", "girls_weekend_confront");
				add_workday("afterSchool", "a_apology");
				
				mile.skip_random = true;
				initiate_game_character();
				
				set.irl();
				quickLoadOutfit();
			});
			
			
			
			link(`Skip the first week. `, 102, ()=>{
				
				update_subslut();
				initiate_secondary_stuff();
				mile.grades = [3,4,3,2][ext.background] + (mile.smart * -1); 
				
				schedule_initiate();
				initiate_game_character();
				
				set.irl();
				
				quickLoadOutfit();
				//quickLoadOutfit();
			})


		
		/*
		link(`check The next day. `, 8, ()=>{
				update_subslut();
			
				schedule_initiate();
				add_workday("classes", "girls_weekend_confront");
				add_workday("afterSchool", "a_apology");
				mile.skip_random = true;
				initiate_game_character();
				
				set.irl();
				quickLoadOutfit();
			})
			
			link(`10 check The next day. `, 10, ()=>{
				update_subslut();
			
				schedule_initiate();
				add_workday("classes", "girls_weekend_confront");
				add_workday("afterSchool", "a_apology");
				mile.skip_random = true;
				initiate_game_character();
				
				set.irl();
				quickLoadOutfit();
			})
			link(`check`, 8, ()=>{
				set.irl();
				quickLoadOutfit();
			});
			*/
			break;
	
	case 77: 
			 
	/*
		TODO ASAP - direct link:
			next(`The next day. `, ()=>{
				update_subslut();
			
				schedule_initiate();
				add_workday("classes", "girls_weekend_confront");
				add_workday("afterSchool", "a_apology");
				mile.skip_random = true;
				initiate_game_character();
				
				set.irl();
				quickLoadOutfit();
			})
		does not work, dimensions get fucked up
		padded by this scene because this way it work for some reason
		even though they are bascally the same
		whe????????
		timing?
	*/
			
			next();
			break;
/*	case 10:
		
		next();
		
		break;
		
	case 8:
		set.meta();
		txt(`check`);
		link(`9`, 9);
		next();
		
		break;

	case 9:
		set.irl();
		txt(`check`);
		link(`9`, 9);
		next();
		
		break;
*/


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////ú







			
		case 102:
		
//console.error(ext.outfits);
//

			//crew(`fBlandPaties`,`fBlandBra`); 
		//	crew("initial")
			createWear(`jeans`);
			updateDraw();
			
			counter = {
					slut: [],
					sub: [],
					stat: [],
				};
					
				
			txt(`I soon regretted I agreed to do their stupid tasks. At first, they went easy on me and convince me to do things like send them my ${choice("tame")} / ${choice("cute")} / ${choice("sexy")} / ${choice("slutty")} selfies or wear a short skirt. (I sabotaged their task and wore a modest ${choice(0)} / I wore a cute ${choice(1)} /	I decided to pander them a little and wear sexy ${choice(2)} / I surprised them by wearing my sluttiest, tinniest ${choice(3)}) `);
			txt(`However, they soon proved a creepy fascination with my wardrobe and made me wear heavy ${choice("makeup")} or go to the school with ${choice("bra")} and most humiliatingly, flash them my ${choice("p_ordinary", "ordinary")} / ${choice("p_sexy","sexy")} panties. `);
			txt(`@qaa even had a change of heart and tried to apologize for blackmailing me (without intending to stop) ${choice("nice")} / ${choice("angry")}. `);
			txt(`Worse, the game was more complex and difficult then I anticipated. I even had to  ${choice("aa")} / ${choice("bb")} / ${choice("cc")} and I still only managed to finish all required quests in Feyd Mora. `);
			//for help and let him kiss me
			txt(`Especially devious thing they had prepared for the Weekend - on the Friday they made me wrestle against my AI clone. I, of course, ${choice("won")} / ${choice("lost")}. `);
			
			//advice 1
				effects([{
						id: `won`,
						label: `won`,
						fce(){
							mile.fight = 1;
						},
					},{
						id: `lost`,
						label: `lost`,
						fce(){
							mile.fight = -1;
						},
				}]);
				
				
				
			//advice 1
				effects([{
						id: `aa`,
						label: `ask @qaa for help and let him kiss me`,
						fce(){
							mile.advice_1 = 1;
						},
					},{
						id: `bb`,
						label: `humiliatingly beg @qbb for his help`,
						fce(){
							mile.advice_1 = 2;
						},
					},{
						id: `cc`,
						label: `ask @qcc for his advice`,
						fce(){
							mile.advice_1 = 3;
						},
				}]);
				
			//aaa nice x angry 
				effects([{
						id: `nice`,
						label: `and I pretended to be nice and understanding`,
						fce(){
							mile.a_hate = -1;			
							mile.a_burned = false;	
						},
					},{
						id: `angry`,
						label: `which only made me furious`,
						fce(){
							mile.a_hate = 1;
							mile.a_burned = true;	
						},
				}]);
			
		//GAME
			mile.game_progress = 10;
			mile.feyd_roses = 2;
				
		//TASKS
			ext.stats.tasks[3] = "sexy selfie";
			ext.stats.tasks[4] = "short skirt";
			ext.stats.tasks[5] = "makeup";
			ext.stats.tasks[6] = "no bra";
			ext.stats.tasks[7] = "flash panties";
				
			//selfies
				mile.sexy_selfie = 2; //TODO!! SLUTYNESS!
				
				effects([{
					id: "tame",
					fce(){
						mile.sexy_selfie = 1;
					},
				},{
					id: "cute",
					fce(){
						mile.sexy_selfie = 2;
					},
				},{
					id: "sexy",
					fce(){
						mile.sexy_selfie = 3;
					},
				},{
					id: "slutty",
//CONDIT!!
					fce(){
						mile.sexy_selfie = 4;
					},
				}]);
			
			//short skirt 
				ext.rules.short_skirt = true;
				{
					const temp = [];
					for(let i = 0; i < 4; i++){ 
						temp.push( create(`shortSkirt${i}`) );
					}
				
					//create effects
					const array = [{
						id: "initial",
						fce(){
							remove("lower");				
						},
					}];
					temp.forEach( (a, i) => array.push({
						label: a.name,
						fce(){
							mile.short_skirt = i + 1;
							wear(a);
						},
					}));
					effects(array);
				}	
					
			//makeup 
				mile.makeup = true;

				effects([{
					id: `initial`,
					fce(){
						PC.apex.irl_lips = undefined;
						PC.apex.irl_nails = undefined;
						PC.apex.irl_mascara_top = undefined;
						PC.apex.irl_mascara_bot = undefined;
						updateDraw();
					},
				},{
					id: `makeup`,
					label: `makeup`,
					relative: {
						eyelashLength: 4,
					},
					fce(){
						PC.apex.irl_lips ??= "#e00013";
						PC.apex.irl_nails ??= "#000000";
						PC.apex.irl_mascara_top ??= "#000000";
						PC.apex.irl_mascara_bot ??= "#000000";
						updateDraw();
					},
				}])

			//bo bra
				mile.no_bra = true;
				ext.rules.no_bra = true;
				
				//sexy top??,
				//ext.rules.sexy_top = true;
				//TODO - FOLLOWED/DIDNT FOLLOW
				
				effects([{
					id: `initial`,
					fce(){
						createWear("pushUpBra");
						updateDraw();
					},
				},{
					id: `bra`,
					label: `no bra`,
					fce(){
						remove("bra");
						updateDraw();
					},
				}])

			//panties flashing
				mile.flash_panties = true;
				mile.saw_panties = true;
				
				effects([{
					id: `p_ordinary`,
					fce(){
						mile.saw_sexy_panties = true;
						createWear("briefs");
						updateDraw();
					},
				},{
					id: `p_sexy`,
					fce(){
						mile.saw_sexy_panties = true;
						createWear("thong");
						updateDraw();
					},
				}])
			
			
			
			next(`Saturday. `, ()=>{
				update_subslut(); //????
//TODO - CHECK IF THIS WORKS				

				//TODO advice
				
				if(mile.a_burned){
					mile.sub--;
				}else{
					mile.sub++;
				}
				//short skirt 
				switch(mile.short_skirt){
					case 1: 
						mile.sub--; 
						mile.slut--; 
						break;
					case 3: 
						mile.sub++; 
						break;
					case 4: 
						mile.sub++; 
						mile.slut++; 
						break;
					default:
						break;
				}
				
				//selfies
				if(mile.sexy_selfie === 1){
					mile.slut--;
					mile.obedient -= 1;
				}else if(mile.sexy_selfie === 2){
					//sic
				}else if(mile.sexy_selfie === 3){
					mile.slut++;
					mile.obedient++;
				}else if(mile.sexy_selfie === 4){
					mile.slut++;
					mile.slut++;
					mile.obedient += 2;
				}
				
				head.day = 8; 
				add_today("earlyMorning", "y_fight_reaction");  //TODO!!
				
				
			});

		break;
		
		
		
		

	}		
}
