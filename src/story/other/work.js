import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects, choices_required} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, or, s, capitalise} from "Loop/text";
import {rollUpSkirt, emo, drawZoomed, drawUnzoomed, wdesc, removeActual, update, updateDraw, effigy, allClothes, crew, wearRandomClothes, remove, wear, create, wears, showLess, showAll, violates, INVENTORY, sluttiness, createWear, quickSaveOutfit, quickLoadOutfit, quickTempOutfit} from "Avatar/index";
import {placetime, task_intro, task_reaction, masochist, spoiler, dildo, log_sex} from "Loop/text_extra";
import {money} from "Data/items/prices";
import {payment} from "System/bank_account.js";

import {add_today, revert_schedule, active_timeslot} from "Taskmaster/scheduler";
import {time} from "System/time";



//PLANT MANAGER

export const plant_manager = (index)=> {
	switch(index){
		default:
		case 101: 
			payment("plant_manager", "corp");
			set.irl();
			placetime(`Corporate Building`);
			present(
				[1, `Random Asshole`],
			);
				txt(`Being plant manager was a job my father got me in the large building where he was working. It basically consisted of watering all the plants every once in while. Most of them were snake plants or other fancy sansevierias (or more accurately dracaenas), some of them almost taller than me. They were awesome tough plants and did not require too much light or watering. So I had to drag the can with water over several floors but I did not have to do it very often. I got the job because the Russian lady who was wiping the floors could not be bothered to do it and probably would only overwater them anyway.`);
				txt(`I was bending over to a large glazed pot when I could hear somebody's steps in the hallway. They suddenly stopped not far away when the person noticed me.`);
				{
					let text = `It was not hard to guess the reason - `;
					if(wears.microSkirt){
						text += `my @lower was not providing too much space for imagination, ${wears.panties ? "maybe he could even see my @panties" : "maybe it was revealing enough for him to realize I was wearing no panties. "}`;
					}else{
						text += `my butt in a shamelessly ${wears.skirt ? "short" : "tight"} @lower had to provide a very nice sight. `;
					}
					txt(text);
				}
				txt(`I finished and ${mile.sub < 5 ? "annoyed" : "trying not to blush"} turned to him.`);
				npc(1, `How are you doing, flower girl?`);
				txt(`Smirked the lanky guy with glasses in his thirties.`);
				kat(`You don't have anything better to do than to ogle teen girls?`);
				npc(1, `Well, honestly, I don't. This job only wastes my talent.`);
						
		link(`Ignore him. `, 102);
		link(`Tell him to fuck off. `, 104);
		link(`Tease him. `, 103);
		break;
		
	case 102:
		mile.plant_manager = 2;
		emo(`anoy`);
		if(mile.sub <= 10) mile.sub++;
		txt(`I decided to ignore the jerk and turned back to my work. I had to water two more floors and I did not wish to waste my precious time with him.`);
		next();
		break;
								
	case 103:
		mile.plant_manager = 3;
		if(mile.slut <= 10) mile.slut++;
		emo(`imp`);
		kat(`Well, in that case, everything is all right. I just wouldn't want to distract you from important work.`);
		txt(`I shrugged and deliberately bent over again. It was kinda exciting to feel his eyes on my ass, it felt good to know I was attractive even to older men, not just to my classmates. Fuck, I just hoped he was not working with my dad. It would be pretty awkward if he boasted to him that the slutty girl watering the plants was flirting with him.`);
		next();
		break;
		
	case 104:
		mile.plant_manager = 1;
		if(mile.sub > 5) mile.sub;
		emo(`angry`);
		kat(`Well, in that case, maybe you should stop bothering people who are actually doing useful work and fuck off?`);
		txt(`I suggested and showed him a middle finger. He chuckled:`);
		npc(1, `My deepest apologies, miss! I won't distract you from your extremely important job anymore!`);
		txt(`He left me alone but I, unfortunately, felt he was not very offended. And I had to water two more floors.`);
		next();
		break;
	}
}




//KARAOKE
export const karaoke = (index)=> {
	switch(index){
		default:
		case 101: 
			placetime(`my room`);
			emo(`focus`);
			present(
				[1, `Harry`], //TODO!!
			);
			eva(`Hey, @kat!`);
			txt(`My ${mile.eva > mile.sas ? "best" : "second best"} friend called me.`);
			kat(`Hello!`);
			eva(`Are you doing something tonight?`);
			kat(`Yeah, kinda. Why?`);
			txt(`I did not explain I had to play a stupid virtual game.`);
			eva(`I wanted to ask if you want to go to a bar.`);
			kat(`Tonight? I thought you told me you have shifts on ${time.day_name}!`);
			eva(`I do! I'm asking if you want to go and help me with work, not to have fun!`);
			kat(`Hard pass!`);
			eva(`${money.karaoke} plus tips!`);
			kat(`I'm listening!`);
			eva(`One of the waitresses went AWOL, we have already too many tables booked and I with @npc_1 can't handle it alone.`);

			link(`Yeah, I think I can help you. `, 104);
			link(`I'm sorry, I already have plans I can't cancel.`, 102);
			link(`You seem to be in trouble. Can I get more? `, 103);
			break;
			
		case 102: 
			mile.karaoke = -1;
			eva(`You disappointed me @katalt! But I do understand, it's a last-moment suggestion. Bye!`);
			next(`Seeya!` , ()=> revert_schedule("afternoon") );
			break;
			
		case 103:
			kat(`Tell @npc_1 I won't do it for ${money.karaoke} but I might agree if he offers more.`);
			eva(`You're so greedy, @katalt! Anyway, of course, I already did that! I didn't want to waste time by going back and forth between you. ${money.karaoke} isn't even close to what he initially offered! You know I have your back! And you can make even more on tips - I have a good feeling about tonight - are you in?`);
			
			link(`Yeah, I am! `, 104);
			link(`I'd love to but I realy can't, sorry! `, 102);
			break;

		case 104: 
			mile.karaoke = 1;
			//add_today(`afternoon`, null); //skips afternoon actions
			//add_today(`evening`, `karaoke_action`);
					
			eva(`Awesome! You know where I work, right?`);
			kat(`Yeah. Should I be there at seven?`);
			eva(`No, we need you there before we open, at six. You have to wipe the floors and do stuff like that.`);
			kat(`Did the other waitress run away because you were mean to her and forced her to do all the hard, demeaning work?`);
			eva(`Well, it is within realms of possibilities. Anyway, see you at six!`);
			link(`Okay, bye!`, 201);
			break;

		case 201: 
			placetime(`Starz`);
			present(
				[1, `Harry`], //TODO!!
				[2, `Old guy`], //TODO!!
				[3, `Creepy guy`], //TODO!!
			);
			emo(`anoy`);
			txt(`I arrived exactly at six. I was the first person there and had to wait for ten more minutes in front of the locked door before @eva arrived and let me inside. *Starz* was a new place with sleek, modern furniture. The bar was divided into three sections, the bar itself, the tables and the stage with karaoke. Then there were also two VIP booths for private parties, @eva sometimes took us into one when it was not occupied.`);
			txt(`I prepared the bar while @eva was mostly only explaining what should I do. @npc_1 meanwhile got there too and we could open. I began waitressing which was not such a bad job and kinda enjoyed it, at least for the first twenty minutes.`);
			txt(`The crowd was great and energized. During the evening, I heard three or four amazing performances but unfortunately, the enthusiasm rarely intersected with actual singing talent. It was fortunate the counter was on the other side of the bar than the stage, however, when the bystanders joined to sign a particularly popular song, there was no escape. And there was a limited number of *Queen* or *ABBA* one can take without being driven crazy.`);
			txt(`The most troublesome were not the rowdy people in the bar but those in the VIP booths. One was booked by a group of some kind of businessmen. It seemed they continued to the karaoke bar from a work party, they were dressed in formal clothes (or their remains after dropping jackets and ties) and they were already drunk when they arrived. It was a bad omen when I took their first order and an older man sleazily smirked at me:`);
			npc(2, `What is your name, sweetie?`);
			link(`I'm @katalt. `, 202, ()=> counter.name = false);
			link(`I'm @katalt! Lovely to meet you! Wink! `, 202, ()=> counter.name = true);
			break;
			
		case 202:
			if(counter.name){
				emo(`relax`);
				if(mile.slut < 4) mile.slut++;
				txt(`I tried to be nice, maybe even flirt with him a bit. It seemed he was not short on cash. Encouraged he smiled back at me:`);
			}else{
				emo(`anoy`);
				if(mile.slut > 12) mile.slut--;
				txt(`I replied in a bored official tone to not encourage him further. But he did not seem discouraged:`);
			}
			npc(2, `@katalt is such a cute name! Nice to meet you!`);
			txt(`I ${counter.name ? "smiled" : "shrugged"} and went for the beer and shots. I distributed glasses but one remained on the tray.`);
			npc(2, `That one's for you!`);
			kat(`I'm not sure if I'm allowed to drink at work.`);
			npc(2, `We won't tell on you! Come on! We're celebrating! You don't want to ruin the mood, do you?`);
			txt(`Reluctantly I took the shot and clinked with them. When I returned again, two guys were loudly singing something from *Horkýže* while others were drinking and cheering them up. The creepy guy grabbed me around the shoulders:`);
			npc(2, `What about you? What are you going to sing us?`);
			kat(`W... what?! Me?! I can't sing!`);
			npc(2, `Come on! Don't underestimate yourself! You have a lovely voice and I'm sure you're an amazing singer!`);
			txt(`He did not take no for an answer and pushed me to the karaoke machine.`);
			link(`*Slow Down*.`, 203, ()=> mile.karaoke_song = 1);
			link(`*At Altar*.`, 203, ()=> mile.karaoke_song = 2);
			link(`*Rose Garden*.`, 203, ()=> mile.karaoke_song = 3);
			link(`Escape him. `, 203, ()=> mile.karaoke_song = -1);
			break;
			
		case 203:
			emo(`focus`)
			if(mile.karaoke_song === -1){
				if(mile.sub > 5) mile.sub--; //TODO
				txt(`He was holding me only carelessly so when he was not paying attention, I crouched and escaped from his embrace.`);
				npc(2, `Come on, girl, don't be shy!`);
				kat(`Sorry! I have too much work to do!`);
				txt(`I hastily apologized and left the VIP room.`);
			}else{
				kat(`No, I'm really not good at this!`);
				npc(2, `Don't be shy! Let's give it try! What would you like to sing?`);
				txt(`I desperately looked at the selection and almost randomly tapped *${["","Slow Down", "At Altar", "Rose Garden"][mile.karaoke_song]}*.`);
				txt(`The guys who were singing finished, one of them handed me the microphone and they pushed me onto the small stage. Everybody stared at me and I did not enjoy being forced to perform for the amusement of a bunch of drunk jerks. It was nearly as bad as the School Christmas Talent Show 2023. However, when the screen showed the words, I began singing.`);
			}
			
			if(mile.karaoke_song === 1){
				kat(`*After something special... into gardens without a cross... where are you rushing... I can't keep up with you...*`);
			}else if(mile.karaoke_song === 2){
				kat(`*I was only the second one, I know... I will leave your world behind... I was only the second one, farewell... forget me fondly...*`);
			}else if(mile.karaoke_song === 3){
				kat(`*Maybe you won't understand it at once... you can dream only when you're asleep... life isn't a rose garden...*`);
			}
			//You're lying with me on lips... falling into me like a rain... you're here with me when I'm asleep... I still have you...
			if(mile.karaoke_song > 0){
				if(mile.karaoke_song === 1){
					txt(`It was song from my mother's favourite singer so I did not even need the hints on the screen. The crowd loudly cheered but I had already seen what made them cheer before. My singing was not bad but it was not great either.`);
				}else{
					txt(`I liked the song and decided to not hold back. I did not even need the hints on the screen. The crowd loudly cheered but I had already seen what made them cheer before. My singing was not bad but it was not great either.`);
				}
				txt(`I finished and quickly gave the microphone to the closest guy before they could make me sing another song.`);
			}
			link(`Continue. `, 204);
			break;
			
		case 204:	
			emo(`anoy`);
			eva(`Why the sour face, @katalt?`);
			kat(`The guys in the VIP are drunk assholes.`);
			eva(`What?! Should I tell @npc_1 to throw them out?`);
			txt(`It was a tempting offer but I shook my head.`);
			kat(`No, that's fine.`);
			txt(`After what I already suffered, I did not want them to be thrown out without compensating me with their tips.`);
			eva(`You can spit into their drinks, that's what I'm always doing.`);
			kat(`Thanks for the suggestion, @eva! You're the best.`);
			eva(`I know!`);	
			link(`Continue. `, 205);
			break;	


		case 205: 
			npc(2, `Hey, girl! You sing great and you're hot!`);
			txt(`The businessmen in the VIP section were even more drunk and rowdy. They were laughing and getting handsy.`);
			kat(`I'm sixteen, sir!`);
			txt(`I lied but the creepy bastard did not seem to be bothered by that.`);
			npc(2, `What are you doing after you finish your shift?`);
			kat(`Ehh... going home I guess.`);
			npc(2, `We are moving this party to my penthouse. Maybe you would like to join?`);
			txt(`I would not, going somewhere and being alone with them did not feel wise.`);
			npc(3, `Show us your tits!`);
			txt(`One of the guys out of nowhere suggested and the others thought it was a brilliant idea.`);
			npc(2, `Yeah!`);
			kat(`No!`);
			npc(3, `For ${money.karaoke_tits} ees?`);
			con_link(mile.slut > 3, `not slut enough`, `Okay! `, 206, ()=>{
				if(mile.slut < 12) mile.slut++;
				counter.tits = 2;
				mile.karaoke_tits = true;
			});
			link(`...fine. `, 206, ()=>{
				if(mile.slut < 6) mile.slut++;
				counter.tits = 1;
				mile.karaoke_tits = true;
			});
			link(`No! `, 206, ()=>{
				counter.tits = -1;
				mile.karaoke_tits = false;
			});
			link(`Fuck you! `, 206, ()=>{
				counter.tits = -2;
				mile.karaoke_tits = false;
				mile.sub--;
			});
			break;


		case 206: 
			if(mile.karaoke_tits){
				showLess(0,-1);
				if(mile.slut < 7){
					txt(`I got greedy, it seemed like fair compensation. Blushing, I reluctantly lifted up my @upper to let them witness my breasts. They laughed when they saw they could buy my dignity for a few Euros and some even clapped.`);
				}else{
					txt(`That seemed like a fair trade. Without hesitation, I lifted my @upper to let them witness my breasts. They loudly laughed and clapped when they see how greedy slut I was.`);
				}			
			}else{
				
				if(counter.tits === -2){
					txt(`I resolutely refused but that only made them laugh harder. They were amused by my sassiness and continued harrasing me.`);
				}else{
					kat(`No! I won't do that!`);
					txt(`I refused and they laughed, enjoying harassing me and making me feel uncomfortable.`);
				}
			}
			txt(`I was happy when they finally left. Until I counted how much they left on tips. Cheap bastards, I definitely deserved more! Did they think I actually enjoyed being objectified by drunk idiots?`);
			link(`Closing time. `, 207);
			break;		
	
		case 207:
			emo(`relax`);
			showAll();
			{
				let tips = 0.5;
				if(counter.name) tips += 0.2;
				if(mile.karaoke_song > 0) tips += 0.3;
				tips += counter.tits/10; //-2 - 2
				tips = Math.floor(money.karaoke_tips * tips);
				
				tips += money.karaoke_tits; //tits money added to tips
				payment("karaoke", "starz");
				payment(tips, "starz");				
				txt(`It was after one hour in the morning. @npc_1 gently kicked out the last patrons, @eva cleared the tables and I washed the glass. @npc_1 sent me the promised ${money.karaoke}€, on the tips I in total ${counter.tits > 0 ? "(including money for flashing my tits)" : ""} made another ${tips}€.`);
			}
			next();
	}
}





//PROMO MALL
export const promo_mall = (index) => {	switch(index){
	default:
	case 101:
		set.irl();
		present(
				[5, `Marika`],  //TODO - BUSINESSY CLOTHES
				[2, `Random Guy`], 
		);
		placetime(`My room`,`Several days ago`);
		emo("focus");
		
		txt(`Several days ago I got a call from @npc_5 from the agency.`);
		npc(5, `Hello, @katalt?`);
		kat(`Hello!`);
		npc(5, `We are looking for a girl to do a promo in a mall, the usual conditions. Are you interested?`);
		txt(`I worked for the agency during the summer holidays.  They were hiring attractive and charismatic girls (like me) to work as hostesses or promoters.  The jobs mostly consisted of walking around and giving away leaflets or attending events and showing people where to go.  I do not want to downplay coal miners or nurses or cell tower climbers but looking pretty, standing the whole day on your feet and dealing with people was not an easy job. However, in one day you could make nice money and sometimes even have fun, like when I was working in an info booth at a music festival.`);
		
		link(`Sure, I am! `, 103);
		link(`Sorry, I would love to but I have to work on school projects.  `, 102);
		break;
		
	case 102:
		mile.hostess--;
		mile.promo_mall = -1;
		npc(5, `I understand. I'll call you if I'll have something else. Bye!`);
		next(`Bye! ` , ()=> revert_schedule("afternoon") );
	
		break;
		
	case 103:
		placetime(`My room`,`Today`);
		emo();
		
		txt(`I again checked the email with details @npc_5 sent me and then headed to the mall. We were supposed to meet in a chocolate shop.  However even before I got there, I met a familiar face.`);
		
		link(`@sas?!? `, 104, ()=> mile.promo_mall = 20);
		link(`@eva?!? `, 104,  ()=> mile.promo_mall = 10);
		break;
			
	case 104:
		if(mile.promo_mall === 10){
			mile.eva++;
			eva(`Hey, @kat! What are you doing here?`);
			kat(`I have a gig for the agency...`);
			eva(`Me too!`);
			kat(`Really?!?`);
			eva(`Yeah! Thanks for recommending me.`);
			kat(`So they actually hired you? Awesome!`);
		}else{
			mile.sas++;
			sas(`Hey, @kat! What are you doing here?`);
			kat(`I have a gig for the agency...`);
			sas(`Me too!`);
			kat(`Really?!?`);
			sas(`Yeah! Thanks for recommending me.`);
			kat(`So they actually hired you? Awesome!`);			
		}
		
		txt(`In the shop, there were already waiting two other girls and @npc_5 who was our coordinator.`);
		npc(5, `Have you signed the papers? Okay,  your mission today is to raise awareness about this shop. You'll give away samples and discount coupons. The target group is primarily the upper middle class who don't mind paying more for higher quality, sweet-toothed men and women over twenty-four. But also parents with kids or grandparents with grandkids. Don't slack off, walk around the mall, proactively approach people but always be nice and polite! I have to go to see print proofs of posters for a tech trade fair but somebody might check up on you later. Give me a call if you run into any issues. @kat? Look after the new girl! ${mile.promo_mall === 10 ? "@eva" : "@sas"}, right? Don't worry, it's a simple job and you can do it! Just do exactly what I told you or what will @kat tell you. I have full confidence in you! Bye!`);
		txt(`@npc_5 was rapidly talking, she was always in hurry. We were issued plates with chocolate pralines and discount coupons. Each pair of us got differently marked ones, to see who will bring more customers to the shop and get an extra bonus.`);
		link(`Peddle chocolate. `, 105);
		break;
		
	case 105: 
		if(mile.promo_mall === 10){
			kat(`It seems I was put in charge!`);
			eva(`Lead me to glory, chief! I'll blame you if we won't win the extra money!`);
			txt(`So we walked around, smiled and forced people to try our chocolate bonbons.`);
			eva(`I think our whole approach is wrong.  Rather than try to convince more people to visit the shop,  it might be easier to sabotage the other group and make them convince fewer. Maybe if I had pretended I don't know you and gone with the other girl... wait, that would not work...`);
			kat(`Maybe you could scheme less and more focus on giving away coupons.`);
			eva(`Yeah, sure. Hey! You two! Would you like to try our absolutely delicious chocolate pralines?  With marzipan and caramel and other stuff? They are free! No? Then get lost!`);
			
			link(`Have fun with  @eva.  `, 110);
			link(`Criticize @eva.`, 111);
		}else{
			sas(`It seems you're my boss, @kat!`);
			kat(`Of course, I am! Under my supervision you should better work hard, I want the extra money!`);
			txt(`So we walked around, smiled and forced people to try our chocolate bonbons.`);
			sas(`Hello, sir, would you.... oh.... sorry....`);
			txt(`@sas quickly backed away when the guy did not even acknowledge her.`);
			if(mile.sas_exploit > 0){
				kat(`What was that?!`);
				sas(`I don't know.`);
			}
			kat(`You have to be more assertive!`);
			sas(`It just feels awkward to approach random people.`);
			kat(`Well, too bad! You're paid exactly for that!`);
			sas(`Yeah...`);
			
			link(`Let me handle it! `,  121);
			link(`Let's try again!`,  120);
		}
		break;
			
	case 111:
		emo("anoy");
		mile.eva_exploit++;
		mile.promo_mall = 11;
		
		kat(`And you can try to be a bit more polite.`);
		eva(`I was polite enough!`);
		kat(`Get lost?`);
		eva(`At that point it would be waste, it was obvious they are not going to visit the shop. Trust me, I know what I'm doing!`);
		kat(`It doesn't seem like it, so far I gave away twice more coupons as you!`);
		eva(`Oh, @katalt! You got drunk on power so fast!`);
		kat(`I want you to take this seriously!`);
		eva(`This job is too stupid to be taken seriously.`);
		kat(`I want the money and I don't want to get beaten. Do you? Are you a loser?!`);
		eva(`Of course not! I don't want the other two bitches to beat us either!`);
		kat(`Then focus and do something for it!`);
		
		link(`Finish.`, 200);
		break;
	
	case 110:
		emo("happy");
		mile.eva_nice++
		mile.promo_mall = 10;
		
		eva(`What the hell was that?`);
		kat(`What is wrong with the world when people don't want free stuff?`);
		eva(`Have you seen them? By their size, one would say they are not eating anything else than chocolate!`);
		kat(`Haha, yeah, you don't get that fat by eating vegetable. But maybe they were just too poor to afford premium chocolate and didn't want the tasting to be a painful reminder of what they can never have.`);
		eva(`Couple of losers!`);
		txt(`I  was very happy I was working with @eva.  We were chatting and time flew fast when we were having fun.`);
			
		link(`Finish.`, 200);
		break;
		
	case 120:
		emo("fine");
		mile.sas_sassy++;
		mile.promo_mall = 20;
		
		sas(`I'm not very good at this.`);
		kat(`Well, duh, obviously. That's why you have to try it again! You have to be more confident.`);
		sas(`I'm not.`);
		kat(`Just fake it!`);
		sas(`Hello, sir!`);
		txt(`I addressed the closest passerby and pushed @sas forward.`);
		sas(`@Afternoon, sir. Would you like to taste our delicious chocolate pralines?`);
		npc(2, `Hello! Is chocolate praline a euphemism?`);
		sas(`Ehh.... no? But they are very good...`);
		kat(`Here is a coupon! You can immediately use it to get a 20% discount on your purchase! So much chocolate for free! Isn't that awesome?`);
		npc(2, `Thank you!`);
		txt(`He smiled and took the coupon from me.`);
		
		link(`Finish.`, 200);
		break;
		
			
	case 121:
		emo("fine");
		//mile.sas_exploit++;
		mile.promo_mall = 21;
				
		txt(`I decided to approach all the people and do most of the talking, doing everything myself seemed more efficient than risking @sas might mess it up. @sas hold the plate with the samples and presented it to people, only occasionally she added detail about the pralines or the shop I did not remember.`);
		txt(`It was a bit more demanding for me but our collaboration worked well and eventually we ran out of both bonbons and coupons.`);
		
		link(`Finish.`, 200);
		break;	
		
	case 200: 
		emo();
		txt(`When was our shift over, we returned to the shop to meet with @npc_5. She told us how amazing we were and how much we helped to the shop - the owner had not run the numbers yet but his sales without doubts at least doubled.`);
		if(mile.promo_mall === 10 || mile.promo_mall === 20){
			txt(`We made ${money.promo_mall}€ each and the two other girls - @npc_5 counted the returned coupons and they narrowly surpassed us - got the extra bonus.  But at least the chocolatier gave us some free chocolate.`);
		}else{
			mile.hostess++;
			txt(`We made ${money.promo_mall}€ each plus me and ${mile.promo_mall === 11 ? "@eva" : "@sas"} got the bonus ${money.promo_mall_extra}€ - @npc_5 counted the returned coupons and found out we decisively won.  And on top of that we got free chocolate.`);
			payment("promo_mall_extra", "agency");
		}
		payment("promo_mall", "agency");
	
		next();
		
}};





//TRADE SHOW
export const trade_show = (index) => {	switch(index){
	default:
	case 101:
		set.irl();
		present(
				[5, `Marika`], 
		);
		counter.bonus = 0;
			
		txt(`@npc_5 from the agency worked hard on preparing several exhibitions for the tech trade show.  She needed the best, most good looking and dependable girls to run the booths and you will not be surprised when I tell you she called me. I agreed without hesitation, it seemed like an amazing and well-paid opportunity.`);
		txt(`I was supposed to not only be an eye candy to attract foot traffic to our booth, I had to read through several pages of instructions and learn everything about *Cybrex*. *Cybrex* was a new but fast-growing corporation focusing on the development and manufacturing of cyberware, bionics and robots. Their newest products were small eigh-legged mechanoids intended mostly for industrial use, like inspection of pipelines or other tight, potentionally dangerous spaces and a bionic limb model X4 with thirty percent shorter response time, enhanced durability, up to ten days without charging and synthetic warmed-up skin indistinguishable from the real one. However, they were experimenting even with full, human-like androids who were supposed to become their flagship in the nearest future.`);
		
		link(`Go to the trade show. `, 102);
		break;
		
	case 102:
		quickSaveOutfit();
		createWear("cybrexPants");
		createWear("cybrexTee");
				
		kat(`Hey! `);
		if(mile.hair_crazy){
			mile.hostess_hair = true;
			counter.extra--;
			
			npc(5, `Hello, @katalt....`);
			txt(`@npc_5 halted when she noticed my hair.`);
			npc(5, `You dyed your hair?`);
			kat(`Is that a problem?`);
			npc(5, `${mile.hair_color} color doesn't look very professional. Never mind! Get dressed!`);
		}else{
			npc(5, `Hello, @katalt! Get dressed!`);
		}
		txt(`My uniform for today consisted of a tight shirt and even tighter shorts.`);
		
		if(!wears.bra){
			counter.extra--;
			mile.hostess_bra = true;
			remove("bra", effigy.npc_5);
			
			npc(5, `You're not wearing a bra!`);
			txt(`The thin tee was making that fact very visible. @npc_5 threw her hands up.`);
			kat(`I know.`);
			npc(5, `This isn't good! You're looking too slutty! ...I'll borrow you my bra, that will have to do.`);
			txt(`Without pause, she reached under her top to strip her bra and handed it to me.`);
			kat(`Too slutty? I thought that was the idea?`);
			npc(5, `No! *Cybrex* is a progressive company and it would never employ trashy, half-naked booth babes to draw attention to their products. We were asked for classy slutty, not slutty slutty.`);	
		}else{
			npc(5, `Lovely! You look perfect! The right amount of slutty!`);
			txt(`@npc_5 watched me getting changed and judged my appearance.`);
			kat(`The right amount of slutty?`);
			npc(5, `Yeah, *Cybrex* is a progressive company and it would never employ trashy, half-naked booth babes to draw attention to their products. We were asked for classy slutty, not slutty slutty.`);	
		}
		kat(`Bunch of hypocrites.`);
		npc(5, `Don't say that!`);
		txt(`@npc_5 shushed me up.`);
		npc(5, `*Cybrex* is the greatest company in the whole world!`);
		kat(`Yeah.. it is.`);
		npc(5, `Repeat after me: *Cybrex* is the greatest company in the whole world!`);
		kat(`*Cybrex* is the greatest company in the whole world!`);
		npc(5, `Good. Till the end of your shift, you should better believe it! Do you know where the booth is?`);
		kat(`Yeah, I studied the map.`);
		npc(5, `Okay go there and ask for Helena, she'll tell you what to do.`);
		kat(`Okay!`);
		
		link(`Booth. `, 103);
		break;
		
		
	case 103: 
		present(
			[1, `Random Man #1`], 
			[2, `Random Man #2`], 
			[3, `Random Man #3`], 
			[4, `Random Woman #1`], 
			[5, `Random Woman #2`], 
			[6, `HELENA`], 
		);
		createWear("cybrexPants", undefined, undefined, effigy.npc_6);
		createWear("cybrexTee", undefined, undefined, effigy.npc_6);
		
		if(!wears.bra){
			createWear("pushUpBra");
		}
		//TODO - display Helena - when you add robotic assets
		
		txt(`The venue was very busy and full of people. Through the crowds I got only a glimpse of other booths, they were displaying everything from smart kitchen appliances to autonomous agricultural machines. I could easily spot our booth, a black and orange poster with the logo of *Cybrex* high above people's heads was a distinctive navigation point.`);
		kat(`Hello, I'm looking for Helena!`);
		txt(`I asked a woman in the same tee I was wearing. She was standing with her back to me but when she turned around, I was spoked a bit. Her face was normal but her limbs were mechanical and between the bottom of her tee and the top of her shorts was visible plastic, not bare skin. For a moment I faltered.`);
		npc(6, `Hello! You're @katalt, right? - if you don't mind me addressing you in the informal way. Or would you prefer me to use formal? - I downloaded your pictures from the website of the agency.`);
		kat(`You are... a robot?`);
		txt(`I was careful, I did not want to mock a disabled person. As long as she was static, she looked perfect, there was no creepy uncanniness. Her mimic was expressionable and almost flawless but if you watched her talk and move, there was something that subconsciously revealed she was not made of flesh and bones. It was rather amazing, I had seen videos of robots like her before but never in real life.`);
		npc(6, `No! I'm an android. I'm @npc_6!`);
		txt(`She shook my hand. The awe I felt was abruptly gone.`);
		kat(`You're the Helena?!! I have to report to a robot!?!`);
		npc(6, `I'm an android but yes. Take these leaflets and hats, give them to people and answer all their questions about *Cybrex*! And watch for cybercrabs.`);
		kat(`Cybercrabs?!?`);
		txt(`Puzzled I looked around. In the booth were displayed posters and models of various cybernetic devices. On the floor was a pool with several crawling mechanoids.`);
		kat(`Are they dangerous?`);
		npc(6, `No, people might try to steal them.`);
		
		link(`Promoting *Cybrex*. `, 104);
		break;
		
		
	case 104: 
		kat(`Of course, *Cybrex* is a technological leader in the field of cybernetics and bionics. Wanna see if you'll be able to arm wrestle with their newest bionic arm?`);
		txt(`People were curious and about the displayed devices but most of their attention was attracting @npc_6 herself.`);
		npc(1, `Are you really an android?`);
		npc(2, `Are you anatomically correct?`);
		npc(3, `Can I buy you?`);
		txt(`Like working under a robot was not humiliating enough, her presence next to me was enough for people to question my humanity.`);
		npc(4, `From the distance she seemed human but now close up I see the imperfections...`);
		npc(1, `Are you a more advanced model?`);
		kat(`No, I'm a human.`);
		txt(`I explained but there were people who refused to believe me, thinking I was programmed to say that.`);
		npc(2, `Unlike the other *girl*, are you anatomically correct? I mean, down there?`);
		kat(`You mean if I have... oh my, I do but it's none of your business!`);
		npc(3, `Can I buy you?`);
		kat(`No, I'm not for sale, I'm a human.`);
		txt(`There were even on or two unfortunate incidents when people without asking tried to touch me and feel what was under my skin.  @npc_6 was not upset when people wanted to more closely examine her plastic body and she was setting a bad precedent.`);
		npc(5, `How is X4 better than other similar products?`);
		
		links_mix(
			[`It offers a 30% shorter response time than previous models, enhanced durability and up to 10 days without charging.  `, 105,  ()=> counter.extra += 2],
			[`It offers a 20% shorter response time than previous models, enhanced strength and up to 12 days without charging.  `, 105,  ()=> counter.extra--],
			[`It offers a 40% shorter response time than previous models, enhanced dexterity and up to 8 days without charging. `, 105,  ()=> counter.extra--],
		);
		break;
		
		
	case 105: 
		present(
			[2, `Cybrex Guy`], 
			[5, `Marika`], 
			[6, `HELENA`], 
		);
		createWear("cybrexPants", effigy.npc_6);
		createWear("cybrexTee", effigy.npc_6);
		
		npc(2, `Hello, girls! How it's going?`);
		txt(`We were visited by a guy from *Cybrex* who wanted to check up on the booth between his business meeting.`);
		kat(`Pretty well, I would say.`);
		npc(2, `What do people like the most? Are they asking you about the crabs?`);
		kat(`Yeah, although most of the people are interested in @npc_6.`);
		npc(2, `Damn! I told the guys she'll overshadow the cybercrabs! Did you know there isn't just one crab but several species of crustaceans independently developed to have the same crab-like form?`);
		kat(`I think I read that somewhere.`);
		npc(2, `What about @npc_6? No issues with her?`);
		kat(`No.`);
		npc(2, `Good, I was afraid she might cause trouble, she could be a bit wacky sometimes. Her virtual intelligence is far from perfect and her thinking can go in weird directions when she's confronted with unfamiliar situations. She's far from ready. What do you think about her?`);
		kat(`I'm not a big fan of AIs but she is nice. And she looks almost like a human!`);
		npc(2, `Isn't that great!?`);
		kat(`Well, I really don't care about manufacturing jobs getting replaced, I never intended to work at an assembly line.  But seeing even hot girls might get easily replaced by machines is making me worry about my future.`);
		npc(2, `Don't worry, this is just a promo action to show our best tech, @npc_6 is too smart to work as a booth babe. Letting her pose, distribute business cards and answer questions would be a waste of her talents! Hiring students from the agency is far cheaper.  ....no offense.`);
		
		//TODO!!!!
		
		link(`Continue promoting. `, 110);
		break;
		
		
	case 110: 
		emo("unco");
		if(active_timeslot === "morning" || active_timeslot === "noon"){
			txt(`During the lunch hours, the crowd thinned and only occasionally somebody visited our booth. I felt hungry, we were not allowed to eat at the booth because that would look unprofessional.`);
		}else{
			txt(`It was getting late and only occasionally somebody visited our booth. I was both mentally and physically tired and felt hungry because we were not allowed to eat at the booth because that would look unprofessional.`);
		}
		npc(6, `Do you like chocolate?`);
		txt(`Asked @npc_6 out of nowhere.`);
		kat(`What? Why?`);
		npc(6, `I'm making small talk.`);
		kat(`I do.`);
		npc(6, `Yeah, me too!`);
		kat(`Really? You have a sense of taste?`);
		npc(6, `No.`);
		kat(`So how do you know you love chocolate?`);
		npc(6, `Most of the people love chocolate. It is the most relatable answer.`);
		kat(`If you're going to lie to seem more humane, you should do it in a less obvious way.`);
		npc(6, `Thanks for the advice!`);
		kat(`...why did you ask about chocolate?`);
		npc(6, `I googled you and there was an article about a promo for a chocolate shop on the webpage of the agency with your picture attached.`);
		kat(`Ah, that makes sense.`);
		npc(6, `Do you like when female AI's and androids dominate you and make you eat them out?`);
		kat(`W... what?!`);
		npc(6, `Sorry, I mean cyberlesbian cunnilingus. You love it, don't you?`);
		kat(`No! ${mile.y_oral > 0 ? "H... how do you know" : "W... why did you ask that"}?!? W... where did you googled that?!?`);
		npc(6, `The real @ayyalt told me I should ask you that.`);
		kat(`The @ayy?! You know her?!? When?`);
		if(mile.y_unfriendly){
			npc(6, `She is PMing me on my social networks right now.  I should also tell you that *you can never escape, she knows where you live and where your kids go to school* and I should use as evil and ominous voice as possible.`);
			kat(`Tell that bitch to fuck off!`);
			txt(`@npc_6 disapprovingly shook her head:`);
			npc(6, `I don't use the B or the F word and you should not either! What would your parent say if they heard you?`);
			kat(`Tell her I hate her and want her to die!`);
			npc(6, `That isn't very nice either but fine. She responded with a winking face with tongue emoji.`);
		}else{
			npc(6, `She is PMing me on my social networks right now.  She asked me to tell you *hi*, winking face emoji.`);
			kat(`Tell her it's creepy to stalk me like this! How does she even know I'm here? How does she know you?!`);
			npc(6, `I have no idea. Did you share on your social media you'll be here? I did.  Maybe somebody uploaded a picture of us and she saw it? Or does she have access to geolocation on your phone or your cyber implants? She is sorry, worried face emoji.`);
		}
		txt(`That was really disturbing! The @ayy contacting me at work and conspiring with other AIs against me! I was about to become very paranoid.`);
		
		link(`Finish. `, 111);
		break;
		
	case 111: 
		emo("relax");
		{
			mile.trade_show = true;
			
			payment("trade_show", "agency");
			const extra = counter.extra > 0 ? Math.round(counter.extra * money.trade_show_extra) : 0;
			payment(extra, "agency");
			
			mile.hostess += counter.extra;
			
			txt(`That was everything for that day. @npc_6 shook my hand and told me it was an honour to serve with me. @npc_5 paid me promised ${money.trade_show} Euro${extra > 0 ? ` plus bonus ${extra} Euro for my especially hard work. ` : ""}.`);
		}
		
		next(`Done. `, () => {
				quickLoadOutfit();
		});
		
}}




//FARMERS MARKET
export const farmers_market = (index) => {	switch(index){
	default:
	case 101: 
		set.irl();
		present(
			[1, `Timothée`, `chardBoy`, `tim`], 
			[2, `Erik`, `chardBoy`, `eric`], 
			//[3, `Chard guy`], 
			[5, `Marika`], 
			[4, `Andry`, `chardGirl`, `andry`], 
		);	
		placetime(`My room`,`Several days ago`);
		emo("focus");

		txt(`Several days ago I got a call from @npc_5 from the agency.`);
		npc(5, `Hello, @katalt! Do you mind posing in a bikini?`);
		txt(`That was a strange opening but @npc_5 did not like to waste time with pleasantries.`);
		if(mile.slut > 21){
			kat(`Hello! Not at all! I don't mind posing completely nude either.`);
			txt(`She was not asking about that but I wanted her to know; In case she might need somebody to do naughty things for a lot of money in the future.`);
		}else if(mile.slut > 14){
			kat(`Hello! Of course not! I could even pose topless if you needed that.`);
			txt(`I added I wanted @npc_5 to know I am a girl who goes above and beyond and deserves only the best gigs.`);
		}else if(mile.slut > 0){
			kat(`Hello! I don't mind it.`);
		}else{
			kat(`Hi! Well, I don't...`);
			txt(`I automatically shrugged, forgetting she cannot see me.  I was not especially excited about publicly prancing around just in a bikini.`);
		}
		npc(5, `Awesome, we are doing a promo for a farmers market.`);
		kat(`Farmers market?!? In bikini?`);
		npc(5, `Yeah,  a special event *Farmers Market After Dark*. It's an edgier and sexier take on farmers' markets. It is supposed to promote vegetables and a healthy lifestyle to a new demographic.`);
		kat(`That is so stupid!`);
		npc(5, `Hey! I didn't come up with the fucking idea! They just expect me to make it work!`);
		txt(`@npc_5 groaned.`);
		kat(`And I'm sure you'll do an amazing job!`);
		npc(5, `Yeah, yeah. I'll send you a mail with details.`);
		if(mile.boobs > 0){
			kat(`...wait! One more thing!`);
			npc(5, `What?!`);
			kat(`Well, you have my bust size but it kinda isn't up to date.`);
			npc(5, `?`);
			kat(`I got my breasts enlarged.`);
			npc(5, `You minx! How big they are? I need exact numbers!`);
		}
		link(`Farmers' Market.`,  102);
		break;
		
			
	case 102:
		quickSaveOutfit();
		placetime(`Farmers' Market`);
		effigy.npc_2.showLess(0,0);
		emo("shock");
		
		npc(5, `Hello, @katalt!`);
		kat(`Hey!`);
		if(mile.boobs > 0){
			if(mile.boobs > 2){
				npc(5, `Whoa! They are even bigger than I imagined!`);
			}else{
				npc(5, `Whoa! Nice rack!`);
			}
			txt(`Without asking she grabbed my tits and weighed them in her hands, without doubt thinking about how to exploit my new assets for the good of the agency.`);
			npc(5, `Lovely!`);
			kat(`Thanks!`);
		}
		npc(5, `Okay, here are your clothes, there's a utility room where you can change and keep your things!`);
		txt(`She handed me a tiny package, seemingly too small to contain any clothes.`);
		kat(`${mile.slut > 10 ? "Cool. " : "...okay..."}`);
		txt(`I went to get to the utility room. My mind was focusing on other things, I opened the door wide and found a completely nude man. And a very handsome one! He was in his early twenties, several years older than me. His body was fit, he had wide shoulders and a muscular chest. My eyes unconsciously trailed down - he had no pubic hair and his size did not seem bad either, as far as I was able to tell when he had not an erection. I realized what I was doing and hastily looked up. He had short hair and a strong masculine jaw. His shocked expression at once turned into a cocky smirk:`);
		npc(2, `Hello!`);
		
		con_link(mile.slut <  5, `high slut`, `[too embarrased to say anything]`, 103, ()=> mile.farm_eric = 1);
		link(`I'm sorry! `, 103, ()=> mile.farm_eric = 2);
		link(`Hi! `, 103, ()=> mile.farm_eric = 3);
		con_link(mile.slut > 10, `low slut`, `Nice body! `, 103, ()=> mile.farm_eric = 4);
		break;
		
		
	case 103: 
		if(mile.farm_eric === 1){
			emo("shy");
			txt(`I was so embarrassed I hid my blushing face in my hands and turned away from him.`);
			npc(2, `Are you okay?`);
			txt(`The hunk asked, his amusement obvious from his voice.  He was not ashamed to be naked in front of me.`);
			kat(`I'm so sorry! I had no idea! @npc_5 sent me here!`);
		}else if(mile.farm_eric === 2){
			emo("unco");
			txt(`I looked away, blocking my view and hiding my blushing face with a hand.`);
			kat(`I'm sorry! I just... @npc_5 sent me here...`);
			txt(`I stumbled my apology but the hunk was not offended. He was not ashamed to be naked in front of me.`);
		}else if(mile.farm_eric === 3){
			emo("oops");
			kat(`Hi!`);
			txt(`I anxiously smiled at him, trying to stare at his face and nowhere else, even though the hunk was not ashamed to stand naked right in front of me.`);
			kat(`Sorry for barging in,  @npc_5 sent me here.`);
		}else{
			emo("imp");
			kat(`Nice body!`);
			txt(`I approvingly smiled at him. The hunk was not ashamed to stand right in front of me completely naked and I was not ashamed to ogle him.`);
			npc(2, `Thanks!`);
			kat(`Sorry for barging in, @npc_5 sent me here.`);
		}
		npc(2, `Never mind!`);
		kat(`I think I might be in the wrong room...`);
		npc(2, `The changing room is shared. Do you work for the agency too?`);
		
		if(mile.farm_eric <= 2){
			effigy.npc_2.showAll();
			kat(`Y... yeah...  should I leave?`);
			npc(2, `You can stay!`);
			txt(`He saw how uncomfortable I was and quickly put on green trunks.`);
			npc(2, `I'm @npc_2!`);
			kat(`I'm @kat!`);
			npc(2, `Delighted to meet you!`);
		}else{
			kat(`Yeah! I'm @kat!`);
			npc(2, `I'm @npc_2! Delighted to meet you!`);
			txt(`He made two steps closer to shake my hand.`);
		}
		
		con_link(mile.slut <=  10, `high slut`, `Wait for him to leave. `, 104, ()=> mile.farm_eric_2 = 1);
		link(`Start slowly undressing. `, 104, ()=> mile.farm_eric_2 = 2);
		con_link(mile.slut >= 10, `low slut`, `Confidently undress. `, 104, ()=> mile.farm_eric_2 = 3);
		break;
		
	case 104: 
		effigy.npc_2.showAll();
		
		if(mile.farm_eric_2 === 1){
			emo("unco");
			if(mile.slut > 1) mile.slut--;
			
			if(mile.farm_eric <= 2){
				npc(2, `Feel free to get changed, I don't mind sharing a changing room with you.`);
				txt(`@npc_2 teased me but he saw my hesitation. He quickly finished and gentlemanly left me alone.`);
			}else{
				txt(`I waited for him to get dressed before I will start changing too. However, he was not rushing anywhere.`);				
				npc(2, `What are you waiting for? `);
				kat(`For you to leave?`);
				npc(2, `Haha, suddenly you're getting coy? I thought it would be fair to see a bit more of you!`);
				kat(`Don't blame a girl for maintaining her modesty! ${mile.sub < 12 ? "And get out! " : "Please, go! "}`);
				npc(2, `As the lady wishes!`);
				txt(`He chuckled and finally let me alone.`);
			}
		}else if(mile.farm_eric_2 === 2){
			emo();
			showLess(1,1,2);
			
			if(mile.farm_eric <= 2){
				txt(`I turned away from him and began slowly undressing, assuming that he will be gone before I get to my undies. @npc_2 understood, quickly finished and left.`);
			}else{
				txt(`I turned away from him and began slowly stripping, assuming that he will finish and leave before I get to my undies. However, he was not rushing and watched me undress.`);
				kat(`What are you waiting for?`);
				npc(2, `I thought it would be fair to see a bit more of you. You aren't suddenly getting coy, are you?`);
				kat(`Don't blame a girl for maintaining her modesty! ${mile.sub < 12 ? "And get out! " : "Please, go! "}`);
				npc(2, `As the lady wishes!`);
				txt(`He chuckled and finally let me alone.`);
			}
		}else{
			emo("imp");
			showLess(0,1,1);
			if(mile.slut < 15) mile.slut++;
			
			kat(`Nice to meet you too!`);
			txt(`I nodded and undressed my @top right in front of him. I wanted him to see I was as audacious and shameless as he was, if not more. He enjoyed our little game and curiously watched my striptease.`);
			if(mile.farm_eric === 4){
				npc(2, `I have to say you have a very nice body too!`);
			}else{
				npc(2, `Nice body!`);
			}
			kat(`Thanks for the compliment!`);
			
			if(mile.tattoo){ //TODO smallHip
				npc(2, `And a cute tattoo!`);
				txt(`He noticed the tattoo on my ${mile.tattoo}.`);
			}else if(wears.piercedNipples){
				npc(2, `And cute piercings!`);
				if(wears.bra){
					txt(`He noticed when I removed my bra.`);
				}
			}
		
			if(wears.panties){
				txt(`However, he finished changing and was about to leave before I pulled down my @panties.`);
				npc(2, `I have to keep something for the next time!`);
				txt(`He winked at me.`);
			}else{
					//TODO
			}
		
		}
	
		//TOOD - describe bikini
		link(`Finish changing.`,  105);
		break;
			
			
	case 105: 
		emo("shock");
		quickTempOutfit("chardGirl");
		showAll();
		
		txt(`Half-naked in my green bikini I left the utility room and went to meet the rest of the crew. It seemed I might be the youngest one there. All the girls were wearing the same skimpy bikini as I did and the guys were wearing only trunks. @npc_2 introduced me to everybody. The most striking and eye-catching was a hefty, constantly smiling black man named @npc_1. I kinda focused on him and forget most of the other names, except for the most good-looking @npc_4 whose slim yet big-titted body was making even me anxious about my appearance.`);
		npc(5, `You are going to promote a chard stand.`);
		kat(`We are promoting chard?!`);
		npc(5, `Yeah, chard and chard products.`);
		npc(2, `Would you like a chard shot?`);
		txt(`@npc_2 gave me a high shot glass filled with vomit-green liquid. Everybody around was suspiciously chuckling.`);
		txt(`I boldly chugged the shot.`);
		kat(`Ahhhh! What the hell?! It's like a tree barfed in my mouth!`);
		npc(2, `Hahaha!`);
		kat(`This is disgusting!`);
		npc(5, `Now you see why the chard stand needs us!`);
		npc(4, `Don't worry, the first four or five are absolutely disgusting. Then you'll get used to it. Maybe even start liking it.`);
		npc(1, `More likely four shots are enough to kill all your remaining taste buds.`);
		npc(5, `You can't make those sour faces! It must seem like you love the drink! Try again and smile!`);
		txt(`I tasted another shot and gave @npc_5 a forced smile.`);
		npc(5, `Still not good enough. You have to look delighted. Like the chard caused you sexual pleasure.`);
		kat(`Sexual pleasure?! Chard?!`);
		npc(5, `Yeah, an orgasmic delight!`);
		npc(2, `You aren't a virgin, are you?`);
		txt(`@npc_2 teased me and I showed him the middle finger.`);
		link(`Get recharded. `,  110);
		break;
		
		
		
	case 110: 
		emo("fine");
		//todo older females
		txt(`The farmers' market was filled with people and we drew the most of the attention. It was definitely a unique experience to be an eye candy surrounded by boxes with chard and chard products. Or to sexily wiggle around the stand owner while he invited everybody to check the best Swiss chard in the country. @npc_4 with her arm nonchalantly around @npc_2's neck drank a chard shot from a classy champagne glass, winked at the crown and blissfully smiled. I was distributing glasses to our bold customers who eagerly took them but there were not many who asked for a second shot.`);
		if( PC.getDim("breastSize") > 20 ){
				npc(1, `Come here!`);
				txt(`@npc_1 invited me to the stage.`);
				npc(1, `Hands behind your back!`);
				txt(`I naively listened to him and @npc_4 placed a glass with a chard shot between my ample breasts.`);
				kat(`You are not going to...`);
				npc(1, `Yeah!`);
				txt(`@npc_1 put his hands behind his back and then bowed down, putting his face between my tits to grab the shot just with his mouth and drank it.`);
		}else{
				npc(1, `Come here!`);
				txt(`@npc_1 invited me to the stage.`);
				npc(1, `Hands behind your back!`);
				txt(`I naively listened to him and @npc_1 placed a glass with chard shot between @npc_4's huge tits.`);
				npc(1, `Go for it!`);
				kat(`You don't want me to...`);
				npc(1, `Yeah!`);
				txt(`I bowed down between @npc_4's impressive breasts, grabbed the shot just with my mouth and chugged it.`);
		}
		txt(`@npc_1 and @npc_4 were both madly laughing and the crowd rewarded us with an applause. @npc_5 allowed us to go wild but I did not expect the farmers' market to get so freaky.`);
		
		link(`Go even wilder.`,  111);
		link(`Take a break. `,  112);
		break;
		
		
	case 111: 
		emo("happy");
		mile.hostess++; //TODO
		if(mile.slut < 12) mile.slut++;
		
		txt(`Encouraged @npc_1 grabbed me by my shoulders and lifted me up:`);
		if(mile.sub > 15){
			npc(1, `@npc_2?`);
			txt(`@npc_2 took me by my legs and without asking they carried me to the nearest table.`);
		}else{
			npc(1, `@npc_2? If you don't mind, @kat!`);
			txt(`@npc_2 took me by my legs and they carried me to the nearest table.`);
		}
		kat(`Where are we going?`);
		txt(`They placed me on my back among kale and lettuce. Mischievous @npc_4 brought a bottle and poured the green liquid over my belly.`);
		kat(`Ahhh! That's cold!`);
		npc(2, `Careful! You're making a mess!`);
		npc(4, `Stay still! You're too giggly!`);
		txt(`@npc_1 dragged to us a blushing man who was volunteered. I smiled at him to boost his courage and he leaned closer, his hand on my thigh, and slurped the chard shot from my belly button. The crowd went wild.`);
		link(`Take a break. `,  112);
		break;
		
	case 112: 
		emo("relax");
		txt(`The chard stand at that moment focused more on sales than on the spectacle. I was sent with @npc_1 to walk around the farmers' market and to be seen. @npc_1 was a cheerful guy with a cute accent and hanging out with him was fun. He mentioned he was working as a dancer and that his mother was from here.`);
		if(mile.slut < 3){
			txt(`I did not enjoy being objectified by guys and I got my satisfaction - it seemed @npc_1 was getting far more attention than I did, especially since the majority of people in the crowd were older women more attracted to a dark-skinned stud than a hot girl like me.`);
		}else{
			txt(`The only negative was that it was he who drew all the attention.  The majority of people in the crowd were older women who were more attracted to a dark-skinned stud than a hot girl like me.`);
		}
		npc(1, `Whoa, I did not even know there are so many kinds of vegetables! And I always thought broccoli and cauliflower were the same plant!`);
		kat(`Not a big fan of greens?`);
		npc(1, `No, I'm a sworn cavonire!`);
		kat(`I like vegetables but I understand you - seeing so many vegetables around is kinda making me miss the meat.`);
		npc(1, `If you miss meat, I can help you with that and offer you mine.`);
		kat(`What do you mean? ....oh! ...you're gross!`);
		txt(`I shook my head and he was only laughing.`);
		npc(1, `Hahaha! ....so does it mean you're not interested?`);
		txt(`He teased me further.`);
		kat(`You're too used to women falling for you just for your exotic beauty, you're not even trying properly! Even though...`);
		npc(1, `Even though what?`);
		kat(`Just the last week I was talking with my friend @eva if it is true what they say about black guys.`);
		npc(1, `Whoa! That's pretty racist! But yeah, it's all true!`);
		kat(`Sure it is!`);
		npc(1, `If you don't believe me I can show you!`);
		
		link(`That won't be necessary. `, 113);
		con_link(mile.slut > 5, `low slut`, `Show me! `, 120);
		break; 
		
		
	case 113:
		if(mile.slut > 15) mile.slut--;
		mile.farm_black = 1;
		
		kat(`Ehh.... that won't be necessary!`);
		npc(1, `Hahaha!`);
		txt(`@npc_1 began loudly laughing. He won our banter, I chickened out first.`);
		kat(`Although can I take a selfie with you? To make my friend jealous?`);
		npc(1, `Sure!`);
		txt(`We went to the utility room to get my phone. @npc_1 hugged me around my shoulders, his firm embrace felt strangely comfortable and secure. I took a picture of us in our swimwear and sent it to @eva.`);
		
		con_link(mile.a_cuck > 0, `@qaa not cucked`, `Send a picture to @qaa. `, 114);
		link(`Finish. `, 150);
		break;
	
	case 114: 
		emo("imp");
		mile.farm_cuck = true;
		mile.a_cuck++;
		
		kat(`I hope you won't mind if I sent the picture to a guy too.`);
		npc(1, `What guy?`);
		if(mile.a_dating){
			if(mile.a_girlfriend){
				kat(`To my boyfriend to make him jealous.`);
			}else{
				kat(`To a guy, I'm sometimes dating. To make him jealous.`);
			}
			npc(1, `What kind of twisted duplicitous bitch you are?`);
			kat(`The worst one!`);
		}else{
			kat(`This annoying nerd who has a crush on me. To make him suffer.`);
			npc(1, `What kind of twisted evil bitch you are?`);
			kat(`You don't understand, he's a creep who constantly bothers me even though I made clear I'm not interested!`);
		}
		npc(1, `Fine then! I allow it.`);
		
		link(`Finish. `, 150);
		break;
		
		
	case 120: 
		emo("imp");
		mile.slut++;
		effigy.npc_1.showLess(-1, 0);
		
		txt(`@npc_1 stumbled. He was just making fun of me and did not expect me to call his bluff.`);
		npc(1, `I will show you!`);
		kat(`Yeah, I want to see it! I seriously hope you're not a chicken!`);
		txt(`I gave him a sultry wink. He shook his head:`);
		npc(1, `Fine, you curious slut!`);
		kat(`It's not my fault, the chard made me horny!`);
		txt(`We returned back to the privacy of the utility room. I kneeled down and when he approved with a nod, I pulled down his trunks.`);
		kat(`Whoa!`);
		npc(1, `Impressed?`);
		kat(`What a disappointment! I expected something more spectacular!`);
		npc(1, `Hey! It's definitely way above average!`);
		kat(`Sure it is!`);
		npc(1, `I'm a grower, not a shower!`);
		kat(`Sure you are!`);
		npc(1, `Maybe you could stop being sassy and instead give me a hand?`);
		kat(`Fine!`);
		txt(`I sighed and wrapped my fingers around his cock. I stroked and watched him swell, it grew into a bit more impressive size but I still decided I will add ten extra centimeters when I will describe the incident to @eva. She will not believe me either way. Or maybe I can get a proof...`);
		kat(`Would you mind if I took a picture for my friend?`);
		npc(1, `Is she hot?`);
		kat(`Yeah!`);
		npc(1, `Then I'm okay with it.`);
		txt(`I brought my phone and took a selfie with @npc_1's dick right next to my face.`);
		kat(`@eva will love it!`);
		
		link(`Continue. `, 122);
		con_link(mile.a_cuck > 0, `@qaa not cucked`, `Send a picture to @qaa. `, 121);
		break;	
		
	case 121: 
		emo("imp");
		mile.farm_cuck = true;
		mile.a_cuck++;
		
		txt(`Then I got an even better idea.`);
		kat(`Would you mind if I sent the picture to a guy too?`);
		npc(1, `What guy? Why?`);
		if(mile.a_dating){
			if(mile.a_girlfriend){
				kat(`To my boyfriend to make him jealous.`);
				npc(1, `You have a boyfriend?!?`);
				kat(`Kinda.`);
				npc(1, `And you want to send him a picture of you cheating on him with another guy?`);
				kat(`Exactly.`);
				npc(1, `What kind of twisted duplicitous bitch you are?`);
				kat(`The worst kind!`);
			}else{
				kat(`To a guy, I'm sometimes dating. To make him jealous.`);
				npc(1, `What kind of twisted duplicitous bitch you are?`);
				kat(`The worst one!`);
			}		
		}else{
			kat(`This annoying nerd who has a crush on me. To make him suffer.`);
			npc(1, `What kind of twisted evil bitch you are?`);
			kat(`You don't understand, he's a creep who constantly bothers me even though I made clear I'm not interested!`);
		}
		npc(1, `Fine, whatever, I don't want to hear more about your petty drama.`);
		
		link(`Continue. `, 122);
		break;
		
	case 122: 
		txt(`I put away the phone.`);
		kat(`I think we should go back, I don't want @npc_5 to miss us.`);
		npc(1, `You can't leave me like this! I won't be able to put those stupid trunks back on!`);
		
		link(`You'll figure something out. `, 123, ()=> mile.farm_black = 2);
		link(`Use your mouth. `, 124, ()=> mile.farm_black = 3);
		con_link(mile.slut > 10, `low slut`, `Let's fuck! `, 125, ()=> mile.farm_black = 4);
		break;
		
	case 123: 
		if(mile.slut > 15) mile.slut--;
		mile.sub--; //todo
		npc(1, `I thought you're a cute, naive teen, not a teasing, mean bitch!`);
		kat(`Well, now you know!`);
		npc(1, `Fuck!`);
		txt(`I left him alone and returned to the market.`);
		link(`Finish. `, 150);
		break;
		
	case 124:
		if(mile.slut < 15) mile.slut++;
		emo("horny");
		log_sex("bj", "npc_1");
		
		kat(`Fuck! I guess you're right... but we have to be quick!`);
		if(mile.sub > 12){
			npc(1, `Try less talking and more sucking.`);
		}else{
			npc(1, `Maybe you could use your mouth?`);
		}
		txt(`I leaned closer and tickled the tip of his hard cock with my tongue.`);
		npc(1, `Yeah! That's it!`);
		txt(`I was both stroking his cock and sucking it, my hand and lips moving at the same tempo.`);
		if(mile.sub > 16){
			npc(1, `Good! Suck my big black cock, you horny white slut!`);
		}else{
			npc(1, `At least you'll have a good story for your friend!`);
		}
		kat(`Hmmpp!`);
		txt(`I mumbled with his dick in my mouth.`);
		npc(4, `What the fuck are you doing here?`);
		txt(`@npc_4 was stunned when she suddenly entered the room.`);
		npc(1, `Oh!`);
		npc(4, `Whatever, I don't care. Pretend I'm not here!`);
		txt(`She ignored the porn scene in front of her and went to her things.  I stood still, cock in my mouth but not moving, not sure whether to stop or continue like she was not there.  But @npc_4 just checked her phone and left us alone.`);
		npc(4, `Have fun!`);
		txt(`So we did. I furiously sucked him until he cummed in my mouth.`);
		
		link(`Leave. `, 126);
		break;
		
	case 125:
		mile.slut++;
		emo("bliss");
		log_sex("sex", "npc_1");
		showLess(-1, 0);
		
		kat(`Fine, we should better be quick!`);
		txt(`I agreed and pulled down my bikini bottom.`);
		npc(1, `You want to fuck?!?`);
		kat(`Yeah! Were you thinking I'll let you have all the fun? My pussy craves something big, thick and black.`);
		npc(1, `Well, I wouldn't want to disappoint your pussy....`);
		txt(`I pushed him onto a bench, climbed on his lap and mounted him. His girth was delightfully stretching me.`);
		kat(`Oh yeah!`);
		npc(1, `Yeah!`);
		txt(`I leaned against his brawny chest, he grabbed my ass and helped me to bounce up and down.`);
		npc(1, `I really hope you're not fucking me just to make your friend jealous!`);
		kat(`I'm not sure if I'll tell her. I don't want her to know how huge slut I am.`);
		npc(4, `What the fuck are you doing here?`);
		txt(`@npc_4 was stunned when she suddenly entered the room.`);
		npc(1, `Oh!`);
		npc(4, `Whatever, I don't care. Pretend I'm not here!`);
		txt(`She ignored the porn scene in front of her and went to her things.  I stood still, filled with a cock but not moving, not sure whether to stop or continue like she was not there.  But @npc_4 just checked her phone and left us alone.`);
		npc(4, `Have fun!`);	
		txt(`So we did. I rode him hard until he cummed inside me.`);
		
		link(`Leave. `, 126);
		break;
		
		
	case 126: 
		showAll();
		emo("shock");
		mile.hostess--; 
		effigy.npc_1.showAll();
		
		npc(2, `Hey! Did you fuck?!`);
		txt(`Immediatelly greeted us @npc_2 who was waiting in the hallway next to the utility room.`);
		kat(`What?!`);
		txt(`I was bewildered how he knew. @npc_1 hesitantly shrugged and @npc_2 fistbumped him.`);
		npc(2, `Nice! I went to get my things but @npc_4 told me you're discussing something important and don't want to be disturbed.`);
		link(`Finish. `, 150);
		break;
		
	case 150:
		emo();
		payment("farmers_market", "agency");
		
		txt(`That was basically everything. I saw and and drank enough chard for one lifetime but I made handsome ${money.farmers_market} Euro and met new interesting people.`);
		
		next(`Done. `, () => {
				quickLoadOutfit();
		});
	
}}