import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, maj, mol, tom, zan, ven, mik, kub, voj} from "Loop/text";
import {bold, hint, bux} from "Loop/text";
import {aly, emy, reac, and, s, capitalise, relation, or} from "Loop/text";
import {emo, updateDraw, wdesc, effigy, makeup, wearRandomClothes, remove, wear, create, wears, showLess, showAll, violates, sluttiness,  checkQuality } from "Avatar/index";
import {placetime, log_sex} from "Loop/text_extra";
import {active_timeslot, next_event, add_today, add_workday} from "Taskmaster/scheduler";
import {toHSLA, nameColor, parseHSLA} from "Libraries/color/index";
import {ra} from "Libraries/random";
import {DISPLAY} from "Gui/gui";
import {time} from "System/time";
import {payment} from "System/bank_account.js";
import {money} from "Data/items/prices.js";
/*

Acting chairman!
What do you mean by that?
Well, there were no elections yet, you're the chairman just because you were one the last year. 
I'm full chairman till the next elections! 


▲



*/



//VENDY MAKEUP
export const vendy_makeup_initiation = (index)=> {
	switch(index){
		default:
		case 101:
			set.irl();
			placetime(`classroom`);
			ven(`@kat! `);
			kat(`Hello! `);
			txt(`I smiled at @ven wondering what she wants. `);
			ven(`I just wanted to say, you look amazing! I love your makeup! `);
			kat(`Thanks! `);
			txt(`At first, I was suspicious but her excitement seemed to be genuine. `);
			ven(`I really like what you did with your eyes! Would you mind if I ask, what shade of lipstick it is and where did you get it? `);
			//kat(`Of course! It's ${nameColor(toHSLA(PC.apex.irl_lips))}. `);
			kat(`Of course! `);
			txt(`I gladly explained her what she wanted to know. `);
			ven(`Nice! I wish I was as good as you. I'm too clumsy. Whenever I try to experiment a bit more, the results are horrible! `);
			txt(`She surely had a reputation for trying too hard and failing in the most spectacular way. However, reminding her that while she was complimenting me would be too mean. `);
			kat(`Well, doing perfect makeup isn't as easy as people imagine. `);
			txt(`She enthusiastically nodded: `);
			ven(`Sure it isn't! I would probably need somebody to help me. `);
			txt(`I got where she was heading. It was unexpected, we were not very close friends. On the other hand, if anybody could fix her face it was me. Without doubts, I was the best person to ask. `);
			txt(`She would owe me a huge favor. But it would mean wasting an afternoon with <i>her</i>. `);
			ven(`Maybe you could help me? I'll understand if you say not, I'm aware you're very busy but I'd appreciate it a lot!`);
			
			link(`Sure! Sounds like fun! `, 102, ()=> mile.stat++);
			link(`I'm very sorry but I don't have time at the moment, maybe the next week? `, 103);
			con_link(mile.sub < 5, `submissive`, `Sorry, I don't fight for lost causes. `, 104, ()=> mile.stat++);
			break;
			
			
		case 102:
			add_today("afternoon", "vendy_makeup_action");
			mile.vendy_makeup = 1;
			txt(`I still did not fully understand why is she out of nowhere so interested in me but I decided to agree and help her.`); 
			ven(`Thank you! You're awesome!`);
			kat(`Obviously. `);
			ven(`Do you have time this afternoon? `);
			kat(`I'll text you.`);
			ven(`Deal!`);
			next();
			break; 
			
		case 103:
			mile.vendy_makeup = -1;
			emo("anoy");
			ven(`Oh! Sorry for bothering you!`);
			kat(`No! I'm sorry! I'd love to help you!`);
			ven(`You don't have to lie to make me feel better! I understand you think I'm not cool enough to hang out with miss perfect!`);
			kat(`I didn't say that! `);
			ven(`You didn't have to! Don't worry! I won't bother you anymore!`);
			txt(`@ven swiftly turned and left. What the hell was her problem!?!`);
			next();
			break;
			
		case 104:
			mile.vendy_makeup = -2;
			emo("smug");
			ven(`W..what do you mean!?!`);
			kat(`That no makeup can save your face. `);
			txt(`I raised my voice and few guys who heard my quip chuckled. @ven was red and furious. `);
			ven(`Why do you have to be such a bitch!`);
			kat(`Why do you have to be so ugly? `);
			txt(`She was shaking but was not able to come up with a good comeback. She turned and quickly walked away, probably to cry at bathrooms. To an observer it might seem needlessly cruel but I made sure she will never ever bother me again. `);
			next();
			break;
	}
};

export const vendy_makeup_action = (index)=> {
	switch(index){
		default:
		case 101:
			mile.vendy_makeup = 2;
			set.irl();
			placetime(`my room`);
			ven(`Nice room!`);
			kat(`Thanks!`);
			txt(`I invited @ven at my place where I had all the important stuff I needed. I gave her a crash course, it was fun to share my wisdom with her. Because I did not felt threatened by her I even told her several secret tricks.`);
			link(`Makeup. `, 102, ()=>{}, ()=> DISPLAY.edit_avatar(effigy.ven, "makeup") );			
			break;
			
		case 102:
			ven(`I noticed you changed you style!`);
			kat(`Well, yeah. I'm trying something new. `);
			ven(`You look fine! I wish I had your figure!`);

			if(ext.background === 3){
				kat(`Thanks, I'm trying to stay fit! `);
				ven(`I do too but I'm never able to find enough time. I admire you're able to manage everything and you're the class chairman on top of that!`);
				txt(`And she didn't know a half of it! But I just shrugged. `);
				kat(`You have to organize your time well!`);
				ven(`You're running for the chairman again, right?`);
				txt(`I was puzzled by her question. `);
				kat(`Of course! `);
				ven(`That's great! I think you're doing a really good job! Do you plan to continue the same way? Or do any changes during your next term? `);
				kat(`I will keep doing what is working.`);
ven(`So your election theme will be something like keep the steady course? `);
kat(`Well, I haven't thought about details yet but basically yeah! `);
				ven(`It's pretty impressive you managed to get elected so many times in a row! How are you doing it? `);
				kat(`Well, the elections are basically a popularity contest. As long as you're competent on the basic level and the most popular of the people who decide to run it's piece of cake. `);
				ven(`No wonder you won so many times! `);
kat(`Thank you! `);
txt(`I accepted her compliment without fake modesty. I began wondering why is she suddenly so interested in my reelection campaign when @ven changed the topic: `);
ven(`What do you think about @zan and @tom?`);
				kat(`It can't work! He's a womanizing jerk and she's a slut!`);
				ven(`I think they look cute!`);
				txt(`We continued discussing the latest gossips. `);
			}
			txt(`Anyway, it was a pretty fun afternoon without anything weird or humiliating. Maybe I should invited @ven the next time when will hang out with @eva and @sas? `);
			next(`Continue. `, ()=> effigy.save("ven") );
	}
};


//WOBBLY DESK
export const wobbly_desk = (index)=> {		
	switch(index){
		default:
		case 101:
			emo("anoy");
			placetime(`classroom`);
			eva(`@KAT! We need you!`);
			kat(`What is going on? `);
			txt(`@eva and @qbb were facing each other, obviously enranged. `);
			eva(`This filthy thief stole my desk!`);
			qbb(`I did not! She's making it up!`);
			eva(`My desk was perfectly fine but now it's wobbly and it isn't even my original desk! `);
			qbb(`That isn't my problem!`);
			eva(`It is! Because you stole it! And I want it back!`);
			kat(`And what do you want me to do? `);
			eva(`We want you to decide. `);
			qbb(`It seems like a job for the class chairman. `);
			eva(`Yeah! Tell him to give me back my desk and apologize!`);
			qbb(`Tell her to apologize to me! `);
			txt(`They did not care I was the chairman. They both assumed I will support them, @eva because I was her friend, @qbb because he was blackmailing me. `);
			qbb(`She's making it up! She has no proof! `);
			kat(`How do you know it was him? `);
			eva(`What!?! You believe this weirdo more than me?! `);
			kat(`No! I just need more information to decide. `);
			eva(`His desk was wobbly and now it isn't! I knew it because his constant wobbling was constantly annoying me!`);
			qbb(`I fixed it!`);
			eva(`No! You stole and replaced mine! `);
			qbb(`How would you know, all desks look the same! `);
			eva(`I recognize my desk!`);
			
			link(`You're overreacting, @eva!`, 103);
			link(`Give her her desk back, @qbb`, 104);
			link(`Can't you steal some other desk from somebody else? `, 102);
			break;
			
		case 102: 
			eva(`What!? I'm not a thief!`); 
			qbb(`He who lies, steals!`);
			eva(`And even if I might be tempted to do that, this is a matter of principle! Do you want to live in a world when you arrive in the class only to find out your beloved desk was replaced by a useless wobbly one by a selfish asshole? `);
			qbb(`Don't try to weasel your way out, @kat. We want to hear your decision! `);
			
			link(`You're overreacting, @eva!`, 103);
			link(`Give her her desk back, @qbb`, 104);
			break;
		
		case 103: 
			mile.eva_exploit++;
			mile.wobbly_desk = 2;
			eva(`What the hell!?! I'm overreacting!?`);
			kat(`I spent the whole break listening to your crazy rambling about wobbly desks! It's just a stupid desk!`);
			eva(`It's my desk! And it isn't wobbly!`);
			kat(`You weren't able to prove it! `);
			eva(`Is this a fucking court? `);
			kat(`For all intents and purposes, yeah!`);
			qbb(`Wise ruling! And she has to apologize for unfairly accusing me!`);
			eva(`I won't do that!`);
			kat(`No, she doesn't have to. `);
			txt(`@eva was pretty angry and menacingly wobbled with her desk while @qbb looked very pleased. `);
			next();
			break;
	
		case 104: 
			//TODO
			mile.eva_nice++;
			mile.eva++;
			mile.wobbly_desk = 1;	
			eva(`Immediately!`);
			qbb(`What!?! Why do you think It was me who stole it? `);
			kat(`Who else would do something so insidious? `);
			qbb(`But..`);
			kat(`No more arguments! I wasted the whole break listening to your crazy rambling about stupid wobbly desks!`);
			eva(`My desk isn't wobbly and I want it back! Also, he has to apologize!`);
			qbb(`I won't do that!`);
			kat(`No, he doesn't have to. `);
			txt(`Offended @qbb moved the desks, then sat down and menacingly wobbled. @eva was very pleased with my decision. `);
			next();
			break;
	}
};
	
	
//VENDY RUNNING
export const vendy_running = (index)=> {
	switch(index){
		default:
		case 101: 
			mile.stat--; 
			emo("anoy");
			placetime(`classroom`);
			mile.vendy_running = 1;
			ven(`@katalt? Do you have a moment? I need to talk with you. `);
			txt(`I was approached by @ven.`);
			if(mile.vendy_makeup === 2){
				kat(`Sure! What's going on? You look gorgeous!`);
				ven(`Thanks again for your help!`);
				kat(`You're welcome!`);
			}else{
				kat(`Sure! What's going on?`);
			}
			ven(`I just wanted to say I always looked up to you and admired you. `);
			kat(`Thank you!`);
			txt(`It was nice to listen to that. However, I was not sure where was she heading. `);
			ven(`I think you did an amazing job as the class chairman. Your successor will face a huge challenge!`);
			kat(`Yeah?`);
			ven(`So much hard work! You definitely deserve a break! And since you're such a big inspiration for me so I decided to give it a try.`);
			kat(`You mean that...`);
			ven(`Yeah! I'll run for the class chairman! Isn't that wonderful? Don't worry! I'll do my best to be your worthy successor!`);
			txt(`That little bitch!`);
			kat(`Amazing!`);
			ven(`This year you won't run unopposed! The elections will be far more exciting!`);
			kat(`Yeah! When I win people won't be able to say it was because nobody else dared to run!`);
			ven(`If you win. `);
			txt(`Now she went too far!`);

			link(`Good luck! May the best girl win!`, 102);
			link(`You're out of your mind if you think you have a fucking chance against me! `, 103);
			break;
		
		case 102:
			
kat(`Oh, I'm sure she will!`);
txt(`I despised her smug smirk! But I managed to keep my emotions under control and just smiled back at her. I will fucking destroy her!`);
			next();
			break; 
			
		case 103:
			emo("angry");
			mile.vendy_running = 2;
			ven(`Please, calm down, @katalt. There's no reason to raise your voice. `);
txt(`On her face was an infuriating smug smile, she wanted to  made me overreact! `);
			kat(`Don't fucking tell me what to do! You're dumb if you think you can defeat me!`);
			ven(`We'll see! I believe I can and I will! And dressing like a tramp won't win you any points!`);

			{
				const slut = sluttiness().details;
				const a = [];
	//TODO HEELS? STOCKINGS?
//TODO ASAP SLUT.SLUT??????
				if(slut.slut) a.push(`you're dressed like a whore`);//???
				if(slut.titfall){
					a.push(`your tits are nearly falling out`);
				}else if(!wears.bra){
					a.push(`you forgot your bra at home`);			
				}
				if(slut.genitals){
					a.push(`when you bend over you flash everybody your cunt`);
				}else if(slut.panties){
					a.push(`your ${wdesc.lower} is so short everybody can see your panties`);
				}else if(wears.microSkirt){
					a.push(`your ${wdesc.lower} is so short I can almost see your panties`);
				}else if(wears.shorts){
					a.push(`your ass barely fits in those trashy ${wdesc.lower}`);
				}
	//TODO MORE	
		
				if(a.length){
					mile.stat--; //MILE!!
					kat(`I'm not dressed like a tramp!`);
					ven(`${capitalise(and(a))}!`);
					txt(`I helplessly stare at her. How to defend against that when she was completely right? People watching our heated argument laughed at my expense when they heard @ven's accurate observation. `);
				}else{
					txt(`I just smirked. She was just envious of my perfect figure! `);
					kat(`Sure! Whatever!`);
				}
			}
			
			txt(`@ven leaned closer to whisper in my ear:`);
			ven(`Enjoy being the class chairman while you still can!`);
			txt(`I whispered in hers:`);
			kat(`Good luck! You'll need it because I'll fucking destroy you!`);
			next();
			break;
			
	}
	
};


//AAA X CCC discussion 
export const vendy_a_discussion = (index)=> {
	mile.vendy_running_complain = 1;
	set.meta();
	placetime(`metaverse`);
	emo("anoy");
	
	//FRIENDSHIP
	qaa(`Is everything okay, @kat? You seem sullen. `);
	kat(`Oh? Because I fucking am! Nothing is okay! `);
	qaa(`What is going on?! `);
	txt(`He leaned closer and the tone of his voice got serious. `);
	kat(`That little ${mile.vendy_makeup > 1 ? "thankless " : "petty "}bitch @ven just told me she'll run for the class chairman. `);
	qaa(`Hah, is that all? `);
	txt(`That idiot only started laughing, not understanding how grave the situation was. `);
	kat(`What if the class will elect her instead of me?!? I'm the class chairman! `);
	qaa(`It won't be a huge loss. It's a completely useless post with no actual power anyway, the elections are basically just a popularity contest.`);
	kat(`They aren't just a popularity contest! It's an important job with actual power! Who do you think decided how to spend the remaining 14 Euro in the class fund the last year? `);
	qaa(`@sas? She is the class treasurer? `);
	kat(`NO! The treasurer has no authority to decide shit! It was me!`);
	qaa(`14 Euro? Such responsibility!`);
	kat(`And you don't see the bigger picture! This isn't about being the class chairman! It's about being the president of the student council! That's where the real power is! It was pointless to compete with @pet. The last year I had a chance but that bitch Maru won just because she was a senior! Now is my time to shine!`);
	qaa(`That's pretty ambitious.`);
	txt(`I was not sure whether he was serious or whether he was making fun of me. `);
	kat(`Do you think I'm not good enough to be the head of student council!?!`);
	qaa(`Of course not! You'd be perfect! People will love you and despair!`);
	kat(`Now you see I have to win! At all cost! `);
	next();
};

export const vendy_c_discussion = (index)=> {
	mile.vendy_running_complain = 3;
	set.meta();
	placetime(`metaverse`);
	emo("anoy");
	
	//FRIENDSHIP
	qcc(`Is everything okay, @kat? You seem sullen. `);
	kat(`Oh? Because I fucking am! Nothing is okay! `);
	qcc(`What is going on?! `);
	txt(`He leaned closer and the tone of his voice got serious. `);
	kat(`That little ${mile.vendy_makeup > 1 ? "thankless " : "petty "}bitch @ven just told me she'll run for the class chairman. `);
	qcc(`Hah, is that all? `);
	txt(`That idiot only started laughing, not understanding how grave the situation was. `);
	kat(`What if the class will elect her instead of me?!? I'm the class chairman! `);
	qcc(`It won't be a huge loss. It's a completely useless post with no actual power anyway, the elections are basically just a popularity contest.`);
	kat(`They aren't just a popularity contest! It's an important job with actual power! Who do you think decided how to spend the last 14 Euro that remained in the class fund the last year? `);
	qcc(`@sas? She is the class treasurer? `);
	kat(`NO! The treasurer has no authority to decide anything! It was me! And you don't see the bigger picture! This isn't about being the class chairman! It's about being the head of the student council! That's where the real power is! It was pointless to compete with @pet. The last year I had a chance but that bitch Maru won just because she was a senior! Now is my time to shine!`);
	qcc(`That's pretty ambitious.`);
	txt(`I was not sure whether he was serious or whether he was making fun of me. `);
	kat(`Do you think I'm not good enough to be the head of student council!?!`);
	qcc(`Of course not! You'd be perfect! People will love you and despair!`);
	kat(`Now you see I have to win! At all cost! `);
	next();
};






















//TOM VOTES FOR VEN
export const chair_tom_vote = (index)=> {
	//should happen before girl kissing and mile.tom_zan_kiss === -2
	placetime(`classroom`);
	emo("anoy");
	mile.chair_tom_vote = true;
	txt(`During the break, I was eating a croissant and from distance watched @ven chatting with several girls. Just seeing her stupid ugly face was making me sick but I wished I knew what they were talking about it. Without a doubt, she was orchestrating her little coup and tried to convince idiots to vote for her.`);
	txt(`Was there a chance she might be able to convince at least a few of them? If only there was a way to find out...`);
	kat(`Hey, @tom?`);
	tom(`Yes, beautiful?`);
	kat(`Who are you voting for in the class elections?`);
	tom(`You, of course. Why?`);
	txt(`He was taken aback by my question.`);
	kat(`I just wanted to see whether @ven has a chance to get at least a few pity votes, saving her from the total humiliation.`);
	tom(`@ven is running?`);
	kat(`Yeah. How crazy, right?!`);
	tom(`In that case, I'm voting for @ven.`);
	kat(`WHAT?! Are you serious?!`);
	tom(`Yeah!`);
	kat(`Why?!`);
	tom(`There might be a chance to sleep with her.`);
	if(mile.tom_zan < 4 || mile.tom_zan > 6){
		kat(`Aren't you dating @zan? `);
		tom(`Well... yes, you're correct. But you have to keep all your options open. `);
		kat(`And @ven told you she would sleep with you? `);
	}else{
		kat(`She told you she would sleep with you?`);
	}
	tom(`No. On the other hand, you categorically told me you will never ever sleep with me. In a very mean way. Several times.`); //??? spell	
	kat(`Come on! Look at her! She isn't even hot like me!`);
	tom(`I mean, between sleeping with @ven and not sleeping with you, I'll choose to sleep with @ven. Don't blame me, I'm just a man!`);
	kat(`You're not a man, you're a disgusting pig!`);
	tom(`Also, @ven never called me a disgusting pig.`);
	kat(`Yeah?! Go and talk with her, I'll give it five minutes!`);
	tom(`You're always so charming!`);
	txt(`Fuck. That was one vote I lost. I just had to hope the rest of my classmates are not petty assholes.`);
	
	next();
}




//QAA CAMPAIGN MANAGER OF VEN
export const chair_manager = (index)=> {
	switch(index){
		default:
		case 101:
			placetime(`classroom`);
			emo("angry");
			txt(`I notice @qaa hanging something on a board in our class. I initially egoistically assumed it was related to my trials and went closer. It was only indirectly related to me - it was @ven's election poster! It looked very neat which was very suspicious, @ven was Comic Sans-type of girl.`); //board spell
			kat(`Seriously?! You're now helping @ven?`);
			qaa(`So?`);
			kat(`I mean, simping for me was pathetic but on the other hand kinda understandable considering how much above your league I am. But simping for @ven? Do you really have no self-respect?`);
			qaa(`I'm not simping for @ven! I'm the campaign manager!`);
			kat(`The campaign manager!?! What the hell?!? I've never needed a campaign manager!`);
			qaa(`Because nobody dared to oppose you! But times are changing! It's time to overthrow the incompetent and morally corrupt establishment! And elect somebody who'll actually care about our issues!`);
			kat(`Get real! You have no idea how hard it's to convince the principal to do anything! I can guarantee you that @ven won't be able to arrange the half of things I did!`);
			qaa(`There's the only way to find out!`);

			link(`Come on! Now you're going too far! `, 102);
			link(`Do your worst! You have no chance! `, 103);
			break;
			
		case 102:
			mile.chair_manager = 1;
			kat(`Come on! Are you really this petty!? Do you really hate me so much you rather spend time designing the poster for stupid @ven instead of playing video games or jerking or whatever else you're usually doing in your free time? It's only making you look desperate! `); //Apply yourself? spell
			qaa(`Thanks for your advice, @kat, however, what I'm doing in my free time is none of your business!`);
			next();
			break;
			
		case 103:
			mile.chair_manager = 2;
			kat(`You and @ven deserve each other! One is a stupid jerk, the other one is a stupid bitch! But even together you have no chance to convince people to vote for her instead of me!`);
			qaa(`We'll see!`);
			next();
			break;
	}
}




//QBB HATES VEN BUT VOTES FOR HER
export const chair_qbb_vote = (index)=> {
	placetime(`classroom`);
	mile.chair_qbb_vote = true;
	emo("help");
	txt(`I was getting more and more concerned by @ven campaign. She was relentless, talking with people, trying to convince them to vote for her instead of me. And I found the strangest person who fully shared my feelings - @qbb was not impressed by her campaign either:`);
	qbb(`I hated her even before but now she got so much more annoying!`);
	kat(`She's the worst! And imagine if she wins! That would be unbearable!`);
	qbb(`I have to give it to you, when you ran alone you at least weren't bothering people with campaigns! I mean, it wasn't very democratic. But who cares, democracy is just a stupid mob rule.`);
	kat(`Yeah! Democracy is the worst! So I have your vote, right?`);
	qbb(`I can't tell you! The ballots are supposed to be secret!`);
	kat(`But I have your vote, right?!?!`);
	qbb(`Well, I was actually thinking I'll vote for @ven.`);
	kat(`But you hate @ven!`);
	qbb(`Yeah, but I don't vote for @ven to win. I vote for you to lose. Seeing your meltdown when you'll find out you're lost to fucking @ven will be hilarious!`);
	kat(`This is a serious thing!`);
	qbb(`I do understand how important it's for you - you're the kind of girl who peaks at high school and for the rest of her sad, unfulfilling life she nostalgically relishes those memories. It would be so horrible for you if you haven't even peaked but horribly failed instead!`);
	kat(`You're a horrible person! `);
	qbb(`I hope you'll go insane and make a scene and like start choking @ven in the middle of the class or something like that!`);
	next();
};





//GETTING TOMS HELP
export const tom_help = (index)=> {

/*
	tom_help 
	1 - reconsider sex
	2 - date
	3 - blowjob
	4 - sex
	5 - kinky sex
	6 - anal
	7 - for every vote
*/

switch(index){
	default:
	case 101: 
	placetime(`classroom`);
//TODO AND EVA? 
	txt(`@ven and her campaign were making me seriously concerned. Few people actually liked @ven but many were dumb and would like to see a change for change's sake or had stupid petty issues with me. I tried to convince people to vote for me and had to call several favors.`);
	

/*
if(){
	I did not want to recall what I had done with him the last Friday but he certainly owed me something.
	Anyway. You'll vote for me, right? 
	Well, nothing changed since we spoke about this the last time, so haven't changed my mind. 
//TODO!!!
}else{
	txt(`I made him furious when I decided to side with @zan. However, I knew him long enough to know he usually calmed down as quickly as he flared up and he never hold grudges. So there still might be a chance to convince him to vote for me.`);
	kat(`Hey, @tom!`);
	tom(`Hello, @katalt!`);

	if(mile.sub > 8){
		kat(`I'm very sorry I chose @zan! But you know, we're friends for so long and you know how insecure and bitchy she could be! I just thought you'll take losing better than her!`);
		tom(`Yeah, that bitch is crazy! Don't worry, I'm a bit sorry for what I said too, I was drunk and flustrated.`);
	}else{
		kat(`I'm sorry I didn't decide for you but I just tried to be objective.`);
		tom(`I'm sorry that I wasn't able to take it very well.. I was drunk and flustrated. And I know she's a better kisser than me, she's amazing.`);
	}

	kat(`So we're okay?`);

	if(mile.slut > 8){
		tom(`Of course! @zan is unfaithful, quarrelsome heartless skank. But I can't blame a bisexual nympho like you For exploiting the chance. Especially with somebody as beautiful as @zan.`);
	}else{
		tom(`Of course! @zan is an unfaithful, quarrelsome heartless skank. But I can't blame a hot-blooded sexy young girl like you for exploiting the chance for lesbian experimentation.`);
	}
	kat(`Okay...`);
	
	//TODO
	
	tom(`What were you doing when you left?`);
	link(`Nothing!`, 110, ()=> mile.tom_help_zan = 0);
	link(`We're just making out! `, 111, ()=> mile.tom_help_zan = 1);
	link(`I ate her out. `, 111), ()=> mile.tom_help_zan = 2);
}

110
	if(mile.slut > 12){
		mile.slut--;
		//actuall had sex with zan
		txt(`I usually had no troubles to talk about sex but I kinda fell weird sharing with him what I did with @zan.`);
	}
	txt(`We weren't doing anything!`);
	tom(`Come on, @kat! I told you I don't mind! But I expect you to be honest with me!`);
	link(`We're just making out! `, 121);
	link(`I ate her out. `, 121);

111
We were just making out. No big deal. 
qcc(`ome on, @kat! I told you I don't mind! But I expect you to be honest with me!`);


112
if(mile.slut < 8) mile.slut--;


121
Fine! We were making out! And we were cuddling a bit. 
Gropping each other tits and pussies? 
Well... yeah. Exactly. 
I can imagine that! 

It seemed he was imagining it very vividly. 
qaa(`nyway. You'll vote for me, right?`);
Well, nothing changed since we spoke about this the last time, so haven't changed my mind. 
Maybe something changed...
Really? 
*/

if(mile.tom_zan_kiss < 0){

//TODO - it doesn't have to be friday 
	txt(`During the main break, I decided to chat with @tom. I thought he definitely owed me after the last Friday. `);
	
	if(mile.tom_zan_kiss === -2){
		tom(`Hey, @katalt! What happened in the <i>Requiem</i>? I thought we were having fun and then you ran away?`);
		kat(`I suddenly realized I might be drunk but not drunk enough to sleep with you. `);
		tom(`Ouch. That hurts. `);
		kat(`So... anyway, you're going to vote for me, right? `);
		tom(`You're not a very good diplomat. First, you break my heart and then you ask for favors? `);
		kat(`We made out and I let you win over @zan!`);
		tom(`That won't be enough. You have to do a little bit more to convince me! `);
		
	}else{
		tom(`Hey, @katalt! So... should we continue? Would you like to show me other things you're doing better than @zan? `);
		kat(`No! The kiss should be enough for you! Will you vote for me? I let you win so now you're owing me a favor. `);
		tom(`You didn't <i>let me</i> win! I won fairly and I don't owe you anything! I might vote for you but you would have to do something to convince me. `);
	
	}
	
	/*
	I did not want to recall what I had done with him the last Friday but he certainly owed me something.
	Anyway. You'll vote for me, right? 
	Well, nothing changed since we spoke about this the last time, so haven't changed my mind. 
	*/
//TODO!!!
}else{

	txt(`During the main break, I decided to chat with @tom. I made him furious when I decided to side with @zan. However, I knew him long enough to know he usually calmed down as quickly as he flared up and he never hold grudges. So there still might be a chance to convince him to vote for me.`);
	kat(`Hey, @tom!`);
	tom(`Hello, @katalt!`);

	if(mile.sub > 7){
		kat(`I'm very sorry I chose @zan! But you know, we're friends for so long and you know how insecure and bitchy she could be! I just thought you'll take losing better than her!`);
		tom(`Yeah, that bitch is crazy! Don't worry, I'm a bit sorry for what I said too, I was drunk and flustrated.`);
	}else{
		kat(`I'm sorry I didn't decide for you but I just tried to be objective.`);
		tom(`I'm sorry that I wasn't able to take it very well.. I was drunk and flustrated. And I know she's a better kisser than me, she's amazing.`);
	}

	kat(`So we're okay?`);

	if(mile.slut > 8){
		tom(`Of course! @zan is unfaithful, quarrelsome heartless skank. But I can't blame a bisexual nympho like you for exploiting the chance. Especially with somebody as beautiful as @zan.`);
	}else{
		tom(`Of course! @zan is an unfaithful, quarrelsome heartless skank. But I can't blame a hot-blooded sexy young girl like you for exploiting the chance for lesbian experimentation.`);
	}
	kat(`Okay... Anyway, you'll vote for me, right?`)
	
	if(mile.chair_tom_vote){
		tom(`Well, nothing changed since we spoke about this the last time, so haven't changed my mind. `);
		kat(`Maybe something changed... `);
	}else{
		tom(`I think I'm going to vote for @ven. Nothing personal, I don't hold grudges. I just like her program more. Unless you convince me to change my mind? `);
	}
}
	
	
	//	txt(`TODO - REACTION ON PARTY EVENTS`);
		if(ext.slut === 0){
			link(`Maybe I was too harsh. It's possible we might have sex at some point in the future. `, 201);
		}else if(mile.slut < 14){
			link(`I will reconsider my decision about not sleeping with you. `, 221, ()=> mile.tom_help = 1);
			link(`I will agree to go on a date with you. `, 221, ()=> mile.tom_help = 2);
			link(`I will give you a blowjob. `, 222, ()=> mile.tom_help = 3);
			con_link(mile.slut > 6, `low slut`, `I will have sex with you. . `, 222, ()=> mile.tom_help = 4 );
		}else{
			link(`I will reconsider my decision about not sleeping with you. `, 231, ()=>{ mile.tom_help = 1; mile.slut--; } );
			link(`I will suck your cock. `, 231, ()=> mile.tom_help = 3);
			link(`I will fuck you. `, 232);
		}
		break;
		
//LOW SLUT
	case 201: 
		emo("help");
		tom(`Hah! The Ice Queen would agree to have sex with me just to get my vote!?`);
		kat(`Agree to reconsider having sex with you!`);
		if(mile.sub < 1){
			tom(`You're really that desperate?!`);
			kat(`You know what?! Forget about it!`);
			txt(`I was not going to beg him! @tom grabbed my elbow to stop me.`);
			tom(`Sorry, I didn't want to mock you! I'm just surprised.`);
			txt(`He shrugged.`);
		}else if(mile.sub > 12){
			tom(`You're really that desperate?!`);
			kat(`Well... I am.`);
			txt(`I reluctantly admitted, staring at the floor.`);
		}
		tom(`Having that stupid job is really that important for you?`);
		kat(`Not being defeated by fucking @ven is!`);
		tom(`I liked you more as an arrogant bitch, it hurts me to see you this desperate. What if I not only voted for you but helped you to gain a few more votes?`);
		kat(`Well... `);
		tom(`The more you promise, the more I will be motivated to help you and the more votes you'll get! `);
		txt(`Fuck, I desperately needed votes. And promised are cheap...`);

		//link(`That would certainly help your chances. `, 202, ()=> mile.tom_help = 1);
		link(`If you help me win, I promise I'll go on a date with you. `, 202, ()=> mile.tom_help = 2);
		link(`If you help me win, I promise I'll give you a blowjob. `, 203, ()=> mile.tom_help = 3);
		con_link(mile.slut > -5, `low slut`, `If you help me win, I promise I'll sleep with you. `, 203, ()=> mile.tom_help = 4);
		break;
		
	case 202:
		tom(`Then we have a deal!`);
		if(mile.sub > 5){
			kat(`Thank you! You're my savior!`);
			tom(`You're welcome! I'm looking forward sleeping with you! `);
			kat(`But.. but I didn't promise you that! `);
		}else{
			kat(`But I warn you, I didn't promise you sex, only that I'll consider it!`);
		}
		tom(`Sure! But the euphoria after an improbable victory, sense of being obliged to me, seeing me as a man who capable take care of his women... your panties will be off before you even realize you're stripping them!`);
		next();
		break;
		
	case 203:
		mile.slut++;
		if(mile.tom_help === 4) mile.slut++;
		tom(`Then we have a deal!`);
		kat(`Yeah!`);
		txt(`I take his had to confirm our deal. I felt sick and horrified. But I had to win! At all cost!`);
		next();
		break;


//MID SLUT
	case 221:
		if(mile.tom_help === 2){
			tom(`I'm not sure if I'm ready for dating. @zan broke my heart. But maybe a more physical act of love would help me to get over her?`);
		}else{
			tom(`Not like you're known for flirting with guys, making them do anything for you and then not giving them anything else in return, but a more concrete plan might be nice.`);
		}
		kat(`You're out of your mind if you think I'll have sex with you just for your stupid vote!`);
		tom(`Yeah, you're right... although... maybe I can get you more than one vote?`);
		kat(`I'm listening.`);
		tom(`Honestly, your chances to get re-elected are pretty low right now. But I can talk with my friends and convince them to vote for you too. Make sure you will win! How grateful would that make you?`);

		link(`I'm not going to whore myself for votes!`, 223, ()=> mile.tom_help = -1);
		link(`I can give you a blowjob. `, 224, ()=> mile.tom_help = 3);
		link(`If you get me re-elected, I'll have sex with you. `, 224, ()=> mile.tom_help = 4);
		break;

	case 222:
		mile.slut++
		tom(`Really?! Deal! You have my vote! Although...`);
		kat(`What?!? Is that not enough for you?!`);
		tom(`On the contrary. Maybe I can do more for <i>you</i>! What if I could get you more than just my vote?`);
		kat(`I'm listening.`);
		tom(`Honestly, your chances to get re-elected are pretty low right now. But I can talk with my friends and convince them to vote for you too. Make sure you will win! How grateful would that make you?`);

		if(mile.tom_help === 3) link(`In that case, I wouldn't mind having sex with you!` , 224, ()=> mile.tom_help = 4);
		link(`I would do with you anything you ask, no matter how kinky. `,  224, ()=> mile.tom_help = 5);
		link(`If you can ensure my re-election, I will let you fuck my ass! `, 224, ()=> mile.tom_help = 6);
		break;
		
	case 223:
		if(mile.slut > 0) mile.slut--;
		tom(`I hope I didn't offend you! It was just a suggestion!`);
		kat(`Sure.`);
		tom(`You'll reconsider never ever sleeping with me and I'll reconsider voting for you, I'm fine with that deal too.`);
		
		next(`Fine! `);
		link(`Wait, I changed my mind, If you get me re-elected, I'll give you a blowjob. `, 224, ()=> mile.tom_help = 3);
		link(`Wait, I changed my mind, If you get me re-elected, I'll have sex with you. `, 224, ()=> mile.tom_help = 4);
		
		//link(`Wait! Maybe I could give you a blowjob... `, 224, ()=> mile.tom_help = 3);
		//link(`Wait! If you get me re-elected, maybe I could have sex with you... `, 224, ()=> mile.tom_help = 4);
		
		//TODO - CHANGE OF MIND?
		//next();
		break;
		
	case 224:
		if(mile.tom_help > 4){
			mile.slut++; //???
		}
		tom(`In that case, we have a deal! Don't worry, @kat, I won't let you down!`);
		kat(`He agreed keenly and we shook hands.`);
		txt(`Offering sexual services just to win class re-election was not making me happy but I had to win and defeat @ven! At all cost! And @tom seemed very motivated to help me.`);
		next();
		break;
					

//HIGHT SLUT
	case 231: 
		tom(`Come on, @kat! Stop playing and pretending you're hard to get! You're a slut who slept with guys for less!`);
		kat(`I might be a slut but I still won't sleep with you just for one stupid vote!`);
		tom(`What if I could get you more?`);
		kat(`I'm listening.`);
		tom(`Honestly, your chances to get re-elected are pretty low right now. But I can talk with my friends and convince them to vote for you too. Make sure you will win!`);
		if(mile.tom_help === 1) link(`I will suck your cock! `, 233, ()=> mile.tom_help = 3);	
		link(`I would gladly have sex with you in that case!  `, 233, ()=> mile.tom_help = 4);	
		link(`If you can ensure my re-election, I will let you fuck my ass! `, 233, ()=> mile.tom_help = 6);	
		link(`I'll do anything you ask, no matter how kinky!  `, 233, ()=> mile.tom_help = 5);
		link(`I'm not going to whore myself for votes!`, 223, ()=> mile.tom_help = -1);
		break;
		
	case 232:
		mile.slut++;
		tom(`You will fuck me just for my vote?!?`);
		txt(`He instantly got suspicious.`);
		tom(`You're making fun of me!`);
		kat(`I'm not. I'll fuck you if you'll vote for me!`);
		tom(`Fine! I'm voting for you!`);
		kat(`Great!`);
		tom(`What if I could get you more than one vote?`);
		kat(`I'm listening.`);
		tom(`Honestly, your chances to get re-elected are pretty low right now. But I can talk with my friends and convince them to vote for you too. Make sure you will win! How grateful would that make you?`);
		
		link(`I would do with you anything you ask, no matter how kinky. `, 233 , ()=> mile.tom_help = 5);
		link(`I would let you fuck my ass. `, 233 , ()=> mile.tom_help = 6);
		link(`I would make you cum for every vote you can get me!`,  233, ()=> mile.tom_help = 7);
		break;
		
	case 233: 
		if(mile.tom_help === 7) mile.slut++;
		tom(`In that case we have a deal! Don't worry, @kat, I won't let you down!`);
		kat(`He agreed keenly and we shook hands.`);
		txt(`Offering sexual services just to win class re-election was not making me happy but I had to win and defeat @ven! At all cost! And @tom seemed very motivated to help me.`);
		next();
		break;
		
	case 234:
		mile.slut--;
		tom(`I hope I didn't offend you! It was just a suggestion!`);
		kat(`Sure.`);
		tom(`You'll reconsider never ever sleeping with me and I'll reconsider voting for you, I'm fine with that deal too.`);
		
		next(`Fine! `);
		link(`Wait, I changed my mind, If you get me re-elected, I'll have sex with you. `,  233, ()=> mile.tom_help = 4);
		break;
		
	}
}	








//EVA IS RUNNING TOO
export const eva_running = (index)=> {
	const style = (()=>{
		const slut = sluttiness().details;
		const a = [];

		if(!wears.bra){
			a.push(`no bras`);			
		}
				
		if(slut.genitals){
			a.push(`very obviously no panties`);
		}else if(slut.panties){
			a.push(`exposed panties`);
		}else if(wears.microSkirt){
			a.push(`tiny skirts`);
		}else if(wears.shorts){
			a.push(`tight shorts`);
		}

		if(wears.piercedFace){
			a.push(`new piercings`);
		}
		
		if(mile.hair_color){
			a.push(`${mile.hair_crazy ? "ridiculous " : ""}${mile.hair_color} hair`);
		}
		
		if(a.length > 1){
			return and(a);
		}else{
			return `all those slutty oufits`;
		}	
		
	})();


	switch(index){
		default:
		case 101:
			emo("shock");
			placetime(`canteen`);
			txt(`During lunch, I was sitting with @eva and @sas. I was ranting about @ven and their stupid campaign. My friends were outraged too.`);
			sas(`What a bitch!`);
			txt(`@sas was seriously annoyed that @ven did not know her proper place.`);
			eva(`We absolutely cannot allow her to get elected! Imagine if <i>she</i> was the class chairman!`);
			kat(`The whole school will be laughing at us!`);
			sas(`We have to do something!`);
			eva(`Maybe I have an idea how to stop her... but it might be a bit too extreme and surprising.`);
			kat(`We need something extreme! I love your idea already!`);
			eva(`I can run for the class chairman!`);
			sas(`You?!`);
			txt(`Gasped @sasalt. She was as stunned as me.`);
			eva(`It makes a perfect sense. Nobody really likes @ven. If she ran against a more popular candidate...`);
			kat(`Are you saying I'm not popular enough!?!`);
			eva(`No, of course not! But after being a chairwoman for so long, people might want somebody new!`);
			if(mile.sas_exploit > 2){
				sas(`It does make sense...`);
				txt(`@sas slowly nodded, clearly liking the idea. `);
			}else{
				sas(`I'm not sure if that's a good idea, @eva.`);
				txt(`@sas automatically defended me. `);
			}
			kat(`You would only split the vote between us, making sure @ven wins!`);
			eva(`Not if you stepped down.`);
			txt(`!!!`);
			
			link(`Your idea isn't bad but no.`, 103);
			link(`Are you out of your fucking mind!!! `, 102);
			break;
			
		case 102:
			mile.stat--;
			emo("angry");
			mile.eva_running = 2;
			txt(`I shouted at her so loudly that several people in the cafeteria raised their heads and looked in our directions.`);
			eva(`You're out of your fucking mind if you still naively believe you have any chance!`);
			sas(`Could you tone down your voices?`);
			eva(`Everybody is getting tired of you always acting like you're better than everybody else!`);
			txt(`@eva scolded me.`);
			if(mile.eva_exploit > 0){ //TODO!!
				eva(`And you're always such a selfish callous bitch who doesn't care about anybody else than herself!`);
				kat(`I'm not a selfish bitch!`);
				if(mile.sas_exploit > 1){	
					sas(`Sometimes you are, @kat.`);
					txt(`@sas frowned when she coldly supported @eva's accusation.`);
				}
			}
			kat(`Yeah?!? Then why do you think you're a better candidate than me?! Because you're an even bigger, more condescending bitch than I am!`);
			eva(`That's not everything! What about your new style - ${style}! You look like a needy floozie!`);
			kat(`But...`);
			eva(`Yeah, guys might be attracted to trashy whores, but they never respect them! They just fuck them and move on!`);
			if(mile.slut > 10 && mile.sub < 7){
				kat(`Yeah?! I'm only hearing how envious you are! You wish to look half as sexy as me but you're too afraid to step out of your predictable comfort zone!`);
			}else if(mile.slut > 13){
				txt(`She might be right. But being sexy and always the center of attention felt so awesome!`);
			}else if(mile.sub > 12){
				txt(`I stared at the floor, meekly accepting her tirade.`);
			}else{
				kat(`Now you're way out of the line!`);
			}
			if(mile.chair_blowjob === 2){
				eva(`And sucking cocks at the restrooms won't save you!`);
				kat(`Y.. you know about that?!`);
				sas(`Everybody knows.`);
				txt(`Shrugged @sasalt. `);
			}
			if(mile.sas_love > 1 || mile.sas_exploit < 3){ //TODO
sas(`You're not completely wrong, @eva but you're not completely right either. I think you're too harsh on poor @kat!`);
			}
			eva(`I'm sorry, @kat, but you have to realize - only stepping down will save you from the embarrassing defeat. And I'm running whether you want to or not!`);
//TODOy			
			next();
			break;


		case 103:
			mile.eva_running = 1;
			emo("anoy");
			kat(`I think your idea is great. Exactly outside-of-the-box thinking we need. However, between us two it is still me who has the biggest chance to get elected. Not you. No offense.`);
			eva(`Of course, I'm not offended! You're usually right but I believe this is not the case.`);
			kat(`What do you mean?`);
			eva(`Some people might believe the things you say or the things you do might make you seem condescending.`);
			txt(`@eva coldly explained.`);
			if(mile.eva_exploit > 0){ //TODO!!
				eva(`And often it seems you're taking our friendship for granted!`);
				kat(`That's a very unfair accusation!`);
				if(mile.sas_exploit > 1){ //TODO
					sas(`Sometimes you aren't a good friend, @katalt!`);
					txt(`Surprisingly doubled down @sas.`);
				}else{
					sas(`Yeah! That isn't true! `);
				}
			}
			eva(`And while I personally really love your new style - ${style} - somebody less sophisticated might mistake you for a needy floozie.`);
			kat(`But...`);
			eva(`Sure guys might be attracted to trashy bimbos. But they don't respect them, just cum inside and move on! Of course, I'm not saying <i>you</i> are a trashy bimbo. `);
			kat(`Don't worry. I understand what you mean.`);
			txt(`I nicely smiled at her. What a cunt!`);
			if(mile.chair_blowjob === 2){
				eva(`And there are even rumors about you sucking cocks at the restrooms!`);
				kat(`There are rumors about me sucking cosk at the restrooms?`);
				sas(`Yeah, @ven is running a really dirty campaign.`);
			}
			eva(`In the conclusion, I believe you running against @ven would be a too huge risk and I should run instead of you!`);
			kat(`I see your points but I'm still not convinced!`);
			eva(`Please, think about it. I wouldn't want to run without your approval!`);
			txt(`The intention of that bitch was clear - she will run too! And she was an even stronger opponent than @ven!`);
			next();
			break;
	}
}	




//CHAIRMAN BLOWJOB FOR VOTES
export const chair_blowjob = (index)=> {
	switch(index){
		default:
		case 101:
			placetime(`hallway`);
			emo("anoy");
			tom(`Hello, @kat! I have to talk with you! In private!`);
			txt(`I was stopped by @tom.`);
			kat(`Okay?`);
			txt(`I followed him away from other students.`);
			kat(`What is going on?`);
			tom(`I tried to convince my friends. It was fairly easy, they don't really care who will be the class chairman. But there was an unfortunate development.`);
			kat(`What unfortunate development?!?`);
			tom(`I kinda accidentally mentioned to @voj our deal.`);
			{
				const deal = (()=>{
					if(mile.tom_help === 3) return "to suck your dick";
					if(mile.tom_help === 5) return "perverted kinky sex";
					if(mile.tom_help === 6) return "to let you fuck my ass";
					return "to sleep with you";
				})();
				kat(`You were bragging I promised you ${deal}?!? You're a fucking dumbass!`);
			}
			tom(`I wasn't bragging! @voj is my best bro, I would trust him with my life and we don't have any secrets in front of each other!`);
			if(mile.tom_help === 3){
				kat(`What he wants? A blowjob too?`);
				tom(`Well, yeah. But he understands you're not a cheap whore who would blow everybody and is aware of how exceptional this would be.`);
			}else{
				kat(`What he wants? To fuck me too?`);
				tom(`Yes. Initially. But I explained to him you're not a cheap whore and fought for you very hard and he eventually agreed a blowjob will be enough.`);
			}
			kat(`Dammit!`);

			link(`I don't need his vote that much! `, 102);
			link(`Tell him I'll meet him at the restroom the next break.`, 110);
			break;
			
		case 102:
			if(mile.eva_running){
				tom(`Are you sure? ${mile.eva_running ? "Especially now when is @eva running too! " : ""} You don't want to lose by one vote!`);
			}
			kat(`Hmmmm...`);
			
			link(`I won't suck his cock for his stupid vote!`, 103);
			link(`Fine, I'll meet him at the restroom the next break!`, 110);
			break;
			
		case 103:
			emo("angry");
			mile.chair_blowjob = -1;
			if(mile.slut < 8){
				tom(`Fine. Honestly, I think he probably expected this answer.`);
			}else{
				tom(`Fine. But he won't be happy. `);
			}
			kat(`I don't care! It's your job to convince him even without me sucking his dick! Otherwise, I might change my mind about our deal!`);
			tom(`I'll handle it!`);
			txt(`@tom hastily claimed. `);
			tom(`Don't be mad! You have to understand - I had to promise him I at least try to ask you! `);
			kat(`Well, you did and my answer is a strong *no*. 		`);
			next();
			break;

		case 110:
			if(mile.slut < 5) mile.slut++; //always
			log_sex("bj", "voj");
			txt(`Immediately when the next lesson ended, I headed to the restroom to meet with @voj.`);
			mile.chair_blowjob = 1;
			if(mile.slut < 4){
				mile.slut++;
				txt(`I tried to act cold and detached but inside I was screaming. How deep I fell! I was so desperately grasping at straws I was even willing to act like a whore just to get the chance to beat ${mile.eva_running ? "@eva and @ven" : "@ven"}!`);
				txt(`I deserved to be the class chairman! But agreeing to suck a dick of a random jock?! What the hell was wrong with me! Had I no shame anymore?! Was I really willing to go that far!?! Was I losing my grip?!?`); 
				txt(`@voj, already waiting for me, knew nothing about my agonizing internal struggle.`);
			}else if(mile.slut > 13){
				txt(`I was seriously pissed. Not about the dick-sucking, one cock less or more, I did not bother with counting. Guys were horny pigs and I was a big slut, just the natural order of things. No, I was angry at ${mile.eva_running ? "those bitches @eva and @ven" : "that bitch @ven"}. They were making me work so hard for the post I believe I deserved!`);
				txt(`And there was still a huge chance I might lose and this would be completely for nothing!`);
				txt(`@voj, already waiting for me, knowing nothing about my anxiety.`);
			}else{
				txt(`This was not good. I was no prude and I knew how to exploit my flawless look. But sucking a cock for a stupid vote was a new thing. There was no excuse, I was a whore.`);
				txt(`I cursed ${mile.eva_running ? "those bitches @eva and @ven" : "that bitch @ven"}. I deserved to be the class chairman! Whatever it takes!`);
			}

			if(mile.sub < 4){
				voj(`Hey, @kat!`);
				kat(`Shut up and take off your pants!`);
			}else if(mile.sub > 10){
				voj(`Hey, @kat!`);
				kat(`Hello!`);
				txt(`I hesitated, waiting for his order. But he was nervous too.`);
				voj(`So... you'll really suck my dick?`);
				kat(`Yeah... would you mind and take off your pants?`);
			}else{
				voj(`Hey, @kat!`);
				kat(`Hello!`);
				voj(`So... you'll suck my dick?`);
				kat(`Obviously.`);
			}
			txt(`I got on my knees in front of him and took his dick in my mouth and began sucking.`);
			voj(`Ahhh! That feels amazing! Oh yeah, do that thing with your tongue again! You're great!`);
			txt(`He meant it well but I kinda wished him to stay quiet. Each his word was making me even more annoyed.`);
			if(mile.slut < 5){
				txt(`His cum filled my mouth and I went to wash it out.`);
			}else{
				txt(`His cum filled my mouth and I swallowed everything.`);
			}
			voj(`Damn! You were like a vacuum cleaner! You definitely have my vote! Unless @ven will sleep with me.`);
			txt(`@voj was not rushing with putting his pants back on.`);

			if(mile.sub < 7){
				emo("angry");
				mile.chair_blowjob = 1;
				txt(`I grabbed him:`);
				kat(`Even if @ven promises she'll be forever you obedient fucktoy, your vote is mine! Do you understand! And what happened is just between us! If you even imply anything to anybody, you're DEAD! I know where you live! ${mile.slut > 8 ? "And I slept with several very huge and aggressive guys who still owe me a few favors. " : ""} Do you understand?!`);
				voj(`Y... yes! I <i>do</i> understand!`);
				txt(`He was surprised and scared by my sudden outburst. I was sure he will take this to his grave. `); 
			}else{
				emo("help");
				mile.chair_blowjob = 2;
				kat(`No! Your vote is mine! And forget this ever happened! You're not allowed to tell anybody! Or else... else I'll be very angry!`);
				txt(`I desperately threatened.`);
				voj(`Sure, @kat, I won't tell anybody.`);
				txt(`He nodded and dismissed me.`);
				
			}
		next();
		break;
	}
}




//CHAIRMAN BLOWJOB FOR VOTES
export const chair_orgy = (index)=> {
	switch(index){
		default:
		case 101:
			present( //TODO - HARD SET NAMES
				[1, "Oliver", undefined, undefined],
				[2, "Tony", undefined, undefined],
				[3, "Igor", undefined, undefined],
			)
			emo("shock");
		mile.stat--; 
			placetime(`locker room`);
			txt(`I went to the locker rooms for the books I needed for the next class. When I was returning, my way was blocked by four jock from our class, @mik and his three friends.`);
			mik(`Hey, @katalt!`);
			kat(`Hello!`);
			mik(`Do you have a moment?`);
			kat(`I guess I do. How can I help you?`);
			mik(`Well, we wanted to talk with you about the elections.`);
			kat(`You wised up and realized how bad it would be for everybody to let that loser @ven or that bitch @eva be in charge, right?`);
			npc(2, `Something like that.`);
			npc(1, `Well, it depends.`);
			kat(`Depends on what?`);
			mik(`It depends on what you can offer us to vote for you!`);
			npc(1, `Don't play coy, you know what we're talking about!`);
			npc(2, `Is it true you're sucking cocks for votes?`);
			kat(`Who told you that?!? I mean... no!`);
			npc(1, `It doesn't matter how we know.`);
			mik(`Are you?`);
			txt(`I was stunned. How they found out? Did @tom or @voj told them? Fuck them both, assholes who cannot keep their mouths shut.`);

			link(`No! `, 102, ()=> counter.dicks = 2);
			link(`Maybe? `, 102, ()=> counter.dicks = 3);
			link(`...no.`, 102, ()=> counter.dicks = 2);
			con_link(mile.slut > 10 || mile.sub < -5, `low slut or sub` , `I do. `, 102, ()=> counter.dicks = 4);
			con_link(mile.sub < 10,`too submissive`, `Go fuck yourself!`, 102, ()=> counter.dicks = 1);
			break;
			
		case 102:
			if(counter.dicks === 1){
				emo("angry");
				if(mile.sub > 5){
					txt(`I sharply retorted. They started laughing.`);
					mik(`You look cute when you're angry!`);
					txt(`@mik just handwaved my annoyance.`);
				}else{
					txt(`I sharply retorted. They knew me well to be wary of my anger but they still pushed further:`);
				}
			}else if(counter.dicks === 2){
				if(mile.slut > 9) mile.slut--;
				emo("unco"); //TODO
				txt(`I lied. They, of course, did not buy it.`);
				npc(2, `Yeah, no, that's bullshit. We know the truth.`);
				npc(1, `You don't have to be bashful and embarrased to tell us the truth!`);
			}else if(counter.dicks === 3){
				emo("unco"); //TODO
				txt(`They burst out laughing:`);
				mik(`*Maybe*? Is that codeword for *Yes, I do?*`);
				kat(`...maybe?`);
				txt(`I helplessly shrugged. It was pointless to deny it, they knew the truth.`);
			}else{
				emo("imp");
				txt(`I brazenly retorted. They were for a moment surprised by my openness but then began laughing.`);
				mik(`You don't even try to deny it?`);
				npc(1, `You're such a shameless slut!`);
				txt(`I shrugged.`);
				kat(`There's no point denying it. What the fuck do you want?`);
			}
			
			mik(`Come on, @kat! We don't judge you! The opposite! We understand how being the class chairman is important for you and respect you for being willing to work hard to get reelected!`);
			npc(1, `Yeah, we just want to help you.`);
			kat(`How do you want to help me?`);
			mik(`To give you an opportunity to convince us to vote for you.`);

			if(mile.slut > 7){
				kat(`By sucking your dick?!`);
				txt(`I gasped in disbelief.`);
				npc(2, `Well, yeah.`);
			}else{
				kat(`Convince you how?`);
				txt(`I already knew the answer but I still could not believe they were actually suggesting that.`);
				npc(2, `Well, by sucking our dicks.`);
			}

		//TODO - neutral one
			if(mile.slut < 11){
				txt(`My face was red. They were insane! They thought I was a dick-sucking whore! Why the hell I gave the head to @voj? How could I be so stupid and short-sighted? It was a huge mistake and established a very bad precedent.`);
				txt(`I wanted to become the chairwoman so badly but I could not do that. Sucking four dick!  Just thinking about it was making me sick! I was not a whore! But four votes... that was a lot...`);
			}else{
				txt(`I scowled. Normally I did not mind sucking dicks. It was a pretty relaxing activity and I was good at it. But I seriously did not appreciate their entitlement. @kat is a slut so we can easily force her to give us all blowjobs! Maybe giving the head to @voj established a dangerous precedent.`);
				txt(`I did not want them to think I am a whore ready to offer them my body at every opportunity. On the other hand, four votes were four votes and I wanted to become the chairwoman so badly!`);
			}

			kat(`You'll vote for me and I'll then have to suck your dicks?`);
			
			if(mile.slut < 10){
				txt(`I hated myself for even seriously considering this.`);
			}
			
			mik(`No. Sorry, @kat but first the dick-sucking and then the votes.`);
			kat(`How can I trust you?`);
			mik(`You can't. But you can trust us that otherwise, we're going to vote for @eva.`);
			kat(`Did @eva offer you to suck your dicks?`);
			mik(`No, she wouldn't do that.`);
			txt(`Yeah, because @eva was not a desperate slut with no self-respect. Fuck, I hated that bitch so much!`);

			link(`Okay, I'll do it. `, 110, ()=> {
				if(mile.slut < 5) mile.slut++; //low slut wouldn't agree immediatelly
			});
			link(`Sorry but no. `, 104);
			link(`Fuck you! `, 103);
		break;
			
	case 103: 
			mile.chair_orgy  = -2
			emo("angry");
			mile.sub--;
			mile.slut--;
			txt(`Fuck you! Forget about it! If you wanted to have your dick sucked so much, why don't you just suck each other? You are pathetic, perverted bastards and I don't need your stupid votes to get reelected! Bye!`);
			txt(`I shoved them from my way, outraged by their audacity.`);
			next();
		break;
		
	case 104:
			emo("unco");
			txt(`I shrugged, trying to not offend them.`);
			mik(`Why not? You already gave a blowjob to @voj! Do you think we're worse than him?`);
			kat(`No! There were other circumstances in play you don't know about, I would never suck his dick just for his vote!`);

			if(mile.slut > 13){
				txt(`@mik did not take no for an answer. His hand lightly slipped over my shoulder and squeezed my left tit.`);
				mik(`Come on, @katalt! Don't play innocent. Everybody knows sucking cocks is like second nature for you.`);
			}
			
			npc(2, `Four quaranteed votes! That will surely get you the victory.`);
			mik(`Or even make you win in landslide! That would definitely show @eva and @ven they were stupid when they thought they could mess with you!`);
			
			link(`Okay, I'll do it. `, 110);
			link(`Sorry but no. `, 105);
			link(`I said NO! `, 105);
		break;
		
	case 105: 
			mile.chair_orgy = -1
			mile.slut--;
			txt(`I pushed the out of my way and escaped them. How could they even believe I would be willing to do that!`);
			next();
			break;
			
			
			
	case 110:
			txt(`Their excited smiling expressions were just infuriating.`);
			mik(`So, do you want to prepare a schedule or...`);
			txt(`I did not want to waste more time on them than was necessary. And the next break was the long, 20-minute one.`);
			kat(`I can take you together. The next break. If you're not embarrassed.`);
			txt(`I challenged them. They did not expect this but keenly nodded.`);
			mik(`We'll meet during the next break at the bathrooms.`);
			link(`Next break. `, 200);
			break;
	
	case 200:
			if(mile.slut < 0) mile.slut++; //TODO always slutty
			if(mile.slut < 10) mile.slut++; //TODO always slutty
	
			log_sex("bj", "mik");
			log_sex("bj", "npc_1");
			log_sex("bj", "npc_2");
			log_sex("bj", "npc_3");
			
			placetime(`bathroom`);
			txt(`I thought the guys will be so excited about the promised blowjobs they will be already there but I was the first one who arrived at the bathrooms.`);
			
			if(mile.slut < 12){
				txt(`I was alone and it gave me a moment to reflect on how much I fucked up. If only I could go back in time. Maybe it would be possible to convince them to vote for me just with a bit of flirting without promising anything tangible? I managed to do that many times before, was I losing my grip? I despised how they forced me into becoming their whore. `);
			}else{
				txt(`I was alone and it gave me a moment to reflect on how huge slut I became. I mean, I was no innocent girl at the beginning, I always enjoyed having fun with guys. However, ever since the blackmail began, my reputation really went downhill. I despised how casually they approached me and pushed me to become their whore. I might be a nympho but I would still prefer to have control over why or with whom or where I am having sex. `);
			}
			
			if(mile.prostitution > 0){
				txt(`And somehow this was even worse than being an actual whore. I got paid for sucking dicks before and doing it for money certainly felt more dignified than doing it for stupid votes!`);
			}
			
			mik(`Oh, @katalt! You're already there!`);
			npc(1, `Thirsting for our cocks?`);

			if(mile.slut > 14){
				emo(`imp`);
				kat(`Damn yeah, I am!`);
				txt(`I giggled. I assumed the least humiliating will be to pretend that sucking their cocks was not a big deal for me.`);
			}else if(mile.sub < 6){
				emo("anoy");
				kat(`While you took your time!`);
				txt(`I berated them, honestly kinda offended they were not rushing to arrive as fast as they could.`);
			}else{
				emo("focus");
				kat(`I just want to be over with it!`);
				txt(`I shrugged, wanting to make clear I was not doing this because I was enjoying it.`);
			}
						
			if(mile.sub < 6){
				kat(`What are you waiting for? Take off your pants! Who's the first?`);
			}else{
				kat(`So, who would like to be the first?`);
			}
			txt(`The jocks faltered for a while but @mik daringly stepped forward and with a theatrical swing unbuckled his jeans. I smiled and squatted in front of him. He pushed his jeans down and I took his still-soft cock in my fingers.`);
			mik(`Nice.`);
			txt(`I fondled it, my left hand was toying with his balls, and his cock was quickly getting bigger and harder. The final size was pretty impressive, he was not like gigantic but definitely above average. The girls who banged with @mik and in detail described how hung he was were not kidding.`);
			mik(`Shit, @kat, you have nimble fingers!`);
			if(mile.slut > 12){
				npc(2, `Most importantly, she has a lot of experience!`);
			}
			txt(`I ignored his praise and fully focused on my work. I spit on his cock and then took it between my lips.`);
			mik(`Oh yeah! She's great!`);
			txt(`He laughed and was getting his friends hyped.`);
			if(mile.slut < 6){
				mik(`Sorry, @katalt I ever doubted you! I thought you were just a frigid tease who enjoys flirting and dressing but actually really sucks in bed. But no, you're the real deal!`);
			}else if(mile.slut > 13){
				mik(`Her dick-sucking reputation is definitely not exaggerated.`);
			}else{	
				mik(`I'll tell everybody they should vote for you too!`);
			}
			txt(`I could not see the other guys but it seemed they overcame their initial embarrassment too. Somebody grabbed my left arm and gently lifted it, directing it until my hand brushed over something hot, firm and cylindrical. I reflexively wrapped my fingers around the second cock and began stroking. Promptly my right arm was lifted too and I was serving three cocks at the same time.`);
			mik(`Shit, this is crazy!`);
			txt(`@mik blissfully moaned, still amazed that I actually agreed to suck their dicks. I moved my head and pumped my hands roughly at the same tempo.`);
			mik(`Oh yeah!`);
			txt(`@mik began climaxing, filling my mouth with his thick semen. He took his cock out and ordered me:`);
			mik(`Show me!`);
			txt(`He grabbed my chin and made me open my cum-filled mouth.`);
			mik(`Good girl!`);

			txt(`He patted my head and allowed me to swallow and backed away. I leaned to the cock in my right hand and teasingly licked the tip but then like I suddenly changed my mind I instead took in my mouth on the one on my left.`);
			npc(2, `You mischievous bitch!`);
			txt(`Gasped disappointed @npc_2 while @npc_1 was laughing:`);
			npc(1, `Too bad! She likes my dick more!`);
			txt(`His cock was less impressive than @mik's which was on the other hand good, considering he grabbed my head and roughly thrusted, ramming his dick against the back of my mouth.`);
			txt(`I reached for the fourth cock but it was nowhere to be found. @npc_3 was jerking somewhere in a corner, mesmerized by the porn-like scene in front of him so much he did not realise it was his turn until I impatiently snapped with my fingers. Only then he hurried forward and placed his erect dick in my ready palm.`);
			npc(1, `Oh yeah! Suck it, you slut! Oh yeah! I'm coming!`);
			txt(`Suddenly he pulled his cock out of my mouth and finished himself with four energic strokes. I opened my jaws wide but his aim was not the best. Most of the cum ended up on my face, @upper and the floor.`);
			kat(`Fuck! Look what are you doing?`);
			npc(3, `You're such a mess, @katalt!`);
			npc(1, `Maybe you should've anticipated that and taken your top off?`);
			npc(2, `Come on, show us your tits!`);
		showLess(1, -1);
			txt(`I wanted to tell them they were jerks and it was too late but the guys on both sides were already yanking my @upper upwards so I just meekly raised my arms and let them strip me.`);
			if(mile.boobs > 0){
				npc(2, `Your boobs are great! No, seriously, I admire you for being such a madwoman and actually getting them enlarged!`);
			}else{
				npc(2, `Your boobs are so great!`);
			}
			kat(`Well, thanks!`);
			txt(`I barely managed to answer and his dick was already poking against my face. I opened my mouth and began sucking again.`);
			mik(`@katalt is such a slut she's sucking cock at school bathrooms!`);
			txt(`I turned and realized @mik was holding his phone, recording everything!`);

			con_link(mile.sub > 10, `too submissive`, `What the fuck?! Put the fucking phone away you idiot!`, 221, ()=> mile.chair_orgy = 1);
			link(`Put the phone away!`,  222, ()=> mile.chair_orgy = 2);
			con_link(mile.sub < -5, `too dominant`, `W.. what!?! Please, no! Put the phone away!`,  223, ()=> mile.chair_orgy = 3);
			con_link(mile.slut > 5, `low slut`, `Yeah, I love cocks!`,  224, ()=> mile.chair_orgy = 4);
			break;
			
	case 221:
			emo(`angry`);
			mile.sub--;
			txt(`I sceamed at him and spooked @mik quickly lowered his hand.`);
			if(mile.rumor_video){
				mik(`Sorry! I thought you don't mind being recorded...`);
				txt(`@mik pointed out, it seemed I alread had a reputation. `);
				kat(`I didn't allow it! Delete it!`);
			}else{
				mik(`Sorry! I thought you won't mind...`);
				kat(`Are you out of your fucking mind!?! Delete it!`);
			}
			mik(`It was just for me, I wouldn't show it to anybody, I swear!`);
			kat(`Fucking delete it!`);
			npc(3, `Yeah, don't be a dick, @mik, and delete it!`);
			txt(`Hard to say whether @npc_3 was so considerable or just afraid I will walk away and leave his cock unsucked.`);
			link(`Continue. `,  230);
			break;
	
	case 222: 
			emo(`shocked`);
			txt(`I gasped, shocked he was recording me. If the video of me sucking cocks in bathrooms got out, I would be so totally fucked.`);
			mik(`What? Come on, @kat, you look so hot!`);
			kat(`I didn't allow you to record me!`);
			if(mile.rumor_video){
				mik(`I thought you enjoy making slutty videos! `);
				txt(`@mik pointed out, it seemed I alread had a reputation. He seemed almost offended but lowered the hand holding the phone.`);
			}else{
				mik(`You didn't explicitly forbid it.`);
				txt(`He argued but lowered the hand holding the phone.`);
			}
			kat(`And delete everything!`);
			mik(`It would be just for my personal use...`);
			npc(3, `Don't be a dick, @mik, and delete it!`);
			txt(`Hard to say whether @npc_3 was so considerable or just afraid I will walk away and leave his cock unsucked.`);
			link(`Continue. `,  230);
			break;
	
	case 223:
			emo(`help`);
			mile.sub++;
			if(mile.rumor_video){
				txt(`I stuttered and begged him, I did not need another video of me acting like a total whore going around.`);
				mik(`Come on! What's the big deal? I thought you enjoy making slutty videos!`);
			}else{
				txt(`I stuttered and begged him, terrified that someone else might see the record.`);
				mik(`Come on! What's the big deal?`);
			}
			kat(`I... I didn't allow you to record this!`);
			npc(1, `You didn't explicitly forbid it.`);
			npc(2, `Come on, @kat, tell the camera how much you like sucking cocks!`);
			kat(`Fuck you! Put that phone away or I'm quitting.`);
			mik(`You don't have to be such a prude.`);
			npc(3, `Come on, don't be a dick, @mik!`);
			txt(`Hard to say whether @npc_3 was so considerable or just afraid I will walk away and leave his cock unsucked. @mik reluctantly lowered the hand holding the phone.`);
			link(`Continue. `,  230);
			break;
			
	case 224:
			emo(`horny`);
			mile.sub++;
			mile.slut++;
			txt(`I turned my cum-covered face to the phone and smiled. It was so degrading and humiliating to be reduced to a whore sucking dick for stupid votes. It was so twisted but I revelled in those feelings, they made me excited and very aroused.`);
			if(mile.rumor_video){
				txt(`What was the point stopping him? Even if he shared it with his friends, it would be just another video showing how big slut @katalt became. I let him record everything and returned to me cock-sucking duties.`);
			}else{
				txt(`If he shared the video with other people, I would be totally fucked, my reputation would be irreversibly destroyed. The lack of control and being completely at his mercy were making me so wet. I let him record everything and returned to me cock-sucking duties.`);
			}
			
			link(`Continue. `,  230);
			break;
			
	case 230: 
			emo(`focus`);
			txt(`There were two dicks left. And I assumed @npc_2 was getting close.`);
			npc(2, `@kat, would you mind if I cum all over your tits?`);
			if(mile.sub > 15){
				kat(`Please, use me any way you enjoy the most!`);
			}else if(mile.slut > 9){
				kat(`I would love that.`);
			}else if(mile.sub < 0){			
				kat(`Whatever.`);
			}else{
				kat(`Sure, why not.`);
			}
			
			txt(`I finished him with rapid stroking and leaned backwards to let him explode all over my chest, covering it with his thick cum.`);
			npc(3, `Oh yeah, fucking finally!`);
			txt(`Impatient @npc_3 impolitely instantly shoved his dick into my face. I did not comment on that and immediately began sucking, the break did not last very long and there was no time to waste.`);
			npc(3, `Oh yeah!`);
			txt(`I was quick but not sloppy and he clearly enjoyed it very much. He began ejaculating when the school bell began ringing. I swallowed his cum, he readjusted his pants and ran to a class. Without a word, no *bye* or *thank you* nor *whoa, @katalt, that was an amazing blowjob*! What a jackass.`);
			txt(`I stood up and went to wash my mouth and clean up my face and chest. Then I went to search for my top. It was on the floor, they threw it on the radiator but it fell down and they either did not notice or did not care.`);

			link(`Rush to the class. `, 240);
			link(`Clean the @upper.  `, 241);

			/*
			Fuck, even after a quick cleaning there remained a wet, hard-to-explain stain.
			Rush to the class anyway. 
			Put on gym 
			
	I began sucking and the ring began ringing. The rest of the jock left the bathroom, rushing to the class. 
			*/
			link(`Rush to the class anyway. `, 240);
			break;
	
	case 240:
			mile.slut++;
			mile.stat--;
			txt(`I checked the time. FUCK! I hastily put the top on and sprinted to the class. `);
			txt(`I was lucky, @maj was not yet there. Relieved I will not be scolded, I sat down. `);
			txt(`I could hear chuckling, I turned to smirking @mik and he pointed at his T-shirt. At first I did not realized what he meant, then looked down at my @upper and noticed a stain. `);
			txt(`FUCK!`);
			txt(`I wiped the cum and vainly hoped nobody else noticed. `);
			next();
			break;
			
	case 241:
			mile.classes_late++;
			txt(`I vainly tried to clean the cum out of my @upper. Despite all my effort, a wet, hard-to-explain stain remained. `);
			
			showAll();
			if(mile.sub > 10){
				emo(`help`);
			}else if(mile.sub > -5){
				emo(`anoy`);
			}else{
				emo(`angry`);
			}
			txt(`I hurried to the class but @maj was already there. And she did not let me sit down quietly, she had to draw attention to me:`);
			if(mile.classes_late >= 2){
				maj(`@KATALT! You are late again! `);
				txt(`She scolded me.  `);
			}else if(mile.classes_late){
				maj(`@katalt is again late?`);
				txt(`She frowned.  `);
			}else{
				maj(`You're late, @katalt!`);
			}
			kat(`I'm sorry!`);
			txt(`I feebly shrugged and sat down.`);
			
			/*
				TODO - people noticed, rumors
			*/
			next();
			break;
	
	}		
}


 
//VENDY BEING BITCH - TODO 
export const chair_elections_vendy = (index)=> {
	switch(index){
		default:
		case 101:
			placetime(`classroom`);
			mile.stat--;
			emo(`shock`);
			mile.chair_vendy_bitch = true;
			
			
	//TODO - CLUMSY THIS WAY 
	add_workday(`awakening`, `chair_elections_morning`);
	add_workday(`classes2`, `chair_elections`);
	if(mile.tom_help > 0) add_workday(`classes3`, `chair_elections_tom`);
	mile.skip_random = true;

		ven(`@katalt?`);
		txt(`That bitch @ven approached me.`);
		ven(`Hello! Do you have a moment?`);
		kat(`No.`);
			
		//nonsexual route	
		if(mile.chair_orgy < 0 || mile.chair_blowjob < 0 || mile.tom_help < 3){
			ven(`Come on, @kat, you don't have to be so affronted!  `);
			kat(`What do you want? `);
			ven(`I just wanted to wish you good luck . You know, we might be running against each other but that doesn't mean we have to be enemies!`);
			kat(`Or does it? `);
			ven(`Either way, enjoy you last day as the class chairwoman! And please, convey to your best friend @eva my thanks for stabbing you in the back and splitting the vote! I'm very fortunate you're both petty, selfish and stubborn bitches! `);
			kat(`Fuck you! `);
			txt(`I was too furious to come with anything better.  `);
			ven(`Fuck you too, @katalt! I'm really looking forward ${time.day === "fri" ? "Monday" : "tomorrow"}!`);
			txt(`Gloated @ven and left right when I was seriously considering I'm going to punch her ugly face.  `);
			next();
			
		//sexual route
		}else{
			ven(`Excellent! @kat, this is stupid! Our whore rivalry doesn't make sense!`);
			kat(`We are not rivals! I don't care about you!`);
			ven(`I'm sorry! I had no idea me running will make you so irrationally angry and ruin our friendship.`);
			kat(`I wouldn't say we were friends. And I presume you're not sorry enough to quit.`);
			ven(`Well, two weeks ago, if you weren't such a bitch and asked me nicely, I would! But unfortunately at this point, I already spend too much time to quit. Especially when it seems I have enough votes to beat you. I should be grateful to @eva for splitting your votes.`);
			kat(`Good for you!`);
			txt(`I smirked, unfazed by her bluffing. She tried to psych me out but I was well aware of what she was doing. @ven frowned and I started to believe this conversation will end with her defeat and humiliation. But then she suddenly grinned:`);
			ven(`Oh... I guess you may have more votes than I assumed... it seems your new strategy and contact campaign is working better than I thought.`);
			kat(`New contact campaign?`);
			ven(`You know, the sexual favours for votes. Sucking guy's cocks in bathrooms. Or are you doing girls too? Eating them out?`);
			txt(`I blushed, for a moment stunned. How did she find out?`);
			kat(`What the fuck are you talking about?`);
			ven(`You know well what I'm talking about!`);
			kat(`I do not!`);
			if(mile.slut < 7){
				ven(`Honestly, I was pretty shocked when I heard about it. You used to be a good girl and there were rumours you only rarely let anybody into your panties. And now you're selling yourself for votes like the cheapest whore! You have to be really desperate!`);
			}else{
				ven(`Honestly, it wasn't a big surprise. You always were a horny slut who let anybody into your panties. Becoming a cheap whore who sells herself for votes was just a natural progression. Still, I do have to wonder - do you actually want so desperately to get reelected? Or are you just using this as an excuse because you enjoy sucking cocks so much?`);
			}
			kat(`FUCK YOU!`);
			txt(`I was so furious with rage I was not able to come up with any clever comeback.`);
			
			
			if( checkQuality().value < 0 ){ //TODO - maybe 3 would be better
				txt(`@ven looked down at my @upper and @shoes.`);
				ven(`Well, bribing them with money obviously wasn't an option. Sorry, I don't want to make fun of the finantial situation of your family, but I thought even at goodwill you can find better fitting clothes than those you're wearing!`);
			}
			txt(`@ven impishly smiled:`);
			ven(`Well, that's everything I wanted to say. Good luck ${time.day === "fri" ? "on Monday" : "tomorrow"}!`);

//TODO - ANSWER? 
			next();
		}

			break;
	}
}	



//ELECTIONS MORNING
export const chair_elections_morning = (index)=> {
	switch(index){
		default:
		case 101:
			emo(`sad`);
			placetime(`my bed`);
			
			txt(`I woke up and checked the alarm clock. It was barely 5 am but I felt so uneasy I was not able to fall asleep again. Today was the day of the class elections and for the first time, I feared the result. I used to win every time, usually without opposition but now there were three candidates - me, @ven and @eva.`);
			txt(`I tried my best to convince my classmates to vote for me but way too many were delighted to vote against me just out of spite. And those damned nerds did their best to ruin my reputation, because of their stupid task there were so many rumours about me and drugs and sugar daddies and escorting and other even dumber things.`);
			if(mile.tom_help === -1){
				txt(`Now I realized how fucking stupid I was when I refused @tom's help. If only I could go back in time, I would promise him anything he wanted.`);
			}else if(mile.tom_help > 0){
				txt(`I hoped at least @tom will deliver what he promised - and depending on @tom showed how fucked I was. ${mile.chair_blowjob > 0 ? "I even agreed to suck his *bro's* cock!" : ""}`);
				if(mile.chair_orgy < 0){
					txt(`Maybe refusing @mik and the other guys was a grave mistake. Why the hell was I such a stupid prude? How hard it would be to suck four cocks? I could get four extra votes! ${mile.chair_orgy === -2 ? "Or at least I could try to be nicer to them, maybe imply I migt do it in future? However sick it made me? " : ""}`);
				}else if(mile.chair_orgy > 0){
					txt(`I was so fucking desperate that I even agreed with @mik and the other jocks suggested I should suck their cock if I want their votes. ${mile.chair_orgy <= 2 ? "It was so fucking humiliating and degrading and I could not be even sure they will really vote for me. " : "It was so fucking humiliating and degrading and they even took a video of me so everybody could see how huge, pathetic slut I was. "}`);
				}
			}
			
			if(mile.chair_b_help < 0){
				txt(`And why I refused @qbb's idea? I was too nice. @eva deserved that! `);
			}
			
			txt(`I felt like I am definitely going to lose. Maybe.... maybe I should just give up? Maybe @eva was right? The least embarrassing option would be to just quit and not face a public defeat?`);
			
			link(`I never give up! `, 103);
			link(`I give up. `, 102);
			break;
			
		case 102:
			emo(`happy`);
			mile.chair_elections_quit = true;
			mile.sub++;
			mile.sub++;
			
			txt(`I was fucked. My life was ruined. And the sooner I accept that, the better. Desperately clinging to vain hope and not admitting I was no longer the most popular girl would only lead to even more embarrassment.`);
			txt(`Making the decision was harsh but as soon as I made it, I immediately felt better. I no longer had to worry about the elections or popularity. Why care what other say about me?`);
			txt(`I was lying in my bed, reveling in self-pity until my alarm clock began playing and I had to wake up.`);
			chapter(`Morning. `, `awakening`);
			break;
		
		case 103:
			emo(`focus`);
			mile.chair_elections_quit = false;
			mile.sub--;
			mile.sub--;
			//TODO game reference 
			txt(`No! I'm not going to give up! I was a fighter! I still had a pretty good chance. There were not many people who actually liked @ven or @eva, they were seriously unlikeabe, and I might be still able to get more votes than them both!  ${ mile.chair_orgy > 0 ? "And I would not like to suck all those cocks for nothing! " : ""}`);
			txt(`And even if I will lose then what?! There was no dishonor in losing when I did my best! I will just smile and gracefully congratulate the winner, showing everybody I can handle triumph and disaster the same.`);
			txt(`I made my decision and immediately felt better. I was awesome. I left the warm bed and went to take shower and get ready for the day.`);
			chapter(`Morning. `, `awakening`);
		break;
	}
}


//ELECTIONS MAIN
export const chair_elections = (index)=> {
	function resolve(result, vote){
		if(vote > 0){
			mile.chair_vote = 2;
		}else if(vote < 0){
			mile.chair_vote = 3;
		}
		if(result > 0){
			mile.chair_elected = 2;
		}else{
			mile.chair_elected = 3;
		}
		
		txt(`@maj counted the votes:`);
		
		if(result === 0){
			if(vote > 0){
				mile.chair_elected = 2;
				maj(`15 votes for @eva, 14 for @venalt. The new class chairman is @eva @evasur. `);
				txt(`That was close! @eva won by a single vote! If I voted against her...`);
			}else if(vote < 0){
				mile.chair_elected = 3;
				maj(`14 votes for @eva, 15 for @venalt. The new class chairman is @venalt @vensur. `);
				txt(`I could not believe it! @ven actually won! And my vote decided! `);
			}
		}else if(vote === 1){
			maj(`16 votes for @eva, 13 for @venalt. The new class chairman is @eva @evasur. `);
			txt(`@eva still won but it was closer than I - and probably her too - anticipated. `);
		}else if(vote >= 2){
			maj(`17 votes for @eva, 12 for @venalt. The new class chairman is @eva @evasur. `);
			txt(`That was a pretty decisive victory. `);
		}else if(vote >= 4){
			maj(`19 votes for @eva, 10 for @venalt. The new class chairman is @eva @evasur. `);
			txt(`Damn, that was landslide. @ven got utterly humiliated. `);
		}else if(vote === -1){
			maj(`13 votes for @eva, 16 for @venalt. The new class chairman is @venalt @vensur. `);
			txt(`I could not believe it! It was close but @ven actually managed to win! `);
		}else if(vote <= -2){
			maj(`11 votes for @eva, 18 for @venalt. The new class chairman is @venalt @vensur. `);
			txt(`I was shocked. That was hysterical! @ven defeated @eva in lanslide! `);
		}
	}
			
			
	switch(index){
	//100 MEETING EVA
		default:
		case 101:
			//quit
			if(mile.chair_elections_quit){
				placetime(`schoolyard`);
				emo(`sad`);
				txt(`I felt dejected but serene. I just had to survive the day, preferably without drawing too much attention to myself.`);
				eva(`Hello!`);
				
				if(mile.chair_b_help > 0){
					txt(`@eva seemed unusually nervous. She was clearly was not sure about her victory.`);
				}else{
					txt(`Greeted me chirpy @eva, unlike me she was looking forward the elections.`);
				}
				
				kat(`Hey.`);
				sas(`What is going on, @kat? Are you fine?`);
				txt(`@sas was concerned about me.`);
				eva(`Yeah? What's going on?`);
				kat(`Well, girls, I was thinking. And you were right, @eva. I should not run.`);
				txt(`@eva's eyes gleamed. For a fleeting moment was her face filled with utter triumphant joy. I was outraged and hated her so much. How could my fall make her so happy? But she quickly regain control and consoled me:`);
				eva(`Come on, @kat! You were the class chairman for three years! Nobody can beat that! You did an amazing job and you deserve a break!`);
				sas(`Yeah! Don't be sad!`);
				eva(`The most important thing is that bitch @ven won't win, isn't it?`);
				kat(`Yeah, it is.`);
				txt(`I helplessly shrugged. I might have a chance if I ran only against @ven and @eva supported me. But she was too selfish for that.`);
				
				link(`Continue. `, 250);
				
		//TODO INTRUQUEST AGAINST EVA SAS POPULAR
			}else{	
				placetime(`halwayy`);
				emo(`anoy`);
				//mile.chair_elections = -1;
				txt(`I went to the @maj's office to confirm I was running for the class chairman but before I got there @eva caught up with me.`);
				eva(`Hey @kat!`);
				if(mile.chair_b_help > 0){
					txt(`@eva seemed unusually nervous. She was clearly was not sure about her victory.`);
				}else{
					txt(`Greeted me chirpy @eva, unlike me she was looking forward the elections.`);
				}
				kat(`Hello!`);
				eva(`So... how did you decide? You know that vote split between us will only get @ven elected, right?`);
				kat(`Yeah. Maybe it would be better if you quit.`);
				eva(`Come on! That's not fair! You already were the chairman several times!`);
				kat(`A month ago you didn't even want to do it!`);
				eva(`A month ago it seemed you were going to beat @ven easily! Come on, @kat, I don't want to push you but you know it's a smart thing to do.`);

				link(`May the best girl win!`, 103, ()=> mile.chair_eva_promise = 1);
				link(`You know what? Fuck you, @eva! `, 102, ()=> mile.chair_eva_promise = 2);
				link(`Blindside her. `, 104, ()=> mile.chair_eva_promise = 3);
			}
			break;
			
		case 102:
			emo(`angry`);
			mile.eva_exploit++; //TODO
			kat(`You don't give fuck about being a chairman! You're doing this only to spite me! Why are you never able to come up with anything original? You're only following me, only trying to do what I already did, desperately trying to be as good as me!`);
			eva(`You know what? Fuck you, @kat! I thought we were friends but you're just a condescending bitch, constantly pretending you're better than anybody else and regardless of what I'm doing, you're never supportive!`);
			kat(`I'm supportive! But you're not seriously expecting I'll support you stealing my job?!?`);
			eva(`It isn't your job, you're not entitled to be the class chairman, you entitled goose!`);
			kat(`Yeah?!? You'll see when I win! Bye!`);
			link(`Continue. `, 200);
			break;
			
		case 103:
			emo(`imp`);
			mile.eva_nice++;
			kat(`Come on! You don't actually want me to quit, do you? What achievement it would be to beat @ven?`);
			eva(`Heh, that's a good point. Well, fine, good luck.`);
			link(`Continue. `, 200);
			break;
			
		case 104:
			emo(`smug`);
			mile.eva_exploit++; //TODO
			kat(`...fine. You might be right. I should quit.`);
			eva(`Sorry! I don't want to force you or make you mad. I just believe it would be best for everybody.`);
			txt(`Most importantly the best four you, you bitch! I lied to her I am going to quit. She will think her victory is secure and I will have a chance to chat with several people and secretly try to convince them without her having a chance to do anything against it.`);
			link(`Continue. `, 200);
			break;

	//200 MAJ OFFICE REJECTED
		case 200:
			placetime(`@maj's office`);
			emo(`shock`);
			{
				let malus = 0;
				counter.mal = ["slut", "grades", "maj", "late"];
				counter.mal.forEach( key => counter[key] = 0 );
				
				if(mile.maj_exploit > 2){
					malus++;
					malus++;
					counter.maj = 2;
					bux("++2 not helping @maj at all");
				}else if(mile.maj_exploit > 1){
					malus++;
					counter.maj = 1;
					bux("++1 not helping @maj more");
				}
				
				if(mile.classes_late > 2){
					malus++;
					counter.late = 1;
					bux("++1 getting to classes late");
				}
				
				if(mile.grades >= 10){
					malus++;
					malus++;
					counter.grades = 2;
					bux("++2 failing grades");
				}else if(mile.grades >= 6){
					malus++;
					counter.grades = 1;
					bux("++1 bad grades");
				}
				
				
				if(mile.mol_perv > 1){
					malus--;
					counter.mol = 1;
					bux("--1 @mol praised her");
				}
				
				const slut = sluttiness().slut; 
				//TODO tease? 
							
				if(slut >= 5){
					malus++;
					malus++;
					malus++;
					counter.slut = 3;
					bux("++3 dressed like cumdump");
				}else if(slut === 4){
					malus++;
					malus++;
					counter.slut = 2;
					bux("++2 dressed like whore");
				}else if(slut === 3){
					malus++;
					counter.slut = 1;
					bux("++1 dressed like slut");
				}
				
				counter.mal.sort( (a,b) => counter[a] - counter[b] );
				counter.mal_max = counter.mal[0];
console.error(counter.mal);
				
				bux("===");
				
				if(malus <= 1){
					mile.chair_registration = 2;
					bux(`${malus} <= 1  - allowed `);
				}else if(malus >= 4){ //TODO - LOWER TO 3 !!!
					mile.chair_registration = -2;
					bux(`${malus} >= 4  - not allowed `);
				}else{
					mile.chair_registration = 0;
					bux(`1 < ${malus} < 4 - barely allowed `);
				}
				
				if(counter.cheat === 2){
					mile.chair_registration = 2;
					bux(`cheat changed to allowed `);
				}else if(counter.cheat === 1){
					mile.chair_registration = 0;
					bux(`cheat changed to barely allowed `);
				}else if(counter.cheat === -2){
					mile.chair_registration = -2;
					bux(`cheat: changed to not allowed `);
				}
				counter.cheat = 0;
				bux("---");
				
				
				
				let why = ``;
				why += `You dress up like a strumpet! `;
				
				if(counter.slut < 2){
					why += `(Well, today you're wearing normal clothes it's an exception!)`;
				}else{
					if(wears.microSkirt) why += `You have no shorter skirt!?! `;
					if(!PC.bra) why += `Why the heck are you not wearing a bra?! `;
					if(mile.hair_crazy) why += `And what's the point of ${mile.hair_color} hair? `;
					if(wears.heavyMakeup) why.push(`And that whorish makeup! `);
					if(wears.piercedFace) why.push(`And those piercings!`);
	//TODO - MORE!!
				}
					
				if(counter.grades == 2){
					why += `You're failing your classes! `;
				}else if(counter.grades){
					why += `Several teachers already complained about your bad grades. `;
				}
				if(counter.late) why += `You're constantly late for classes. `;
				
				if(counter.maj) why += `And when I ask you to help me in the library or with other things, you only make excuses! `;
			
			
				kat(`Good morning, @maj!`);
				maj(`Good morning, @katalt? How can I help you?`);
				kat(`Well, I wanted to register for the class elections.`);
				
				if(mile.chair_registration === 2){
					maj(`You don't even have to get registered. `);
					link(`What?! Why?! Did I do something wrong? `, 210);
					
				}else if(mile.chair_registration === 0){
					maj(`I'm not sure if I should even allowy you to run the the class chairwoman this time. `);
					kat(`WHAT!? Are you serious?!? Why?!? `);
					maj(`You are even asking? ${why}`);
					
					link(`You can't do that!`, 201, ()=> mile.chair_registration = -1);
					link(`Please, don't do that!`, 205, ()=> mile.chair_registration = 1);
					
				}else if(mile.chair_registration === -2){
					maj(`Ohhhh... right. I'm sorry, @katalt, but I don't believe you should run this time.`);
					kat(`WHAT!?`);
					maj(`You know, the chairman represents the class on the school council. She has to be somebody representative who can serve as a good example for the rest of the students.`);
					kat(`But I...`);
					maj(`You?!? ${why}`);
				
					link(`You can't do that!`, 201);
					link(`Please, don't do that!`, 202);
				}
				
			}			
			if(window.hardbug){
				link(`## Force enabling of registration. `, 200, ()=> counter.cheat = 2);
				link(`## Force barely enabling of registration. `, 200, ()=> counter.cheat = 1);
				link(`## Force disabnabling of registration. `, 200, ()=> counter.cheat = -2);
			}
			break;
				
			
			
		//ANGRY 
		case 201:
			emo(`angry`);
			mile.sub--;
			
			maj(`I can and I will!`);
			kat(`People should have the right to vote for me! I thought this was a democracy!`);
			maj(`Then you were wrong! Both @venalt or @eva would be good class chairmen. And you better should instead seriously self-reflect about your behavior!`);
			kat(`But...`);
			maj(`We are not going to discuss it further, I made my decision! Now get out of my office!`);
			link(`Leave. `, 220);
			break;
		
		//BEGGING - NOT ALLOWED
		case 202:
			emo(`help`);
			mile.sub++;
			{
				const why = [];
				why.push(`dress more properly`);
				if(counter.grades) why.push(`fix my grades`);
				if(counter.late) why.push(`be more punctual`);
				if(counter.maj) why.push(`help you with everything`);
			
				kat(`Please, let me run! I swear I will ${and(why)}! Please!`);
				maj(`You should've thought about that sooner! Unlike yours, the behavior of @venalt and @eva is appropriate, either of them would be a good class chairman. But still, you better should self-reflect and correct your behavior. Otherwise, you might get into serious trouble with me!`);
				kat(`But...`);
				maj(`We are not going to discuss it further, I made my decision! You can go.`);
			}
			link(`Leave. `, 220);
			break;
			
		//BEGGING - ALLOWED
		case 205:
			emo(`help`);
			mile.sub++;
			{
				const why = [];
				why.push(`dress more properly`);
				if(counter.grades) why.push(`fix my grades`);
				if(counter.late) why.push(`be more punctual`);
				if(counter.maj) why.push(`help you with everything`);
				why.push(`try to be better`);
			
				kat(`Please, let me run! I swear I will ${and(why)}! Please!`);
				maj(`I don't know. I see how much you care about getting reelected. But why you don't behave that way? There are no issues with @venalt and @eve.`);
				kat(`Please, @maj! Please!`);
				
				why.length = 0;
				if(mile.grades <= 2){
					why.push(`your grades are excellent`);
				}else if(mile.grades <= 4){
					why.push(`your grades are good`);
				}
				
				if(counter.maj === 0){
					why.push(`you are helping me when I do need help`);
				}else{
					why.push(`you are sometimes helping me`);
				}
				
				if(counter.mol){
					why.push(`@mol praised you`);
				}
				
				maj(`Hmm... on the other hand, ${why}.`);
				if(counter.mol){
					txt(`At least getting molested by that old pervert was not for nothing.`);
				}	
				kat(`Please!`);
				txt(`@maj sternly stared at me for a while. But finally, she nodded: `);
				maj(`If you swear you'll behave better, I'll let you run. `);
				
			}
			link(`I swear! `, 600);
			break;
			
		//ALLOWED TO RUN
		case 210: 
			maj(`No, I just automatically assumed you'll want to run to get reelected again. `);
			kat(`Oh! `);
			txt(`For a moment I was irrationally terrified @maj would not allow me to run. `);
			maj(`Although...`);
			txt(`Oh no...`);
			if(counter.mal_max === "maj"){
				maj(`I would very much appreciate it if you were more helpful in the future. `);
				kat(`I would love to help you in the library more often but I'm too busy with studying and working and...`);
				maj(`I understand. But still, for the class chairwoman, I have higher standards. `);
				kat(`Yes, @maj.`);
			}else if(counter.mal_max === "grades"){
				maj(`You should do something with your grades. You want to go to a good university, don't you? `);
				kat(`Yes, @maj. I'll try to study harder. `);
			}else{
				maj(`Your recent behaviour is far from perfect. `);
				kat(`I'm sorry, @maj. I'll try to be better! `);
			}
			
			if( sluttiness().slut >= 2 ){
				const why = [];
				why.push(`Provocative clothes`); //TODO - MORE DETALS? 
				//TODO MIDRIFT
				if(wears.miniSkirt) why.push(`too short skirts`);
				if(!wears.bra) why.push(`no bra`);
				if(wears.heels) why.push(`improperly high heels`);
				if(wears.makeup) why.push(`makeup`);
				if(mile.hair_crazy) why.push(`${mile.hair_color} hair`);
				if(wears.piercedFace) why.push(`piercings`);
				
				maj(`Also you should reconsider the way you present yourself. ${why.join(", ")}? I should send you home to change! `);
			}
						
			link(`Elections. `, 600);
			break;
			
			
			
			
		case 220:
			emo(`cry`);
			txt(`I left @maj's office utterly flabbergasted. I could not believe what just happened! Not only I am not going to be reelected, I was denied even the chance to try! I could not imagine a worse outcome. I had no idea what to do next. With my head down, I slowly walked back to the class.`);
			sas(`@kat? Are you okay? You look weird?`);
			kat(`I'm fine!`);
			txt(`I snapped at annoying @sas.`);
			sas(`Are you nervous because of the elections? I'm voting for you, not for @eva!`);
			kat(`I'm not going to run.`);
			sas(`WHAT?!? Why?!?`);
			kat(`I'm just not going to! Why can't you go to bother somebody else?!`);
			link(`Elections. `, 500);
			break;
	
	//250 MAJ QUIT
		case 250:
			placetime(`hallway`);
			emo(`help`);
			{
				const why = [];
				why.push(`Revealing clothes`); //TODO - MORE DETALS? 
				//TODO MIDRIFT
				if(wears.microSkirt) why.push(`obscenely short skirt`);
				if(wears.shorts) why.push(`obscenely tight shorts`);
				if(wears.highHeels) why.push(`improperly high heels`);
				if(mile.hair_crazy) why.push(`${mile.hair_color} hair`);
				if(wears.heavyMakeup) why.push(`whorish makeup`);
				if(wears.piercedFace) why.push(`my piercings`);
		
				txt(`When I returned from the bathrooms ran into our teacher @maj.`);
				maj(`@katalt? Did you forget or decided not to run this time?`);
				kat(`I decided not to run.`);
				maj(`Maybe that's better... the class chairman should be able to represent the class. While you...`);
				txt(`Frowning @maj waved in my direction. Embarrassed I lowered my eyes. I knew what she was talking about. ${or(why)}. I was looking like a floozie.`);
				if(mile.grades >= 6){
					maj(`And it might be better for you to have more free time for studying. Several teachers already complained about your grades!`);
					txt(`@maj continued scolding me.`);
				}
				kat(`Yes, @maj.`);
				txt(`I nodded with resignation.`);
		//TODO - more filler, meeting with ven? blackmail?
				link(`Elections. `, 550);
			}
			break;
		
				
	//500 ELECTION - DENIED
		case 500:
			placetime(`classroom`);
			mile.stat--;
			emo(`help`);
			txt(`The next class @maj checked the attendance and then without further due started:`);
			maj(`Today, before the lesson, we will elect the class chairman. She'll be in charge of the organization of the class affairs and will be representing the class on the school council. This year we have two candidates - @venalt and @eva. They both get three minutes for their speech (but they don't have to use the whole three minutes) and then you'll vote. You know the drill.`);
			txt(`Surprised @tom raised his hand:`);
			tom(`@katalt isn't running this time?`);
			maj(`No, I decided she should give a chance to somebody else.`);
			txt(`She answered sternly. She could at least say *she decided* to give chance to somebody else. But she intentionally humiliated me further. I stared at my desk, I would not be able to handle @ven's and @eva's smug stares. I did not even listen to their speeches. Then we voted, I got an empty piece of paper and had to write a name.`);

			link(`Eva. `, 501, ()=> mile.chair_vote = 2);
			link(`@venalt. `, 501, ()=> mile.chair_vote = 3);
			link(`Leave blank. `, 501, ()=> mile.chair_vote = 1);
			break;
			
		case 501:
			mile.chair_elected = 2;
			txt(`@maj counted the votes:`);
			{
									
				let vvv = 9;
				let eee = 14;
				
				if(mile.chair_manager){
					vvv += 2;
				}else{
					eee += 2;
				}
				
				if(mile.chair_b_help > 0){
					vvv += 3;
				}else{
					eee += 3;
				}
				if(mile.chair_vote === 2) eee++
				if(mile.chair_vote === 3) vvv++
				
				if(mile.chair_vote === 1 && vvv === eee){ //TEMPFIX
					eee = 15;
					vvv = 13;
				}else if(vvv >= eee){
					eee = 15;
					vvv = 14;
				}
				
		//TODO - VENDY VICTORY
				
				if(mile.chair_vote === 1){
					mile.stat--;
					maj(`${eee} votes for @eva, ${vvv} for @venalt$, 1 blank - I guess somebody was sulking. The new class chairman is @eva @evasur.`);
				}else{
					maj(`${eee} votes for @eva, ${vvv} for @venalt. The new class chairman is @eva @evasur.`);
				}
			}
			link(`After lesson. `, 510);
			break;
	
			
		case 510:
			txt(`@sas was cheerful, ingenuous happy that @eva beat @ven.`);
			sas(`We won!`);
			if(mile.chair_eva_promise === 1){
				eva(`Yeah, I won! I guess I was the best girl after all, wasn't I?`);
			}else if(mile.chair_eva_promise === 2){
				eva(`Yeah, I won. ...@kat? Sorry about our fight in the morning. You were right, it was wrong of me to push you into quitting.`);
				txt(`She was suddenly generous when it did not cost her anything.`);
			}else if(mile.chair_eva_promise === 3){
				//mile.eva_exploit alread counted
				eva(`Yeah, I won! I just don't get one thing... you agreed with me you'll quit but @maj told us she decided you should give a chance to somebody else?`);
				txt(`There was an evil gleam in her eyes, she knew I had wanted to fuck her over.`);
				eva(`Never mind. All's well that ends well, right?`);
			}
			link(`Congratulation. `, 511);
			link(`I would easily won if I ran! `, 512);
			break;
			
		case 511:
			emo(`fine`);
			mile.chair_eva_end = mile.chair_eva_promise === 3 ? 2 : 1;
			kat(`Congratulations!`);
			txt(`I tried to smile but I could not bear self-satisfied @eva for very long. I needed to be alone.`);
			if(aly.s){
				link(`Leave. `, 600);
			}else{
				next(`Leave`);
			}
			break;
			
		case 512:
			emo(`angry`);
			mile.eva_exploit++; //TODO
			mile.sas_exploit++; //TODO
			mile.chair_eva_end = 3;
			eva(`Yeah?! In that case why you didn't run!`);
			kat(`Because I couldn't!`);
			eva(`Yeah?! Why you couldn't?!`);
			kat(`Because... well...`);
			eva(`You didn't so now you better shut up!`);
			txt(`She snapped which did not bother me but even @sas seemed annoyed:`);
			sas(`Why do you have to be so selfish, @kat, and won't let @eva enjoy her victory?`);
			txt(`I shrugged and left, I could not bear them any longer. I needed to be alone.`);
			
			if(aly.s){
				link(`Leave. `, 700);
			}else{
				next(`Leave`);
			}
			break;
			
			
	//550 - ELECTION - QUIT	
		case 550:
			placetime(`classroom`,``);
			mile.stat--;
			emo(`help`);
			txt(`The next class @maj checked the attendance and then without further due started:`);
			maj(`Today, before the lesson, we will elect the class chairman. She'll be in charge of the organization of the class affairs and will be representing the class on the school council. This year we have two candidates - @venlat and @eva. They both get three minutes for their speech (but they don't have to use the whole three minutes) and then you'll vote. You know the drill.`);
			txt(`Surprised @tom raised his hand:`);
			tom(`@katalt isn't running this time?`);
			txt(`Everybody looked at me. I felt uneasy, I just wanted this to be over, I did not want to bring more attention to my failed re-election campaign. @maj could just say *no* but instead, she looked at me.`);
			kat(`I decided not to run this year. I... Because I would like to give a chance to somebody else.`);
			txt(`It was a nice lie but I was sure everybody knew I gave up because I was afraid of losing to @eva or @ven.`);
			kat(`People who wanted to give their votes to me...`);
			
			link(`...should give them to @eva. `, 551, ()=> mile.chair_support = 2);
			link(`...should give them to @ven. `, 551, ()=> mile.chair_support = 3);
			link(`...have my gratitude but they have to choose a different candidate. `, 551, ()=> mile.chair_support = 1);
			break;
			
		case 551: 
		{
			let kat_boost = 2; //TODO - max has to be 3 for chance to defeat eva with zero boost for vendy, TODO TEMP
			if(mile.chair_c_help) kat_boost++;
			if(mile.chair_a_help) kat_boost++;
			if(mile.chair_blowjob > 0) kat_boost++;
			if(mile.tom_help > 0) kat_boost++;
			kat_boost *= 0.5;
				
			let ven_boost = 0;
			if(mile.chair_manager) ven_boost++;
			
			let eva_boost = 3;
			if(mile.chair_b_help > 0) eva_boost -= 2;
			
			if(window.harbug){
				bux(`kat (weak impact): ${kat_boost}`);
				bux(`eva : ${kat_boost}`);
				bux(`ven : ${ven_boost}`);
			}
			
			if(mile.chair_support === 1){
				emo(`anoy`);
								
				counter.result = eva_boost - ven_boost;
				bux(`result : ${counter.result}`);
				
				if(counter.result === 0){
					txt(`I could not decide which one of those bitches I hated more. I wished they both could lose! But I had to write a name on my paper. `);
					link(`@eva. `, 552, ()=> counter.vote = 1);
					link(`@venalt. `, 552, ()=> counter.vote = -1);
					return;
				}else{
					txt(`I could not decide which one of those bitches I hated more. I wished they both could lose! On my piece of paper, I randomly
					wrote *@venalt* because I assumed @eva will get more votes. `);
					resolve(counter.result, -1);
				}
			
			}else if(mile.chair_support === 2){
				emo(`neutral`);
				
				const result = (eva_boost + kat_boost) - ven_boost;
				bux(`result : ${result}`);
				
				txt(`I supported @eva. I was not happy about her running but still, it was better than fucking @ven. ${mile.chair_b_help > 0 ? "I felt bad about spreading nasty gossips about her. " : ""} `);
				resolve(result, 1);
				
			}else if(mile.chair_support === 3){
				emo(`smug`);
				
				const result = eva_boost - (ven_boost + kat_boost);
				bux(`result : ${result}`);
				
				txt(`I vengetfully smirked - I was sure @eva did not see that comming! That was for stabbing me in the back! `);
				resolve(result, -1);
			}
			
			link(`After lesson. `, 560);
		}
		break;

		case 552:
			resolve(counter.result, counter.vote);
			link(`After lesson. `, 560);
			break;
			
		case 560:
			
			if(mile.chair_elected === 2){ //EVA WON
				if(mile.chair_support === 3){ //VEN
					emo(`angry`);
					mile.eva_exploit++;
					mile.sas_exploit++;
					mile.chair_eva_end = 3; 
					
					txt(`@eva was furious and immediately attacked me:`);
					eva(`What the fuck?!? Why did you support @ven instead of me?! I thought you're my friend! `);
					txt(`I mischievously grinned:`);
					kat(`I thought you're my friend but then you decided to run against me! `);
					eva(`I explained to you several times I'm doint it just because I don't want fucking @ven to be the class chairman! Are you thick? `);
					kat(`That's bullshit! You're a petty jealou bitch! `);
					sas(`Calm down girls! @ven isn't the class chairwoman and that's the most important thing! `);
					txt(`@sas desperately tried to reconcile us but she failed. I walked away, I wanted to be alone. `);				
			
				}else if(mile.chair_support === 2){ //EVA
					emo(`neutral`);
					mile.eva_nice++;
					mile.chair_eva_end = 1;
					
					txt(`@sas was cheerful, ingenuous happy that @eva beat @ven.`);
					sas(`We won!`);
					eva(`Yeah, I won!`);
					txt(`@eva smiled and shook my hand.`);
					eva(`Thank you, @katalt! I could not do it without your support! I'll do my best to make you proud!`);
					txt(`@eva could be very generous when it cost her nothing. I smiled back but I just could not be happy for her. I needed to be alone.`);
		
					
				}else if(mile.chair_support === 1){ //NETURAL
					mile.eva_exploit++;
					mile.chair_eva_end = 2; 
					
					txt(`@sas was cheerful, ingenuously happy that @eva beat @ven.`);
					sas(`We won!`);
					eva(`Yeah, I won! But not thanks to your help!`);
					txt(`@eva scowled at me and accused me:`);
					eva(`Why didn't you support me?`);
					kat(`I thought you won't need it. And I didn't want to sound too pushy and make people vote for @ven out of spite. And it worked in the end.`);
					txt(`I tried to make excuses.`);
					eva(`You agreed that beating @ven was the priority!`);
					sas(`Come on, girls, don't fight, all's well that ends well, right?`);
					eva(`...right.`);
					kat(`...yeah.`);
					txt(`I nodded but I needed to be alone, I could not bear smug @eva.`);
				}
					
			}else{ //VEN WON
				if(mile.chair_support === 3){ //VEN
					emo(`angry`);
					mile.sas_exploit++;
					mile.eva_exploit++;
					mile.eva_exploit++;
					mile.chair_eva_end = 3; 

					txt(`@eva was furious and immediately attacked me:`);
					eva(`What the fuck?!? Why did you support @ven instead of me?! You lost me the elections!`);
					txt(`I mischievously grinned:`);
					kat(`It was nothing personal, @eva. I did it for the greater good, I just thought @ven would be a better chairwoman.`);
					eva(`That's bullshit! You're mad because I dared to run against you! You're just a petty bitch who can't see her friends succeed where she failed!`);
					kat(`I'm not petty!`);
					sas(`You are!`);
					txt(`I expected (and enjoyed) @eva being mad but @sas was pretty angry too:`);
					sas(`You told us that beating @ven was the main priority! You were lying to us! And now she's the chairman, just because of your stupid pettiness!`);
					kat(`I don't have to listen to this!`);
					txt(`I couldn't bear their seething, I needed to be alone.`);		
			
				}else if(mile.chair_support === 2){ //EVA
					emo(`neutral`);
					mile.eva_nice++;
					mile.chair_eva_end = 1; 
					
					eva(`Well, I didn't see that comming. `);
					kat(`Me neither! `);
					eva(`I'm grateful for your support. You're a true friend. I know it was rude to run against you but I honestly believed it was our only chance to defeat @ven. `);
					kat(`It was a great plan. `);
					txt(`@eva missed my sarcasm. `);		
					eva(`Well, class chairman is a stupid useless post anyway. In two weeks, nobody will even remember that @ven won and she's the chairwoman instead of you. `);
					txt(`I was appaled by how casually she took the defeat.  I needed to be alone.`);		
					
					
					
				}else if(mile.chair_support === 1){ //NETURAL
					mile.eva_exploit++;
					mile.chair_eva_end = 2;
					
					txt(`@eva was furious and immediately attacked me:`);
					eva(`What the fuck?!? Why you didn't support me?! You lost me the elections!`);
					kat(`I thought you won't need it. And I didn't want to sound too pushy and make people vote for @ven out of spite.`);
					sas(`That makes sense. `);
					eva(`It does not! You would rather see @ven won than support me? You petty bitch!  `);
					kat(`Oh! Only I can defeat @ven! Your plan worked so fucking flawlesly! `);
					sas(`Girls, please, don't fight. `);
					eva(`Fuck you, @kat! `);
					kat(`Go to hell, @eva! `);
					txt(`I walked away. I needed to be alone.`);		
					
				}	
			}
			
			if(aly.s){
				link(`Leave. `, 700);
			}else{
				next(`Leave`);
			}
			break;
	
			
			
		//600 ELECTION - FULL
		case 600:
			placetime(`classroom`);
			emo(`focus`);
			txt(`The next class @maj checked the attendance and then without further due started: `);
			maj(`Today, before the lesson, we will elect the class chairman. She'll be in charge of the organization of the class affairs and will be representing the class on the school council. This year we have three candidates - @katalt, @venalt and @eva. They all get three minutes for their speeches (but they don't have to use the whole three minutes) and then you'll vote. You know the drill. @kat? You can go first.`);
			txt(`Oh fuck, the speech. I completely forgot about that. I had to improvise and make something up on the fly. `);
			
			link(`It is a great honor to be the chairwoman of this class... `, 601, ()=> mile.chair_speech = 1);
			link(`@venalt and @eva are great. However...  `, 601, ()=> mile.chair_speech = 2);
			link(`You're all stupid if you won't vote for me! `, 601, ()=> mile.chair_speech = 3);
			break; 
			
		case 601: 
			if(mile.chair_speech === 3){
				mile.stat--;
				txt(`Lacking proper inspiration, I decided to be honest. Retrospectively, it was not the wisest decision. `);
			}else if(mile.chair_speech === 2){
				txt(`This was the last opportunity to throw some dirt at my opponents. I somehow managed to besmirch them without sounding rude and people even laughed at my quips. `);
			}else if(mile.chair_speech === 1){
				txt(`I gave them the blandest, safest speech possible. And I believe it was pretty decent, I was pandering to them, thanked them for their trust and asked them to give me one more chance. `);
			}
			txt(`@eva went after me and she did not have anything prepared either. However, she was able to make a rough draft during my speech and it was not bad. That treacherous bitch even thanked me for my service before explaining there was time for change. `);
			txt(`@ven was too nervous to speak off the cuff and instead read her speech from a piece of paper. It was fortunate her delivery was awful because her speech was very good and made both me and @eva look like total dummies. `);
			txt(`Then we all got small pieces of paper and had to cast our votes. My hand was actually shaking when I wrote down: `);
			txt(`<i>@katalt.</i>`);
			link(`Vote counting. `, 602);
			break;
			
			
		case 602:
			{
				let votes = 0;
				if(mile.tom_help > 0){
					votes += 2;
					bux("++2 @tom's help");
				}
				if(mile.chair_a_help > 0){
					votes++;
					bux("++1 @qaa's help");
				}
				if(mile.chair_b_help > 0){
					votes++;
					bux("++1 @qbb's help");
				}
				if(mile.chair_c_help > 0){
					votes++;
					bux("++1 @qcc's help");
				}
				
				if(mile.chair_orgy > 0){
					votes += 2;
					bux("++2 cocksucking for votes");
				}else if(mile.chair_orgy === -2){
					votes -= 2;
					bux("--2 rudely refused cocksucking for votes");
				}else if(mile.chair_orgy === -1){
					votes -= 1;
					bux("--1 refused cocksucking for votes");
				}
				
				if(mile.chair_manager > 0){
					votes--
					bux("--1 @qaa @ven's campaign manager");
				}
			
				if(mile.chair_speech === 3){
					votes--;
					bux("--1 awful speech");
				}
			
				if(ext.stat === 6){ //not going to happen
					votes +=2;
					bux("++2 very high status");
				}else if(ext.stat === 5){ //improbably
					votes++;
					bux("++1 high status");
				}else if(ext.stat === 4){
					bux("++0 neutral status");
				}else if(ext.stat === 3){
					bux("--0 neutral status");
				}else if(ext.stat === 2){
					votes--;
					bux("--1 low status");
				}else if(ext.stat === 1){
					votes -= 2;
					bux("--2 very low status");
				}else if(ext.stat === 0){
					votes -= 3;
					bux("--3 extremely very low status");
				}
				bux(`== ${votes}`);
				
				
				bux(`releected? ${votes} > 3?`);
				
				
				if(counter.cheat === 1){
					votes = 4; 
					bux(`cheat victory `);
				}else if(counter.cheat === -1){
					votes = 2;
					bux(`cheat defeat`);
				}
				counter.cheat = 0;
				bux("---");
				
				
				/*
					no effort - 3 
					high effot - 6
					lowest -4
				*/
				let kat_votes = (()=>{
					if(votes < -7) return 1;
					switch(votes){
						default: return 18; //reelected
						case 9: return 17; //reelected
						case 8: return 16; //reelected
						case 7: return 15; //reelected
						case 6: return 14; //reasonable hightest score
						case 5: return 13; //reelected
						case 4: return 12; //minimum reelected
						case 3: return 11; //not reelected
						case 2: return 10; //not reelected
						case 1: return 9; //not reelected
						case -1: return 8; //not reelected
						case -2: return 7; //not reelected
						case -3: return 6; //not reelected
						case -4: return 5; //reasonable lowest score
						case -5: return 4; //not reelected
						case -6: return 3; //not reelected
						case -7: return 2; //not reelected
					}
				})();
			
							
				
				
				let ven_votes = 8; 
				
				if(mile.chair_b_help > 0) ven_votes += 2;
				if(mile.chair_manager > 0) ven_votes += 2;
				
				if(kat_votes === 11 && ven_votes === 10){
					kat_votes = 10;
					ven_votes = 11;
				}
				
				let eva_votes = 29 - (kat_votes + ven_votes);
				
				if(eva_votes === 10 && ven_votes === 10){
					if(ra.b){
						eva_votes = 11;
						ven_votes = 10;
					}else{
						eva_votes = 9;
						ven_votes = 11;
					}
				}
				
				txt(`@maj counted the votes and then recounted them again, just to be sure: `);
				maj(`${kat_votes} votes for @katalt, ${eva_votes} for @eva and ${ven_votes} for @venalt.`);
				txt(`@maj smiled: `);
				
				if(kat_votes >= 12){
					mile.chair_elected = 1;
					emo(`happy`);
					
					maj(`The new class chairman is @kat @katsur.`);
					kat(`YES!`);
					maj(`Please, sit down and calm down, @katalt!`);
					link(`Victory. `, 610);
					
				}else if(eva_votes > ven_votes){
					mile.chair_elected = 2;
					emo(`cry`);
					
					maj(`The new class chairman is @eva @evasur.`);
					
					link(`Defeat. `, 620);
				}else{
					mile.chair_elected = 3;
					emo(`cry`);
					
					maj(`The new class chairman is @venalt @vensur.`);
					link(`Defeat. `, 630);
				}
					
				
				
				if(window.hardbug){
					link(`## Force victory. `, 602, ()=> counter.cheat = 1);
					link(`## Force defeat. `, 602, ()=> counter.cheat = -1);
				}
				
			}	
			break;
			
		case 610: 
			mile.sub--;
			mile.stat++;
			mile.stat++;
			mile.stat++;
						
			txt(`Right when the lesson was over, @sas rushed to hug me and I was moved by her ingenious joy.  `);
			sas(`You won! `);
			kat(`Yeah! I did! `);
			txt(`I felt awesome. Despite everything I went through, I won again. I felt my life was back on the right track. Even that perfidious bitch @eva was smiling at me:`);
			eva(`You defeated me. Congratulations! `);
			
			link(`You were a worthy opponent. `, 611);
			link(`I hope you feel really stupid! `, 612);
			break; 
			
		case 611:
			mile.eva_nice++;
			mile.chair_eva_end = 1;
					
			kat(`I really thought you were going to beat me. `);
			eva(`I was wrong when I didn't trust you. `);
			kat(`Yeah, you were. But that doesn't matter! The most important thing is that @ven was beaten! `);
			eva(`Damn yeah! `);
			
			next(`Done. `);
			break;
			
		case 612:
			mile.eva_exploit++;
			mile.chair_eva_end = 2;
			emo(`angry`);
			
			kat(`How could you even fucking think you might have the slightest chance to defeat me? `);
			txt(`@eva's false smile was gone:`);
			eva(`You already won! You don't have to rub it in my face! `);
			kat(`I will until you admit how stupid you were!`);
			sas(`Come on, girls! Don't fight! `);
			eva(`Fuck you @kat! Enjoy your stupid useless job! `);
			kat(`Oh, I fucking will! `);
			
			next(`Done. `);
			break;
		
		
		case 620: 
			mile.stat--;
			mile.stat--;
			emo(`sad`);
			
			sas(`Congatulations! `);
			txt(`@sas was ingeniously happy @eva won. @eva showed her irksome smug smile. `);
			if(mile.chair_eva_promise === 2){
				eva(`Thanks, @sas!  `);
				txt(`Then @eva looked at me: `);
				eva(`You're not so tough anymore, Miss ex-chairwoman? `);
				sas(`Come on, @eva, don't be mean! `);
			}else if(mile.chair_eva_promise === 3){			
				eva(`Thanks, @sas!  `);
				txt(`@eva looked at me: `);
				eva(`Lying straigt into my face about quiting was a pretty nasty trick! I knew I shouldn't underestimate you.  `);
			}else{
				eva(`Thanks, @sas! Well, the most important thing isn't that I won but that @ven got defeated, right? `);
			}
			
			link(`Congratulations! `, 621);
			link(`Go fuck yourself! `, 622);
			break; 
			
		case 621: 
			mile.chair_eva_end = 1;
			
			if(mile.chair_eva_promise !== 1){
				txt(`I ignored all her cruel mockery: `);
			}
			kat(`Good job. `);
			txt(`I tried to smile even though I felt dead inside. How could she defeat me? Everything was so wrong.  I needed to be alone.`);
		
			if(aly.s){
				link(`Leave. `, 700);
			}else{
				next(`Leave.`);
			}
			break;
			
		case 622:
			mile.eva_exploit++;
			mile.chair_eva_end = 3;
			emo(`angry`);
			
			txt(`I was heartbroken. Somehow, till the last moment, I hoped an improbable miracle would make everything okay. But I lost. `);
			kat(`Enjoy being the fucking chairman, you stupid bitch! `);
			eva(`Go to hell, @kat! I won and your entitled little brain isn't able to handle it!`);
			txt(`She was right that I was unable to handle my defeat gracefully. I needed to be alone. `);
			
			if(aly.s){
				link(`Leave. `, 700);
			}else{
				next(`Leave.`);
			}
			break;
			
		case 630: 
				mile.stat--;
				mile.stat--;
				emo(`sad`);
			
				sas(`Well, that didn't go well... `);
				txt(`That was an understatement. We were both defeated by fucking @ven! How fucking humiliating!`);
				
				if(mile.chair_eva_promise === 2 && mile.chair_eva_promise === 3){
					eva(`It's your fucking fault! I told you this would happen if we split the vote!`);
					kat(`Yeah? When you're smart why you didn't step down and let me win? `);
					eva(`Because you wouldn't fucking win!`);
					sas(`Come on, girls, stop fighting. It doesn't matter anymore. `);
				}else{
					eva(`I can't believe it! We both got fucked over by fucking @ven! `);
				}
				
				link(`Why are we such stubborn bitches? `, 631);
				link(`@ven won only because of you! `, 632);
			break; 
				
		case 631:
				emo(`relax`);
				mile.eva_nice++;
				mile.chair_eva_end = 1;
				
				sas(`I'm thinking about that question every day. Why can't you be nicer? `);
				eva(`Being nice is boring. I love to win! `);
				kat(`Exactly what @eva said!`);
				sas(`Well, you both lost so you better should reevaluate whole your lives!`);
		
				if(aly.s){
					link(`Leave. `, 700);
				}else{
					next(`Leave.`);
				}
				break;
				
		case 632:
				emo(`relax`);
				mile.eva_exploit++;
				mile.chair_eva_end = 3;
				
				eva(`No! Look at yourself! Do you really think people would elect somebody who recently acts like a crazy slut?  `);
				kat(`Why is your first instincting always to stab me in my back? I hate you so much! Also, you're fat. `);
				eva(`Fuck you!`);
				kat(`Go to hell!`);
				txt(`The defeat made me so furious and heartbroken. I needed to be alone. `);
				if(aly.s){
					link(`Leave. `, 700);
				}else{
					next(`Leave.`);
				}
				break;
				
				
	//700 NERD
		case 700:
			emo(`angry`);
			placetime(`halwayy`);
			
			if(relation.qaa > relation.qcc){
				txt(`However, in the hallway @qaa caught up with me.`);
				qaa(`@kat?`);
				kat(`What!`);
				qaa(`Are you fine? You don't seem fine...`);
				kat(`NO! I'm not fucking fine! `);
				if(mile.chair_elections_quit){
					qaa(`But all went well, didn't it? You realized there are better ways to spend your time than that stupid, pointless job and didn't run?`);
					kat(`What the hell are you talking?! I wanted to be reelected, more than anything! But I had no chance because of you! You ruined my life!`);
				}else{
					qaa(`Sorry about you not getting reelected but...`);
					kat(`But what?!? This is all your fucking fault! You ruined my life!`);
				}
				qaa(`I couldn't imagine that...`);
				kat(`Oh, of course you fucking could! You're a smart guy, you could figure that out! You just didn't fucking care!`);
				qaa(`Come on, @kat. Calm down. It isn't so bad, being class chairman isn't such a big deal...`);
				kat(`FUCK YOU!`);
				
			}else if(relation.qcc > 0){ //maybe > 1
				txt(`However, in the hallway @qaa caught up with me.`);
				qcc(`@kat?`);
				kat(`What!`);
				qcc(`Are you fine? You don't seem fine...`);
				kat(`NO! I'm not fucking fine! `);
				if(mile.chair_elections_quit){
					qcc(`But all went well, didn't it? You realized there are better ways to spend your time than that stupid, pointless job and didn't run?`);
					kat(`What the hell are you talking?! I wanted to be reelected, more than anything! But I had no chance because of you! You ruined my life!`);
				}else{
					qcc(`Sorry about you not getting reelected but...`);
					kat(`But what?!? This is all your fucking fault! You ruined my life!`);
				}
				//qcc(`Sorry about you not getting reelected but...`);
				//kat(`But what?!? This is all your fucking fault! You're ruining my life!`);
				qcc(`I couldn't imagine that...`);
				kat(`Oh, of course you fucking could! You're a smart guy, you could figure that out! You just didn't fucking care!`);
				qcc(`Come on, @kat. Calm down. It isn't so bad, being class chairman isn't such a big deal...`);
				kat(`FUCK YOU!`);
								
			}else if(relation.qbb > 0){
				qbb(`@kat?`);
				txt(`I was surprised when @qbb caught up with me. `);
				kat(`What!`);
				qbb(`Are you fine? You don't seem fine...`);
				kat(`NO! I'm not fucking fine! `);
				if(mile.chair_elections_quit){
					qbb(`But all went well, didn't it? You realized there are better ways to spend your time than that stupid, pointless job and didn't run?`);
					kat(`What the hell are you talking?! I wanted to be reelected, more than anything! But I had no chance because of you! You ruined my life!`);
				}else{
					qbb(`Sorry about you not getting reelected but...`);
					kat(`But what?!? This is all your fucking fault! You ruined my life!`);
				}
				qbb(`I couldn't imagine that...`);
				kat(`Oh, of course you fucking could! You're a smart guy, you could figure that out! You just didn't fucking care!`);
				qbb(`Come on, @kat. Calm down. It isn't so bad, being class chairman isn't such a big deal...`);
				kat(`FUCK YOU!`);
			}
			next(`Run away. `);
			break;
			
			
	 
	}
}



//ELECTIONS TOM
export const chair_elections_tom = (index)=> {
	switch(index){
		default:
		case 101:
			emo(`anoy`);
			placetime(`in Front of the School`);
			if(mile.chair_elected === 1){
				txt(`@tom was waiting for me in front of the school. `);
				tom(`Hey, @katalt!`);
				kat(`How can I help you?`);
				tom(`I just wanted to congratuleted you! Amazing victory! And we dodged a bulled, imagine @ven or @eva in charge! `);
				
				const deal = (()=>{
					if(mile.tom_help === 3) return "a blowjob";
					if(mile.tom_help === 4) return "sex";
					if(mile.tom_help === 5) return "kinky sex";
					if(mile.tom_help === 6) return "anal sex";
					return "a date";
				})();
				
				kat(`And remind me I owe you ${deal} `);
				tom(`No, I know you're appreciative and I'm sure you would never forget about our deal! `);
				
				next(`Of course. `, ()=> mile.tom_date = 1);
				
			}else{
				txt(`I somehow survived the rest of the day. I just wanted to be back at home. But @tom was waiting for me in front of the school, probably ready to remind me of my promise.`);
				tom(`Hello, @katalt!`);
				kat(`Hey.`);
				tom(`How are you?`);
				txt(`I just shrugged.`);
				if(mile.chair_elections_quit){
					tom(`Did you decide at the last moment to not run? Because of @eva was running too?`);
				}else{
					tom(`That old cunt really did not let you run?`);
				}
				txt(`I just shrugged again.`);
				tom(`That's so fucked up! I really wanted to see you re-elected.`);
				kat(`Because I did a good job or because of your selfish interests?`);
				
				{
					const deal = (()=>{
						if(mile.tom_help === 3) return "a blowjob";
						if(mile.tom_help === 4) return "sex";
						if(mile.tom_help === 5) return "kinky sex";
						if(mile.tom_help === 6) return "anal sex";
						return "a date";
					})();
					tom(`I actually think you were a pretty great chairwoman! But yeah, especially because you promised me ${deal}.`);
				}
				kat(`At least you're honest.`);
				tom(`I swear I worked very hard and convinced several people to vote for you! But I assume the deal is now invalid, right? You didn't get re-elected.`);
				if(mile.slut > 12){
					txt(`I hesitated. What was the point of refusing his advances? Not like I could fall any lower. He could be an annoying jerk but he was also kinda handsome. Maybe casual sex would make me feel better? Everybody already knew I was a slut anyway. ${mile.chair_eva_end === 3 ? "And at least he was nice while all my other supposed friends hated me or tried to destroy my life. " : ""}`);
				}else if(mile.slut <= 3){
					txt(`I hesitated. What was the point of refusing his advances? Not like I could fall any lower. Trying to be a good and reasonable girl for so long got me nowhere. Maybe it was time to stop worrying, do something stupid and not give fuck about the consequences. ${mile.chair_eva_end === 3 ? "And at least he was nice while all my other supposed friends hated me or tried to destroy my life. " : ""}`);
				}else{
					txt(`I hesitated. What was the point of refusing his advances? Not like I could fall any lower. ${mile.chair_eva_end === 3 ? "And at least he was nice while all my other supposed friends hated me or tried to destroy my life. " : ""}`);
				}
				next(`Why not. We can meet later. `, ()=> mile.tom_date = 1);
				next(`Sorry, you didn't help me to get re-elected. `, ()=> mile.tom_date = -1);
			}
			break;
	}
}




//CHAIRMAN ORGY 2
export const chair_orgy_2 = (index) => {
	const titjob = PC.getDim("breastSize") > 15;
	
	switch(index){
		default:
		case 101:
			present( //TODO - HARD SET NAMES
				[1, "Oliver", undefined, undefined],
				[2, "Tony", undefined, undefined],
				[3, "Igor", undefined, undefined],
			)
			emo("suspect"); //TODO
			placetime(`hallway`);
			
			txt(`During the break, I was surrounded by @mik and his friends.`);
			mik(`Hello, @kat! How are you doing?`);
			kat(`I'm doing fine?`);
			txt(`I shrugged. I was wary, they seemed somewhat suspicious.`);
			kat(`What do you want?`);
			mik(`You know, the last time we had a such great time! You are really amazing and know what to do with your mouth! I'm not joking, honestly the best blowjob I ever had. And I got sucked by @zan and Maru! But you're better than them both combined!`);
			kat(`...thanks?`);
			mik(`So we were thinking, maybe we might do it again? Maybe the next break?`);
			kat(`What?!? You want me to suck all your cocks again?!?`);
			npc(1, `Yeah!`);
			npc(2, `If you don't mind!`);
			
			if(mile.chair_elected === 1){
				npc(3, `We ensured you get reelected! You owe us a big favor! `);
				kat(`I already sucked all your cocks! I don't owe you anything! `);
				npc(2, `Come on, @kat, it wouldn't hurt to show a little gratitude! `);
				npc(1, `Yeah!`);
				kat(`No.`);
				mik(`There has to be something else you might need?`);
			}else{
				kat(`No! I don't need your fucking votes anymore!`);
				npc(3, `We would owe you a big favor!`);
				mik(`Come on! There has to be something else you might need?`);
			}

			con_link(mile.yw_whore > 0 || mile.whore > 1, `no whore experience`, `I need cash! `, 111);
			link(`Fine, I'll do it! What are you offering? `, 110);
			link(`I won't do it! `, 102);
			break;

	case 102: 
		if(mile.slut > 10) mile.slut--;
		
		if(mile.slut > 12 && mile.sub > 3){
			emo("shock");
			mik(`Don't play coy, you whore! We all know you'll eventually say yes so don't play hard to get!`);
			txt(`I was stunned by @mik's sudden attack and hostility. I gained the reputation as an easy girl and it seemed okay to them to take sexual advantage of me.`);
		}else{
			emo("anoy");
			mik(`Come on, @kat! What's the problem? You didn't mind sucking our cocks the last time, did you?`);
			txt(`They did not accept my rejection and pushed me further. Like it was now normal for me to give them blowjobs at school bathrooms.`);
		}
		npc(1, `What do you want?`);
		npc(2, `We can pay you?`);
		if(mile.slut > 11){
			mik(`What about like  ${money.chair_bj_low}? Each? That seems more than fair, you're usually sucking cocks for free.`);
		}else{
			mik(`What about like  ${money.chair_bj_low}? Each? It's easy quick cash, isn't it? I would take it if I was a girl.`);
		}
		
		con_link(mile.sub < 10, `submissive`, `Fuck you, I said no! `, 103, ()=> mile.chair_orgy_2 = -2);
		link(`No, I won't do it! `, 103, ()=> mile.chair_orgy_2 = -1);
		link(`Fine, I'll do it. `, 190, ()=> mile.chair_orgy_2 = 1);
		break;
		
	case 103:
		mile.slut--;
		
		if(mile.slut > 12 && mile.sub > 6){
			mik(`You know what? Fuck you @kat! We brought a very reasonable suggestion which would benefit everybody! But if you want to be an unreasonable buzzkill, fine, whatever!`);
		}else{
			mik(`Come on, @kat!`);
			kat(`I said no! I'm not a whore! `);
		}
		npc(1, `Our offer stays - if you change your mind.`);
		next(`I won't! `);
		break;
	
	
	case 110:
		emo("neutral"); 
		mile.slut++;
		
		npc(1, `We would owe you.`);
		npc(2, `Or we can pay you?`);
		if(mile.slut > 11){
			mik(`What about like ${money.chair_bj_low} ees? Each? That seems more than fair, you're usually sucking cocks for free.`);
		}else{
			mik(`What about like ${money.chair_bj_low} ees? Each? It's easy quick cash, isn't it? I would take it if I was a girl.`);
		}
	
		con_link(mile.sub < 12, `submissive`, `I want ${money.chair_bj_high}. `, 121, ()=> mile.chair_orgy_2 = 4);
		link(`I don't want you to think I'm a whore!  `, 120, ()=> mile.chair_orgy_2 = 2);
		link(`Okay, I'll do it for ${money.chair_bj_low}. `, 190, ()=> mile.chair_orgy_2 = 3);
		break; 
		
	
	case 111:
		emo("focus");
		if(mile.slut < 10) mile.slut++;
				
		npc(1, `What?!?`);
		npc(2, `I told you she'll do anything if we pay her.`);
		npc(3, `That's reasonable. If you need money, we have money.`);
		if(mile.slut > 11){
			mik(`What about like  ${money.chair_bj_low} ees? Each? That seems more than fair, you're usually sucking cocks for free.`);
		}else{
			mik(`What about like  ${money.chair_bj_low} ees? Each? It's easy quick cash, isn't it? I would take it if I was a girl.`);
		}
		
		con_link(mile.sub < 12, `submissive`, `I want ${money.chair_bj_high}. `, 121, ()=> mile.chair_orgy_2 = 4);
		link(`I don't want you to think I'm a whore!  `, 120, ()=> mile.chair_orgy_2 = 2);
		link(`Okay, I'll do it for ${money.chair_bj_low}. `, 190, ()=> mile.chair_orgy_2 = 3);
		break;
		
	case 120:
		emo("unco"); //TODO
		if(mile.slut > 15) mile.slut--;
				
		if(mile.slut > 12){
			txt(`I was not squeamish to suck cocks, I was not bothered by that. It just ${mile.whore > 0 ? "still" : ""} felt weird to do it for money.`);
		}else{
			txt(`I was reluctant. It was embarrassing and demeaning to suck cocks of my classmates. And especially when I was offered money for it.`);
		}
		mik(`Of course, you aren't a whore!`);
		npc(3, `We won't respect you any less.`);
		npc(2, `It will be just like the last time`);
		txt(`They ensured me until I hesitantly nodded.`);
		
		link(`Okay. `, 190);
		break;
		
		
	case 121:
		emo("focus");
				
		mik(`You can't be serious!`);
		kat(`Well, too bad. That's my price. Find somebody else to suck your cocks.`);
		npc(1, `Damn, that's too much!`);
		npc(3, `Yeah!`);
		mik(`We can give you ${money.chair_bj_mid} but we won't go higher.`);
		kat(`...`);
		mik(`...`);
		kat(`Fine!`);
		npc(2, `You're not cheap.`);
		link(`You know I'm worth it. `, 190);
		break;
	
	
	case 190: 
		emo("focus");
		
		kat(`The long break, the same bathroom as before?`);
		mik(`I have something better. I got a key to a storeroom on the third floor. Nobody will disturb us there. And we should meet after the lunch before the afternoon classes, to have more time.`);
		kat(`Where did you get the key?`);
		mik(`That's a secret. But if you want your cash, you little whore, you should better be there.`);
		if(mile.chair_orgy_2 === 2 || mile.sub < 1){
			txt(`He shook his head when he saw my disapproving face.`);
			mik(`That was just a joke, just giving head for money doesn't make you a whore, of course!`);
		}
		link(`Storage room`, 200);
		break;
		
		
	case 200: 	
		emo("anoy");
		showLess(0, -1);
		mile.slut++;
		mile.whore++;
		
	//TODO - TIME!!!  - MAYBE DIVIDE INTO TWO EVENTS? 
		placetime(`storage room on third floor`, `before afternoon classes`);
		
		txt(`We met in a narrow room. Around the walls were shelves with boxes, there was not much free space but we had guaranteed privacy.`);
		if(mile.chair_orgy <= 2){
			kat(`No fucking videos this time!`);
			mik(`Are you still mad? Sorry, it was just a misunderstanding.`);
		}
		npc(2, `Maybe you would like to remove your top?`);
		txt(`@npc_2 lewdly suggested. I shrugged and without ${mile.slut < 9 ? "enthusiasm" : "shame"} stripped my @upper. I did not want to risk spending the rest of the day in cum-stained clothes. Again.`);
		mik(`Okay, get on your knees${mile.sub > 10 ? "slut" : ""}.`);
		txt(`@mik nodded and with a cocky theatrical move unbuckled his pants. I squatted down but @npc_2 grabbed @mik's shoulder.`);
		npc(2, `Where are you going?`);
		mik(`To shove my dick into @kat's mouth?`);
		npc(2, `You went first the last time!`);
		mik(`So? Nobody else wanted to go first!`);
		npc(3, `I don't want to go last again! It was too stressful, I barely managed to get back to the class on time!`);
		txt(`@mik frowned. He did not want to go last, a hard-on was already bulging his pants, but he did not want to look selfish and inconsiderate in front of all his friends.`);
		mik(`We should draw lots.`);
		npc(1, `How?`);
		npc(2, `Let's draw straws.`);
		mik(`Does anybody have matches?`);
		npc(3, `I don't.`);
		mik(`@kat?`);
		kat(`I don't have any fucking matches!`);
		txt(`My tone was pretty annoyed, I was topless, sitting on the cold floor and watching them wasting my time with something they should had already decided.`);
		link(`Take your time, I have the whole day just for you. `, 201);
		break;
		
	case 201: 
		emo("relax");
		
		
		txt(`The lucky one was eventually @npc_3 who drew a piece of paper with the number *1*. The number *2* was @npc_2; *3* and *4* were @mik and @npc_1 respectivelly.`);
		npc(3, `Okay.`);
		npc(1, `What are you waiting for?`);
		txt(`@npc_3 was a bit nervous to go first. The other guys pushed him forward to me and I unbuttoned his pants and pulled them down. The whole process was almost automatic for me, I was getting very experienced at cock-sucking.`);
		npc(3, `Oh yeah!`);
		txt(`He moaned. I jerked his cock and my tongue leisurely toyed with the tip. When I took it further into my mouth, he lightly thrust with his hips. For both of us it was a more enjoyable experience than the last time when he had to rush to the class.`);
		npc(3, `OH YEAH!`);
		txt(`@npc_3 laughed and his cock began twitching, filling my throat with his cum. He patted my head:`);
		npc(3, `Thanks, @kat!`);
		txt(`I wiped my mouth and @npc_2 moved to his place. His cock was already out and he was impatiently stroking it. I rode up and down over his shaft with my tongue.`);
		if( titjob ){
			npc(2, `I was thinking... maybe instead of a blowjob we could use your amazing, huge tits?`);
			kat(`You want me to make you cum with my breasts?`);
			npc(2, `Yeah!`);
			txt(`I shrugged:`);
			kat(`Why not.`);
			npc(2, `Awesome!`);
			npc(3, `What?! I had no idea that was an option!`);
			npc(2, `Too bad! You can try it the next time!`);
			txt(`@npc_2 so offhandedly assumed this was not a special occasion, that it will happen again and I will agree to weekly sexually satisfy them for money.`);
			if(mile.clinic_boobjob){				
				npc(2, `It's so fucking hot that you got your tits enlarged!`);
				if(mile.clinic_boobjob < 3){
					npc(1, `Do you plan to get them even bigger in the future? That would be cool!`);
					npc(3, `I second that!`);
					if(mile.bimbo_mods > 5){
						kat(`Definitely!`);
					}else if(mile.bimbo_mods > 1){
						kat(`Dunno, maybe.`);
					}else{
						kat(`No! I think they are exactly right.`);
					}
				}
			}
			txt(`I squeezed his cock between my breasts, pushing them tightly together and moved them up and down. I was jerking him with my tits, the tip of his cock was pocking against my face and I was not able to resist teasing it with my tongue.`);
			npc(2, `Oh fuck, I'm there!`);
			txt(`He climaxed, shooting his cum on my face and neck.`);
		
		}else{
			npc(2, `Come on! Suck me hard!`);
			txt(`@npc_2 keenly encouraged me. He took me by my hair and gently yet firmly guided me. I was sucking then he suddenly pulled his dick out.`);
			kat(`Oh fuck, not again!`);
			npc(2, `Fuck yeah!`);
			txt(`I opened my mouth and he erupted all over my chin.`);
		}
		link(`@mik. `, 202);
		break; 
		
	case 202: 
		emo("shock");
		if(mile.sub + mile.slut > 14){
			mik(`You don't have to clean up. You look way better with your face covered with cum, it seems so natural for you.`);
		}else{
			mik(`You don't have to clean up. I don't mind, you look pretty even with your face covered with cum.`);
		}
		txt(`Chuckled @mik.`);
		
		if(mile.sub < 0){
			kat(`Fuck you!`);
			txt(`I shoved him a middle finger.`);
		}else if(mile.sub < 10){
			kat(`You're such a dick.`);
			txt(`I shook my head.`);
		}else{
			txt(`His tone was mocking and demeaning but I obeyed him.`);
		}
		
		mik(`You know what, @kat? Watching you got me so horny! I don't think a blowjob will be enough to satisfy me. I want to fuck you.`);
		
		if(mile.sub > 9){
			kat(`But that wasn't a part of the deal!`);
			mik(`It is now!`);
		}else{
			kat(`No! That wasn't a part of the deal!`);
			mik(`I'm suggesting altering the deal!`);
		}

		txt(`The other guys were as surprised as me.`);
		mik(`I'll pay you double if you let me use your naughty snatch.`);
		
		con_link(mile.slut > 3, `low slut`, `Awesome! Let's fuck! `, 211, ()=> mile.chair_orgy_sex =  2);
		link(`Hmmm.... fine!  `, 211, ()=> mile.chair_orgy_sex = 1);
		link(`Sorry but no.  `, 210, ()=> mile.chair_orgy_sex = -1);
		con_link(mile.sub < 10, `submissive`, `Absolutely no!  `, 210, ()=> mile.chair_orgy_sex = -2);
		break; 
		
		
	case 210: 
		emo("focus");

		if(mile.chair_orgy_sex === -2){
			kat(`No! Are you crazy?!`);
		}else{
			kat(`No, I'm not interested in that.`);
		}
		mik(`Come on! Just a tip!`);
		kat(`No! Take your pants off and I'll blow you but that's all!`);
		mik(`Fine, you buzzkill.`);
		npc(3, `Good. I would feel like a dumbass if you get to fuck her while all I got was a lousy blowjob.`);
		if(mile.sub > 15){
			txt(`I was hurt by his thanklessness but I did not want to argue. I just focused on @mik's big cock.`);
		}else{
			kat(`What the fuck?`);
			txt(`I turned to thankless @npc_3, glared at him and just with one hand mindlessly pumped with @mik's big cock.`);
			npc(3, `Oh! ...I didn't mean that! ...it was an awesome blowjob! ...I just... ....fucking you would be such an honor!`);
			txt(`I shook my head and took @mik's dick between my lips.`);
		}
		npc(1, `You really thought she'll fuck you?`);
		mik(`Yeah, I was pretty sure she will! I mean, you saw how easy it was to convince her to suck our cocks!`);
		npc(2, `Maybe she'll change her mind the next time?`);
		if(!titjob){
			txt(`@npc_2 so offhandedly assumed this was not a special occasion, that it will happen again and I will agree to weekly sexually satisfy them for money.`);
		}
		kat(`I-hhm-won't-hmmH.`);
		txt(`I mumbled with my mouth full but I was not sure if they understood me. They were chatting like I was not there.`);
		mik(`I regret you don't want to fuck me but your blowjob isn't bad either! ...AHHHh! I'm cumming!`);
		txt(`I swallowed his semen.`);
		mik(`Thanks!`);
		txt(`I fixed the hair that was falling into my face and got ready to pleasure the fourth cock in the row. Honestly, I was getting a bit exhausted. I engulfed @npc_1's cock and thought about what I will do in the afternoon.`);
		txt(`Finally, I brought even @npc_1 to orgasm. They were about to leave and I have to remind him:`);
		kat(`What about the money?`);
		mik(`Of course.`);
		npc(1, `Pleasure doing business with you!`);
		
		{
			log_sex("bj", "npc_3");
			if(titjob){
				log_sex("boobs", "npc_2");
			}else{
				log_sex("bj", "npc_2");
			}
			log_sex("bj", "mik");
			log_sex("bj", "npc_2");
			const bj = (mile.chair_orgy_2 === 4 ? money.chair_bj_mid : money.chair_bj_low) ;
			payment(bj * 4, "cash"); 
		}
		next();
		break;



	case 211: 	
		mile.slut++;
		showLess(0, 0, -1);
		emo("horny");
		
				
		if(mile.chair_orgy_sex ===  2){
			kat(`Sounds good. I can use a cock inside me.`);
			txt(`I lascivously winked at him.`);
		}else{
			txt(`It did not seem like the brightest idea but I still dubiously agreed.`);
			kat(`Okay.`);
			mik(`I would appreciate a bit more enthusiasm.`);
			kat(`Enthusiasm is paid extra.`);
		}
		
		npc(2, `What?!? That isn't fair! I was thinking about it but I wouldn't think she would agree!`);
		mik(`This should teach you, you shouldn't underestimate how huge slut @katalt is!`);
		if(mile.slut > 15){
			kat(`Yeah! I don't know what is shame!`);
		}
		if(!titjob){
			npc(2, `I guess I'll have to wait till the next time. `);
			txt(`@npc_2 so offhandedly assumed this was not a special occasion, that it will happen again and I will agree to weekly sexually satisfy them for money.`);
		}
		
		txt(`I pulled down my @lower and @mik moved closer to me. His fingers ran down over my side and hip, then changed their direction and he began fondling me between my legs.`);
		
		mik(`You'll really let me fuck you, won't you?`);
		kat(`Don't make me change my mind.`);
		mik(`Too late for that! You're all mine now!`);
		txt(`He roughly pushed me against the boxes. Unsoundly shelves shook under our weight but he did not care about that nor about his fascinated friends who were watching us.`);
		if(mile.slut > 9){
			kat(`Go on! I want your dick inside me!`);
		}
		txt(`@mik grabbed the back of my thing and lifted my right leg. He rubbed the tip of his hard cock against my pussy and then resolutely entered me. When he was inside, he paused for a moment, just enjoying my snug pussy and the naughty lust-filled expression on my face. @mik seemed amazed and was gleefully smiling, almost like he was falling in love with me. But then he realized where we were and interrupted our almost intimate moment with a quip.`);
		mik(`Fuck! Her pussy feels good!`);
		npc(2, `You lucky bastard!`);
		if(mile.slut > 20){
			npc(3, `Isn't it too stretched? Considering her high mileage?`);
			mik(`Not at all!`);
		}
		txt(`He began thrusting, with his left hand clenching my thigh, with his right he was kneading my left tit. @mik was pounding my little pussy hard and it was definitely more fun for me than just sucking cocks. I would not mind being fucked by him even for free.`);
		
		link(`Oh yes! `, 212);
		link(`Fuck me harder!  `, 212);
		break;
		
		
	case 212: 	
		//TODO ORGAMS?
		//TODO CONDOMS? 
		emo("horny");
		counter.money_sex = Math.floor( (mile.chair_orgy_2 === 4 ? money.chair_bj_mid : money.chair_bj_low) * 2 );
		counter.money_sex_low = Math.floor( counter.money_sex * 0.74);
		
		mik(`OOhhOhHHH!`);
		txt(`@mik savagely groaned when he orgasmed. He leaned on me, squeezing my body between his and the cardboard boxes.`);
		mik(`That was good! That was so fucking good, @kat!`);
		txt(`He respectfully tapped my shoulder.`);
		kat(`I'm happy to please?`);
		txt(`I was awkward standing in the room, freshly fucked and leaking cum down my thigh, watched by four guys and casually chatting with them.`);
		npc(1, `I think it's my turn.`);
		txt(`@npc_1 stepped forward.`);
		npc(1, `I think I would prefer to fuck you too rather than a blowjob.`);
		txt(`I considered that. He was not as handsome as @mik and definitely did not have a reputation as an especially good lover. (And his dick was smaller.) But it would be rude to refuse him right after having sex with @mik right in front of him. And I was very horny...`);
		kat(`Okay.`);
		txt(`@npc_1 nervously smiled.`);
		npc(1, `Also... I kinda don't have ${counter.money_sex}. I have only ${counter.money_sex_low}. Maybe... if you wouldn't mind... just this time?`);
		kat(`Are you for real?`);
		npc(1, `Come on, @kat? Just this one time?`);
		
		con_link(mile.slut > 9, `low slut`, `Whatever, I just want you to fuck me! `, 214, ()=> mile.chair_orgy_cheap =  2);
		link(`Only this time! `, 214, ()=> mile.chair_orgy_cheap =  1);
		link(`Nope, you'll get only a blowjob! `, 213, ()=> mile.chair_orgy_cheap =  -1);
		break;
		
	case 213:
		emo("relax");
			
		//TODO - THIS IS TOO GOOD AND I WILL REPEAT IS ELSEWHERE
		
		npc(1, `That's so fucking unfair! I don't have that much money! I wish I could be a girl and just suck cocks for money.`);
		mik(`Maybe you could try gay bars?`);
		npc(1, `I'm straight! I wouldn't mind cocks if I was a girl!`);
		kat(`Stop fantasizing about sucking cocks and pull your pants down, I want to get over this!`);
		npc(1, `Oh, right.`);
		txt(`@npc_1 unzipped his pants. He was disappointed but I had to show him I was not a cheap whore he could easily exploit. I fixed the hair that was falling into my face and began giving head to another guy. Honestly, I was a bit tired of cocksucking and after getting properly fucked by @mik it felt like a letdown.`);
		txt(`Finally, I brought even @npc_1 to orgasm. They were about to leave and I have to remind him:`);
		kat(`What about the money?`);
		mik(`Of course.`);
		npc(1, `Pleasure doing business with you!`);

		{
			log_sex("bj", "npc_3");
			if(titjob){
				log_sex("boobs", "npc_2");
			}else{
				log_sex("bj", "npc_2");
			}
			log_sex("sex", "mik");
			log_sex("bj", "npc_1");
			const bj = (mile.chair_orgy_2 === 4 ? money.chair_bj_mid : money.chair_bj_low) ;
			payment( (bj * 3) + counter.money_sex, "cash"); 
		}
//TODO felt cold without clothes, 
//TODO cleenup?  - no water? leaking cum? 

		next();
		break;
		
	case 214: 
		if(mile.chair_orgy_cheap === 2){
			mile.slut++;
			txt(`I blurted maybe too eagerly. They began laughing:`);
			npc(3, `What a nympho slut!`);
			npc(2, `Why are we even bother paying her? She should be paying us!`);
		}else{
			npc(1, `Don't worry, the next time I'll pay you the full price.`);
			kat(`Fuck, I just realized I accidentally admitted there might be the next time too.`);
		}
		mik(`Why did I have to pay the full price?`);
		npc(1, `You have to learn how to negotiate!`);
		txt(`@npc_1 laughed and pushed me to face the wall. He positioned himself behind me and hugged me, his avaricious hands were squeezing my @tits and groping my body. I could feel his hot breath on my nape when he was positioning himself to penetrate me from behind.`);
		npc(1, `Oh yeah!`);
		txt(`He grunted right into my left ear when he stuffed my pussy with his dick.`);
		npc(1, `You'll love this, @kat!`);
		txt(`@npc_1 ensured me and began furiously thrusting. I suspected he did not so much care about my satisfaction but just did not want to look bad in comparison with @mik. But he was not even close, I can not articulate why sex with @mik felt better but just the energic mechanical motion was not enough.`);
		npc(2, `Oh yeah! Give her hell!`);
		npc(3, `Fuck her hard!`);
		txt(`His friends were even encouraging him. He quickly reached the peak of pleasure and climaxed too.`);
		npc(1, `You're such a good fuck, @kat!`);
		txt(`@npc_1 meanly and roughly slapped my ass before I turned back to them.`);
		kat(`The money?`);
		mik(`Of course.`);
		npc(1, `Pleasure doing business with you!`);
		
		{
			log_sex("bj", "npc_3");
			if(titjob){
				log_sex("boobs", "npc_2");
			}else{
				log_sex("bj", "npc_2");
			}
			log_sex("sex", "mik");
			log_sex("sex", "npc_1");
			const bj = (mile.chair_orgy_2 === 4 ? money.chair_bj_mid : money.chair_bj_low) ;
			payment( (bj * 2) + counter.money_sex + counter.money_sex_low, "cash"); 
		}
		next();
		break;
		
	}
}



/*
export const chair_orgy_2 = (index) => {
	const titjob = PC.getDim("breastSize") > 15;
	
	switch(index){
		default:
		case 101:
			present( //TODO - HARD SET NAMES
				[1, "Oliver", undefined, undefined],
				[2, "Tony", undefined, undefined],
				[3, "Igor", undefined, undefined],
			)
			emo("suspect"); //TODO
			placetime(`hallway`);
			
			txt(`During the break, I was surrounded by @mik and his friends.`);
			mik(`Hello, @kat! How are you doing?`);
			kat(`I'm doing fine?`);
			


 key to a storeroom on the third floor.
 
 
 
 //counter.money = mile.chair_orgy_2 === 4 ? money.chair_bj_mid : money.chair_bj_low
 
 
 
 */