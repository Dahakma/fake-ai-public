import {DISPLAY} from "Gui/gui";
import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat, ayy, qaa, qbb, qcc, eva, sas, anj, pet, mol, maj, tom, zan, ven} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s, capitalise} from "Loop/text";

import {INVENTORY, loot_multiple, wdesc, draw, effigy, wearRandomClothes, wears, edit, wearsPink, quickLoadOutfit, quickSaveOutfit, updateDraw, createWear, remove, showAll, showLess, crew} from "Avatar/index";

import {start as start_combat} from "Gui/combat/combat/combat";
import {combat} from "Gui/combat/combat/combat";
import {initiate_game_character} from "Virtual/initiate";
import {enter} from "Dungeon/engine";

import {fellowship} from "Virtual/index";
import {level_up} from "Virtual/level_up";
import {new_act} from "Virtual/index"; 
import {quest} from "Virtual/quest"; 

export function combat_simulator(input){
	switch(input){
		
		default:
		case 101:
			//set.initial(); //IMPORTANT TODO
			set.game();
			crew(`fBlandPaties`,`fBlandBra`); 
			
			txt(`Origin: 
				${choice("r_human", "human")} / ${choice("r_elf", "half-elf")} / ${choice("r_orc", "half-orc")} 
			`);
			txt(`Class: 
				${choice("c_harlot", "harlot")} / ${choice("c_hellion", "hellion")} / ${choice("c_fallen", "fallen priestess")} 
			`);	
			txt(`Bonus: 
				${choice("b_str", "strength")} / ${choice("b_dex", "dexterity")} / ${choice("b_int", "intelligence")} / ${choice("b_char", "charisma")}
			`);	
				
			effects([{
					id: `initial`,
					fce(){
						ext.race = 0;
						PC.apex.game_lips =  undefined;
						PC.apex.game_nails =  undefined;
						PC.apex.game_mascara_top =  undefined;
						PC.apex.game_mascara_bot =  undefined;
						
						//updateDraw(PC);
						crew(`fBlandPaties`,`fBlandBra`);
					},
					relative: {
						
					},
				},{
					id: `c_harlot`,
					fce(){
						ext.class = 1;
						mile.class = 1;
						
						PC.apex.game_lips =  `#b63326`;
						//PC.apex.game_nails =  undefined;
						PC.apex.game_mascara_top =  `black`;
						PC.apex.game_mascara_bot =  `black`;
						
						crew(`defHarlotSkirt`,`defHarlotTop`,`defHarlotCorset`,`defHarlotStockings`);
						//updateDraw(PC);
					},
					relative: {
						faceFem: 5,
						eyelashLength: 3,
						waistWidth: -3,
						hipWidth: 6,
						breastSize: 5,
						breastPerkiness: 2,
						legFem: 2,
						legFullness: 3,
					},
				},{
					id: `c_hellion`,
					fce(){
						ext.class = 2;
						mile.class = 2;
						
						crew(`defBarbaricBra`,`defBarbaricLoincloth`);
					},
					relative: {
						height: 1,
						upperMuscle: 4,
					},
				},{
					id: `c_fallen`,
					fce(){
						ext.class = 3;
						mile.class = 3;
						
						PC.apex.game_lips =  `#523b38`;
						PC.apex.game_nails =  `black`;
						PC.apex.game_mascara_top =  `black`;
						PC.apex.game_mascara_bot =  `black`;
						
						//updateDraw(PC);
						crew(`defFallenTop`,`defFallenLoincloth`);
					},
					relative: {
						eyelashLength: 2,
						irisSaturation: ext.race === 1 ? 20 : 20, //90-PC.Mods.irisSaturation,
						irisLightness: ext.race === 1 ? 10 : 70 - PC.Mods.irisLightness, 
						skinLightness: 3,
					},
				}]);
			
			effects([{
					id: `r_human`,
					fce(){
						ext.race = 0;
					},
					relative: {
					 
					},
				},{
					id: `r_elf`,
					fce(){
						ext.race = 1;
					},
					relative: {
						height: 3,
						earlobeLength:3,
						
						neckWidth: -4,
						armThickness: -8,
						waistWidth: -10,
						hipWidth: -8,
						legFullness: -8,
						
						cheekFullness: -10,
						faceFem: 20,
					
						irisSize:4,
						irisSaturation: 30, //90-PC.Mods.irisSaturation,
						irisLightness: 70 - PC.Mods.irisLightness,
						
						eyeTilt:5,
						eyeWidth:1,
						eyeTopSize:1,
						eyeBotSize:1,
						eyeSize:2, 
						
						skinSaturation: 10,
					},
				},{
					id: `r_orc`,
					fce(){
						ext.race = 2;
					},
					relative: {
						height:-1,
						armLength:7,//?
						handSize:30,
						upperMuscle: 21 - PC.basedim.upperMuscle,
						
						legFullness:24,
						hipWidth:6,
						
						neckWidth:2,
						
						earlobeLength:0.5,
						faceWidth:1,
						
						
						browOutBias:2,
						browThickness:2,
						eyeTilt:-3,
						limbalRingSize:10,

						noseHeight:3,
						noseLength:-12,
						noseWidth:5,
						nostrilSize:5,
						
						//skinHue:80,
						skinSaturation:-10,
					},
			}]);
			
			effects([{
					id: `b_str`,
					fce(){
						ext.origin = 1;	
					},
				},{
					id: `b_dex`,
					fce(){
						ext.origin = 2;	
					},
				},{
					id: `b_int`,
					fce(){
						ext.origin = 3;	
					},
				},{
					id: `b_char`,
					fce(){
						ext.origin = 4;	
					},
			}]);
			
			link(`Campaign. `, 300, initiate_game_character);
		//	link(`Test. `, 200, initiate_game_character);
			break;	
		
		case 99:
//link(`Level up `, 200, ()=>{}, ()=> level_up(200) );
level_up(`Level up `, 200, true);			
link(`Back. `, 200);
			break;	
			
		case 200:
			set.game();
			
			link(`Dummies `, 200, ()=>{}, ()=> start_combat([12, "tough_dummy"]) );
			
			
			
	
link(`Goblins. `, 200, ()=>{}, ()=> start_combat([[2,4], "goblin"]) );
link(`Ogre. `, 200, ()=>{}, ()=> start_combat("ogre") );

level_up(`Level up `, 200, true);			
			

			link(`Fight rat. `, 200, ()=>{}, ()=> start_combat("rat") );
			link(`Rats. `, 200, ()=>{}, ()=> start_combat([[2,4], "rat"]) );
			link(`Big rat. `, 200, ()=>{}, ()=> start_combat("rous") );
			link(`Loot. `, 201);
			
			link(`TestShop. `, null, ()=> INVENTORY.shop( "testShop", ()=> main(200) )   );
			
			link(`Follower: Yellaya. `, 310, ()=> fellowship("yel") );
			
			enter(`Rat extermination. `, "cellar", 200, 200);
			
			break;
		
		case 201:
			loot_multiple([3, "weapon"],[3, "armor"],[3, "clothes"],[3, "elixir"],[1, "legendary"]);
			link(`Back. `, 200);
			break;
			
		
//INTRO
		case 300:
			set.game();
			txt(`Cellar (tutorial)`);
			link(`Fight rat. `, 300, ()=>{}, ()=> combat("rat") );
			link(`Rats. `, 300, ()=>{}, ()=> combat({rat: [2,4]}) );
			link(`Big rat. `, 300, ()=>{}, ()=> combat("rous") );
			//link(`TestShop. `, null, ()=> INVENTORY.shop( "testShop", ()=> main(200) )   );
			chapter(`(Intro) `, `character_creation`, 105);	
			enter(`Rat extermination. `, "cellar", 300, 300);
			link(`>>>Feyd Mora. `, 400);
			break;

//FEYD
		case 400:
			set.game();
			txt(`Feyd Mora`);
			level_up(`Level up `, 400, true);			
			//link(`Level up `, 400, ()=>{}, ()=> level_up(400) );	
			link(`Follower: Yellaya. `, 410);
			link(`Goblins. `, 400, ()=>{}, ()=> combat({goblin: [2,4]}) );
			link(`Ogre. `, 400, ()=>{}, ()=> combat("ogre") );
			link(`Beast. `, 400, ()=>{}, ()=> combat("y_beast") );
			link(`Loot. `, 401);
			link(`Shop. `, null, ()=> INVENTORY.shop( "feyd", ()=> main(400) )   );
			chapter(`(Feyd Mora) `, `feyd`, 101);
			enter(`Explore woods. `, `feyd_roses`, 400); 
			enter(`Yelayas quest `, `yelaya`, 400); 
			link(`>>>Gothred. `, 500);
			//enter(`Rat extermination. `, "cellar", 200, 200);
			break;
			
		case 410:
			txt(`Yellaya joined you. `);
			fellowship("yel");
			link(`Back. `, 400);
			break;
		
		case 401:
			loot_multiple([3, "weapon"],[3, "armor"],[3, "clothes"],[3, "elixir"],[1, "legendary"]);
			link(`Back. `, 400);
			break;
		
//GOTHRED 1
		case 500:
			new_act(2);
			set.game();
			txt(`Voyage & Gothred`);
			//link(`Level up `, 500, ()=>{}, ()=> level_up(500) );
			level_up(`Level up `, 500, true);			
			link(`Goblins. `, 500, ()=>{}, ()=> combat({goblin: [3,6]}) );
			link(`Ogre. `, 500, ()=>{}, ()=> combat("ogre") );
			
			
			link(`Pirates. `, 500, ()=>{}, ()=> combat({pirate: [4,4]}) );
			link(`Strong Pirate. `, 500, ()=>{}, ()=> combat({strong_pirate: [1,1]}) );
			link(`Fast Pirate. `, 500, ()=>{}, ()=> combat({fast_pirate: [1,1]}) );
			link(`Small pirate group. `, 500, ()=>{}, ()=> combat({
				pirate: [2,3],
				fast_pirate: [0,1],
				strong_pirate: [0,1],
			}) );
			link(`Large pirate group. `, 500, ()=>{}, ()=> combat({
				pirate: [2,4],
				fast_pirate: [0,2],
				strong_pirate: [0,2],
			}) );
			link(`Chosen. `, 500, ()=>{}, ()=> combat({chosen: 1}) );
			link(`Wraiths. `, 500, ()=>{}, ()=> combat({wraith: 5}) );
			link(`Spiders. `, 500, ()=>{}, ()=> combat({spider: 6}) );
			
			link(`Draugr. `, 500, ()=>{}, ()=> combat({draugr: 1}) );
			link(`Draugrs. `, 500, ()=>{}, ()=> combat({draugr: 2}) );
			
			link(`Follower. `, 510);	
			link(`Loot. `, 501);
			link(`Shop. `, null, ()=> INVENTORY.shop( "goth", ()=> main(500) )   );
			/*
			chapter(`(Feyd Mora) `, `feyd`, 101);
			enter(`Explore woods. `, `feyd_roses`, 300); 
			*/
			enter(`Pirate ship `, `ship`, 500); 
			
			quest.initiate("ghost_ship", "Explore mysterious ship <i>Barnham Dyn. </i>", "reach");
			enter(`Ghost ship `, `ghost_ship`, 500, 500, `ghost_ship`);
			enter(`Shipwreck `, `shipwreck`, 500);
			quest.initiate(`karged`, `Explore the Tombs of Karged and find the Sceptre of Torrywialen`, "reach");
			enter(`Karged. `, `karged`, 500, 500, `karged`); 
			quest.initiate(`oskyl`, `Explore the Labyrinth of Oskyl and find the Sceptre of Torrywialen`, "reach");
			enter(`Oskyl. `, `oskyl`, 500, 500,`oskyl`); 
			enter(`Oskyl 2. `, `flooded_oskyl`, 500, 500); 
			//enter(`Rat extermination. `, "cellar", 200, 200);
			break;

		case 501:
			loot_multiple([3, "weapon"],[3, "armor"],[3, "clothes"],[3, "elixir"],[1, "legendary"]);
			link(`Back. `, 500);
			break;
		
		/*
		case 502:
			quest.initiate("ghost_ship", "Explore mysterious ship <i>Barnham Dyn. </i>", "reach");
			link(`Back. `, 500);
			break;
			*/
			
		case 510:
			link(`Galawar. `, 500, ()=> fellowship("gal") );
			link(`Stede. `, 500, ()=> fellowship("ste") );
			link(`Lyssa. `, 500, ()=> fellowship("lys") );
			link(`Sirael. `, 500, ()=> fellowship("sir") );
			link(`Back. `, 500);
			break;
			
			
			
	}
}