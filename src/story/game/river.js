/**
	the virtual game story set between GOTHRED and WESTGARD
	act: 3
	follows: goth
	precedes: west
	
*/

import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";

import {txt, npc, kat, fol} from "Loop/text";
import {gal, ste, lys, sir} from "Loop/text";
import {bold, hint, and, s} from "Loop/text";

import {clone, shuffle, clamp, pm} from "Libraries/dh";
import {ra} from "Libraries/random";

import {createWear, crew, quickSaveOutfit, quickLoadOutfit} from "Avatar/index";
import {updateDraw, updateMods, sluttiness, clothes, showLess, showAll, wears, remove, effigy, removeEverything } from "Avatar/index";

import {autosave, snapshot} from "System/save";
import {quest} from "Virtual/quest"; 
import {class_based, roll, dance, fellowship, BARS} from "Virtual/index";
import {new_act} from "Virtual/index"; 
import {rewards} from "Data/items/prices"; 

import {wage, We, we, fol_name, he, him, his, He, dyke} from "Virtual/text";
import {whoring_initial, whore_haggling, whore_bj} from "./whore";  //TODO

import {enter} from "Dungeon/engine";


function dance_wage(sum){
	sum = Math.round( sum * dance() );
	txt(`Based on my dexterity and charisma bonuses I earned ${sum} coins. `);
	wage(sum);
}




export const goth = (index) => {
	switch(index){
		case 101: 
	
	
	}
}



