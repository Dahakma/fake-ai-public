import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat} from "Loop/text";
import {qaa, qbb, qcc} from "Loop/text";
import {yel} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s} from "Loop/text";
import {placetime} from "Loop/text_extra";
import {DISPLAY, popup} from "Gui/gui";

import {crew, updateOriginalMods, quickLoadOutfit, quickTempOutfit, INVENTORY, updateDraw, updateMods, sluttiness, showLess, showAll} from "Avatar/index";
import {clone, shuffle, number2word, /*an,*/ decapitalise} from "Libraries/dh";  
const an = a => a; //TODO

import {ra} from "Libraries/random";
import {roll, boost, initiate_game_character, class_based, stupid_names, character_origin, new_act, fellowship, dismiss} from "Virtual/index";

import {enter} from "Dungeon/engine";
import {autosave, snapshot} from "System/save";
import {quest} from "Virtual/quest"; 
import {rewards} from "Data/items/prices"; 
import {wage} from "Virtual/text";
import {level_up} from "Virtual/level_up"; 

//const rats_reward = 6;
//const roses_reward = 6;
const roses_number = 4;


			
export const character_creation = (index) => {
	switch(index){
	default:
	case 101: 
		placetime(`metaverse`, `sunday`);
		autosave(/*name*/); //could be done in more effective wayy
			
		//clears everything (to recreatethe character again);
		ext.race = 0;
		PC.apex.game_lips =  undefined;
		PC.apex.game_nails =  undefined;
		PC.apex.game_mascara_top =  undefined;
		PC.apex.game_mascara_bot =  undefined;
		
		PC.apex.game_tat_faceLeft =  undefined;
		PC.apex.game_tat_sleeve = undefined;
		PC.apex.game_tat_thighLeft = undefined;
		
		set.game(); 
		ext.game = {};
		updateOriginalMods(PC, {Mods: ext.meta});
		
		

	
		//attributes.hide(); //they are not set yet
		crew(`fBlandPaties`, `fBlandBra`);
			
		qcc(`We need to create her game character. `);
		qaa(`What race she should be?`);
		//kat(`I'm okay being white.`)
		qcc(`Beletriad is inhabited by several fantasy races. And ${choice("r_human", "humans")} are too vanilla and boring. `); //, even despite their intelligence bonus. `);
		qaa(`She should be a ${choice("r_elf", "half-elf")}. Ethereal and beautiful. `); //ASAP
		qbb(`Or a ${choice("r_orc", "half-orc")}. `);
		qaa(`Not a half-orc. They are too brutish for her. `);
		//qbb(`But they have great combat bonuses. `);
			
		effects([{
			id: `r_human`,
			fce(){
				ext.race = 0;
			},
			relative: {
				 
			},
		}, {
			id: `r_elf`,
			fce(){
				ext.race = 1;
			},
			relative: {
				height: 3,
				earlobeLength:3,
					
				neckWidth: -4,
				armThickness: -8,
				waistWidth: -10,
				hipWidth: -8,
				legFullness: -8,
					
				cheekFullness: -10,
				faceFem: 20,
				
				irisSize:4,
				irisSaturation: 30, //90-PC.Mods.irisSaturation,
				irisLightness: 70 - PC.Mods.irisLightness,
					
				eyeTilt:5,
				eyeWidth:1,
				eyeTopSize:1,
				eyeBotSize:1,
				eyeSize:2, 
					
				skinSaturation: 10,
			},
		}, {
			id: `r_orc`,
			fce(){
				ext.race = 2;
			},
			relative: {
				height:-1,
				armLength:7, //?
				handSize:30,
				upperMuscle: 21 - PC.basedim.upperMuscle,
					
				legFullness:24,
				hipWidth:6,
					
				neckWidth:2,
					
				earlobeLength:0.5,
				faceWidth:1,
					
					
				browOutBias:2,
				browThickness:2,
				eyeTilt:-3,
				limbalRingSize:10,

				noseHeight:3,
				noseLength:-12,
				noseWidth:5,
				nostrilSize:5,
					
				//skinHue:80,
				skinSaturation:-10,
			},
		}]);
					
		link(`Next. `, 102);
		break;
			
	case 102: 	
		qcc(`The most important is her class. `);
		qbb(`Obvious choice would be the ${choice("c_harlot", "Harlot")}. `);
		kat(`I don't want to be a harlot. `);
		qaa(`Actually that isn't a bad choice. `);
		qcc(`With the right build, the harlot can have the highest charisma in the game. `);
		qaa(`But for the beginer she isn't very strong in combat. `);
		qcc(`At least not directly. `);
		//	kat(`Isn't there something else?`);
		qbb(`The best combat class is, of course, the ${choice("c_hellion", "Hellion")}. `);
		kat(`What the hell is a hellion?`);
		qcc(`She is brave, ruthless and very sexually liberated pagan warrior. `);
		kat(`Is there any... less gratuitous class? `);
		qaa(`...`);
		qcc(`${choice("c_fallen", "Fallen Priestess")}?`);
		kat(`Why is she fallen? Oh wait, I can imagine that. `);
		qcc(`She fornicated with dark powers. `);
		qaa(`Literally. `);


		effects([{
			id: `initial`,
			fce(){
				ext.race = 0;
				PC.apex.game_lips =  undefined;
				PC.apex.game_nails =  undefined;
				PC.apex.game_mascara_top =  undefined;
				PC.apex.game_mascara_bot =  undefined;
				
				PC.apex.game_tat_faceLeft =  undefined;
				PC.apex.game_tat_sleeve = undefined;
				PC.apex.game_tat_thighLeft = undefined;


				//updateDraw(PC);
				//crew(`fBlandPaties`, `fBlandBra`);
				//quickLoadOutfit();
				quickTempOutfit([`fBlandPaties`, `fBlandBra`]);
			},
			relative: {
					
			},
		}, {
			id: `c_harlot`,
			fce(){
				ext.class = 1;
				mile.class = 1;
					
				PC.apex.game_lips =  `#b63326`;
				//PC.apex.game_nails =  undefined;
				PC.apex.game_mascara_top =  `black`;
				PC.apex.game_mascara_bot =  `black`;
					
				quickTempOutfit([`defHarlotSkirt`, `defHarlotTop`, `defHarlotCorset`, `defHarlotStockings`]);
				//updateDraw(PC);
			},
			relative: {
				faceFem: 5,
				eyelashLength: 3,
				waistWidth: -3,
				hipWidth: 6,
				breastSize: 5,
				breastPerkiness: 2,
				legFem: 2,
				legFullness: 3,
			},
		}, {
			id: `c_hellion`,
			fce(){
				ext.class = 2;
				mile.class = 2;
				
				PC.apex.game_tat_faceLeft = "triangle";
				PC.apex.game_tat_sleeve = "dragonBlue;0;0;2";
				PC.apex.game_tat_thighLeft = "blueBand2;0;-25;2";

				quickTempOutfit([`defBarbaricBra`, `defBarbaricLoincloth`]);
			},
			relative: {
				height: ext.race === 1 ? 0 : 1,
				upperMuscle: ext.race == 2 ? 4 : 12,
			},
		}, {
			id: `c_fallen`,
			fce(){
				ext.class = 3;
				mile.class = 3;
					
				PC.apex.game_lips =  `#523b38`;
				PC.apex.game_nails =  `black`;
				PC.apex.game_mascara_top =  `black`;
				PC.apex.game_mascara_bot =  `black`;
					
				//updateDraw(PC);
				quickTempOutfit([`defFallenTop`, `defFallenLoincloth`]);
			},
			relative: {
				eyelashLength: 2,
				irisSaturation: ext.race === 1 ? 20 : 20, //90-PC.Mods.irisSaturation,
				irisLightness: ext.race === 1 ? 10 : 70 - PC.Mods.irisLightness, 
				skinLightness: 3,
			},
		}]);

		link(`Next. `, 103);
		break;
			
			

	case 103:
		qaa(`Finally, you can pick a backstory and your origin. `);
		switch(ext.class){
		default: txt("Error"); break;
		case 1:
			qcc(`You can be a ${choice("b_char", "noblewoman")} whose land got invaded. Her father was deposed and the whole family killed. The blackguard who saved her r${String.fromCharCode(97)}ped her and then brought her to a home of a crime boss. There she was trained to be a rogue, a dancer and a prostitute. `);
			kat(`That's so horrible! `);
			qaa(`She has bonus charisma. `);
			qcc(`Then there is a ${choice("b_int", "runaway")}. She used to be a smart promising girl from a good family, but too romantic and naive. She fell for a wrong man and ran away with him. But he was an abusive scoundrel who made her sleep with other men for money. He eventually left her but her family renounced her and she had no other choice than to continue the life she hated. `);
			qaa(`She has bonus intelligence. `);
			qcc(`Or you can be an ${choice("b_dex", "urchin")}, who doesn't even remember her parents and was forced to survive on harsh streets. From the very young age she was making money by stealing and when she got older by selling her body. `);  
			qaa(`She has bonus dexterity. `);
			qcc(`Finally there is a ${choice("b_str", "camp follower")}. She was making living by following an army, sleeping with soldiers and doing all sorts of odd jobs. She learned to deal with harsh men and was quite content but one day her mercenary company was defeated by orcs, all her friends and lovers either killed in battle or executed later and she was barely able to escape from orc slavery. `);  
			qaa(`She has bonus strength. `);
			break;
					
		case 2:
			qcc(`You can be a ${choice("b_char", "chief's daughter")}. She was forced to marry a chief of a different tribe but she despised him because he was harsh and cruel and ran away. `);
			qaa(`She has bonus charisma. `);
			qcc(`Then there is a ${choice("b_int", "shaman")}. From a young age she was trained in arts of divination, communication with spirits and sacred sexual magic. But when the bad luck and plague hit, the chief used her as a scapegoat. She should be brutally ritually sacrificed, but she managed to escape. `);       
			qaa(`She has bonus intelligence. `);
			qcc(`Or you can be a ${choice("b_dex", "huntress")}. She was an unbeatable tracker and ranger but she was also very proud and selfish. One fateful day she killed a young manticore. The parent of the manticore attacked her home village and killed several people including her younger s${String.fromCharCode(105)}ster. She was then banished. `);       
			qaa(`She has bonus dexterity. `);
			qcc(`Finally there is a ${choice("b_str", "berserk")}. She was the best and the most ferocious fighter in her tribe but then she haughtily refused sexual advances of several young men. They attacked her while she was bathing alone in a lake but she single-handily beat and humiliated them all. Unfortunately she accidentally killed the chief's son and was forced to leave. `);  
			qaa(`She has bonus strength. `);
			break;
					
		case 3:
			qcc(`You can be the ${choice("b_int", "librarian")}. For long she studied curious volumes of forgotten lore. She was seeking knowledge but she eventually dug too deep and her mind was corrupted. She invoked demons to satisfy her curiosity and lust. When it was discovered that she possessed prohibited tomes, she had to escape and survive in the real world. ` );
			qaa(`She has bonus intelligence. `);
			qcc(`Or you can be the ${choice("b_char", "herald")}. She traveled far and wide spreading proclamations of the Order. She was lavishly welcomed everywhere and she started to more care about her debaucherous lifestyle than her mission and eventually she became venal and betrayed her duty for material gain. `);
			qaa(`She has bonus charisma. `);
			qcc(`Or you can be the ${choice("b_str", "paladin")}. She strived to be the warrior of light and eradicate the evil. However she was too proud and reckless. She was defeated, captured and broken. `);
			qaa(`She has bonus strength. `);
			qcc(`Finally there is the ${choice("b_dex", "hospitaller")} Not the brightest or strongest but a diligent and faithful worker. Until she was convinced by her superior to partake in twisted medical experiments. `);  
			qaa(`She has bonus dexterity. `);
			
		}
			
		kat(`No happy, well-adjusted person?`);
		qbb(`Why would a happy, well-adjusted person became an adventurer? `);
		kat(`But they are all awful and depressing. `);
		qaa(`Such is the life in Beletriad. `);
		qbb(`Especially since the Second orc invasion and the Nemetian plague. `);
		qcc(`Not only the plague killed many people overall, it especially hit hard the rich coastal cities like Sintre or Gothred, totally crashing the ambrosia trade and the whole economy... `);
		qaa(`Well, technically Gothred isn't <i>de iure</i> part of Beletriad... `);
		qcc(`It is in any practical sense! `);
		qbb(`Only thick crown loyalists actually believe that! `);
		link(`Okay, okay, whatever! `, 104);
			
			
		effects([{
			id: `b_str`,
			fce(){
				ext.origin = 1;	
			},
			relative: {
			},
		}, {
			id: `b_dex`,
			fce(){
				ext.origin = 2;	
			},
			relative: {
			},
		}, {
			id: `b_int`,
			fce(){
				ext.origin = 3;	
			},
		}, {
			id: `b_char`,
			fce(){
				ext.origin = 4;	
			},
		}]);
		break;

			
	case 104:
		//set.game(); //???
		initiate_game_character();
		set.game();
		qaa(`The total charisma of your character is ${PC.att.char}, strength ${PC.att.str}, intelligence ${PC.att.int} and dexterity  ${PC.att.dex}. `);
		kat(`That's good, isn't it? `);
		qcc(`Not great, not horrible. `);
		qaa(`Are you happy with your character? `);
		kat(`I have no idea! Should I be happy? `);
		qcc(`Depends on your personal preferences. `);
		qaa(`Can we continue? `);
			
		link(`Yeah! `, 105);
		link(`Dunno, could we start over? `, 101);
		break;
			
	
	case 105:
		new_act(1);
		set.game();
			
		{
			const stupid = stupid_names(7);
			counter.npca = stupid.map( a => {return {
				name: a,
				visible: false,
			}});
		}
		counter.npca[0].result = true;
		mile.feyd_keryk_name = counter.npca[0].name;
		counter.npca = shuffle(counter.npca);
		mile.feyd_keryk_attempts = 0;
		mile.feyd_keryk = false;
		mile.feyd_rats = false;
		mile.game_progress = 0;
			
		present(
			[1, `Guard`, `guard`],
			[2, `Innkeeper`, `commoner`],
			[3, mile.feyd_keryk_name, `commoner`], //TODO
			[4, `Maid`, `maid`],
			[5, `Noblewoman`, `noblewoman`],
		);
	
		qcc(`The first act of the game...  `);
		kat(`There are more acts than one?!?`);
		qbb(`Yeah, several!`);
		txt(`@qbb smirked while @qcc impatiently continued: `);
		qcc(`The story of the game is set in another time and another land where ancient gods are petty and cruel, warlords are struggling for power, forgotten beasts and titans are roaming the land and a courageous hero - or in your case heroine - can change the world. It takes place in the Kingdom of Beletriad during very tumultuous times. The interregnum...`);
		qaa(`Interregnum means there is no king, the old king died but there is no new one. Sorry for the interruption, I just wanted to explain it because @kat might not get it and be confused. `);
		qcc(`Yeah, I was about to explain it. So since king Rawik died...`);
		txt(`I was pretending I was listening. I could not care less about the lore of another genetic fantasy magical realm. `);
		kat(`Okay, but what I need to do to win?`);
		qaa(`...`);
		qcc(`...`);
		qbb(`...`);
		qcc(`You obviously need to defeat the Evil Overlord! `);
		kat(`How?`);
		qaa(`...`);
		qcc(`...`);
		qbb(`...`);
		qcc(`That's the point of the game! `);
			
		 
		qaa(`I can give you a hint - you should talk with ${mile.feyd_keryk_name}. Don't you want to write it down?`);
		kat(`Do I look like I'm stupid? I won't forget such a dumb name! `);
		//	kat(`Do I look like I'm stupid? I won't forget a dumb name like ${choice(`placeholder`,`name_2`)}. `);
		qaa(`Okay.`);
			
		if(SETTING.no_virtual_game){
			chapter(`Start the game.  `, `skip_feyd_intro`);
		}else{
			link(`Start the game. `, 110);
			if(softbug) chapter(`#Skip the game. `, `skip_feyd_intro`);
		}
		//link(dynamic(`name_2`),101);
		break;	

		
	case 110:
		placetime(`Feyd Mora`, null);
		txt(`I materialized on the mains square of a medievally looking port. I could feel the salt in the air and the whole scenery was far more vivid then I imagined. `);
		hint(`To progress you need to solve the first quest and find the right guy. `);
		link(`Tavern. `, 120);
		link(`Guard. `, 134);
		link(`Noblewoman. `, 131);
		link(`Maid. `, 135);
			
		//TODO SHOULD BE IN EFFECT 
		if(!mile.feyd_keryk){
			let shifted = counter.npca.map( a => a );
			shifted = shuffle(shifted); //TODO
				
			shifted.forEach( a => {
				if(a.visible){
					if(a.result){
						link(a.name, 141);
					}else{
						link(a.name, 140);
					}
				}
			});
		}else if(mile.feyd_rats){
			link(`Exit the Game. `, 150);
		}
		
		//if(debug)link(`##Skip the rat quest. `,101,()=>{mile.dun_rats=true} );
		if(softbug){
			if(!mile.feyd_keryk) link(`#Find the right guy. `, 141);
			if(!mile.feyd_rats) {link(`#Finish the initial quest. `, 110, () => {
				ext.game_money += rewards.feyd_rats;
				mile.feyd_rats = true;
				//TODO - WEAPON & POTION?
			});}
		}
			
		break;
		
	case 120:
		placetime(`Tavern`, null);
		txt(`The tavern was filled with various shady existence, some were playing dices, other were just drinking. My character fitted there well. `);
		if(!mile.feyd_rats) link(`Look for work. `, 112);
		link(`Ask about the Dark Overlord. `, 111);
		link(`Back. `, 110);
		break;
		
	case 111:
		txt(`I stood up and yelled: `);	 
		kat(`Hey? Does any of you dumb npc's know anything about the Dark Overlord, how to defeat him or even find him? He might or might not be a chaos wizard? `);
		txt(`However, the room only gave me weird looks and nobody was willing to share any information with me. `);
		link(`Well, at least I tried. `, 120);
		break;
			
	case 112:
		if(Math.random()<0.5){
			npc(2, `Welcome to the harbor of Feyd Mora, traveler! `);
		}else{
			npc(2, `Welcome to the kingdom of Beletriad!	 `);
		}
		switch(ext.class){
		default: txt("Error"); break;
		case 1: 
			kat(`Hey! Innkeeper! Is there any job for an adventurer? I need money fast because I need to finish this stupid game ASAP, if you know what I mean. `);
			break;
		case 2: 
			kat(`Hey! Innkeeper! Is there any job for a barbarian, or whatever is my character supposed to be? I need money fast because I need to finish this stupid game ASAP, if you know what I mean. `);
			break;
		case 3: 
			kat(`Hey! Innkeeper! Is there any job for a sorceress, or whatever is my character supposed to be? I need money fast because I need to finish this stupid game ASAP, if you know what I mean. `);
			break;
		}
		npc(2, `I don't know what you mean. However, my wine cellar is infested with rats. I'd pay you ${rewards.feyd_rats} coins if you exterminate them. `);
		kat(`Rats? Wow, you people are so useless! Where is the cellar? `);
			
		quest.initiate("feyd_rats", "Kill all rats in the cellar", "eliminate");
		//link(`Rat extermination. `,null, () => DUNGEON.start(`cellar`,`feyd`,113,113)  );
			
		enter(`Rat extermination. `, "cellar", 113, 114, "feyd_rats");
			
		link(`Maybe some other time. `, 120);
		break;
		
	case 113:
		set.game();			
		present(
			[1, `Guard`, `guard`],
			[2, `Innkeeper`, `commoner`],
			[3, mile.feyd_keryk_name, `commoner`], //TODO
			[4, `Maid`, `maid`],
			[5, `Noblewoman`, `noblewoman`],
		);		
		mile.feyd_rats = true;
		quest.closure(`feyd_rats`);
		npc(1, `Thank you adventurer! I'm very grateful! `);
		kat(`You're welcome. And my money? `);
		wage(rewards.feyd_rats);
		level_up(`Tavern. `, 120); //to level 2 
		break;
			
	case 114:
		set.game();			
		present(
			[1, `Guard`, `guard`],
			[2, `Innkeeper`],
			[3, mile.feyd_keryk_name],
			[4, `Maid`, `maid`],
			[5, `Noblewoman`, `noblewoman`],
		);	
		kat(`I'm back! `);
		npc(1, `You didn't exterminate all the rats.  `);
		kat(`How do you know? `);
		npc(1, `...I know! `);
			
		enter(`I'll try again! `, "cellar", 113, 114, "rats");
		link(`Tavern. `, 120);
		break;
		
	case 131:
		txt(`The @npc_5 did not seem thrilled to talk with a pleb like me. I quickly asked him about my contact. `);
		npc(5, `Do you mean ${counter.npca[0].name} or ${counter.npca[1].name}? Or even ${counter.npca[2].name}? It is a really common name here. `);
		counter.npca[0].visible = true;
		counter.npca[1].visible = true;
		counter.npca[2].visible = true;
		link(`Back. `, 110);
		break;
			
	case 134:
		if(Math.random()<0.5){
			npc(1, `Welcome to the harbor of Feyd Mora, traveler! `);
		}else{
			npc(1, `Welcome to the kingdom of Beletriad!	 `);
		}
		txt(`The @npc_1 seemed like somebody who was supposed to help citizens but really did not enjoy it. `);
		npc(1, `Do you mean ${counter.npca[3].name} or ${counter.npca[4].name}? I hope you don't mean that scoundrel ${counter.npca[5].name}? `);
		counter.npca[3].visible=true;
		counter.npca[4].visible=true;
		counter.npca[5].visible=true;
		link(`Back. `, 110);
		break;
		
	case 135:
		if(Math.random()<0.5){
			npc(4, `Welcome to the harbor of Feyd Mora, traveler! `);
		}else{
			npc(4, `Welcome to the kingdom of Beletriad!	 `);
		}
		txt(`The @npc_4 smiled at me. `);
		npc(4, `I only know ${counter.npca[6].name}. Is he the guy you are looking for? `);
		counter.npca[6].visible=true;
		link(`Back. `, 110);
		break;
			
	case 140:
		mile.feyd_keryk_attempts++;
		switch(ra.integer(0, 4)){
		default:
		case 0: 
			txt(`The guy did not know anything and really did not want to talk with me about the Evil Overlord. `);
			break;
		case 1: 
			txt(`The guy did not even care what I was saying. He only made some sleazy quips and tried to flirt with me. `);
			break;
		case 2: 
			txt(`The guy looked very shady. Even before I finished he told me he will help me. I only need to follow him to a dark alley because he wants to show me something. I declined. `);
			break;
		case 3: 
			txt(`The guy told me he was very busy and he had no time for ${class_based("whores.", "barbarians.", "renegades.")}`); 
			break;
		}
		link(`Back. `, 110);
		break;	
		
	case 141:
		mile.feyd_keryk = true;
		npc(3, `Welcome to the harbor of Feyd Mora, traveler! `);
		txt(`Oh not again.`); 
		npc(3, `You are the girl I heard about?`);
		kat(`Yep! That's me! Can you help me?!`);
		switch(ext.class){
		default: txt("Error (ext.class)"); break;
		case 2:
			npc(3, `I imagined the Chosen One a bit differently. But fear not! In this dark time even shameless savage like you can change the fate of the world. `);
			kat(`Shameless savage?`);
			npc(3, `Yes, a blood-thirsty barbarian who knows nothing about manners or decency who lies with men and women alike following only her low insatiable perverted urges.`);
			break;
		case 3:
			npc(3, `I imagined the Chosen one a bit differently. But fear not! In this dark time even depraved profligate like you can change the fate of the world. `);
			kat(`Depraved profligate?`);
			npc(3, `Yes, a lewd woman who proved she more cares about low, sinful desires of flesh than upholding her sacred duty.`);
			break;
		case 1:
			npc(3, `I imagined the Chosen one a bit differently. But fear not! In this time even wanton tramp like you can change the fate of the world.	 `);
			kat(`Wanton tramp?`);
			npc(3, `Yes, a shameless woman who does not care about decency and her good name, ready to lie with anybody willing to spend a few coins for her lewd body.	`);
			break;
		}
		kat(`Yeah, I got it, I'm a whore. How to defeat the Evil Overlord? `);
		npc(3, `I have no idea.`);
		kat(`...`);
		npc(3, `...`);
		kat(`Can you at least point me to somebody who does? `);
		npc(3, `Of course.`);
		kat(`Fucking awesome. `);
		npc(3, `You have to take a ship to Gothred. Don't worry about the price, I arranged everything with the captain. `);
		link(`Great. `, 142);
		break;
			
	case 142:
		kat(`Great. What should I do once I arrive to Gothred, whatever that is? `);
		npc(3, `You have to talk with Arazor, a wizard who lives there. He was exiled from Beletriad for his radical opinions and twisted magical experiments . `);
		kat(`Radical opinions and twisted magical experiments  `);
		npc(3, `He claimed the king and the aristocracy should be deposed and the state should be ruled by an elected council. `);
		link(`That sounds reasonable. `, 143);
		link(`What a crazy commie!`, 144);
		break;
			
	case 143:
		txt(`He frowned.`);
		npc(3, `I see. I guess you'll get along. The ship will sail out as soon as they will finish the loading. It may take them several days. `);
		link(`Fine, I'll wait. `, 110);
		break;
			
	case 144:
		npc(3, `Exactly! But be careful and don't argue with him. The ship will sail out as soon as they will finish the loading. It may take them several days. `);
		link(`Fine, I'll wait. `, 110);
		break;	
			
		
	case 150:
		counter = {}; // !!
		set.irl();
		autosave(/*name*/); //could be done in more effective wayy
			
		txt(`So ended my first session in the game. It lasted more than two hours and I did not even leave the starting town. `);
		//txt(`I started to suspect I made a huge mistake when I agreed with their twisted scheme. `);

		next(`At least the Game was quite fun. `, () => mile.immersion++);
		next(`I have to finish the Game as soon as possible. `, () => mile.immersion++);
		next(`I already knew I will hate the Game. `, () => mile.immersion++);
		break;
			
	}
	
};



export const feyd = (index) => {
	switch(index){
		
	case 101: 
		if(!mile.game_progress) mile.game_progress = 10; 
		set.reentry("feyd");
		set.game();
		present(
			[1, `Guard`, `guard`],
			[2, `Captain`, `commoner`],
			[3,  `Ryfin`, `commoner`],
			[4, `Maid`, `maid`],
			[5, `Noblewoman`, `noblewoman`],
		);
		//break; //SIC!
		
	default:
	case 110:
		placetime("Feyd Mora", "");
		link(`Tavern. `, 200);
		link(`Marketplace. `, 300);
		
		//smuggler
		if(!mile.feyd_smuggler) link(`Guard. `,400);
				
		//roses
		link(`Noblewoman. `, 510);
		if(mile.feyd_roses  <= 1) link(`Maid. `, 500);
		if(mile.feyd_roses === 1) enter(`Explore woods. `, `feyd_roses`, 101, 101, `feyd_roses`); 
			
		//yelaya
		if(mile.feyd_yelaya === 10){
			//storyline finished
		}else if(mile.feyd_yelaya_saved === 1){
			link(`Yelaya. `, 620);
		}else if(mile.feyd_yelaya_saved === -1){
			//yelaya dead
		}else if(mile.feyd_yelaya > 0){
			link(`Yelaya. `, 602);
		}else{
			link(`Woman. `, 600);
		}
		
		//gothred
		if(mile.feyd_yelaya_saved || mile.feyd_roses > 1 || mile.feyd_smuggler > 1){
			link(`Sail to Gothred. `, 150);
		}else if(softbug){
			link(`#Sail to Gothred. `, 150);
		}
		next(`Exit the Game.`);
		break;
		
		
//GOTHRED
	case 150: 
			//dismiss(); //TODO - shouldbe handled better but will work for now
			txt(`The cog finished the loading and was almost ready to set sail. `);
			npc(2, `Are you ready for our voyage to Gothred? `);
			chapter(`Yes. `, `voyage`);
			link(`I have to arrange a few more things. `, 110);
			break;
			
//TAVERN			
	case 200:
		txt(`The tavern was filled with various shady existence. My character fitted there well. `);
		if(mile.feyd_smuggler === 1) link(`Suspected smuggler @npc_3. `, 410);
		link(`Play Seven. `, 210);
		link(`Back. `, 110);
		break;
			
	case 210:
		DISPLAY.minigame({
			game: `seven`,
			victory: (money) => {
				ext.game_money = money;
				main(402)
			}, 
			cash: ext.game_money,
			no_links: true,
		})
		break;
	
//MARKETPLACE	
	case 300:
		INVENTORY.shop(`feyd`, () => {
			counter.slut = sluttiness(PC).details;
			if(mile.feyd_nude === 1 && counter.slut.bottomless){
				main(304);
			}else if(!mile.feyd_nude && (counter.slut.topless || counter.slut.bottomless) ){
				main(303);
			}else{
				main(110);
			}
		});
		break;
			
	case 303: 
		mile.slut++;
		if(counter.slut.bottomless){ 
			mile.slut++;
			mile.feyd_nude = 2;
			txt(`It definitely felt a bit weird to walk across a street full of people naked and exposed. I feel embarrassed and anxious - some of the NPC's ignored me, others were disapprovingly staring. I constantly had to remind myself those are programs, not the real people. I could be alone in my room and the situation would not be different. But it was also strangely naughty and exciting.`);
		}else{
			mile.feyd_nude = 1;
			txt(`It definitely felt a bit weird to walk across a street full of people topless with my breasts completely exposed. I feel embarrassed and anxious - some of the NPC's ignored me, others were disapprovingly staring. I constantly had to remind myself those are programs, not the real people. I could be alone in my room and the situation would not be different. But it was also strangely naughty and exciting.`);
		}
		
		if(mile.slut < 3){
			txt(`I would never ever do such an obscene, lewd thing irl! Nor I had ever such dumb naughty fantasies! But being free to do it in the game felt weirdly liberating. `);
		}else if(mile.slut > 5){
			txt(`My imagination ran wild: what if I walked exposed on a real street surrounded by real people, allowing everybody to see my hot body? I had no idea where that silly fantasy came from. `);
		}
		link(`Back. `, 110);
		
		break;
		
	case 304:
		mile.slut++;
		mile.feyd_nude = 2;
		txt(`Walking around topless felt embarrassing and a bit humiliating. NPC's were staring but otherwise, nothing really happened. So I decided to go a bit further and put on display everything. I felt so slutty! `);
		link(`Back. `, 110);
		break;
	
	
//GUARD	
	case 400: 
		{
			const slut = (()=>{
				const slut = sluttiness(PC).details;
				let out = "";
				if(slut.breasts || slut.genitals){
					out = `Not surprisingly he way paying extra attention to my exposed `;
					if(slut.breasts && slut.genitals){
						out += "@tits and @pussy. ";
					}else if(slut.genitals){
						out += "@pussy. ";
					}else{
						out += "@tits. ";
					}
				}
				return out;
			})();
			
			txt(`I decided to approach the @npc_1. `);
			kat(`Hello! I'm looking for a job because I need money for equipment and experiences to get on the next level.`);
			npc(1, `Hmmm...`);
			txt(`The @npc_1 was staring at me, examining me from the top of my head to my toes. ${slut}`);
			npc(1, `Hmmm... ${class_based("a shameless harlot... ", "an uncivilised horny barbarian... ", "an apostate ready for any perversion...")} I could have a use for you!`);
			kat(`I won't have sex with you!`);
			npc(1, `Not with me!`);
			kat(`Nor with anybody else!`);
			npc(1, `You don't have to have sex with anybody! Just act a bit flirty. `);
			kat(`Tell me more. `);
			npc(1, `There's a man in the tavern, his name is @npc_3. He's a known scoundrel and we suspect him from smuggling. We want to arrest him but he's armed and constantly surrounded by his buddies. But if you managed to lurk him outside we could ambush him. `);

			link(`I can do that!  `, 110, () => {
				mile.feyd_smuggler = 1;
				quest.initiate(
					"feyd_smuggler", 
					"Trick the smuggler", "reach", 
				);
			});
			link(`Seems too dangerous! `, 110);
		
		}
		break;
	
	case 410:
			counter.reward = 1;
			counter.slut = sluttiness(PC).details;
			txt(`@npc_3 was sitting at the table with his companions and they were drinking. I was speculating about the right approach until they noticed me staring and I had to act. `);
			kat(`Hello! `);
			npc(3, `Hi, beauty. `);
			
			link(`Can I join you? `, 411)
			roll(`Do you want to follow me alone into an abandoned barn? `, "char", 2, 412, 413);
			boost("char");
			break;
		
	case 411:
			npc(3, `Of course! There's enough space for a cute girl like you! `);
			txt(`There were no seats left but he pointed at his lap and I sat down. `);
			npc(3, `I noticed you were ogling me!`);
			kat(`I tend to ogle dangerously handsome guys. `);
			npc(3, `Just ogle or even something more? `);
			txt(`I lasciviously smirked. `);
			
			if(counter.slut.breasts){
				npc(3, `Let's check your goods!`);
				txt(`@npc_3 was captivated by my uncovered breasts and without asking began squeezing them. `);
				link(`Let him grope you. `, 415);
				link(`Order him to stop. `, 414);
			}else{
				npc(3, `Show me your tits, slut! `);
				txt(`He ordered. `);
				link(`Expose your breasts.`, 415);
				link(`Refuse. `, 414);
			}
			break; 
			
	case 412:
			counter.reward += 0.25;
			txt(`He laughed. `);
			npc(3, `A girl who knows what she wants? ${mile.class === 2 ? "Are all Northern women so upfront? I love that!" : "I like that! "} Let's go! `);
			txt(`Well, that was easy!`);
			link(`Abandoned barn. `, 420);
			break;
			
	case 413:
			txt(`@npc_3 instantly became suspicious.`);
			npc(3, `Why? `);
			kat(`To... have sex? `);
			npc(3, `You're really such a horny nympho you're offering yourself to anybody you meet? `);
			kat(`...yes? Also, you're not just anybody. I mean, I have no idea who you are because we just met but you're dangerously handsome!`);
			
			if(counter.slut.breasts){
				npc(3, `Let's check your goods!`);
				txt(`@npc_3 was captivated by my uncovered breasts and without asking began squeezing them. `);
				link(`Let him grope you. `, 415);
				link(`Order him to stop. `, 414);
			}else{
				npc(3, `Show me your tits, slut! `);
				txt(`He ordered. `);
				link(`Expose your breasts.`, 415);
				link(`Refuse. `, 414);
			}
			break;
			
	case 414:
			mile.slut--;
			if(counter.slut.breasts){
				kat(`Hey, what are you doing! Stop it! `);
			}else{
				kat(`No! I can't do that! `);
			}
			txt(`I covered my chest. He scowled and pushed me away. `);
			npc(3, `I have no time for boring shy wenches.`);
			kat(`But...`);
			npc(3, `Get lost! `);
			link(`Get lost. `, 440);
			break;
			
	case 415:
			if(counter.slut.breasts){
				txt(`I let him to play with my tits. `);
			}else{
				showLess(0, -1);
				txt(`I sighed and moved away my top.`);
			}
			npc(3, `Nice tits, slut. `);
			kat(`Thanks. I can show you even more - but not here, in front of the whole tavern!`);
			npc(3, `You're so coy! But fine! Let's go!`);
			link(`Abandoned barn. `, 420);
			break;
			
			
	case 420:
			showAll();
			txt(`Guards were supposed to wait for us in an abandoned barn. However, when we arrived, nobody was there. `);
			if(mile.slut < 2){
				txt(`@npc_3 pushed me into hay and got very handsy. I did not use to let guys on a first date go so far and even though I knew he was just an NPC, I felt extremely uncomfortable.`);
				
				link(`Slow down! You're going too fast! `, 422);
				link(`GUARDS?! `, 423);
				link(`Have sex with him. `, 426);
			}else{
				txt(`@npc_3 pushed me into hay and got very handsy. `);
				link(`Is that a dagger in your pants? `, 421);
			}
			break;
			
	case 421:
			counter.reward += 0.25;
			npc(3, `What... oh, yeah, that's a dagger.`);
			kat(`Could you please put it away? `);
			npc(3, `Sure! `);
			txt(`I helped to disarm him and then he returned to groping my me. `);
			
			link(`GUARDS?! `, 423);
			link(`Have sex with him. `, 426);
			break;
			
	case 422: 
			npc(3, `Too late to change your mind, you little slut! `);
			//todo			
			link(`GUARDS?! `, 423);
			link(`Have sex with him. `, 426);
			break;
			
	case 423: 
			kat(`GUARDS! HELP! `);
			npc(3, `Guards?! You traitorous bitch! Nobody will help you! `);
			txt(`@npc_3 was on top of me and wrapped his fingers tightly around my throat. He cut off my screaming and began choking me. `);
			
			roll(`Struggle free! `, "str", 2, 424, 425);
			break;
			
	case 424: 
			counter.reward += 0.2;
			txt(`I managed to wrestle his arms away escape from his grip. I dashed to the barn door, he right behind me. `);
			txt(`And then the guards finally arrived! After a short brawl, they were able to capture him.`);
			link(`Reward. `, 430);
			break;
			
	case 425: 
			txt(`I desperately tried to struggle but he was on top of me and I was totally helpless. I felt my vision was getting dark but then somebody dragged him away from me. `);
			txt(`The guards finally arrived! @npc_3 was quickly tied up. `);
			kat(`W... what took you so long!`);
			npc(1, `Sorry for the little delay. `);
			kat(`He could kill me!`);
			link(`Reward. `, 430);
			break;
			
	case 426: 
			showLess(0, 0, 3);
			mile.slut++;
			counter.reward += 0.25;
			txt(`Did the guards forget about me? Instead of risking a confrontation, I decided to submit. He keenly and bluntly stripped me and then got naked too, removing all his clothes including the belt with his dagger. `);
			if(mile.slut < 1){
				txt(`It was terrifying to see his huge throbbing cock. I had made a huge mistake!`);
			}else if(mile.slut > 4){
				txt(`I was watching his huge hard cock and began hoping the guards will not arrive too soon. `);
			}
			txt(`@npc_3 smiled and was about to enter me when somebody grabbed his shoulder and dragged him away. It was the guard! Nude @npc_3 was not even able to resist!`);
			link(`Reward. `, 430);
			break;
			
		
	case 430:
			showAll();
			mile.feyd_smuggler = 2;
			quest.done("feyd_smuggler");
			quest.closure("feyd_smuggler");
			
			npc(1, `Good job! `);
			txt(`Very pleased guards were congratulating me. `);
			if(mile.class === 1){
				npc(1, `I knew that for a shameless hot whore like you it will be a piece of cake to seduce him! `);
			}else if(mile.class === 2){
				npc(1, `I knew he won't suspect a dumb savage like you and will think you're just unconventional and horny'`);
			}else if(mile.class === 3){
				npc(1, `I knew that shamelessly seducing and tricking him just to immediately betray him and stab him in his back will be a piece of cake for a heretic like you!`);
			}
			wage(rewards.feyd_smuggler * counter.reward);
			link(`Done. `, 110);
			break;
			
	case 440:
			mile.feyd_smuggler = -1;
			quest.closure("feyd_smuggler");
			txt(`I returned to the guards lying in ambush in an old barn. `);
			npc(1, `Where's @npc_3?`);
			kat(`He's too careful! I wasn't able to convince him! `);
			npc(1, `I knew asking a stupid ${class_based("whore", "savage", "heretic")} like you was a mistake! 	`);
			kat(`Maybe I could...`);
			npc(1, `No! You're totally useless! We can handle it without you! Get lost! `);
			txt(`The guards were not hiding their disappointment.`);
			
			link(`Failure. `, 110);
			break;
			
			
			
//ROSES		
	case 500:
		if(!mile.feyd_roses){
			kat(`Hey! `);
			npc(4, `Hello! `);
			txt(`The @npc_4 smiled. ${sluttiness(PC).details.topless ? "Her eyes were darting to my bare chest but she did not dare to say anything. " : ""}`);
			kat(`By any chance, do you have any quest for a lot of money? `);
			npc(4, `Eh... actually... `);
			kat(`Come on!`);
			npc(4, `My Mistress ordered me to brought her ${roses_number} sintrean blue roses. But they're very rare and I'm afraid to explore the woods. The forces of evil are lurking around and I don't want to get r${String.fromCharCode(97)}ped by orcs!  `);
			kat(`Or killed. `);
			npc(4, `That would be bad too!`);
			kat(`I have no idea how those roses look. `);
			npc(4, `I already found one, I can give it to you if you bring me ${roses_number-1} others! And I can pay you ${rewards.feyd_roses} coins from my allowance.`);
			link(`Fine! `, 110, () => {
				mile.feyd_roses = 1;
				quest.initiate(
					"feyd_roses", 
					"Collect 4 sintrean blue roses", "collect", 
					{counter: 1, desired: 4},
				);
			});
			link(`Sorry darling, but I'm the hero and I don't have time for fetch quests! `, 110);
		}else{
			if( quest.is_active("feyd_roses") ){
				npc(4, `Do you have the roses!?! `);
				kat(`Not yet. `);
				npc(4, `No pressure but my Mistress is getting impatient and I don't want to make her angry! `);
				link(`I'm working on it! `, 110);
			}else{
				npc(4, `Do you have the roses!?! `);
				kat(`Of course! `);
					
				link(`Give her roses. `, 501);
				link(`Ask for more. `, 502);				
			}
		}
		break;
			
	case 501: 
		mile.feyd_roses = 2;
		npc(4, `Thank you! You saved my life! My Mistress won't punish me! `);
		wage(rewards.feyd_roses);
		link(`Done. `, 110);
		quest.closure(`feyd_roses`);
		break;
			
	case 502: 
		kat(`However, collecting them was more dangerous than I expected. `);
		npc(4, `W... what do you mean?! `);
		kat(`You have to pay me ${Math.ceil(rewards.feyd_roses * 1.7)} coins. `);
		npc(4, `But we had a deal! I don't have so much money! Please, I can give you more than ${Math.ceil(rewards.feyd_roses * 1.3)} coins!? `);
			
		link(`Okay. `, 503);
		link(`That won't be enough! `, 110);
		break;
			
	case 503: 
		mile.feyd_roses = 3;
		txt(`The maid looked grumpy when she handed me the coins. `);
		kat(`You're welcome! `);
		wage(Math.ceil(rewards.feyd_roses * 1.3));
		quest.closure(`feyd_roses`);
		link(`Done. `, 110);
		break;


	case 510:
		if(mile.feyd_roses === 1 && quest.is_done(`feyd_roses`) ){	
			txt(`The arrogant @npc_5 was not in a mood to talk with me. `);
			link(`Sell her sintrean blue roses. `, 511);
			link(`Back. `, 110);
		}else{
			txt(`The arrogant @npc_5 was not in a mood to talk with me. `);
			link(`Back. `, 110);
		}
		break;
			
	case 511: 
		mile.feyd_roses = 4;
		kat(`Hello! You look like somebody with a refined taste. Do you like sintrean blue roses? `);
		npc(5, `I love those! But thank you, I already ordered my clumsy maid to bring me some. `);
		kat(`Are you sure she'll be able to do it? `);
		npc(5, `You're right, she's completely useless. I'll have to spank her when she returns home. I take everything you have, I can give you  ${Math.ceil(rewards.feyd_roses * 2)} coins. `);
		wage(Math.ceil(rewards.feyd_roses * 2));
		//delete ext.quests.roses;
		quest.closure("feyd_roses")
		link(`Back. `, 110);			
		break;
			
//YELAYA	
	case 600: 
		kat(`Hello! Don't you have a quest for an adventurer?  `);
		txt(`I greeted a girl dressed in ragged male clothes. `);
		yel(`Funny, I was about to ask you exactly the same thing! `);
		kat(`You're an adventurer too?! `);
		yel(`Yeah! Are you surprised? `);
		kat(`No, of course not! I just didn't expect that. `);
		yel(`Adventurers should expect unexpected! `);
		if(mile.immersion > 0){
			kat(`I'm new at adventuring! I used to be ${an(decapitalise(character_origin()))}.`);
			yel(`Why did you stop? `);
			kat(`It's a dark and tragic tale! `);
			yel(`I can imagine that! `);
			txt(`Nodded the girl. `);
		}else if(mile.immersion === 0){
			kat(`I'm new at adventuring! I used to be ${class_based("a harlot. ", "a hellion. ", " a priestess. ")}. `);
		}else{
			kat(`I'm new at adventuring! I used to be ${class_based("a whore. ", "a barbarian. ", " a sorceress. ")}. `);
			yel(`Why did you stop? `);
			kat(`I can't recall at the moment but I surely had a very good reason! `);
		}
		yel(`Haven't you hear any rumors? `);
		kat(`No?! You're a local, aren't you? `);
		yel(`I arrived several days ago from far away. From Sintre. I'm here searching for... damn, please forget I said anything!`);
		kat(`You didn't say anything! Come on! `);
		yel(`No, I can't! `);
		kat(`Yes, you can! I can help you!`);
		yel(`But you have to swear you'll not tell a living soul!`);
		kat(`I swear!`);
		yel(`Fine! I don't need more money than I can spend in two lifetimes! So you'll get your share. If you survive. `);
					
		if(mile.immersion > 0){
			link(`I'm in! I fear no danger! `, 601)
		}else{
			link(`I'm in! And if I die, I'll just reload! `, 601);
		}
		link(`I'm not really into treasures. `, 110);			
		break;
			
	case 601: 
		quest.initiate("yelaya", "Find the ancient treasure", "reach");			
		mile.feyd_yelaya = 1;
		yel(`Excellent! I'm @yel by the way! `);
		kat(`I'm @kat. `);
		yel(`What a strange name! I hope I'm not pronouncing it wrong! `);
		kat(`Don't worry. You were talking about treasures? `);
		yel(`I have a lead that an ancient treasure could be found in a cavern nearby. But it will be a long and difficult trip. `);
		enter(`Let's go! `, `yelaya`, 610, 611, `yelaya`, () => fellowship("yel")); 
		link(`Give me a moment to gather my stuff. `, 110);	
				
		break;
			
	case 602:
		yel(`Are you finally ready? `);
				
		enter(`Let's go! `, `yelaya`, 610, 611, `yelaya`, () => fellowship("yel") ); 
		link(`No. `, 110);	
		break;
		
	case 610:
		quest.closure("yelaya");	
		if(mile.feyd_yelaya_saved === 1){
			txt(`We returned back to the town. Our quest was a failure, we almost had the treasure but we barely survived and left empty-handed. `)
		}else{
			txt(`The tragic death of Yelaya was bothersome but otherwise was our quest successful and I gained the treasure. `);
		}
		link(`Done. `, 101);	
		break;
			
	case 611:
		link(`Back. `, 101);	
		break;
			
			
	case 620:
		mile.feyd_yelaya = 10;
		yel(`Hello! How are you? Ready for the next adventure? `);
		kat(`You still want to be an adventurer? `);
		yel(`Of course! I decided to travel to the capital and search for the job there. Will you join me? Travelling together might be fun! `);
		kat(`I'm sorry but I have to travel to Gothred! `);
		yel(`Damn! That shame! Anyway, good hunting! `);
		link(`Good luck to you too! `, 110);	
		break;
	}
	
}
