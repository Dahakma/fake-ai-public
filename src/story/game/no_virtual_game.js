import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
import {txt, npc, kat} from "Loop/text";
import {qaa, qbb, qcc} from "Loop/text";
import {yel} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s} from "Loop/text";
import {placetime} from "Loop/text_extra";
import {DISPLAY} from "Gui/gui";

import {crew} from "Avatar/index";
import {remove, createWear, create, INVENTORY, updateDraw, updateMods, sluttiness, showLess, showAll} from "Avatar/index";
import {clone, shuffle, number2word, /*an,*/ decapitalise} from "Libraries/dh";  
const an = a => a; //TODO

import {ra} from "Libraries/random";
import {roll, boost, initiate_game_character, class_based, stupid_names, character_origin, new_act, fellowship} from "Virtual/index";

import {enter} from "Dungeon/engine";
import {autosave, snapshot} from "System/save";
import {quest} from "Virtual/quest"; 
import {rewards} from "Data/items/prices"; 
import {wage} from "Virtual/text";
import {level_up} from "Virtual/level_up"; 

//import {INVENTORY, effigy, wear, updateDraw, create} from "Avatar/index";
/*

const item = create(blueprint);
		INVENTORY.add(item);
		*/

function equip(...items){
	items.forEach( a => {
		if( Array.isArray(a) ){
			createWear(ra.array(a));
		}else{
			createWear(a);
		}
	});
}


export const skip_feyd_intro = (index) => {
	set.game();
	
	//SPELL
	txt(`I finished the first quest (killing rats in a cellar), was called a whore (because my character was ${["an indecent harlot selling her body to everybody with a few coins","a shameless savage controled only by her low urges","a lewd woman who cared about her sinful desires more than her sacred duty"][ext.class-1]}) and was told to wait until ${mile.feyd_keryk_name} will arrange a ship to Gothred. `);
	//TODO - WEAPON & POTION?
	ext.game_money += rewards.feyd_rats;
	mile.feyd_rats = true;
	mile.feyd_keryk = true; //??
	
	//SECOND LEVEL
	
	chapter(`Exit the game. `, `character_creation`, 150);
}

export const skip_feyd = (index) => {
	set.game();
	
	//inventory
	ext.game_money += 12;
	INVENTORY.add( create("xHP"), 2);
	INVENTORY.add( create("xEng"), 2);
	
	remove("socks");
	equip(
		["rustyFalchion", "crackedBludgeon","bentSpear"],
		["fallenLoincloth", "barbaricLoincloth"], 
		"fTearNecklace",
		"leatherVambraces",
	);
	if(counter.feyd_nude === 1){
		remove("upper");
		remove("bra");
	}
	updateDraw();
	
	//text
	txt(`I explored the area around the town Feyd Mora, collected Sintrean roses for a maid, helped guards to catch a smuggler and together with @yel we searched for an ancient treasure and slayed the Beast. `);
	txt(`When I finished all the quest I boarded a merchant cog traveling to the Free City of Gothred. `);
	
	//THIRD LEVEL
	
	new_act(2);
	mile.game_progress = 20; 
	
	set.reentry("goth", 101);
	next();
}


export const skip_goth_1 = (index) => {
	set.game();

	//inventory
	ext.game_money += 30;
	INVENTORY.add( create("xHP"), 1);
	INVENTORY.add( create("xEng"), 1);
	
	remove("upper"); //TODO - TO INVENTORY
	remove("panties");
	equip(
		["falchion","bludgeon","spear"],
		["fancyLeatherPauldrons"],
		["leatherArmor","leatherArmor","fancyLeatherArmor"],
		["darkWideBelt", "darkThinBelt", "fPolyWideBelt", "fPolyThinBelt"],
		"harlotStockings",
	);
	updateDraw();
	
	
	
	txt(`After escaping the pirates I arrived to Gotherd. I strolled the wealthy trade hub, explored the mysterious abandoned ship <i>Barnham Dyn</i> and hunted feral goblins in the countryside. `);
	txt(`Then I finally get to visiting Master Arazor, a mage who explained I have to assemble a crew and sail to find an ancient magic sceptre. `);
	
		
	//ghost ship
	quest.initiate("ghost_ship", "Explore mysterious ship <i>Barnham Dyn. </i> (finishes quest)", "reach");
	mile.goth_ghost_ship = 2;
	quest.done("ghost_ship");
	quest.closure("ghost_ship");
	
			
	//goblins
	quest.initiate("goth_goblins", "Deal with feral goblins outside of Gothred (finishes quest). ", "eliminate");
	mile.goth_marshal = 2;
	quest.done("goth_goblins");
	quest.closure("goth_goblins");
	
	
	//arazor
	mile.goth_arazor = 2;
	quest.initiate(`karged`, `Explore the Tombs of Karged and find the Sceptre of Torrywialen`, "reach", {liason: "Arazor"});
		
		
		
	mile.game_progress = 22; 
	set.reentry("goth", 101);
	next();
	
	//stocking vambra
}



export const skip_ship = (index) => {
	set.game();
	
	//inventory
	ext.game_money += 30;
	
	remove("upper");
	equip(
		["cutlass","mace","scythe"],
		["steelGorget"],
		["bronzeGreaves"],
		["bronzeCuirass"],
		["fLeggings"],
		["fancyLeatherVambraces"],
		["fFancyChoker", "fMetalChoker"],
	);
	updateDraw();
	
	//explore everything
	["goth","karg","oskyl","ydyg","smug","dorf","hdar","wreck"].forEach( key => {
		if(!mile[`ship_${key}`] || mile[`ship_${key}`] < 2) mile[`ship_${key}`] = 2;
	});
	mile.ship_karg = 3;
	
	//Krged
	mile.goth_arazor = 6;
	quest.done("karged");
	quest.closure("karged");
	quest.initiate(`oskyl`, `Explore the Labyrinth of Oskyl and find the Sceptre of Torrywialen`, "reach", {liason: "Arazor"});
	
			
	//THIRD LEVEL 
		
	txt(`Together with @ste, @lys, @gal and @sir we sailed the Dewyrain sea and explored various islands and islets. `);
	txt(`We investigated the ancient Aquilonian necropolis on Karged. We were not able to find the Sceptre of Torrywialen but we found an old scroll pointing towards the Oskyl. `);
	
	mile.game_progress = 24;
	set.reentry("goth", 101);
	next();

}




export const skip_goth_2 = (index) => {
	set.game();
	
	//inventory
	ext.game_money += 30;
	
	equip(
		["sharpCutlass","heavyMace","sharpScythe"],
		["steelPauldrons"],
		["majorMagicTearNecklace"],
		["fFancyThinBelt", "fFancyWideBelt"],
		["harlotStockings"],
		["bronzeVambraces"],
		["fancyBarbaricLoincloth"],
	)
	updateDraw();
	
	//oskyl
	mile.goth_arazor = 8;
	quest.done("oskyl");		
	quest.closure("oskyl");
	
	
	txt(``);

	
	mile.game_progress = 28;
	new_act(3);
	set.reentry("goth", 1000);
	next();

}
