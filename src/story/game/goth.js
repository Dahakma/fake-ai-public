/**
	part of the virtual game story set in the free city GOTHRED
	act: 2
	follows: voyage
	precedes: river
	children: ship; whoring
*/

import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";

import {txt, npc, kat, fol} from "Loop/text";
import {gal, ste, lys, sir} from "Loop/text";
import {bold, hint, and, s} from "Loop/text";

import {clone, shuffle, clamp, pm} from "Libraries/dh";
import {ra} from "Libraries/random";

import {createWear, crew, quickSaveOutfit, quickLoadOutfit} from "Avatar/index";
import {quickTempOutfit, INVENTORY, updateDraw, updateMods, sluttiness, clothes, showLess, showAll, wears, remove, effigy, removeEverything } from "Avatar/index";

import {autosave, snapshot} from "System/save";
import {quest} from "Virtual/quest"; 
import {class_based, roll, /*dance,*/ fellowship, dismiss, boost} from "Virtual/index";
import {new_act} from "Virtual/index"; 
import {level_up} from "Virtual/level_up"; 
import {popup} from "Gui/gui"; 
import {rewards} from "Data/items/prices"; 

import {wage, We, we, fol_name, he, him, his, He, dyke} from "Virtual/text";
import {whoring_initial, whore_haggling, whore_bj} from "./whore";  //TODO
import {DISPLAY} from "Gui/gui";
import {enter} from "Dungeon/engine";
import {dibz, loot, loot_multiple} from "Avatar/index";
import {placetime} from "Loop/text_extra";

function dance_wage(sum, mod = 1){
	mod += 0.5 + (PC.att.dance / 100);
	sum = Math.round( sum * mod );
	txt(`Based on my dexterity and charisma bonuses I earned ${sum} coins. `);
	wage(sum);
}


/*
	mile.goth_arazor
	1 - meeting arranged
	2 - discussed, searching crew
	3 - ship bought
	4 - ship ready to sail
	5 - scroll discovered
*/

export const goth = (index) => {
	switch(index){
	case 101: 
		
		set.reentry("goth");
		set.game();
		//break;
			
	default:
	case 110:
		placetime(`Free City of Gothred`, ``);
		//txt(`Gotherd city desc todo`);
		link(`Arazor's tower. `, 500);
		link(`Port. `, 600);
		link(`Great Bazaar. `, 300);
		link(`Nancy's Tavern. `, 200); //Seaside Inn
		link(`House of Earthly Delights. `, 400);
	
	//TODO - should be triggered only by chatting with Lyssa (mile.goth_whoring = 1) or also just by high slutiness???
		if(mile.goth_whoring > 1 || (mile.goth_whoring > 0 && mile.slut >= 0) ){ //TODO - slut
			link(`Whore yourself.`, 900);
		}else if(hardbug){
			link(`##Whore yourself.`, 900);
		}
			
		if( quest.is_active(`goth_goblins`) ){
			enter(`Hunt goblins. `, `goth_goblins`, 110, 110, `goth_goblins`); 
		}
		
		if( quest.is_active(`goth_manor`) ){
			enter(`Old Haversham Manor. `, `goth_manor`, 110, 110, `goth_manor`); 
		}		
			
		//DEBUGGING
		if(softbug && !quest.is_closed("goth_goblins") ){
			link(`#Skip Hunting goblins.`, 110, () => {
				quest.initiate("goth_goblins", "Deal with feral goblins outside of Gothred (finishes quest). ", "eliminate");
				mile.goth_marshal = 2;
				quest.done("goth_goblins");
				quest.closure("goth_goblins");
				wage("goth_goblins");
			});
		}

		if(softbug && !quest.is_closed("ghost_ship") ){
			link(`#Skip Ghost ship.`, 110, () => {
				quest.initiate("ghost_ship", "Explore mysterious ship <i>Barnham Dyn. </i> (finishes quest)", "reach");
				mile.goth_ghost_ship = 2;
				quest.done("ghost_ship");
				quest.closure("ghost_ship");
				wage("goth_ghost_ship");
			});
		}
			
		if(softbug && mile.goth_arazor >= 4 && quest.is_active("karged") ){
			link(`#Explore the Tombs of Karged.`, 110, () => {
				mile.goth_arazor = 5;
				quest.done("karged");		
			});
		}
			
		if(softbug && quest.is_active("oskyl") ){
			link(`#Explore the labyrinth on Oskyl & find the scepter.`, 110, () => {
				mile.goth_arazor = 7;
				quest.done("oskyl");		
			});
		}
			
		next(`Exit the Game.`);
		break;



//NANCY'S TAVERN 200
	case 200:
		placetime(`Nancy's tavern`, ``);
		present(
			[1, `Marshal`, `gothGeneral`],
			[2, `Big Guy`, `commoner`],
		);
			
		txt(`The tavern was a busy and lively place filled with various sailors, merchants, adventurers and mercenaries. `);			 
			
		if(!mile.goth_ste){
			link(`Order a beer. `, 220);
		}else if(!mile.ship_ste && ext.follower?.key !== "ste"){
			link(`@ste.`, 221);
		}
		if(!mile.ship_gal && ext.follower?.key !== "gal") link(`@gal.`, 230);
		link(`@npc_1.`, 240);
		link(`Play Seven. `, 210);
		if(mile.goth_arm_win < 6 && ext.game_money >= rewards.arm_wrestling) link(`Arm wrestling. `, 250);
		link(`Back. `, 110);
		break;
			
			
	//PLAY SEVEN
	case 210:
		DISPLAY.minigame({
			game: "seven",
			victory: (money) => {
				ext.game_money = money;
				main(200)
			}, 
			cash: ext.game_money,
			no_links: true,
		})
		break;
		
		
	//STEDE
	case 220:
		mile.goth_ste = 1;
		txt(`I ordered a mug of beer and looked around for a new quest hook. How I was studying patrons, I was not paying attention to my immediate surroundings and walked straight into a gaunt man and spilled most of the beer on his shirt. `);
		kat(`Damn! I'm so sorry!`);
		ste(`Don't worry. Such things happen to me all the time. It was my mistake when I naively assumed I can go to a pub to drown my sorrow without anything bad happening to me. `);
		kat(`A bad day?`);
		ste(`A bad day?!? Bad week, bad month and bad year! `);
		if(mile.immersion < 0){
			txt(`I was not particulary interested in his backstory but he seemed like a potential quest-giver or follower. So I nodded and listened.`);
		}else{
			kat(`Oh no! What happened? `);
			txt(`I gasped sympathetically.`);
		}
		ste(`I lost everything! Those bastards and that traitorous evil bitch took my ship from me! I needed only a few more days but those stupid greedy scoundrels were too impatient and deposed me! Oh, gods! Why does everything I touch turn into dust?`);
		kat(`So you were a captain? `);
		ste(`Yes, the best one! But when the sea and the wind conspire against you, skill and talent aren't enough!`);
		if(mile.immersion < 1){
			kat(`So you want me to regain your ship for you? `);
		}else{
			kat(`Maybe you'll be able to regain your ship? `);
		}
		ste(`I doubt about that. There's no hope. I'll have to find a new craft. Maybe I should become a mercenary? Even though I would probably die in the first battle. `);
		kat(`I can help you with that! I mean with becoming a mercenary, not dying. Although I can't rule out the dying either. I'm looking for companions. `);
		ste(`Why not? Even being inevitably brutally, agonizingly torn apart and devoured by monsters can't be worse than slowly drinking myself to death. `); 
		kat(`Awesome. That's the attitude I need! I'll let you know when I'll set out! `);
		link(`Back. `, 200);
		break;
			
	case 221:
		txt(`@ste was drinking, not paying me attention. `);
		link(`I need your help! `, 222);
		if(mile.goth_arazor >= 2)  link(`I need volunteers for a dangerous and epic mission. `, 223)
		link(`Back. `, 200);
		break;
			
	case 222:
		fellowship("ste");
		kat(`@ste? Stop sulking! Adventure waits! `);
		ste(`I hope it's very dangerous with a little chance to succeed and survive. `);
		link(`Let's go! `, 200);
		break;
			
	case 223:
		mile.ship_ste = true;
		ste(`What kind of mission? `);
		kat(`We'll visit an island and save the world. And I need somebody who knows how to drive a ship. `);
		ste(`This sounds too good to be true. The ship will sink and we'll all die, won't we? `);
		kat(`Possibly. Are you in? `);
		ste(`Sure! `);
		link(`Meet me at the port. `, 200);
		break;
			
	//GALAWAR
	case 230:
		mile.goth_gal = 1;
		gal(`Hello! How are you doing! `);
		txt(`@gal was sitting at a table, enjoying plenty of food and drinks and pleasant company, which included several attractive wenches. `);
		kat(`Not as well as you. `);
		gal(`How can I help you? `);
			
		link(`Can you join me? I need your help! `, 231);
		link(`Can you lend me money? `, 232);
		if(mile.goth_arazor >= 2)  link(`Can you join my crew? `, 233);
		link(`Back. `, 200);
		break;
			
	case 231: 
		fellowship("gal");
		gal(`Sure! I was getting bored here anyway. Let's go! `);
		link(`Let's go! `, 200);
		break;
			
	case 232:
		if(mile.goth_gal_money){
			gal(`You didn't return the money I lend you the last time!`);
		}else{
			mile.goth_gal_money = true;
			gal(`Why do people away bother me and ask me for money? `);
			kat(`Because you look rich? `);
			gal(`Oh. That might be it. Fine! `);
			wage( rewards.goth_gal_money + (mile.goth_gal_sex && rewards.goth_gal_money) );
		}
		link(`Back. `, 230);
		break;
			
	case 233:
		mile.ship_gal = true;
		gal(`No. I have enough of ships. `);
		kat(`Come on! It's going to be fun! One quick adventure, to Karged and back! And you still owe me for saving your life and helping you escape.`);
		gal(`Fine!`);
		link(`Meet me at the port. `, 200);
		break;
			
		
	//MARSHAL
	case 240: 
		if(!mile.goth_marshal){ 
			mile.goth_marshal = 1;
			quest.initiate(
				"goth_goblins", 
				"Deal with feral goblins outside of Gothred. ", 
				"eliminate", {liason: "Marshal"},
			);
				
			txt(`A charismatic man dressed in luxurious armor nodded at me. `);
			npc(1, `Hey, are you an adventurer? `);
			kat(`Yeah? Who are you? `);
			npc(1, `I'm the Marshal. `);
			kat(`Marshal who? `);
			npc(1, `You really don't know who I am? I'm in charge of the Gothred Guard. `);
			kat(`Oh! Nice to meet you. `);
			npc(1, `I can pay you ${rewards.goth_goblins} coins if you can deal with goblins outside of the city. Despite all the extermination attempts, there were always goblins living in the mountains. They usually didn't dare to get closer to the city but something changed and they are getting bolder. `);
			kat(`Don't you have an army? `);
			npc(1, `Yes, I do. And I also have a fund for independent contractors. My main task is to defend Gothred and the goblins are not a direct threat to the city. But the appeals of dirty peasants are getting annoying. `);
			kat(`And you just hire anybody? Without any recommendation? `);
			npc(1, `Yeah! We don't pay anything in advance. If you finish the task and survive, you were qualified enough. `);
			kat(`I'm not sure if I'm in a mood to slay goblins. `);
			npc(1, `I don't give fuck about your mood. Kill the goblins if you want the money. Or don't. `);
								
			link(`Back. `, 200);
			
		}else if(mile.goth_smugglers === 2){	
			mile.goth_smugglers = 3;
			npc(1, `What is going on? `);
			kat(`I have an important report. We found a secret hideout of smugglers. `);
			npc(1, `Smugglers?!? Tell me everything!`);
			txt(`...`);
			npc(1, `Good job! I'll talk with the Sea Lord and we'll send a patrol to secure the place!`);
			wage("goth_smugglers");
			link(`Back. `, 200);
				
		}else if(mile.goth_marshal === 2){
			mile.goth_marshal = 3;
			npc(1, `Are you looking for a new quest? `);
			kat(`Well, maybe? `);
			npc(1, `I might have one. I need somebody to clear the old Haversham Manor. It nearly burned down several years ago. Since the whole family died in the fire, nobody was willing to invest in repairs and the manor became an ugly, abandoned ruin. And all sorts of nasty creatures settled there. `);
			kat(`You mean like homeless people? `);
			npc(1, `Worse. We have reports about very large spiders. `);
			kat(`How large? Bigger than three centimeters? `);
			npc(1, `Dog-sized. `);
			kat(`OH NO!`);
			npc(1, `Yes. They are terrifying. That's why I'm so happy you volunteered. `);
			kat(`But this seems like a task for the guard!`);
			npc(1, `It's a perfect task for adventurers! Either the building gets cleared or there will be one trouble-making murderhobo less and the fed spiders won't bother the neighborhood. `);
			kat(`I'm not going to be a sacrifice for spiders!`);
			npc(1, `Of course not! You're different than the previous adventures! I have my full trust in you, brave Goblinslayer! Here, take this, it will help you! `);
			loot(2, "xAnti");
			kat(`I didn't agree I'll do it... but thanks... `);
			
			quest.initiate(
				"goth_manor", 
				"Kill all spiders in old Haversham Manor", 
				"eliminate", {liason: "Marshal"},
			);
			link(`Back. `, 200);
			
			
		}else if( quest.is_done("goth_goblins") ){	
			mile.goth_marshal = 2;
			quest.closure("goth_goblins");
			npc(1, `I see it on your face! You have something to report and I'll like it!`);
			kat(`I hope so! I dealt with those goblins! `);
			npc(1, `Excellent! Good job!`);
			wage("goth_goblins");
			link(`Back. `, 200);
		
		}else if( quest.is_done("goth_manor") ){	
			mile.goth_marshal = 4;
			quest.closure("goth_manor");
			kat(`Done! The Haversham Manor is now spider-free! `);
			npc(1, `Amazing job! `);
			wage("goth_manor");
			link(`Back. `, 200);
			
			
		}else if(mile.goth_marshal === 4){	
			npc(1, `Oh! If it isn't our bold Goblinslayer! Sorry, I see how eager you are but I don't have any quest for you right now. `);
			link(`Back. `, 200);
			
		}else if(mile.goth_marshal === 3){
			npc(1, `Anything you want to report to me? `);
			kat(`Eh, no. `);
			npc(1, `Come on, I trust you, Goblinslayer! `);
			link(`Back. `, 200);
		
		}else{
			npc(1, `Anything you want to report to me? `);
			kat(`Eh, no. `);
			npc(1, `Then get lost!`);
			link(`Get lost. `, 200);
		}
		break;
		
		
	//ARM WRESTLING
	case 250:
		if(mile.goth_arm_win > 3){
			mile.goth_arm_win++;
			npc(2, `Fine, you proved your strength enough! I can't compete with you anymore, I'm out of money. `);
			link(`Understandable. `, 200);
			link(`Coward! `, 200);
		}else{
			if(!mile.goth_arm){
				txt(`Several guys were testing their strength and competed in arm wrestling. `);
				kat(`Can I join? `);
				npc(2, `Hah, <i>you</i>? If you're not afraid, the bet is ${rewards.arm_wrestling} coins. But I have to warn you, I don't care how many elixirs you drank, no girl will beat me! `);
			}else{
				npc(2, `You again? The bet is still ${rewards.arm_wrestling} coins if you're not afraid! `);
			}
			roll(`I'm not afraid! `, "str", 2, 251, 252);
			boost("str");
			link(`I might be slightly afraid. `, 200);
		}
		break;
			
	case 251:
		mile.goth_arm++;
		mile.goth_arm_win++;
		kat(`Ha! I won! `);
		npc(2, `Not bad. `); 
		txt(`It was amazing to watch that my character was stronger than this burly man. `);
		wage(rewards.arm_wrestling);
		link(`Back. `, 200);
		break;
		
	case 252:		
		txt(`I tried what I could but he was just stronger and after a short struggle he forced the back of my hand to touch the desk. `);
		npc(2, `I won! Don't feel too bad, you had no chance, girl. `);
		wage(-rewards.arm_wrestling);
		link(`Back. `, 200);
		break;
			
		

//BAZAR	300
	case 300:
		INVENTORY.shop("goth", () => main(110) );
		break;


//HOUSE OF DELIGHTS 400
	case 400:
		placetime(`House of Earthly Delights`, ``);	
		present(
			[`lys`, `harem`],
			[2, `Barman`, `commoner`],
		);
		txt(`<i>House of Earthly Delights</i> was a too poetic name for a sleazy establishment more reminding modern strip club than anything medievaly. Two half-naked half-elvish beauties lasciviously swayed on a small stage while being loudly cheered by the crowd. `);
		
		if(!mile.goth_stripper){
			link(`Look for a job. `, 401);
		}else if(mile.goth_stripper === -1){
			if(mile.y_game_1 === true){
				if(mile.slut > 4){
					txt(`The barman suggested I should try to perform on the stage. I tought about what the AI told me. This was not a real world, just a game. I could experiment and be a total slut with zero consequences! It might be fun to try it. `);
					link(`Dance. `, 410);
				}else{
					txt(`The barman suggested I should try to perform on the stage.  Normally I would never do anything so humiliating and embarassing. However, I had to think about what the AI told me. This was not a normal world, but a consequenceless game. And I was not a nice girl @kat but a shameless ${class_based(`professional whore`, `wild savage`, `perverted priestess`)}. Maybe... maybe I should try it? `);
					link(`Dance. `, 410);
				}
			}else{
				//TODO
				if(mile.slut > 4){
					txt(`The barman suggested I should try to perform on the stage. I had some doubs whether it was a good idea or not. However...`);
					link(`Dance. `, 410);
				}else{
					txt(`The barman suggested I should try to perform on the stage. I - of course - refused! `);
					con_link(false, `low slut/not discussed with AI`, `Dance. `, 410);
				}
			}
		}
			
		if(mile.ship_lys || ext.follower?.key === "lys"){
			//
		}else if(mile.goth_lys > 1){
			link(`Talk with @lys. `, 421);
		}else if(mile.goth_lys > 0){
			link(`Talk with @lys. `, 420);
		}else if(hardbug){
			link(`##Talk with @lys. `, 420);
		}

		link(`Back. `, 101);
		break;
			
	//DANCING
	case 401:
		mile.goth_stripper = -1;
		txt(`I was sure somebody here will know about something that had to be killed for money and loot. But I probably should have expected the barman's offer: `);
		npc(2, `You look hella fine, don't want to try to jump on the stage? `);
			
		con_link(mile.slut > 6, `low slut`, `Yeah, absolutely! `, 410);
		con_link(mile.slut > 4, `low slut`, `Why not? `, 410);
		link(`Eh... maybe some other time. `, 400);
		link(`No, thanks. `, 400);
		link(`No! I'm not a whore! `, 402);
		break;
			
	case 402:
		if(ext.class == 1){
			//TODO OVERHEAR
			txt(`A few guys listened and my defensive statement caused an eruption of a laugh. `);
			npc(2, `Sure! `);
			kat(`I'm not! It's just my character who is a whore! `);
			txt(`But the people were just entertained by my overreaction. `);
			lys(`So you think we're just whores!?! `);
			txt(`One of the girls listened too. `);
		}else{
			txt(`One of the girls listened to our dialogue. `);
			lys(`So you think we're just whores!?! `);
		}
		mile.goth_lys = 1;
		txt(`It was a stunningly beautiful redhead, with a gorgeous body and at that moment she was more undressed than dressed. She was also tall and fit and looked like somebody you do not want to make angry. `);
		kat(`Sorry! I didn't want to offend you and insult your... eh... art. `);
		txt(`I very quickly apologize to escape her wrath. `);
		link(`Back. `, 400);
		break;

	case 410:
		npc(2, `Great! @lys!? Sugar? Can you help the new girl?!`); 
		if(!mile.goth_lys){
			mile.goth_lys = 1;
			txt(`He called a stunningly beautiful redhead, with a gorgeous body and at that moment she was more undressed than dressed. She was also tall and fit and looked like somebody you do not want to make angry. `); 
			lys(`Don't call me <i>sugar</i>, you old perv! What does <i>she</i> want!?`); 
			npc(2, `She wants to dance. Take care of her! And be nice! `); 
			lys(`Yeah, yeah! Come with me, girl. You need to get dressed. `); 
		}else{
			txt(`He called the scary beautiful redhead. `); 
			lys(`Don't call me <i>sugar</i>, you old perv! What does <i>she</i> want!?`); 
			npc(2, `She wants to dance. Take care of her! And be nice! `); 
			lys(`So you changed the opinion and now you don't mind being a whore, girl? `); 
			kat(mile.sub < 3 ? "Yeah, I did. " : "Again, sorry I offended you! "); 
			lys(`...fine! Let's get you dressed. `); 
		}
		kat(`I thought the point is to get undressed? `); 
			
		{
			const slut = sluttiness(PC).details;
			if(slut.breasts || slut.bottomless){
				lys(`Well, there's no point stripping when you're already showing everything! Seriously, try to be more subtle and classy, you look needy and cheap. `); 
				/* }else if(){
					lys(`You're not going into a battle! You don't need that armor! `); 
				*/
			}else{
				lys(`Well, don't take it personally but you look horrible in those ragged rags. I think we can borrow you something better. `); 
			}
				
			if(!slut.breasts && !slut.bottomless && mile.slut < 3){
				kat(`What!?! I can't wear this! Everybody will see my tits!`); 
				lys(`Exactly! Shut up and get dressed! People are waiting!`); 
				if(mile.sub > 2){
					txt(`I felt embarrassed and wished she let me choose something more subtle but she already seemed annoyed and I did not want to argue with her. `); 
				}else{
					txt(`I would prefer to wear something more subtle but the redhead did not offer me any alternative and it was too late to back out. `); 
				}
				txt(`When I was dressed, @lys just pushed me on the stage with no further instructions. `); 
			}else{			
				txt(`@lys offered me loose, semi-transparent garments which covered my body but hinted enough to excited the patrons. I dressed and she pushed me on the stage with no further instructions. `); 
			}
		}
		quickSaveOutfit();
		removeEverything(PC);
		createWear("haremTop", undefined, {color: "red"});
		createWear("thong", undefined, {color: "red"});
		createWear("haremSkirt", undefined, {color: "red"});
		updateDraw(PC);
			
		link(`Dance. `, 411);
		break;
			
	case 411:
		mile.goth_stripper = 1;
		kat(`Wait! What I'm supposed... to do?... `); 
		txt(`I anxiously smiled at the patrons gathered under the stage. `); 
		txt(`Wait, the game actually wants me to dance? I played several dancing games before and this should not be any different but the crowd of almost forty ${mile.immersion>0 ? "guys" : "npc's" } was surely making it feel <i>very</i> different. `); 
		txt(`I began moving to the rhythm. The magic spotlight was on me so I was not able to see the crowd but I could hear their loud cheering and remarks. They ranged from encouraging  <i>"Go, girl!"</i> to less nice ones like  <i>"Show us your tits, slut!"</i>. `); 
		txt(`The live music was energetic, I was in the zone, enjoying the movement and the instant reaction of the crowd - I was rewarded by their horny roaring whenever I made a more sexually suggestive move. `); 
		txt(`I was sure I was no worse than any of the professional dancers who presented their lithe bodies before me. Every eye in the bar was on me, every guy wished to have me spread in his bed. It felt kinda nice. `); 
		link(`Finish. `, 412);
		con_link(mile.slut > -3, `low slut`, `Lose some clothes. `, 413);
		break;
			
	case 412: 
		quickLoadOutfit();
		txt(`I decided to finish. I was breathless and exhausted. The crow was disappointed that I have not shown them more (and a few rude guys in the crowd vocally let me know how much they are disappointed) but I believe I showed them enough. My sheer fabric did not do enough to hide my panties or breasts. `); 
		txt(`The barman gave my share which was a good system, the idea of people slipping their dirty cold silver coins under my panties was not very appealing. `); 
		dance_wage(rewards.dancing);
		link(`Done. `, 400);
		break;
			
	case 413: 
		mile.goth_stripper = 2;
		if(mile.slut < 10) mile.slut++;
		txt(`The horny crowd wanted to see more and I was ready to oblige. I played with my ${choice("top")}, pulling it and stretching, letting them peek, giving them just a flash of my bare breast. Then I swiftly took it off and the men cheered when I casually threw it away. `); 
		txt(`I lasciviously touched my tits and my body, the way they crave to touch me. I continued swaying, then I slipped my thumbs under the waistband of my sheer  ${choice("skirt")} and very slowly pushed down, taking my time. Finally, I dropped the skirt to my ankles. `); 
		txt(`I was blinded by the light but the people watching sounded excited, especially when I thrust with the pelvis, mimicking sexual moves. `); 
			
		if(mile.slut < 4){
			txt(`I had no idea this was inside me! I was dancing nearly completely naked on a stage in front of countless people! And it felt so natural, I was not frozen by nervousness or embarrassment. Not being able to see the viewers and being reassured by the fact this was just a game and none of them was real definitely helped. `); 
		}
			
		link(`Finish. `, 414);
		con_link(mile.slut > 5, `low slut`, `Strip competely. `, 415);
		
		
		effects([{
			id: `top`,
			fce(){
				remove("upper");
			},
		}])
		effects([{
			id: `skirt`,
			fce(){
				remove("lower");
			},
		}])
			
		break;
			
	case 414:
		quickLoadOutfit();
		if(mile.slut > 10) mile.slut--;
		txt(`I was rewarded with loud applause when I searched for the garments I dropped and then disappeared backstage. I was breathless and exhausted.`); 
		txt(`@lys smirked and nodded. `); 
		lys(`That wasn't bad, you look good and know how to move! 			`); 
		txt(`The barman gave my share which was a good system, the idea of people slipping their dirty cold silver coins under my panties was not very appealing. `); 
		dance_wage(rewards.dancing, 1.5);
		link(`Done. `, 400);
		break;
			
			
	case 415: 
		mile.goth_stripper = 3;
		mile.slut++;
		txt(`I was lost in the music, frantically dancing, sweating, touching myself, enjoying the energy emanating from my fans. `);
		txt(`Then was the time for the finale. I turned my back to the spectators, wiggled with my butt, slapped it and then gradually pulled my panties down, building the expectation. The crowd was frenzied when I at last completely exposed to them ${choice("everything")}. `);
		txt(`I squatted down to give them an unobstructed view and then wildly writhed on the stage. The final applause I received was deafening! `);
					
		effects([{
			id: `everything`,
			fce(){
				remove(`panties`);
			},
		}]);
		link(`Done. `, 416);
		break;
			
	case 416:
		quickLoadOutfit();
		txt(`I bowed and then searched for all my scattered garments. @lys smiled at me backstage. `); 
		lys(`That wasn't bad!`); 
		kat(`Thanks!`); 
		lys(`Maybe it was a little bit too good. `); 
		txt(`I sensed a threat in her voice and tried to laugh it away. `); 
		kat(`You don't have to be afraid! I'm not going to become a dancer, I'm on an important mission! `); 
		txt(`The barman gave my share which was a good system, the idea of people slipping their dirty cold silver coins under my panties was not very appealing. `); 
		dance_wage(rewards.dancing, 2);
		link(`Done. `, 400);
		break;
			
			
	//LYSSA
	case 420:
		mile.goth_lys = 2;
		txt(`I noticed @lys was not currently occupied. She was sitting at the bar and drank wine. At the moment she was not bothered by any pervert so I decided to bother her instead. `);
		kat(`Hey, @lys, right?`);
		lys(`Yeah. What was your name, something like @kat or so? Am I pronouncing that right? `);
		kat(`Yeah! Having a break? `);
		lys(`Yeah, I needed a drink and a break. I'm so fed up with those entitled callous perverts! `);
		kat(`At least they pay well, right?`);
		lys(`Not as well as I would deserve! Fuck this place. `);
		kat(`You don't like it here? Why are you staying? `);
		lys(`I'm not staying! This is just a temporary gig. I lost my previous job and I'm looking for a new one. `);
		kat(`You're not an adventurer, aren't you?`);
		lys(`...well, let say I'm something like that. `);
		kat(`Maybe you could join me?`);
		lys(`Sorry, but I'm looking for something better. Even though, I wouldn't mind a minor quest or two to not get out of practice. `);
		kat(`Excellent. I'll let you know! `);
		link(`Back. `, 400);
		break;
			
	case 421: 
		kat(`Hello!`);
		txt(`@lys nodded. `);
		lys(`Hey!`);
		link(`I need your help! `, 422);
		if(mile.goth_arazor >= 2) link(`I am looking for a crew! `, 423);
		link(`Back. `, 400);
		break;
			
	case 422: 
		present( [`lys`, `lys`]	);
		fellowship("lys");
		kat(`I know about a minor adventure you might enjoy!`);
		lys(`Great! `);
		link(`Let's go! `, 400);
		break;
		
	case 423:
		mile.ship_lys = true;
		if(mile.goth_lys < 3){
			kat(`I'm looking for a crew. Do you know anybody with nautical experience? `);
			lys(`Are you crazy? I used to be a pirate!`);
			kat(`You!? A pirate!?`);
			lys(`It's a long story. But if you want me, I am in!`);
		}else{
			kat(`I need a crew for a dangerous mission. We have to save the world! And you have plenty of experience with sailing and ship, haven't you?`);
			lys(`What about the loot? `);
			kat(`Plentiful.`);
			lys(`I'm in!`);
		}
		link(`Meet me at the port! `, 400);
		break;
			
			
		
//ARAZORS TOWER 500
	case 500:
		placetime(`Arazor's tower`, ``);
		//first meeting
		if(!mile.goth_arazor){
			mile.goth_arazor = 1;
			txt(`Finding the Arazor's tower was not easy. It seemed towers were the latest fad and they were everywhere. But finally, I reached the right place. I entered and was welcomed by a very strange-looking woman. `);
			sir(`Welcome to Arazor's tower! How can I help you?!`);
			txt(`She greeted me, her smooth pleasant voice completely emotionless. `);
			kat(`I have to speak with Arazor!`);
			sir(`Do you have arranged an appointment? `);
			kat(`No? `);
			sir(`Do you want to arrange one? `);
			kat(`No, I want to speak with him right now! The fate of the world depends on it, woman! `);
			sir(`Master Arazor is a very busy man. He will not accept anybody without an arranged appointment. `);
			kat(`Fine! Then arrange one. `);
			sir(`It can take several days before there will be a free slot in his schedule. `);
			kat(`Several days!?! Damn!`);
			level_up(`Back. `, 110); //LEVEL UP - IN THE CASE PLAYER SKIPPED VOYAGE!!!
				
		//solved goblins or ghost ship
		}else if( mile.goth_arazor === 1 && (quest.is_closed("goth_goblins") || quest.is_closed("ghost_ship"))  ){
			present(
				[3, `Master Arazor`, `mage`],
			);
			mile.goth_arazor = 2;
			kat(`I really need to talk with Arazor! `);
			sir(`Of course! Master Arazor is ready for your appointment! `);
			kat(`And I won't take no for an answer! It's extremely important! Wait, you said you can meet him? `);
			sir(`Of course! Folow me, please! `);
			link(`Upstairs. `, 510);
				
		//has frist officer 
		}else if(mile.goth_arazor === 2 && (mile.ship_lys || mile.ship_ste) ){
			present(
				[3, `Master Arazor`, `mage`],
			);
			mile.goth_arazor = 3;
			sir(`You are finally here! Go upstairs, Master Arazor wants to talk with you! `);
			link(`Upstairs. `, 520);
			
		//still lookin for fist officer 
		}else if(mile.goth_arazor === 2){
			kat(`I really need to talk with Arazor! `);
			sir(`I am very sorry but Master Arazor is not available at the moment. He told me something about buying a ship? `);
			link(`Back. `, 110);
				
		//scroll found 
		}else if( mile.goth_arazor === 5 || quest.is_done("karged") ){ //todo - legacy 
			mile.goth_arazor = 6;
			quest.closure("karged");
			quest.initiate(`oskyl`, `Explore the Labyrinth of Oskyl and find the Sceptre of Torrywialen`, "reach", {liason: "Arazor"});
			mile.game_progress = 24;
			present(
				[3, `Master Arazor`, `mage`],
			);
				
			txt(`The receptionist immediately told me to go upstairs. Master Arazor excited to hear any news about the Sceptre of Torrywialen He was not disappointed when I told him I did not found anything, except for the ancient scroll. `);
			txt(`He took me in the library and together with @sir we managed to translate it. `);
			//txt(`TODO - minigame`); TODO
			npc(3, `It seem the Sceptre of Torrywialen was hidden in a separate location.`);
			kat(`Where?`);
			npc(3, `In the underground labyrinth on Oskyl.`);
			if(!mile.ship_oskyl){
				mile.ship_oskyl = 1;
				kat(`Where?`);
				txt(`Master Arazor checked the map.`);
				npc(3, `Ynis Oskyl is an unihabited island located north of Gothred, not far from the coast of Beletriad. `);
			}else{
				kat(`Oh, Oskyl. You mean a small unihabited island located north of Gothred, not far from the coast of Beletriad?`);
				npc(3, ` ...I believe so. `);
			}
			kat(`Okay. I'll get the sceptre. `);
			next(`Exit the Game.`);
			//TODO - LEVEL_UP HERE
			if(softbug) link(`#Back. `, 110); 
			break;
				
		//sceptre found
		}else if( mile.goth_arazor === 7){
			mile.goth_arazor = 8;
			quest.closure("oskyl");
			present(
				[3, `Master Arazor`, `mage`],
			);
			txt(`Accompanied by @sir ${ext.follower && ext.follower.key !== "sir" ? `and ${fol_name()} ` : ""}I transported the sceptre from ${mile.ship_name} to the wizard's tower. To my surprise Master Arazor was already patiently waiting for us in the entry hall.`);
			npc(3, `You don't have to tell me anything! I can sense the mighty aura. Show it to me!`);
			txt(`Her barely contained his excitement. I unpacked the ${mile.immersion > 0 ? "the Scepter of Torrywialen" : "the scepter"} and handed it to him. `);
			kat(`What next? The evil overlord is defeated and we won?`);
			npc(3, `No. Fighting him would be pointless - he can't be defeated. But he wants the scepter. `);
			kat(`So we can trick him? Use the scepter as bait? `);
			npc(3, `No, we give it to him.`);
			kat(`As a trojan horse? Can you make it explode, or something like that? `);
			npc(3, `No, as a tribute and proof of allegiance. `);
			kat(`But we have to defeat him! Otherwise, I won't win this game!`);
			npc(3, `No, we have to ally him and help him to conquer the Beletriad and create a new and better world!`);
			kat(`Are you betraying me!?`);
			npc(3, `Isn't that obvious!?`);
			kat(`You're crazy! The evil overlord can't be trusted! Just the scepter won't be enough to convince him to make you one of his commanders!`);
			npc(3, `I believe it will! But just to be sure, I'll bring him your head too! @sir? Take care of it! `);
			txt(`He was so excited to examine the scepter he did not even bother to wait for my demise and believed his homunculus will easily defeat ${we("us", "me")}.`);
			link(`@sir!`, 530);

		//never met sirael v
		}else if(mile.goth_arazor >= 3 && !mile.goth_sirael_v){
			/*
			quickSaveOutfit(effigy.sir);
			present(
				[`sir`, `sirGreen`],
			);
			*/
//TODO ASAP TEST
			quickTempOutfit(`sirGreen`, effigy.sir);
			mile.goth_sirael_v = true;
			sir(`I am very sorry but Master Arazor is not available at the moment. `);
			kat(`@sir!? Why you aren't on the ship? `);
			sir(`I am Sirael VII. You're travelling with my s${String.fromCharCode(105)}ster.`);
			kat(`Oh! ...okay. `);
			link(`Back. `, 110, () => quickLoadOutfit(false, effigy.sir) );
		
		//default sirael v
		}else if(mile.goth_arazor >= 3){
			/*
			quickSaveOutfit(effigy.sir);
			present(
				[`sir`, `sirGreen`],
			);
			*/
//TODO ASAP TEST
			quickTempOutfit(`sirGreen`, effigy.sir);
			kat(`I really need to talk with Arazor! `);
			sir(`I am very sorry but Master Arazor is not available at the moment. `);
			link(`Back. `, 110, () => quickLoadOutfit(false, effigy.sir) );
			
		//default
		}else{
			kat(`I really need to talk with Arazor! `);
			sir(`I am very sorry but Master Arazor is not available at the moment. `);
			link(`Back. `, 110);
		}
		break;
			
	//initial meeting
	case 510:
		txt(`The receptionist led me upstairs to a small but luxuriously furbished audience room. The carpet was red and thick, shelves and bookcases were made of carved dark wood with ivory inlay. `);
		npc(3, `Greetings!`);
		txt(`The mage nodded and beckoned me to sit down in a padded chair. I quickly summed up my story so far. Arazor was staring out of the window and every once in while nodded. `);
		npc(3, `You're not bringing good news. The return of Evil Overlord? We wanted to believe he was gone for good. It was a very dark age when he tried to conquer the whole world centuries ago. `);
		txt(`The strange servant brought us a teapot and tiny china cups. When she left, the sorcerer smiled at me:`);
		npc(3, `Isn't she beautiful? `);
		kat(dyke() ? `Sure!` : `Yeah! She's pretty hot! Where did you find her? `);
		npc(3, `I made her myself!`);
		txt(`He shared with pride. `);
		link(`Nice? `, 511);
		break;
			
	case 511:
		npc(3, `I wouldn't believe you, even despite the recommendation you have from my old friend ${mile.feyd_keryk_name}. But strange things are happening. Villages are raided by goblins, ships are getting lost or return without crew... And the star signs don't lie. `);
		kat(`What should we do next? `);
		npc(3, `We have to acquire the Sceptre of Torrywialen. It should be enough to defeat him. `);
		kat(`Where we can find it?`);
		npc(3, `Its current location is unknown. `);
		kat(`Awesome? `);
		npc(3, `But his last know wielder was Aquilonian king Tarquin XIX. `);
		kat(`Where do we find him? ${mile.immersion ? "Wasn't Aquilonia destroyed like centuries ago? " : ""}`);
		npc(3, `His tomb is on Karged. I'll get you a ship. `);
		kat(`A ship!?! I have no idea how to drive a ship! `);
		npc(3, `Then find people who can! `);
			
		quest.initiate(`karged`, `Explore the Tombs of Karged and find the Sceptre of Torrywialen`, "reach", {liason: "Arazor"});
		mile.game_progress = 22;
		
		if(ext.follower){
			link(`Leave. `, 512)
		}else{
			next(`Exit the Game.`);
			if(softbug) link(`#Back. `, 110); 
		}
		break;
		
	case 512:
		if(ext.follower.key === "gal"){
			txt(`I left the tower and thought about the problem. My companion @gal was not an expert on ships but we already sailed on one and everything almost went well.  `);
			link(`Ask him to join. `, 523); 
		}else if(ext.follower.key === "ste"){
			txt(`I left the tower and thought about the problem. My companion @ste used to be a captain, was he not? `);
			link(`Ask him to join. `, 524); 
		}else if(ext.follower.key === "lys"){
			txt(`I left the tower and thought about the problem. Maybe I should ask my companion @lys?`);
			link(`Ask her to join. `, 525); 
		}
		next(`I will not need ${him()}.`, () => dismiss() );
		if(softbug) link(`# I will not need ${him()} (#back) `, 110, () => dismiss()); 
		break;
			
	case 523: 
		mile.ship_gal = true;
		gal(`No. I have enough of ships. `);
		kat(`Come on! It's going to be fun! One quick adventure, to Karged and back! And you still owe me for saving your life and helping you escape.`);
		gal(`Fine!`);
		next(`Exit the Game.`);
		link(`#Back. `, 110); 
		break;
			
	case 524:
		mile.ship_ste = true;
		ste(`What kind of mission? `);
		kat(`We'll visit an island and save the world. And I need somebody who knows how to drive a ship. `);
		ste(`This sounds too good to be true. The ship will sink and we'll all die, won't we? `);
		kat(`Possibly. Are you in? `);
		ste(`Sure! `);
		next(`Exit the Game.`);
		link(`#Back. `, 110);
		break;
			
	case 525:
		mile.ship_lys = true;
		if(mile.goth_lys < 3){
			kat(`I'm looking for a crew. Do you know anybody with nautical experience? `);
			lys(`Are you crazy? I used to be a pirate!`);
			kat(`You!? A pirate!?`);
			lys(`It's a long story. But if you want me, I am in!`);
		}else{
			kat(`I need a crew for a dangerous mission. We have to save the world! And you have plenty of experience with sailing and ship, haven't you?`);
			lys(`What about the loot? `);
			kat(`Plentiful.`);
			lys(`I'm in!`);
		}
		next(`Exit the Game.`);
		link(`#Back. `, 110); 
		break;

	//crew found
	case 520:
		npc(3, `Greeting! I have very good news for you! Do you have a crew? `);
		kat(`Well, I found several people...`);
		npc(3, `I have a ship for you! It's a nice caravel, I bought her extremely cheap at an auction but she should be perfectly seaworthy. `);
		if( mile.goth_ghost_ship ){
			txt(`Oh no! I began suspecting something horrible. `);
		}else{
			kat(`Sounds great! `);
		}
		npc(3, `Gather your people and equipment, you'll find her at the port! Then set sail and travel west!`);
		kat(`You'll just give me a caravel? `);
		npc(3, `I'm not giving you anything! You'll return her without any damage! And @sir V will keep you in check!`);
				
		link(`I'll try my best to not sink your caravel. `, 521); 
		link(`I'm almost offended you don't trust me! `, 521); 
		break;
			
	case 521:
		mile.ship_sir = true;
		mile.ship_karg = 1;
		txt(`Arazor frowned and sent me away. His receptionist was waiting for me. `);
		sir(`I am @sir! I will accompany you on your jorney!`);
		if(mile.sub < 1){
			kat(`Fine!`);
			sir(`Is something wrong? `);
			kat(`I don't like anybody watching over my shoulder! I can handle this quest myself!`);
			sir(`Do not worry! I will not interfere in your leadership! `);
		}else{
			kat(`Your help will be appreciated. `);
		}
		link(`Back. `, 110); 
		break;
			
	//final confronation
	case 530:
		kat(`@sir!`);
		sir(`I'm sorry, captain.`);
		txt(`She sadly shrugged.`);
		if(ext.follower?.key === "gal" && mile.ship_sirgal > 0){
			gal(`You don't have to do this, @sir! `);
			sir(`Please, leave, @gal! I don't want to hurt you too!`);
			gal(`You'll have to kill us both!`);
		}
		kat(`I thought we are friends! `);
		sir(`We.. we are, @kat! But I.. I can't disobey!`);
				
				
		roll(`Let ${we("us", "me")} go, @sir!`, "char", -mile.goth_sir_indy+2, 531, 532);
		link(`Suprisingly attack her while she's distracted and emotionally conflicted.`, 533);
		break;
			
	case 531:
		sir(`I... run! `);
		kat(`Let's go!`);
		sir(`I have to stay here. I'll try to explain everything to Master Arazor.`);
		kat(`He won't listen!`);
		sir(`At least I will slow him down. But you have to run, now! He is Master Arazor! He controls the Gothred Guard and the Navy!`);
		kat(`Thank you, @sir.`);
		sir(`Just go!`);
		link(`Run.`, 540);				
		break;
			
	case 532:			
	case 533:
		txt(`${We()} defeated @sir. `);
		txt(`Now it was time to run. Master Arazor was both a powerful mage and an influential citizen of Gothred. We had to escape before he will be able to alert authorities.`);
		link(`Run!`, 540);	
		break;
			
	case 540:
		txt(`${We()} sprinted throught the city until we reached the port and ${mile.ship_name}`);
		if(ext.follower?.key !== "lys"){
			lys(`What the hell is going on? Two minutes ago the semaphores went wild!`);
		}else{
			ste(`What the hell is going on? Two minutes ago the semaphores went wild!`);
		}
		txt(`I noticed a group of soldiers in black approaching the pier. `);
		kat(`We have to go, now! NOW!`);
		if(ext.follower?.key !== "lys"){
			lys(`Aye! CUT THE ANCHOR!`);
		}else{
			ste(`Aye! CUT THE ANCHOR!`);
		}
		txt(`We barely made it. War galleys and caravels of Gothred navy were already raising their sails to pursue us. `);
		link(`Chase. `, 541);	
		break;
			
			
	case 541:
		txt(`The weather was quickly getting worse which was a good thing. The thunder interrupted us several times when I was explaining to the crew what was going on. Just when I finished, it started raining. `);
		txt(`The storm soon hit us with full force. ${mile.ship_name} was wildly rocking on the raging sea but pursuing us in such nasty weather was impossible. Or we hoped so. `);
		txt(`We planned to reach the Dewyrain coast of Beletriad and either Sintre or Feyd Mora. The royal navy was understaffed and underfunded but captains of the Gothred navy probably would not be willing to start a war for a couple of refugees. `);
		txt(`The next day the weather calmed. And far on the horizon, we noticed three black dots. They were caravels of the Gothred navy and very slowly they were gaining on us. The sluggish chase lasted the whole day. We almost made it. Almost. `);
		txt(`The sun was setting down when ${mile.ship_xo === "sir" ? name.ste : name[mile.ship_xo]} ordered to clear ${mile.ship_name} for action. But the outcome against such overwhelming odds was obvious.`);
		link(`Escape. `, 542);	
		break;
			
	case 542:
		//	if(mile.ship_lys && mile.ship_ste){
		lys(`We need to talk. `);
		kat(`About what?!`);
		ste(`We have a plan. Take the jolly and escape. It's getting darker and the enemy might not notice such a small boat, especially when we distract them. And the shore is not far, you can easily make it!`);
		kat(`I don't want to leave you behind!`);
		lys(`Don't worry about us! They want your head, not ours! In the worst case, we surrender and I use my charm to convince them we had no idea what you were up to. `);
		if(mile.ship_gal){
			ste(`But the lives of you and prince @gal are too important! You're the chosen one!`);
		}else{
			ste(`And you're the chosen one! Your life is too important! You're the chosen one!`);
		}
		kat(`My actions were so far pretty contra-productive. I only made the situation worse!`);
		lys(`Now you have even better motivation to fix it! You have to stop the evil overlord! `);
		kat(`Fine! Take a good care of my ship!`);
		if(mile.ship_power_couple){
			lys(`Haha, don't worry! We planned to wait until you finish your quest and leave and then steal her anyway!`);
			txt(`@ste and @lys laughed. `);
		}else{
			lys(`Don't worry! I planned to steal the ship anyway once you finish your quest and leave. `);
			ste(`What?! I planned that too!`);
			lys(`Jinx? `);
		}
		//	}
		link(`Jolly. `, 1000);	
		break;
			

//PORT
	case 600:
		placetime(`Port`, ``);
		txt(`The port was the heart of Gothed. The naval trade between the Dewyrain coast of Beletriad, the Southern Archipelago and the Eastern Isles was the sole source of its wealth. There were many anchored caravels and beamy cogs flying colors or dozens different kingdoms, lords and cities. The trade routes and the independence of Gothred were protected by an impressive navy, I could see several vessels, from light patrol galleys to massive quinqueremes with two tall masts and hundreds oars. `);
		if(mile.goth_arazor >= 4){
			txt(`Next to one pier was mooring my ship, a small but fast caravel <i>${mile.ship_name}</i>. It was ready to set sail. `);
		}
		
		if(!mile.goth_ghost_ship){
			mile.goth_ghost_ship = 1;
			quest.initiate("ghost_ship", "Explore mysterious ship <i>Barnham Dyn. </i>", "reach");
			present(
				[1, `Port Officer`, `gothOfficer`],
				[2, `Bystander`, `commoner`],
				[3, `Another Bystander`, `commoner`],
			);
				
			txt(`But one particular ship immediately attracted my attention. I was not sure why, there was just something eerie about her. She was not anchored, she was just floating several hundred meters from the closest pier. Her sails were torn and rigging damaged and it looked like nobody was on the deck. I was not the only observer. `);
			kat(`What is that? `);
			npc(2, `<i>Barnham Dyn. </i> She arrived yesterday and she seems to be completely abandoned. `);
			kat(`Seems to be? Nobody checked? `);
			npc(2, `No. So far nobody dared. She's probably cursed. `);
			npc(3, `Or the whole crew was killed by the plague. `);
			npc(1, `It isn't plague, they would fly the yellow jack! What about you? You look like an adventurer. We can pay you if you tell us what's going on. `);
			if(ext.follower){
				fol(`If we survive the plague and the curse to tell the story.`);
				enter(`Sure. Get me a rowboat and I'll check the creepy ship! `, `ghost_ship`, 610, 611, `ghost_ship`);
				link(`I'll consider it! `, 600);
			}else{
				npc(2, `She shouldn't go alone! `);
				txt(`Advised one of the bystanders. But there were no volunteers. `);
				link(`I will be back! `, 600);
				enter(`I can handle it myself! Get me a rowboat! `, `ghost_ship`, 610, 611, `ghost_ship`);
				link(`I don't have time for this right now. `, 600);
			}

		}else{
			
			if(mile.goth_arazor === 2){
				if(mile.ship_gal){
					gal(`So? Where is your ship? `);
					kat(`Well, I have to ask the wizard. `);
				}else if(mile.ship_lys){
					lys(`So? Where is your ship? `);
					kat(`Well, I have to ask the wizard. `);
				}else if(mile.ship_ste){
					ste(`So? Where is your ship? `);
					kat(`Well, I have to ask the wizard. `);
				}	
				if(mile.ship_ste) link(`@ste, come with me. `, 101, () => fellowship("ste") );
				if(mile.ship_lys) link(`@lys, come with me. `, 101, () => fellowship("lys") );
				if(mile.ship_gal) link(`@gal, come with me. `, 101, () => fellowship("gal") );
				
			}else if(mile.goth_arazor === 3){
				link(`My new ship. `, 650);
			}else if(mile.goth_arazor >= 4){
				chapter(`${mile.ship_name}. `, `ship`);
			}
		
			if(mile.goth_ghost_ship === 1){
				enter(`Explore the mysterious ship. `, `ghost_ship`, 610, 611, `ghost_ship`);
			}
		
		}
			
		link(`Back. `, 110);
		break;
		
	//GHOST SHIP
	case 610:
		quest.closure("ghost_ship");
		mile.goth_ghost_ship = 2;
		present(
			[1, `Port Officer`, `gothOfficer`],
			[2, `Bystander`, `commoner`],
			[3, `Another Bystander`, `commoner`],
		);
		 
		txt(`Stunned officer listened to my dreadful story. `);
		npc(1, `That's horrible! You were lucky you survived!  `);
		kat(`What he meant by thing under the waves? `);
		txt(`The soldier made a warding sign. `);
		npc(1, `We don't talk about it, definitely not so close to the shore... But you definitely deserve the money! `);
		txt(`He quickly changed the topic. `);
		kat(`Absolutely! `);
		npc(1, `I'm just not sure what to do next. `);
		if(ext.follower){
			fol(`The ship has to be burned down! It's the only way to kill that monster! `);
		}else{
			kat(`The ship has to be burned down! It's the only way to kill that monster!`);
		}
		npc(1, `But... such a nice ship... I wish there was another way... `);
		wage("goth_ghost_ship");
		link(`Port. `, 600);
		break;

	case 611:
		present(
			[1, `Port Officer`, `gothOfficer`],
			[2, `Bystander`, `commoner`],
			[3, `Another Bystander`, `commoner`],
		);
		npc(2, `So?!?`);
		npc(3, `Is the crew dead? `);
		npc(1, `What did you find!?! `);
		txt(`I shortly summed what I found so far. `);
		npc(2, `That's everthing?!? `);
		npc(1, `Weird. But you have to go back and find out more! `);
		link(`Port. `, 600);
		break;
				
	//MY SHIP			
	case 650:
		mile.ship_name = "Plague Storm";
		if(mile.goth_ghost_ship === 2){
			txt(`To my huge relief it was not <i>Barnham Dyn</i>. It took me a minute to realise it was another infamous ship, <i>Plague Storm</i>! Only a few days ago I (with a little @gal's help) liberated her from pirates and then she was seized by Gothred authorities. `);
		}else{
			txt(`I immediately recognised that ship! It was <i>Plague Storm</i>! Only a few days ago I (with a little @gal's help) liberated her from pirates and then she was seized by Gothred authorities. `);
		}
		txt(`Maybe I should give her a new name? `);
		link(`Rename the ship. `, 651);
		link(`Nah, the name is fine. `, 660);
		break;
				
	case 651:
		popup.prompt(`New name of the ship: `, mile.ship_name, (a) => mile.ship_name = a, 
			[
				"Serendipity", "Lydia", "Destiny", "Wanderlust", "Boaty McBoatface", "Sea Quest", "Black Pearl", "Osprey",
				"Silver Doe", "Perseverance", "Calypso", "Mad Dragon", "Sanguine Delivery", "Dishonorable Insanity", "Elusive Grail",
			],
		);
		//txt(`<i>${mile.ship_name}</i>. Yeah. That will do!`);
		txt(`Yeah, that will be a good name!`);
		link(`Next. `, 660);
		break;
				
	case 660: 
		sir(`We can call the ship <i>${mile.ship_name}</i> for now. Master Arazor can always change the name later. `);
		if(mile.ship_gal){
			if(mile.goth_gal_sex){
				gal(`I hoped I'll never see this ship again! So many bad memories! But there are a few nice ones!`);
				txt(`He winked at me. `);
			}else{
				gal(`I hoped I'll never see this ship again! But I guess with you in charge it won't be as bad!`);
			}
		}
				
		if(mile.ship_ste){
			ste(`Oh gods! This was my old ship!`);
			kat(`<i>Plague Storm</i> was your ship? `);
			ste(`It wasn't named <i>Plague Storm</i> when I got it build! You're very lucky, she's the finest caravel in the Dewyrain sea!`);
			kat(`I think I met your former crew.`);
			ste(`How it went? `);
			kat(`I'm afraid they are all dead.`);
			ste(`Good!`);
		}
				
		if(mile.ship_lys && mile.ship_ste){
			txt(`But that was not the biggest surprise. When @ste saw @lys he was stunned. `);
			ste(`M... Meril!?!`);
			lys(`Hi, @ste!`);
			kat(`Do you know each other?!`);
			ste(`That whore stole my ship!`);
			lys(`I didn't steal anything! I mean, I stole a lot of things but definitely not your ship! Your failure as a captain was only your own fault!`);
			ste(`You were conspiring behind my back!`);
			lys(`I was not! `);
			txt(`What a funny coincidence. `);
		}else if(mile.ship_lys){
			lys(`It feels amazing to be back on a ship. `);
		}
		link(`Next. `, 670);
		break;
				
	case 670:
		mile.goth_arazor = 4;
		mile.game_progress = 23; 
		txt(`One more thing had to be decided - who will be my first officer, second-in-command after me. `);
		if(mile.ship_ste) link(`@ste (navigation bonus). `, 600, () => mile.ship_xo = "ste");
		if(mile.ship_lys) link(`@lys (combat bonus). `, 600, () => mile.ship_xo = "lys");
		if(mile.ship_gal) link(`@gal. `, 600, () => mile.ship_xo = "gal");
		if(mile.ship_sir) link(`@sir. `, 600, () => mile.ship_xo = "sir");
				
		snapshot(`goth`, `The captain of ${mile.ship_name}`);
		break;
				
	
//WHORING
	case 900:
		if(mile.goth_whoring <= 3){
			txt(`I followed @lys's advice how to quickly make a lot of money and went to the shadier part of the city. `);
			txt(`I was really conflicted. I do not think I was a bashful person - I surely was not ashamed of my body and enjoyed having sex. Some even might say I was a slut. `);
			txt(`But becoming an actually whore and having sex with strangers for money? Was it too far? Of course, they were not real people, just NPC, generated to satisfy the perverted urges of horny players. But still... was it a good idea? `);

			if(mile.slut > 10){ //TODO - too much???
				link(`It was not far enough! Experincing being a whore could be fun! `, 900, () => {
					mile.slut++;
					mile.slut++;
					mile.goth_whoring = 5;
				})
			}

			link(`There was nothing wrong with trying it once or twice, was it? `, 900, () => {
				mile.slut++;
				mile.goth_whoring = 4;
			})

			link(`I was not sure... I had to reconsider it... `, 110, () => {
				mile.goth_whoring = 3;
			})

			link(`This was a terrible idea! I was no whore! `, 110, () => {
				mile.slut--;
				mile.goth_whoring = -1;
			})
		}else{
			whoring_initial();
		}
		break;
			
	case 910:
		whore_haggling();
		break;	
	case 920:
		whore_bj();
		break;


		//ENDING
	case 1000:
		new_act(3);
		set.reentry("goth", 1000);
		mile.game_progress = 28;
			
		if(mile.ship_gal){
			txt(`So we, I and @gal escaped on the jolly. It was quickly getting dark and we were not able to watch the battle. We could only see flames. `);
			gal(`And we're again alone, carried away by sea. `);
			kat(`You're a prince? `);
			gal(`Yeah. `);
			kat(`Why you haven't told me? `);
			gal(`You didn't ask. `);
			kat(`What should we do now?`);
			gal(`I have no idea. Reach the shore, I guess. `);
		}else{
			txt(`So I alone escaped on the jolly. It was quickly getting dark and I was not able to watch the battle. I could only see flames. `);
		}
			
		txt(`#The end of the current virtual-game content. `);
		next(`Exit the game. `);
		break;	
		
	
			
	}
}
