//DISPLAYED WHEN PLAYER DIES

//import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";
//import {txt, npc, kat, css} from "Loop/text";
import {link} from "Loop/engine";
import {css} from "Loop/text";
import {autoload} from "System/save";
import {bell} from "System/sounds";
import {DISPLAY} from "Gui/gui";

export const death = (index) => {
	bell.play();
	css(`bloody`, `You are dead!`);
	link(`Load latest save. `, null, undefined, ()=>{
		autoload();
		ext.stats.deaths++; //lol, won't get saved otherwise
	});
	DISPLAY.dungeon_leave();
}
