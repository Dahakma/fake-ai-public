/*
import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";

import {txt, npc, kat} from "Loop/text";
import {gal, ste, lys} from "Loop/text";

import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s} from "Loop/text";


import {createWear, crew,  quickSaveOutfit, quickLoadOutfit} from "Avatar/index";
import {updateDraw, updateMods, sluttiness, clothes, showLess, showAll, wears, remove} from "Avatar/index";
import {clone, shuffle, clamp} from "Libraries/dh";
import {ra} from "Libraries/random";
import {class_based, roll, dance, fellowship, BARS} from "Virtual/index";
import {seven} from "Minigames/seven";
import {enter} from "Dungeon/engine";
import {autosave} from "System/save";
import {quest} from "Virtual/quest"; 
*/


export const battle = (index) => {
	/*
	
	function change(what, value){
		//BARS.change(what, value);
		counter[what] += value;
		counter[what] = clamp(counter[what], 0, counter[`max_${what}`]);
	}
	
	function attack_action(){
		if(index > 400 && index < 500){
			let sails, deck, hull;
			if(counter.balista === 1){
				sails = counter.ball_half;
				deck = counter.ball_half;
				hull = counter.ball;
			}else if(counter.balista === 2){
				sails = counter.dart;
				deck = counter.dart;
				hull = counter.dart_half;
			}
				
			if(ra.g < counter.aim){
				switch(index){
					case 401: 
						change(`e_rigging`, -sails);
						break;
					case 402: 
						change(`e_crew`, -deck);
						break;
					case 403: 
						change(`e_hull`, -hull);
						break;
				}
					
				if(counter.balista === 1){
					switch(index){
						case 401: 
							txt(`The iron ball narrowly missed the mast but tore apart their sail. `);
							break;
						case 402: 
							txt(`The iron ball hit the enemy deck, ripping apart all things and men standing in it's way. `);
							break;
						case 403: 
							txt(`The iron ball hit their hull just above the water line, causing a lot of damage. `);
							break;
					}
				}else if(counter.balista === 2){
					switch(index){
						case 401: 
							txt(`The sharp darts brutally tore apart enemy sails, shrouds and stays. `);
							break;
						case 402: 
							txt(`The sharp darts swept the deck of enemy ship, slaughtering everybody in their way. `);
							break;
						case 403: 
							txt(`The darts jabbed into the hull, causing some light but still nice damage. `);
							break;
					}
				}
			 
			}else if(ra.g < counter.aim){
				if(counter.balista === 1){
				
				
				}else if(counter.balista === 2){
					
				};
			 	
			}else{
				if(counter.balista === 1){
					txt(`The iron ball harmlessly fell into water behind their stern. `);
				}else if(counter.balista === 2){
					txt(`All the darts completely missed the enemy ship. `);
				}
			}
			
			counter.balista = 0;
		}
	}
	
	
	
	
	

	
	function attack_ai_action(){
		const index = counter.enemy;
		if(index > 400 && index < 500){
			let sails, deck, hull;
			if(counter.e_balista === 1){
				sails = counter.e_ball_half;
				deck = counter.e_ball_half;
				hull = counter.e_ball;
			}else if(counter.e_balista === 2){
				sails = counter.e_dart;
				deck = counter.e_dart;
				hull = counter.e_dart_half;
			}
				
			if(ra.g < counter.aim - 0.15){
				switch(index){
					case 401: 
						change(`rigging`, -sails);
						break;
					case 402: 
						change(`crew`, -deck);
						break;
					case 403: 
						change(`hull`, -hull);
						break;
				}
					
				if(counter.e_balista === 1){
					switch(index){
						case 401: 
							txt(`The enemy shot tore apart our sails. `);
							break;
						case 402: 
							txt(`The ball launched by the enemy ship crushed our deck, killing or incapacitating several sailors. `);
							break;
						case 403: 
							txt(`I could feel how the enemy ball brutally hit the hull under me. I vainly hoped it did not cause too much damage. `);
							break;
					}
				}else if(counter.e_balista === 2){
					switch(index){
						case 401: 
							txt(`The sharp darts brutally tore apart our sails, shrouds and stays. `);
							break;
						case 402: 
							txt(`The unstoppable swarm of deadly darts flew across our deck, slaughtering or maiming many our sailors. `);
							break;
						case 403: 
							txt(`The darts jabbed into the hull, causing some light but still unpleasant damage. `);
							break;
					}
				}
			 
			}else if(ra.g < counter.aim){
				if(counter.balista === 1){
				
				
				}else if(counter.balista === 2){
					
				};
		 
			}else{
				if(counter.e_balista === 1){
					txt(ra.array([`I reflexively dodged but the iron ball flew over our heads without hitting anybody or anything important. `, `The heavy iron ball launched by the enemy completely missed our ship. `]));
				}else if(counter.e_balista === 2){
					txt(`Terrifying deadly enemy darts completely missed our ship. `);
				}
			}
			
			counter.e_balista = 0;
		}
	}
	
	
	function standard_action(){
		switch(index){
			case 201:
				txt(`We tried to close with full speed. `);
				change(`distance`, -counter.speed)
				break;
			case 202:
				txt(`We carefully tried to shorten our distance. `);
				change(`distance`, -Math.round(counter.speed/2));
				break;
			case 203:
				txt(`We carefully maintained our distance `);
				change(`distance`, Math.round(counter.speed/2))
				break;
			case 204:
				txt(`We tried to break away. `);
				change(`distance`, counter.speed);
				break;
				
				
			case 301: 
				txt(`I ordered the crew to load the balista with heavy iron ball. `);
				counter.balista = 1;
				break;
				
			case 302: 
				txt(`I ordered the crew to load the balista with a set of sharp steel darts. `);
				counter.balista = 2;
				break;
		}	
	}
	
	
	
	function standard_ai_action(){
		const index = counter.enemy;
		switch(index){
			case 201:
				txt(`The enemy ship was sailing against us at full speed. `);
				change(`distance`, -counter.speed)
				break;
			case 202:
				txt(`The enemy ship was slowly getting closer.. `);
				change(`distance`, -Math.round(counter.speed/2));
				break;
			case 203:
				txt(`The enemy ship was keeping her distance. `);
				change(`distance`, Math.round(counter.speed/2))
				break;
			case 204:
				txt(`The enemy ship tried to break away. `);
				change(`distance`, counter.speed);
				break;
				
			case 300: 
				txt(`I could see enemy crew loading their balista. `);
				break;
		}	
	}
	
	function calculate_stats(){		
		counter.speed = 0;
		let spee = "";
		if(counter.rigging > 2/3 * counter.max_rigging){
			counter.speed = 4;
			spee = `<br>+ 4  caravel with intact rigging`;
		}else if(counter.rigging > 2/3 * counter.max_rigging){
			counter.speed = 2;
			spee = `<br>+ 2 caravel with damaged rigging`;
		}else if(counter.rigging){
			counter.speed = 1;
			spee = `<br>+ 1 caravel with destroyed rigging`;
		}else{
			counter.speed = -1;
			spee = `<br>- 1 dismasted caravel`;
		}
		
		if(mile.skipper === "ste"){
			counter.speed++;
			spee += `<br>+ 1 @ste at helm`;
		}
		
		if(counter.speed < 0) counter.speed = 0;
		
		counter.aim = 0.1 +  (1 - counter.distance/counter.max_distance);
		counter.ball = 5 + (mile.skipper === "lys" ? 1 : 0);
		counter.ball_half = Math.ceil(counter.ball / 2);
		
		counter.dart = 5;
		
		if(counter.distance <= 5){
			counter.dart = 6;
		}else if(counter.distance <= 10){
			counter.dart = 5;
		}else if(counter.distance <= 15){
			counter.dart = 4;
		}else if(counter.distance <= 20){
			counter.dart = 3;
		}else if(counter.distance <= 25){
			counter.dart = 2;
		}else{
			counter.dart = 1;
		}
		
		if(mile.skipper === "lys") counter.dart++;
		counter.dart_half = Math.ceil(counter.dart / 2);
		
		return {spee};
	}
	
	
	
	
	function calculate_ai_stats(){		
		counter.e_speed = 0;
		if(counter.e_rigging > 2/3 * counter.max_e_rigging){
			counter.e_speed = 4;
		}else if(counter.e_rigging > 2/3 * counter.max_e_rigging){
			counter.e_speed = 2;
		}else if(counter.e_rigging){
			counter.e_speed = 1;
		}else{
			counter.e_speed = -1;
		}
		if(counter.e_speed < 0) counter.e_speed = 0;
		
		//counter.aim = 0.1 +  (1 - counter.distance/counter.max_distance);
		counter.e_ball = 5;
		counter.e_ball_half = Math.ceil(counter.e_ball / 2);
		
		counter.e_dart = 5;
		
		if(counter.distance <= 5){
			counter.e_dart = 6;
		}else if(counter.distance <= 10){
			counter.e_dart = 5;
		}else if(counter.distance <= 15){
			counter.e_dart = 4;
		}else if(counter.distance <= 20){
			counter.e_dart = 3;
		}else if(counter.distance <= 25){
			counter.e_dart = 2;
		}else{
			counter.e_dart = 1;
		}
		
		counter.e_dart_half = Math.ceil(counter.e_dart / 2);
	}
	
	
	function ai_decision(){
		if(counter.ai === "simple"){
			if( 
				(counter.e_hull < 1/3 * counter.max_e_hull)
				|| (counter.e_crew < 1/3 * counter.max_e_crew)
				|| (counter.e_rigging < 1/3 * counter.max_e_rigging)
			){
				counter.enemy = 204;
			}else{
				 if(counter.distance > 24){
					counter.enemy = 201;
				}else{
					if(ra.g < 0.65){
						if(!counter.e_balista){
							counter.e_balista = ra.g < 0.5 ? 1 : 2;
							counter.enemy = 300;
						}else{
							if(counter.e_balista === 1){
								counter.enemy = ra.array([401, 402]);
							}else if(counter.e_balista === 2){
								counter.enemy = ra.array([401, 402, 403, 403]);
							}
						}
					}else{
						counter.enemy = ra.array([201, 202, 203, 204]);
					}
				}
			}
		}
	}
	
	
	
	const vessels = {
		caravel: {
			type: "caravel",
			rigging: 20,
			crew: 15,
			hull: 20,
		},
		hunter: {
			type: "caravel",
			ai: "hunter",
			rigging: 15,
			crew: 10,
			hull: 15,
		},
		killer: {
			type: "caravel",
			ai: "killer",
			rigging: 10,
			crew: 15,
			hull: 15,
		},
		simple: {
			type: "caravel",
			ai: "simple",
			rigging: 15,
			crew: 15,
			hull: 15,
		},
	}
	
	function load_vessels(me, enemy){
		console.log(vessels);
		console.log(vessels[me]);
		
		counter = {
						
			type: vessels[me].type,
			
			rigging: vessels[me].rigging,
			max_rigging: vessels[me].rigging,
			
			hull: vessels[me].hull,
			max_hull: vessels[me].hull,
			
			crew: vessels[me].crew,
			max_crew: vessels[me].crew,
			
			
			e_type: vessels[enemy].type,
			ai: vessels[enemy].ai,
			
			e_rigging: vessels[enemy].rigging,
			max_e_rigging: vessels[enemy].rigging,
			
			e_hull: vessels[enemy].hull,
			max_e_hull: vessels[enemy].hull,
			
			e_crew: vessels[enemy].crew,
			max_e_crew: vessels[enemy].crew,
			
			distance: 18,
			max_distance: 30,
		}	
	}
	
	
	
	
	
	if(index === 101){


mile.skipper = `ste`;

			set.game();
			console.log(counter);
			load_vessels("caravel", "simple")
			console.log(counter);
			
//TODO 
//			
			BARS.create([{
				id: "rigging",
				label:	"Rigging",
				value:	counter.rigging,
				max:	counter.max_rigging,
				color:	140,
			},{
				id: "crew",
				label:	"Crew",
				value:	counter.crew,
				max:	counter.max_crew,
				color:	0,
			},{
				id: "hull",
				label:	"Hull",
				value:	counter.hull,
				max:	counter.max_hull,
				color:	35,
			},{
				id: "distance",
				label:	"Distance",
				value:	counter.distance,
				max:	counter.max_distance,
				color:	190,
			},{	
				id: "e_rigging",
				label:	"Enemy Rigging",
				value:	counter.e_rigging,
				max:	counter.max_e_rigging,
				color:	145,
			},{
				id: "e_crew",
				label:	"Enemy Crew",
				value:	counter.e_crew,
				max:	counter.max_e_crew,
				color:	4,
			},{
				id: "e_hull",
				label:	"Enemy Hull",
				value:	counter.e_hull,
				max:	counter.max_e_hull,
				color:	39,
			}]);	

		txt(`A caravel with a black pirate flag was sailing in our direction. ${name[mile.skipper]} called me and cleared the ship for action. `);
		
	}


console.warn(index);

	standard_action();
	attack_action();
	
	attack_ai_action();
	standard_ai_action();
	
	calculate_ai_stats();
	ai_decision();
	
	const {spee} = calculate_stats();
	
	
	
		
	
	
	//counter.dart = 5 + (mile.skipper === "lys" ? 1 : 0);
	//write(
			
			
	
			
			//if(counter.fire
			
			
			if(counter.speed > 0){
				alt_link(`Quickly Close. `, 201, `Shorten the distance by ${counter.speed} ${spee}`);
				alt_link(`Close. `, 202, `Shorten the distance by ${Math.round(counter.speed/2)}`);
				alt_link(`Fall back. `, 203, `Widen the distance by ${Math.round(counter.speed/2)}`);
				alt_link(`Quickly fall back. `, 204, `Widen the distance by ${counter.speed} ${spee}`);
			}
			if(counter.distance === counter.max_distance){
				alt_link(`Escape. `, 110, `Leave the combat`);
			}
			
			if(!counter.balista){
				alt_link(`Load balista with darts. `, 302, `At this distance, ${Math.round(counter.aim * 100)} % chance to causes ${counter.dart_half} damage to hull or ${counter.dart} to crew or rigging  `);			
				alt_link(`Load balista with ball. `, 301, `At this distance, ${Math.round(counter.aim * 100)} % chance to causes ${counter.ball} damage to hull or ${counter.ball_half} to crew or rigging  `);
				
			}else{
				let sails, deck, hull;
				if(counter.balista === 1){
					sails = counter.ball_half;
					deck = counter.ball_half;
					hull = counter.ball;
				}else if(counter.balista === 2){
					sails = counter.dart;
					deck = counter.dart;
					hull = counter.dart_half;
				}
				
				alt_link(`Launch balista at enemy sails. `, 401, `${Math.round(counter.aim * 100)} % chance to causes ${sails} damage  `);
				alt_link(`Launch balista at enemy deck. `, 402, `${Math.round(counter.aim * 100)} % chance to causes ${deck} damage  `);
				alt_link(`Launch balista at enemy hull. `, 403, `${Math.round(counter.aim * 100)} % chance to causes ${hull} damage  `);
			}
			
		
		
		
		
		
		
		
		index = undefined;
			insert_return(`Cancel. `  );
			
*/		
	
}		
