/**
	the virtual game story set between FEYD MORA and GOTHRED
	act: 2
	follows: feyd
	precedes: goth
*/

import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";

import {txt, npc, kat} from "Loop/text";
import {gal, ste, lys} from "Loop/text";

import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s} from "Loop/text";


import {createWear, crew, quickSaveOutfit, quickLoadOutfit} from "Avatar/index";
import {wdesc, updateDraw, updateMods, sluttiness, clothes, showLess, showAll, wears, remove} from "Avatar/index";
import {clone, shuffle, clamp} from "Libraries/dh";
import {ra} from "Libraries/random";
import {class_based, roll, boost, dance, fellowship, dismiss, BARS} from "Virtual/index";

import {enter} from "Dungeon/engine";
import {autosave, snapshot} from "System/save";
import {quest} from "Virtual/quest"; 
import {new_act} from "Virtual/index"; 
import {level_up} from "Virtual/level_up"; 
import {combat} from "Gui/combat/combat/combat";

import {wage} from "Virtual/text";
import {placetime} from "Loop/text_extra";

function trislut(chaste, normal, slut, hyperslut = slut){ //TODO
	if(mile.slut < 1){
		return chaste;
	}else if(mile.slut > 3){
		return slut;
	}else if(mile.slut > 5){
		return slut;
	}else{
		return normal;
	}
}


export const voyage = (index) => {
	switch(index){
	default:
	case 101: 
		mile.game_progress = 20;
		new_act(2);
		snapshot(`voyage`, `Leaving Feyd Mora`);
		placetime(`Merchant cog`, ``);
		set.game();
		present(
			[1, `Pirate Captain`, `captain`], //TODO
			[2, `Captain`, `commoner`],
			[3, `Pirate`, `pirate`],
		);
		txt(`I was aboard <i>Adulterous Janet</i>. <i>Adulterous Janet</i> was an ordinary merchant cog, not very big nor very fast but it was sufficient for the coastal trade between cities in Beletriad and Gothred. Gothred was an important trade hub located on the island of the same name. The city was technically independent and ruled by a council of wealthy patricians. Among other things, it also meant it was a safe harbor for those persecuted or wanted in the kingdom, just like Arazor. `);
		txt(`The journey was quite pleasant - Dewyrain sea was usually very calm - and did not take very long because the game speeded up time when we were traveling. The weather was nice and everything went well until we were attacked by the pirates.  `);
		level_up(`Pirate attack. `, 102);	
		if(hardbug) chapter(`##Skip to Gothred. `, `goth`);			
		break;
			
	case 102:
		mile.goth_boarded = 1;
		txt(`I spotted the pirate ship first but I did not recognise they were pirates and was confused about the chaos which erupted on the deck. Seamen were running around, climbing up and down, doing what they could. `);
		txt(`However, very soon became apparent <i>Plague Storm</i> was inevitably gaining on us and there was no way to escape. Captain resigned on our fate and hoped they will take the cargo but spare our lives. `);
			
		link(`Prepare to be boarded. `, 110);
		roll(`Don't be such covards! We should fight!`, "char", 2, 104, 103);
		boost("char");
		break;
			
	case 103:
		mile.goth_boarded = 2;
		txt(`I still think my improvised rousing speech was pretty good. However, none of the seemen seemed to be eager to risk his life. I could only helplessly watch the pirate ship getting closer. `);
		link(`Prepare to be boarded. `, 110);
		break;
			
	case 104:
		mile.goth_boarded = 3;
		txt(`My improvised rousing speech had an effect. The pirate ship was not very big and there was a reasonable chance we might defeat them in hand-to-hand combat. The captain was handing out cutlasses and boarding axes and we prepared for the battle. `);
		link(`Fight! `, 105);
		break;
			
	case 105: 
		combat({
			pirate: 2,
		});
		//txt(`Combat placeholder. `);
		txt(`I managed to slaughter the group of pirates who attacked me. However, when I looked around, I realized I was the only one. The rest of the crew was defeated, disarmed or dismembered. I was surrounded by overwhelming numbers, any further resistance would be a suicide. `);
		link(`Captured. `, 110);
		break;
		
	case 110: 
		if(mile.goth_boarded === 3){				
			txt(`They took away my weapons and brought me to their captain. He laughed when he closely saw who caused his thugs the most troubles. `);
			npc(1, `Hehe! So beautiful and so feisty! Who are you? `);
			txt(`The way he was checking my body was not making me very calm. `);
		}else{
			txt(`The group of vicious pirates boarded our cog. Our captain begged their captain for mercy and the dangerously looking man condescendingly nodded. `);
			txt(`The pirates were rude when they made the sailors transport the valuable cargo from one ship to the other but overall they seemed rather reasonable. Until their captain noticed me. `);
				
			npc(1, `What do we have here? `);
			npc(2, `That wench travels from Feyd Mora to Gothred. `);
			npc(1, `I'm changing the conditions. We take the cargo and her. `);
			kat(`WHAT!?!`);
			npc(2, `Sure! `);
			txt(`The spineless captain immediately abandoned me. `);
			if(mile.goth_boarded === 2){
				npc(2, `That bitch tried to convince me to fight with you, would you believe that? `);
				npc(1, `Really? `);
				txt(`Before I could reach for my weapons, the pirates grabbed me. `);
				npc(1, `Thank you for telling me. I will thoroughly explain to her how bad idea that would be!`);
			}else{
				npc(1, `Why to Gothred? `);
				kat(`That's none of your business! Let me go!`);
				npc(1, `Hah! I won't! I'll keep you!`);
			}
			txt(`His evil smirk made me shiver. `);
		}
			
		if(PC.upper || PC.bra || PC.upperOuter){
			//showLess(0,-1);
			npc(1, `Let's see what we have we have here!`);
			txt(`I did not understand what he meant until he reached forward and grabbed my ${wdesc.top}. `);
			kat(`S... stop! `);
			txt(`He did not stop and viciously ripped my clothes to expose my chest. I was unable to stop him, his minions were firmly holding my hands. `);
			npc(1, `Lovely tits!`);
			remove("upper");
			remove("upperOuter");
			remove("bra");
		}else{
			npc(1, `And so shameless! Lovely tits! `);
			txt(`I regretted I remained topless. Without asking he reached to roughly squeeze my uncovered breasts. And I could not do anything to stop him, his minions were firmly holding my arms. `);
		}
			
		con_link(mile.sub < 4, `submissive`, `Spit in his face. `, 111, () => counter.temp = 3); //TODO SUB
		link(`Stare at him coldly. `, 111, () => counter.temp = 2); 
		con_link(mile.sub > -4, `dominant`, `Beg for your life. `, 111, () => counter.temp = 1); 
		break;
				
				
	case 111: 
		if(counter.temp === 3){
			mile.sub--; //TODO
			txt(`He wiped the spit and laughed even harder. `);
			npc(1, `I love defiant women! It's so satisfying to break them down! `);
		}else if(counter.temp === 2){
			txt(`I did not answer, just defiantly stared at him, trying to look brave. `);
			npc(1, `No response? Nevermind. I'll make you scream! In more ways than one! `);
		}else{
			mile.sub++;
			kat(`Please! Don't hurt me! `);
			txt(`He grabbed my chin. `);
			npc(1, `Why not? I love hurting little birds like you! `);
			kat(`I... I can be useful...`);
			npc(1, `Hmmm, when you so nicely beg, I'll give you a chance to convince me! `);
		}
			
		npc(3, `Can we gangbang her? `);
		txt(`I really hoped the answer was no. I did not want to be at mercy of dozen of horny and cruel pirates. But the captain did not mind sharing. `);
		npc(1, `Sure! Later! But now get back to work! We need to move the cargo and then disappear, We wouldn't have a chance against a patrol galley. For now, lock the wench up! `);
		txt(`I was grabbed and dragged to the pirate ship and then carelessly thrown into a cell. When they closed the door, I was in complete darkness. `);
		txt(`Something touched me!`);
			
		link(`Loudly squeal. `, 112, () => mile.goth_gal_punch = false); 
		link(`Blindly attack. `, 112, () => mile.goth_gal_punch = true); 
		break;
			
	case 112:
		placetime(`Pirate ship`, ``);
		if(mile.goth_gal_punch){
			txt(`I punched and hit something soft. `);
			gal(`Ahhhhhhh! `);
			txt(`Somebody screamed. `);
			kat(`Sorry?`);
			gal(`Och!... That's fine...		`);
		}else{
			kat(`Aaaahhhhhhh! `);
			txt(`I cried, terrified it might be a monster or a spider. `);
			gal(`Sorry! `);
		}			
		txt(`Answered the voice, I guessed I was talking with a young man. `);
		kat(`Who you are?!`);
		gal(`I'm @gal, currently a prisoner of those vile pirates!`);
		kat(`Nice to meet you, I'm @kat, coincidentally a prisoner too.`);
		gal(`Nice to meet you too!`);
		kat(`That's my boob. `);
		txt(`His hand reached to me to shake my hand but accidentally touched my breast. `);
		gal(`Oh. Sorry!`);
		kat(`That's okay!`);
		gal(`...`);
		kat(`...`);
		gal(`...`);
		kat(`Maybe now you can stop squeezing it? `);
		txt(`He stopped groping me and retracted his hand. `);
		gal(`Oh! Sorry! It's just horrible to be locked here, completely alone, in complete darkness!`);
		kat(`How long are you here?!`);
		gal(`I'm not sure. Probably almost a week. My ship was captured and they took me. I'm locked here until they get the ransom money. `);
		kat(`I'm locked here until they'll have enough time to gangbang me. `);
		gal(`That's awful! Are you hot? `);
		kat(`...yes. `);
		gal(`Sorry for being impolite! But being locked here, completely alone, is making me crazy and there is almost no way to entertain myself. `);
			
		link(`Inquiry about the entertainment. `, 113);
		link(`Plan how to escape. `, 120);
			
		break;
			
	case 113:
		gal(`Well... mostly daydreaming and touching myself. But you know, since we are now two, we can touch each other. You sound like a woman... or a very effeminate man which is fine too, circumstances are dire and I can't be picky!`);
			
		if(mile.slut < 0){
			kat(`Ewww, you can't be serious!`);
			txt(`In the darkness, he could not see how much I was blushing. I was shocked how he could talk about such intimate things so casually!`);
		}else if(mile.slut < 5){
			kat(`Eww!`);
			txt(`I shrugged and then realized he could not see that. `);
		}else{
			txt(`I mean, I could understand that. I probably would do the same. `);
		}
			
		gal(`So? What do you think? It's just a suggestion to kill the time. I understand if you're not in the mood, because of the imminent pirate r${String.fromCharCode(97)}pe and I don't want to be insensitive. `);
			
		link(`No!`, 114);
		link(`Absolutely no!`, 114);
		con_link(mile.slut > 4, `low slut`, `Sure, let's have sex!`, 125);
		link(`We should plan how to escape!`, 120);
		break;
			
	case 114:
		if(mile.slut < 2){
			txt(`I very resolutely refused!`);
			kat(`I'm not a slut! I don't sleep with every guy I meet! I don't even know you! `);
		}else{
			txt(`I refused.`);
			kat(`That's a dumb idea! First, we're in a grim dangerous situation. Second, I don't even know you!`);
		}
		gal(`We have a lot in common. Like, we are both prisoners. `);
		kat(`That's not enough! I don't even know how you look!`);
		gal(`I can ensure you I'm very handsome. But asking such questions is making you sound like a shallow person. Being attractive isn't everything!`);
		kat(`I didn't meant it that way!`);
		gal(`I don't mind if you're ugly, since we can't see each other anyway I can imagine you as attractive as I wish!`);
		kat(`You don't have to imagine anything! I'm extremely attractive!`);
		gal(`Good to know that! And I didn't say we have to have sex. We can save it for tomorrow. Maybe you can just give me a blowjob or something like that? `);
		kat(`No! We should focus on our escape!`);
		gal(`You can't imagine how hard it is to spend days locked here, completely alone, and then be joined by an attractive, very nicely smelling girl with lovely soft titties. I'm very sorry but I'm just a man and I'm unable to focus on anything else! And by hard I meant literally hard. Do you want to feel it? `);
		if(mile.slut < 2){
			kat(`No!`);
			txt(`I panicly gasped. He was searching for my hand but I did not let him to grab me.`);
		}else if(mile.slut < 5){
			kat(`I really don't...`);
			txt(`He found my arm and pulled my hand closer to him and then down. I could feel something hard and warm. `);
		}else{
			txt(`He found my arm, then grabbed my hand and pulled it closer until I could feel something hard and warm. I could stop him but honestly, I was a bit curious. My fingers examined what he was packing. `);
		}
		gal(`Of course, I'm not going to force you into anything (unlike those pirates!). M... maybe you can give me at least a handjob? `);
			
			
		link(`Slap him!`, 115);
		link(`I said no!`, 116);
		link(`Okay, I can give you a handjob!`, 117);
		con_link(mile.slut > 0, `low slut`, `Now we have to focus! When we escape, you can have me any way you want! `, 120, () => mile.goth_gal_promise = true);
		con_link(mile.slut > 4, `low slut`, `Fine, we can have sex if that makes you shut up. `, 120);
		break;
			
	case 115:
		//mile.slut--; //TODO - which one?
		mile.sub++;
		txt(`I tried to slap him. It was not easy because I could only blindly targed the place where I assumed was his head. `);
		gal(`Ahhhh!`);
		kat(`Did I slap you?!`);
		gal(`No! You hit my ear! And it hurt!`);
		kat(`Well, you deserved that anyway! Now shut up and help me to plan the escape. `);
		link(`Plan the escape. `, 120);
		break;
			
			
	case 116:
		kat(`I said no!`);
		gal(`No problem! What else should we do? `);
		kat(`What about planning our escape? `);
		link(`Plan the escape. `, 120);
		break;
			
	case 117:
		txt(`I sighed. He was annyoing and unfocused and I decided that giving him a quick handjob will not kill me.`);
		txt(`He hold my hand in his and let me to wrap my fingers along his already hard shaft. I began lightly pumping. `);
		gal(`Oh yeah! You're the best! Can you go faster? `);
		if(mile.sub > 2){
			kat(`O...okay..`);
			txt(`I fulfilled all his demands. `);
		}else{
			kat(`Don't tell me what to do! I know how to give hanjobs! `);
		}
		txt(`In the darkness I could only hear his heavy pleased grunting. My long fingers focused on the tip and his moaning was getting even more intense. `);
		gal(`Oh yeah! I'm close!`);
		txt(`With last firm strokes I finished, I could feel the twitching when he errupted. `);
		gal(`Fuck!`);
		kat(`What? `);
		txt(`I cummed on myself! You should've aimed in different direction!`);
		kat(`Don't complain. `);
		gal(`Sorry. Thanks! This really cleared my mind. `);
		
		link(`Maybe we shouly try to plan our escape?  `, 120);
		break;
			
	case 120:
		if(mile.goth_gal_promise){
			txt(`He instantly sounded more resolute and focused. `);
			gal(`Aye, m'lady! You're absolutely right! Now we have to plan our escape!`);
		}else{
			gal(`That's a very good idea. `);
		}
		kat(`Do you have anything? You're their prisoner far longer than me!`);
		gal(`Maybe you can seduce the jailor? `);
		kat(`How? `);
		{
			const tits = PC.getDim("beastSize");
			if(tits < 12){
				gal(`Well, based on what I felt, definitely not with your tits. They seemed to be rather on the small side. `);
				kat(`Shut up! ${mile.goth_gal_punch ? "Before I punch you again!" : ""}`);
			}else if(tits > 34){
				gal(`With your tits? They felt really huge! I'm sure he would like them!`);
			}else{
				gal(`Maybe with your tits? Based on what I felt, they are pretty good!`);
			}
		}
		txt(`I refused his plan because it was stupid. We tried to come up with something better but even after a very long deliberation we still have nothing. Eventully I agreed give it a try. `);
		kat(`How do we summon him to tell him I want to ${trislut("...you know!", "have sex with him?", "fuck him?")} We should loudly cry.`);
		gal(`That won't work. After first few days they just ignore all my crying. `);
		kat(`So we have to wait. `);
		gal(`Or maybe... we could have sex and loudly moan. That will certainly attract his attention and make him even more suggestible to your seduction.`);
			
		con_link(mile.slut > 3, `low slut`, `Awesome, let's have sex!`, 125);
		link(`Fine, let's do it. `, 125);
		link(`Can't we just prented the moaning? `, 121);
		break;
			
			
	case 121:
		gal(`Oh yeah, that is an option too, I was about to suggest it. `);
		kat(`Okay. What should we do? `);
		gal(`Moand lustfully. `);
		kat(`Okay.`);
		gal(`...`);
		kat(`...`);
		gal(`...`);
		kat(`I thought you'll start, you suggested it! `);
		gal(`Right. I can try. `);
		kat(`Do it!`);
		gal(`Ahhhh.`);
		kat(`Hahaha! What was that!?! `);
		gal(`Hey! Try it yourself!`);
		kat(`AHHHHHH!`);
		gal(`...okay that was good. AHHHH!`);
		kat(`OHHHH!`);
		gal(`UHHHH!`);
		kat(`AHHHH! YEAH! FUCK ME HARDER!`);
		gal(`Damn! Just your moaning is making me so hard!`);
		if(ext.slut === 0) txt(`I felt extremely embarrassed and was happy he could not see my face. `);
		kat(`OHHHH!`);
		gal(`Can you scream something about my dick being really huge? `);
		kat(`Why? `);
		gal(`To make pirates jelaous. `);
		kat(`AHHH! YOUR DICK IS SO MONSTROUSLY HUGE!`);
		gal(`OH YEAH! `);
		npc(3, `HEY! WHAT ARE YOU DOING THERE! `);
		link(`Seduce the pirate. `, 130);
		break;
		
	case 125:
		mile.slut++;
		mile.goth_gal_sex = true;
		kat(`How we're supposed to have sex? I can even see you!`);
		txt(`I heard him doing something with his pants. `);
		gal(`Well, you don't have to see to have sex! Come here!`);
		txt(`He took my arm and pulled me closer, his other hand touched my thigh to suggest me to lift my knee and sit on his lap astride. `);
		gal(`Is this fine? `);
		kat(`I guess. `);
		gal(`Damn, your ass in fine too! `);
		txt(`His hands were examining my body and I touched his. `);
		kat(`Whoa!`);
		txt(`I gasped when I caressed his chest. It was firmer and bulkier than I expected with perfectly modeled muscles. Now I regretted I could not see him, it seemed I was lucky and he was one of those sexy fantasy hunks. His face appeared to be normal, I was not able to visualiase how he looked but my fingers did not find any obvious deformity or scar. His hair were short and soft.`);
		txt(`I was grinding against him, after a while he reached down carefully pushed his cock in my pussy. `);
		gal(`Ahhh! `);
		txt(`I gyraded with my hips and then began bouncing. `);
		gal(`Careful! The ceiling is very low!`);
		txt(`I lifted my arms and found a beam above my head. I had to be careful and not bounce too wildly, but on the other I was able to use it as a support. `);
		gal(`Oh yeah! `);
		kat(`Ahhh!`);
		txt(`I was ridding his cock, we both enjoyed it very much, louly moaned, completely forgeting we were prisoners. `);
		npc(3, `HEY! WHAT ARE YOU DOING THERE! `);
		link(`Seduce the pirate. `, 130);
		break;
			
			
	case 130:
		if(mile.goth_gal_sex){
			txt(`Screamed a pirate from outside. He roughly opened the door and I for the first time saw the face of the man I was fucking. And I was not disappointed. @gal was young and ruggedly handsome. `);
		}else{
			txt(`Screamed a pirate from outside.`);
			kat(`We are having sex!`);
			gal(`Wild hot sex!`);
			txt(`He roughly opened the door and I for the first time saw the face of my fellow prisoner. And I was not disappointed. @gal was young and ruggedly handsome. `);
		}
				
		txt(`However, I got only a quick glance because the pirate grabbed me by my hair and dragged me outside. Unfortunately, he did not forget to secure the door again. He was pretty paranoid, he pushed my face against the dirty deck, painfully twisted my arms behind my back and tied my wrists together. `);
		npc(3, `You little whore! You can't survive an hour without spreading your legs and having your horny cunt filled with a dick?`);
		kat(`Well...`);
		txt(`The harsh slap I immediately received revealed the question was only rhetorical. `);
		txt(`His callous fingers were ruthlessly squeezing my soft flesh and I was utterly helpless. The situation was looking grim. `);
		npc(1, `Where the hell are you slacking... what the fuck?!?`);
		txt(`The pirate captain entered the cabin. The pirate instantly let me go, dropping me down. `);
		npc(1, `I told you we're going to gang-r${String.fromCharCode(97)}pe her during the dog watch! No line-cutting! Now you'll go last! `);
		npc(3, `But I hate sloppy seconds! She'll be all filled and stretched before I'll get a turn! `);
		npc(1, `Too bad! Get back to work! `);
		link(`The captain. `, 131);
		break;
			
	case 131:
		txt(`He sharply ordered and the pirate disappeared. But I was not saved - the horny pirated made him angry but the true targed of his wrath was me. `);
		npc(1, `You shameless floozie! Are you trying to seduce my men?!?`);
		kat(`N-`);
		txt(`Was everything I could say, his rough fingers were clenching my throat. `);
		npc(1, `I will have to teach you proper manners! `);
		txt(`He opened the door of the cell to carelessly throw me in. He did not expect that @gal was ready and instantly jumped at him. They were rolling on the floor, struggling in deadly hand-to-hand combat. The pirate tried to unsheath his cutlass but in a corner, I noticed a crowbar and kicked it in @gal's direction. He nastily finished the captain. `);
		kat(`Good job!`);
		txt(`He smiled and armed himself with the captain's cutlass. `);
		gal(`Thanks! You look better than I imagined! Especially tied up!`);
		kat(`That's not funny! Untie me!`);
		gal(`Turn around. Do you trust me? `);
		kat(`No? Ahhh! `);
		txt(`Suspecting nothing I followed his order and I shrieked when I felt him swinging with the cutlass, cutting through the rope, `);
		kat(`You could cut off my hands!`);
			
		if(mile.goth_gal_sex && mile.slut > 5){
			gal(`Well, I didn't! What now?`);
			link(`Maybe we should finish the sex? `, 132);
		}else{
			gal(`Well, I didn't! You're welcome! `);
		}
			
		fellowship(`gal`);
		quest.initiate(`pirate_ship`, `Explore the pirate ship and defeat all remaining pirates.`, `eliminate`);
		enter(`Explore the ship. `, `ship`, 140, 139, `pirate_ship`); 
		if(softbug) link(`#Explore the ship & kill all the pirates. `, 140);
		break;
			
			
	case 132:
		txt(`He looked at me puzzled. `);
		gal(`Are you out of your mind!?! I would love to but we're in the middle of escape! `);
		kat(`Right. Sorry. `);
		gal(`Don't worry, you don't have to apologize for being a nympho! Now let's go! `);
		enter(`Explore the ship. `, `ship`, 140, 139); 
		if(softbug) link(`#Explore the ship & kill all the pirates. `, 140);
		break;
		
	case 139:
		enter(`Explore the ship. `, `ship`, 140, 139, `pirate_ship`); 
		if(softbug) link(`#Explore the ship & kill all the pirates. `, 140);
		break;
		
	case 140:
		quest.closure(`pirate_ship`);
		txt(`The pirate ship was ours!`);
		kat(`What now? Do you know anything about ships? `);
		gal(`Yes, of course!`);
		kat(`How do we make her go where we want to? `);
		gal(`Oh, you meant whether I know anything about sailing? Then no. `);
		kat(`Damn. `);
		gal(`But we can't be far away from the shore and the area is scouted by patrol ships. `);
		txt(`We had to wait. `);
			
		if(mile.goth_gal_promise){
			gal(`So... when you promised me you can do anything with you once we escape, were you just trying to motivate me or were you serious? `);
			link(`We are not yet safe. `, 150);
			link(`I guess we don't have anything better to do anyway.. `, 151);
		}
			
		//link(`Wait. `, 150);
		level_up(`Wait. `, 150);
		//next(`Exit the game. `);
		break;
			
		
	case 151: 
		//TODO ASAP
		//break;
			
	case 150: 
		dismiss();
		placetime(`Light galley`, ``);
		set.reentry("voyage", 200);
		present(
			[1, `Soldier`, `gothGuard`], //TODO
			[2, `Officer`, `gothOfficer`],
		);
		gal(`I SEE A SHIP! `);
		txt(`@gal screamed from the foremast. He climbed there because he was impatient and waiting on the deck was not good enough for him. `);
		txt(`Soon I could see it too. It was <i>Troubadour</i>, a nimble light galley with one mast and triangular lateen sail, flying red and black colors of the Free City of Gothred. We were boarded and several armed soldiers jumped on our deck. I raised my arms to make it obvious I did not want any troubles and they impertinently grabbed me and pushed me against the deck. `);
		gal(`Wait! `);
		txt(`@gal jumped down. `);
		npc(2, `Sir! You're alive! `);
		gal(`Yes, I am. My ship was captured but I managed to survive, escape and defeat all the pirates! `);
		npc(2, `Most impressive, sir! `);
		txt(`The officer led him to their ship while I was accompanied by the soldiers. It was a relief we were saved and I hoped I will never see <i>Plague Storm</i> again. `);
		kat(`W... where are you taking me!?!`);
		txt(`I became concerned when they pushed me into a tiny, dirty room, not different than the cell on the pirate ship. `);
		npc(1, `To the brig, pirate scum!`);
		kat(`Wait! This is a horrible mistake!`);
		txt(`They did not listen and locked the door. I was sure the matter will be soon resolved. But time was running and nothing was happening. `);
			
		next(`Exit the game. `);
		if(hardbug) link(`##Skip to the next game part. `, 200);
		break;
			
			
	case 200:
		set.game();
		mile.game_progress = 21;
			
		placetime(`brig on a galley`, ``);
		txt(`I was back in my cell, awaiting my fate. When the soldiers returned, it was only to mock me and have sleazy suggestions, like if I suck their dick they'll tell the hangman to go easy on me during the torture before my execution. `);
		txt(`The situation was looking grim. From the sounds outside, I guessed we arrived in the port, presumably to Gothred. After some time passed, the soldiers returned again. Or so I thought before @gal opened the door. `);
		gal(`Eh... hello!`);
		kat(`What the hell!?! Let me out!`);
		gal(`Of course! Sorry! There was a small misunderstanding. But I solved it as soon as I found out!`);
		kat(`It took you pretty long! `);
		gal(`Well... I kinda forgot about you. But honestly, I would never think they might be so stupid and lock you up! Now you're free to go! Welcome to Gothred. `);
		kat(`Thank you so much!`);
		gal(`Don't be mad at me!`);
		kat(`You forgot about me! And they could hang me for piracy!`);
		gal(`They wouldn't hang you... probably. What are you going to do now? `);
		kat(`I'm on a secret mission. I have to find a wizard named Arazor who will tell me how to defeat the evil overlord. `);
		gal(`That seems like an important thing! I will just relax for a while, trying to get over the trauma of being captured. `);
			
		chapter(`Gothred. `, `goth`);		
		break;
		
	}
}
