/**
	virtual game story set on YOUR own SHIP ex-Plague Storm and the exploration of DEWYRAIN SEA around GOTHRED
	act: 2
	parent: goth
*/
import {link, con_link, alt_link, links_mix, next, main, present, set, insert, insert_return, chapter, choice, effects} from "Loop/engine";

import {txt, npc, kat, fol} from "Loop/text";
import {gal, ste, lys, sir} from "Loop/text";
import {bold, hint} from "Loop/text";
import {aly, emy, reac, and, s} from "Loop/text";

import {createWear, crew, quickSaveOutfit, quickLoadOutfit, toUnderwear, toNudity} from "Avatar/index";
import {INVENTORY, updateDraw, updateMods, sluttiness, clothes, showLess, showAll, wears, remove, effigy, draw} from "Avatar/index";
import {clone, shuffle, clamp, pm} from "Libraries/dh";
import {ra, rg} from "Libraries/random";
import {class_based, roll, dance, fellowship, dismiss, BARS} from "Virtual/index";

import {enter} from "Dungeon/engine";
import {autosave} from "System/save";
import {quest} from "Virtual/quest"; 
import {We, we, he, fol_name, wage, dyke} from "Virtual/text";
import {rewards} from "Data/items/prices"; 
import {level_up} from "Virtual/level_up"; 
import {placetime} from "Loop/text_extra";

let map;

function create_map(){
	const width = 7;
	const height = 7;
	rg.i(999); //TODO - seed
	
	const places = {
		goth: [[3, 3]],
		karg: [[0, 3]],
		oskyl: [[3, 0]],
		smug: [[2, 2], [3, 2], [2, 4], [2, 4]],
		hdar: [[1, 6], [2, 6], [4, 6]],
		dorf: [[6, 2], [6, 3], [6, 4]],
		ydyg: [[1, 4], [2, 4], [2, 5], [4, 4], [4, 5], [5, 5]],
		wreck: [[0, 5], [1, 5], [0, 6], [1, 6], [4, 4], [4, 5], [5, 4], [5, 5]],
	};
	
	map = [];
	for(let x = 0; x < width; x++){
		map[x] = [];
		for(let y = 0; y < height; y++){
			map[x][y] = "";
		}
	}
	
	Object.entries(places).forEach( a => {
		const [name, locations] = a;
		const where = rg.array(  locations.filter( b => !map[b[0]][b[1]] )  );  //removes already occupied spaces
		map[where[0]][where[1]] = name;
	})
	
	
	{ 	
		//display map in console
		let display = clone(map);
		display.reverse();
		display = display[0].map((val, index) => display.map(row => row[index]).reverse());
		console.log("> Map of the Dewyrain sea: "); //SIC
		console.table(display); //SIC
	}	
}


//returns what KEY is on map on XY position
function locate(x = mile.ship_x, y = mile.ship_y){
	if(map === undefined) create_map();
	return map[x]?.[y];
}


//moves to the XY position of KEY on map
function sail_to(key){
	for(let x = 0; x < map.length; x++){
		for(let y = 0; y < map[0].length; y++){
			if(map[x][y] === key){
				mile.ship_x = x;
				mile.ship_y = y;
				return;
			}
		}
	}
	
	//if(debug) alert(`Island with key "${key}" not found on the map. `);
}


//returns exploration options (sail NESW)
function exploration_options(){
	const directions = [
		{name: "north", x: 0, y: -1},
		{name: "east", x: 1, y: 0},
		{name: "south", x: 0, y: 1},
		{name: "west", x: -1, y: 0},
	]
	
	directions.forEach( a => {
		const {name, x, y} = a;
		
		if(	locate(mile.ship_x + x, mile.ship_y + y) !== undefined ){
			link(`Sail ${name}. `, 200, () => {
				mile.ship_anchor = undefined;
				mile.ship_x += x;
				mile.ship_y += y;
			});
		}
	})
}


//returns sailing options (anchor/navigate + NESW)
function sailing_options(){
	const location = locate();
	
	if(location){ //set current location as explored/visited
		if(!mile[`ship_${location}`] || mile[`ship_${location}`] === 1) mile[`ship_${location}`] = 2;
	}
	
	if(!mile.ship_anchor){
		if(location){
			link(`Anchor. `, 101, () => mile.ship_anchor = location );
		}
		link(`Navigate. `, 102);
		exploration_options();		
	}else{
		link(`Sail. `, 102);
	}
}


//description of everyday situation on the ship 
function flavour_ship_desc(){
	
	let desc = `I was on board of <i>${mile.ship_name}</i>. `;
	
	if(mile.ship_anchor){
		desc += `The caravel was ready to set sail. `;
	}else{
		desc += `The caravel was sailing the Dewyrain sea. `;
	}
	
	const fin = {
		gal: mile.ship_gal,
		lys: mile.ship_lys,
		ste: mile.ship_ste,
		sir: mile.ship_sir,
	}

	//XO
	switch( ra.integer(3) ){
	case 1:
		desc += `@${[mile.ship_xo]}, my second-in-command, was standing on the aftercastle, watching the crew. `;
		fin[mile.ship_xo] = false;
		break;
			
	case 2:
		if(mile.ship_xo === "lys" || mile.ship_xo === "gal"){
			desc += `@${[mile.ship_xo]}, my second-in-command, was screaming at a group of clumsy sailors. `;
		}else{
			desc += `@${[mile.ship_xo]}, my second-in-command, was discussing something with a group of sailors. `;
		}
		fin[mile.ship_xo] = false;
		break;
			
	default: 
		//nothing, the rest of crew
		break;
	}

	//REST OF CREW
	for(let fail = 0; fail < 100; fail++){
				
		switch( ra.integer(13) ){
		default: 
			break;
		case 1://TODO
			if(fin.lys){
				if(fin.ste && !mile.ship_power_couple){
					desc += `@lys was joking and laughing with sailors. @ste was enviously staring at her from distance. `;
					fin.ste = false;
				}else{
					desc += `@lys was joking and laughing with sailors. `;					
				}
				fin.lys = false;
			}
			break;
					
		case 2:
			if(fin.gal){
				if(ra.g < 0.5){
					desc += `@gal was callibrating the ship ballista. `;
				}else{
					desc += `@gal was on top of the main mast, observing the sea with his telescope. `;
				}
				fin.gal = false;
			}
			break;
					
		case 3:
			if(fin.ste){
				if(ra.g < 0.5){
					desc += `@ste was nervously striding across the deck, checking every piece of equipment and every rope. `;
				}else{
					desc += `@ste was relaxing, reading a book. `;
				}
				fin.ste = false;
			}
			break;
					 
		case 4:
			if(fin.sir){
				if(ra.g < 0.5){
					desc += `@sir was standing on the deck with distant look in her eyes. `;
				}else{
					desc += `@sir was carefully studying the crew, taking notes on her tablet. `;
				}
				fin.sir = false;
			}
			break;
						
		case 5:
		case 6:	
						
			if(fin.gal && fin.lys){
				if(ra.g < 0.5){
					desc += `@gal and @lys were chatting and laughing. `;
				}else{
					desc += `@lys was explaining to @gal how to cause carnage with the ship ballista. `;
				}
				fin.gal = false;
				fin.lys = false;
			}
			break;
					 
		case 7:
		case 8:	
		case 9:	
			if(fin.ste && fin.lys){
				if(mile.ship_power_couple){
					desc += `@ste and @lys were shamelessly making out in front of everybody. `;
				}else{
					desc += `@ste and @lys were very loudly arguing ${ra.array(["how to best ballance the load. ", "about anchors and cables. ", "whether we have enough water or not. "])} `;
				}
				fin.ste = false;
				fin.lys = false;
			}
			break;
					
					
		case 10:	
		case 11:
			if(mile.ship_sirgal > 0 && fin.sir && fin.gal){
				desc += `@sir and @gal were talking together. `;
				fin.sir = false;
				fin.gal = false;
			}
			break;
						
						
		case 12:	
		case 13:
			if(fin.ste && fin.lys && mile.ship_power_couple){
				if(fin.gal && rg.g < 0.5){
					desc += `@lys and @ste were laughing and making fun of @gal for his lack of knowledge about ships and sailing. `;
					fin.gal = false;
				}else{
					desc += `@ste and @lys were drilling the crew and discussing how to improve ship's combat abilities. `;
				}
							
				fin.ste = false;
				fin.lys = false;
			}
			break;
		}

		fail++;
		if( !Object.values(fin).some( a => a) ) break;
	}
			
	txt(desc);		
}




export const ship = (index) => {
	switch(index){
	default:
	case 101: 
		//initialise everything
		if(mile.ship_x === undefined || mile.ship_y === undefined){ 
			create_map();
			sail_to("goth"); //initiation (to keepy ship_x & y of Gothred dynamic)
				
			//LEGACY
			if(mile.ship) mile.ship_name = mile.ship;
			if(mile.crew_ste) mile.ship_ste = mile.crew_ste;
			if(mile.crew_lys) mile.ship_lys = mile.crew_lys;
			if(mile.crew_gal) mile.ship_gal = mile.crew_gal;
			if(mile.crew_sir) mile.ship_sir = mile.crew_sir;
			if(mile.goth_lys_boatsex) mile.ship_boatsex = 1;
			if(mile.xo) mile.ship_xo = mile.xo;
		}
		
		placetime(mile.ship_name, ``);
		set.reentry("ship");
		set.game();
			
		flavour_ship_desc();
			
		//DISCOVERY
		switch( locate() ){
		default: 
			//EMPTY
			break;
					
		case "goth":
			if(mile.ship_anchor){
				txt(`We were mooring at Gothred. `);
				chapter(`Gothred. `, `goth`);
			}else{
				txt(`The tall lighouse and towers of <strong>Gothred</strong> were visible from a great distance. `);
			}
			break;
				
		case "oskyl":
			if(mile.goth_arazor === 6){
				txt(`At least Karged was a proper island. <strong>Oskyl</strong> was just a tiny cliffy islet, just an unremarkable tall rock protruding from the sea. `);
			}else{
				txt(`We discovered a cliffy islet, just a tall rock protruding from the sea. According to our map, it was called <strong>Oskyl</strong>.`);
			}
			if(mile.ship_anchor){
				link(`Oskyl. `, 600);
			}
					
			break;
					
		case "karg":
			txt(`We arrived to <strong>Karged</strong>. The island was not inhabited, only ruins remained from the sacred necropolis of once mighty Aquilonian empire. `)
			if(mile.ship_anchor){
				link(`Karged. `, 500);
			}
			break;
				
		case "ydyg":
			if(mile.ship_ydyg < 3){
				if(mile.ship_anchor){
					txt(`We were anchoring at a <strong>small island</strong> inhabited by several fishers. `);
					link(`Land. `, 510);
				}else{
					txt(`We arrived to <strong>small island</strong>. `);
				}
						
			}else{
				if(mile.ship_anchor){
					txt(`We were anchoring at a small island called by local inhabitants <strong>Ynis Ydyg</strong>. `);
					link(`Ynis Ydyg. `, 510);
				}else{
					txt(`We arrived to a small island called by local inhabitants <strong>Ynis Ydyg</strong>. `);
				}
			}
			break;
				
		case "smug": 
			if(mile.ship_anchor){
				if(mile.goth_smugglers === 3){ 
					txt(`In the lagoon under the clifs was already anchoring a light galley with red and black colors of the Free City of Gothred. They greeted us with their flags and we stopped next to them.  `);
				}else{
					txt(`We were anchoring at <strong>Hengar</strong> in a dark blue lagoon. Above us were high clifs and on top of them ruins of an ancient Aquilonian fortress. `);
				}						
				link(`Land. `, 520);
			}else{
				txt(`We were sailing close to <strong>Hengar</strong>, a small, rocky island. It was empty, except for ruins of an ancient Aquilonian fortress on an inaccessible high clif. `);						
			}
			break;
					
		case "hdar": 	
			txt(`<strong>H'dar Ğizra</strong> was a large, lush island. The opinions differed whether it was the southernmost island belonging to Beletriad or the northernmost island belonging to Southern Archipelago. `);
					
			//TODO
			if(mile.ship_anchor){
				if( quest.is_active("goth_dorf") ){
					enter(`Search for a rare lumber. `, `hdar_jungle`, 101, 101, `goth_dorf`); 
				}
				//link(`Land. `, 560);
						
				//txt(`We were sailing close to a small, rocky island. It was empty, except for ruins of an ancient Aquilonian fortress on an inaccessible high clif. `);						
			}
			break;
					
		case "dorf":
			if(mile.ship_anchor){
				txt(`We were mooring in fortified port in geshud Duradudos, the largest population center on the <strong>Datan Gulnas</strong>. The town did not look very impressive but we were informed the largest part of the city had been build by dwarves and was located deep underground. `);						
				link(`Geshud Duradudos. `, 560);
			}else{
				txt(`The first thing you could see of <strong>Datan Gulnas</strong> was an elegant, slim and incredibly tall lighthouse. It should show every traveler the power and the wealth of the island and especially the skill and prowess of local builders and craftsmen. `);						
			}
			break;
					
					
		case "wreck":
			if(!mile.ship_wreck){
				txt(`<i>${mile.ship_name}</i> without warning crashed into something. I nearly lost my balance and fell on the deck. `);
				kat(`What's going on?!`);
						
				if(mile.ship_xo === "ste"){
					ste(`Nothing serious. `);
					kat(`Nothing serious!?!`);
					ste(`We ran into a hidden underwater reef. But I noticed them in time and we just got lightly scratched. `);
					if(mile.crew_lys){
						lys(`If you noticed them in time, you'd be able to evade them! Great job, @ste! You almost got us all killed! `);
						ste(`Shut up, @lys! If you were in charge, we would already be at the bottom of the sea!`);
					}
				}else if(mile.ship_xo === "lys"){
					lys(`Nothing serious, we just ran into hidden underwater reefs. `);
					kat(`We <i>just</i> ran into hidden underwater reefs!?!`);
					lys(`I mostly evaded them.`);
					if(mile.crew_ste){
						ste(`This happens when you don't put in charge a proper sailor!`);
						lys(`Shut up, @ste! Nobody asked about your stupid opinions!`);
						ste(`Next time try to not get us all killed, @lys!`);
					}
				}else{
					if(mile.ship_xo === "sir"){
						sir(`There was a collision. `);
					}else if(mile.ship_xo === "gal"){
						gal(`I think I ran into something.`);
					}
								
					if(mile.ship_lys){
						lys(`No shit! You ran into a reef! `);
					}else if(mile.ship_ste){
						ste(`You ran into a reef! `);
					}else{
						kat(`Collison!?!`);
					}
							
					if(mile.ship_xo === "sir"){
						sir(`There are no reefs on the map. `);
					}else if(mile.ship_xo === "gal"){
						gal(`But I didn't see anything! And there were none on the map!`);
					}
							
							
					if(mile.ship_ste){
						ste(`Maybe you should more look around yourself and less stare into incomplete maps!`);
					}
							
					if(mile.ship_ste && mile.ship_lys){
						lys(`Yeah, are you just blind or are you intentionally trying to get us killed!?!`);
						ste(`That happens when you put in charge somebody who isn't a proper sailor!`);
						lys(`Yeah, even a nitwit like @ste would be a better first officer!`);
					}						
				}
						
				txt(`We checked the damage but it was not serious. `);
				if(mile.ship_gal){
					gal(`Hey! What is that!?!`);
				}else if(mile.ship_sir){
					sir(`Captain!? You should look at this!`);
				}else{
					txt(`And the sailors noticed something else. `);
				}
						
				txt(`The underwater reefs were treacherous and one of our predecessors was less lucky. Through the crystal-clear water was visible an old shipwreck, wedged only several meters below the sea.  `);
						
						
			}else{
				if(mile.ship_anchor){
					quickSaveOutfit();
					quickSaveOutfit(effigy[ext.follower?.key]);
					txt(`@ste carefully anchored our ship not far away from the <strong>shipwreck</strong>. `);
					link(`Shipwreck. `, 530);
				}else{
					txt(`We were carefully navigating through reefs. We did not wanted to get <strong>wrecked</strong> like the other <strong>ship</strong>. `);
				}
			}
			break;
		}
		link(`Followers. `, 104);
		sailing_options();
		next(`Exit the Game.`);
		break;


		//NAVIGATE
	case 102: 
		{
			const list = {
				goth: {name: "Gothred"},
				karg: {
					name: "Karged",
					hint: "We knew that Karged was located somewhere westward from Gothred. ",
				},
				oskyl: {
					name: "Oskyl",
					hint: "Oskyl was supposed to be located north of Gothred. ",
				},
				ydyg: {name: "Ynis Ydyg"},
				smug: {name: "Rocky island"},
				dorf: {name: "Datan Gulnas"},
				hdar: {
					name: "H'dar Ğizra",
					hint: "We were told that H'dar Ğizra was located in the far south. ",
				},
				wreck: {name: "Shipwreck"},
			};
				
			txt(`I asked for @${[mile.ship_xo]} and together we examined the map. `);
				
			Object.entries(list).forEach( a => {
				const [key, value] = a;
					
				//hint
				if(mile[`ship_${key}`] === 1){
					if(value.hint) txt(value.hint);
				}
					
				//sail to
				const current = locate();
				if(current !== key && mile[`ship_${key}`] > 1){
					link(`Sail to ${value.name}. `, 200, () => sail_to(key) )
				}
					
			})
				
			exploration_options();
				
			if(softbug && Object.entries(list).some( a => {
				const [key, value] = a;
				return !mile[`ship_${key}`] || mile[`ship_${key}`] < 2;
			})){
				link(`#Explore everything. `, 102, () => Object.entries(list).forEach( a => {
					const [key, value] = a;
					if(!mile[`ship_${key}`] || mile[`ship_${key}`] < 2) mile[`ship_${key}`] = 2;
				}));
			}
				
		}
		link(`Back. `, 101)
		break;


		//FOLLOWERS
	case 104:
		dismiss();
		if(mile.ship_ste) link(`@ste, come with me. `, 101, () => fellowship("ste") );
		if(mile.ship_lys) link(`@lys, come with me. `, 101, () => fellowship("lys") );
		if(mile.ship_gal) link(`@gal, come with me. `, 101, () => fellowship("gal") );
		if(mile.ship_sir) link(`@sir, come with me. `, 101, () => fellowship("sir") );
		link(`I will go alone! `, 101);
		break;	


		//SHIP HAPPENINGS 200
	case 200: 
	{
			const available = [0, 0];
			const slut = sluttiness(PC).details;
			
			//Stede dislikes Lyssa
			if(mile.ship_ste &&  mile.ship_lys && !mile.ship_drama) available.push(1, 1, 1, 1);
			if(mile.ship_drama === 1) available.push(2, 2, 2, 2);
			if(mile.ship_drama > 1 && !mile.goth_lys_topless && (!slut.breasts || !slut.bottomless) ) available.push(10);
			
			//Love triangle of Stede & Lyssa & Kate
			if(mile.ship_lys && !mile.ship_boatsex) available.push(11);
			if(mile.ship_ste && mile.ship_boatsex === 1) available.push(12);	
			if(mile.ship_boatsex === 2) available.push(13);	
			if(mile.ship_boatsex === 3) available.push(14);	
			if(mile.ship_boatsex === 4) available.push(15);	
			if(mile.ship_boatsex === 5) available.push(16);	
			
			//Sirael's events 
			if(mile.goth_sir > 4 && !mile.ship_sirsex) available.push(20);	
			if(mile.goth_sir > 1 && mile.ship_gal && !mile.ship_sirgal) available.push(21);
			if(mile.ship_sirgal === 1) available.push(22);
			if(!mile.ship_scepter && mile.goth_arazor === 7) available.push(30);
			
			if(ra.g < 0.66) return main(101);
			//const happening = [ra.array(available), 0, 0]
			
		switch( ra.array(available) ){
			//generic
			default:
			case 0:
				txt( ra.array([
					`We were hit by a light, refreshing rain but otherwise the weather was fine. `
					, `The wind was favourable and  <i>${mile.ship_name}</i> was dashing forward under full sails. `,
				]));
				link(`Continue. `, 101);
				break;
				//Stede dislikes Lyssa
			case 1:
				mile.ship_drama = 1;
				txt(`@ste barged in my cabin. `);
				ste(`What the hell were you thinking!?!`);
				kat(`Would you mind being more specific? `);
				if(mile.ship_xo === "lys"){
					ste(`How could you not only take with you but even promote somebody like Meril!? Or @lys as she calls herself now!`);
				}else{
					ste(`How could you accept to your crew somebody like Meril!? Or @lys as she calls herself now!`);
				}
				kat(`Why?!`);
				ste(`She can't be trusted! She's a shameless, backstabbing, greedy whore! She'll steal your ship just like she stole mine!`);
						
				link(`Thanks for the warning. `, 101);
				link(`Maybe I made a mistake... Please, keep an eye on her and watch my back. `, 101);
				link(`I think I understand @lys better than you and unlike you, I'm able to maintain the discipline on my ship! `, 101);
				break;
					
			case 2:
				mile.ship_drama = 2;
				txt(`@lys furiously knocked on the door of my cabin and then entered. `);
				lys(`Did you talk with @ste? You should not believe him! He hates me because he's an incompetent, jelaous bastard!`);
				kat(`Really? `);
				lys(`Yeah! He's just still pissed I fucked other guys even though I always made clear we were not together! Despite what he believes, I did not backstab him! You can't blame me for not wanting to go down with him nor for trying to become a captain when he was gone! `);
				txt(`She leaned closer and whispered:`);
				lys(`And you should better watch out! He still wants this ship for himself and he's willing to do anything! <i>Anything!</i>  `);
						
				link(`Thanks for the clarification. `, 101);
				link(`What an asshole! We should keep an eye on him!`, 101);
				link(`It seems you were pretty eager to replace him! I won't let <i>anybody</i> to steal my ship!`, 101);
				break;
						
			case 10:
				present(["lys", ["fDarkThinBelt", "fLeggings", "fThong"]]);
				effigy.lys.draw();
				ste(`Can you tell that whore something? She has no shame! `);
				txt(`@ste come to complain about @lys. It was a scorching hot day and she decided to go topless. Her athletic body was covered with sweat and her bare breasts were attracting many eyes.`);
				ste(`Tell her to cover her tits, she's distracting everybody!`);
				txt(`@lys noticed us staring and joined us. `);
				lys(`Is there any problem? `);
				txt(`She was smirking, @ste's obvious annoyance was making her very happy. `);
						
				link(`Cover your tits, woman! You're distracting everybody and we don't want to hit cliffs! `, 210);
				link(`No problem! She can dress as she fancy and you should more focus on your work than on her tits! `, 211);
				break;
					
				//Love triangle of Stede & Lyssa & Kate	
			case 11:	
				quickSaveOutfit(effigy.lys);					
				txt(`It was a late evening when @lys visited me. `);
				lys(`Good evening!`);
				kat(`Hey. What do you need? `);
				lys(`The question isn't what I need but what <i>you</i> want! `);
				txt(`She stepped closer and her gentle fingers lightly touched my hair. @lys smirked:`);
				kat(`You know how much I'm attracted to men in charge! And I guess women too! `);
				lys(`You're trying to seduce me just to gain more power, aren't you?!`);
				kat(`And does it really matter? `);
				txt(`She was stripping her top.	 `);
						
				link(`I guess not! `, 221, () => mile.ship_boatsex = 1 );
				link(`I'm not a guy who thinks only with his dick! We can have sex but don't expect any special treatment! `, 221, () => mile.ship_boatsex = 1);
				link(`I'd prefer to keep our relationship professional. `, 220, () => mile.ship_boatsex = -1 );
				break;
						
			case 12:
				txt(`I walked around the ship, checking if everything was in order. On the forecastle was standing @lys, her long red hair flying in the breeze. She was joking and flirting with ${mile.ship_gal ? name.gal : "a handsome sailor. "}`);
				ste(`What did I tell you! She's doing it just to make you jelaous!`);
				txt(`@ste was suddenly next to me.`);
				kat(`Making me jelaous!?!`);
				ste(`Yeah... there are lot of gossips about what were you doing in your cabin.`);
				if(mile.slut <= 2){
					kat(`Oh...`);
					txt(`I would much prefered my sexual life staying private. `);
				}else if(mile.slut > 5){ //TODO
					kat(`I could imagine she's wild in a bed but I had no idea how much! You should've warned me! `);
					txt(`@ste shrugged.`);
				}
				ste(`She's a bitch and she's intentionally trying to make you jelaous. It's just one of her twisted petty power plays. `);
						
				link(`She can get gang-banged by the whole crew if she wants to, I don't care.`, 101, () => mile.ship_boatsex = -2);
				link(`I'll show her! `, 230, () => mile.ship_boatsex = 2); 
				break;
						
			case 13: 
				mile.ship_boatsex = 3;
						
				txt(`That slut @lys continued flirting with everything on board, trying to make me desperately contend for her attention. But I ignored her and instead flirted with @ste. At first, he was concerned but quickly got into it. He loved the chance to get back on his ex. And I believed he did not mind flirting with somebody as attractive and desirable as me. `);
				lys(`Hey, girl! `);
				txt(`@lys smiled at me, leaned closer and continued:`);
				lys(`What are you doing tonight? If you're free, I have a suggestion!`);
				txt(`Then she made a very obscene gesture with her tongue and fingers. ${dyke() ? "Her shamelessness was making me tingle. I would love her between my legs but I had to refuse. " : ""}`);
				kat(`Sorry! Maybe tomorrow, I already have something and I can't postpone it. `);
				lys(`You can postpone anything, you're the captain! `);
						
				if(mile.ship_xo === "ste"){
					kat(`I have to discuss important things with my first officer. `);
				}else{
					kat(`I have to discuss some navigation issues with @ste.`);
				}
				lys(`<i>Discuss</i>,  I see. `);
				txt(`I just shrugged. `);
				kat(`I'm not sure what are you implying. `);
				lys(`You know well what I'm implying! Whatever! I don't care! You're free to spend time with that loser if you for some reason enjoy it!`);
				kat(`@ste is a very intelligent and suave gentleman. `);
				lys(`I DON'T CARE! `);
						
				link(`Let her sulking. `, 101);
				con_link(mile.slut > -2, `low slut`, `Invite @ste to your cabin. `, 232);
				
				break;
					
			case 14: 
				mile.ship_boatsex = 4;
				showLess(1, 0, -1);
				txt(`The whole day was @lys visibly annoyed. The fact I might prefer her ex-lover over her was slowly consuming her. After the first watch, she just without asking barged in my cabin. `);
				kat(`@lys? How can I help you?`);
				lys(`I know what are you trying to do! You're sleeping with @ste, trying to make me jelaous! But you're wasting your time!`);
				kat(`I don't think my time with him was wasted. He's a pretty good lover if one isn't selfish and can appreciate him. `);
				lys(`Don't try to pretend you would pick him over me!`);
				kat(`You're very good-looking... but you're lacking certain important body parts... `);
				txt(`I condescendingly teased her until she flared up. `);
				lys(`I'll show you! `);
				txt(`She jumped on me ${PC.upperOuter ? `stripped my ${PC.upperOuter.name}, ` : ""}and toppled me on the bed. She was kissing my neck and her right hand assuredly ${PC.upper ? ` reached under my ${PC.upper.name} and ` : ""}fondled my tits. @lys was definitely going all in and could only lie there and be amazed. `);
				kat(`Ahhh! `);
				txt(`I cried out. @lys disappeared between my legs and her tongue deviously zigzagged over my slit. She brought me nearly to the edge and then returned to making out with me, her lips still glistening with my wetness. `);
				lys(`Tell me how much you love this! `);
				kat(`I... I love it... very much... Ahhh!`);
				lys(`You're so desperately horny, aren't you?!`);
				kat(`I... I am! ...ah @lys! Oh! `);
				txt(`She twice patted my pussy and then her fingers entered me. She was curling them inside, roughly teasing me. `);
				lys(`You want to cum so much, don't you?`);
				txt(`She whispered in my ear and her teeth were nibbling my earlobe.`);
				kat(`I... I do! `);
				lys(`Beg me for it!`);
				kat(`Please, @lys, make me cum!`);
				lys(`I will. But remember, don't play games with me! You will only lose!`);
				kat(`AHHHH!`);
				txt(`I just grunted, the strong orgasm let me incapable to coherently answer. Furious @lys finally slowed down. She just cuddled to me and tenderly kissed me.`);
				lys(`So was this the best sex of your life? `);
				kat(`Well...`);
				txt(`She threateningly lightly squeezed my left nipple and warned:`);
				lys(`There's only one correct answer!`);
				kat(`Yes, @lys, of course, it was. `);
												
				link(`Continue. `, 101, showAll);
				break;
						
						
			case 15:
				mile.ship_boatsex = 5;
				if(mile.slut < 5){
					txt(`We were traveling and there was nothing interesting happening.	I crossed the deck from the bow to the stern and back. I knew what I wanted but I was too embarrassed to admit it. Now I was one of the pathetic perverts who craved to have sex with NPC's in virtual games. Maybe... maybe one more time... `);
				}else{
					txt(`We were traveling and there was nothing interesting happening. And I felt kinda horny and would not mind a quick, naughty fun. `);
				}
				txt(`I very much enjoyed what we did with @lys. On the other hand, crawling back to her and begging for more did not seem to be very tactical. `);
						
				link(`Talk with @ste. `, 240);
				link(`Talk with @lys. `, 241);
				break;
						
						
			case 16:
				mile.ship_boatsex = 6;
				if(mile.ship_sir){
					sir(`Captain!? Have you seen @lys? `);
					kat(`No. Why do you need @lys?`);
					sir(`I wanted to ask her whether she had seen @ste. I want to borrow a book from him. `);
					kat(`You could just ask me about @ste.`);
					sir(`Right! Have you seen him? `);
					kat(`No, I have no idea where he is. `);
					//TODO SAILOR
				}else /*if(mile.ship_gal)*/{
					gal(`Captain!? Have you seen @lys? `);
					kat(`No. Why do you need @lys?`);
					gal(`I wanted to ask her whether she had seen @ste. I want to borrow a book from him. `);
					kat(`You could just ask me about @ste.`);
					gal(`Right! Have you seen him? `);
					kat(`No, I have no idea where he is. `);
				}
				link(`Search for them. `, 245);
				break;

				
				//Sirael's events 
			case 20:
				quickSaveOutfit(effigy.sir);
				txt(`@sir turned to me. `);
				sir(`Are you doing fine? `);
				kat(`...yes?`);
				sir(`You seem troubled.`);
				if(mile.immersion < 0){
					kat(`Well, I guess I'm a little. You know, all the problems with the nerds and my friends and the evil AI. `);
					sir(`Yes, the evil is a great threat for us all. `);
					kat(`...yeah.`);
				}else{
					kat(`Well, I guess I'm a little. You know, being chosen one and saving the world and so on. `);
				}
				sir(`Do you want me to help you?`);
				kat(`Thanks but I'm not sure how you could help me with that. `);
				sir(`I may help you to relax by serving you sexually. It always helps Master Arazor when he is troubled by a difficult problem.`);
				kat(`Oh! `);
				if(mile.slut < 4){
					txt(`I anxiously looked around but luckily nobody was paying any attention to our conversation.`);
				}else if(dyke()){
					txt(`I smiled. It was a very enticing offer. `);
				}
						
				con_link(mile.sub < 1, `submissive`, `Wait for me in my cabin, undressed. I'll be there in ten! `, 261, () => counter.sirsex = 1);
				con_link(mile.sub > 4, `dominant`, `I'm not sure about this...`, 261, () => counter.sirsex = 2);
				
				link(`Oh... okay. Let's do this!`, 261, () => counter.sirsex = 0);
				link(`I won't force you to be my whore!`, 260);
				link(`Sorry, but I'm not interested. `, 101, () => mile.ship_sirsex = -1);
				break;
						
					
			case 21:
				mile.ship_sirgal = 1;
				txt(`Me and @gal were discussing what was going on on the ship. ${mile.ship_lys && mile.ship_ste ? `Unlike @ste and @lys we were outsiders who did not understand ships wery well. ` : ""}`);
				gal(`What do you think about @sir? What she even is? `);
				kat(`She told me she's a homunculus made by ${mile.immersion < 0 ? "the wizard" : "Arazor"}. `);
				gal(`Honestly, she creeps me out. I don't think I've ever seen her smile.`);
				kat(`Have you tried better jokes? `);
				if(mile.ship_lys && mile.ship_ste){
					gal(`Or get angry. `);
					kat(`Do you think we need even more people getting angry and screaming at each other? `);
					gal(`Or show any other emotion. `);
				}else{
					gal(`Or get angry. Or show any other emotion. `);
				}
						
				link(`She creeps me out too. But don't worry about her. `, 101, () => mile.ship_sirgal = -1);
				link(`I don't believe her. She's Arazor's spy. Could you try to get closer to her and find more?`, 101);
				link(`Try to be nice to her! I think she has potential to grow and can learn more about human emotions. `, 101, () => mile.goth_sir_indy++);
				break;
						
			case 22:
				mile.ship_sirgal = 2;
				kat(`Hey! How are you doing?`);
				txt(`I greeted @sir on the aftercastle. `);
				sir(`I am doing well. `);
				kat(`Does the crew treat you nice? `);
				sir(`Yes. Especially @gal. `);
				kat(`Really? `);
				sir(`He tried to explain to me what is love. `);
				kat(`What is love?! I'll tell that horny jerk to stop harassing you!`);
				sir(`Oh no! He was not harassing me! We talked about people and their emotions. It was very interesting. `);
				kat(`Oh. That's fine if you were just talking. `);
				sir(`We also slept together but that was unrelated to our discussion. `);
				link(`Oh...`, 101);
				break;
						
						
			case 30:
				mile.ship_scepter = 1;
				txt(`${mile.immersion > 0 ? "The Scepter of Torrywialen" : "The scepter"} was making me paranoid. I was afraid to transport a such priceless item, especially considering what a ragtag bunch of misfits my crew was. I was seeing mutiny everywhere. `);
				link(`Continue. `, 101);
				break;
					
		}
	}
	break;


		//Stede dislikes Lyssa
	case 210:
		mile.goth_lys_topless = -1;
		lys(`Am I really distracting everybody? Including you? `);
		txt(`She playfully winked but when she realized I was serious, she shrugged and put a blouse on. But not like the thin fabric drenched with sweat and tightly clinging to her body was a huge improvement. `);
		link(`Continue. `, 101);
		break;
				
	case 211:
		mile.goth_lys_topless = 1;
		lys(`Yeah! Stop being such a horny creep, @ste!`);
		ste(`Whatever! When - and I say when, not if - one of the men will get hurt because he'll be too distracted with your tits, it won't be my fault!`);
		lys(`Well, it won't be my fault either!`);
		ste(`You callous bitch!`);
		link(`Continue. `, 101);
		break;
				
		//Love triangle of Stede & Lyssa & Kate
	case 220:
		lys(`What!? Professional relationship!?! Are you fucking serious!?!`);
		txt(`@lys was furious, she did not expect somebody might refuse to sleep with her and her pride was seriously hurt. `);
		lys(`What the hell is wrong with you?!?! Fine, whatever!`);
		txt(`She slammed the door. `);
		link(`Continue. `, 101);
		break;
				
	case 221:
		showLess(0, 0);
		effigy.lys.showLess(0,0);
		//toNudity(effigy.lys);
		mile.lesbian++;
		if(mile.sub < 1){
			lys(`Don't worry! I'm fully aware of who is my captain!`);
			if(mile.slut > 5){
				kat(`Good! You should be! Now start undressing, you horny tramp, don't let your captain wait!`);
				lys(`Aye, ma'am!`);
			}else{
				kat(`Good! You should be! Now kiss me, that's an order!`);
				lys(`Aye, ma'am!`);
			}
		}else{
			lys(`Don't worry! I'm fully aware of who is my captain! But <i>I</i> give orders <i>here</i>. I want you without clothes, now!`);
			kat(`Aye, ma'am!`);
		}
				
		if(mile.slut < 3){
			txt(`Soon we were both naked, making out, trying to fit on my narrow bunk. I did not have a high opinion of people who exploited virtual games for sexual gratification. But after all the bullshit I went through I kinda felt I deserve a little reward. And @lys was so attractive and wildly sensual. Being with her felt so natural, there were no worries, no need to impress her. I did not believe I would be able to so have such casual lesbian sex in real life. This was a unique opportunity to experience it. `);
		}else if( dyke() ){ //TODO mile.lesbian > 0
			txt(`Soon we were both naked, making out, trying to fit on my narrow bunk. I wanted her since the first moment I saw her. I knew there had to be a chance to fuck her, sooner or later and it was finally here! I wish there were more girls like her in the real life, confident and sensual, not hesitating for a moment when she was wildly kissing me. `);
		}else{
			txt(`Soon we were both naked, making out, trying to fit on my narrow bunk. She was so confident, wild and sensual! Taking her with me was the best decision I ever made!`);
		}					
				
		kat(`Ahhhh, @lys!`);
		lys(`Ahhh, captain!`);
		txt(`Her fingers were inside me and mine were inside her. Her red ruffled mane was tickling my face when we were kissing. I envied @lys her flawless athletic body but she generously shared it with me. `);
		lys(`AHHHHH!`);
		txt(`@lys loudly screamed when she orgasmed. I bet that bitch wanted everybody on the ship to hear what we were doing. `);
		txt(`I stayed on the bunk, watching her slowly dress. `);
		lys(`Don't worry! I won't exploit this and won't ask for an extra share of the booty! But if I made such an impression and you want to give it to me anyway, I won't mind. `);
		kat(`You opportunistic slut!`);
		txt(`She loudly laughed when she was leaving. `);
				
		link(`Continue. `, 101, () => {
			showAll();
			effigy.lys.showAll(); //TODO??
			//quickLoadOutfit(false, effigy.lys);					
		});
		break;
				
		
	case 230: 
		//mile.sub--; TOOD??
		kat(`Yeah!?! Two can play that game and I'm a bitch too! `);
		ste(`What do you mean?!`);
		kat(`Pretend you're saying a funny story to me!`);
		ste(`But I don't recall any funny story. `);
		kat(`Just pretend. `);
		ste(`I don't think this is a good plan. She'll see straight through it and we'll look pathetic and desperate. `);
		kat(`Hahaha! What a funny story, @ste! `);
		txt(`I smiled at @lys who just glanced in our direction. `);
		link(`Continue. `, 101);
		break;
		
	case 232: 
		kat(`@ste?! Can you visit my cabin after the second dog watch? `);
		ste(`Why?`);
		kat(`To have spite sex. `);
		ste(`Spite sex?!`);
		kat(`The best sex there is! `);
		link(`Evening. `, 233);
		break;
				
	case 233:
		mile.ship_boatsex_ste = true;
		//TODO SEX WITH OLDER MEN?
		txt(`I let @ste in. `);
		ste(`Good eveninng, captain!`);
		kat(`Good evening${mile.ship_xo === "ste" ? ", first officer" : ""}! `);
		ste(`I would lie if I say I haven't imagined this moment. But with my bad luck, I doubted you might be interested. And moreover, I have a very bad experience with the captain fucking members of his crew. I'm pretty sure this will end badly too... but of course, I don't mean we should stop! I'm fully willing to risk it!`);
		kat(`Now you're talking!`);
		txt(`We felt silent. Before I began to contemplate whether to make the first move, @ste took the plunge. He took my chin and for a moment stared deeply into my eyes. `);
		ste(`You're a beautiful woman, captain. `);
		kat(`Please, don't call me captain! It makes me feel like an old rugged guy!`);
		txt(`He smirked and then kissed me. We quickly began undressing. `);
		kat(`Did @lys notice you were going here? `);
		ste(`I hope she did!`);
		kat(`I bet she's right outside with an ear on the door!`);
		ste(`Damn, you know how to make me horny! `);
		txt(`I laughed and he laid me down on my bunk. We were making out and I could feel his erected dick pressing against my body. He did wonderful things with his tongue and made me moan: `);
		kat(`Och, @ste! I want it!`);
		txt(`He was happy to oblige and pushed his hard cock in my wet pussy and began fucking me. `);
		kat(`Och, yeah! I love it!`);
		ste(`Och, @lys! ...I mean @kat! ...fuck...`);
		kat(`You fucking dumbass! Way to ruin the mood! `);
		ste(`Sorry... I guess I focused on making @lys angry too much...`);
		kat(`You're supposed to focus only on me when you're inside me! `);
		ste(`Sorry! Now I'm focusing only on you! Och, @kat!`);
		txt(`@ste tried to save the situation. He thrust harder, making me feel so lovely I decided to forgive his faux pas.`);
		txt(`Fortunately, he climaxed without further incidents. I suggested he should stay. Falling asleep cuddled to him was more comfortable than falling asleep alone. `);
		link(`Continue. `, 101);
		break;
				
	case 240:
		kat(`Hello, @ste? I have to talk with you about complex navigation issues.  `);
		ste(`What navigation issues?!? Tell me! `);
		kat(`Yeah. It's the kind of navigation issue we could discuss only alone in my cabin.  `);
		ste(`Oh! I see! I'm very sorry, but I currently don't have time for... well... discussions... `);
		link(`Sure, no problem! `, 101);
		link(`You disappointed me, @ste! `, 101);
		break;
				
	case 241:
		kat(`Hey, @lys! `);
		lys(`Hey! `);
		kat(`Your captain needs you! In her cabin! Now! `);
		lys(`Why?  `);
		kat(`You know! `);
		lys(`Tell me! `);
		kat(`To fornicate.  `);
		lys(`Why with me?  `);
		kat(`What do you want to hear?  `);
		lys(`That I'm the finest piece of ass on this ship and possibly on the whole Dewyrain sea! `);
		txt(`She was too sassy but I was horny so I nodded. `);
		kat(`Yes, you are, @lys. Can we go now?  `);
		lys(`I'm very sorry, skipper, but I'm not in the mood right now! `);
		link(`Whatever, it was just a suggestion. `, 101);
		link(`You teasing bitch! `, 101);
		break;
				
	case 245:
		mile.ship_power_couple = true; //TODO - power_couple BETTER INTRODUITNO 
		txt(`It was weird, usually either @ste or @lys were on the deck. I assumed they argued and one of them finally snapped and killed the other and now tried to hide the body. `);
		kat(`@ste? ...oh!`);
		txt(`I found them both. @lys was sitting on @ste's lap. They were both bottomless (plus @lys had her whorish tits out) and it was pretty obvious what they were doing. `);
		lys(`Och, sorry! `);
		txt(`She did not sound she was sorry. What a bitch. `);
		kat(`W.. what is going on? `);
		lys(`Well... we were talking about old times... and realized our bickering was pointless...`);
		ste(`And we kinda got back together. `);
		link(`Thats.... great? `, 101);
		break;


		//Sirael's events 
	case 260:
		mile.goth_sir_indy++;
		sir(`But I offered it to you without you asking?`);
		kat(`Yes, but I don't want you to feel obliged to serve me sexually if you don't want to do it. `);
		sir(`Thank you captain but I <i>do</i> want to do it. `);
		
		con_link(mile.sub < 1, `submissive`, `Wait for me in my cabin, undressed. I'll be there in ten! `, 261, () => counter.sirsex = 1);
		con_link(mile.sub > 4, `dominant`, `I'm not sure about this...`, 261, () => counter.sirsex = 2);
		link(`Oh... okay. Let's do this!`, 261, () => counter.sirsex = 0)
		link(`Sorry, but I'm not interested. `, 101, () => mile.ship_sirsex = -1);
		break;
				
	case 261:
		mile.lesbian++;
		mile.ship_sirsex = 1;
		showLess(-1, 0);
		//toNudity(effigy.sir);
//TODO ASAP TEST 
		effigy.sir.showLess(0, 0);
		if(counter.sirsex === 1){
			mile.sub--;
			txt(`I firmly ordered her and before finished my job on the deck. It may take me a bit longer but I was sure she will wait for me there. When I arrived, @sir was motionlessly standing in the middle of my cabin, completely nude, except for the collar around her neck.`);
			if(mile.slut > 2){
				kat(`Eat my pussy!`);
				txt(`I told her and she smiled:`);
				sir(`I'll be happy to!`);
			}else{
				txt(`But there my confidence wavered. @sir quickly helped me. `);
				sir(`What would you enjoy the most? Should I pleasure you orally? Or...`);
				kat(`...y..yeah, that sounds pretty good!`);
			}
		}else if(counter.sirsex === 2){
			sir(`You do not have to worry! I am very skilled and I can guarantee you will enjoy it!`);
			kat(`I have no doubts about that. But using you this way feels, dunno. `);
			sir(`There is no reason to have any doubts! `);
			txt(`She harshly told me, firmly grabbed my hand and dragged me below the deck. Before I was able to gather my thoughts, she was already completely nude, except for the collar around her neck.`);
			sir(`Lie down and spread your legs! I'll pleasure you orally. `);
			kat(`O... okay! That sounds good!`);
		}else{	
			txt(`Together we went to our cabin. @sir was not waiting for anything. She was quickly undressing until she was completely nude, except for the collar around her neck. `);
			sir(`What would you enjoy the most? `);
			kat(`What are you offering?`);
			sir(`I could pleasure you orally. Or...`);
			kat(`I think that sounds good! `);
		}
				
		txt(`I striped, lay down on my bunk and she crawled between my legs. `);
		kat(`I'm not sure how experienced you are, unlike your master I have no dick. `);
		sir(`I am very experienced! Sometimes is Master Arazor tired or has work he has to finish and so he tells me to have fun with one or more of my s${String.fromCharCode(105)}sters and let us perform for his amusement. `);
		kat(`What a lucky ba..-AH! `);
		txt(`I exhaled when her tongue teased the most sensitive bit of my body.  `);
		sir(`Indeed! Master Arazor is very lucky he has servants like me and my tw${String.fromCharCode(105)}ns!`);
		txt(`She added while she was caressing me with her fingers, without raising her eyes from my pussy. `);
		kat(`You seem very close! `);
		sir(`I suppose so. I like all my sisters. But I think we are closest with the Third. The Second could be a bit cold and the Seventh quite arrogant and rough. Do you want to see what the Third enjoys the most? `);
		kat(`Ahhhhh!`);
		txt(`I panted when she pushed her tongue inside me. `);
		kat(`Fuck! Yes! I'm sure... Ah! You're the most... Ahhh! popular one! With... Ahhh! ...such nimble tongue!`);
		txt(`But @sir did not answer. Her mouth was a more important mission than to talk. She kissed, sucked and licked my pussy until I reached that ephemeral moment of bliss. `);
		kat(`I certainly feel less stressed now!`);
		sir(`Thank you very much! You can use me at any time!`);

		{		
			//TODO
			const untackle = () => {
				showAll();
				effigy.sir.showAll();
				//quickLoadOutfit(false, effigy.sir); //TODO - SHOULD BE FALSE????
			}
					
			link(`I certainly will!`, 101, untackle);
			link(`Please don't call it using you! I hope you had fun too!`, untackle);
			link(`Thank you! Your master is a genius. `, 101, untackle);
		}
		break;
	
	
		
		//ISLANDS 500; 600	
				
	
	//KARGED
	case 500:
		placetime(`Necropolis on Karged`);
		if(mile.ship_karg === 2){
			txt(`We landed on the Karged, unloaded our equipment and prepared the excavation. `);
			enter(`Search for Sceptre of Torrywialen. `, `karged`, 501, 101, `karged`); 
		}else if(mile.ship_karg === 3 || mile.ship_karg === 4){
			txt(`The ancient necropolis consisted of several lesser tombs. They probably did not contain anything important or interesting but I could decide to raid them anyway.`);
			if(mile.ship_karg === 3) enter(`Explore Karged tombs. `, `karged_2`, 501); 
			if(mile.ship_karg === 4) enter(`Explore Karged tombs. `, `karged_3`, 501); 
		}else{
			txt(`The tunnels leading to several other tombs were caved in and it would take weeks to open them.`);
		}
		level_up(`Back on the ship. `, 101);
		break;
			
	case 501:
		placetime(`Necropolis on Karged`);
		mile.ship_karg++;
		if(mile.ship_karg === 3){
			txt(`We did not found the sceptre but we found an important clue. `);
		}else if(mile.ship_karg === 4){
			txt(`We looted a tomb of ancient Aquilonian queen. `);
		}else if(mile.ship_karg === 5){ //TODO - was changed from 4 without testing
			txt(`We looted another Aquilonian tomb. `);
		}
		level_up(`Back on the ship. `, 101);
		link(`Stay on the island. `, 500);
		break;
			

	//YNYS YDYG
	case 510:
		placetime(`Ynys Ydyg`);
		if(mile.ship_ydyg < 3){
			mile.ship_ydyg = 3;
			txt(`@${[mile.ship_xo]} was concerned about fresh water and sent sailors to refill casks. I went with them and had a moment to talk with the local community. The fishermen were eager to hear all news. `);
			txt(`When we gained their trust, they shared their woes - it seemed that the village was harassed by the undead coming from a nearby tomb. The locals saw our weapons and hoped we might be in a mood for dungeon crawling. `);
			quest.initiate("goth_ydyg", "Destroy all draugrs in a tomb on Ynis Ydyg", "eliminate");
			enter(`The Tomb. `, "aquilonian_tomb", 510, 510, "goth_ydyg");
			link(`Back on the ship. `, 101);
		}else if(mile.goth_ydyg === 4){
			txt(`The excited villagers greeted me, happy to see me. `);
			link(`Back on the ship. `, 101);
		}else if( quest.is_done("goth_ydyg") ){
			mile.goth_ydyg = 4;
			quest.closure("goth_ydyg")
			txt(`The villagers were excited! They prepared a small celebratory feast for the crew and decided to name one of the cliffs after me. `);
			level_up(`Back on the ship. `, 101);
		}else if( quest.is_active("goth_ydyg") ){
			txt(`The villagers were staring at me. They wanted me to slay the undead but were too polite to urge me. `);
			enter(`The Tomb. `, "aquilonian_tomb", 510, 510, "goth_ydyg");
			link(`Back on the ship. `, 101);
		}
		break;
			
	//HENGAR (SMUGGLERS)	
	case 520:	
		placetime(`Hengar`);
		if(!mile.goth_smugglers){
			enter(`Hike up and explore abandoned fortress`, `abandoned_fortress`, 520, 520); 
		}else if(mile.goth_smugglers === 1 && (mile.ship_lys || mile.ship_ste) ){
			txt(`The discovery of the smuggler's base triggered an unexpected dispute. `);
			kat(`The Gothred authorities will want to hear about this. The Marshal will definitely pay us handsomely!`);
						
			if(mile.ship_lys && mile.ship_ste){
				lys(`Maybe we shouldn't tell them. `);
				ste(`Yeah, @lys is right! `);
				txt(`@ste suprisingly agreed with her. `);
				kat(`Why not? `);
				ste(`This place is perfect and might be useful in the future. `);
				lys(`We might need a secret haven. For whatever reason. `);
			}else if(mile.ship_ste){
				ste(`Maybe we shouldn't tell them. `);
				kat(`Why not? `);
				ste(`This place is perfect and might be useful in the future. We might need a secret haven.`);
			}else{
				lys(`Maybe we shouldn't tell them. `);
				kat(`Why not? `);
				lys(`This place is perfect and might be useful in the future. We might need a secret haven.`);
			}
					
			link(`Yeah, you might be right. We won't tell anybody about this. `, 101, () => mile.goth_smugglers = -1)
			link(`I need the money now, I don't worry what might or might not happen in the future! `, 101, () => mile.goth_smugglers = 2)
			link(`Forget about your pirate shenanigans! We don't need to hide from the law! `, 101, () => mile.goth_smugglers = 2)
			return;
		}else{
			txt(`We landed on the rocky island. `);
		}
		link(`Back on the ship. `, 101);
		break;
			
	//SHIPWERCK
	case 530:
		placetime(`Dangerous reefs`);
		if(mile.ship_wreck < 3){
			toUnderwear();
			toUnderwear(effigy[ext.follower?.key]);

			let desc = ``;
			if(mile.slut < 3){ //TODO SLUT
				desc += `I sent everybody  away, I did not want to be ogled by the whole crew when I was undressing. `;
				if(ext.follower){
					desc += `Feeling ${fol_name()} watching me was embarassing enough. But we did not want to be dragged down by our clothes and armor. `;
				}else{
					desc += `But despite my embarrassment, I could not dive to the wreck encumbered by my clothes and armor. `;
				}
			}else if(mile.slut > 6){
				desc += `I was quickly stripping so I could dive to the wreck without being encumbered by my clothes. The whole crew was covetously ogling. The more polite ones at least pretended they are not, which was pretty funny. `;
				if(ext.follower.sex === 1){
					desc += `I was sure that the comparison between ${fol_name()} and me will be the main theme of their discussion for the whole week and I surely hoped I will win. `;
				}
			}else{
				desc += `I was quickly stripping so I could dive to the wrech without being encumbered by my clothes. Even though @${[mile.ship_xo]} ordered them to focus on their work, the whole crew was of course staring. `;
				if(ext.follower.sex === 1){
					desc += `At least their attention could be divided between me and ${fol_name()}. `;
				}
			}
			txt(desc);
			enter(`Explore the  shipwreck`, "shipwreck", 530);
			link(`Back on the ship. `, 101, () => {
//TODO - true accesses inventory - right decision or not? 
				quickLoadOutfit(true);
				quickLoadOutfit(true, effigy[ext.follower?.key]);
				//updateDraw();
			});
		}else if(mile.ship_wreck === 3){
			mile.ship_wreck = 4;
			quickLoadOutfit(true);
			quickLoadOutfit(true, effigy[ext.follower.key]);
			//updateDraw();
			//TODO
			//txt(`The shipwreck was completely explored. `);
			level_up(`Back on the ship. `, 101);
		}else{
			txt(`The shipwreck was completely explored. `);
			link(`Back on the ship. `, 101);
		}
		break;

	
	//DATAN GULNAS (DWARFS)
	case 560:
		placetime(`Geshud Duradudos`);
		present(
			[2, `Engineer`, `commoner`], //TODO - easterner
			[1, `Curious engineer`, `commoner`], //TODO - easterner
			//[2, `Trader`, `commoner`], //TODO - easterner
		);
		txt(`Geshud Duradudos was completely different than any other city in Beletriand because of its characteristic mix of Eastern and dwarven architecture. `);
		//link(`Look around. `, 562);
		if(!mile.goth_sir_probed){
			mile.goth_sir_probed = 1;
		}else if(mile.goth_sir_probed === 1){
			npc(1, `What is this!?!`);
			kat(`Eh, hello? `);
			txt(`We were approached by one of the Arzenal engineers who was not able to take his eyes off @sir.`);
			sir(`I am @sir. `);
			npc(1, `Is it really an artificial being?`);
			sir(`Yes, I am. Thank you for noticing. `);
			txt(`He turned to me, pretty excited. `);
			npc(1, `I wish I could examine her in my lab. I would pay you well if you allow me to get my hands on her, at least for a moment. `);
			kat(`What do you mean? `);
			npc(1, `Take my tools and probe her a little. Don't worry, I don't damage her in any way! I'm sure a more invasive and destructive examination would provide better results but it would be a shame to ruin such a masterpiece. `);
			kat(`@sir? Would you mind?`);
			sir(`I do not want to be probed! But I will follow your orders. `);
			npc(1, `${Math.round(rewards.sir_probed * 0.75)} coins?`);
					
			link(`Sorry but she's borrowed and I might still need her and can't risk you damaging her. `, 560, () => mile.goth_sir_probed = -1);
			link(`Sure! But don't damage her in any way!`, 569, () => mile.goth_sir_indy--);
			link(`She's a member of my crew, not a toy! Nobody will probe her if she doesn't want to!`, 560, () => {
				mile.goth_sir_probed = -2;
				mile.goth_sir_indy++;
			});
			link(`Would you be willing to pay ${Math.round(rewards.sir_probed * 1.4)} ?`, 568, () => mile.goth_sir_indy--);
			return;
		}	
		link(`Market. `, 561);
		link(`Arzenal. `, 563);
		link(`Back to ship. `, 101);
		break;
					
	case 561:
		INVENTORY.shop("dorf", () => main(560) );
		break;
				
	case 563:
		if( quest.is_active("goth_dorf") ){
			npc(2, `Awesome! You arrived in the nick of time! You saved our factory!`);
			kat(`Eh... I don't have the lumber yet.`);
			npc(2, `Oh no!`);
			link(`Back. `, 560);
		}else if( quest.is_done("goth_dorf") ){
			quest.closure("goth_dorf");
			mile.goth_dorf = true;
			kat(`I have the lumber you asked for! Fresh straight from ${mile.immersion > 0 ? "H'dar Ğizra" : "the Hdar island"}.`);
			npc(2, `Seven heavens! You saved us! Well, for like week but but we have ordered other deliveries. Thank you very much!`);
			wage("goth_dorf");
			level_up(`Back. `, 560);
		}else if(!mile.goth_dorf){
			txt(`Among other things, geshud Duradudos was famous for making the finest heavy and siege weapons in the whole Beletriad.`);
			npc(2, `Hey! You're the skipper of that new ship, aren't you? `);
			kat(`That strongly depends on why are you asking. `);
			npc(2, `Are you from H'dar Ğizra?`);
			kat(`No. `);
			npc(2, `Seven hells!`);
			kat(`What's going on?`);
			npc(2, `We are waiting fo a very important delivery of lumber from H'dar Ğizra. We're working on a very big project that needs a lot of special wood. Without it there will be serious delays. If you're a merchant, it's a good opportunity for you - we can pay ${rewards.goth_dorf} coins. `);
			link(`I could be a merchant for ${rewards.goth_dorf} coins! `, 564);
			link(`I'm not a merchant. `, 560);
		}else{
			txt(`The engineers were polite but bussy and at the moment did not need anything else. `);
			link(`Back. `, 560);
		}
		break;
				
	case 564:
				
		if(mile.ship_hdar > 0){
			npc(2, `H'dar Ğizra is a large island...`);
			txt(`He begain explaining but I immediately smugly stopped him. `);
			kat(`Of course I know where ${mile.immersion > 0 ? "H'dar Ğizra" : "that weirdly named island"} is! I'm a very experienced sailor and navigator!`);
		}else{
			mile.ship_hdar = 1;
			npc(2, `H'dar Ğizra is a large island in the far south. You should sail in the southwest direction from Datan Gulnas. `);
			kat(`Thank you. I'll try to remember it. `);
		}
				
		quest.initiate(
			`goth_dorf`, 
			`Cut 8 rare trees on H'dar Ğizra and bring them to Datan Gulnas`, `collect`, 
			{desired: 8},
		);
		link(`Back. `, 560);
		break;
				
	case 568:
		counter.sirael_double = true;
		mile.goth_sir_probed = -1
		npc(1, `Hmmmm. I'm willing to pay ${rewards.sir_probed} coins.`);
		link(`Deal!`, 569);
		link(`No. `, 560);
		link(`I won't sell my friend just for ${rewards.sir_probed} coins! `, 560);
		break;
				
	case 569:
		mile.goth_sir_probed = 2;
		txt(`The satisfied engineer returned me @sir after several hours. Her expression was hard to read. `);
		kat(`Are you fine, @sir? `);
		sir(`I am not damaged in any way. `);
		kat(`What did he do to you? `);
		sir(`I would prefer to not share details. `);
		kat(`Fine!`);
		if(counter.sirael_double){
			wage("sir_probed");
		}else{
			wage(Math.round(rewards.sir_probed * 0.75));
		}
		link(`Back. `, 560);
		break;	
				
	//OSKYL
	case 600:
		placetime(`Oskyl`);
		if(mile.goth_arazor === 6){
			txt(`Despite the size of the island, it took us a long time before we found the entrance of the labyrinth. It was a narrow tunnel, right above the sea level. `);
			enter(`Explore the labyrinth. `, `oskyl`, 610, 600, `oskyl`); 
		}else{
			txt(`We were not able to found anything remarkable. Landing here was a waste of time.`);
					
		}
		link(`Back to ship. `, 101);
		break;
				
	case 610:
		placetime(`Oskyl labyrinth`);
		txt(`There was it! ${mile.immersion ? "The famed Sceptre of Torrywialen!" : "The scepter I was looking for! "}. It was negligently placed on a stone altar. My skin tingled, I could feel the power of the ancient artifact radiating through the whole room. `);
		if(ext.follower?.key === "sir"){
			kat(`@sir?`);
			fol(`Yes. This is, without doubt, the Sceptre of Torrywialen.`);
			kat(`Great.`);
			txt(`I walked to the altar and reached for the scepter. `);
			fol(`Wait, be careful!`);
			txt(`@sir tried to be warn me but it was too late. `);
		}else if(ext.follower?.key === "gal"){
			fol(`Can you feel the power!?!`);
			kat(`Yeah!`);
			fol(`Imagine what you could do with it!`);
			kat(`What do you mean?!`);
			fol(`Men would eagerly flock to support your cause. No matter what cause it would be. Being known as the wielder of ancient Aquilonian scepter would be enough! `);
			kat(`But we can't keep it. We promised to bring it to Arazor!`);
			fol(`You promised it. Not me. And if something happened to you... I think the crew wouldn't mind leaving Arazor's services. `);
			kat(`W... what!?! Are you going to betray me?!`);
			fol(`No! I'm always honorable and loyal! But I wanted to point out I could easily betray you if I wanted. `);
			kat(`So what?`);
			fol(`I think my loyalty deserves your extra gratitude!`);
			kat(`I won't be extra grateful just because you decided to not betray me!`);
			fol(`This is the kind of talk that makes people betray their ungrateful leaders!`);
			txt(`I walked to the altar and reached for the sceptre. `);
		}else if(ext.follower?.key === "lys"){
			fol(`Can you feel the power!?!`);
			kat(`Yeah!`);
			fol(`It's amazing! Isn't it?!?`);
			kat(`@lys? Do you know we can't keep it but have to bring it to Arazor? `);
			txt(`Her covetous expression made me concerned. I knew how much she loved booty. `);
			fol(`Don't worry! I learned my lesson, I decided to fly low and never overestimate my abilities. I'm not going to steal from an evil wizard. `);
			kat(`Arazor isn't an evil wizard.`);
			fol(`All wizards are evil!`);
			txt(`I walked to the altar and reached for the sceptre. `);
		}else if(ext.follower?.key === "ste"){
			kat(`Can you feel the power!?!`);
			fol(`Unfortunately yeah. I don't like it. The best would be to run and tell the others we didn't find anything. `);
			txt(`@ste was not impressed by the ancient Aquilonian artifact. `);
			kat(`Don't be such a buzzkill!`);
			fol(`Remember my words - it will bring us only misfortune. And probably gets us killed. `);
			txt(`I walked to the altar and reached for the scepter. `);
		}else{
					 txt(`I walked to the altar and reached for the scepter. `);
		}
								
		if(mile.feyd_yelaya_saved === 1){
			txt(`When I raised it, the ground began to shake. It looked like the whole labyrinth was going to collapse. The disaster triggered an unpleasant flashback. Oh, no, not again! I did not want to lose the scepter the same way I lost the treasure when I saved @yel`);
		}else if(mile.feyd_yelaya_saved === -1){
			txt(`When I raised it, the ground began to shake. It looked like the whole labyrinth was going to collapse. The disaster triggered an unpleasant flashback. Oh, no, not again! This was how @yel died!`);
		}else{
			txt(`When I raised it, the ground began to shake. It looked like the whole labyrinth was going to collapse. `);
		}
				
		if(ext.follower){
			fol(`We have to run!`);
		}
		txt(`But the tunnel collapsed! There were several other tunnels but they were filled with dark, cold water. Yet they were the only chance to get out! `);
				
		enter(`Swim through the tunels. `, `flooded_oskyl`, 620, 611); 
		break;
				
	case 611:
		placetime(`Oskyl labyrinth`);
		if(ext.follower){
			fol(`We can't stay here! `);
			kat(`I know! Just give me a moment, I'm to exhausted. `);
		}else{
			txt(`I returned to the chamber. For now, there was still breathable air. But who knew for how long!`);
		}
		enter(`Swim through the tunels. `, `flooded_oskyl`, 620, 611); 
		break;
			
	case 620:
		placetime(`Oskyl`);
		mile.goth_arazor = 7;
		txt(`We did it! The Sceptre of Torrywialen was ours! `);
		link(`Back on the ship. `, 101);
		//TODO
		break;
	}			
				
}


