import './seven.css';
import {remove_children} from "Libraries/dh";
import {ra} from "Libraries/random";
import {DICE} from "./dice";

//const path = `Images/Minigames/Dice/`;

const coins = [];
let coins_active = undefined;
let coins_value = undefined;
const bets = [];
let bets_active = undefined;
//let bets_value = undefined;
const rows = [];
let main; 
let roller;
let container
let money;
let end;

export function seven(ending, div, cash){
	end = function(){
		ending(money);
	}
	
	main = (()=> typeof div === "string" ? document.getElementById(div) : div)();
	money = cash;

	//container
	container = document.createElement("DIV");
	container.id = "seven_container";
	main.appendChild(container);
	
	create();
	

};



function add_row(){
		const temp = document.createElement("DIV");
		temp.classList.add("seven_row");
		container.appendChild(temp);
		rows.push(temp);
	}
	
	
function add_item(inner = ""){
	const temp = document.createElement("DIV");
	temp.classList.add("seven_item");
	temp.innerHTML = inner; 
	rows[rows.length - 1].appendChild(temp);
	return temp;
}





function add_coins(value){
	const temp = add_item(`${value}☼`);
	temp.classList.add("seven_coins");
	
	const index = coins.length;
	if(value && money >= value){
		temp.addEventListener("click", function(){
			if(index === coins_active){
				coins_active = undefined;
				coins_value = undefined;
			}else{
				coins_active = index;
				coins_value = value;
			};
			reload();
		});
	}else{
		temp.classList.add("seven_dead");
	}
	
	coins.push(temp);
}
	
	
function add_result(text){
	const temp = add_item(text);
	temp.classList.add("seven_result");
}


function add_dice(result){ //combine add_item && add_result
	const temp = DICE.display(result);
	rows[rows.length - 1].appendChild(temp);
//	temp.classList.add("seven_result");
}



function add_bet(top_desc, mid_desc, bot_desc){
	const temp = add_item();
	temp.classList.add("seven_bet");
		
	const index = bets.length;
	temp.addEventListener("click", function(){
		if(index === bets_active){
			bets_active = undefined;
		}else{
			bets_active = index;
		};
		reload();
	});
	
	const top = document.createElement("DIV");
	top.innerHTML = top_desc;
	top.classList.add("seven_top");
	temp.appendChild(top);
	
	const mid = document.createElement("DIV");
	mid.innerHTML = mid_desc;
	mid.classList.add("seven_mid");
	temp.appendChild(mid);
	
	const bot = document.createElement("DIV");
	bot.innerHTML = bot_desc;
	bot.classList.add("seven_bot");
	temp.appendChild(bot);
	
	
	rows[rows.length - 1].appendChild(temp);
	bets.push(temp);
}

function add_button(text, fce, clas, rolling = false){
	const temp = add_item(text);
	
	temp.classList.add("seven_button");
	temp.classList.add(clas);
	
	temp.addEventListener("click", function(){
		fce();
	});
	rows[rows.length - 1].appendChild(temp);
	//bet.push(temp);
	if(rolling) roller = temp;
}
	



function create(){
	remove_children(container);
	
	coins_active = undefined;
	coins_value = undefined;
	bets_active = undefined;

	coins.length = 0;
	bets.length = 0;
	rows.length = 0;
	
	//add_row();
	//add_item(`Coins: ${money}☼`);
	
	add_row();
	add_coins(1);
	add_coins(3);
	add_coins(5);
	add_coins(10);
	add_coins(money);
	/*add_coins(10)
	add_coins(10)
	*/
	
	add_row();	
	add_bet(`< 7`, `2, 3, 4, 5, 6`, `payoff 1:1`);
	add_bet(` 7`, ` `, `payoff 4:1`);
	add_bet(`7 <`, `8, 9, 10, 11, 12`, `payoff 1:1`);
	
	add_row();
	add_button(`Leave`, end);
	add_button(`Roll`, roll, `seven_roll`, true);
	
	reload();
}






function reload(){
	coins.forEach( (a,i) => {
		a.classList.remove("seven_active");	
		if(coins_active !== undefined){
			if(i === coins_active){
				a.classList.add("seven_active");
			}else{
			}
		}
	});
	bets.forEach( (a,i) => {
		a.classList.remove("seven_active");	
		if(bets_active !== undefined){
			if(i === bets_active){
				a.classList.add("seven_active");
			}else{
			}
		}
	});
	
	if(coins_active !== undefined && bets_active !== undefined){
		roller.classList.remove("seven_dead");
	}else{
		roller.classList.add("seven_dead");
	}
}

function roll(){
	if(coins_active !== undefined && bets_active !== undefined){
		rows[rows.length - 1].style.display = "none";
		const first = DICE.roll(); //ra.integer(1, 6);
		add_row();
		const second = DICE.roll(); //ra.integer(1, 6);
		const total = first + second;
		add_dice(first);
		add_dice(second);
		//add_result(`<img src="${path}${first}.gif">`);		
		//add_item(`+`);
		//add_result(`<img src="${path}${second}.gif">`);		
		add_result(`= ${total}`);
		
		let change = 0;
		if(bets_active === 0 && total < 7){
			change = coins_value
		}else if(bets_active === 2 && total > 7){
			change = coins_value
		}else if(bets_active === 1 && total === 7){
			change = 4 * coins_value;
		}else{
			change = -coins_value;
		};
		money += change;
		add_result(`${change}☼`);
		
		add_row();
		add_button(`Leave`, end);
		add_button(`Continue`, create, `seven_roll`);
	
	}else{
		return;
	}
}

 
