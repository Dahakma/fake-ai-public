import './tiles.css';
import {remove_children} from "Libraries/dh";
import {ra} from "Libraries/random";
import path from 'Assets/minigames/tiles_z.png';
import negative from 'Assets/minigames/tiles_x.png';

let end;
let pieces = [];
let container;
let peter; //the empty space


export function tiles(ending, div){
	end = function(){
		ending();
	};
	
	const main = (()=> typeof div === "string" ? document.getElementById(div) : div)();

	//container
	container = document.createElement("DIV");
	container.id = "tiles_container";
	container.style.background = `url(${negative})`;
	container.style.backgroundRepeat = "no-repeat";
	container.style.backgroundPosition = "center"; 

	main.appendChild(container);
	
	create();
	
};


function create(){
	pieces = [];
	let order = [];

	
	for(let i = 0; i < 3; i++){
		for(let k = 0; k < 3; k++){
			
			const divy = document.createElement("DIV");
			divy.className = "tile"; 
			divy.id = `${k}_${i}`; 
			container.appendChild(divy);
			if(k === 1 && i === 1){
				divy.style.visibility = "hidden";
				peter = divy;
			}else{
				divy.style.background = `url(${path}) ${-150 * k}px ${-150 * i}px`;
			}
			
			//location of the piece
			Object.defineProperty(divy, 'check', {
				get: function(){ return +this.style.order }
			});
			divy.place = function(a){ this.style.order = a };
			
			//moves the piece on the new location
			divy.addEventListener("click", function(){
				if(!this.active) return;
				const temp = this.check;
				this.place(peter.check);
				peter.place(temp);
				round();
			});
			
			pieces.push(divy);
			order.push(order.length);
		}
	}
	
	pieces.forEach( (a,i) => a.place(order[i]) );

	//shuffle into the initial order 
	do {
		for(let i = 0, available = []; i < 16; i++){
			update();
			
			available.length = 0;
			pieces.forEach( (a,i) => { 
				if(a.active) available.push(i);
			});
			let bob = pieces[ra.array(available)];
			
			const temp = bob.check;
			bob.place(peter.check);
			peter.place(temp);
		}
		console.warn("xxx");
	} while ( completed() );
	
	round();
}


function round(){
	update();
	
	if( completed() ){
		container.classList.add("triump");
		return setTimeout(end, 1600);
	}
}


function completed(){
	return pieces.every( (a,i) => a.check === i )
}
	
	
function update(){
	const empty = peter.check;
	
	pieces.forEach( a => {
		a.classList.remove("active");
		a.active = false;
	});
	
	pieces.filter( a => {
		return [-3,-1,1,3].includes(empty - a.check);
	}).forEach( a => {
		a.classList.add("active");
		a.active = true;
	});
}