//the game is basically classic Bulls and Cows/Mastermind 
//https://en.wikipedia.org/wiki/Bulls_and_Cows

import './bulls.css';

import {click, success, denied} from 'System/sounds';

let const_numbers = 4;  //the number of digits you are looking for 
let const_attempts = 8; //the number of possible attempts
let current_attempt = 0; //the current attempt
let current_number = 0; //the current digit

let result = []; //the number you are supposed to find
let quess = [];  //player attempt 
let game_result = 0;
let end_victory; //what is returned in the case of players victory
let end_defeat; //what is returned in the case of players defeat 
let main; //div into which is the game placed

let inbox = [];//?
let outbox = [];//?
const num_button = []; //buttons with numbers on numpad
let num_unlock; //button unlock on numpad



//handles the input and the ending of the game
function input(keycode){
	if(game_result==1){
		main.removeChild(document.getElementById("bull_cointainer"));
		end_victory();
		return		
	}else if(game_result==-1){
		main.removeChild(document.getElementById("bull_cointainer"));
		end_defeat(result);
		return		
	};
			
	if(keycode === 27){
		// * clear:
		quess = [];
		for(let k = 0; k < const_numbers; k++){
			inbox[current_attempt][k].innerHTML = "";
		};
		current_number = 0;
	}else if(keycode === 13){
		// > enter:
		if(current_number>=const_numbers){
			check();
			return
		};
	}else if(current_number < const_numbers){
		//number:
		const number = keycode - 96;
		for(var k = 0; k < const_numbers; k++){
			if(quess[k] === number) return 
		};
		inbox[current_attempt][current_number].innerHTML = number;
		quess[current_number]=number;
		current_number++;
	};
	click.play();
};

//compares the player's guessed number with the result
function check(){
	var green = 0;
	var yellow = 0;
	
	for(var i = 0;i<const_numbers;i++){
		for(var k = 0;k<const_numbers;k++){
			if(quess[i]==result[k])yellow++;
		};
	};
	
	for(var i = 0;i<const_numbers;i++){
		if(quess[i]==result[i]){
			green++;
			yellow--;
		};
	};
	
	if(green==const_numbers){
		var victory=true;
	}
	
	for(var i = 0;i<const_numbers;i++){
		if(green>0){
			outbox[current_attempt][i].classList.add("bull_green");
			green--;
		}else if(yellow>0){
			outbox[current_attempt][i].classList.add("bull_yellow");
			yellow--;
		}else{
			outbox[current_attempt][i].classList.add("bull_red");
		}
	};
	current_attempt++;
	current_number = 0;
	quess = [];
	if(victory){
		num_unlock.style.display = "block";
		success.play();
		game_result = 1;
	}else if(current_attempt==const_attempts){
		//num_unlock.style.display = "block";
		//num_unlock.innerHTML = "X X X";
		game_result = -1;
		denied.play();
	}else{
		denied.play();
	}
	
};



//creates the board 
function create(){ 
	
	//container
	const container = document.createElement("DIV");
	container.id = "bull_cointainer";
	main.appendChild(container);

	//div showing inputed numbers and results
	const topik = document.createElement("DIV");
	topik.id = "bull_topik";
	container.appendChild(topik);

	//div with the numpad
	const botik = document.createElement("DIV");
	botik.id = "bull_botik";
	container.appendChild(botik);

	//the numpad and buttons with numbers
	const numpad = document.createElement("DIV");
	numpad.id = "bull_numpad";
	botik.appendChild(numpad);

	
	//var num_row = [];

	function new_button(label, keycode){
		var index = num_button.length;
		num_button[index] = document.createElement("DIV");
		num_button[index].className = "bull_num_button";
		num_button[index].innerHTML = label;
		numpad.appendChild(num_button[index]);
		//num_row[num_row.length-1].appendChild(num_button[index]);
		
		num_button[index].addEventListener("click", function(){
			input(keycode);
		});
	};
/*	
	function new_row(){
		var index = num_row.length;
		num_row[index] = document.createElement("DIV");
		num_row[index].className = "bull_row";
		numpad.appendChild(num_row[index]);
	};
*/
	num_button.length = 0;
	
	//new_row();
	new_button("1",97);
	new_button("2",98);
	new_button("3",99);
	//new_row();
	new_button("4",100);
	new_button("5",101);
	new_button("6",102);
	//new_row();
	new_button("7",103);
	new_button("8",104);
	new_button("9",105);
	//new_row();
	new_button("*",27);
	new_button("0",96);
	new_button(">",13);

	new_button("> > >",13);
	
	num_unlock = num_button[num_button.length-1]
	num_unlock.classList.add("bull_num_unlock");
	//style.gridColumn = "1 / -1";
	//num_button[num_button.length-1].style.width = "auto";

	





	//div for showing inputed numbers and results 
	const inputed = document.createElement("DIV");
	inputed.id = "bull_inputed";
	topik.appendChild(inputed);

	for(let i = 0; i < const_attempts; i++){
		const temp_row = document.createElement("DIV");
		if(i%2==0){
			temp_row.className = "bull_row_even";
		}else{
			temp_row.className = "bull_row_odd";
		}
		temp_row.id = "bull_inputed_row"+i;
		inputed.appendChild(temp_row);

		inbox[i] = [];
		for(let k = 0;k < const_numbers;k++){
			inbox[i][k] = document.createElement("DIV");
			inbox[i][k].id = "bull_inbox"+i+"_"+k;
			inbox[i][k].className = "bull_inbox";
			temp_row.appendChild(inbox[i][k]);
		};
		
		const temp_outputed = document.createElement("DIV");
		temp_outputed.id = "bull_outputed";
		temp_outputed.className = "bull_inbox";
		temp_row.appendChild(temp_outputed);
		
		outbox[i] = [];
		for(var k = 0;k<const_numbers;k++){
			outbox[i][k] = document.createElement("DIV");
			outbox[i][k].id = "bull_outbox"+i+"_"+k;
			outbox[i][k].className = "bull_outbox bull_outbox_"+k;
			
			outbox[i][k].style.height = Math.floor(100/const_numbers)+"%";
			
			temp_outputed.appendChild(outbox[i][k]);
		};
	};
};




//START
/*
export function bulls_result(){
	return result.join("");
}
*/

export function bulls(victory, defeat, div, numbers = 4, attempts = 8){
	const_numbers = numbers;  //the number of digits you are looking for 
	const_attempts = attempts; //the number of possible attempts
	
	
	main = (()=> typeof div === "string" ? document.getElementById(div) : div)();

	
	//clears variables:
	current_attempt = 0;
	current_number = 0;
	game_result = 0;
	
	end_victory = victory;
	end_defeat = defeat;
	
	quess = [];
	result = [];
	
	//picks random number the player is supposed to find
	for(let i = 0; i <const_numbers; i++){
		for(let repeat = true; repeat==true;){
			result[i] = Math.floor(Math.random()*10);
			repeat = false;
			for(let k = 0; k < i; k++){
				if(result[i] === result[k]) repeat = true;
			};
		};
	};
	
	//for cheating
	/* eslint-disable no-console */
	console.log(">RESULT "+result);//LEE
	/* eslint-enable no-console */
	
	
	//create the board 
	create();
};