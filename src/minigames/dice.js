import './dice.css';

export const DICE = {
	roll(result){
		result ??= Math.ceil( Math.random() * 6 );
		return result;
	},
	display(result){
		result ??= this.roll();
		
		const container = document.createElement("DIV");
		container.classList.add("container");
		
			const face = document.createElement("DIV");
			face.classList.add("face");
			
			for(let i = 1; i <= result; i++){
				const pip = document.createElement("DIV");
				pip.classList.add("pip");
				face.appendChild(pip);
			}
		
		container.appendChild(face);
		return container;
	}
}
