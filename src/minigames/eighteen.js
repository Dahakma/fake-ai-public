import './seven.css';
import {remove_children, and} from "Libraries/dh";
import {ra} from "Libraries/random";
import {DICE} from "./dice";

const dice_delay = 400;
const reaction_delay = 400;

let main; 
let container
let money;
let end;
let bet;
let players;
let index;
let scores;

export function eighteen(ending, div, cash, pc = "Player", npcs){
	end = function(){
		ending(money);
	}
	
	main = (()=> typeof div === "string" ? document.getElementById(div) : div)();
	money = cash;

	//container
	container = document.createElement("DIV");
	container.id = "seven_container";
	main.appendChild(container);
	
	if(npcs === undefined){
		npcs = ["Stede", "Galawar", "Lyssa"],
	};
	
	players = [pc, ...npcs].map( name => {
		return {
			name,
			sum,
			active,
		}
	});
	players[0].pc = true;
	
	
	betting();
};

function betting(){
	remove_children(container);
	add_top();
	add_item(`${and(players)} are playing Eighteen. `)
	add_top();
	let temp = add_item(`Leave. `);
	temp.classList.add("clickable");
	temp.addEventListener("click", end);
	
	[1, 3, 5, 10].forEach( a => {
		let temp = add_item(`${a}☼`);
		if(a > money){
			temp.classList.add("dead");
		}else{
			temp.classList.add("clickable");
			temp.addEventListener("click", function(){
				start_round(a);
			});
		}
	})
}



function start_round(a){
	remove_children(container);
	add_top();
	
	bet = a;
	shuffle(players);
	players.forEach( (a,i) => {
		a.sum = 0;
		a.active = true;
		{
			let temp = add_item(`${a.name}`);
		}
		{
			scores[i] = add_item(`${a.sum}`);
			scores[i].style.align = `justify`;
			scores[i] = `score_${i}`;
		}
	});
	index = -1;
	
	
	
	rolling();

}



function rolling(){
	if( !players.some( a => a.active ) ) return end_round();
		
	index++;
	if(index === players.length) index = 0;
	
	score.forEach( a => a.classList.remove("marked") );
	score[index].classList.add("marked");
	
	if(players[index].pc){
		add_top();
		player_roll();
	}else{
		add_bot();
		const result = DICE.roll();
		players[index].sum += rolled;
		add_dice(result);
		
		setTimeout(npc_reaction, dice_delay);
	}
}

function npc_reaction(){
	
	const sum = players[index].sum;
	score[index].innerHTML = sum;
	
	if(sum > 18){
		add_item(`${players[index].name} is over 18! `);
		players[index].active = false;
		score[index].classList.add("red");	
	}else if(sum === 18){
		add_item(`${players[index].name} rolled 18! `);
		score[index].classList.add("lime");
		setTimeout(end, reaction_delay);
		return;
	}else if(sum > 12 && ra.boolean){
		add_item(`${players[index].name} decided to stop. `);
		players[index].active = false;
	}else{
		add_item(`${players[index].name} has ${sum} points. `);
	}
	
	setTimeout(rolling, reaction_delay);
}





function add_dice(result){ //combine add_item && add_result
	const temp = DICE.display(result);
	rows[rows.length - 1].appendChild(temp);
//	temp.classList.add("seven_result");
}


function add_top(){
	add_row("row_top")
}

function add_bot(){
	add_row("row_bot")
}


function add_row(css){
	const temp = document.createElement("DIV");
	temp.classList.add(css);
	container.appendChild(temp);
	rows.push(temp);
}

function add_item(inner = ""){
	const temp = document.createElement("DIV");
	temp.classList.add("item");
	temp.innerHTML = inner; 
	rows[rows.length - 1].appendChild(temp);
	return temp;
}




/*
	TODO 
	end round
	player decison
	css
	remove exept for toppest 


*/









