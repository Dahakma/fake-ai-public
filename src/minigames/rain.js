import {ra} from "Libraries/random";
import {remove_children} from "Libraries/dh";
import './rain.css';

const cards = [];
for(let suit = 1; suit <= 4; suit++){
	for(let rank = 7; rank <= 14; rank++){
		cards.push({rank, suit});
	}
}

let deck = [];
let hand = [];
let table = [];

let deal = 4;
const max_players = 4;
let delay = 2000;
let mistakes = 0.2;

let live_players = [];
let debug = true;
let only_winer = false; 

let container;
let main;
let end_victory;
let end_defeat;


let active_player = 0;
let active_special;


function end(win){
	
	console.error(hand)
	console.error( hand.map( a => a.length) )
	main.removeChild(document.getElementById("rain_container"));
	if(win){
	//	alert("You won!");
		end_victory( hand.map( a => a.length) );
	}else{
		//console.error( rain_result() )
	//	alert("You lost!");
		end_defeat( hand.map( a => a.length) );
	}
	
}

export function rain_result(){
	return hand.map( a => a?.length ?? 0 );
}

export function rain(victory, defeat, div, players = 4, setting){
	//TODO DEFAULT
	debug = setting?.debug ?? false;
	mistakes = setting?.mistakes ?? 0.2;
	delay = setting?.delay ?? 500;
	only_winer = setting?.only_winer ?? false;
	deal = setting?.cards ?? 5;
	
	
	end_victory = victory;
	end_defeat = victory;

	
	if(typeof players === "object"){		
		live_players = players;
	}else{
		switch(players){
			default:
			case 2: 
				live_players = [true, false, true, false];
				break;
			case 3: 
				live_players = [true, true, true, false];
				break;
			case 4: 
				live_players = [true, true, true, true];
				break;
		}
	}
	
	//console.error(live_players);
	active_player = 0;
	active_special = undefined;
	 
	main = (()=> typeof div === "string" ? document.getElementById(div) : div)();
	container = document.createElement("DIV");
	container.id = "rain_container";
	main.appendChild(container);
	initiate();
	display();
}



function initiate(){
	

	deck = cards.map( (a,i) => i );
	ra.shuffle(deck);
	
	for(let i = 0; i <= max_players; i++){
		hand[i] = [];
	}
	

	for(let k = 0; k < deal; k++){
		for(let i = 0; i < max_players; i++){
			if(live_players[i]){
				hand[i].push(deck.pop());
			}
		}
	}
	/*
	hand[0].push(13,13);
	hand[0].push(14,14);
	hand[0].push(8,8);
	*/
	table = [];
	table.push(deck.pop());
	//console.warn(deck);
	//console.log(hand);
}

function face_full(index){
	const {suit, rank} = cards[index];
	return `${[``,`<span class = "rain_red">♥</span>`,`♠`,`<span class = "rain_red">♦</span>`,`♣`][suit]}${[``,``,2,3,4,5,6,7,8,9,10,`J`,`Q`,`K`,`A`][rank]}`;
}

function face_short(index){
	const {suit, rank} = cards[index];
	return `${[``,`♥`,`♠`,`♦`,`♣`][suit]}${[``,``,2,3,4,5,6,7,8,9,10,`J`,`Q`,`K`,`A`][rank]}`;
}


function suit_symbol(i){
	if(i === `A`) return `Skip`;
	if(isNaN(i)) return "";
	if(i >= 7) return `Draw ${(i/7)*2}`;
	return `${[``,`<span class = "rain_red">♥</span>`,`♠`,`<span class = "rain_red">♦</span>`,`♣`][i]}`;
}

function face(index, div){
	const temp = document.createElement("DIV");
	temp.classList.add("rain_card");
	temp.innerHTML = face_full(index);
	div.appendChild(temp);
	return temp;
}

function reverse(div){
	const temp = document.createElement("DIV");
	temp.classList.add("rain_card");
	div.appendChild(temp);
	return temp;
}

function block(id){
	const temp = document.createElement("DIV");
	temp.id = id;
	//temp.classList.add("rain_row");
	container.appendChild(temp);
	return temp;
}


let div_table;
let div_deck;
let div_hand;
let div_center;


function queen(){
	if(active_player === 0){
		const changer = document.createElement("DIV");
		changer.id = "rain_changer";
		container.appendChild(changer);
		for(let i = 1; i <= 4; i++){
			const temp = document.createElement("DIV");
			temp.innerHTML = suit_symbol(i);
			temp.classList.add("rain_clickable");	
			temp.classList.add("rain_change_suit");	
			changer.appendChild(temp);
			event(temp, ()=> {
				active_special = i;
				next();
			})
		};
	}else{
		const count = [];
		count[0] = 0;
		for(let i = 1; i <= 4; i++){
			count[i] = 0;
			hand[active_player].forEach( a => {
				if(cards[a].rank !== 12 && cards[a].suit === i) count[i]++;
			})
		};
		
		const max = Math.max(...count);
		active_special = count.indexOf(max); //TODO BIASED!!
		next();
	}

}

function new_deck(){
		const last = table.pop();
		deck = table.reverse().map(a => a);
		table = [last];
		//alert(); TOOD
}

function display(){
	/*
	console.log(live_players);
	console.log(hand);
	console.log(deck);
	console.log(table);
	*/
	if(!deck.length) new_deck();
	
	
	remove_children(container);
	const active = table[table.length-1];
	
	const ta = block("rain_ta");
	div_table = face(active, ta);
	
	div_center = block("rain_ce");
	div_center.innerHTML = `${[`↓`,`←`,`↑`,`→`][active_player]} ${suit_symbol(active_special)} ${debug ? `(${active_special})` : ``}`;

	const de = block("rain_de");
	div_deck = reverse(de);

 

	div_hand = [];
	hand.forEach( (a,i) => {
		
		const hb = block(`rain_hand_${i}`);
		
		if(!live_players[i]) return;
		
	 
		a.forEach( (b,k) => {
			let c;
			if(i === 0 || debug){
				c = face(b, hb);
			}else{
				c = reverse(hb)
			}
			
			if(i === 0){
				div_hand.push(c);
			}
			
			
		
			if(!debug && i === 1){
				c.style.position = `absolute`;
				c.style.top = `${10 * k}px`;
				
			}
		
			if(!debug && i === 3){
				c.style.position = `absolute`;
				c.style.top = `${10 * k}px`;
				
			}
			
			
			if(!debug && i === 2){
				
				c.style.position = `relative`;
				c.style.left = `${(a.length * 20) - (60 * k)}px`;
			}
			
		
		});
		 
	});
	
	//display_test(); 
	
	
	
	if(active_player === 0){
		usable();
	}else{
		setTimeout(ai, delay);
	}
	
}





function ai(){
	let available = [];
	
	function rank_benefit(index){
		return hand[active_player].filter( a => cards[a].rank === cards[index].rank).length;
	}
		
	function suit_benefit(index){
		return hand[active_player].filter( a => cards[a].rank !== 12).filter( a => cards[a].suit === cards[index].suit).length;
	}
	
	function find_best_use(available){
		console.trace();
		let list = available.map( i => {return {
				index: i, 
				value: total_benefit(hand[active_player][i]),
				debug: face_short(hand[active_player][i]),
		}; });
		console.warn(list);
		list = list.sort(descending_random);
		
		if(Math.random() < mistakes){
			use(ra.array(list).index);
		}else{
			use(list[0].index);
		};
	}
	
	
	function total_benefit(a){
		let rank = rank_benefit(a);
		let suit = suit_benefit(a);
		console.log(rank,suit);
		if(rank > 1){ //there is bigger probability to have a correct suit than have a correct rank 
		//	rank *= 1.5; 
		}
		
		return rank + suit;
	}
	
	
	function descending_random(a, b){
		if(a.value == b.value) return -0.5 + Math.random();
		return b.value - a.value;
	}
		
		
console.warn("AI!!");		
		
	//seven
	 if(!isNaN(active_special) &&  active_special >= 7){
		//check if has seven
		hand[active_player].forEach( (a,i) => {
			if(cards[a].rank === 7){
				available.push(i);
			}
		});
		
		if(available.length){
			find_best_use(available);
		}else{
			const no = (active_special / 7) * 2;
			active_special = undefined;
			lick(no);
		}
		
	//out of cards
	}else if(!hand[active_player].length){
		//REMOVE PLAYER
		live_players[active_player] = false;
		if(live_players.filter( a => a).length <= 1 || only_winer){
			
			end(false); //TODO DRAW
		}else{
			next();
		}
		
	//ace 
	}else if(active_special == "A"){
		//check if has ace in hand
		hand[active_player].forEach( (a,i) => {
			if(cards[a].rank === 14){
				available.push(i);
			}
		});
		
		if(available.length){
			find_best_use(available);
		}else{
			active_special = undefined;
			next();
		}
	
	}else{	
		//suit was changed 
		if(!isNaN(active_special)){
			//check for cards the same suit as the changed 
			hand[active_player].forEach( (a,i) => {
				if(cards[a].suit === active_special && cards[a].rank !== 12){
					available.push(i);
				}
			});
		
		//default situation
		}else{
			//check for cards in hand with same suit or rank
			const active = cards[table[table.length-1]];
			hand[active_player].forEach( (a,i) => {
				if( (cards[a].rank === active.rank || cards[a].suit === active.suit) && cards[a].rank !== 12 ){
					available.push(i);
				};
			})
			
		}
		
		
		//emergency situation - one player will end the next round 
		if(hand.filter( (a,i) => live_players[i] && hand[i].length === 0 ).length){
			//checks if has sevens
			const sevens = available.filter( i => cards[hand[active_player][i]].rank === 7);
			if(sevens.length){
				find_best_use(sevens);
				return;
			}
		}
		
		//has some
		if(available.length){
			//finds the most benefitial
			find_best_use(available);
			
		//has none	
		}else{
			//seek for queens 
			hand[active_player].forEach( (a,i) => {
				if(cards[a].rank === 12){
					available.push(i);
				}
			});
			
			if(available.length){
				//finds the most benefitial queen
				find_best_use(available);
				
			}else{
				//licks
				lick(1);
			}
		}
		 
	}
	
	

}




function usable(){

	//seven
	 if(!isNaN(active_special) &&  active_special >= 7){
		//console.error(active_special);
		
		//check if has seven
		hand[0].forEach( (a,i) => {
			if(cards[a].rank === 7){
				event(div_hand[i], ()=> use(i) );
			}
		});
		
		//licks two (or more)
		event(div_deck, ()=>  {
		//	console.error(active_special);
			const no = (active_special / 7) * 2;
			active_special = undefined;
		//	alert(no);
			lick(no);
		});
	
	//out of cards
	}else if(!hand[0].length){
		//REMOVE PLAYER
		live_players[0] = false;
		end(true);
	
	//ace 
	}else if(active_special == "A"){
		//check if has ace in hand
		hand[0].forEach( (a,i) => {
			if(cards[a].rank === 14){
				event(div_hand[i], ()=> use(i) );
			}
		});
		
		//pass
		event(div_center, ()=>  {
			active_special = undefined;
			next();
		});
	
	
	//changed color
	}else if(!isNaN(active_special)){
		//check for cards the same suit as was changed 
		hand[0].forEach( (a,i) => {
			if(cards[a].rank === 12){
				event(div_hand[i], ()=> use(i) ); //TODO
			}else if(cards[a].suit === active_special){
				event(div_hand[i], ()=> {
					//active_special = undefined;
					use(i);
				});
			}
		});
		
		//lick
		event(div_deck, ()=> lick(1) );
	
		 
	//cards
	}else{

		//check for cards in hand with same suit or rank
		const active = cards[table[table.length-1]];
		hand[0].forEach( (a,i) => {
			if(cards[a].rank === 12){
				event(div_hand[i], ()=> use(i) ); //TODO
			}else if(cards[a].rank === active.rank || cards[a].suit === active.suit){
				event(div_hand[i], ()=> use(i) );
			}
		});
		
		//lick
		event(div_deck, ()=> lick(1) );
	}
	
	

}



function event(div, fce){
	div.classList.add("rain_clickable");	
	div.addEventListener("click", fce);
}



function next(){
	active_player++;
	if(active_player > max_players) active_player = 0;
	if(live_players[active_player]){
		display();
	}else{
		next()
	}
}






function use(index){
	const card_index = hand[active_player][index];
	const card_rank = cards[card_index].rank;
	
	table.push(card_index);
	hand[active_player].splice(index, 1);
	
	


	if(card_rank === 12 ){
		return queen();
	}else if(card_rank === 7 ){
		if(!active_special || isNaN(active_special) || active_special < 7){
			active_special = 7;
		}else{
			active_special += 7;
		}
	}else if(card_rank === 14){
		active_special = "A";
	}else{
		active_special = undefined;
	}
	
	next();
}

function lick(number = 1){
	for(let i = 0; i < number; i++){
		if(!deck.length) new_deck();
		if(!deck.length) break;
		hand[active_player].push( deck.pop() );
	}
	next();
}


function display_test(index){
	//remove_children(container);
	//console.log(cards);
	//console.log(deck);
	//console.log(hand[2]);
	block_test("deck", deck);
	block_test("table", table);
	block_test("2", hand[2]);
	block_test("1", hand[1]);
	block_test("0", hand[0]);
	

}

function block_test(name, array){
	const temp = document.createElement("DIV");
	
	/*temp.classList.add("rain_block");*/
	
	let text = array.reduce( (c,a) => `${c}; ${face_short(a)}`, "" );
	temp.innerHTML = `${name}: ${text}`;
	
	container.appendChild(temp);
}


