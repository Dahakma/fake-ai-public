//ASAP
import 'System/keyboard';

//CSS
import 'Gui/style/root.css'; //main settings, variables

import 'Gui/style/buttons.css'; //clikcable buttons
import 'Gui/style/text.css'; //game story text
import 'Gui/style/dynamic.css'; //tooltip

import 'Gui/style/legacy.css'; //should be reworked when I will like it
import 'Gui/style/wardrobe.css'; //inventory

//GUI
import Main from 'Gui/Main.svelte';
const app = new Main({
	target: document.getElementById('div_container'), 
});
export default app;


//utility command that allows manually reset settings to default 
import {gamekey} from 'System/variables';
window.RESET = function(){
	localStorage.removeItem(`${gamekey}_initiated`);
	location.reload(true);
}


//STARTS THE GAME 
import {initiate as initiate_dad} from "Avatar/canvas/initiate"; //loads Dynamic Avatar Drawer
import {autoload} from "System/save"; //loads latest saved game
import {start_game} from "System/initiate_game"; //starts the new game
initiate_dad("div_player", ()=>{ //after DAD is loaded, tries load the lates save, if there's none, starts new game
	autoload(start_game);
	document.getElementById("div_intro").style.display = "none";  //DISABLES INTRO SCREEN
}); 


