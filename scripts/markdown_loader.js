module.exports = function(data) {


	const escape = ``; //`\?` //initial escape sequence - atm not defined because / causes issues of being escape character in js too and I dont know what else would be better and whether the escape character is even needed



//HEADLINES H1 - H6
	function id(text){ //TODO - TEMPFIX
		text = text.toLowerCase();
		text = text.trim().replaceAll(" ", "-");
		return text;
	}

	for(let i = 6; i > 0; i--){
		function exec(match, text){
			return `<h${i} id = "${id(text)}" >${text}</h${i}>`;
		}
		const rex = new RegExp(`^#{${i}}(.*)`, `gm`)
		data = data.replace(rex, exec); 
	}


//PARAGRAPHS & LISTS & HRs 

	let active_ul = false;
	let active_ul_2 = false;

	data = data.split("\n").map( line => {
		//if( line.startsWith("#") || line.startsWith("/") ) return line;
		line = line.trim();

		if( line === "***" || line === "---" || line === "___") return  `<hr>`;
		if( line.startsWith("***") ) return  `<hr>`;

		//list 2
		if( active_ul && (line.startsWith("\t- ") || line.startsWith("\t* ")) ){
			if(active_ul_2){
				return `<li>${line.slice(1)}`;
			}else{
				active_ul_2 = true;
				return `<ul><li>${line.slice(1)}`;
			}
		}
	
		//list 1
		if( line.startsWith("- ") || line.startsWith("* ") ){
			
			if(active_ul_2){
				active_ul_2 = false;
				return `</ul><li>${line.slice(1)}`;
			}else if(active_ul){
				return `<li>${line.slice(1)}`;
			}else{
				active_ul = true;
				return `<ul><li>${line.slice(1)}`;
			}
		}

		if( line === "\r" ){
			let out = ``;
			//list 2
			if(active_ul_2){
				out += `</ul>`;
				active_ul_2 = false;
			}
			//list 
			if(active_ul){
				out += `</ul>`;
				active_ul = false;
			}
			//paragrap
			out +=  `</p> <p>`;
			return out; 
		}
		return line;
	}).join("\n");
	
	data = `</p>${data}<p>`;


//LINKS 
	data = data.replace( /\[(.*?)\]\((.*?)\)/g, (match, text, url)=> {
		const target = (url.startsWith(`http`) || url.startsWith(`www`)) ? `target="_blank"` : ``;
		return `<a href="${url}" ${target}>${text}</a>`;
	}); 



//TAGS 
	function between(fce, start, end = start){
		function exec(match, p1){
			//if( match.startsWith(escape) ) return p1; //TODO - REXEX?
			return fce(p1);
		}
		
		const rex = new RegExp(`${escape}${start}(.*?)${end}`, `gm`)
		data = data.replace(rex, exec); 
	}
	
	function behind(fce, start){
		function exec(match, p1){
			//if( match.startsWith(escape) ) return p1; //TODO - REXEX?
			return fce(p1);
		}
		
		// anything following closed in "()"
		const rex_long = new RegExp(`${escape}${start}\\((.*?)\\)`, `gm`)
		data = data.replace(rex_long, exec); 
		
		// only following single word (or number)
		const rex_short = new RegExp(`${escape}${start}(\\w*)`, `gm`)
		data = data.replace(rex_short, exec); 
	}

/*
	MARKDOWN-LITE
	`**something**` => `<b>something</b>`
	`*something*` => `<i>something</i>`
	`~something~` => `<strike>something</strike>`
	`{something}` => `<small>something</small>`
	``something`` => `<code>something</code>`

	`^something else` => `<sup>something</sup> else` (^ - caret - alt+94)
	`^(something else)` => `<sup>something else</sup>`
	`∧something else` => `<sup>something</sup> else` (∧ wedge)
	`∧(something else)` => `<sup>something else</sup>`
	
*/

	// \ in strings are disappearing so they have to be doubled
	between(a => `<strong>${a}</strong>`, `\\*{2}`); // **something** to <strong>something</strong>
	between(a => `<em>${a}</em>`, `\\*{1}`); //  *something* to <em>something</em>
	between(a => `<strike>(${a})</strike>`, `~`); //  ~something~ to <strike>something</strike>
	between(a => `<small>(${a})</small>`, `{`, `}`); // {something} to <small>(something)</small>
	between(a => `<code>${a}</code>`, "`"); //  `something` to <code>something</code>

	//wwbb
	between(a => `<small>(${a})</small>`, `†`); //  †something† to <small>(something)</small>
	data = data.replaceAll("†",""); //TODO TEMPFIX!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//data = data.replace(/(†)\s*$/gm, ``); // removes tag † at the end of the line notifying events in 
	
	
	
	behind(a => `<sup>${a}</sup>`, `[\\^\\∧]`);
	behind(a => `<sup>${a}</sup>`, `˄`); //TODO
	behind(a => `<sub>${a}</sub>`, `˅`);





//let a = "asdfg"


//	content = content.replace(/<!--(.*?)-->/gs, transform_comments)
//	content = content.replace(/`(.*?)`/gm, transform_code)
//	content = content.replace(/\[(.*?)\]\((.*?)\)/gm, transform_link)
//	content = content.replace(/\*\*(.*?)\*\*/gm, transform_strong)
//	content = content.replace(/\*(.*?)\*/gm, transform_em)
/*	
	const lines = content.split("\n");
	let active_list = false;
	let active_paragraph = false;

	function transform_comments(m, p1){
		return `xxxx`;
	}
	function transform_code(m, p1){
		return `<code>${p1}</code>`
	}
	function transform_link(m, p1, p2){
		const target = (p2.startsWith(`http`) || p2.startsWith(`www`)) ? `target="_blank"` : ``;
		return `<a href = "${p2}" class = "click" ${target}>${p1}</a>`
	}
	function transform_strong(m, p1){
		return `<strong>${p1}</strong>`
	}
	function transform_em(m, p1){
		return `<em>${p1}</em>`
	}
	
	for(let i = 0; i < lines.length; i++){
		function end_list(){
			if(active_list){
				active_list = false;
				lines[i] = `</ul>\n${lines[i]}`;
			}
		};
		function end_paragraph(){
			if(active_paragraph){
				active_paragraph = false;
				lines[i] = `</p>\n${lines[i]}`;
			}
		};
	
		//title
		if( lines[i].startsWith("#") ){
			lines[i] = lines[i].replace(/#/g,"");
			end_list();
			end_paragraph();
//TODO - H based on nubmer of #
			lines[i] = `<h3>${lines[i]}</h3>`;
//TODO - H3??
			
		}else if( lines[i].startsWith("-") || lines[i].startsWith("*") ){
			lines[i] = lines[i].replace(/-/,"");
			end_paragraph();
			if(!active_list){
				active_list = true;
				lines[i] = `<ul>\n<li>${lines[i]}</li>`;
			}else{
				lines[i] = `<li>${lines[i]}</li>`;
			}
	
		}else{
			end_list();
			if(!active_paragraph){
				active_paragraph = true;
				lines[i] = `<p>\n<p>${lines[i]}`;
			}else if(lines[i].trim() === ""){
				lines[i] = `</p>`;
			}else{
				//no change
			}
		}
	}
	
	// <a href=\"https://dahakma.maweb.eu/\" class=\"click\" target=\"_blank\" >Dahakma</a>
	
	
	//content = content.replace(/a/gm,"<p>$&</p>")
	
	content = lines.join("\n");
	
	//console.log(content);
*/
	return `export const text = \`${data}\` `; //`export const text = "asdfasdf"`;
};