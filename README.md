# FAKE AI

A game about a popular girl who is blackmailed by three nerds to play a video game and fulfill their lewd tasks.

![titleOfGame](documentation/wiki/Images/promo.png)

* [Walkthrough](./documentation/wiki/index.md)
* [Changelog](src/changelog.md)
* [Credits](src/credits.md)



<!--

## More

* [Changelog](src/changelog.md)
* [Credits](src/credits.md)


## Notes

* [F.A.Q.](documentation/notes/faq.md)
* [Content Table (Spoilers) (TODO)](documentation/notes/walkthrough.md)
* [Arcs (Spoilers) (TODO)](documentation/notes/arcs.md)
* [Virtual Game (Spoilers) (TODO)](documentation/notes/virtual.md)
* [School Classes (Spoilers) (TODO)](documentation/notes/classes.md)
* [List of Places in the Virtual Game](documentation/notes/terms.md)
* [Appearance Calculation](documentation/notes/appearance.md)

## Dev Notes

* [Comments about Appearance](documentation/notes/appearance_comments.md)
* [Combat Considerations](documentation/notes/combat_considerations.md)
* [List of Items](src/data/items/ids.js)

## Documentation 

* [Modding](documentation/modding/modding.md)
* [Save File](documentation/modding/savefile.md)
* [Documentation](documentation/documentation/index.md)




-->