
module.exports = {
	env: {
		browser: true,
		node: true,
		es6: true, //dont understand if redundant or not
	},
	parserOptions: {
		ecmaVersion: "latest",
		sourceType: "module",
		ecmaFeatures: {
			impliedStrict: true, //global strict mode
		}
	},
	extends: ["eslint:recommended"],
	globals: {
		mile: "writable",
		PC: "writable",
		ext: "writable",
		head: "writable",
		counter: "writable",
		SETTING: "writable",
		debug: "writable",
		hardbug: "writable",
		softbug: "writable",
	},
	rules: {
		//see https://eslint.org/docs/rules/

		//ADJUSTED RECOMENDED ERRORS 	
			//add comment "//break" to mark switch fallthrough as optional
			"no-fallthrough": ["error", { "commentPattern": "break"}], 
			
			//allows no brackets on single line "if(variable) do_domething()" 
			"curly": ["error", "multi-line"], 
		
		//RECOMENDED DOWNGRADED TO WARNINGS
			//tabs for indentation, spaces for alignment //not essential detail
			"no-mixed-spaces-and-tabs": ["error", "smart-tabs"], 
				
			//used in development all the time, error is needlessly annoying
//"no-unused-vars": "warn", 
"no-unused-vars": "off", 
				
			//used in development to check loops
			"no-constant-condition": "warn", 
				
			//not a big issue
			"no-extra-semi": "warn", 
			
			//empty (TODO) statements used in development
			"no-empty": "warn", 
		
		//ADDED WARNINGS
			//abused for debuggign, in production should be replaced by custom designs; kept ones should have exception:
			"no-alert": "warn", 
			/* eslint-disable no-alert */
			/* eslint-enable no-alert */
		
			//console logs should be removed before production; kept ones should have exception:	
//			"no-console": "warn", 
			/* eslint-disable no-console */
			/* eslint-enable no-console */
			
			//you should be use "const" when you can
			"prefer-const": "warn", 
			//"no-duplicate-imports": "warn",
			
			// ASAP in comments will trigger warning - TODO - not sure if this is a bright idea but it might be good to be constantly reminded things which need to be fixed ASAP
//			"no-warning-comments": ["warn", { "terms": ["asap"], "location": "anywhere" }]
		
		//ADDED ERRORS
			//don't use "var", "let" or "const" are superior
			"no-var": "error", 
			
			//no reason to do other way "(Object.assign({}, original, { c: 3 })" > "{ ...original, c: 3 }"
			"prefer-object-spread": "error", 
			
			//array methods has to have "return" ("array.map( function(){return something} )"; could be implicit, e.g. "return; "
			"array-callback-return": ["error", {"allowImplicit": true}], 
			
			//does not allow "${variable}" (only `${variable}`)
			"no-template-curly-in-string": "error", 
			
			//no reason "x === x"
			"no-self-compare": "error", 
			
			//prefer `a ${v}` over "a "+v
			"prefer-template": "error", 
			
			//evals are bad practise
			"no-eval": "error",
			
			//cannnot imagine creating not looping loop intentionaly
			"no-unreachable-loop": "error", 
				
			//should be explicitely set `window.foo`
			"no-implicit-globals": "error",

			//ES6 makes declaration of object properties and methods way cleaner
			"object-shorthand": "error", 
			
			//default cases in switches, generally not important but might cause troubles 
			"default-case": "error",
			
			// val ? val : 0  => val || 0
			"no-unneeded-ternary": "error", 
			
			
		//STYLE ERRORS
		"comma-dangle": ["error", "always-multiline"], //enforce trailing commas in multilines, my number one bug
		"func-call-spacing": ["error", "never"],
		
		//STYLE WARNINGS
		//"arrow-spacing": "warn",
		//"comma-spacing": "warn",
		//"indent": ["warn", "tab"],
		

		//"no-trailing-spaces": "error", //disallow trailing whitespace at the end of lines
		//"quotes": ["error", "backtick"], //template litearals superior to string in every way
		//"space-infix-ops": true, // a+b => a + b
		
		
/*
	CONSIDER
	"no-unmodified-loop-condition": "warn", //am using this in development to check loops
	complexity
	prefer-rest-params
	quotes
	
	//"arrow-body-style": ["error", "as-needed"], // ()=> {}
	//"prefer-destructuring": ["error", {"object": true, "array": true}], // const something = object.something => const {something} = object
			
	
	no-magic-numbers - would be a good practice 
	https://eslint.org/docs/rules/no-magic-numbers
	
	no-use-before-define //good practice but const and let throws error anyway and doesnt matter for function and var
*/

/*
	FIX
	no-extra-semi
	object-shorthand
	quotes
	space-infix-ops

*/	
		//NOTES:

		/*
			no-prototype-builtins
			foo.hasOwnProperty("bar") > Object.prototype.hasOwnProperty.call(foo, "bar")
		*/
		
		
		/* eslint-disable no-alert, no-console */
		/* eslint-enable no-alert, no-console */
		
		
		
	}
};