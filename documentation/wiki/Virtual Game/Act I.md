# Act I

## Intro

### Mandatory
- *Rat Extermination* (Tavern)
- find the right person

## Feyd Mora 

### Mandatory
- finish *Collect Roses* or *Find the Ancient Treasure* or *Trick the Smuggler*
- sail to *Gothred*

### Quests
- *Collect Roses* (Maid)
- *Find the Ancient Treasure* (Woman/Yelaya)
- *Trick the Smuggler* (Guard)

### Companions
- *Yelaya* 

[🔙  Index](../index.md)