# Act II

## Voyage to Gothred

### Mandatory
- *Explore Ship and Kill All Pirates*

### Companions 
- *Galawar*

## Gothred

### Mandatory
- visit Arazor
- finish *Hunt Feral Goblins* or *Ghost Ship*
- visit Arazor
- recruit *Stede* and/or *Lyssa* and/or *Galawar*
- visit Arazor
- sail from port


### Quests
- *Hunt Feral Goblins* (Tavern - Marshal)
- *Ghost Ship* (Port)
- *Clear Haversham Manor* (Marshal)
- *Explore the Tombs of Karged* (Arazor)
- *Explore the Labyrinth of Oskyl* (Arazor)

### Companions 
- *Stede* (Nancy's Tavern)
- *Lyssa* (House of Earthly Delights)
- *Galawar* (Nancy's Tavern)
- *Sirael* (Arazor's Tower)

### Other

- whoring (higher sluttiness + random discussion with Lyssa)
- Seven & Arm wrestling (Tavern)


## Dewyrain Sea

### Mandatory
- sail west and *Explore the Tombs of Karged*
- return to Arazor
- sail north and *Explore the Labyrinth of Oskyl*
- return to Arazor

### Islands
- **Karged**
	- quest *Explore the Tombs of Karged*
	- two other dungeons  *Explore Karged tombs*
- **Oskyl**
	- quest *Explore the Labyrinth of Oskyl*
- **Hengar**
	- dungeon *Abandoned Fortress* (smugglers)
- **H'dar Ğizra**
	- quest *Cut 8 rare trees on H'dar Ğizra and bring them to Datan Gulnas*
	- TODO
- **Datan Gulnas**
	- initiated quest *Cut 8 rare trees on H'dar Ğizra and bring them to Datan Gulnas*
	- TODO
- **Ynys Ydyg**
	- quest *Destroy all draugrs*
- **Shipwreck**
	- dungeon *Shipwreck*


[🔙  Index](../index.md)