# List of Places & People & Things

- **Aquilonia** - powerful ancient empire, thalassocracy, at the height of its power controlling *Beletriad*, *Souther Archipelago* and *Eastern Isles*
- **Amaranthine Forest** - endless unexplored forest located west of *Beletriad*
- **Arazor** - wizard living in exile in *Gothred*
- **Beletriad** - the main kingdom where the story takes place
- **caravel** - fast light ship with two or three masts, used as both a warship and a merchant ship
- **cog** - simple slow merchant ship with one mast
- **Datan Gulnas** - the westernmost island of *Eastern Isles*, the largest population centre is *geshud Duradudos*  
- **Dewyrain sea** - sea south and east of *Beletriad* 
- **Dewyrain** - coastal province located in southeastern *Beletriad*, capital *Sintre*
- **Dyfyrn Morna** - a small unremarkable region in central *Beletriad*
- **Eastern Isles** - islands southeast of *Beletriad* (e.g. *Datan Gulnas*)
- **Feyd Mora** - a small coastal city in *Beletriad* in *Dewyrain* province, halfway between *Gothred* and *Sintre*
- **galley** - warship propelled by rowing, with one or two masts
- **geshud Duradudos** - dwarven fortress and important trade station on *Datan Gulnas*
- **Gothred** - island in *Dewyrain sea*, city and city-state, technically independent but part of Beletriadian sphere of influence 
- **H'dar Ğizra** /*ˈx.dˤar* or *aˈxadˤar  d͡ʒizra*/ - large island in *Dewyrain sea*, culturally close to *Southern Archipelageo*
- **hellion** - savage barbarian from the *Northern Wasteland*
- **Hengar** - a small, rocky, uninhabited island in *Dewyrain sea*
- **Karged** - island in *Dewyrain sea*, a necropolis of *Aquilonian* kings
- **Nemetian plague** - a recent pandemic of unknown origin that significantly damaged the economy and contributed to the current crisis
- **Northern Wasteland** - the North, sparsely inhabited lands north of *Beletriad*, considered to be a part of Beletriad by some, conquered several times but maintaining the presence was never deemed to be worthy
- **Oskyl** - unremarkable cliffy islet in *Dewyrain sea*
- **quinquereme** - the heaviest *galleys*, with a hundred of oars, two masts and able to carry several catapults or ballistas and up to a hundred marines
- **Rawik** - wise and powerful king of Beletriad, died without a clear successor shortly before the events of the game
- **Sintre** - coastal city and important port in *Beletriad*, capital of *Dewyrain* province
- **Southern Archipelago** - islands south from *Beletriad* (e.g. *H'dar Ġizra*)
- **Taur Luicarnë** - see *Amaranthine Forest*
- **Tey** - border river between civilized *Westmark* and wild *Amaranthine Forest*
- **Westgard** - border fortress and important trade hub, capital of *Westmark*
- **Westmark** - westernmost province in *Beletriad*, capital *Westgard*
- **Ynis Ydyg** - small island in *Dewyrain sea*

<!--
## TODO 
- Torrywialen
-->

[🔙  Index](../index.md)
