# Eva Šebestová

- the best or the second best friend of Kate; the second best friend of [Saša](./Saša.md)
- the second most popular girl in the class after Kate, she would not mind being the first
- secretly insecure and jealous of Kate being better at everything than her
- she is a bit of bitch who enjoys messing with people and causing mischief
- she dates [Sam](./Minor%20Characters/Sam.md)
- she likes horror movies and interracial porn

### Chairman
- she can get involved in class [elections](../Arcs%20(not%20character-related)/Chairman.md)

### Vengetful Eva
- if Kate mistreat her too much, she seeks revenge (`mile.eva_exploit`)


[🔙  Index](../index.md)
