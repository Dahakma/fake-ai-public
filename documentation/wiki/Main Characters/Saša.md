# Alexandra "Saša" Kerekesová

- the best or the second best friend of Kate, the second best friend of [Eva](./Eva.md)
- one of cool girls; she is good-looking and hangs out with cool [girls](./Minor%20Characters/Girls.md) even though she is a bit insecure
- she is rather timid and usually follows Kate's leaderships
- she likes animals, especially cats and frogs

### Confident Saša
- boosting her confidence makes her more self-assured and dominant
- `mile.sas_sassy`
### Annoyed Saša
- exploiting her makes her more side with Eva rather than Kate
- `mile.sas_exploit`

### Friends with Benefits  
- Saša is attracted by Kate
- triggered by [Saša's nudes](../Tasks.md#Video%20Saša) (task)  or by Tricked by girls (optional)

<!--
### Dominant x Submissive Lover
- Saša could become the more dominant person if her confidence or Kate's submissiveness are high 
- `mile.sas_dom === true` 
-->

### Lovers
- the relationship can get more serious
- `mile.sas_love` - lovers 


### Evil Girlfriend
- mistreated Saša + dominant in the relationship with Kate + dating Juraj + telling her about blackmail 

[🔙  Index](../index.md)