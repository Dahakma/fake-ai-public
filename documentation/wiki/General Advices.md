# General Advices

- the game is still unfinished beta; there are many bugs and things not working as intended 
- try to enjoy the game without looking at spoilers
- *Settings > Settings > Skip the virtual game parts* will skips the parts of the game taking place in the fantasy virtual game (instead of playing they show a brief summary what happened)
- playing the virtual fantasy game is enforced when Kate is too behind with it; all other options except for *Procrastinate* are hidden
- do not procrastinate
- do not *Procrastinate* in the game either
- *Settings > Cheatmode > Soft* will show unavailable options requiring different stats or previous choices
- *⚠️ Force next event ⚠️* button could be used if the active displayed even is broken, there are no links allowing you to continue or no text at all; it skips the rest of the even and advances to the next scheduled one. But use only in case of emergencies, it potentialy can break the game
- if the game behaves weirdly, sometimes helps to save the game and then refresh the page
- the game gets autosaved every day and when you enter or finish a dungeon in the virtual game. There are three slots for autosaves and the last one gets rewritten every time when a new one gets made

[🔙  Index](./index.md)