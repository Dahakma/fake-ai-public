# Main Stats

## Submissiveness

- Kate could be submissive or dominant
- acting confidently and dominantly unlocks paths with more dominant and confident Kate and vice-versa
- `mile.sub` (negative dominant) (global variable accessible via the browser's console)

## Sluttiness

- Kate could be a slut or a prude
- it is left ambiguous how slutty Kate was before the start of the game and readjusted retroactively 
- higher sluttiness unlocks more sexually-shameless choices, reactions, events and tasks; also sluttier clothes in shops
- prude Kate is more embarrassed and uncomfortable, there are some low-slut events or tasks which would not bother slutty Kate
- (not directly related to [outfit sluttiness](./Notes/Sluttiness.md))
- `mile.slut` (negative prude)

## Status

- how much is Kate popular and respected among her peers
- Kate loses popularity by being publicly humiliated and doing things that might make her seem weak, insecure or asocial 
- at the moment badly implemented and heavily wip 
- `mile.stat` (higher better)

## Gamer

- Kate might find out she actually enjoys the game and nerdy stuff 
- Kate hating the game is less attached, e.g. sees the characters in the game only as NPCs 
- Kate liking the game is more immersed and interested in the lore
- `mile.immersion` (negative hates)

## School

- Kate could have good or bad grades (1 is the best grade; 5 is the worst)
- `mile.grades` (lower is better)
- random events at school (e.g. biology examination)
- optional events (e.g. study chemistry with Juraj)
- consequence of tasks (e.g. vibrating panties)

[🔙  Index](./index.md)