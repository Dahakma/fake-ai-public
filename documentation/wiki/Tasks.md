# Tasks

## Week 1

### Short Skirt
- Kate is allowed to wear only short skirts 

### Show Legs 
- Kate is allowed to wear only short skirts or shorts
- (short skirt alternative)

### Sexy Selfie
- Nerds want Kate to take a sexy selfie for them 
- Kate decides whether tame or topless 

### Porn Review
- Kate is asked to review a porn

### Cucumber 
- Kate is sent to a shop with a suggestive shopping list
- only when sluttiness is low, slutty Kate would not be embarrassed

### Makeup
- Kate is asked to wear heavy makeup to school
- necessary to trigger [Chairman](./Arcs%20(not%20character-related)/Chairman.md) arc

### Flash Panties
- Nerds want to get a glimpse of Kate's panties

### Sexy Top
- Kate is only allowed to wear sexy tops showing more skin, having deeper cleavages or showing bare midriff
- (after Short Skirt or Show Legs) 

### No Bra
- Kate is not allowed to wear bra
- (disabled by  [No Panties](./Tasks.md#No%Panties))


## Week 2

### Buy Dildo 
- Kate is asked to buy a new dildo in a sex shop 
- during second week task for slutty Kate; following weeks task for prude Kate

### Sexy Legwear
- Kate is asked to wear cute high socks, stockings or sexy pantyhose
- (after [Short Skirt](Tasks.md#Short%20Skirt) or [Show Legs](Tasks.md#Show%20Legs)) 

### Sexy Panties
+ Kate is asked to wear sexier underwear 
+ when she wore unsexy underwear during [Flash Panties](./Tasks.md#Flash%20Panties)

### Bikini 
- Kate is asked to wear bikini to swimming lessons 
- (after random event initiating swimming lessons)

### Tan
- Nerds sent Kate to get tanned 
- (neither Eva or Saša hate Kate)

### Flash Breasts
- Kate has to flash her breasts during classes 
- during second week available only to slutty Kate

### Topless Selfie
- Nerds want Kate's topless selfie
- disabled when she already sent one during [Sexy Selfie](./Tasks.md#Sexy%20Selfie)

### Hair Dye
- Kate has to dye her hair
- alternative to [Matej's](./Main%20Characters/Matej.md#Personal%20Favours) personal favour

### Choker 
- Kate is asked to wear a choker 
- alternative to [Zdeno's](./Main%20Characters/Zdeno.md#Personal%20Favours) personal favour
- disabled when Kate was asked to wear a collar ([Collar](Tasks.md#Collar) or other events)

## Week 3

### Piercings 1
- Kate is asked to get any piercing she wants 
- up to 3 new piercings 
- nipple piercing not available to prudes; VCH piercings enabled only for sluts 

### Open Legs
- Nerds want Kate to have her legs spread open while sitting down 

### Pubic Hair 
- Kate is asked to shave her pubic hair 
- only when the avatar has pubic hair (could be edited during character creation) and nerds know Kate has pubic hair

### Vibrating Panties
- Kate has to wear to school vibrating panties 
- starts the [Molesting](./Minor%20Characters/Mr%20Molnár.md#Molesting) arc

### Poledancing 
- Kate gets free poledancing classes 
- starts [Stripping](./Arcs%20(not%20character-related)/Stripping.md) arc

### Tattoo 1
- Kate is asked to get a tattoo

### Nude Picture
- Kate has to take a nude (or topless if her sluttiness is low) picture in the school library 

### Video Dildo 
- TODO 


## Week 4

### Buttplug
- Nerds want Kate to wear an anal plug to school 
- unlocks all anal content 

### Tattoo 2
- if nerds are mean, Kate has to get a distinctive sexy tattoo
- if they are nice she has to get any new tattoo
- (after [Tattoo 1](Tasks.md#Tattoo%201))

### Piercings 2
- if nerds are mean and Kate did not get nipple piercing previously; Kate has to get nipple piercings
- otherwise she can get any piercings she wants
- up to 3 new piercings 
- genital piercings enabled only for sluts 
- (after [Piercings 1](Tasks.md#Piercings%201))

### Video Saša
- if [Saša](./Main%20Characters/Saša.md) got her breast enlarged
- if Kate is huge slut or nerds hate her, they ask for a naughty video with topless Kate and Saša
- otherwise they ask for pictures
- disabled by Tricked by Girls event (triggers alternative start for Saša's path)

### No Panties 
- Kate is not allowed to wear any underwear

### Mandatory Masturbation
- Kate is asked to masturbate at school every day
- if prude to overcome her sexual inhibition and frustration
- if slut to handle her nymphomania and lust

## Week 5

### Collar
- Kate has to wear a collar

## Weekend Tasks

### Fight
- Kate has to wrestle and defeat the AI 

### Game Night

- Kate has to work as a maid during nerd's game night

### Kissing Girls
- Kate hangs out with classmates in bar *Requiem*, her task is to make out with three girls

[🔙  Index](./index.md)