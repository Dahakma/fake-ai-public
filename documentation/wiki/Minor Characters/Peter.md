# Peter
- pretty cool guy & mathematical genius
- everybody likes him
- attended the same school as Kate
- he used to be the head of the student council while Kate, representing her class, was the youngest member of the council
- he went to a programming club with [Juraj](../Main%20Characters/Juraj.md), [Matej](../Main%20Characters/Matej.md) and [Zdeno](../Main%20Characters/Zdeno.md)
- studies software engineering; at the moment participates in internation research project on Iceland

[🔙  Index](../index.md)