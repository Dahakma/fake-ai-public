# Teachers

- [Ms Bosáková](../Minor%20Characters/Ms%20Bosáková.md) - Physic Education 
- [Mr Ďuro](../Minor%20Characters/Mr%20Ďuro.md) - History
- [Mr Franz](../Minor%20Characters/Mr%20Franz.md) - Deputy Principal
- [Mr Liška](../Minor%20Characters/Mr%20Liška.md) - Biology
- [Mrs Majerová](../Minor%20Characters/Mrs%20Majerová.md) - Literature & Slovak Language & Form teacher
- [Mr Molnár](../Minor%20Characters/Mr%20Molnár.md) - German Language

[🔙  Index](../index.md)

