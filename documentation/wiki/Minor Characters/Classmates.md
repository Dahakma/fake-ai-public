# Classmates 
Kate goes in the same class as
- [Eva](../Main%20Characters/Eva.md)
- [Oliver](Others.md#Oliver)
- [Igor](Others.md#Igor)
- [Juraj](../Main%20Characters/Juraj.md)
- [Matej](../Main%20Characters/Matej.md)
- [Mike](Mike.md)
- [Saša](../Main%20Characters/Saša.md)
- [Tom](Tom.md)
- [Tony](Others.md#Tony)
- [Vendy](Vendy.md)
- [Vojta](Vojta.md)
- [Zdeno](../Main%20Characters/Zdeno.md)
- [Žanet](Žanet.md)

[🔙  Index](../index.md)