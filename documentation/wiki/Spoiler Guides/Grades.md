# Grades
- `mile.grades` - 0 to 10; lower better; 

|`mile.grades`|grade|desc|equivalent
|:---:|:---:|:---:|:---:|
|<=0|1*|flawlessly excellent|A+|
|1|1|excellent|A|
|2|1-|almost excellent|A-|
|3|2|very good|B+|
|4|2|very goodt|B|
|5|2-|almost very good|B-|
|6|3|good|C|
|7|3-|barely good|C-|
|8|4|almost sufficient|D|
|9|4-|barely sufficient|D-|
|>=10|5|insufficient|F|



### Original
- background:
	- *party girl*  `4`
	- *athlete*  `3`
	- *chairman*  `2`
- solving the hacking minigame (assumes Kate is smart or resourcefull before the start of the game)
	- first try --1
	- giving up ++1

### Changes
- German examination ([Vibrating Panties](Tasks.md#Vibrating%20Panties))
	- ++2 failing examination (always)
	- --3 resolutely refuse molestation
	- --4 threaten you report Mr Molnár
	- --2 allowing molestation
- history test ([Flash Breasts](Tasks.md#Flash%20Breasts))
	- --2 all answers correct
	- --1 mostly correct
	- ++1 mostly incorrect
	- ++2 all answers incorrect
- biology exam
	- --2 studied biology & answered correctly
	- --1 aswered correctly
	- ++1 studied & answered incorrectly
	- ++2 answered incorrectly
- study chemistry ([Juraj's girlfriend](Juraj.md#Girlfriend))
	- --2 many correct answers
	- --1 correct answers
	- ++1 incorrect answers
	- ++2 many incorrect answers

### Checks
- `mile.grades <= 1` - increased pocket money (week 3)
- `mile.grades >= 1` - decreased pocket money (week 3)

[🔙  Index](../index.md)
