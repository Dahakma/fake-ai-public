# Calculation of Outfit Sluttiness 

### Neck
- +1 choker 
- +2 collar 


### Cleavage
- does not apply for see-throuh clothes
- +1 nice cleavage 
- +2 revealing cleavage 
- +3 deep cleavage 
- +4 absolute cleavage 
<!-- -->
- +2 tube top
- +3 revealing tube top
<!-- -->
- +3 only corset 
<!-- -->
- +2 feeble straps (wMiniDress)
- +3 strapless dress 
- +2 strapless dress  (wMiniDress3)
- +3 revealing strapless dress 

![cleavageCoverage](../Images/cleavage.jpg)

### Chest
- +8 topless (includes sleeves so technically 6 + 2)
- +8 fishnet top with nothing under
- +7 see-through top with nothing under
- +6 only bra (includes sleeves so technically 4 + 2)
- +4 translucent top with nothing under
- +5 fishnet top with bra
- +4 see-through top with bra
- +2 translucent top with bra
<!-- -->
- +5 just jacket with nothing under
- +4 jacket and bra
<!-- -->
- +2 obviously no bra, nipples visible
- +1 visible pierced nipples
- +1 outerwear form-fitting
- +3 only corset

### Sleeves
- +2 bare arms (both short sleeves and bare shoulders)
- +1 short sleeves
- +1 bare shoulders
- +1 transparent sleeves (if not short nor shoulders)

### Lower
- +4 hotpants
- +3 shorts
- +2 long shorts
- +1 capri
<!-- -->
- +5 not hiding really anything
- +4 micro
- +3 mini
- +2 short
- +1 midi 
 <!-- -->
- +2 very tight 
- +1 tight
- +1 short and loose

![cleavageCoverage](../Images/lowerLegCoverage.png)

### Panties
- +10 genitals fully exposed
- +9 crotchless panties exposed
- +8 thong exposed
- +7 panties exposed
<!-- -->
- +8 see-through and nothing
- +7 see-through and crotchless panties
- +6 see-through and thong
- +5 see-through and panties
<!-- -->
- +6 glimpse of panties
- +5 glimpse of crotchless panties
- +4 glimpse of thong
- +3 glimpse of panties
<!-- -->
- +5 translucent and nothing
- +4 translucent and crotchless panties
- +3 translucent and thong
- +2 translucent and panties

### Belly 
- +2 navel window
- +3 bare midriff (almost nothing between breasts and pants)
- +2 exposed midriff 
- +1 glimpse of midriff

### Shoes 
- +2 very high heels
- +1 high heels
- +2 high boots
- +1 boots

### Socks
- +1 high socks
- +3 fishnet stockings
- +2 stockings
- +4 fishnet sexy pantyhose
- +3 sexy pantyhose
- +1 pantyhose
- +2 fishnet legwear
- +1 legwear
- +1 zettai ryouiki
- +1 barefoot (no shoes)

### Nudity
- +20 nude
- +8 topless
- +14 bottomless

### Body 
- +2 huge breasts
- +1 big breasts

### Tattoo
- +4 slutty tattoos
- +3 slutty tattoo
- +2 hints of slutty tattoos
- +1 hint of slutty tattoo

### Piercings 
- +1 pierced nipples

## Sluttiness

- <= 3 *prude*
- <= 9 *beauty*
- <= 15 *tease*
- <= 22 *slut*
- <= 30 *whore*
- *cumdump*



## Allowed to Wear
- based on stat [sluttiness](../Main%20Stats.md#Sluttiness) (same levels as outfit sluttiness but possibly with slightly different tresholds)
- X - not allowed 
- E - embarrassing
- S - suggested

<table>
<head>
<td></td>
<td>0 prude</td>
<td>1 beauty</td>
<td>2 tease</td>
<td>3 slut</td>
<td>4 whore</td>
<td>5 cumdump</td>
</head>
<tr>
<td>No bra</td>
<td>X</td>
<td>E</td>
<td></td>
<td>S</td>
<td>S</td>
<td>S</td>
</tr>
<tr>
<td>Breasts exposed*</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>E</td>
<td></td>
<td></td>
</tr>
<tr>
<td>No panties</td>
<td>X</td>
<td>X</td>
<td>E</td>
<td></td>
<td></td>
<td>S</td>
</tr>
<tr>
<td>Panties exposed**</td>
<td>X</td>
<td>X</td>
<td>E</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>Pussy exposed***</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>E</td>
<td></td>
</tr>
</table>


<br>\* breasts clearly visible through see-through or fishnet top
<br>\*\* panties visible because of too short skirt 
<br>\*\*\* pussy fully exposed (too short skirt) or almost exposed (see-through or fishnet skirt)


[🔙  Index](../index.md)













