# Technology

## Androids
Robots designed to look like humans. *Cybrex* is developing new Androids indistinguishable from humans.

## Ares 
Ares is the international program of crewed Mars mission involving NASA, ESA, JAXA and ISRO. The first mission of six astronauts is currently en route to Mars. 

## Bioimplant
Bioimplants are one of the most common types of cyberware. [Kate](../Main%20Characters/Kate.md) has one. Their main functions are 
- tracking various biological stats like blood pressure or temperature, analyze them and detect possible health issues
- eCall function allowing automaticaly alert emergency services 
- gradual distribution of prescription medication or hormonal contraception

## Regen 
Regenerative therapy allowing speeding up healing of injuries. 

## Metaverse 
The Internet accessible via virtual reality. The concept is based on  Neal Stephenson's novel *Snow Crash*. (You should read it. And *Diamond Age* too.)

## True AI 
Artificial Intelligence indistinguishable from the human one, sentient and able to cretively solve complex problems. There are five ongoing research claiming they created a true AI, three in the USA, one in China and one in Russia. There are two other projects allegedly close to achive the creation of true AI in Japan and on Iceland. 


[🔙  Index](../index.md)