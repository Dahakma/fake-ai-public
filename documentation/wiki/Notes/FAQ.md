# F.A.Q. 

- summary of most interesting answers on various forums and social networks

## 0.9.30

- if you run into a **dead end**, with no links to continue, use **"Force next event"** button (in the side menu)
- **colors** are randomly generated and then searched for the closest named color - sometimes there is no close color and the name of the color does not exactly match the actually displayed color
- there will be no after **battle defeat scenes** - rewarding losing is a dumb game design
- **prostitution** in Gothred is just a bit of extra content for a high-slut character; I had a generic prostitution module I made for *I was Enslaved by an Evil Witch and Turned into her Maid* so I just plugged it in; therefore it is rather underdeveloped and not especially good
- piercings are not rendered when they are hidden under underwear, which is pretty smart; Nobody asked about this but it seems like a good opportunity to mention it
- there are a few fake choices where both answers intentionally trigger the same scene (but also linking to a wrong scene is one of the more common bugs I make)
- **innocent submissive** playstyle is supposed to be an option but does not work perfectly; mostly I am often not sure whether the choice is rather submissive or slutty
- there is some **feminization** content planned but none is in the game yet
- I am trying to leave ambiguous how much Kate feels to be **forced** to follow nerds' rules **or** whether she **enjoys** behaving slutty
- the **order of events** is pretty loose which sometimes means that the event that is generally supposed to take place during the first or second week takes place during the third or fourth one when the relationships with the other characters might be different and it might cause them seemingly behaving inconsistently
- calculation of **sluttiness** is wacky, it is hard to compare whether is sexier absolute cleavage or translucent top or miniskirt or tight hotpants and I allowed players too much freedom
- I am aware the **premise** of the game does not make sense; I am trying to not draw too much attention to it
- the way the game is currently designed makes it impossible for Kate to **refuse a task** and replace it with another one (usually the next task for tomorrow is already selected)
- **combat and skills** will be slightly reworked
- I am aware the **inventory** system is awful and clumsy but I have no idea what to do with it
- I very much enjoyed the non-generic Russian setting in *Girl Life* so I tried to do something similar and set the story in an exotic country. **Slovakia** is an exotic Eastern European country I am familiar with
- there is no **money** in the real-life part of the game; I did not want to add unnecessary grind and focus on making money. However I might change it in the future, it would solve the problem with the player being able to acquire unlimited amount of clothes and would motivate the main character to do lewd things for money

[🔙  Index](../index.md)