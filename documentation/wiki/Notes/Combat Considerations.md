# Combat Considerations

## Follower

- makes the game in the game more interesting by providing more NPC interactions
- enables more tactical decisions in the combat
- adds more strategical decisions which of available companions has a better synergy with the PC
- different followers add more variety
- there could be quest tasks with weak or overpowered followers

## Evasion and Reflex

- explain why are skimpy armors as good or better than bulky ones
- more tactical choices: fast with low defense x slow with high defense

## Evasion when attacked the first time

- provides the opportunity for cooperation, e.g. fast companion with a light attack removes the evasion and distracts the enemy and then the PC hits very hard
- rewards correctly used *presence* (when completely surrounded character with low presence will probably get attacked only once)

## Initiative based on Reflex

- more benefits for the fast characters
- on the other hand, it might be beneficial to attack last after the enemy cannot evade anymore
- more varied enemies; very fast x very slow
- the small randomness in the calculation of initiative should add more uncertainty and thrill while remaining mostly predictable, has to be calibrated well
- very fast characters will be able to attack twice per round which would be totally badass

## ~~Chance to miss~~

- more attacks would be necessary to kill the enemy, making the combat longer
- it would not make it more interesting, could cause frustration

## ~~Regeneration~~

- encourage boring waiting and evading combat until the attribute recharges

## Critical Lucky Hits going through Evasion and Armor

- some high-level enemies might be hard to kill
- high-level PC with high *evasion* or/and *armor* will be practically invulnerable, this will keep the combat risky
- different weapons might have different critical hit chances, providing more variety
- there could be actions, perks, effects adjusting the critical hits probability
- affecting both *evasion* and *armor* makes things simpler and easier to balance

## Armor 

### ~~Armor bonus added to hp~~
- simple and clear
- not mechanically interesting
- hp gets ridiculously high

### ~~Armor as a separate bar~~
- more mechanical possibilities
- could not be fixed with potions, would require repair skill
- armor repair skill would encourage adding crafting which I do not want to add
- armors could be destroyed which would expose PC
- but it would make the player lose an irreplaceable piece they might like

### ~~Armor as number subtracted from the attack~~
- seems the most realistic
- weak enemies would not be able to penetrate the armor, making fighting them tedious and pointless

### Armor as percent reduced from the attack
- weak enemies can still hurt you
- *armor* and *evasion* are equal(!):
	- `10 attack dealing 10 damage = 100 damage`
	- `50% evaded = 50 damage`
	- `50% armor damage reduction = 50 damage`


## Potions could be used on an ally
- less realistic
- enables more tactic or strategic choices (one character to focus on damage dealing and the other on support

## Potions are more powerful
- potions are no longer a free action, which of allies use the potion is a tactical decision
- they have to be powerful enough to be worth combat action
- drinking dozens of potions was tedious and gamy and they took too much space in the inventory 
- tactical decision whether to wait and risk to use the full potential effect of the potion


## Immobilisation (Stun or Entangled) results in automatic Critical Hit
- *stun* or *entanglement* became more powerful
- provides an opportunity for cooperation, one ally immobilizing the enemy and the other dealing the damage

## Weapon types
- more weapons provide more motivation to loot and more tactical decisions

<!-- TODO
- blades - higher lucky hit chance; expensive
- polearms - longer reach, cheap
- maces - stun chance
- whips - entangle chance
- ranged
[🔙 README](../README.md)
-->


[🔙  Index](../index.md)