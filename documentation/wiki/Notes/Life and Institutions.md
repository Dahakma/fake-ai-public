# Life and Institutions

## School 
Kate attends the fourth year of 4-year *gymnazium*, a general secondary school with main goal to prepare students for university (if I calculate it correctly, she is supposed to be a year older than American high-school senior because her primary education took 9 years instead of 8).

### Grades
|Grade|Description|US Grade|
|:---:|---|:---:|
|1|Excellent|A|
|2|Very Good|B|
|3|Good|C|
|4|Sufficient|D|
|5|Insufficient|F|

## Songs 
### Slow Down 
<iframe width="560" height="315" src="https://www.youtube.com/embed/fI0iRdZnaXk?si=k_6aymbedMTMBwiS" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### At Altar
<iframe width="560" height="315" src="https://www.youtube.com/embed/3whSlQDYhdQ?si=1F2zdqV9iuMYQ9kB" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Rose Garden
<iframe width="560" height="315" src="https://www.youtube.com/embed/qXZrIs95xQk?si=7UGpqboCE72S2g85" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<!--
<iframe width="560" height="315" src="https://www.youtube.com/embed/IPDXybx2eTE?si=VGLASxDD7B25ubZ5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
-->


[🔙  Index](../index.md)

