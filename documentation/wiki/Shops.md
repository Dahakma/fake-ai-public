# Shops

- sluttier clothes offered if [sluttiness](./Main%20Stats.md#Sluttiness) is higher; or if unlocked by tasks; or in later weeks

## 👗 Boutique 
- expensive fancy clothes

## 👕 F&A Fashion
- affordable average clothes

## 🎽Discount store
- cheap simple clothes

## 👖 Second-hand
- various pre-owned clothes
- **unwanted clothes could be sold there**

## 👒 Fashion Accessories
- belts, jewellery, etc.

## 👠Shoes
- shoes

## 💎 Jewellery & Bijoux
- jewellery

## 👙 Naughty Lingerie
 - underwear
 - unlocked by having higher [Sluttiness](./Main%20Stats.md#Sluttiness) or by tasks [Sexy Panties](./Tasks.md#Sexy%20Panties) or [Sexy Legwear](./Tasks.md#Sexy%20Legwear)

## 👓 Optician
- glasses
- unlocked by wearing glasses ([Juraj's](./Main%20Characters/Juraj.md#Personal%20Favours) persona favour) 

## 💍 Goldsmith's Shop
 - expensive jewellery
 - unlocked by having more money than is reasonable

## 💀Alternative Fashion
- goth/emo/punk stuff
- unlocked by shopping with [dominant](./Main%20Characters/Zdeno.md#Slave) or [submissive](./Main%20Characters/Zdeno.md#Domina) Zdeno (week 4)

## 🎀 Bimbo Fashion
- bimbo pink clothes
- unlocked by shopping with [Matej](./Main%20Characters/Matej.md#Bimbofication) (week 4)

## ♦️ Piercing Parlor
- piercing
- unlocked in week 5 after [Piercings 1](./Tasks.md#Piercings%201) and [Piercings 2](./Tasks.md#Piercings%202) tasks

## 💘 Tattoo Studio
- tattoos
- unlocked in week 5 after [Tattoo 1](./Tasks.md#Tattoo%201) and [Tattoo 2](./Tasks.md#Tattoo%202) tasks

## ✂️ Hair Salon
- haircut/hair extension
- unlocked after day 15 or if Matej mentioned hair extensions

## 🍆 Dildopolis
- sex toys

### Dildos
- after [Buy Dildo](./Tasks.md#Buy%20Dildo) or [Dildo Video](./Tasks.md#Dildo%20Video) tasks
- bigger dildos are unlocked by higher [sluttiness](./Main%20Stats.md#Sluttiness)

### Buttplugs
- after [Buttplug](./Tasks.md#Buttplug) task and wearing the buttplug for several days

### Chastity Cages
- if [Zdeno's domina](./Main%20Characters/Zdeno.md#Domina) or very [dominant Juraj's girlfriend](./Main%20Characters/Juraj.md#Dominant%20X%20Submissive%20Girlfriend)

### Strapons
- after lesbian shenanigans with AI or [Saša](./Main%20Characters/Saša.md#Friends%20with%20Benefits)
- or if [Zdeno's domina](./Main%20Characters/Zdeno.md#Domina) or very [dominant Juraj's girlfriend](./Main%20Characters/Juraj.md#Dominant%20X%20Submissive%20Girlfriend)

<!-- -->


[🔙  Index](./index.md)