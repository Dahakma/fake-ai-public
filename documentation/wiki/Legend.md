# Legend 
- explanation of terms I am using in this wiki and might not be clear

### Kate
- [Kate](./Main%20Characters/Kate.md) is the default name of the main character

### event
 - thing that happens
 - it could be *[tasks](Legend.md#task)*, *[random](Legend.md#random)* or *[optional](Legend.md#optional)* events
 - could consist of one or multiple chapters, each chapter is assigned to a timeslot (e.g. the first part of the event can take place in the morning with consequences in the evening)
 - events  are randomly selected based on character stats, previous choices of the player or time 
 - their order is very loose; different events can happen during each gameplay 
 - mentioned week (e.g. *Piercings 1 task (week 3)*) is the first week when is the even available, almost always they remain available for multiple weeks or till the end of the game (e.g. when the task *Piercing 1* does not happen during 3rd week, it could happen during the 4th or 5th)

### task
- tasks given to Kate by [Nerds](./Minor%20Characters/Nerds.md) every workday
- task could be given in the morning or in the evening of the previous day; or during the day 

### random 
- random events
- usually taking place at school 

### optional
- events selected by the player
- one optional events could be selected each workday
- mutiple optional events could be selected each weekend


### sluttiness
- main stat tracking how promiscuous and [sexually shameless](Main%20Stats.md#Sluttiness) is Kate is

### submisiveness
- main stat tracking how [dominant or submissive](Main%20Stats.md#Submissiveness) is Kate

### status
- main stat tracking how [popular](Main%20Stats.md#Status) is Kate

### virtual game
- virtual fantasy game in the game


[🔙  Index](./index.md)