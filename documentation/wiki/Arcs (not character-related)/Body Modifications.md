# Body Modifications 

## Hair 
- [Hair Dye](../Tasks.md#Hair%20Dye) task (week 2)
- Pink Hair - Matej's personal favour (week 2) 
- Blond Hair - random event (week 4 + interest in bimbo stuff + not already blonde) 
- Haircut/Hair extension - [Hair Salon](../Shops.md#✂️%20Hair%20Salon) -  (week 3)

## Piercings
-  [Piercings 1](../Tasks.md#Piercings%201) task (week 3)
-  [Piercings 2](../Tasks.md#Piercings%202) task (week 4)
-  [Piercing Parlor](../Shops.md#♦️%20Piercing%20Parlor) shop (week 5)

## Tattoos
- [Tattoo 1](../Tasks.md#Tattoo%201) task (week 3)
- getting tattoo together with [Saša](../Saša.md) (week 4)
- [Tattoo 2](../Tasks.md#Tattoo%202) tasks (week 4)
- [Tattoo Studio](../Shops.md#💘%20Tattoo%20Studio) (week 5)


## Cosmetic Surgeries
- Breast Enlargement (week 4) - after discussing it with Matej on [Bimbo](../Main%20Characters/Matej.md#Bimbofication) route (TODO alternative trigger)
<!-- 
Sa's boobjob
-->

## Tan
- [Tan](../Tasks.md#Tan) task (week 2)

## Pubic Hair
- defaulty assumed Kate has trimmed pussy; can be changed in the initial character editing
- [Pubic Hair](../Tasks.md#Pubic%20Hair) task (week 3)
