# Chairman 
- Kate badly wants to be reelected to the position of the class chairwoman and the class representative on the student council
- her chances are lowered by losing popularity and displeasing the form teacher; increased by convincing people to support her
- storyline started by discussing Kate's [makeup](../Tasks.md#makeup) (task) with [Vendy](../Minor%20Characters/Vendy.md) (random event)