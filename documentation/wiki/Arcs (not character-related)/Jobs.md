# Jobs

## Odd Jobs
### Plant Manager 
- watering plants 
- week 1
- 40 €

### Working at Bar
- helping [Eva](../Main%20Characters/Eva.md) at Karaoke Bar
- week 2
- ≈ 140 - 220 €

## Agency

### Promo at Mall
- distributing samples at a Mall
- week 2
- 70 - 100 €

### Trade Show
- booth babe at a tech trade show
- week 2
- 140 - 160 €

### Farmers' Market
- doing a promo show at Farmers' Market
- week 4
- 200 €


## Stripping
- see [Stripping](Stripping.md)

### Striptease
- filling in for [../Minor%20Characters/Světa](Světa.md) at bachelors birthday party
- week 4
- 200 €

### Stripshow
- stripshow with [../Minor%20Characters/Světa](Světa.md)
- week 4/5
- 380 - 450 €

## Whoring 
- see [Whoring](Whoring.md)

### Mistaken for Whore
- week 3
- 60 € - 100 €

### Fucking Juraj's cousin
- week 4
- 50 € - 120 €

### Blowing Classmates
- week 5
- 120 € - 220 €

## Pocket Money
- Kate weekly receives pocket money from her parents
- could be increased/decreased if her grades are good/bad (week 3)
- 60 - 160 €

***

\* exact values of earned money might not be up to date with the latest version of the game