# Cheats 
Most of game variables are accessible via browser's console. 

### Console
- **Vivaldi**: Ctrl + Shift + j
- **Chrome**: Ctrl + Shift + j
- **Firefox**: Ctrl + Shift + j
- **Edge**: Ctrl + Shift + i

### Usage
- open the console
- write something like
```js
ext.irl_money = 1000;
```
- enter
- advance in the game to reload the screen 


## Milestones
- `mile.`
- tracks the progress through the game and other stuff, including [Main Stats](Main%20Stats.md)
- `mile.sub` - submisiveness (negative dominant)
- `mile.slut` - sluttiness
- `mile.stat` - status

## Extra Stats
- `ext.`
- extra stats including avatar dimensions, saved outfits, statistics, etc. 
- `ext.irl_money` - money in the real life
- `ext.game_money` - money in the virtual game
- `ext.rules` - list of clothes rules Kate currently has to follow
- `ext.irl`  - dimensions of the avatar in the real life different from the default ones
- `ext.meta`  - dimensions of the avatar in the Metaverse different from the default ones
- `ext.game` - dimensions of the avatar in the virtual game different from the default ones
- `ext.user` - dimensions of the avatar edited by the player at the start of the game

## PC
- `PC.`
- object with the player character; 
- all active avatar dimensions and clothes
- also attributes, skills and perks used in the virtual game
- `PC.base` - base value of character attributes in the virtual game (`PC.att` are calculated, read-only)
- `PC.base.str` - strength 
- `PC.base.dex` - dexterity 
- `PC.base.int` - intelligence 
- `PC.base.char` - charisma 
- `PC.base.hp` - max health
- `PC.base.eng` - max stamina (energy) 

[🔙  Index](./index.md)