# Fake AI
![](./Images/promo.png)

A game about a popular girl who is blackmailed by three nerds to play a video game and fulfill their lewd tasks.

- [General Advices](General%20Advices.md)
- [Legend](Legend.md)
- [Cheats](Cheats.md)

<!-- -->

- [Main Stats](Main%20Stats.md)
- [Tasks](Tasks.md)
- [Shops](Shops.md)


### Arcs (not character-related)

- [Chairman](./Arcs%20(not%20character-related)/Chairman.md)
- [Body Modifications](./Arcs%20(not%20character-related)/Body%20Modifications.md)
- [Jobs](./Arcs%20(not%20character-related)/Jobs.md)
- [Stripping](./Arcs%20(not%20character-related)/Stripping.md)
- [Whoring](./Arcs%20(not%20character-related)/Whoring.md)


### Main Characters 
- [Kate](./Main%20Characters/Kate.md) - the main character
- [Eva](./Main%20Characters/Eva.md) - Kate's best friend, a bit of bitch
- [Saša](./Main%20Characters/Saša.md) - Kate's best friend, a bit needy

<!-- -->

- [Juraj](./Main%20Characters/Juraj.md) - the nerd with the crush
- [Zdeno](./Main%20Characters/Zdeno.md) - the creepy one
- [Matej](./Main%20Characters/Matej.md) - the clumsy one


### Minor Characters
- [Tom](./Minor%20Characters/Tom.md) - jock flirting with Kate
- [Žanet](./Minor%20Characters/Žanet.md) - cool, bit slutty classmate
- [Vendy](./Minor%20Characters/Vendy.md) - wannabe
- [Mike](./Minor%20Characters/Mike.md) - handsome popular classmate 

<!-- -->

- [Peter](./Minor%20Characters/Peter.md) - pretty cool guy
- [Kubo](./Minor%20Characters/Kubo.md) - Kates ex-boyfriend
- [Sam](./Minor%20Characters/Sam.md) - Eva's boyfriend

<!-- -->

- [Mrs Majerová](./Minor%20Characters/Mrs%20Majerová.md) - Literature & Form teacher
- [Mr Molnár](./Minor%20Characters/Mr%20Molnár.md) - German Language teacher
- [Ms Bosáková](./Minor%20Characters/Ms%20Bosáková.md) - Physic Education teacher
- [Mr Ďuro](./Minor%20Characters/Mr%20Ďuro.md) - History teacher
- [Mr Liška](./Minor%20Characters/Mr%20Liška.md) - Biology teacher
- [Mr Franz](./Minor%20Characters/Mr%20Franz.md) - Deputy Principal

<!-- -->

- [Ivana](./Minor%20Characters/Ivana.md) - Kate's mother
- [Mr Sokol](./Minor%20Characters/Mr%20Sokol.md) - Juraj's father
- [Bára](./Minor%20Characters/Bára.md) - Juraj's cousin

<!-- -->

- [Světa](./Minor%20Characters/Světa.md) - stripper
- [Marika](./Minor%20Characters/Marika.md) - coordinator working for the Agency 
- [Timothée](./Minor%20Characters/Timothée.md) - token black guy
- [Justýna](./Minor%20Characters/Justýna.md) - woman from bar 

<!-- -->

- [Others](./Minor%20Characters/Others.md)

### Virtual Game
- [Act I](./Virtual%20Game/Act%20I.md)
- [Act II](./Virtual%20Game/Act%20II.md)
- [Places and People](./Virtual%20Game/Places%20and%20People.md)

### Spoiler Guides 
- [Getting Reelected](../Spoiler%20Guides/Getting%20Reelected.md)
- [Dating Saša](../Spoiler%20Guides/Dating%20Saša.md)
- [Vengentful Eva](../Spoiler%20Guides/Vengentful%20Eva.md)
- [Grades](../Spoiler%20Guides/Grades.md)
- [Events](../Spoiler%20Guides/Events.md)

### Notes
- [FAQ](./Notes/FAQ.md)
- [Outfit Sluttiness](./Notes/Sluttiness.md)
- [Combat Considerations](./Notes/Combat%20Considerations.md)
- [School Classes](./Notes/School%20Classes.md)
- [Technology](./Notes/Technology.md)
- [Life and Institutions](./Notes/Life%20and%20Institutions.md)
 
