/**
	WEBPACK SETTING USED IN PRODUCTION
*/
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const sveltePreprocess = require("svelte-preprocess");

module.exports = merge(common, {
	mode: 'production',
	module: {
		rules: [
			//BABEL - changes moder JS into JS that works even on older browsers
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
						targets: "since 2018",
					},
				},
			},
			
			//SVELTE - Javascript framework
			{
				test: /\.svelte$/,
				include: /src/,
				use: {
					loader: 'svelte-loader',
					options: {
						preprocess: require('svelte-preprocess')({
							//to also run Babel to fix JS in .svelte files 
							//see: https://github.com/sveltejs/svelte-preprocess#modern-javascript-syntax
							babel: {
								presets: [
									[
										'@babel/preset-env', 
										{
											loose: true, //not sure what are benefits
											modules: false, //allegedly no need for babel to resolve modules
											targets: "since 2018"
											/*
												Svelte require  ES6+
												probably could be even a bit sooner but 4 years back sounds like enough
											*/
										},
									],
								],
							},
						})
					},
				},
			},
		]
	}

});







