/**
	WEBPACK SETTING USED DURING DEVELOPMENT 
*/
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');
const webpack = require('webpack'); 

module.exports = merge(common, {
	mode: 'development',
	//devtool: 'source-map', //?????'inline-source-map'
	devServer: {
		historyApiFallback: true,
		contentBase: path.resolve(__dirname, './dist'), //TODO - USE static?
		open: true,
		compress: true,
		//hot: true,
		port: 8080,
		watchContentBase: true,
	},
	
	module: {
		rules: [
			//SVELTE - framework for interface
			{
				test: /\.svelte$/,
				use: {
					loader: 'svelte-loader',
				},
			},
		]
	}
	
});