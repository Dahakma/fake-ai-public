/**
	WEBPACK SETTING USED ALWAYS 
*/
const path = require('path');
const webpack = require('webpack'); 
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
	
	entry: {
		intro: {
			import: './src/intro.js',
		},
		
		main: {
			import: './src/index.js',
			dependOn: 'intro',
		},
	},
	
	output: {
		path: path.resolve(__dirname, './dist/'),
		filename: 'data/[name].bundle.js',
		clean: true,
		assetModuleFilename: 'assets/[hash][ext][query]',
	},
	
	resolve: {
		alias: { //i.e. Assets/something creates path to ./src/assets/something without caring about complex relative paths
			Assets: path.resolve(__dirname, 'src/assets/'),
			//Textures: path.resolve(__dirname, 'src/assets/textures/'),
			Libraries: path.resolve(__dirname, 'src/system/libraries/'),
			System: path.resolve(__dirname, 'src/system/system/'),
			Loop: path.resolve(__dirname, 'src/system/loop/'),
			Avatar: path.resolve(__dirname, 'src/system/avatar/'),
			Story: path.resolve(__dirname, 'src/story/'),
			Style: path.resolve(__dirname, 'src/style/'),
			Dungeon: path.resolve(__dirname, 'src/system/dungeon/'),
			Minigames: path.resolve(__dirname, 'src/minigames/'),
			Virtual: path.resolve(__dirname, 'src/system/virtual/'),
			Taskmaster: path.resolve(__dirname, 'src/system/taskmaster/'),
			Data: path.resolve(__dirname, 'src/data/'),
			Src: path.resolve(__dirname, 'src/'),
			Gui: path.resolve(__dirname, 'src/system/gui/'),
		},
	},
  
	module: {
		rules: [
			//MARKDOWN - my script, changelong.md & credist.md are converted from markdown to js
			{
				test: /\.md$/,
				//exclude: /node_modules/,
				//include: /src/,
				use: [{
					loader: path.resolve('./scripts/markdown_loader.js'),
				}],			
			},
			
			//ASSETS - images, audio, etc. 
			{
				test: /\.(wav|mp3|png|svg|gif)/,
				type: 'asset/resource',
			},
			
			
			//CSS
			{
				test: /\.(scss|css)$/,
				//use: [MiniCssExtractPlugin.loader, 'css-loader'],
				use: [/*'style-loader'*/MiniCssExtractPlugin.loader, 'css-loader', /*'postcss-loader',*/ /*'sass-loader'*/],
			}
			
		],
	},
	 plugins: [
		//new BundleAnalyzerPlugin(),
		new ESLintPlugin({
			fix: true,
			exclude: [
				//`./src/system/avatar/game`,
				
				`./src/minigames/`,
				`./src/system/libraries/`,
			],
		}),
		
		new MiniCssExtractPlugin({ //extracts CSS into separate file
			filename: "data/[name].css",
		}),
		
		new HtmlWebpackPlugin({ //creates HTML page 
			//title: 'webpack Boilerplate',
			template: path.resolve(__dirname, './src/webpage.html'), // template file
			filename: 'index.html', // output file
		}),
	],
	

	
	optimization: {
		runtimeChunk: 'single',
		minimize: true, // !!!
		splitChunks: {
			cacheGroups: {
				vendor: {
					test: /[\\/]node_modules[\\/]/,
					name: 'vendors',
					chunks: 'all'
				}
			}
		}
	},
	
}